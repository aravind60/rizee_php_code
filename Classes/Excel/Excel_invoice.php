<?php
require_once 'PHPExcel.php';
$excel = new PHPExcel();
$excel->setActiveSheetIndex(0)
 ->setCellValue('A1','Exporter Name')
 ->setCellValue('B1','Exporter Address')
 ->setCellValue('C1','phone No.')
 ->setCellValue('D1','Email')
 ->setCellValue('E1','Website');
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="IMPEXP_Excel.xlsx"');
header('Cache-Control: max-age=0');
$file = PHPExcel_IOFactory::createWriter($excel,'Excel2007');
$file->save('php://output');
?>