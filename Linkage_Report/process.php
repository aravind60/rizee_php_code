<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>

<style>
.card-header h6{
font-family: 'Varela Round', sans-serif !important;
}
.card{
	font-family: 'Varela Round', sans-serif !important;
}
</style>
	<?php
	$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
	//$con = mysqli_connect("localhost","root","","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}
	if(isset($_REQUEST['addForm'])){
		
		
	
	?>
		
	<script>
	$('#exampaper').selectpicker1();
	</script>
	
	
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Questions Report</h6>
							</div>
							<div class="card-body">
								
								<div class="form-row">
									
									<div class="col-md-3 mb-3">
									  <label for="validationCustom02">Class</label>
										<select id="class" name="class" class="form-control"   onchange="setState('aaa','<?php echo SECURE_PATH;?>Linkage_Report/ajax.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter=<?php echo $_POST['chapter']; ?>')"  >
											<option value=''>-Select-</option>
											<?php
											$sql=$database->query("select * from class where estatus='1'");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['class'])) { if($_REQUEST['class']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['class']; ?></option>
											<?php
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
									</div>
									<div class="col-md-3 mb-3">
									  <label for="validationCustom02">Subject</label>
										<select id="subject" name="subject" class="form-control"  onchange="setState('aaa','<?php echo SECURE_PATH;?>Linkage_Report/ajax.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter=<?php echo $_POST['chapter']; ?>')"  >
											<option value=''>-Select-</option>
											<?php
											$sql=$database->query("select * from subject where estatus='1'");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['subject']; ?></option>
											<?php
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
									  
									</div>
									
									<div class="col-md-4 mb-3" id="aaa" >
									  <label for="validationCustom02">Chapter</label>
										<select id="chapter" name="chapter" class="form-control selectpicker1"  multiple data-actions-box="true" data-live-search="true"  >
											<?php
											$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1'  "); 
											while($data1 = mysqli_fetch_array($row1))
											{
											  ?>
											<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data1['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['chapter'];?></option>
											<?php
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
									  
									</div>

									
									
									<div class="col-md-3 mb-3" >
									  <label for="validationCustom02">Verication Status</label>
										<select id="status" name="status" class="form-control selectpicker5"  multiple data-actions-box="true" data-live-search="true"  >
											<option value='0' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='0') { echo 'selected="selected"'; }  } ?>>Pending</option>
											<option value='1' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='1') { echo 'selected="selected"'; }  } ?>>Verified</option>
											<option value='2' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='2') { echo 'selected="selected"'; }  } ?>>Rejected</option>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['status'])){ echo $_SESSION['error']['status'];}?></span>
									  
									</div>
									<div class="col-md-3 mb-3" >
									  <label for="validationCustom02">Verifier names</label>
										<select id="vusername" name="vusername" class="form-control selectpicker4"  multiple data-actions-box="true" data-live-search="true" >
											<?php
											$sel=$database->query("select * from users where valid='1' and userlevel='3'");
											while($row=mysqli_fetch_array($sel)){
											 ?>
											<option value="<?php echo $row['username'];?>" <?php if(isset($_POST['vusername'])) { if(in_array($row['username'], $_POST['vusername'])) { echo 'selected="selected"'; } } ?>   ><?php echo $row['username'];?></option>
											<?php
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['vusername'])){ echo $_SESSION['error']['vusername'];}?></span>
									  
									</div>
									<div class="col-md-4 mb-3" >
									  <label for="validationCustom02">Question Type</label>
										<select id="questiontype" name="questiontype" class="form-control selectpicker2"  multiple data-actions-box="true" data-live-search="true" >
											
												<?php
												$dquestion = $database->query("SELECT * FROM questiontype WHERE estatus='1' "); 
												
												while($datadquestion = mysqli_fetch_array($dquestion))
												{
													?>
												<option value="<?php echo $datadquestion['id'];?>" <?php if(isset($_REQUEST['questiontype'])) { if($_REQUEST['questiontype']==$datadquestion['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datadquestion['questiontype'];?></option>
												<?php
												}
												?>
												
											</select>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['type'])){ echo $_SESSION['error']['type'];}?></span>
									  
									</div>
									<div class="col-md-3 mb-3" >
									  <label for="validationCustom02">Review Status</label>
										<select id="review_status" name="review_status" class="form-control selectpicker6"  multiple data-actions-box="true" data-live-search="true"  >
												<option value='0' <?php if(isset($_REQUEST['review_status'])) { if($_REQUEST['review_status']=='0') { echo 'selected="selected"'; }  } ?>>Pending</option>
												<option value='1' <?php if(isset($_REQUEST['review_status'])) { if($_REQUEST['review_status']=='1') { echo 'selected="selected"'; }  } ?>>Verified</option>
												<option value='2' <?php if(isset($_REQUEST['review_status'])) { if($_REQUEST['review_status']=='2') { echo 'selected="selected"'; }  } ?>>Rejected</option>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['type'])){ echo $_SESSION['error']['type'];}?></span>
									  
									</div>
									<div class="col-md-3 mb-3" >
									  <label for="validationCustom02">Reviewer names</label>
										<select id="username" name="username" class="form-control selectpicker3"  multiple data-actions-box="true" data-live-search="true" >
											<?php
											$sel=$database->query("select * from users where valid='1' and userlevel='6'");
											while($row=mysqli_fetch_array($sel)){
											 ?>
											<option value="<?php echo $row['username'];?>" <?php if(isset($_POST['username'])) { if(in_array($row['username'], $_POST['username'])) { echo 'selected="selected"'; } } ?>   ><?php echo $row['username'];?></option>
											<?php
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['type'])){ echo $_SESSION['error']['type'];}?></span>
									  
									</div>
									<div class="col-md-3 mb-3" >
									  <label for="validationCustom02">Previous Papers</label>
										<select id="type" name="type" class="form-control "   >
											<option value=''>-Select-</option>
											<option value='1'>Include</option>
											<option value='2'>Exclude</option>
											<option value='3'>Only Previous Papers</option>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['type'])){ echo $_SESSION['error']['type'];}?></span>
									  
									</div>
									
								 </div>
								 <div class="text-right col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<a type="button" class="btn-blue my-2 px-5 btn btn-primary text-white" onclick="setState('adminTable','<?php echo SECURE_PATH;?>Linkage_Report/process.php','tableDisplay=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&type='+$('#type').val()+'&status='+$('#status').val()+'&questiontype='+$('#questiontype').val()+'&review_status='+$('#review_status').val()+'&username='+$('#username').val()+'&vusername='+$('#vusername').val()+'');">Submit</a>
								</div> 
								 
									
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
		<?php
			unset($_SESSION['error']);
	}
		?>
	<?php
		if(isset($_REQUEST['tableDisplay'])){
			$field = 'subject';
			if(!$_REQUEST['subject'] || strlen(trim($_REQUEST['subject'])) == 0){
			  $_SESSION['error'][$field] = "* Please select Subject";
			}
			$field = 'class';
			if(!$_REQUEST['class'] || strlen(trim($_REQUEST['class'])) == 0){
			  $_SESSION['error'][$field] = "* Please select class";
			}
			
			if(count($_SESSION['error']) > 0){
			?>
				<script type="text/javascript">
				$('#adminForm').slideDown();

				setState('adminForm','<?php echo SECURE_PATH;?>Linkage_Report/process.php','addForm=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&type=<?php echo $_REQUEST['type'];?>&status=<?php echo $_REQUEST['status'];?>&questiontype=<?php echo $_REQUEST['questiontype'];?>&review_status=<?php echo $_REQUEST['review_status'];?>&username=<?php echo $_REQUEST['username'];?>&vusername=<?php echo $_REQUEST['vusername'];?>')
				</script>

			<?php
			}else{
				$sql=$database->query("select * from subject where estatus='1' and id='".$_REQUEST['subject']."'");
				$row=mysqli_fetch_array($sql);
					?>	
						<div class="container pt-3">
							<div class="text-center pt-2 pb-2" >
							<a class="btn btn-primary text-white" href="<?php echo SECURE_PATH;?>Linkage_Report/report.php?class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&subjectname=<?php echo $row['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&type=<?php echo $_REQUEST['type']; ?>&status=<?php echo $_REQUEST['status']; ?>&questiontype=<?php echo $_REQUEST['questiontype']; ?>&review_status=<?php echo $_REQUEST['review_status']; ?>&username=<?php echo $_REQUEST['username']; ?>&vusername=<?php echo $_REQUEST['vusername']; ?>" style="float:center" target="_blank"  title="PDF Export" >Download PDF <i class="text-center fa fa-file-pdf-o" style="font-size: 25px;color:red;"></i></a>
							</div>
						</div>
						<script type="text/javascript">
						$('#example').DataTable({
							
							
						});
						</script>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Reviewer Report</h6>
											</div>
											<div class="card-body">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Reviewer Name</th>
															<th> Chapter</th>
															<th> Reviewed</th>
															<th> Review Pending</th>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("select * from users where  userlevel='6' and valid='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															if(isset($_REQUEST['chapter'])){
																if($_REQUEST['chapter']!=""){
																	$chapter=" AND id in (".$_REQUEST['chapter'].")";
																} else {
																	$chapter="";
																}
															}else{
																$chapter="";
															}
															$selchap=$database->query("select * from chapter where estatus='1'  and subject in (".$rowcom['subject'].") ".$chapter."");
															while($rowchap=mysqli_fetch_array($selchap)){
																//$sqlee=$database->query("select id from createquestion where estatus='1'  and subject in (".$rowcom['subject'].") and find_in_set(".$rowchap['id'].",chapter)>0 ");
																//if(mysqli_fetch_array($sqlee)>0){
																	//while($rowchapter=mysqli_fetch_array($sqlee)){

																	$sqler11=$database->query("select id from createquestion where estatus='1'  and subject in (".$rowcom['subject'].") and find_in_set(".$rowchap['id'].",chapter)>0 and vstatus1='1'");
																	$rowe11=mysqli_num_rows($sqler11);

																	$sqler=$database->query("select id from createquestion where estatus='1'  and subject in (".$rowcom['subject'].") and find_in_set(".$rowchap['id'].",chapter)>0 and vstatus1='1' and review_status='1' ");
																	$rowe=mysqli_num_rows($sqler);

																	$sqler1=$database->query("select id from createquestion where estatus='1'  and subject in (".$rowcom['subject'].") and find_in_set(".$rowchap['id'].",chapter)>0 and vstatus1='1' and review_status='0'");
																	$rowe1=mysqli_num_rows($sqler1);
																	if($rowe11>0){
																?>
																<tr class="<?php echo $style; ?>">
																	<td><?php echo $rowcom['username']; ?></td>
																	<td><?php echo $rowchap['chapter']; ?></td>
																	<td><?php echo $rowe; ?></td>
																	<td><?php echo $rowe1; ?></td>
																</tr>
																
																<?php
																	}
																
																//}
															}
														}
														?>
													
													
												</table>
												
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
				<?php	
				
			}
	}		
	?>

	
	<script>
	function examtypediv(){
		var exam=$('#exam').val();
		if(exam=='2'){
			$('#examtypedatadiv').show();
		}else{
			$('#examtypedatadiv').hide();
		}

	}
	
	</script>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
   $(".selectpicker5").selectpicker('refresh');
 $(".selectpicker6").selectpicker('refresh');
</script>