<?php
 session_start();
 //define('SECURE_PATH','http://'.$_SERVER['HTTP_HOST'].'/rizee/');
define('SECURE_PATH','http://'.$_SERVER['HTTP_HOST'].'/');
ini_set("display_errors","ON");
//$conn=mysqli_connect('localhost','root','','qsbank') ;
$conn=mysqli_connect('localhost','rspace','Rsp@2019','qsbank') ;

function query($sql)
{
    global $conn;
	return mysqli_query($conn,$sql);
}
//$conn1=mysqli_connect('localhost','root','','neetjee') ;
$conn1=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
function query1($sql)
{
    global $conn1;
	return mysqli_query($conn1,$sql);
}
?>
<script>
function studentexamtype(val){
		var examtype=$("#exam_short_full").val();
		
		if(val=='1'){
			$("#exam").val("1");
			if(examtype==1){
				$("#duration").val("45 Mins");
			}else if(examtype==3){
				$("#duration").val("15 Mins");
			}else{
				$("#duration").val("3 Hours");
			}
		}else if(val=='2'){
			$("#exam").val("2");
			if(examtype==1){
				$("#duration").val("1 Hour");
			}else if(examtype==3){
				$("#duration").val("15 Mins");
			}else{
				$("#duration").val("3 Hours");
			}
		}

	}
	function studentexamtype2(val){
		var exam=$('#exam').val();
		if(val=='1'){
			if(exam==1){
				$(".assist45Min").show();
				$(".assist3Hrs").hide();
				$(".assist_1h").hide();
				$(".assist15Min").hide();
				$("#duration").val("45 Mins");

			}else{
				$(".assist45Min").hide();
				$(".assist3Hrs").hide();
				$(".assist_1h").show();
				$(".assist15Min").hide();
				$("#duration").val("1 Hour");
			}
			$("#exam_short_full").val("1");
			$("#examtype").val("1");
            
		}else if(val=='2'){
			$("#exam_short_full").val("2");
			if(exam==1){
				$(".assist45Min").hide();
				$(".assist15Min").hide();
				 $(".assist3Hrs").show();
				$(".assist_1h").hide();
				$("#duration").val("3 Hours");
			}else{
				$(".assist45Min").hide();
				$(".assist15Min").hide();
				$(".assist3Hrs").show();
				$(".assist_1h").hide();
				$("#duration").val("3 Hours");
			}
			$("#examtype").val("1");
		}else if(val=='3'){
			$("#exam_short_full").val("3");
			$("#examtype").val("2");
			if(exam==1){
				$(".assist45Min").hide();
				 $(".assist3Hrs").hide();
				$(".assist_1h").hide();
				$(".assist15Min").show();
				$("#duration").val("15 Mins");
			}else{
				$(".assist45Min").hide();
				$(".assist3Hrs").hide();
				$(".assist_1h").hide();
				$(".assist15Min").show();
				$("#duration").val("15 Mins");
			}
			
		}


	}
	function studentexamyear(value){
		
		$("#class").val(value);

	}
	function subjectdatadiv(value){
		//alert(value);
		$("#subject").val(value);

	}

	function studentexamtype1(val){
		
		$("#examtype").val(val);
		

	}


	function stufirstnameCheck(){
		var stufirstnameval = $('#firstname').val();
		
		if(stufirstnameval.length==0){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname should not be empty");
			 $('#stufirstname_error').css("color","red");
             $('#stufirstname_error').css("font-size",10);
		}else if(stufirstnameval.length < 4 || stufirstnameval.length > 20){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname name must be between 4 and 20 characters");
			 $('#stufirstname_error').css("color","red");
                    $('#stufirstname_error').css("font-size",10);
		}else
		{
			$('#stufirstname_error').hide();
		}

	}
	function stupassCheck(){
		var stupassval = $('#password').val();
		
		if(stupassval.length==0){
			$('#stupassword_error').show();
			$('#stupassword_error').html("Password should not be empty");
			 $('#stupassword_error').css("color","red");
             $('#stupassword_error').css("font-size",10);
		}else
		{
			$('#stupassword_error').hide();
		}

	}

	function stupassCheck1(){
		var stupassval = $('#repassword').val();
		
		if(stupassval.length==0){
			$('#stupassword_error1').show();
			$('#stupassword_error1').html("Password should not be empty");
			 $('#stupassword_error1').css("color","red");
             $('#stupassword_error1').css("font-size",10);
		}else
		{
			$('#stupassword_error1').hide();
		}

	}
	
	function stuemailCheck(){
		var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
		);
		var stuemail_errorval = $('#email').val();
		if(stuemail_errorval.length ==0){
			
			$('#stuemail_error').show();
			$('#stuemail_error').html("Please enter email");
			$('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		}else if(stuemail_errorval.length < 4){
			 
			$('#stuemail_error').show();
			$('#stuemail_error').html("email cannot be less than 5 chars");
			$('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		}else if(!pattern.test(stuemail_errorval)){
			
			$('#stuemail_error').show();
			$('#stuemail_error').html("Invalid email");
			 $('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		   
		}
		else
		{
			$('#stuemail_error').hide();
		}

	}
	function stumobilenoCheck(){
		var pattern = new RegExp("^[6-9][0-9]{9}$");
		var stumobileno_errorval = $('#mobile').val();
		if(stumobileno_errorval.length=""){
			
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Mobile No. Cannot Be Empty");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
			
		}else if(!pattern.test(stumobileno_errorval)){
			
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Invalid Mobile No.");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
		}
		else
		{
			$('#stumobileno_error').hide();
		}

	}
	function sendotpdata(){
		alert(1);
		fbq('track', 'SendOTP');
	}
</script>
<?php

if(isset($_REQUEST['addForm'])){ 
	if(isset($_REQUEST['otpvalue'])){
		?>
			<script>
			$('#getOTPdiv').show();
		$('#assessmentotp').hide();
		
		countdowninterval();
			</script>
		<?php
	}
	if(isset($_POST['mobile'])){
		if($_POST['mobile']!=''){
			$mobile=$_POST['mobile'];
		}else{
			$mobile='';
		}
	}else{
		$mobile='';
	}
	if(isset($_POST['email'])){
		if($_POST['email']!=''){
			$email=$_POST['email'];
		}else{
			$email='';
		}
	}else{
		$email='';
	}
	if(isset($_REQUEST['src'])){
		if($_REQUEST['src']!=''){
			$src=$_REQUEST['src'];
		}else{
			$src='';
		}
	}else{
		$src='';
	}
	print_r($_SESSION);
	?>
	
	  <form action="" class="jumbotron py-4 register-form">
		<div class="row align-items-center">
			<div class="col-xl-7 col-lg-7 col-md-12">
				<h5 class="mb-4 text-darkblue font-weight-bold">Personal Information: </h5>
				<div class="form-label-group">
					<input type="text" class="form-control" id="firstname" placeholder="Name" value="<?php if(isset($_REQUEST['firstname'])){ echo $_REQUEST['firstname'];}?>"  onkeyup="stufirstnameCheck();" required autocomplete="off">
					<label for="firstname" class="custom-label">Name</label>
					<span id="stufirstname_error"  class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['firstname'])){ echo $_SESSION['error']['firstname'];}?></span>
				</div>
				<div class="form-label-group">
					<input type="phone" class="form-control" id="mobile" value="<?php if(isset($_REQUEST['mobile'])){ echo $_REQUEST['mobile'];}?>" onkeyup="stumobilenoCheck();"  onkeypress="return isNumber(event,$(this),10)"  maxlength="10" required autocomplete="off"
						placeholder="Mobile Number">
					<label for="mobile" class="custom-label">Mobile
						Number</label>
					<span id="stumobileno_error" class="text-danger" style="font-size:13px;"> <?php if(isset($_SESSION['error']['mobile'])){ echo $_SESSION['error']['mobile'];}?></span>
				</div>
				<div class="form-label-group">
					<input type="email" class="form-control" id="email" value="<?php if(isset($_REQUEST['email'])){ echo $_REQUEST['email'];}?>" onkeyup="stuemailCheck();" required autocomplete="off"
						placeholder="Email id">
					<label for="email" class="custom-label">Email id</label>
					<span  id="stuemail_error" class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['email'])){ echo $_SESSION['error']['email'];}?></span>
				</div>
				<div class="form-label-group">
					<input type="password" class="form-control" id="password" value="<?php if(isset($_REQUEST['password'])){ echo $_REQUEST['password'];}?>" onkeyup="stupassCheck();" required autocomplete="off"
						placeholder="Password">
					<label for="password" class="custom-label">Password</label>
					<span  id="stupassword_error" class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['password'])){ echo $_SESSION['error']['password'];}?></span>
				</div>
				<div class="form-label-group">
					<input type="password" class="form-control" id="repassword" placeholder="Re-Password" value="<?php if(isset($_REQUEST['repassword'])){ echo $_REQUEST['repassword'];}?>" onkeyup="stupassCheck1();" required autocomplete="off"
					   >
					<label for="repassword" class="custom-label">Re-Password</label>
					<span  id="stupassword_error" class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['repassword'])){ echo $_SESSION['error']['repassword'];}?></span>
				</div>
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-12">
							<div class="form-label-group">
								<select id="exam_id" class="custom-select theme-select">
									<option value="">Select Exam</option>
									<?php
								$sql=query("select * from exam where estatus='1' and id in (1,2)");
								while($row=mysqli_fetch_array($sql)){
									?>
										<option value='<?php echo $row['id']; ?>' <?php if(isset($_POST['exam_id'])){ if($_POST['exam_id']==$row['id'])
							echo 'selected="selected"';} ?>><?php echo $row['exam']; ?></option>
										<?php
								}
								?>
								</select>
								<label for="exam_id" class="custom-label">Select Exam</label>
								<span   class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['exam_id'])){ echo $_SESSION['error']['exam_id'];}?></span>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12">
							<div class="form-label-group">
								<select id="class_id" class="custom-select theme-select">
									<option value="">Select Class</option>
									<?php
								$sql=query("select * from class where estatus='1' ");
								while($row=mysqli_fetch_array($sql)){
									?>
										<option value='<?php echo $row['id']; ?>' <?php if(isset($_POST['class_id'])){ if($_POST['class_id']==$row['id'])
							echo 'selected="selected"';} ?>><?php echo $row['class']; ?></option>
										<?php
								}
								?>
								</select>
								<label for="class_id" class="custom-label">Select Class</label>
								<span   class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['class_id'])){ echo $_SESSION['error']['class_id'];}?></span>
							</div>
						</div>
						<div class="col-xl-6 col-lg-6 col-md-12">
							<div class="form-label-group">
								<select id="year" class="custom-select theme-select">
								<option value=''>Select Year</option>
									<option value='2020' <?php if(isset($_POST['year'])){ if($_POST['year']=='2020')
							echo 'selected="selected"';} ?>>2020</option>
								<option value='2021' <?php if(isset($_POST['year'])){ if($_POST['year']=='2021')
							echo 'selected="selected"';} ?>>2021</option>
								<option value='2022' <?php if(isset($_POST['year'])){ if($_POST['year']=='2022')
							echo 'selected="selected"';} ?>>2022</option>
							</select>
								</select>
								<label for="year" class="custom-label">Select Year</label>
								<span   class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['year'])){ echo $_SESSION['error']['year'];}?></span>
							</div>
						</div>
					<!-- <div class="col-xl-6 col-lg-6 col-md-12">
						<div class="form-label-group">
							<select name="exam_id" id="exam_id" class="form-control"  placeholder="Select Exam">
								<option selected=""></option>
								<?php
								$sql=query("select * from exam where estatus='1' and id in (1,2)");
								while($row=mysqli_fetch_array($sql)){
									?>
										<option value='<?php echo $row['id']; ?>' <?php if(isset($_POST['exam_id'])){ if($_POST['exam_id']==$row['id'])
							echo 'selected="selected"';} ?>><?php echo $row['exam']; ?></option>
										<?php
								}
								?>
							</select>
							<label for="exam_id" >Select Exam</label>
							
						</div>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-12">
						<div class="form-label-group">
							<select name="year" id="year" class="form-control" placeholder="Select Year">
								<option selected=""></option>
								<option value='2020' <?php if(isset($_POST['year'])){ if($_POST['year']=='2020')
							echo 'selected="selected"';} ?>>2020</option>
								<option value='2021' <?php if(isset($_POST['year'])){ if($_POST['year']=='2021')
							echo 'selected="selected"';} ?>>2021</option>
								<option value='2022' <?php if(isset($_POST['year'])){ if($_POST['year']=='2022')
							echo 'selected="selected"';} ?>>2022</option>
							</select>
							<label for="year" >Select Year</label>
						</div>
					</div> -->
				</div>
				<div class="form-group" id="assessmentotp">
					<a class="btn btn-darkblue" type="button" onClick="setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','formdataval=1&firstname='+$('#firstname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&password='+$('#password').val()+'&repassword='+$('#repassword').val()+'&exam_id='+$('#exam_id').val()+'&class_id='+$('#class_id').val()+'&year='+$('#year').val()+'&src=<?php echo $src; ?>');sendotpdata();">Send
						OTP</a>
				</div>
			</div>
			<div class="col-xl-5 col-lg-5 col-md-12" id="getOTPdiv" style="display:none;">
				<h5 class="mb-4 text-darkblue font-weight-bold">OTP Verification</h5>
				<div class="form-label-group">
					<input type="text" class="form-control" id="otp"
						placeholder="Mobile Number" >
					<label for="otp" class="custom-label">Enter OTP</label>
					<span   class="text-danger" style="font-size:13px;"><?php if(isset($_SESSION['error']['otp'])){ echo $_SESSION['error']['otp'];}?></span>
				</div>
				<address>
					<p>OTP sent to your <br />Mobile No. +91 <?php echo $mobile; ?> and your Email-ID
						<?php echo $email; ?></p>
				</address>
				<div class="d-flex justify-content-between align-items-center" >
					<small id="timingtype">Resend OTP In <span id="time">00:00</span> Secs</small>
					<small class="text-warning" id="resendtype" style="display:none;"><a onclick="emailsendfunction();countdowninterval();" style="cursor:pointer;"> Resend OTP</a></small>
				</div>
				<div class="form-group mt-4">
					<button class="btn btn-darkblue" type="button" onClick="setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','validateForm2=1&firstname='+$('#firstname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&password='+$('#password').val()+'&repassword='+$('#repassword').val()+'&exam_id='+$('#exam_id').val()+'&class_id='+$('#class_id').val()+'&year='+$('#year').val()+'&otp='+$('#otp').val()+'')" >OTP
						Submit</button>
				</div>
			</div>
		</div>
	</form>
	<?php
	unset($_SESSION['error']);
	}
	if(isset($_POST['formdataval'])){
		$_SESSION['error'] = array();
		$post =$_REQUEST;
		$id = 'NULL';
		
 
  
	$field = 'firstname';
	if(!$post['firstname'] || strlen(trim($post['firstname'])) == 0){
	  $_SESSION['error'][$field] = "* Firstname cannot be empty";
	}
	
	
  $mobile=$post['mobile'];
   $email=$post['email'];
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }
		$sql1=query1("select * from student_users where mobile='".$post['mobile']."'");
		$rowc=mysqli_num_rows($sql1);
		if($rowc>0){
			
			  $_SESSION['error']['mobile'] = "* A User account with mobile ".$post['mobile']." already exists! ";
			
			
		}

	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}
	$sql1=query1("select * from student_users where email='".$post['email']."'");
		$rowc=mysqli_num_rows($sql1);
		if($rowc>0){
			
			  $_SESSION['error']['email'] = "* A User account with Email ".$post['email']." already exists! ";
			
			
		}
	$field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	$field = 'repassword';
	if(!$post['repassword'] || strlen(trim($post['repassword'])) == 0){
	  $_SESSION['error'][$field] = "* Re Enter Password cannot be empty";
	}
	if($post['password']!=$post['repassword']){
		 $_SESSION['error'][$field] = "* Password and Re  cannot be empty";
	}
	$field = 'exam_id';
	if(!$post['exam_id'] || strlen(trim($post['exam_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam_id cannot be empty";
	}
	$field = 'class_id';
	if(!$post['class_id'] || strlen(trim($post['class_id'])) == 0){
	  $_SESSION['error'][$field] = "* Class Id cannot be empty";
	}
	$field = 'year';
	if(!$post['year'] || strlen(trim($post['year'])) == 0){
	  $_SESSION['error'][$field] = "* Target Year cannot be empty";
	}
	$field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 ||  $post['formdataval'] == 2){
		//print_r($_SESSION);
		
	?>
    <script type="text/javascript">
	
     setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','addForm=2&firstname=<?php echo $post['firstname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&password=<?php echo $post['password'];?>&repassword=<?php echo $post['repassword'];?>&exam_id=<?php echo $post['exam_id'];?>&class_id=<?php echo $post['class_id'];?>&year=<?php echo $post['year'];?>&src=<?php echo $post['src'];?>');
    </script>
  <?php
	}
	else{
		$options = [
					  'cost' => 12,
					'salt' => 'e12fd2278f952fd0d4a8d97701c85ce7'
				];
	  $pass=password_hash($post['password'], PASSWORD_BCRYPT, $options);
	  $pass1= substr($pass,3);
	  $password='$2b'.$pass1;
		if($post['src']!=''){
			if($post['src']=='fb'){
				$src='2';
			}else{
				$src='3';
			}
		}
		$result=query1("insert student_users set name='".$post['firstname']."',email='".$post['email']."',mobile='".$post['mobile']."',password='".$password."',target_year='".$post['year']."',exam_id='".$post['exam_id']."',class_id='".$post['class_id']."',userlevel='2',estatus='1',source='".$src."',timestamp='".time()."'");
		query1("insert registration_src set username='".$post['mobile']."',src='".$_SESSION['src']."',timestamp='".time()."'");
		
		if($result){
			$otp=rand(1,999999);
			$timestamp=time();
			$expirytime=$timestamp + 2*60;
			$post['name']=$post['firstname'];

			$sql=query1("select * from verify_students where mobile='".$post['mobile']."'");
			$rowcount=mysqli_num_rows($sql);
			if($rowcount>0){
				$row=mysqli_fetch_array($sql);
				if($post['mobile']!=''){
					$message="Hi ".$post['name'].",your verification OTP is ".$row['email_otp'].".";
					$url="http://bsms.entrolabs.com/spanelv2/api.php?username=rizeemlp&password=rizeemlp6952&to=".$post['mobile']."&from=RIZAPP&message=".urlencode($message)."";
					
					
					@file($url);
				}
				@file(SECURE_PATH."Registration/email.php?email=".$post['email']."&otp=".$row['email_otp']."&name=".$post['name']."");
				unset($_SESSION['src']);
				?>
				
				<script>
				setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','addForm=1&otpvalue=1&firstname=<?php echo $post['firstname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&password=<?php echo $post['password'];?>&repassword=<?php echo $post['repassword'];?>&exam_id=<?php echo $post['exam_id'];?>&class_id=<?php echo $post['class_id'];?>&year=<?php echo $post['year'];?>');

				
				</script>
				<?php
				
			}else{
				
				$result=query1("INSERT INTO verify_students set name='".$post['name']."',email_otp='".$otp."',mobile_otp='".$otp."',mobile='".$post['mobile']."',email='".$post['email']."',timestamp='".$timestamp."'");
				if($result){
					$message="Hi ".$post['name'].",your verification OTP is ".$otp.".";
						$url="http://bsms.entrolabs.com/spanelv2/api.php?username=rizeemlp&password=rizeemlp6952&to=".$post['mobile']."&from=RIZAPP&message=".urlencode($message)."";
						
					
					@file($url);
					
					@file(SECURE_PATH."Registration/email.php?email=".$post['email']."&otp=".$otp."&name=".$post['name']."");
					unset($_SESSION['src']);
					?>
					<script>
					setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','addForm=1&otpvalue=1&firstname=<?php echo $post['firstname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&password=<?php echo $post['password'];?>&repassword=<?php echo $post['repassword'];?>&exam_id=<?php echo $post['exam_id'];?>&class_id=<?php echo $post['class_id'];?>&year=<?php echo $post['year'];?>');

					
					</script>
					<?php
					 
				}
			}
			//echo SECURE_PATH."registration/ajax1.php?sendemail=1&firstname=".$post['firstname']."&mobile=".$post['mobile']."&email=".$post['email']."";
			//@file(SECURE_PATH."registration/ajax1.php?sendemail=1&firstname=".$post['firstname']."&mobile=".$post['mobile']."&email=".$post['email']."");
			
			}
		
		
	}
	}

	if(isset($_POST['validateForm2'])){
	$_SESSION['error'] = array();
  $post =$_REQUEST;
  $id = 'NULL';
	$field = 'firstname';
	if(!$post['firstname'] || strlen(trim($post['firstname'])) == 0){
	  $_SESSION['error'][$field] = "* Firstname cannot be empty";
	}
	
	
  $mobile=$post['mobile'];
   $email=$post['email'];
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }
		

	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}
	$field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	$field = 'repassword';
	if(!$post['repassword'] || strlen(trim($post['repassword'])) == 0){
	  $_SESSION['error'][$field] = "* Re Enter Password cannot be empty";
	}
	
	$field = 'exam_id';
	if(!$post['exam_id'] || strlen(trim($post['exam_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam_id cannot be empty";
	}
	$field = 'class_id';
	if(!$post['class_id'] || strlen(trim($post['class_id'])) == 0){
	  $_SESSION['error'][$field] = "* Class ID cannot be empty";
	}
	$field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
  
$field = 'otp';
	if(!$post['otp'] || strlen(trim($post['otp'])) == 0){
	  $_SESSION['error'][$field] = "* OTP cannot be empty";
	}else if(strlen($post['otp']) > 0){
		$sql=query1("select count(id)  as cnt from verify_students where  mobile='".$post['mobile']."' and email_otp='".$post['otp']."' ");
			$rowl=mysqli_fetch_array($sql);
			
			if($rowl['cnt']==0){
				$_SESSION['error']['otp'] = "* Invalid OTP";
			}
			
		}
			
	
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm2'] == 2){
		//print_r($_SESSION['error']);
	?>
    <script type="text/javascript">
	setState('adminForm','<?php echo SECURE_PATH;?>Registration/process.php','addForm=2&firstname=<?php echo $post['firstname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&password=<?php echo $post['password'];?>&repassword=<?php echo $post['repassword'];?>&exam_id=<?php echo $post['exam_id'];?>&class_id=<?php echo $post['class_id'];?>&year=<?php echo $post['year'];?>&otp=<?php echo $post['otp']; ?>&otpvalue=1');
  
    </script>
  <?php
	}
	else{
		$currentdate= strtotime(date('d-m-Y'));
		
			$expiry_date=date('m/d/Y',strtotime('+7 days',$currentdate));
			
			$result=query1("update student_users set name='".$post['firstname']."',email='".$post['email']."',mobile='".$post['mobile']."',target_year='".$post['year']."',exam_id='".$post['exam_id']."',class_id='".$post['class_id']."',mobile_verified='1',valid='1',current_plan_id='5',expiry_date='".strtotime($expiry_date)."' where mobile='".$post['mobile']."'");
			if($result){ 
				
				?>


			<div class="row">
		<h2 class="text-success">You have Registered Successfully <i class="fa fa-thumbs-up" aria-hidden="true"></i></h2>
		</div>
		<br /><br >
		<h4 class="mr-5"> Please <a style="cursor:pointer;" href="https://rizee.in/student/">Login</a></h4>
		<script>
						<script>
					setTimeout(function(){
					window.location.href="https://rizee.in/student";
					
					
					}, 2000);
					</script>
					
				<?php
				
			}
		}
		
	}
?>
<script>
var timer;
function startTimer(duration, display) {
    timer = duration;
    var minutes, seconds;
    setInterval(function () {
        minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
            timer = duration;
			$('#resendtype').show();
			$('#timingtype').hide();
        }
    }, 1000);
}

function countdowninterval() {
	 fiveMinutes = 60 * 2,
        display = document.querySelector('#time');
    startTimer(fiveMinutes, display);
	
}


	
	function emailsendfunction(){
		
		var firstname=$('#firstname').val();
		var email=$('#email').val();
		var mobile=$('#mobile').val();
		var password=$('#password').val();
		var course=$('#course').val();
		var class_id=$('#class_id').val();
		var exam_id=$('#exam_id').val();
		if(email==''){
			
			stuemailCheck();
		}else{
			var datas = `sendemail=1&email=${email}&mobile=${mobile}&password=${password}&course=${course}&class_id=${class_id}&exam_id=${exam_id}&mobile=${mobile}&firstname=${firstname}`;

						$htmlObj=$.ajax({
						type:"POST",
						url:"<?php echo SECURE_PATH;?>Registration/ajax1.php",
						ifModified:true,
						dataType:"html",
						data: datas,
						cache: false,		 
						success: function(result) {
							$('#result').html("result",result);
							
							$('#resendtype').hide();
							$('#timingtype').show();
							}
						});
		}
	}
	function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>