<?php
include('../include/session.php');
?>
<!DOCTYPE html>
	<html>   
	
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="Swamy">
		<meta name="theme-color" content="#0c8bf1" />
		<meta name="keywords" content="">
		<link rel="shortcut icon" href="<?php echo SECURE_PATH;?>vendor/images/favicon.ico">

  		

		<?php echo $session->gdc_title();?>
		<?php echo $session->commonAdminCSS();?>
	
		<?php echo $session->commonJS();?> 
	
		
	</head>
	<body id="page-top">
		<div class="preloader">
			<div class="loader"></div>
		</div>
		<div id="loginForm">
			<?php
			if($session->logged_in){
			?>
				<script type="text/javascript">
				//navigate('<?php echo SECURE_PATH;?>home/','home');
				window.location = '<?php echo SECURE_PATH;?>home/';
				</script>
			<?php
			}
			else{
			?>
				<script type="text/javascript">
				setStateGet('loginForm','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
				</script>
			<?php
			}
			?>
		</div>
		<?php echo $session->commonFooterAdminJS();?>    
	</body>
</html>
