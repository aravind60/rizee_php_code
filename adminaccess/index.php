<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    	//header('Location: '.SECURE_PATH);
    }
    else
    {
    ?>
		<style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				/*margin:auto;*/
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}

			.list-Unstyles{
				position:absolute;
				z-index:30 !important;
				cursor:pointer;
			}
		</style>
                
				<div class="modal" id="myModal">
						<div class="modal-dialog">
						  <div class="modal-content">
						  
							<!-- Modal Header -->
							<div class="modal-header">
							  <h6 class="modal-title">Reset Password</h6>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							
							<!-- Modal body -->
							<div class="modal-body" id="viewdetails">
							 
							</div>
							
						  </div>
						</div>
					  </div>

					  	<div class="modal fade " id="messageDetails1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Users assigned by Chapters</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails1">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
							<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
						</div>
						</div>
					</div>
				</div>
                    <!-- Breadcrumbs-->
                    <section class="breadcrumbs-area2 my-3">
                        <div class="container">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="title">
                                    <h1 class="text-uppercase">Add Modules <small></small></h1>
                                </div>
                            </div>
                            <hr>
                        </div>
                    </section>
				
                    <!-- End Breadcrumbs-->

                    <!-- Content Area-->
                    <section class="content-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="card border-0 shadow mb-4">
                                        <div
                                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                            <h6 class="m-0 font-weight-bold text-primary">Modules</h6>
                                        </div>
                                        <div class="card-body">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-8">
                                                                <a href="#" class="btn btn-md btn-theme-2"onClick="$('#adminForm').slideToggle()">Admin Modules</a>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="adminForm" style="display:none;">
                                                            <div class="col-sm-8">
                                                                <script type="text/javascript">
                                                                    setStateGet('adminForm','<?php echo SECURE_PATH;?>adminaccess/process.php','addForm=1');
                                                                </script>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- End Cards-->
                    <!-- Content Area-->
                    <section>
                        <div id="adminTable">
                            <script type="text/javascript">
                                setStateGet('adminTable','<?php echo SECURE_PATH;?>adminaccess/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
                            </script>
                        </div>
                    </section>
                    <!-- End Cards-->
                 </div>
 <?php
    
    }
?>