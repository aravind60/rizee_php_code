<?php
include('../include/session.php');
error_reporting(0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>

 <?php echo $session->commonJS();?>
 <?php echo $session->commonAdminCSS();?>
   <style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				/*margin:auto;*/
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}

			.list-Unstyles{
				position:absolute;
				z-index:30 !important;
				cursor:pointer;
			}
		</style>
	
	<div id="adminTable">
		<?php
		if(isset($_REQUEST['viewDetails2']))
		{
			
			
			
		?>
		
		<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Users assigned by Chapters</h6>
						</div>
						<div class="card-body">
							
								
								<div class="table-responsive" >
									<table class="table table-bordered dashboard-table mb-0" >
										<thead class="thead-light">
											<tr>
												<th>Sr.No.</th>
												<th>Username</th>
												<th>Subject</th>
												<th>Chapter</th>
											</tr>
										</thead>
										<?php
										
										$k=1;
										$user=$database->query("select username,chapter from users where valid='1' and FIND_IN_SET(".$_REQUEST['chapter'].",`chapter`)");
										$rowdcount1=mysqli_num_rows($user);
										while($rowm=mysqli_fetch_array($user)){
											$sql=$database->query("select * from chapter where estatus='1' and id='".$_REQUEST['chapter']."'");
											$data1=mysqli_fetch_array($sql);
										?>	
											
												<td><?php echo $k;?></td>
												<td><?php echo $rowm['username']; ?></td>
												<td ><?php echo $database->get_name('subject','id',$data1['subject'],'subject'); ?></td>
												<td ><?php echo $data1['chapter'];?></td>
											<?php
											echo "</tr>";
											$k++;
										}
												?>
										</tbody>
								</table>
							</div>
							
					</div>
				</div>
			</div>
	</section>                              
		
<?php 
	
}

 ?>
 </div>
 <?php echo $session->commonFooterAdminJS(); ?>
 <script>

    $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
	</script>