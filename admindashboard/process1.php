<?php 
include('../include/session.php');
error_reporting(0);

if(isset($_REQUEST['verificationOverview'])){

    ?>


    <div class="card-header" id="verification">
        <a href="" class="d-flex justify-content-between align-items-center"
           data-toggle="collapse" data-target="#verifydiv" aria-expanded="true"
           aria-controls="verifydiv" >
            <h5 class="card-title mb-0"><b>Verification Overview</b></h5>
            <h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
        </a>
    </div>

    <div class="collapse show" id="verifydiv"   aria-labelledby="verification" >
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <?php
                $con="";
                $examcon="";

                    if($_REQUEST['exam']!=''){
                        $con=" AND id IN (".$_REQUEST['exam'].")";
                        $examcon=" AND exam IN (".$_REQUEST['exam'].")";
                    }
                    $sqlv=$database->query("select * from class where estatus='1'");
                    $i=1;
                    while($rowlv=mysqli_fetch_array($sqlv)){
                        $dataevtotal=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 ".$examcon);
                        $rowdataevtotal=mysqli_fetch_array($dataevtotal);
                        $dataetotal=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 ".$examcon."");
                        //echo "select count(*) from createquestion where   estatus='1' and class LIKE '%".$rowlv['id']."%'".$examcon."";
                        $rowdataetotal=mysqli_fetch_array($dataetotal);
                        $status = array('0' =>'Total Entered','1' =>'Verified','2' => 'Deleted','3' => 'Rejected','4' => 'No Chapter & Topic','5' => 'No Topic','6' => 'Pending');
                        if($i=='1'){
                            $style="mr-1";
                        }else{
                            $style="";
                        }
                        ?>
                    <div class="col-lg-6 col-md-6 col-sm-12 my-2">
                        <table class="table table-bordered <?php echo $style; ?> mb-0 bg-white">
                            <thead>
                            <tr>
                                <th class="bg-gray-color" colspan="">
                                    <div class="d-flex justify-content-between">
                                        <h5><b><?php echo "Class ".$rowlv['class']; ?></b></h5>
                                        <h5><?php  if($rowdataevtotal!=''){ echo $rowdataevtotal[0]; } else{ echo '0'; } ?></h5>
                                    </div>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                          
                            <?php
                            $sqll2=$database->query("select * from exam where estatus='1'".$con."");
                            while($rowll2=mysqli_fetch_array($sqll2)){
                                ?>
                                <tr class="bg-gray-color verification-items">


                                    <td class="d-flex justify-content-between p-0">
                                        <div class="item-td"><h6 class="text-dark"><b><?php echo $rowll2['exam']; ?></b></h6></div>
                                        <?php
                                        /*if($rowll2['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($rowll2['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($rowll2['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }*/
										$data3 = $database->query("select * from subject where estatus=1 and id in (".$rowll2['subjects'].") ");
                                        while($row3=mysqli_fetch_array($data3)){

                                            ?>

                                            <div class="item-td"><h6><?php echo $row3['subject']; ?></h6></div>


                                            <?php
                                        }
                                        ?>

                                    </td>
                                </tr>
                                <?php  foreach($status as $key => $stat){

                                    ?>
                                    <tr class="verification-items">

                                        <td class="d-flex justify-content-between p-0">
                                            <div class="item-td"><h6><?php echo $stat; ?></h6></div>
                                            <?php
                                           /* if($rowll2['id']=='1'){

                                                $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                            }else if($rowll2['id']=='2'){
                                                $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                            }else  if($rowll2['id']=='1,2'){
                                                $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                            }else{
                                                $data3 = $database->query("select * from subject where estatus=1");
                                            }*/
											$data3 = $database->query("select * from subject where estatus=1 and id in (".$rowll2['subjects'].") ");
                                            while($row3=mysqli_fetch_array($data3)){
                                                $dataetotalsub=$database->query("select count(id) from createquestion where    FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0  and subject='".$row3['id']."'  ");
                                                $rowdataetotalsub=mysqli_fetch_array($dataetotalsub);
												/*if($key=='2'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='2' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and vstatus1='".$key."' ");
												}else{
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and vstatus1='".$key."' ");
												}*/
												if($key=='0'){
													$dataetotalneet=$database->query("select count(id) from createquestion where    FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' ");
												}else if($key=='1'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and vstatus1='1' ");
												}else if($key=='2'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='0' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."'  ");
												}else if($key=='3'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and vstatus1='2'  ");
												}else if($key=='4'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and chapter='' and topic=''  ");
												}else if($key=='5'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and chapter!='' and topic=''  ");
												}else if($key=='6'){
													$dataetotalneet=$database->query("select count(id) from createquestion where   estatus='1' and FIND_IN_SET(".$rowlv['id'].",class) >0 and  FIND_IN_SET(".$rowll2['id'].",exam) >0 and subject='".$row3['id']."' and vstatus1='0' ");
												}
                                                $rowdataetotal=mysqli_fetch_array($dataetotalneet);
                                                $percentp = $rowdataetotal[0]/$rowdataetotalsub[0];
                                                
                                                if(is_nan($percentp))
                                                    $percentp=0;
                                                else
                                                    $percentp=$percentp;
                                                $percent_vp= number_format( $percentp * 100) . '%';
                                                ?>

                                                <div class="item-td"><h6><?php  if($rowdataetotal!=''){ echo $rowdataetotal[0]; } else{ echo '0'; } ?></h6><?php if($key!='0'){ ?><small class="text-primary pl-1"><?php echo $percent_vp; ?> </small><?php } ?></div>

                                                <?php
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                <?php	 }
                            } ?>

                            </tbody>
                        </table>
                        </div>
                        <?php
                        $i++;
                    }

                ?>
            </div>
        </div>
    </div>
<?php
}
?>