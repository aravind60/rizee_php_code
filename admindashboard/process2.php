<?php
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['timelineOverview'])){

    ?>
   
    <!-- <div class="card-header">
        <h5 class="m-0"><b>Timeline Overview</b></h5>
    </div> -->
	<div class="card-header" id="timelineview">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#timelinediv" aria-expanded="true"
		   aria-controls="timelinediv"  >
			<h5 class="card-title mb-0"><b>Timeline Overview</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <div class="collapse show" id="timelinediv"   aria-labelledby="timelineview" >
    <div class="d-flex justify-content-between border-bottom">
        <ul class="nav nav-pills timeline" id="pills-tabtime" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-home-tabtime" data-toggle="pill"
                   href="#pills-hometime" role="tab" aria-controls="pills-hometime"
                   aria-selected="true"><b>Data Entry</b></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tabtime" data-toggle="pill"
                   href="#pills-profiletime" role="tab" aria-controls="pills-profiletime"
                   aria-selected="false"><b>Verification</b></a>
            </li>

        </ul>


    </div>
    <div class="tab-content p-2" id="pills-tabContent">
    <?php
    $con="";

    if($_REQUEST['exam']!=''){
            $con=" AND id IN (".$_REQUEST['exam'].")";
        }

        ?>
        <div class="tab-pane fade show active" id="pills-hometime" role="tabpanel"
             aria-labelledby="pills-home-tabtime">
            <table class="table table-bordered">
                <thead class="bg-light">
                <tr>
                    <th rowspan="3"></th>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        if($_REQUEST['exam']=='1'){
                            $cols="4";
                        }else if($_REQUEST['exam']=='2'){
                            $cols="3";
                        }else if($_REQUEST['exam']=='3'){
                            $cols="5";
                        }else if($_REQUEST['exam']=='3'){
                            $cols="5";
                        }else if($_REQUEST['exam']=='1,2'){
                            $cols="7";
                        }else{
                            $cols="17";
                        }
                        ?>

                        <th colspan="<?php echo $cols; ?>">
                            <h6 class="text-dark p-2"><b>Class <?php echo $row['class']; ?></b></h6>
                        </th>


                    <?php } ?>
					<th rowspan="3"  class="text-center">Total</th>
                </tr>
                <tr>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        $sqle=$database->query("select * from exam where estatus='1'".$con."");
                        while($rowe=mysqli_fetch_array($sqle)){
                            if($rowe['id']=='1'){
                                $colspan="4";
                            }else if($rowe['id']=='2'){
                                $colspan="3";
                            }else if($rowe['id']=='3'){
                                $colspan="5";
                            }else if($rowe['id']=='4'){
                                $colspan="5";
                            }
                            ?>
                            <th colspan="<?php echo $colspan; ?>">
                                <h6 class="text-dark p-2"><?php echo $rowe['exam']; ?></h6>
                            </th>

                        <?php } }
                    ?>
                </tr>

                <tr>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        $sqle=$database->query("select * from exam where estatus='1'".$con."");
                        while($rowe=mysqli_fetch_array($sqle)){
                            if($rowe['id']=='1'){

                                $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                            }else if($rowe['id']=='2'){
                                $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                            }else  if($rowe['id']=='1,2'){
                                $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                            }else{
                                $data3 = $database->query("select * from subject where estatus=1");
                            }
                            while($row3=mysqli_fetch_array($data3)){ ?>


                                <th>
                                    <p><?php echo $row3['subject']; ?></p>
                                </th>
                            <?php } } } ?>
                </tr>


                </thead>
                <tbody>
                <?php
                $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
                $dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
                $dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
                $datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
                $date3days = strtotime(date('d-m-Y',strtotime("-3 days")));
                $date7days = strtotime(date('d-m-Y',strtotime("-7 days")));
                $date15days = strtotime(date('d-m-Y',strtotime("-15 days")));
                $date30days = strtotime(date('d-m-Y',strtotime("-30 days")));
                $date45days = strtotime(date('d-m-Y',strtotime("-45 days")));
                $date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
                $today=" and timestamp between ".$dateto." and ".$dateto1."";
                $yesterday=" and timestamp between ".$dateyes." and ".$datetoyes1."";
                $last3days=" and timestamp between ".$date3days." and ".$dateto1."";
                $last7days=" and timestamp between ".$date7days." and ".$dateto1."";
                $last15days=" and timestamp between ".$date15days." and ".$dateto1."";
                $last30days=" and timestamp between ".$date30days." and ".$dateto1."";
                $last45days=" and timestamp between ".$date45days." and ".$dateto1."";
                $overall=" and timestamp between ".$date364days." and ".$dateto1."";
				
                $daysrange = array($today =>'Today',$yesterday => 'Yesterday',$last3days=> 'Last 3 Days',$last7days => 'Last 7 Days',$last15days => 'Last 15 Days',$last30days => 'Last 30 Days',$last45days => 'Last 45 Days',$overall => 'Overall');
                foreach($daysrange as $key => $days){
                    $rgpArray[$index] = array('class'=>array('1'=>'XI', '2'=>'XII'),
                        'exam'=>array('1'=>'NEET', '2'=>'JEE'),
                        'subject'=>array('weight'=>$weight, 'height'=>$height));


                    ?>

                    <tr>
                        <th>
                            <h6><?php echo $days; ?></h6>
                        </th>
                        <?php
                        $j=1;
						 $data1 = $database->query("select * from class where estatus=1 ");
                        while($data1row=mysqli_fetch_array($data1)){
                            $data2 = $database->query("select * from exam where estatus=1".$con." ");
                            while($data2row=mysqli_fetch_array($data2)){
                                if($data2row['id']=='1'){

                                    $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                }else if($data2row['id']=='2'){
                                    $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                }else  if($data2row['id']=='1,2'){
                                    $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                }else{
                                    $data3 = $database->query("select * from subject where estatus=1");
                                }
                                while($row3=mysqli_fetch_array($data3)){

                                    $todata = $database->query("select count(*) from createquestion where estatus=1 and class LIKE '%".$data1row['id']."%' and exam LIKE '%".$data2row['id']."%' and subject='".$row3['id']."'".$key."");

                                    $todayttocount=mysqli_fetch_array($todata);


									 ?>
                                    <td>
                                        <p><?php  if($todayttocount[0]!=''){ echo $todayttocount[0]; } else{ echo '0'; } ?></p>
                                    </td>


                                    <?php $j++; } } } ?>
									<?php
									 $todata1 = $database->query("select count(*) from createquestion where estatus=1 ".$key."");

                                    $todayttocount1=mysqli_fetch_array($todata1);
									?>
										<td>
                                        <p><?php  echo $todayttocount1[0]; ?></p>
                                    </td>
                    </tr>
                    <?php

                }
                ?>


                </tbody>
            </table>
        </div>
        <div class="tab-pane fade" id="pills-profiletime" role="tabpanel"
             aria-labelledby="pills-profile-tabtime">
            <table class="table table-bordered">
                <thead class="bg-light verification-header">
                <tr>
                    <th colspan="3" rowspan="4"></th>

                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        if($_REQUEST['exam']=='1'){
                            $cospne="12";
                        }else if($_REQUEST['exam']=='2'){
                            $cospne="9";
                        }else if($_REQUEST['exam']=='1,2'){
                            $cospne="21";
                        }else if($_REQUEST['exam']=='3' || $_REQUEST['exam']=='4'){
                            $cospne="15";
                        }else{
                            $cospne="51";
                        }
                        ?>
                        <th colspan="<?php echo $cospne; ?>">
                            <h6 class="text-dark p-2"><b>Class<?php echo $row['class']; ?></b>
                            </h6>
                        </th>
                    <?php } ?>
					<th colspan="3" rowspan="3" class="text-center">Total</th>
					
                </tr>
                <tr>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        $sqle=$database->query("select * from exam where estatus='1'".$con."");
                        while($rowe=mysqli_fetch_array($sqle)){
                            if($rowe['id']=='1'){
                                $colspan="12";
                            }else if($rowe['id']=='2'){
                                $colspan="9";
                            }else if($rowe['id']=='3'){
                                $colspan="15";
                            }else if($rowe['id']=='4'){
                                $colspan="15";
                            }
                            ?>
                            <th colspan="<?php echo $colspan; ?>">
                                <h6 class="text-dark"> <?php echo $rowe['exam']; ?></h6>
                            </th>
                            <?php
                        }

                    }
                    ?>


                </tr>
                <tr>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        $sqle=$database->query("select * from exam where estatus='1'".$con."");
                        while($rowe=mysqli_fetch_array($sqle)){
                            if($rowe['id']=='1'){

                                $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                            }else if($rowe['id']=='2'){
                                $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                            }else  if($rowe['id']=='1,2'){
                                $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                            }else{
                                $data3 = $database->query("select * from subject where estatus=1");
                            }
                            while($row3=mysqli_fetch_array($data3)){ ?>
                                <th colspan="3">
                                    <h6 class="text-dark p-2"><?php echo $row3['subject']; ?></h6>
                                </th>
                                <?php
                            }
                        }
                    }
                    ?>

                </tr>
                <tr>
                    <?php
                    $sql=$database->query("select * from class where estatus='1'");
                    while($row=mysqli_fetch_array($sql)){
                        $sqle=$database->query("select * from exam where estatus='1'".$con."");
                        while($rowe=mysqli_fetch_array($sqle)){
                            if($rowe['id']=='1'){

                                $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                            }else if($rowe['id']=='2'){
                                $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                            }else  if($rowe['id']=='1,2'){
                                $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                            }else{
                                $data3 = $database->query("select * from subject where estatus=1");
                            }
                            while($row3=mysqli_fetch_array($data3)){
                                $status = array('1' =>'Verified','0' => 'Overall Pending','2' => 'Rejected');
                                foreach($status as $key => $stat){
                                    ?>
                                    <th>
                                        <p><?php echo  $stat; ?></p>
                                    </th>
                                    <?php
                                }
                            }
                        }
                    }
					 $status = array('1' =>'Verified','0' => 'Overall Pending','2' => 'Rejected');
						foreach($status as $key => $stat){
							?>
							<th>
								<p><?php echo  $stat; ?></p>
							</th>
							<?php
						}
                    ?>
					
                </tr>


                </thead>
                <tbody>
                <?php
                $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
                $dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
                $dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
                $datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
                $date3days = strtotime(date('d-m-Y',strtotime("-3 days")));
                $date7days = strtotime(date('d-m-Y',strtotime("-7 days")));
                $date15days = strtotime(date('d-m-Y',strtotime("-15 days")));
                $date30days = strtotime(date('d-m-Y',strtotime("-30 days")));
                $date45days = strtotime(date('d-m-Y',strtotime("-45 days")));
                $date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
                $today=" and vtimestamp1 between ".$dateto." and ".$dateto1."";
                $yesterday=" and timestamp between ".$dateyes." and ".$datetoyes1."";
                $last3days=" and timestamp between ".$date3days." and ".$dateto1."";
                $last7days=" and timestamp between ".$date7days." and ".$dateto1."";
                $last15days=" and timestamp between ".$date15days." and ".$dateto1."";
                $last30days=" and timestamp between ".$date30days." and ".$dateto1."";
                $last45days=" and timestamp between ".$date45days." and ".$dateto1."";
                $overall=" and timestamp between ".$date364days." and ".$dateto1."";
				
                $daysrange = array($today =>'Today',$yesterday => 'Yesterday',$last3days=> 'Last 3 Days',$last7days => 'Last 7 Days',$last15days => 'Last 15 Days',$last30days => 'Last 30 Days',$last45days => 'Last 45 Days',$overall => 'Overall');
                foreach($daysrange as $key => $days){


                    ?>

                    <tr>
                        <th colspan="3">
                            <h6><?php echo $days; ?></h6>
                        </th>
                        <?php
                        $j=1;
						
                        $data1 = $database->query("select * from class where estatus=1 ");
                        while($data1row=mysqli_fetch_array($data1)){
                            $data2 = $database->query("select * from exam where estatus=1".$con." ");
                            while($data2row=mysqli_fetch_array($data2)){
                                if($data2row['id']=='1'){

                                    $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                }else if($data2row['id']=='2'){
                                    $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                }else  if($data2row['id']=='1,2'){
                                    $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                }else{
                                    $data3 = $database->query("select * from subject where estatus=1");
                                }
                                while($row3=mysqli_fetch_array($data3)){
                                    $subtotal=$database->query("select count(*) from createquestion where   estatus='1' and class LIKE '%".$data1row['id']."%' and exam LIKE '%".$data2row['id']."%'  and subject='".$row3['id']."'".$key."");
                                    $rowsubtotaltot=mysqli_fetch_array($subtotal);
                                    $status = array('1' =>'Verified','0' => 'Overall Pending','2' => 'Rejected');
                                    foreach($status as $keystat => $stat){
										if($keystat=='2'){
											$dataveripen=$database->query("select count(*) AS count from createquestion where   estatus='2' and class LIKE '%".$data1row['id']."%' and exam LIKE '%".$data2row['id']."%'  and subject='".$row3['id']."'  and vstatus1='".$keystat."'".$key."");
										}else{
											$dataveripen=$database->query("select count(*) AS count from createquestion where   estatus='1' and class LIKE '%".$data1row['id']."%' and exam LIKE '%".$data2row['id']."%'  and subject='".$row3['id']."'  and vstatus1='".$keystat."'".$key."");
										}
                                        $rowveripentot=mysqli_fetch_array($dataveripen);

                                        $psubtotp1 = $rowveripentot[0]/$rowsubtotaltot[0];
                                        if(is_nan($psubtotp1))
                                            $psubtotp1=0;
                                        else
                                            $psubtotp1=$psubtotp1;
                                        $psubtotp1_vp1=number_format($psubtotp1 * 100,1).'%';
                                        if($keystat=='0'){
                                            $class='class="bg-light"';
                                        }else{
                                            $class='';
                                        }
										
                                        ?>

                                        <td <?php echo $class; ?>>
                                            <p><?php echo $rowveripentot[0]; ?><small class="text-primary pl-2"><?php if($keystat!='1'){ echo $psubtotp1_vp1; } ?></small></p>
                                        </td>
										
                                        <?php
                                    }
									

                                    ?>
									


                                    <?php $j++; } } } ?>
									<?php
										$todatap = $database->query("select count(*) from createquestion where estatus=1 and vstatus1='0' ".$key."");
										$todayttocountp=mysqli_fetch_array($todatap);
										$todatav = $database->query("select count(*) from createquestion where estatus=1 and vstatus1='1' ".$key."");
										$todayttocountv=mysqli_fetch_array($todatav);
										$todatar = $database->query("select count(*) from createquestion where estatus=2 and vstatus1='2' ".$key."");
										$todayttocountr=mysqli_fetch_array($todatar);
										
									?>
									<td>
                                        <p><?php  echo $todayttocountv[0]; ?></p>
                                    </td>
									<td>
                                        <p><?php  echo $todayttocountp[0]; ?></p>
                                    </td>
									<td>
                                        <p><?php  echo $todayttocountr[0]; ?></p>
                                    </td>
                    </tr>
                    <?php

                }
                ?>


                </tbody>
            </table>
        </div>
        </div>
        </div>
    <?php
    ?>
<?php
}
?>