

<?php
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['attributesOverveiw'])){
    ?>
	<div class="card-header" id="attributeoverview">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#attributediv" aria-expanded="true"
		   aria-controls="attributediv"  >
			<h5 class="card-title mb-0"><b>Attribute Overview</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <div class="collapse show" id="attributediv"  aria-labelledby="attributeoverview" >
        <div class="border-bottom">
            <ul class="nav nav-pills attribute" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
                       role="tab" aria-controls="pills-home" aria-selected="true"><b>Complex Wise</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
                       role="tab" aria-controls="pills-profile" aria-selected="false"><b>Type Of
                            Question</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
                       role="tab" aria-controls="pills-contact" aria-selected="false"><b>Time Wise
                            Questions</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-usage"
                       role="tab" aria-controls="pills-usage" aria-selected="false"><b>Usage Set</b></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-theory"
                       role="tab" aria-controls="pills-theory" aria-selected="false"><b>Question Theory</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-category"
                       role="tab" aria-controls="pills-category" aria-selected="false"><b>Question Category</b></a>
                </li>
            </ul>
        </div>
        <div class="tab-content p-2" id="pills-tabContent">
            <?php
            $con="";
            $con2="";
                if($_REQUEST['exam']!=''){
                    $con=" AND id IN (".$_REQUEST['exam'].")";
                    $con2=" AND exam_id IN (".$_REQUEST['exam'].")";

                }
               

                if($_REQUEST['exam']=='1'){
                    $cols="5";
                }else if($_REQUEST['exam']=='2'){
                    $cols="4";
                }else if($_REQUEST['exam']=='3' ||  $_REQUEST['exam']=='4'){
                    $cols="6";
                }else if($_REQUEST['exam']=='1,2'){
                    $cols="21";
                }else{
                    $cols="21";
                }
                ?>
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                     aria-labelledby="pills-home-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3"></th>
                            <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){

                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>Class <?php echo $data1row['class']; ?></b></h6>
                                </th>
                            <?php } ?>

                        </tr>
                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3' || $rowe['id']=='4' ){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <h6 class="text-dark p-2"> <?php echo $rowe['exam']; ?></h6>
                                    </th>
                                    <?php
                                }

                            }
                            ?>
                        </tr>

                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><?php echo $row3['subject']; ?></p>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     <th>
                                            <p>Total</p>

                                        </th>
                                <?php }
                            }
                            ?>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from complexity where estatus='1'");
                        while($rowll2=mysqli_fetch_array($sell2)){
							$todata_com = $database->query("select count(id) as cnt from createquestion where estatus=1 and vstatus1='1' and  complexity='".$rowll2['id']."' ");
							 $quecountcom=mysqli_fetch_array($todata_com);
                            ?>

                            <tr>
                                <th nowrap>
                                    <h6><?php echo $rowll2['complexity']; ?> (<?php echo  $quecountcom['cnt']; ?>)</h6>
                                </th>
                                <?php
                                $j=1;
                                $data1 = $database->query("select * from class where estatus=1 ");
                                while($data1row=mysqli_fetch_array($data1)){

                                    $data2 = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2row=mysqli_fetch_array($data2)){
                                        $htotal=0;

                                        if($data2row['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2row['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3=mysqli_fetch_array($data3)){
											
                                            $todata = $database->query("select count(id) from createquestion where estatus=1  and vstatus1='1' and  find_in_set(".$data1row['id'].",class) >0 and  find_in_set(".$data2row['id'].",exam) >0 and   subject='".$row3['id']."' and complexity='".$rowll2['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($complexcount[0]!=''){ echo $complexcount[0]; } else{ echo '0'; } 
                                                   $htotal= $htotal+$complexcount[0]; 
                                                    $total+=$complexcount[0];

                                                ?></p>
                                            </td>
   

                                            <?php $j++; }
                                           
                                            ?>
                                            <td><?php echo $htotal; ?></td>
                                        <?php //$total2 =$total2+$htotal; 
                                    }  } ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                           
                            $data1v = $database->query("select * from class where estatus=1 ");
                                while($data1rowv=mysqli_fetch_array($data1v)){
                                
                               $data2v = $database->query("select * from exam where estatus=1".$con." ");

                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                       $total2=0;  
                                        if($data2rowv['id']=='1'){

                                            $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2rowv['id']=='2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3v = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3v=mysqli_fetch_array($data3v)){
                                             $total1=0;
                                            $vtotal=0;
                                         $sell2v=$database->query("select * from complexity where estatus='1'");
										 while($rowll2v=mysqli_fetch_array($sell2v)){
											 $todatav = $database->query("select count(id) from createquestion where estatus=1 and vstatus1='1' and find_in_set(".$data1rowv['id'].",class) >0 and  find_in_set(".$data2rowv['id'].",exam) >0  and subject='".$row3v['id']."' and complexity='".$rowll2v['id']."'");

                                            $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
										}
									$total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td>
                                       <?php }  ?>
                                    <td><?php echo $total2; ?></td>
                               <?php 
                                    } }
                            ?>

                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3"></th>
                            <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){
                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>Class <?php echo $data1row['class']; ?></b></h6>
                                </th>
                            <?php } ?>

                        </tr>
                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3'){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <h6 class="text-dark p-2"> <?php echo $rowe['exam']; ?></h6>
                                    </th>
                                    <?php
                                }

                            }
                            ?>
                        </tr>

                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><?php echo $row3['subject']; ?></p>
                                        </th>
                                        <?php
                                    }?>
                                     <th>
                                            <p>Total</p>

                                        </th>
                               <?php }
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $sell3=$database->query("select * from questiontype where estatus='1'");
                        while($rowll3=mysqli_fetch_array($sell3)){
							$todata_q = $database->query("select count(*) as cnt from createquestion where estatus=1 and vstatus1='1' and  find_in_set(".$rowll3['id'].",inputquestion) >0 ");
							 $quecountq=mysqli_fetch_array($todata_q);
                            ?>

                            <tr>
                                <th nowrap>
                                    <p style="font:size:20px;font-weight:bold"><?php echo $rowll3['questiontype']; ?> (<?php echo  $quecountq['cnt']; ?>) </p>
                                </th>
                                <?php
                                $j=1;
                                $data1 = $database->query("select * from class where estatus=1 ");
                                while($data1row=mysqli_fetch_array($data1)){
                                    $data2 = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2row=mysqli_fetch_array($data2)){
                                         $htotal=0;
                                        if($data2row['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2row['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3=mysqli_fetch_array($data3)){

                                            $todata = $database->query("select count(*) from createquestion where estatus=1 and vstatus1='1' and  find_in_set(".$data1row['id'].",class) >0 and find_in_set(".$data2row['id'].",exam) >0  and subject='".$row3['id']."'  and find_in_set(".$rowll3['id'].",inputquestion) >0 ");

                                            $quecount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($quecount[0]!=''){ echo $quecount[0]; } else{ echo '0'; } 
                                                    $htotal= $htotal+$quecount[0];
                                                ?></p>
                                            </td>


                                            <?php $j++; } ?>
                                         <td><?php echo $htotal; ?></td>
                                            <?php } } ?>
                            </tr>
                            <?php

                        }

                        ?>
                         <tr><th>
                                    <h6>Total</h6>
                                </th>

                               <?php $data1v = $database->query("select * from class where estatus=1 ");
                                while($data1rowv=mysqli_fetch_array($data1v)){
                                    $data2v = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                         $total2=0;
                                        if($data2rowv['id']=='1'){

                                            $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2rowv['id']=='2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2rowv['id']=='1,2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3v = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                              $sell3=$database->query("select * from questiontype where estatus='1'");
                        while($rowll3v=mysqli_fetch_array($sell3)){
                          $todatav = $database->query("select count(*) from createquestion where estatus=1  and vstatus1='1' and find_in_set(".$data1rowv['id'].",class) >0  and find_in_set(".$data2rowv['id'].",exam) >0  and subject='".$row3v['id']."'  and find_in_set(".$rowll3['id'].",inputquestion) >0");

                        $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
                        }
                        $total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td>
                                       <?php }  ?>
                                    <td><?php echo $total2; ?></td>
                               <?php 
                                    } }
                            ?>
                              </tr>  
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-contact" role="tabpanel"
                     aria-labelledby="pills-contact-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3"></th>
                            <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){
                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>Class <?php echo $data1row['class']; ?></b></h6>
                                </th>
                            <?php } ?>

                        </tr>
                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3'){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <h6 class="text-dark p-2"> <?php echo $rowe['exam']; ?></h6>
                                    </th>
                                    <?php
                                }

                            }
                            ?>
                        </tr>

                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><?php echo $row3['subject']; ?></p>
                                        </th>
                                        <?php
                                    }
                                    ?>
                                     <th>
                                            <p>Total</p>

                                        </th>
                               <?php
                                }
                            }
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $zero=" and timeduration < 10 and timeduration!=''";
                        $ten=" and timeduration between 10 and 20 and timeduration!=''";
                        $twenty=" and timeduration between 20 and 30 and timeduration!=''";
                        $thirty=" and timeduration between 30 and 40 and timeduration!=''";
                        $fourty=" and timeduration between 40 and 50 and timeduration!=''";
                        $fifty=" and timeduration between 50 and 60 and timeduration!=''";
                        $oneminute=" and timeduration between 60 and 120 and timeduration!=''";
                        $twominute=" and timeduration between 120 and 180 and timeduration!=''";
                        $threeminute=" and timeduration between 180 and 240 and timeduration!=''";
                        $fourminute=" and timeduration between 240 and 300 and timeduration!=''";
                        $fiveminute=" and timeduration between 300 and 360 and timeduration!=''";

                        $timerange = array($zero =>'0 To  10 Seconds',$ten => '10 To  20 Seconds',$twenty=> '20 To  30 Seconds',$thirty => '30 To  40 Seconds',$fourty => '40 To  50 Seconds',$fifty => '50 To  60 Seconds',$oneminute => '1 To 2 minutes',$twominute => '2 To 3 minutes',$threeminute => '3 To 4 minutes',$fourminute =>'4 To 5 minutes');

                        foreach($timerange as $timekey => $time){
							$datatime=$database->query("select count(id) as cnt from createquestion where   estatus='1' and vstatus1='1' ".$timekey."");
                             $rowtime=mysqli_fetch_array($datatime);
                            ?>

                            <tr>
                                <th nowrap>
                                    <h6><?php echo $time; ?> (<?php echo $rowtime['cnt']; ?>)</h6>
                                </th>
                                <?php
                                $j=1;
                                $data1 = $database->query("select * from class where estatus=1 ");
                                while($data1row=mysqli_fetch_array($data1)){
                                    $data2 = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2row=mysqli_fetch_array($data2)){
                                         $htotal=0;
                                        if($data2row['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4' ");
                                        }else if($data2row['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3=mysqli_fetch_array($data3)){


                                            $dataveripen=$database->query("select count(*) from createquestion where   estatus='1' and vstatus1='1' and class LIKE '%".$data1row['id']."%' and exam LIKE '%".$data2row['id']."%'  and subject='".$row3['id']."'".$timekey."");
                                            
                                            $rowveripentot=mysqli_fetch_array($dataveripen);
                                            ?>

                                            <td>
                                                <p><?php echo $rowveripentot[0]; ?></p>
                                            </td>

                                            <?php
                                             $htotal= $htotal+$rowveripentot[0];
                                        }


                                        ?>
                                         <td><?php echo $htotal; ?></td>



                                    <?php  } }  ?>
                            </tr>
                            <?php

                        }
                        ?>
                         <tr><th>
                                    <h6>Total</h6>
                                </th>
                               <?php $data1v = $database->query("select * from class where estatus=1 ");
                                while($data1rowv=mysqli_fetch_array($data1v)){
                                    $data2v = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                         $total2=0;
                                        if($data2rowv['id']=='1'){

                                            $data3v = $database->query("select * from subject where estatus=1 and id!='4' ");
                                        }else if($data2rowv['id']=='2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2rowv['id']=='1,2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3v = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                            $zero1=" and timeduration < 10 and timeduration!=''";
                        $ten1=" and timeduration between 10 and 20 and timeduration!=''";
                        $twenty1=" and timeduration between 20 and 30 and timeduration!=''";
                        $thirty1=" and timeduration between 30 and 40 and timeduration!=''";
                        $fourty1=" and timeduration between 40 and 50 and timeduration!=''";
                        $fifty1=" and timeduration between 50 and 60 and timeduration!=''";
                        $oneminute1=" and timeduration between 60 and 120 and timeduration!=''";
                        $twominute1=" and timeduration between 120 and 180 and timeduration!=''";
                        $threeminute1=" and timeduration between 180 and 240 and timeduration!=''";
                        $fourminute1=" and timeduration between 240 and 300 and timeduration!=''";
                        $fiveminute1=" and timeduration between 300 and 360 and timeduration!=''";
                        // $timerange1 = array($zero1 =>'0 To  10 Seconds',$ten1 => '10 To  20 Seconds',$twenty1=> '20 To  30 Seconds',$thirty1 => '30 To  40 Seconds',$fourty1 => '40 To  50 Seconds',$fifty1 => '50 To  60 Seconds',$sixty1 => '> 60 Seconds');
                        $timerange1 = array($zero1 =>'0 To  10 Seconds',$ten1 => '10 To  20 Seconds',$twenty1=> '20 To  30 Seconds',$thirty1 => '30 To  40 Seconds',$fourty1 => '40 To  50 Seconds',$fifty1 => '50 To  60 Seconds',$oneminute1 => '1 To 2 minutes',$twominute1 => '2 To 3 minutes',$threeminute1 => '3 To 4 minutes',$fourminute1 =>'4 To 5 minutes');
                        foreach($timerange1 as $timekey1 => $time1){
                          $dataveripen=$database->query("select count(*) from createquestion where   estatus='1' and vstatus1='1' and class LIKE '%".$data1rowv['id']."%' and exam LIKE '%".$data2rowv['id']."%'  and subject='".$row3v['id']."'".$timekey1."");
                       $complexcountv=mysqli_fetch_array($dataveripen);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
                        }
                        $total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td>
                                       <?php }  ?>
                                    <td><?php echo $total2; ?></td>
                               <?php 
                                    } }
                            ?>
                         </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-usage" role="tabpanel"
                     aria-labelledby="pills-home-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3"></th>
                            <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){

                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>Class <?php echo $data1row['class']; ?></b></h6>
                                </th>
                            <?php } ?>

                        </tr>
                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3'){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <h6 class="text-dark p-2"> <?php echo $rowe['exam']; ?></h6>
                                    </th>
                                    <?php
                                }

                            }
                            ?>
                        </tr>

                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><?php echo $row3['subject']; ?></p>
                                        </th>
                                        <?php
                                    }?>
                                     <th>
                                            <p>Total</p>

                                        </th>
                               <?php
                                }
                            }
                            ?>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $sell2=$database->query("select * from question_useset where estatus='1'");
                        while($rowll2=mysqli_fetch_array($sell2)){
							$todata_uset = $database->query("select count(id) as cnt from createquestion where estatus=1 and vstatus1='1' and  usageset='".$rowll2['id']."' ");
							 $quecountuset=mysqli_fetch_array($todata_uset);
                            ?>

                            <tr>
                                <th nowrap>
                                    <h6><?php echo $rowll2['usageset']; ?> (<?php echo  $quecountuset['cnt']; ?>)</h6>
                                </th>
                                <?php
                                $j=1;
                                $data1 = $database->query("select * from class where estatus=1 ");
                                while($data1row=mysqli_fetch_array($data1)){
                                    $data2 = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2row=mysqli_fetch_array($data2)){
                                        $htotal=0;
                                        if($data2row['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2row['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3=mysqli_fetch_array($data3)){

                                            $todata = $database->query("select count(id) from createquestion where estatus=1 and vstatus1='1' and  find_in_set(".$data1row['id'].",class)>0  and find_in_set(".$data2row['id'].",exam) >0 and subject='".$row3['id']."' and usageset='".$rowll2['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($complexcount[0]!=''){ echo $complexcount[0]; } else{ echo '0'; } $htotal= $htotal+$complexcount[0];?></p>
                                            </td>


                                            <?php $j++; } 
                                        ?>
                                          <td><?php echo $htotal;?></td>
                                            <?php
                                        } } ?>
                            </tr>
                            <?php

                        }
                        ?>
                       <tr><th>
                                    <h6>Total</h6>
                                </th>
                        <?php $data1v = $database->query("select * from class where estatus=1 ");
                                while($data1rowv=mysqli_fetch_array($data1v)){
                                    $data2v = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                        $total2=0;
                                        if($data2rowv['id']=='1'){

                                            $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2rowv['id']=='2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2rowv['id']=='1,2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3v = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3v=mysqli_fetch_array($data3v)){ 
                                            $vtotal=0;
                                        $sell2v=$database->query("select * from question_useset where estatus='1'");
                                        while($rowll2v=mysqli_fetch_array($sell2v)){

                                            $todatav = $database->query("select count(id) from createquestion where estatus=1 and vstatus1='1' and  find_in_set(".$data1rowv['id'].",class)>0  and find_in_set(".$data2rowv['id'].",exam) >0  and subject='".$row3v['id']."' and usageset='".$rowll2v['id']."'");
                       $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
                        }
                        $total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td>
                                       <?php }  ?>
                                    <td><?php echo $total2; ?></td>
                               <?php 
                                    } }
                            ?>
                         </tr>

                        </tbody>
                    </table>
                </div>
				<div class="tab-pane fade" id="pills-theory" role="tabpanel"
                     aria-labelledby="pills-home-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        <tr>
                            <th rowspan="3"></th>
                            <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){

                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>Class<?php echo $data1row['class']; ?></b></h6>
                                </th>
                            <?php } ?>

                        </tr>
                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3'){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <h6 class="text-dark p-2"> <?php echo $rowe['exam']; ?></h6>
                                    </th>
                                    <?php
                                }

                            }
                            ?>
                        </tr>

                        <tr>
                            <?php
                            $sql=$database->query("select * from class where estatus='1'");
                            while($row=mysqli_fetch_array($sql)){
                                $sqle=$database->query("select * from exam where estatus='1'".$con."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><?php echo $row3['subject']; ?></p>
                                        </th>
                                        <?php
                                    }
                                    ?>
                                    <th>
                                            <p>Total</p>

                                        </th>
                                <?php }
                            }
                            ?>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                        $sell2=$database->query("select * from question_theory where estatus='1'");
                        while($rowll2=mysqli_fetch_array($sell2)){
							$todata_qt = $database->query("select count(id) as cnt from createquestion where estatus=1 and vstatus1='1' and  question_theory='".$rowll2['id']."' ");
							 $quecountqt=mysqli_fetch_array($todata_qt);
                            ?>

                            <tr>
                                <th nowrap>
                                    <h6><?php echo $rowll2['question_theory']; ?> (<?php echo  $quecountqt['cnt']; ?>)</h6>
                                </th>
                                <?php
                                $j=1;
                                $data1 = $database->query("select * from class where estatus=1 ");
                                while($data1row=mysqli_fetch_array($data1)){
                                    $data2 = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2row=mysqli_fetch_array($data2)){
                                        $htotal=0;
                                        if($data2row['id']=='1'){

                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2row['id']=='2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2row['id']=='1,2'){
                                            $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3 = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3=mysqli_fetch_array($data3)){

                                            $todata = $database->query("select count(*) from createquestion where estatus=1 and vstatus1='1' and find_in_set(".$data1row['id'].",class) >0 and find_in_set(".$data2row['id'].",exam)>0  and subject='".$row3['id']."' and question_theory='".$rowll2['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($complexcount[0]!=''){ echo $complexcount[0]; } else{ echo '0'; } ?></p>
                                            </td>


                                            <?php 
                                              $htotal= $htotal+$complexcount[0];
                                            $j++; }

                                            ?>
                                         <td><?php echo $htotal; ?></td>
                                            <?php
                                             } } ?>
                            </tr>
                            <?php

                        }
                        ?>
                             <tr><th>
                                    <h6>Total</h6>
                                </th>

                        <?php $data1v = $database->query("select * from class where estatus=1 ");
                                while($data1rowv=mysqli_fetch_array($data1v)){
                                    $data2v = $database->query("select * from exam where estatus=1".$con." ");
                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                         $total2=0;
                                        if($data2rowv['id']=='1'){

                                            $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                        }else if($data2rowv['id']=='2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        }else  if($data2rowv['id']=='1,2'){
                                            $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                        }else{
                                            $data3v = $database->query("select * from subject where estatus=1");
                                        }
                                        while($row3v=mysqli_fetch_array($data3v)){
                                             $vtotal=0;
                                         $sell2v=$database->query("select * from question_theory where estatus='1'");
                                    while($rowll2v=mysqli_fetch_array($sell2v)){
                                            $todatav = $database->query("select count(*) from createquestion where estatus=1 and vstatus1='1' and find_in_set(".$data1rowv['id'].",class)>0  and find_in_set(".$data2rowv['id'].",exam)>0  and subject='".$row3v['id']."' and question_theory='".$rowll2v['id']."'");
                                       $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
                                                }
                                                $total2=$total2+$vtotal;
                                                ?>
                                                <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td>
                                       <?php }  ?>
                                    <td><?php echo $total2; ?></td>
                               <?php 
                                    } }
                            ?>


                        </tbody>
                    </table>
                </div>
            <div class="tab-pane fade" id="pills-category" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    <table class="table table-bordered" id="aaas">
                        <thead class="bg-light">
                       <!--  <tr> -->
                            <th rowspan="3" class="text-center">
                                <div class="form-group mb-1 text-center btn btn-light p-0">
 

  
    <select  name="class1" id="class1" class="p-2 border-0 bg-transparent" onchange="setState('pills-category','<?php echo SECURE_PATH;?>admindashboard/ajax.php','&getclass=1&class='+$(this).val()+'&exam=<?php echo $_REQUEST['exam'];?>');"><option value=''>-- Select --</option>
                                                
                                                <?php
                                                $row = $database->query("select * from class where estatus='1'");
                                                while($data = mysqli_fetch_array($row))
                                                {
                                                    ?>
                                                <option class="dropdown-item" type="button" value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class1'])) { if($_REQUEST['class1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
                                                <?php
                                                }
                                                ?>
                                           
    </select>
  </div>
</th>


                            <!-- <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){
                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>ClassXI</b></h6>
                                </th>
                            <?php } ?> -->

                        <!-- </tr> -->
                        <tr>
                            <?php

                                $sqle=$database->query("select * from pexam_types where estatus='1'".$con2."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['id']=='1'){
                                        $colspan="5";
                                    }else  if($rowe['id']=='3'){
                                        $colspan="6";
                                    }else if($rowe['id']=='2'){
                                        $colspan="4";
                                    }
                                    ?>
                                    <th colspan="<?php echo $colspan; ?>">
                                        <?php $exam_name1  = $database->query("select * from exam where estatus=1 and id='".$rowe['exam_id']."'"); 
                                          $exam_name2 = mysqli_fetch_array($exam_name1); 
                                          $exam_name = $exam_name2['exam'];
                                          //echo $exam_name;
                                        ?>
                                        <h6 class="text-dark p-2"> 
                                            <?php if($rowe['exam_id'] == '2'){
                                                if($rowe['pexam'] == '1'){
                                                    echo $exam_name.' Mains';
                                                }else{
                                                    echo $exam_name.' Advance';
                                                }
                                            }else{
                                                echo $exam_name;
                                            }?>
                                            </h6>

                                    </th>

                                    <?php
                                }

                            
                            ?>
                            
                        </tr>

                        <tr>
                            <?php
                            
                                $sqle=$database->query("select * from pexam_types where estatus='1'".$con2."");
                                while($rowe=mysqli_fetch_array($sqle)){
                                    if($rowe['exam_id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($rowe['exam_id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($rowe['exam_id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }

                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th>
                                            <p><!-- <a  title="Print" target='_blank' onclick='window.open("<?php echo SECURE_PATH;?>admindashboard/process6.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row3['id'];?>&exam=<?php echo $rowe['exam_id'];?>&pexam=<?php echo $rowe['pexam'];?>");'><?php echo $row3['subject'];?></a> -->
                                            <a  title="Print" target='_blank' onclick='window.open("<?php echo SECURE_PATH;?>admindashboard/process6.php?viewDetails2=1&class1=<?php echo $_REQUEST['class'];?>&subject1=<?php echo $row3['id'];?>&exam=<?php echo $rowe['exam_id'];?>&pexam=<?php echo $rowe['pexam'];?>");'><?php echo $row3['subject'];?></a>
                                            </p>
                                        </th>
                                        <?php
                                    }
                                    
                                    ?>

                                     <th>
                                            <p>Total</p>

                                        </th>
                               <?php }
                            
                            ?>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $class="";

                        $sell3=$database->query("select * from questiontype where  estatus='1' and id in(7,5,8,3,9)");
                        while($rowll3=mysqli_fetch_array($sell3)){
							$todata_qtype = $database->query("select count(id) as cnt from createquestion where estatus=1  and  qtype='".$rowll3['id']."' ");
							 $quecountqtype=mysqli_fetch_array($todata_qtype);
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll3['questiontype']; ?> (<?php echo  $quecountqtype['cnt']; ?>)</h6>
                                </th>
                                <?php
                                $j=1;
                               
                                    $data2 = $database->query("select * from pexam_types where estatus='1'".$con2."");
                                    while($data2row=mysqli_fetch_array($data2)){
                                         $htotal=0;
                                        if($data2row['exam_id']=='1'){

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($data2row['exam_id']=='2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($data2row['exam_id']=='1,2'){
                                        $data3 = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3 = $database->query("select * from subject where estatus=1");
                                    }
                                        while($row3=mysqli_fetch_array($data3)){
                                              
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and find_in_set(".$data2row['exam_id'].",exam) >0  and pexamtype in (".$data2row['pexam'].") and subject='".$row3['id']."' and qtype='".$rowll3['id']."' ".$class."");
                                            $quecount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($quecount[0]!=''){ echo $quecount[0]; } else{ echo '0'; } 
                                                    $htotal= $htotal+$quecount[0];
                                                ?></p>
                                            </td>


                                            <?php $j++; } ?>
                                         <td><?php echo $htotal; ?></td>
                                            <?php }  ?>
                            </tr>
                            <?php

                        }

                        ?>
                         <tr><th>
                                    <h6>Total</h6>
                                </th>

                               <?php 
                                    $data2v = $database->query("select * from pexam_types where estatus='1'".$con2."");
                                    while($data2rowv=mysqli_fetch_array($data2v)){
                                         $total2=0;
                                        if($data2rowv['exam_id']=='1'){

                                        $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                    }else if($data2rowv['exam_id']=='2'){
                                        $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    }else  if($data2rowv['exam_id']=='1,2'){
                                        $data3v = $database->query("select * from subject where estatus=1 and  id in (2,3)");
                                    }else{
                                        $data3v = $database->query("select * from subject where estatus=1");
                                    }
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                              $sell3v=$database->query("select * from questiontype where estatus='1' and id in(7,5,8,3,9)");
                        while($rowll3v=mysqli_fetch_array($sell3v)){
                            $todatav = $database->query("select count(*) from createquestion where estatus=1  and find_in_set(".$data2rowv['exam_id'].",exam) >0  and pexamtype in (".$data2rowv['pexam'].") and subject='".$row3v['id']."' and qtype='".$rowll3v['id']."' ".$class."");
                                        
                        $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                               $total1+=$vtotal;
                                              
                        }
                        $total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <?php echo $vtotal; 
                                                    
                                                ?>
                                            </td> 
                                       <?php }  ?>
                                       <td><?php echo $total2; ?></td>
                               <?php 
                                    } 
                            ?>
                              </tr>  
                        </tbody>
                    </table>
                </div>
            <?php     ?>
        </div>
    </div>
<?php
}
?>