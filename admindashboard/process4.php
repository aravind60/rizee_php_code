<?php 
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['topicOverview'])){
    ?>
    <div class="card-header" id="subjectoverview">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#subjectchapdiv" aria-expanded="true"
		   aria-controls="subjectchapdiv"  >
			<h5 class="card-title mb-0"><b>Subject ,Chapter & Topic wise Overview</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <div class="collapse show" id="subjectchapdiv"  aria-labelledby="subjectoverview" >
        <?php

    $con="";
            if($_REQUEST['exam']!=''){
                $con=" AND id IN (".$_REQUEST['exam'].")";
            }

            if($_REQUEST['exam']=='1'){
                ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php 
									   	$sqlne1=$database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and  FIND_IN_SET(1,exam) > 0 order by class asc limit 0,1");
															
										$rowsqlne1=mysqli_fetch_array($sqlne1);
										$neetcountt1=$rowsqlne1['total'];

									   if($neetcountt1!=''){ echo $neetcountt1; } else { echo '0'; }
									  ?>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="neetxihome-tab" data-toggle="tab"
                                                 href="#neetxihome" role="tab" aria-controls="neetxihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php
													 $sqlneet= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 order by exam asc limit 0,1" );
                                                               $rowsqlneet=mysqli_fetch_array($sqlneet);
                                                                 $sqlneetcountt= $rowsqlneet['total'];
																  if($sqlneetcountt!=''){ echo $sqlneetcountt; } else { echo '0'; }
													  ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php 
														
														  $sqlb1neet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and subject='1' order by subject asc limit 0,1");
                                                               $rowb1neet=mysqli_fetch_array($sqlb1neet);
                                                                $b1neetcountt= $rowb1neet['total'];
																 if($b1neetcountt!=''){ echo $b1neetcountt; } else { echo '0'; }
														 ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php 
															$sqlpneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and subject='2' order by subject asc limit 0,1");
                                                        $rowpneet=mysqli_fetch_array($sqlpneet);
                                                        $pneetcountt= $rowpneet['total'];
														 if($pneetcountt!=''){ echo $pneetcountt; } else { echo '0'; }
															 ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																$sqlcneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and subject='3' order by subject asc limit 0,1");
																$rowcneet=mysqli_fetch_array($sqlcneet);
																$cneetcountt= $rowcneet['total'];
																  if($cneetcountt!=''){ echo $cneetcountt; } else { echo '0'; } 
																 ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php 
																	 $sqlzneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and subject='5' order by subject asc limit 0,1");
																	$rowzneet=mysqli_fetch_array($sqlzneet);
																	$zneetcountt= $rowzneet['total'];
																	 if($zneetcountt!=''){ echo $zneetcountt; } else { echo '0'; } 
																	 ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <p class="text-muted"><?php 
									   	$sqlneet2=$database->query("SELECT exam_count as total FROM syllabus_totals_exam where  FIND_IN_SET(2,class) > 0  and  FIND_IN_SET(1,exam) > 0 order by class asc limit 0,1");
															
										$rowneet2=mysqli_fetch_array($sqlneet2);
										$neetcountt2=$rowneet2['total'];

									   	
										if($neetcountt2!=''){ echo $neetcountt2; } else { echo '0'; }
									   //echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="jeexihome-tab" data-toggle="tab"
                                                 href="#jeexihome" role="tab" aria-controls="jeexihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php 
													 				 $sqlneetc2= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0  order by subject asc limit 0,1");
                                                               $rowsqlneetc2=mysqli_fetch_array( $sqlneetc2);
                                                                 $sqlneetcounttc2= $rowsqlneetc2['total'];
																if($sqlneetcounttc2!=''){ echo $sqlneetcounttc2; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>


                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php 
															 $sqlneetc3= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='1' order by subject asc limit 0,1");
                                                               $rowsqlneetc3=mysqli_fetch_array( $sqlneetc3);
                                                                 $sqlneetcounttc3= $rowsqlneetc3['total'];
																if($sqlneetcounttc3!=''){ echo $sqlneetcounttc3; } else { echo '0'; }
															//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','1','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php 
															$sqlneetc4= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='2' order by subject asc limit 0,1");
                                                               $rowsqlneetc4=mysqli_fetch_array( $sqlneetc4);
                                                                 $sqlneetcounttc4= $rowsqlneetc4['total'];
																if($sqlneetcounttc4!=''){ echo $sqlneetcounttc4; } else { echo '0'; }
															//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																 $sqlneetc5= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='3' order by subject asc limit 0,1");
                                                               $rowsqlneetc5=mysqli_fetch_array( $sqlneetc5);
                                                                 $sqlneetcounttc5= $rowsqlneetc5['total'];
																if($sqlneetcounttc5!=''){ echo $sqlneetcounttc5; } else { echo '0'; }
																//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php 
																	 $sqlneetc6= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='5' order by subject asc limit 0,1");
                                                               $rowsqlneetc6=mysqli_fetch_array( $sqlneetc6);
                                                                 $sqlneetcounttc6= $rowsqlneetc6['total'];
																if($sqlneetcounttc6!=''){ echo $sqlneetcounttc6; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>
                        <?php
                        $subjects = array('1' =>'Botony','5' => 'Zoology','2'=> 'Physics','3' => 'Chemistry');
                        ?>
                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="neetxihome" role="tabpanel"
                                         aria-labelledby="neetxihome-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <div class="col-lg-3 col-md-3 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php 
                                                                        $datatn = $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and subject='".$key."' order by subject asc limit 0,1"); 
                                                        
                                                                        $rowdatatn=mysqli_fetch_array($datatn);
                                                                        $counttn= $rowdatatn['total'];
                                                                        if($counttn!=''){ echo $counttn; } else{ echo '0'; }
                                                                    //echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','1','estatus','1','','','','',''); ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject-<?php echo $key;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php
                                                                            
                                                                            $datat = $database->query("SELECT chapter_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and  subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."' order by chapter asc limit 0,1 ");
                                                                        
                                                                        
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                            
                                                                        ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php 
                                                                                        $topic1=$rowchapter['class']."_1_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter['class']."_2_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            
                                                                                            
                                                                                            $datat1 = $database->query("SELECT sum(total) as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(1,class) > 0 and  subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."'   and topic in ('".$topic1."','".$topic2."') order by topic asc limit 0,1"); 
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); 
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="jeexihome" role="tabpanel"
                                         aria-labelledby="jeexihome-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <div class="col-lg-3 col-md-3 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject1-<?php echo $key;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php
                                                            $datat1n = $database->query("SELECT subject_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='".$key."' order by subject asc limit 0,1"); 
                                                            $rowdatat1n=mysqli_fetch_array($datat1n);
                                                            $countt1n= $rowdatat1n['total'];
                                                            if($countt1n!=''){ echo $countt1n; } else{ echo '0'; }
                                                            //echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','2','estatus','1','','','','',''); ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject1-<?php echo $key;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php 
                                                                            //echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','','');
                                                                        
                                                                            $datat = $database->query("SELECT chapter_count as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='".$key."'  and chapter='".$rowchapter['id']."' "); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                            
                                                                        ?> 
                                                                        </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p>
                                                                                        <?php 
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); 
                                                                                        

                                                                                        $topic1=$rowchapter1['class']."_1_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter1['class']."_2_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            
                                                                                            $datatt2 = $database->query("SELECT sum(total) as total FROM syllabus_totals_exam where FIND_IN_SET(1,exam) > 0 and FIND_IN_SET(2,class) > 0 and subject='".$key."'  and topic in ('".$topic1."','".$topic2."') "); 
                                                                                            $rowdataqq2=mysqli_fetch_array($datatt2);
                                                                                            $countq1=$rowdataqq2['total'];
                                                                                            if($countq1!=''){ echo $countq1; } else{ echo '0'; }
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }else if($_REQUEST['exam']=='2'){ ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php 
									   $sqlneet21= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0 order by class asc limit 0,1");
                                        $rowneet21=mysqli_fetch_array($sqlneet21);
                                        $neetcountt21= $rowneet21['total'];
										if($neetcountt21!=''){ echo $neetcountt21; } else { echo '0'; }
									   //echo $database->SrchReportCount('createquestion','exam','2','','','','','estatus','1','','','','',''); ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">

                                        <li class="nav-item">
                                            <div class="nav-link" id="neetxiiprofile-tab" data-toggle="tab"
                                                 <a href="#neetxiiprofile" role="tab" aria-controls="neetxiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php
													 			 $sqlneetc21= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0 order by exam asc limit 0,1");
                                                               $rowsqlneetc21=mysqli_fetch_array( $sqlneetc21);
                                                                 $sqlneetcounttc21= $rowsqlneetc21['total'];
																if($sqlneetcounttc21!=''){ echo $sqlneetcounttc21; } else { echo '0'; }
																//echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php 
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','4','estatus','1','','','','','');
																	 $sqlneetc31= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='4' order by class asc limit 0,1");
																	   $rowsqlneetc31=mysqli_fetch_array( $sqlneetc31);
																		 $sqlneetcounttc31= $rowsqlneetc31['total'];
																		if($sqlneetcounttc31!=''){ echo $sqlneetcounttc31; } else { echo '0'; }
																?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php 
																	 $sqlneetc41= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='2' order by class asc limit 0,1");
                                                               $rowsqlneetc41=mysqli_fetch_array( $sqlneetc41);
                                                                 $sqlneetcounttc41= $rowsqlneetc41['total'];
																if($sqlneetcounttc41!=''){ echo $sqlneetcounttc41; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																$sqlneetc51= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='3' order by class asc limit 0,1");
                                                               $rowsqlneetc51=mysqli_fetch_array( $sqlneetc51);
                                                                 $sqlneetcounttc51= $rowsqlneetc51['total'];
																if($sqlneetcounttc51!=''){ echo $sqlneetcounttc51; } else { echo '0'; }
																		//echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <p class="text-muted"><?php 
									   $sqlneet2= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0   order by class asc limit 0,1");
                                        $rowneet2=mysqli_fetch_array($sqlneet2);
                                        $neetcountt2= $rowneet2['total'];
										if($neetcountt2!=''){ echo $neetcountt2; } else { echo '0'; }
									   //echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">

                                        <li class="nav-item">
                                            <div class="nav-link" id="jeexiiprofile-tab" data-toggle="tab"
                                                 href="#jeexiiprofile" role="tab" aria-controls="jeexiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php 
													 			 $sqlneetc2= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0   order by exam asc limit 0,1");
                                                               $rowsqlneetc2=mysqli_fetch_array( $sqlneetc2);
                                                                 $sqlneetcounttc2= $rowsqlneetc2['total'];
																if($sqlneetcounttc2!=''){ echo $sqlneetcounttc2; } else { echo '0'; }
																//echo $database->SrchReportCount1('createquestion','exam','2','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php 
																	 $sqlneetc3= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='4'  order by subject asc limit 0,1");
                                                               $rowsqlneetc3=mysqli_fetch_array( $sqlneetc3);
                                                                 $sqlneetcounttc3= $rowsqlneetc3['total'];
																if($sqlneetcounttc3!=''){ echo $sqlneetcounttc3; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php 
																$sqlneetc4= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='2'  order by subject asc limit 0,1");
                                                               $rowsqlneetc4=mysqli_fetch_array( $sqlneetc4);
                                                                 $sqlneetcounttc4= $rowsqlneetc4['total'];
																if($sqlneetcounttc4!=''){ echo $sqlneetcounttc4; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																 $sqlneetc5= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='3'  order by class asc limit 0,1");
                                                               $rowsqlneetc5=mysqli_fetch_array( $sqlneetc5);
                                                                 $sqlneetcounttc5= $rowsqlneetc5['total'];
																if($sqlneetcounttc5!=''){ echo $sqlneetcounttc5; } else { echo '0'; }
																		//echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>


                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">

                                    <?php  $subjects1 = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry'); ?>
                                    <div class="tab-pane fade show active" id="neetxiiprofile" role="tabpanel"
                                         aria-labelledby="neetxiiprofile-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <div class="col-lg-4 col-md-4 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject3-<?php echo $key1;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php  
                                                                    //echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','1','estatus','1','','','','','');
                                                                    $sqlneets1= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$key1."'  order by class asc limit 0,1");
                                                                    
                                                                    $rowneets1=mysqli_fetch_array($sqlneets1);
                                                                    $neetcountts1= $rowneets1['total'];
                                                                    if($neetcountts1!=''){ echo $neetcountts1; } else{ echo '0'; }
                                                                    ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject3-<?php echo $key1;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key1."'");
                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter1-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php 
                                                                            $datat = $database->query("SELECT chapter_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."' order by chapter asc limit 0,1"); 
                                                                        
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }

                                                                            //echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); 
                                                                        ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter1-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php 
                                                                                        

                                                                                        $topic1=$rowchapter1['class']."_1_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter1['class']."_2_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            
                                                                                            
                                                                                            $datat1 = $database->query("SELECT sum(total) as total  FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."' and topic in ('".$topic1."','".$topic2."') order by class asc limit 0,1"); 

                                                                                            
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); 
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                    </div>
                                            <?php } ?>
                                            </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>
							 <?php  $subjects1 = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry'); ?>
                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                 
                                    <div class="tab-pane fade" id="jeexiiprofile" role="tabpanel"
                                         aria-labelledby="jeexiiprofile-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <div class="col-lg-4 col-md-4 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject4-<?php echo $key1;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php 
                                                                    $sqlneets11= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$key1."'  order by subject asc limit 0,1");
                                                                    
                                                                    $rowneets11=mysqli_fetch_array($sqlneets11);
                                                                    $neetcountts11= $rowneets11['total'];
                                                            if($neetcountts11!=''){ echo $neetcountts11; } else{ echo '0'; } ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject4-<?php echo $key1;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key1."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php 
                                                                            //echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); 
                                                                            
                                                                            $datat = $database->query("SELECT chapter_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."' order by chapter asc "); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                        ?> 
                                                                        </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); 
                                                                                        

                                                                                        $topic1=$rowchapter1['class']."_1_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                    $topic2=$rowchapter1['class']."_2_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                    
                                                                                    $datat2 = $database->query("SELECT sum(total) as total  FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."'   and topic in ('".$topic1."','".$topic2."') "); 
                                                                                    $rowdataq2=mysqli_fetch_array($datat2);
                                                                                    $countq=$rowdataq2['total'];
                                                                                    if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                    </div>
                                            <?php } ?>
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }else{

                ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php 
										$sqln=$database->query("SELECT class_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0   order by class asc limit 0,1");
								   		$rown=mysqli_fetch_array($sqln);
									  if($rown['total']!=''){ echo $rown['total']; } else { echo '0'; } ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="neetxihome-tab" data-toggle="tab"
                                                 href="#neetxihome" role="tab" aria-controls="neetxihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <?php 
                                                               $sqlneet= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0   order by exam asc limit 0,1");
                                                               $rowsqlneet=mysqli_fetch_array($sqlneet);
                                                                 $sqlneetcountt= $rowsqlneet['total'];
                                                           
                                                                ?>
                                                                <p>
                                                                <?php 
                                                                    //echo $database->SrchReportCount1('createquestion','exam','1','class','1','','','estatus','1','','','','','');
                                                                    if($sqlneetcountt!=''){ echo $sqlneetcountt; } else { echo '0'; }
                                                                 ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                             <?php 
                                                               $sqlbneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='1' order by subject asc limit 0,1");
                                                               $rowbneet=mysqli_fetch_array($sqlbneet);
                                                                $bneetcountt= $rowbneet['total'];
                                                                ?>
                                                            <h5>Botany</h5>
                                                            <h4><?php 
                                                            //echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','1','estatus','1','','','','',''); 
                                                            if($bneetcountt!=''){ echo $bneetcountt; } else { echo '0'; }
                                                            ?> </h4>
                                                        </td>

                                                        <td>
                                                        <?php 
                                                        $sqlpneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='2' order by subject asc limit 0,1");
                                                        $rowpneet=mysqli_fetch_array($sqlpneet);
                                                        $pneetcountt= $rowpneet['total'];
                                                        ?>
                                                            <h5>Physics</h5>
                                                                <h4><?php 
                                                                if($pneetcountt!=''){ echo $pneetcountt; } else { echo '0'; }
                                                                //echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','2','estatus','1','','','','',''); 
                                                                ?> </h4>
                                                        </td>
                                                        <?php 
                                                        $sqlcneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='3' order by subject asc limit 0,1");
                                                        $rowcneet=mysqli_fetch_array($sqlcneet);
                                                        $cneetcountt= $rowcneet['total'];
                                                        ?>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php
                                                            if($cneetcountt!=''){ echo $cneetcountt; } else { echo '0'; } 
                                                            //echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','3','estatus','1','','','','',''); 
                                                            ?> </h4>
                                                        </td>
                                                        <?php 
                                                        $sqlzneet= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='5' order by subject asc limit 0,1");
                                                        $rowzneet=mysqli_fetch_array($sqlzneet);
                                                        $zneetcountt= $rowzneet['total'];
                                                        ?>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php
                                                             if($zneetcountt!=''){ echo $zneetcountt; } else { echo '0'; }  
                                                          // echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','5','estatus','1','','','','','');
                                                             ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <div class="nav-link" id="neetxiiprofile-tab" data-toggle="tab"
                                                 href="#neetxiiprofile" role="tab" aria-controls="neetxiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <?php 
                                                               $sqljee= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0   order by subject asc limit 0,1");
                                                               $rowjee=mysqli_fetch_array($sqljee);
                                                                $jeecountt= $rowjee['total'];
                                                                ?>
                                                                <p><?php 
                                                                if($jeecountt!=''){ echo $jeecountt; } else { echo '0'; }  
                                                                //echo $database->SrchReportCount1('createquestion','exam','1','class','1','','','estatus','1','','','','',''); 
                                                                ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                            <?php 
                                                               $sqlmjee= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='4' order by subject asc limit 0,1");
                                                               $rowmjee=mysqli_fetch_array($sqlmjee);
                                                               $jeemcountt= $rowmjee['total'];
                                                             ?>
                                                                <h4><?php 
                                                                     if($jeemcountt!=''){ echo $jeemcountt; } else { echo '0'; }  
                                                                    // echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','4','estatus','1','','','','',''); 
                                                                ?> </h4>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                               $sqlpjee= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='2' order by subject asc limit 0,1");
                                                               $rowpjee=mysqli_fetch_array($sqlpjee);
                                                               $jeepcountt= $rowpjee['total'];
                                                             ?>
                                                            <h5>Physics</h5>
                                                            <h4><?php 
                                                                if($jeepcountt!=''){ echo $jeepcountt; } else { echo '0'; }  
                                                               // echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','2','estatus','1','','','','','');
                                                             ?> </h4>
                                                        </td>
                                                        <td>
                                                            <?php 
                                                               $sqlcjee= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='3' order by subject asc limit 0,1");
                                                               $rowcjee=mysqli_fetch_array($sqlcjee);
                                                               $jeeccountt= $rowcjee['total'];
                                                             ?>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
                                                                 if($jeeccountt!=''){ echo $jeeccountt; } else { echo '0'; }  
                                                              //  echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','3','estatus','1','','','','',''); 
                                                                ?> </h4>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <?php 
									   
                                        ?>
                                        <h5 class="text-muted"><?php
											$sqln1=$database->query("SELECT class_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  order by class asc limit 0,1");
								   			$rown1=mysqli_fetch_array($sqln1);
											if($rown1['total']!=''){ echo $rown1['total']; } else { echo '0'; } ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="jeexihome-tab" data-toggle="tab"
                                                 href="#jeexihome" role="tab" aria-controls="jeexihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php 
																	//echo $database->SrchReportCount1('createquestion','exam','2','class','2','','','estatus','1','','','','',''); 
												 					
												 				 $sqlneetc2= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(1,exam) > 0   order by class asc limit 0,1");
                                                               $rowsqlneetc2=mysqli_fetch_array( $sqlneetc2);
                                                                 $sqlneetcounttc2= $rowsqlneetc2['total'];
																if($sqlneetcounttc2!=''){ echo $sqlneetcounttc2; } else { echo '0'; }
                                                                ?>
															</p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>


                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php 
															//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','1','estatus','1','','','','','');
															 $sqlneetc3= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='1' order by class asc limit 0,1");
                                                               $rowsqlneetc3=mysqli_fetch_array( $sqlneetc3);
                                                                 $sqlneetcounttc3= $rowsqlneetc3['total'];
																if($sqlneetcounttc3!=''){ echo $sqlneetcounttc3; } else { echo '0'; }
														?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php 
																 $sqlneetc4= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='2' order by class asc limit 0,1");
                                                               $rowsqlneetc4=mysqli_fetch_array( $sqlneetc4);
                                                                 $sqlneetcounttc4= $rowsqlneetc4['total'];
																if($sqlneetcounttc4!=''){ echo $sqlneetcounttc4; } else { echo '0'; }
																//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																	 $sqlneetc5= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='3' order by class asc limit 0,1");
                                                               $rowsqlneetc5=mysqli_fetch_array( $sqlneetc5);
                                                                 $sqlneetcounttc5= $rowsqlneetc5['total'];
																if($sqlneetcounttc5!=''){ echo $sqlneetcounttc5; } else { echo '0'; }
																	//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php
																		 $sqlneetc6= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='5' order by class asc limit 0,1");
                                                               $rowsqlneetc6=mysqli_fetch_array( $sqlneetc6);
                                                                 $sqlneetcounttc6= $rowsqlneetc6['total'];
																if($sqlneetcounttc6!=''){ echo $sqlneetcounttc6; } else { echo '0'; }
																		//echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <div class="nav-link" id="jeexiiprofile-tab" data-toggle="tab"
                                                 href="#jeexiiprofile" role="tab" aria-controls="jeexiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php 
													  $sqlneetc7= $database->query("SELECT exam_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='1' order by class asc limit 0,1");
                                                               $rowsqlneetc7=mysqli_fetch_array($sqlneetc7);
                                                                 $sqlneetcounttc7= $rowsqlneetc7['total'];
																if($sqlneetcounttc7!=''){ echo $sqlneetcounttc7; } else { echo '0'; }
													 //echo $database->SrchReportCount1('createquestion','exam','2','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php 
														  $sqlneetc8= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='4' order by class asc limit 0,1");
                                                               $rowsqlneetc8=mysqli_fetch_array($sqlneetc8);
                                                                 $sqlneetcounttc8= $rowsqlneetc8['total'];
																if($sqlneetcounttc8!=''){ echo $sqlneetcounttc8; } else { echo '0'; }
														 //echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php 
															  $sqlneetc9= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='2' order by class asc limit 0,1");
                                                               $rowsqlneetc9=mysqli_fetch_array($sqlneetc9);
                                                                 $sqlneetcounttc9= $rowsqlneetc9['total'];
																if($sqlneetcounttc9!=''){ echo $sqlneetcounttc9; } else { echo '0'; }
															 //echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php 
																 $sqlneetc10= $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(2,class) > 0  and FIND_IN_SET(2,exam) > 0  and subject='3' order by class asc limit 0,1");
                                                               $rowsqlneetc10=mysqli_fetch_array($sqlneetc10);
                                                                 $sqlneetcounttc10= $rowsqlneetc10['total'];
																if($sqlneetcounttc10!=''){ echo $sqlneetcounttc10; } else { echo '0'; }
																 //echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>


                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>
                        <?php
                        $subjects = array('1' =>'Botony','5' => 'Zoology','2'=> 'Physics','3' => 'Chemistry');
                        ?>
                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="neetxihome" role="tabpanel"
                                         aria-labelledby="neetxihome-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <div class="col-lg-3 col-md-3 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php
                                                                    
                                                                    //echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','1','estatus','1','','','','','');
                                                                    $datatn = $database->query("SELECT subject_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='".$key."' order by class asc limit 0,1"); 
                                                                        $rowdatatn=mysqli_fetch_array($datatn);
                                                                        $counttn= $rowdatatn['total'];
                                                                        if($counttn!=''){ echo $counttn; } else{ echo '0'; }
                                                                    ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject-<?php echo $key;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php
                                                                            $datat = $database->query("SELECT chapter_count as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='".$key."' and chapter='".$rowchapter['id']."' "); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                            //echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php 
                                                                                        

                                                                                        $topic1=$rowchapter1['class']."_1_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter1['class']."_2_".$rowchapter1['chapter']."_".$rowchapter1['id'];
                                                                                            
                                                                                            $datat1 = $database->query("SELECT sum(total) as total FROM syllabus_totals_exam where   FIND_IN_SET(1,class) > 0  and FIND_IN_SET(1,exam) > 0  and subject='".$key."' and chapter='".$rowchapter['id']."'  and topic in ('".$topic1."','".$topic2."') "); 
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); 
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php  $subjects1 = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry'); ?>
                                    <div class="tab-pane fade" id="neetxiiprofile" role="tabpanel"
                                         aria-labelledby="neetxiiprofile-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                 <div class="col-lg-4 col-md-4 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject3-<?php echo $key1;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php  
                                                                        //echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','1','estatus','1','','','','','');
                                                                        $datatn1 = $database->query("select  subject_count as total FROM syllabus_totals_exam where exam='2' and class='1' and subject='".$key1."' "); 
                                                                        $rowdatatn1=mysqli_fetch_array($datatn1);
                                                                        $counttn1= $rowdatatn1['total'];
                                                                        if($counttn1!=''){ echo $counttn1; } else{ echo '0'; }
                                                            ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject3-<?php echo $key1;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key1."'");
                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter1-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php 
                                                                            //echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); 
                                                                            $datat = $database->query("select  chapter_count as total FROM syllabus_totals_exam where exam='2' and class='1' and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."' order by chapter asc limit 0,1 "); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                        $countt= $rowdatat['total'];
                                                                        if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                        ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter1-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php 
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); 
                                                                                        

                                                                                        $topic1=$rowchapter['class']."_1_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter['class']."_2_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            
                                                                                            $datat1 = $database->query("select  sum(total) as total FROM syllabus_totals_exam where exam='2' and class='1' and subject='".$rowchapter['subject']."' and chapter ='".$rowchapter['id']."'   and topic in ('".$topic1."','".$topic2."') "); 
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                            </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="jeexihome" role="tabpanel"
                                         aria-labelledby="jeexihome-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                 <div class="col-lg-3 col-md-3 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject1-<?php echo $key;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php  
                                                                    //echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','2','estatus','1','','','','','');
                                                                    $datatj1 = $database->query("select subject_count  as total FROM syllabus_totals_exam where exam='1' and class='2' and subject='".$key."'  "); 
                                                                        $rowdatatj1=mysqli_fetch_array($datatj1);
                                                                        $counttj1= $rowdatatj1['total'];
                                                                        if($counttj1!=''){ echo $counttj1; } else{ echo '0'; }
                                                                    ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject1-<?php echo $key;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php 
                                                                            $datat = $database->query("select  chapter_count as total FROM syllabus_totals_exam where exam='1' and class='2'  and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."'  order by chapter asc limit 0,1 "); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                            $countt= $rowdatat['total'];
                                                                            if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                            //echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','','');
                                                                        ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php
                                                                                        
                                                                                        $topic1=$rowchapter['class']."_1_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter['class']."_2_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            
                                                                                            $datat1 = $database->query("select  sum(total) as total FROM syllabus_totals_exam where exam='1' and class='2' and subject='".$rowchapter['subject']."' and chapter ='".$rowchapter['id']."'   and topic in ('".$topic1."','".$topic2."') "); 
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); 
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="jeexiiprofile" role="tabpanel"
                                         aria-labelledby="jeexiiprofile-tab">
                                         <div class="row no-gutters">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                 <div class="col-lg-4 col-md-4 col-sm-12 my-2">
                                                    <table class="table table-bordered mr-2 bg-white">
                                                        <thead onclick="$('#subject4-<?php echo $key1;?>').toggle(200);">

                                                        <tr>
                                                            <th class="bg-gray-color" colspan="4">
                                                                <div class="d-flex justify-content-between">
                                                                    <h5><?php  echo $sub;?></h5>
                                                                    <h5><?php
                                                                        //echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','2','estatus','1','','','','','');
                                                                        $datat1 = $database->query("select  subject_count as total FROM syllabus_totals_exam where exam='2' and class='2' and subject='".$key1."' "); 
                                                                        $rowdatat1=mysqli_fetch_array($datat1);
                                                                        $countt1= $rowdatat1['total'];
                                                                        if($countt1!=''){ echo $countt1; } else{ echo '0'; }
                                                                    ?></h5>
                                                                </div>
                                                            </th>
                                                        </tr>
                                                        </thead>

                                                        <tbody style="display: none;" id="subject4-<?php echo $key1;?>">
                                                        <tr>
                                                            <td>
                                                                <h6><b>Chapters & Topics</b></h6>
                                                            </td>
                                                            <td>
                                                                <h6><b>Questions</b></h6>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $i=1;
                                                        $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key1."'");

                                                        while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                            ?>
                                                            <tr>
                                                                <th class="bg-gray-color" colspan="2" onclick="$('#chapter2-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                    <div class="d-flex justify-content-between">
                                                                        <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                        <p><?php
                                                                            $datat = $database->query("select  chapter_count as total FROM syllabus_totals_exam where exam='2' and class='2' and subject='".$rowchapter['subject']."' and chapter='".$rowchapter['id']."'  order by chapter asc limit 0,1"); 
                                                                            $rowdatat=mysqli_fetch_array($datat);
                                                                            $countt= $rowdatat['total'];
                                                                            if($countt!=''){ echo $countt; } else{ echo '0'; }
                                                                            //echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); 
                                                                        ?> </p>
                                                                    </div>
                                                                </th>
                                                            </tr>

                                                            <tr>
                                                                <td colspan="2" style="padding:0;">

                                                                    <table  width="100%" style="display: none;" id="chapter2-<?php echo $rowchapter['id'];?>">
                                                                        <?php
                                                                        $j=1;
                                                                        $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                        while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                            ?>

                                                                            <tr>
                                                                                <td width="70%">
                                                                                    <p><?php echo $rowchapter1['topic']; ?></p>
                                                                                </td>
                                                                                <td width="30%">

                                                                                    <p><?php
                                                                                        
                                                                                        

                                                                                        $topic1=$rowchapter['class']."_1_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            $topic2=$rowchapter['class']."_2_".$rowchapter['id']."_".$rowchapter1['id'];
                                                                                            
                                                                                            $datat1 = $database->query("select  sum(total) as total FROM syllabus_totals_exam where exam='2' and class='2' and subject='".$rowchapter['subject']."' and chapter ='".$rowchapter['id']."'   and topic in ('".$topic1."','".$topic2."') "); 
                                                                                            $rowdataq1=mysqli_fetch_array($datat1);
                                                                                            $countq=$rowdataq1['total'];
                                                                                            if($countq!=''){ echo $countq; } else{ echo '0'; }
                                                                                        //echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','','');
                                                                                    ?></p>
                                                                                </td>
                                                                            </tr>

                                                                            <?php
                                                                            $j++;
                                                                        }
                                                                        ?>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                            $i++;
                                                        }


                                                        ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <?php } ?>
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }

        ?>
        <!-- Tabs Section2 End-->
    </div>
<?php
}
?>