<?php 
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['topicOverview'])){
    ?>
    <div class="card-header" id="subjectoverview">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#subjectchapdiv" aria-expanded="true"
		   aria-controls="subjectchapdiv"  >
			<h5 class="card-title mb-0"><b>Subject ,Chapter & Topic wise Overview</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <div class="collapse show" id="subjectchapdiv"  aria-labelledby="subjectoverview" >
        <?php

    $con="";
            if($_REQUEST['exam']!=''){
                $con=" AND id IN (".$_REQUEST['exam'].")";
            }

            if($_REQUEST['exam']=='1'){
                ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php echo $database->SrchReportCount('createquestion','exam','1','','','','','estatus','1','','','','',''); ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="neetxihome-tab" data-toggle="tab"
                                                 href="#neetxihome" role="tab" aria-controls="neetxihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','1','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <p class="text-muted"><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="jeexihome-tab" data-toggle="tab"
                                                 href="#jeexihome" role="tab" aria-controls="jeexihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>


                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','1','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>
                        <?php
                        $subjects = array('1' =>'Botony','5' => 'Zoology','2'=> 'Physics','3' => 'Chemistry');
                        ?>
                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="neetxihome" role="tabpanel"
                                         aria-labelledby="neetxihome-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','1','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject-<?php echo $key;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="jeexihome" role="tabpanel"
                                         aria-labelledby="jeexihome-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject1-<?php echo $key;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','2','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject1-<?php echo $key;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }else if($_REQUEST['exam']=='2'){ ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php echo $database->SrchReportCount('createquestion','exam','2','','','','','estatus','1','','','','',''); ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">

                                        <li class="nav-item">
                                            <div class="nav-link" id="neetxiiprofile-tab" data-toggle="tab"
                                                 <a href="#neetxiiprofile" role="tab" aria-controls="neetxiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <p class="text-muted"><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">

                                        <li class="nav-item">
                                            <div class="nav-link" id="jeexiiprofile-tab" data-toggle="tab"
                                                 href="#jeexiiprofile" role="tab" aria-controls="jeexiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>


                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>

                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">

                                    <?php  $subjects1 = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry'); ?>
                                    <div class="tab-pane fade show active" id="neetxiiprofile" role="tabpanel"
                                         aria-labelledby="neetxiiprofile-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject3-<?php echo $key1;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','1','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject3-<?php echo $key1;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key1."'");
                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter1-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter1-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                            </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="jeexihome" role="tabpanel"
                                         aria-labelledby="jeexihome-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject1-<?php echo $key;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','2','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject1-<?php echo $key;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="jeexiiprofile" role="tabpanel"
                                         aria-labelledby="jeexiiprofile-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject4-<?php echo $key1;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','2','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject4-<?php echo $key1;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key1."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }else{

                ?>
                <div class="question-tabs card border-0 shadow-sm table-responsive">
                    <div class="card-body">
                        <ul class="nav nav-pills nav-fill question-pills" id="questionpills-tab" role="tablist">
                            <li class="nav-item mr-2">
                                <a class="nav-link active" id="neetpills-home-tab" data-toggle="pill" href="#NEETXIpills-home"
                                   role="tab" aria-controls="NEETXIpills-home" aria-selected="true">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XI</b></h6>
                                        <h5 class="text-muted"><?php echo $database->SrchReportCount('createquestion','exam','1','','','','','estatus','1','','','','',''); ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="neetxihome-tab" data-toggle="tab"
                                                 href="#neetxihome" role="tab" aria-controls="neetxihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','1','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','1','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <div class="nav-link" id="neetxiiprofile-tab" data-toggle="tab"
                                                 href="#neetxiiprofile" role="tab" aria-controls="neetxiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                    </ul>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="jeepills-profile-tab" data-toggle="pill" href="#JEEXIpills-profile"
                                   role="tab" aria-controls="JEEXIpills-profile" aria-selected="false">
                                    <div class="d-flex justify-content-between">
                                        <h6 class="text-dark"><b>Class XII</b></h6>
                                        <h5 class="text-muted"><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></h5>
                                    </div>
                                    <ul class="nav nav-tabs" id="questionTablist" role="tablist">
                                        <li class="nav-item">
                                            <div class="nav-link active" id="jeexihome-tab" data-toggle="tab"
                                                 href="#jeexihome" role="tab" aria-controls="jeexihome"
                                                 aria-selected="true">
                                                <table class="table table-bordered mr-3">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>NEET</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','2','class','1','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>


                                                        <td>
                                                            <h5>Botany</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','1','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                        <td>
                                                            <h5>Physics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Zoology</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','1','class','2','subject','5','estatus','1','','','','',''); ?> </h4>
                                                        </td>

                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>
                                        <li class="nav-item">
                                            <div class="nav-link" id="jeexiiprofile-tab" data-toggle="tab"
                                                 href="#jeexiiprofile" role="tab" aria-controls="jeexiiprofile"
                                                 aria-selected="false">
                                                <table class="table table-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th colspan="3">
                                                            <div class="d-flex justify-content-between">
                                                                <h6><b>JEE</b></h6>
                                                                <p><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','','','estatus','1','','','','',''); ?></p>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>
                                                            <h5>Mathematics</h5>
                                                                <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','4','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Physics</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','2','estatus','1','','','','',''); ?> </h4>
                                                        </td>
                                                        <td>
                                                            <h5>Chemistry</h5>
                                                            <h4><?php echo $database->SrchReportCount1('createquestion','exam','2','class','2','subject','3','estatus','1','','','','',''); ?> </h4>
                                                        </td>


                                                    </tr>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </li>

                                    </ul>
                                </a>
                            </li>
                        </ul>
                        <?php
                        $subjects = array('1' =>'Botony','5' => 'Zoology','2'=> 'Physics','3' => 'Chemistry');
                        ?>
                        <div class="tab-content date-content" id="pills-tabContent">
                            <div class="tab-pane date-contentactive fade show active" id="NEETXIpills-home"
                                 role="tabpanel" aria-labelledby="neetpills-home-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="neetxihome" role="tabpanel"
                                         aria-labelledby="neetxihome-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','1','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject-<?php echo $key;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <?php  $subjects1 = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry'); ?>
                                    <div class="tab-pane fade" id="neetxiiprofile" role="tabpanel"
                                         aria-labelledby="neetxiiprofile-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject3-<?php echo $key1;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','1','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject3-<?php echo $key1;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='1' and subject='".$key1."'");
                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter1-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter1-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='1' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','1','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                            </tbody>
                                            </table>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade date-contentactive" id="JEEXIpills-profile" role="tabpanel"
                                 aria-labelledby="jeepills-profile-tab">
                                <div class="tab-content" id="questionInnerTabContent">
                                    <div class="tab-pane fade show active" id="jeexihome" role="tabpanel"
                                         aria-labelledby="jeexihome-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects as $key => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject1-<?php echo $key;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class','2','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject1-<?php echo $key;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>

                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="jeexiiprofile" role="tabpanel"
                                         aria-labelledby="jeexiiprofile-tab">
                                        <div class="d-flex">
                                            <?php
                                            foreach($subjects1 as $key1 => $sub){

                                                ?>
                                                <table class="table table-bordered mr-2 bg-white">
                                                    <thead onclick="$('#subject4-<?php echo $key1;?>').toggle(200);">

                                                    <tr>
                                                        <th class="bg-gray-color" colspan="4">
                                                            <div class="d-flex justify-content-between">
                                                                <h5><?php  echo $sub;?></h5>
                                                                <h5><?php  echo $database->SrchReportCount1('createquestion','exam','2','subject',$key1,'class','2','estatus','1','','','','',''); ?></h5>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>

                                                    <tbody style="display: none;" id="subject4-<?php echo $key1;?>">
                                                    <tr>
                                                        <td>
                                                            <h6><b>Chapters & Topics</b></h6>
                                                        </td>
                                                        <td>
                                                            <h6><b>Questions</b></h6>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                    $i=1;
                                                    $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='2' and subject='".$key1."'");

                                                    while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                        ?>
                                                        <tr>
                                                            <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                                <div class="d-flex justify-content-between">
                                                                    <h6><?php echo $rowchapter['chapter']; ?></h6>

                                                                    <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'','','subject',$key1,'estatus','1','','','','',''); ?> </p>
                                                                </div>
                                                            </th>
                                                        </tr>

                                                        <tr>
                                                            <td colspan="2" style="padding:0;">

                                                                <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                                    <?php
                                                                    $j=1;
                                                                    $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='2' and subject='".$key1."' and chapter='".$rowchapter['id']."'");
                                                                    while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                                        ?>

                                                                        <tr>
                                                                            <td width="70%">
                                                                                <p><?php echo $rowchapter1['topic']; ?></p>
                                                                            </td>
                                                                            <td width="30%">

                                                                                <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class','2','chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key1,'','','estatus','1','','',''); ?></p>
                                                                            </td>
                                                                        </tr>

                                                                        <?php
                                                                        $j++;
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </td>
                                                        </tr>

                                                        <?php
                                                        $i++;
                                                    }


                                                    ?>

                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }

        ?>
        <!-- Tabs Section2 End-->
    </div>
<?php
}
?>