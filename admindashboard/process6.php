<style type="text/css">
.time-chapter-tabledata .table-bordered {
border:1px solid #dee2e6;   
 }
    .time-chapter-tabledata table th {
        padding: 0.5rem;
    }
</style>
<?php
include('../include/session.php');
error_reporting(0);
if(!$session->logged_in){
?>
    <script type="text/javascript">
        alert("User with the same username logged in to another browser");
        //setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
        location.replace("<?php echo SECURE_PATH;?>admin/");
    </script>
<?php
}
?>

 <?php echo $session->commonJS();?>
 <?php echo $session->commonAdminCSS();?>
   <script type="text/javascript">
       function search_report() {
        
            var class1 = $('#class1').val();
            var subject1 = $('#subject1').val();
            var exam = $('#exam').val();
            var pexam = $('#pexam').val();

            setStateGet('adminTable','<?php echo SECURE_PATH;?>admindashboard/process6.php','viewDetails2=1&class1='+class1+'&exam='+exam+'&pexam='+pexam+'&subject1='+subject1);
        }
   </script>
    
    
    <div id="adminTable">
        <?php
        if(isset($_REQUEST['viewDetails2']))
        {
            
            
            
        ?>
        
        <section class="content-area time-chapter-tabledata">
        <div class="container">
            <div class="row Data-Tables">
                <div class="col-xl-12 col-lg-12"> 
                    <div class="card border-0 shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Chapter wise question time analysis</h6>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered table-responsive mb-0" id="aaas">
                        <thead class="bg-light">
                       <!--  <tr> -->
                            <th rowspan="3" class="text-center">
</th>
                   <div class="row">
                     <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Class</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="class1" value=""  id="class1" onchange="search_report();">
                                                <option value=''>-- Select --</option>
                                                <?php
                                                $row = $database->query("select * from class where estatus='1'");
                                                while($data = mysqli_fetch_array($row))
                                                {
                                                    ?>
                                                <option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class1'])) { if($_REQUEST['class1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
                                                <?php
                                                }
                                                ?>
                                            </select>
                                            <span class="error"><?php if(isset($_SESSION['error']['class1'])){ echo $_SESSION['error']['class1'];}?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Exam</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="exam" value=""  id="exam" onchange="search_report();setState('aaa1','<?php echo SECURE_PATH;?>admindashboard/process6.php','getsubject1=1&class='+$('#class1').val()+'&exam='+$('#exam').val()+'');">
                                                <option value=''>-- Select --</option>
                                                <option value='1' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='1') { echo 'selected="selected"'; }  } ?>>NEET</option>
                                                <option value='2' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='2') { echo 'selected="selected"'; }  } ?>>JEE</option>
                                                
                                            </select>
                                            <span class="error"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if(isset($_REQUEST['exam'])) { 
                                    if($_REQUEST['exam']=='1') {
                                        $style="style='display:none;'";
                                    }else{
                                        $style="";
                                    }
                                }
                                ?>
                                
                                <div class="col-lg-4" >
                                    <div class="form-group row" id="aaa1">
                                        <label class="col-sm-4 col-form-label">Subject</label>
                                        <div class="col-sm-8">
                                        <select class="form-control" name="subject1" value=""   id="subject1" onChange="search_report();">
                                            <option value=''>-- Select --</option>
                                            <?php
                                            $row = $database->query("select * from subject where estatus='1'");
                                            while($data = mysqli_fetch_array($row))
                                            {
                                                ?>
                                            <option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject1'])) { if($_REQUEST['subject1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
                                            <?php
                                            }
                                            ?>
                                        </select>
                                            <span class="error"><?php if(isset($_SESSION['error']['subject1'])){ echo $_SESSION['error']['subject1'];}?></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4" <?php echo $style; ?> >
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">Exam Type</label>
                                        <div class="col-sm-8">
                                            <select class="form-control" name="pexam" value=""  id="pexam" onchange="search_report();">
                                                <option value='0'>-- Select --</option>
                                                <option value='1' <?php if(isset($_REQUEST['pexam'])) { if($_REQUEST['pexam']=='1') { echo 'selected="selected"'; }  } ?>>Mains</option>
                                                <option value='2' <?php if(isset($_REQUEST['pexam'])) { if($_REQUEST['pexam']=='2') { echo 'selected="selected"'; }  } ?>>Advance</option>
                                                
                                            </select>
                                            <span class="error"><?php if(isset($_SESSION['error']['pexam'])){ echo $_SESSION['error']['pexam'];}?></span>
                                        </div>
                                    </div>
                                </div>
                              </div>

                            <!-- <?php
                            $data1 = $database->query("select * from class where estatus=1 ");
                            while($data1row=mysqli_fetch_array($data1)){
                                ?>
                                <th colspan="<?php echo $cols; ?>">
                                    <h6 class="text-dark p-2"><b>ClassXI</b></h6>
                                </th>
                            <?php } ?> -->

                        <!-- </tr> -->
                        <tr>
<?php
                        $zero=" and timeduration < 10 and timeduration!=''";
                        $ten=" and timeduration between 10 and 20 and timeduration!=''";
                        $twenty=" and timeduration between 20 and 30 and timeduration!=''";
                        $thirty=" and timeduration between 30 and 40 and timeduration!=''";
                        $fourty=" and timeduration between 40 and 50 and timeduration!=''";
                        $fifty=" and timeduration between 50 and 60 and timeduration!=''";
                        $oneminute=" and timeduration between 60 and 120 and timeduration!=''";
                        $twominute=" and timeduration between 120 and 180 and timeduration!=''";
                        $threeminute=" and timeduration between 180 and 240 and timeduration!=''";
                        $fourminute=" and timeduration between 240 and 300 and timeduration!=''";
                        $fiveminute=" and timeduration between 300 and 360 and timeduration!=''";

                        $timerange = array($zero =>'0 To  10 Seconds',$ten => '10 To  20 Seconds',$twenty=> '20 To  30 Seconds',$thirty => '30 To  40 Seconds',$fourty => '40 To  50 Seconds',$fifty => '50 To  60 Seconds',$oneminute => '1 To 2 minutes',$twominute => '2 To 3 minutes',$threeminute => '3 To 4 minutes',$fourminute =>'4 To 5 minutes');

                        foreach($timerange as $timekey => $time){
                            ?>
                                <th><h6><?php echo $time; ?></h6></th>
                           <?php }?>

                        <th><h6>Total</h6></th>

                        </tr>

                        
                        </thead>
                        <tbody>
                        <?php
                        $condition="";
                        if(isset($_REQUEST['class1'])){
                                            if($_REQUEST['class1']!=""){
                                                $class=" AND class LIKE '%".$_REQUEST['class1']."%'";
                                            } else {
                                                $class="";
                                            }
                                        }else{
                                            $class="";
                                        }
                                        if(isset($_REQUEST['subject1'])){
                                            if($_REQUEST['subject1']!=""){
                                                $subject=" AND subject='".$_REQUEST['subject1']."'";
                                            } else {
                                                $subject="";
                                            }
                                        }else{
                                            $subject="";
                                        }
                                        if(isset($_REQUEST['exam'])){
                                            if($_REQUEST['exam']!=""){
                                                $exam=" AND exam LIKE '%".$_REQUEST['exam']."%'";
                                            } else {
                                                $exam="";
                                            }
                                        }else{
                                            $exam="";
                                        }
                                        if(isset($_REQUEST['pexam'])){
                                        if($_REQUEST['exam']!=""){

                                            $pexam = " and pexamtype in (".$_REQUEST['pexam'].")";
                                        }else{
                                            $pexam = "and pexamtype in (0)";
                                        }
                                        }
                                        else{
                                            $pexam="and pexamtype in (0)";
                                        }

                         // if(strlen($_REQUEST['class'])>0){
                         //   $condition = "and class='".$_REQUEST['class']."'";
                         // }
                            $condition= $class.$subject;            
                        $sell3=$database->query("select * from chapter where  estatus='1' ".$condition."");
                        while($rowll3=mysqli_fetch_array($sell3)){
                             $htotal=0;

                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll3['chapter']; ?></h6>
                                </th>
                                <?php
                                $j=1;
                                $timerange1 = array($zero =>'0 To  10 Seconds',$ten => '10 To  20 Seconds',$twenty=> '20 To  30 Seconds',$thirty => '30 To  40 Seconds',$fourty => '40 To  50 Seconds',$fifty => '50 To  60 Seconds',$oneminute => '1 To 2 minutes',$twominute => '2 To 3 minutes',$threeminute => '3 To 4 minutes',$fourminute =>'4 To 5 minutes');

                                 foreach($timerange1 as $timekey1 => $time1){

                                            $todata = $database->query("select count(*) from createquestion where estatus=1 ".$exam." ".$pexam."  and chapter='".$rowll3['id']."' ".$timekey1." ".$condition."");
                                            
                                            $quecount=mysqli_fetch_array($todata);

                                            ?>
                                            <td>
                                                <p><?php  if($quecount[0]!=''){ echo $quecount[0]; } else{ echo '0'; } 
                                                    $htotal= $htotal+$quecount[0];
                                                ?></p>
                                            </td>


                                            <?php $j++; } ?>
                                         <td><?php echo $htotal; ?></td>
                                            <?php }  ?>
                            </tr>
                            <?php

                        

                        ?>
                         <tr><th>
                                    <h6>Total</h6>
                                </th>

                               <?php 
                                $total2=0;
                                $timerange2 = array($zero =>'0 To  10 Seconds',$ten => '10 To  20 Seconds',$twenty=> '20 To  30 Seconds',$thirty => '30 To  40 Seconds',$fourty => '40 To  50 Seconds',$fifty => '50 To  60 Seconds',$oneminute => '1 To 2 minutes',$twominute => '2 To 3 minutes',$threeminute => '3 To 4 minutes',$fourminute =>'4 To 5 minutes');

                                 foreach($timerange2 as $timekey2 => $time2){


                                            $vtotal=0;
                                            
                                   $sell3v=$database->query("select * from chapter where  estatus='1' ".$condition."");
                                   while($rowll3v=mysqli_fetch_array($sell3v)){
                                    $todatav = $database->query("select count(*) from createquestion where estatus=1 ".$exam." ".$pexam."  and chapter='".$rowll3['id']."' ".$timekey1." ".$condition."");
                                        
                                          $complexcountv=mysqli_fetch_array($todatav);
                                              $vtotal= $vtotal+$complexcountv[0];
                                              
                                   }
                                      $total2=$total2+$vtotal;
                        ?>
                        <td>
                                                <p><?php echo $vtotal; 
                                                    
                                                ?></p>
                                            </td> 
                                       <?php }  ?>
                                <td><?php echo $total2; ?></td>

                              </tr>  
                        </tbody>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
    </section>                              
        
<?php 
    }


 ?>
 </div>
 <?php
 if(isset($_REQUEST['getsubject1']))
    {   
    ?>
 
    
        <label class="col-sm-4 col-form-label">Subject</label>
         <div class="col-sm-8"> 
            <select class="form-control" name="subject1" value=""   id="subject1"  onChange="search_report();">
                <option value=''>-- Select --</option>
                <?php
                if($_REQUEST['exam']=='1'){
                    $row1 = $database->query("select * from subject where estatus='1'  and id!='4'");
                }else if($_REQUEST['exam']=='2'){
                    $row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5 ");
                    echo "select * from subject where estatus='1'  and id!=1 and id!=5 ";
                }else{
                    $row1 = $database->query("select * from subject where estatus='1' ");
                }
                while($data = mysqli_fetch_array($row1))
                {
                    ?>
                <option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
                <?php
                }
                ?>
            </select>
             </div>
                    
    <?php
    }
    ?>
 <?php echo $session->commonFooterAdminJS(); ?>
 <script>

    $(function () {
        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });
    </script>