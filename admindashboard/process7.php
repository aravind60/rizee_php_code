
<?php
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['previousAnalysis'])){
    ?>
	<div class="card-header" id="previouspaper">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#previousdiv" aria-expanded="true"
		   aria-controls="previousdiv"  >
			<h5 class="card-title mb-0"><b>Previous Paper Analysis</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <div class="collapse show" id="previousdiv"  aria-labelledby="previouspaper" >
        <div class="border-bottom">
            <ul class="nav nav-pills attribute" id="pills-tab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home1"
                       role="tab" aria-controls="pills-home1" aria-selected="true"><b>NEET</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile1"
                       role="tab" aria-controls="pills-profile1" aria-selected="false"><b>JEE Mains</b></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact1"
                       role="tab" aria-controls="pills-contact1" aria-selected="false"><b>JEE Advance</b></a>
                </li>
                 <li class="nav-item">
                    <a class="nav-link" id="pills-bitsat-tab" data-toggle="pill" href="#pills-bitsat"
                       role="tab" aria-controls="pills-bitsat" aria-selected="false"><b>BITSAT</b></a>
                </li>
				 <li class="nav-item">
                    <a class="nav-link" id="pills-eamcetap-tab" data-toggle="pill" href="#pills-eamcetap"
                       role="tab" aria-controls="pills-eamcetap" aria-selected="false"><b>AP EAMCET MPC</b></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="pills-eamcetts-tab" data-toggle="pill" href="#pills-eamcetts"
                       role="tab" aria-controls="pills-eamcetts" aria-selected="false"><b>TS EAMCET MPC</b></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="pills-eamcetapbipc-tab" data-toggle="pill" href="#pills-eamcetapbipc"
                       role="tab" aria-controls="pills-eamcetapbipc" aria-selected="false"><b>AP EAMCET BIPC</b></a>
                </li>
				<li class="nav-item">
                    <a class="nav-link" id="pills-eamcettsbipc-tab" data-toggle="pill" href="#pills-eamcettsbipc"
                       role="tab" aria-controls="pills-eamcettsbipc" aria-selected="false"><b>TS EAMCET BIPC</b></a>
                </li>
            </ul>
        </div>
        <div class="tab-content p-2" id="pills-tabContent">
            <?php
            $con="";
            $con2="";
                if($_REQUEST['exam']!=''){
                    $con=" AND id IN (".$_REQUEST['exam'].")";
                    $con2=" AND exam_id IN (".$_REQUEST['exam'].")";

                }
               

                if($_REQUEST['exam']=='1'){
                    $cols="6";
                }else if($_REQUEST['exam']=='2'){
                    $cols="5";
                }else if($_REQUEST['exam']=='1,2'){
                    $cols="9";
                }else{
                    $cols="9";
                }
                ?>
                <div class="tab-pane fade show active" id="pills-home1" role="tabpanel"
                     aria-labelledby="pills-home-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        

                      
                        <tr>
                         <th colspan="1"></th>

                            <?php

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row3['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                         <th colspan="1"></th>

                            <?php

                                        $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=1");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							$htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                            $data3 = $database->query("select * from subject where estatus=1 and id!='4'");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc=$complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"> <?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
											<?php
											 $htotal= $htotal+$complexcount[0]; 
											$htotal1= $htotal1+$totc;
                                              $total+=$complexcount[0];
											 $j++; 
											 }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&subject=<?php echo $row3['id']; ?>&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal1; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                           
                                
                               
                                       $total2=0;  
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id!='4'");
                                        
                                        while($row3v=mysqli_fetch_array($data3v)){
                                             $total1=0;
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=1");

										while($rowll2v=mysqli_fetch_array($sell2v)){
										 $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
										// echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
										 while($checksetv = mysqli_fetch_array($checkset1v)){
											
										$todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");

															$complexcountv=mysqli_fetch_array($todatav);
															$todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

															$complexcountverv=mysqli_fetch_array($todataverv);
															$todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and find_in_set(1,exam)>0 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

															$complexcountpenv=mysqli_fetch_array($todatapenv);
															 // $vtotal= $vtotal+$complexcountv[0];
															 $vtotal1= $vtotal1+$complexcountverv[0];
															$vtotal2= $vtotal2+$complexcountpenv[0];
															    $tot1= $complexcountpenv[0]+$complexcountverv[0];
															 $vtotal= $vtotal+$tot1;
															  
										}
									}
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1; ?></a></td>
						<td><a  href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                     <?php }  ?>
                               
                          <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=1&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=&examtype=&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></a></td>

                            </tr>

                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-profile1" role="tabpanel"
                     aria-labelledby="pills-profile-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                                                     <th colspan="1"></th>

                            <?php

                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row3['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                               
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                                                    <th colspan="1"></th>

                            <?php

                                    $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                               
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=2 and pexamtype=1");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                         //echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
                        $htotal=0;
						$htotal1=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                            
                                            $todata = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0  and pexamtype=1 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");
                                           // echo "select count(*) from createquestion where estatus=1 and class LIKE '%".$data1row['id']."%' and exam=2 and pexamtype=1 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'";
                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=1 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=1 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc=$complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=1&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=1&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=1&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?><a>
                                            </td>
   

                                            <?php 
												$htotal= $htotal+$complexcount[0]; 
                                                    $total+=$complexcount[0];
													$htotal1= $htotal1+$totc; 
												$j++; 
											}
                                           
                                            ?>
                                        
                                   <td> <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&examtype=1&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal1; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                            $total2=0;
                                
                               
                                        
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                             $total1=0;
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=2 and pexamtype=1");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        while($checksetv = mysqli_fetch_array($checkset1v)){
                          
                        $todatav = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=1 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=1 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1'  and FIND_IN_SET(2,exam) > 0  and pexamtype=1 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
											//$vtotal= $vtotal+$complexcountv[0];
											 $vtotal1= $vtotal1+$complexcountverv[0];
											$vtotal2= $vtotal2+$complexcountpenv[0];
											$tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;
                                               
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=1&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1;  ?></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=1&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"> <?php echo $vtotal2; ?></td>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=1&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal;  ?>
                                            </td>
                     <?php }  ?>
                               
                                    <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=1&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></a></td>

                            </tr>
  
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-contact1" role="tabpanel"
                     aria-labelledby="pills-contact-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row3['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=2 and pexamtype=2");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=2&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=2&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&examtype=2&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&examtype=2&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id!=1 and id!=5");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=2 and pexamtype=2");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(2,exam) > 0 and pexamtype=2 and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=2&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=2&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=2&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&examtype=2&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
				<div class="tab-pane fade" id="pills-bitsat" role="tabpanel"
                     aria-labelledby="pills-bitsat-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data3 = $database->query("select * from subject where estatus=1");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row3['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data3 = $database->query("select * from subject where estatus=1");
                                    
                                    while($row3=mysqli_fetch_array($data3)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=4");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(4,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(4,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(4,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=4");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(4,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(4,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(4,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>



				<div class="tab-pane fade" id="pills-eamcetap" role="tabpanel"
                     aria-labelledby="pills-eamcetap-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data4 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row4['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data4 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=3");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(3,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(3,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(3,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=3");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(3,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(3,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(3,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>



				<div class="tab-pane fade" id="pills-eamcetts" role="tabpanel"
                     aria-labelledby="pills-eamcetts-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data4 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row4['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data4 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=6");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(6,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(6,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(6,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id in (2,3,4)");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=6");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(6,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(6,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(6,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=2&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>


				<div class="tab-pane fade" id="pills-eamcetapbipc" role="tabpanel"
                     aria-labelledby="pills-eamcetap-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data4 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row4['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data4 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=7");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(7,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(7,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(7,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=7");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(7,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(7,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(7,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=7&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="tab-pane fade" id="pills-eamcettsbipc" role="tabpanel"
                     aria-labelledby="pills-eamcettsbipc-tab">
                    <table class="table table-bordered">
                        <thead class="bg-light">
                        
                        

                        <tr>
                            <th colspan="1"></th>
                            <?php

                                        $data4 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                        <th colspan="3">
                                            <h6 class="text-dark p-2"><?php echo $row4['subject']; ?></h6>

                                        </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th><h6>Total</h6></th>
                        </tr>

                        <tr>
                           <th colspan="1"></th>
                            <?php

                                    $data4 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                    
                                    while($row4=mysqli_fetch_array($data4)){ ?>
                                     <th>
                                        <p>Verified</p>
                                    </th>
                                    <th>
                                        <p>Pending</p>
                                    </th>
                                    <th>
                                        <p>Total</p>
                                    </th>
                                        <?php
                                    }
                                    ?>
                                     
                                
                            <th>
                                            <p>Total</p>

                                        </th>
                        </tr>
                        </thead>

                        <tbody>
                        <?php
                       $total=0;
                      
                        $sell2=$database->query("select * from previous_questions where estatus='1' and exam=8");

                        while($rowll2=mysqli_fetch_array($sell2)){
                         $checkset1 = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checkset = mysqli_fetch_array($checkset1)){
							  $htotal1=0;
                        $htotal=0;
                            ?>

                            <tr>
                                <th>
                                    <h6><?php echo $rowll2['year']." ".$checkset['qset']; ?></h6>
                                </th>
                                <?php
                                $j=1;




                                        $data3 = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                        
                                        while($row3=mysqli_fetch_array($data3)){

                                             
                                            $todata = $database->query("select count(*) from createquestion where estatus=1  and ppaper='1' and FIND_IN_SET(8,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."'");

                                            $complexcount=mysqli_fetch_array($todata);
                                            $todataver = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1'  and FIND_IN_SET(8,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=1");

                                            $complexcountver=mysqli_fetch_array($todataver);
                                            $todatapen = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(8,exam) > 0  and subject='".$row3['id']."' and year='".$rowll2['year']."' and qset='".$checkset['id']."' and vstatus1=0");

                                            $complexcountpen=mysqli_fetch_array($todatapen);
											$totc= $complexcountver[0]+$complexcountpen[0];
                                            ?>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountver[0]!=''){ echo $complexcountver[0]; } else{ echo '0'; }?></a></td>
                                            <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php  if($complexcountpen[0]!=''){ echo $complexcountpen[0]; } else{ echo '0'; }?></a></td>
                                            <td>
                                                <a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&subject=<?php echo $row3['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php  if($totc!=''){ echo $totc; } else{ echo '0'; } ?></a>
                                            </td>
   

                                            <?php
												 $htotal= $htotal+$complexcount[0]; 
                                                 $total+=$complexcount[0];
												  $htotal1= $htotal1+$totc; 
												$j++; }
                                           
                                            ?>
                                        
                                   <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2['year']; ?>&set=<?php echo $checkset['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $htotal; ?></a></td>
                               <?php }   ?>
                            </tr>
                            <?php

                        }
                        ?>

                        <tr><th>
                                    <h6>Total</h6>
                                </th>

                            <?php 
                                                                       $total2=0;  

                                
                               
                                       
                                        

                                        $data3v = $database->query("select * from subject where estatus=1 and id in (1,2,3,5)");
                                        while($row3v=mysqli_fetch_array($data3v)){
                                            $vtotal=0;
                                             $vtotal1=0;
                                             $vtotal2=0;
                                         $sell2v=$database->query("select * from previous_questions where estatus='1' and exam=8");

                        while($rowll2v=mysqli_fetch_array($sell2v)){
                         $checkset1v = $database->query("select * from previous_sets where estatus=1 and pid='".$rowll2v['id']."'");
                        // echo "select * from previous_sets where estatus=1 and pid='".$rowll2['id']."'";
                         while($checksetv = mysqli_fetch_array($checkset1v)){
                            
                        $todatav = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(8,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."'");
                            
                                            $complexcountv=mysqli_fetch_array($todatav);
                                            $todataverv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(8,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=1");

                                            $complexcountverv=mysqli_fetch_array($todataverv);
                                            $todatapenv = $database->query("select count(*) from createquestion where estatus=1 and ppaper='1' and FIND_IN_SET(8,exam) > 0  and subject='".$row3v['id']."' and year='".$rowll2v['year']."' and qset='".$checksetv['id']."' and vstatus1=0");

                                            $complexcountpenv=mysqli_fetch_array($todatapenv);
                                             // $vtotal= $vtotal+$complexcountv[0];
                                               $vtotal1= $vtotal1+$complexcountverv[0];
                                            $vtotal2= $vtotal2+$complexcountpenv[0];
                                            $tot1= $complexcountpenv[0]+$complexcountverv[0];
											 $vtotal= $vtotal+$tot1;  
                                              
                        }
                    }
                        $total2=$total2+$vtotal;
                        ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=1" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal1?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=0" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal2; ?></a></td>
						<td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $vtotal; ?></a></td>
                                           
                       <?php }  ?>
                        <td><a href="<?php echo SECURE_PATH;?>admindashboard/report.php?exam=8&year=<?php echo $rowll2v['year']; ?>&set=<?php echo $checksetv['id']; ?>&subject=<?php echo $row3v['id']; ?>&vstatus1=" style="float:center;cursor:pointer;" target="_blank"><?php echo $total2; ?></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
               
        </div>
    </div>
<?php
}
?>