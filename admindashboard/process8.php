<?php 
include('../include/session.php');
error_reporting(0);
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
if(isset($_REQUEST['pendingquestion'])){

    ?>
   <div class="card-header" id="pendingquestion">
		<a href="" class="d-flex justify-content-between align-items-center"
		   data-toggle="collapse" data-target="#pendingdiv" aria-expanded="true"
		   aria-controls="pendingdiv" >
			<h5 class="card-title mb-0"><b>Pending Question Range -Chapter Count</b></h5>
			<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
		</a>
	</div>
    <!-- <div class="card-header">
        <h5 class="m-0"><b>Timeline Overview</b></h5>
    </div> -->
    <div class="collapse show" id="pendingdiv"   aria-labelledby="pendingquestion" >
    
    <div class="tab-content p-2" id="pills-tabContent">
    <?php
    $con="";

    if($_REQUEST['exam']!=''){
            $con=" AND id IN (".$_REQUEST['exam'].")";
        }
		 $array = array('0' =>'0','1'=> '1 To 10','2' => '10 To 50','3' => '50 To 100','4' => '100 To 200','5' => '200 To 500','6' => 'More Than 500');

        ?>
        <div class="tab-pane fade show active" id="pills-hometime" role="tabpanel"
             aria-labelledby="pills-home-tabtime">
            <table class="table table-bordered">
                <thead class="bg-light">
					<tr>
						<th></th>
						<th></th>
						<?php
				 		 foreach($array as $key => $counts){
						?>
							<th class="text-center"><?php echo $counts; ?></th>
						<?php
						 }
						?>
					</tr>
                </thead>
                <tbody>
                <?php
                
			
				 $i=1;
				
				$sqlcc1=$database->query("select * from  subject where  estatus='1' ");

				while($rowcc1=mysqli_fetch_array($sqlcc1)){
					$sqlcc=$database->query("select * from  class where  estatus='1' ");

					while($rowcc=mysqli_fetch_array($sqlcc)){
				?>
					<tr>
						<td>
                            <h6 class="text-left"><?php echo $rowcc1['subject']; ?></h6>
                        </td>
						<td>
                            <h6><?php echo $rowcc['class']; ?></h6>
                        </td>
                        <?php
						
							foreach($array as $key => $counts1){
								$var=explode(" To ",$counts1);

								
								if($counts1 == 'More Than 500'){
									$sello=$database->query("select id from createquestion where estatus='1' and class in (".$rowcc['id'].") and subject='".$rowcc1['id']."' order by id desc limit 0,1");
									$rowllo=mysqli_fetch_array($sello);
									$id=$rowllo['id'];
									
									$var[0]=501;
									$var[1]=$id;
								}
								$k='';
								
								$sql=$database->query("select id,chapter from createquestion where estatus='1' and class in (".$rowcc['id'].") and subject='".$rowcc1['id']."' order by id asc limit ".$var[0].",".$var[1]." ");
								$rowcount=mysqli_num_rows($sql);
								if($rowcount>0){
									while($rowc=mysqli_fetch_array($sql)){
										
										$k.=$rowc['chapter'].",";
										
									}
								}
								
								$cha=explode(",",$k);
								$cha1=array_unique($cha);
								$chapids=implode(",",$cha1);
								$chap=explode(",",$chapids);
								//print_r($chap);
								$k1='';
								foreach($chap as $key => $chap2){
									$sqll1=$database->query("select * from chapter where estatus='1' and class='".$rowcc['id']."' and subject='".$rowcc1['id']."' and id='".$chap2."'"); 
									$rowcount1=mysqli_num_rows($sqll1);
									if($rowcount1>0){
										$k1.=$chap2.",";
									}
								}
								
								$chapter2=explode(",",$k1);
								$chapter12=array_unique($chapter2);
								$chapnos=implode(",",$chapter12);
								//$chapter_count=count($cha1);
								//$chapids=implode(",",$cha1);
								$count=count($chapter12)-1;
								?>
								<td><a class="text-success" style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal1" onClick="setStateEnc2('viewDetails1','<?php echo SECURE_PATH;?>admindashboard/process8.php','viewDetails1=1&chapter=<?php echo $chapnos;?>&range=<?php echo $counts1;?>');"><?php echo $count; ?></a></td>
                        <?php
							}
						?>
                       
                    </tr>
				<?php

					}
				}
                ?>


                </tbody>
            </table>
        </div>
        
        </div>
        </div>
    <?php
    ?>
<?php
}
?>
<?php
if(isset($_REQUEST['viewDetails1'])){

	if(strlen($_REQUEST['chapter'])>0){
		$chapter=explode(",",rtrim($_REQUEST['chapter'],","));
	}
	?>
		<table class="table table-bordered">
			  <thead>
				<tr>
				  <th scope="col" rowspan="2">Sln.</th>
				  <th scope="col" rowspan="2">Chapter</th>
				  <th scope="col" rowspan="2">Question Count</th>
					<th scope="col" colspan="2" class="text-center" >Assigned Users</th>
				</tr>
				<tr>
				  <th scope="col">Dataentry</th>
				  <th scope="col">Lecturer</th>
				 
				</tr>
			  </thead>
			  <tbody>
				
				<?php
					$i=1;
					foreach($chapter as $key => $chapterdata){
						$var=explode(" To ",$_REQUEST['range']);
						if($_REQUEST['range'] == 'More Than 500'){
							$sellor=$database->query("select id from createquestion where estatus='1' and  FIND_IN_SET(".$chapterdata.",chapter) > 0 order by id desc limit 0,1");
							$rowllo=mysqli_fetch_array($sellor);
							$var[0]=501;
							$var[1]=$rowllo['id'];
						}
						
						$sello=$database->query("select id from createquestion where estatus='1' and  FIND_IN_SET(".$chapterdata.",chapter) > 0  order by id desc limit ".$var[0].",".$var[1]." ");
						$rowtot=mysqli_num_rows($sello);
						
						
							
					?>
						<tr>
							<td class="text-left"><?php echo $i; ?></td>
							<td class="text-left"><?php echo $database->get_name('chapter','id',$chapterdata,'chapter'); ?></td>
							<td class="text-left"><?php echo $rowtot; ?></td>
							<td class="text-left">
							<?php 
							$j=1;
							$selusers=$database->query("select * from users where valid='1'  and FIND_IN_SET(".$chapterdata.",chapter)  > 0 and userlevel='7' ");
							while($rowusers=mysqli_fetch_array($selusers)){
								$user=$rowusers['username'];
								echo $j.".".$user."<br />";
								$j++;

							}
							?>
							</td>
							<td class="text-left">
							<?php 
							$j=1;
							$selusers=$database->query("select * from users where valid='1'  and FIND_IN_SET(".$chapterdata.",chapter)  > 0 and userlevel='3' ");
							while($rowusers=mysqli_fetch_array($selusers)){
								$user=$rowusers['username'];
								echo $j.".".$user."<br />";
								$j++;

							}
							?>
							</td>
							
						</tr>
				<?php	$i++; } ?>
				
				
			  </tbody>
			</table>
	<?php

}
?>


						
							