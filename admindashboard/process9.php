<?php 
include('../include/session.php');
error_reporting(0);

if(isset($_REQUEST['sverificationOverview'])){

    ?>


    <div class="card-header" id="sverification">
        <a href="" class="d-flex justify-content-between align-items-center"
           data-toggle="collapse" data-target="#sverifydiv" aria-expanded="true"
           aria-controls="sverifydiv" >
            <h5 class="card-title mb-0"><b>Subject Wise Overview</b></h5>
            <h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
        </a>
    </div>

    <div class="collapse show" id="sverifydiv"   aria-labelledby="sverification" >
        <div class="card-body">
            <div class="d-flex justify-content-between">
               
                    
                        <table class="table table-bordered <?php echo $style; ?> mb-0 bg-white">
                            <thead>
                            <tr>
                                <th class="bg-gray-color text-center">Subject</th>
								<th class="bg-gray-color text-center">Total</th>
								<th class="bg-gray-color text-center">Verified</th>
								<th class="bg-gray-color text-center">Deleted</th>
								<th class="bg-gray-color text-center">Rejected</th>
								<th class="bg-gray-color text-center">No Chapter & Topic</th>
								<th class="bg-gray-color text-center">No Topic</th>
								<th class="bg-gray-color text-center">Pending</th>

                            </tr>
                            </thead>
                            <tbody>
                          
                            <?php
                            $sqll2=$database->query("select * from subject where estatus='1'");
                            while($rowll2=mysqli_fetch_array($sqll2)){
								$dataetotal=$database->query("select count(id) as cnt from createquestion where   subject='".$rowll2['id']."' ");
								$rowdataetotal=mysqli_fetch_array($dataetotal);

								$dataetotalv=$database->query("select count(id)  as cnt from createquestion where estatus='1' and vstatus1='1' and   subject='".$rowll2['id']."' ");
								$rowdataetotalv=mysqli_fetch_array($dataetotalv);

								$dataetotald=$database->query("select count(id)  as cnt from createquestion where estatus='0' and   subject='".$rowll2['id']."' ");
								$rowdataetotald=mysqli_fetch_array($dataetotald);

								$dataetotalr=$database->query("select count(id)  as cnt from createquestion where estatus='1' and vstatus1='2' and   subject='".$rowll2['id']."' ");
								$rowdataetotalr=mysqli_fetch_array($dataetotalr);

								$dataetotalct=$database->query("select count(id)  as cnt from createquestion where estatus='1'  and   subject='".$rowll2['id']."' and chapter='' ");
								$rowdataetotalct=mysqli_fetch_array($dataetotalct);
								$dataetotalct=$database->query("select count(id)  as cnt from createquestion where estatus='1'  and   subject='".$rowll2['id']."' and chapter!='' and topic='' ");
								$rowdataetotalctopic=mysqli_fetch_array($rowdataetotalctopic);


								$dataetotalp=$database->query("select count(id)  as cnt from createquestion where estatus='1' and vstatus1='0' and   subject='".$rowll2['id']."' ");
								$rowdataetotalp=mysqli_fetch_array($dataetotalp);
                                ?>
                                <tr class="erification-items">


                                    <td><?php echo $rowll2['subject']; ?></td>
									<td><?php  if($rowdataetotal['cnt']!=''){ echo $rowdataetotal['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotalv['cnt']!=''){ echo $rowdataetotalv['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotald['cnt']!=''){ echo $rowdataetotald['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotalr['cnt']!=''){ echo $rowdataetotalr['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotalct['cnt']!=''){ echo $rowdataetotalct['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotalctopic['cnt']!=''){ echo $rowdataetotalctopic['cnt']; } else{ echo '0'; } ?></td>
									<td><?php  if($rowdataetotalp['cnt']!=''){ echo $rowdataetotalp['cnt']; } else{ echo '0'; } ?></td>
                                </tr>
                               
                            
                        <?php
                        $i++;
                    }

                ?>
					</tbody>
                  </table>
            </div>
        </div>
    </div>
<?php
}
?>