<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
$imagepath="https://admin.rizee.in/files/";
//$imagepath="http://localhost:81/neetjee/files/";
?>
<style type="text/css">
/*ul {
    margin: 20px;
}*/

.input-color {
    position: relative;
}
/*.input-color input {
    padding-left: 20px;
}*/

.input-color .color-box {
    width: 14px;
    height: 12px;
    display: inline-block;
    background-color: #ccc;
/*    position: absolute;
    left: 5px;
    top: 5px;*/
}
.image{
	top:auto;
	width:50 !important;
}
.table.table-bordered img {
     width: 90px !important; 
    height: auto;
}
</style>
<script>

 $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

</script>
<script>

$('#is_expired').selectpicker2();
$('#plan_id').selectpicker1();
('#exam_id').selectpicker3();
</script>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
		?>
	 <script>
		$('html, body').animate({
		   scrollTop: $("#myDiv").offset().top
	   }, 2000);
	  </script>
	 <?php
    $data_sel = $database->query1("SELECT * FROM home_banners WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	
	//$path1=explode($imagepath,$_POST['image']);
	$_POST['image']=str_replace($imagepath,"",$data['image']);
	if($_POST['app_action_id']=='9'){
		$_POST['plan_id']=$_POST['target_id'];
		$_POST['video_url']='';
	}else if($_POST['app_action_id']=='13'){
		$_POST['plan_id']='';
		$_POST['video_url']=$_POST['target_id'];
	}else{
		$_POST['plan_id']='';
		$_POST['video_url']='';
	}
	
	$_POST['app_actions']=$_POST['action_type'];

	
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>

  <div class="col-lg-12 col-md-12" id="myDiv">
 
 	<div class="row">
	 	
	
		<div class="col-lg-6">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Image<span style="color:red;">*</span></label>
					<div class="col-sm-8">
						

						<div id="file-uploader2" style="display:inline">
				<noscript>
					<p>Please enable JavaScript to use file uploader.</p>
					<!-- or put a simple form for upload here -->
				</noscript>

			</div>
			<script>

				function createUploader(){

					var uploader = new qq.FileUploader({
						element: document.getElementById('file-uploader2'),
						action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=image&width=480&height=640&upload_type=single',
						debug: true,
						multiple:false
					});
				}

				createUploader();



				// in your app create uploader as soon as the DOM is ready
				// don't wait for the window to load

			</script>
						<input type="hidden" name="image" id="image" value="<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>"/>
						<?php
						if(isset($_POST['image']))
						{
							if($_POST['image']!=''){
						?>
								<img src="<?php echo SECURE_PATH.'files/'.$_POST['image'];?>" style="height:50px; width:60px;" />
						<?php
							}

						}
						?>

						<span class="error text-danger"  ><?php if(isset($_SESSION['error']['image'])){ echo $_SESSION['error']['image'];}?></span>
					</div>
				</div>
			
			</div>
			<div class="col-lg-6">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Sort Order<span style="color:red;">*</span></label>
					<div class="col-sm-8">
						<input type="text" class="form-control" name="sort_order" id="sort_order" value="<?php if(isset($_POST['sort_order'])) { echo $_POST['sort_order']; }else{} ?>" autocomplete="off" >
						<span class="error text-danger"><?php if(isset($_SESSION['error']['sort_order'])){ echo $_SESSION['error']['sort_order'];}?></span>
					</div>
				</div>
			</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select class="form-control selectpicker3"  multiple data-actions-box="true" data-live-search="true" name="exam_id" value=""  id="exam_id"  >
						<?php
						
						$sel=$database->query("select * from exam where estatus='1' ORDER by id asc");
						while($row=mysqli_fetch_array($sel)){
							
							?>
								
								<option value="<?php echo $row['id'];?>" <?php if(isset($_POST['exam_id'])) { if(in_array($row['id'], explode(",",$_POST['exam_id']))) { echo 'selected="selected"'; } } ?>   ><?php echo $row['exam'];?></option>
						<?php
							
						}
						?>
				</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_id'])){ echo $_SESSION['error']['exam_id'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Action Type</label>
				<div class="col-sm-8">
					<select class="form-control" name="app_actions" value=""   id="app_actions"  onchange="actiontypedata();">
						<option value=''>-- Select --</option>
						<option value="1" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==1) { echo 'selected="selected"'; }  } ?>>Learn</option>
						<option value="2" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==2) { echo 'selected="selected"'; }  } ?>>Practice</option>
						
						<option value="3" <?php if(isset($_POST['app_actions'])) { if($_POST['app_actions']==3) { echo 'selected="selected"'; }  } ?>>Action</option>
						
						
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['app_actions'])){ echo $_SESSION['error']['app_actions'];}?></span>
				</div>
			</div>
		</div>
		<?php
			if(isset($_POST['app_actions'])){
				if($_POST['app_actions']!=''){
					if($_POST['app_actions']=='1' || $_POST['app_actions']=='2'){
						$styled='';
						$styled1='style="display:none;"';
						$styled2='style="display:none;"';
						$styled3='style="display:none;"';
					}else if($_POST['app_actions']=='3'){
						if($_POST['app_action_id']=='9'){
							$styled2='';
							$styled3='style="display:none;"';
						}else if($_POST['app_action_id']=='13'){
							$styled2='style="display:none;"';
							$styled3='';
						}else{
							$styled2='style="display:none;"';
							$styled3='style="display:none;"';
						}
						
						$styled='style="display:none;"';
						$styled1='';
						
					}else{
						$styled='style="display:none;"';
						$styled1='style="display:none;"';
						$styled2='style="display:none;"';
						$styled3='style="display:none;"';
					}
				}else{
					$styled='style="display:none;"';
					$styled1='style="display:none;"';
					$styled2='style="display:none;"';
					$styled3='style="display:none;"';
				}
			}else{
				$styled='style="display:none;"';
				$styled1='style="display:none;"';
				$styled2='style="display:none;"';
				$styled3='style="display:none;"';
			}
			?>
		<div class="col-lg-6 actiondata" <?php echo $styled; ?> >
			
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Class</label>
				<div class="col-sm-8">
					<select class="form-control " name="class" value=""  id="class" >
						<option value=''>-- Select --</option>
						<?php
						$selexam =$database->query("select * from class where estatus='1' ");
						
						while($data = mysqli_fetch_array($selexam))
						{
							?>
						<option value="<?php echo $data['id'];?>"   <?php if(isset($_POST['class'])) { if($_POST['class']==$data['id']) { echo 'selected="selected"'; }  } ?> ><?php echo $data['class'];?></option>
						
							<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
				</div>
			</div>
			
		</div>
		<div class="col-lg-6 actiondata"  <?php echo $styled; ?>>
			<div class="form-group row" id="aaa">
				<label class="col-sm-4 col-form-label">Subject</label>
				<div class="col-sm-8">
				<select class="form-control" name="subject" value=""   id="subject" onChange="setState('bbb','<?php echo SECURE_PATH;?>app_banners/ajax1.php','getsubchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'');">
					<option value=''>-- Select --</option>
					<?php
					$row = $database->query("select * from subject where estatus='1'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
					<?php
					}
					?>
				</select>
					<span class="error"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 actiondata" <?php echo $styled; ?>>
			<div class="form-group row" id="bbb">
				<label class="col-sm-4 col-form-label">Chapter</label>
				<div class="col-sm-8">
				<select class="form-control" name="chapter" value=""   id="chapter" onChange="setState('ccc','<?php echo SECURE_PATH;?>app_banners/ajax1.php','getsubtopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'');">
						<option value=''>-- Select --</option>
						<?php
						$row = $database->query("select * from chapter where estatus='1'");
						
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if($_POST['chapter']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 actiondata" <?php echo $styled; ?>>
			<div class="form-group row" id="ccc">
				<label class="col-sm-4 col-form-label">Topic</label>
				<div class="col-sm-8">
				<select class="form-control" name="topic" value=""   id="topic" >
						<option value=''>-- Select --</option>
						<?php
						$row = $database->query("select * from topic where estatus='1'");
						
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['topic'])) { if($_POST['topic']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['topic'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 actiondata1" <?php echo $styled1; ?> id="actiondata1">
			<div class="form-group row" >
				<label class="col-sm-4 col-form-label">App Action Data<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					
					<select id="app_action_id" class="form-control" name="app_action_id" onchange="actiontypedata1();">
						<option value="">-Select-</option>
						<?php
						$sql=$database->query1("select * from app_modules where estatus='1'");
						while($row=mysqli_fetch_array($sql)){
						?>
							<option value="<?php echo $row['id'];?>" <?php if(isset($_POST['app_action_id'])) { if(in_array($row['id'], explode(",",$_POST['app_action_id']))) { echo 'selected="selected"'; }  } ?>><?php echo $row['title'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['app_action_id'])){ echo $_SESSION['error']['app_action_id'];}?></span>
				</div>
			</div>
		</div>

		<div class="col-lg-6 actiondata2" <?php echo $styled2; ?> id="actiondata2">
			<div class="form-group row" >
				<label class="col-sm-4 col-form-label">Plan Id</label>
				<div class="col-sm-8">
					
					<select id="plan_id" class="form-control" name="plan_id" >
						<option value=''>-Select-</option>
						<?php
						$sql=$database->query1("select * from student_plans where estatus='1'");
						while($data=mysqli_fetch_array($sql)){
						?>
							
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['plan_id'])) { if($_POST['plan_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['plan_name'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['plan_id'])){ echo $_SESSION['error']['plan_id'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6 actiondata3" <?php echo $styled3; ?> id="actiondata3">
			<div class="form-group row" >
				<label class="col-sm-4 col-form-label">Video URL</label>
				<div class="col-sm-8">
					<select id="video_url" class="form-control" name="video_url" >
						<option value=''>-Select-</option>
						<?php
						$sql=$database->query("select * from videos where estatus='1' and publish='1'");
						while($data=mysqli_fetch_array($sql)){
						?>
							
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['video_url'])) { if($_POST['video_url']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['title'];?></option>
						<?php
						}
						?>
					</select>
					
					<span class="error"><?php if(isset($_SESSION['error']['video_url'])){ echo $_SESSION['error']['video_url'];}?></span>	
						
				</div>
			</div>
		</div>
		
		
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
			<label></label>
				<div class="col-sm-8">
					<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>app_banners/process.php','validateForm=1&image='+$('#image').val()+'&sort_order='+$('#sort_order').val()+'&app_actions='+$('#app_actions').val()+'&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&app_action_id='+$('#app_action_id').val()+'&plan_id='+$('#plan_id').val()+'&video_url='+$('#video_url').val()+'&exam_id='+$('#exam_id').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
					</div>
			</div>
		</div>
	</div>
	
</div>

<?php
unset($_SESSION['error']);
}
?>
<?php
if(isset($_GET['rowDelete'])){
	//$database->query("DELETE FROM chapter WHERE id = '".$_GET['rowDelete']."'");
	$database->query1("update  home_banners set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Home Card  deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>app_banners/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>
<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	
	 $field = 'image';
	if(!$post['image'] || strlen(trim($post['image'])) == 0){
	  $_SESSION['error'][$field] = "* Image cannot be empty";
	}
	 $field = 'sort_order';
	if(!$post['sort_order'] || strlen(trim($post['sort_order'])) == 0){
	  $_SESSION['error'][$field] = "* Sort Order cannot be empty";
	}
	 $field = 'exam_id';
	if(!$post['exam_id'] || strlen(trim($post['exam_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam Type cannot be empty";
	}
	if(count($_SESSION['error']) > 0 || $post['validateFormc'] == 2){
		$desc=str_replace("'","%27",$post['short_description']);
		$title=str_replace("'","%27",$post['title']);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
	  
      setState('adminForm','<?php echo SECURE_PATH;?>app_banners/process.php','addForm=1&class=<?php echo $post['class'];?>&image=<?php echo $post['image'];?>&subject=<?php echo $post['subject'];?>&chapter=<?php echo $post['chapter'];?>&exam_id=<?php echo $post['exam_id'];?>&topic=<?php echo $post['topic'];?>&app_actions=<?php echo $post['app_actions'];?>&app_action_id=<?php echo $post['app_action_id'];?>&plan_id=<?php echo $post['plan_id'];?>&video_url=<?php echo $post['video_url'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		if($post['exam_id']!=''){
				$exam_id=rtrim($post['exam_id'],",");
			}else{
				$exam_id="";
			}
		if($id=='NULL')
		{
			$image=$imagepath.$post['image'];
			if($post['app_action_id']!=''){
				$app_action_id=$post['app_action_id'];
				if($post['app_action_id']=='9'){
					$target_id=$post['plan_id'];
				}else if($post['app_action_id']=='13'){
					$target_id=$post['video_url'];
				}else{
					$target_id='0';
				}
			}else{
				$app_action_id=0;
				$target_id='0';
			}
			

			
			$result=$database->query1("insert home_banners set image='".$image."',action_type='".$post['app_actions']."',class='".$post['class']."',subject='".$post['subject']."',chapter='".$post['chapter']."',topic='".$post['topic']."',app_action_id='".$app_action_id."',target_id='".$target_id."',exam_id='".$exam_id."',sort_order='".$post['sort_order']."',estatus='1',timestamp='".time()."'");
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>Home Card Created Successfully!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>app_banners/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			
		}else{
			$image=$imagepath.$post['image'];
			if($post['app_action_id']!=''){
				$app_action_id=$post['app_action_id'];
				if($post['app_action_id']=='9'){
					$target_id=$post['plan_id'];
				}else if($post['app_action_id']=='13'){
					$target_id=$post['video_url'];
				}else{
					$target_id='0';
				}
			}else{
				$app_action_id=0;
				$target_id='0';
			}


			$result=$database->query1("update home_banners set image='".$image."',action_type='".$post['app_actions']."',class='".$post['class']."',subject='".$post['subject']."',chapter='".$post['chapter']."',topic='".$post['topic']."',app_action_id='".$app_action_id."',target_id='".$target_id."',exam_id='".$exam_id."',sort_order='".$post['sort_order']."' where id='".$id."'");
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> App Home Banner Updated Successfully</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>app_banners/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
		}
	}
	

?>

 
	<?php
	

}
?>
<?php


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	?>
	
	<?php
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'home_banners';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."app_banners/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query1($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." ";
  
  $data_sel = $database->query1($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
 <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50,
    "order": [[ 1, 'asc' ]]
    
  });

</script>
	

  	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">App Home Cards</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered dashboard-table" width="100%" cellspacing="0"  id="dataTable">
									<thead>
										<tr>
											<th class="text-left">Sr.No.</th>
											<th class="text-left">Image</th>
											<th class="text-left">Sort Order </th>
											<th class="text-left">App Action Type</th>
											<th class="text-left">Action Data</th>
											<th class="text-left">Exam</th>
											<th class="text-left">Subject</th>
											<th class="text-left">Chapter</th>
											<th class="text-left">Topic</th>
											<th class="text-left">Actions</th>
											
										</tr>
									</thead>
									<tbody>
									<?php
									
									$k=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										$time=$value['start_time'];
										$time1=$value['end_time'];
										if($value['action_type']=='1'){
											$actions="Learn";
											$action_id='-';
											$sub=$database->query("select * from subject where estatus='1' and id='".$value['subject']."'");
											$rowsub=mysqli_fetch_array($sub);
											$chap=$database->query("select * from chapter where estatus='1' and id='".$value['chapter']."'");
											$rowchap=mysqli_fetch_array($chap);
											$top=$database->query("select * from topic where estatus='1' and id='".$value['topic']."'");
											$rowtopic=mysqli_fetch_array($top);
											$subject=$rowsub['subject'];
											$chapter=$rowchap['chapter'];
											$topic=$rowtopic['topic'];

										}else if($value['action_type']=='2'){
											$actions="Practice";
											$action_id='';
											$sub=$database->query("select * from subject where estatus='1' and id='".$value['subject']."'");
											$rowsub=mysqli_fetch_array($sub);
											$chap=$database->query("select * from chapter where estatus='1' and id='".$value['chapter']."'");
											$rowchap=mysqli_fetch_array($chap);
											$top=$database->query("select * from topic where estatus='1' and id='".$value['topic']."'");
											$rowtopic=mysqli_fetch_array($top);
											$subject=$rowsub['subject'];
											$chapter=$rowchap['chapter'];
											$topic=$rowtopic['topic'];
										}else if($value['action_type']=='3'){
											$actions="App Action";
											$sell1=$database->query1("select * from app_modules where estatus='1' and id='".$value['app_action_id']."'");
											$rowll1=mysqli_fetch_array($sell1);
											$action_id=$rowll1['title'];
											$subject='-';
											$chapter='-';
											$topic='-';
										}
										$exam='';
										$sqlexam=$database->query("select id,exam from exam where estatus='1' and id in (".$value['exam_id'].")");
										while($rowexam=mysqli_fetch_array($sqlexam)){
											$exam.=$rowexam['exam'].",";
										}
                  					?>
										<tr>
											<td><?php echo $k; ?></td>
											<td ><img src="<?php echo $value['image']; ?>" /></td>
											<td><?php echo $value['sort_order']; ?></td>
											<td><?php echo $actions; ?></td>
											<td><?php echo $action_id; ?></td>
											<td><?php echo rtrim($exam,","); ?></td>
											<td><?php echo $subject; ?></td>
											<td><?php echo $chapter; ?></td>
											<td><?php echo $topic; ?></td>
											<td> <a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>app_banners/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;<a href="#" class="text-danger" onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>app_banners/process.php','rowDelete=<?php echo $value['id'];?>')"><i
												  class="material-icons md-16 pt-1">delete</i></a></td>
										</tr>
									<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
  </section>
  
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
?>
	

   
   <br  />
   		<?php
if(isset($_REQUEST['validateForm1'])){
			if($_REQUEST['status']=='enablestatus'){
				$result=query("update  referral_codes  set status='1' where  id='".$_REQUEST['editform']."'");
			
				if($result){
					
					?>
					

					<script type="text/javascript">
					
						alert("User Enabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>referralcodes/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
						
						
					</script>
					<?php
				}

		}else if($_REQUEST['status']=='disablestatus'){
			
			$result=query("update  referral_codes  set status='0' where  id='".$_REQUEST['editform']."'");
			if($result){
				
				?>

				<script type="text/javascript">
				
					alert("User Disabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>referralcodes/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
					
				</script>
				
				<?php
			}
		}
		

	}

?>
<script>
function actiontypedata(){
	var action=$('#app_actions').val();
	
	if(action=='1' || action=='2'){
		$('.actiondata').show();
		$('.actiondata1').hide();
		$('.actiondata2').hide();
		$('.actiondata3').hide();
	}else if(action=='4'){
		$('.actiondata').hide();
		$('.actiondata1').hide();
		$('.actiondata2').show();
		$('.actiondata3').hide();
	}else if(action=='5'){
		$('.actiondata').hide();
		$('.actiondata1').hide();
		$('.actiondata2').hide();
		$('.actiondata3').show();

	}else{
		$('.actiondata1').show();
		$('.actiondata').hide();
		$('.actiondata2').hide();
		$('.actiondata3').hide();
	}
}

function actiontypedata1(){
	var action=$('#app_action_id').val();
	
	if(action=='9'){
		$('.actiondata2').show();
		$('.actiondata3').hide();
	}else if(action=='13'){
		$('.actiondata2').hide();
		$('.actiondata3').show();

	}else{
		$('.actiondata2').hide();
		$('.actiondata3').hide();
	}
}


 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
</script>




