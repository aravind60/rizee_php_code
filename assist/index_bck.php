<?php
include('../include/session.php');
$conn=mysqli_connect('localhost','root','','qsbank') ;

function query($sql)
{
    global $conn;
	return mysqli_query($conn,$sql);
}
$conn1=mysqli_connect('localhost','root','','neetjee') ;
function query1($sql)
{
    global $conn1;
	return mysqli_query($conn1,$sql);
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Rizee - The Perfect Guide </title>

    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style1.css">
    <link rel="stylesheet" href="css/assist-your.self.css">

    <link rel="stylesheet" href="css/responsive.css">
</head>

<body>
	<script type="text/javascript">
	
    function validate(){
		 var count_err = 0;
		 
		if(($('#firstname').length)==0 || $('#firstname').val()=='' || $('#firstname').val()=='0'){
            count_err=count_err+1;
           $('#stufirstname_error').html("Firstname can not be empty");
			$('#stufirstname_error').css("color","red");
			$('#stufirstname_error').css("font-size",10);
        }else{ $('#stufirstname_error').hide();}


		if(($('#lastname').length)==0 || $('#lastname').val()=='' || $('#lastname').val()=='0'){
            count_err=count_err+1;
          $('#stulastname_error').html("Lastname can not be empty");
				$('#stulastname_error').css("color","red");
				$('#stulastname_error').css("font-size",10);
        }else{ $('#stulastname_error').hide();}
	if(($('#mobile').length)==0 || $('#mobile').val()=='' || $('#mobile').val()=='0'){
            count_err=count_err+1;
			 $('#stumobileno_error').html("Mobile No. can not be empty");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
        }else{ $('#stumobileno_error').hide();}

		if(($('#email').length)==0 || $('#email').val()=='' || $('#email').val()=='0'){
            count_err=count_err+1;
           $('#stusubjecterror1').html("Subject can not be empty");
			$('#stusubjecterror1').css("color","red");
			$('#stusubjecterror1').css("font-size",10);
        }else{ $('#stuemail_error').hide();}
	
		var examtype=$('#examtype').val();
		var chapter=$('#chapter').val();
		
		if(examtype=='1'){
			
			if(($('#subject1').length)==0 || $('#subject1').val()=='' || $('#subject1').val()=='0'){
				count_err=count_err+1;
				$('#stusubjecterror1').show();
				$('#stusubjecterror1').html("Subject can not be empty");
				$('#stusubjecterror1').css("color","red");
				$('#stusubjecterror1').css("font-size",10);
			}else{ $('#stusubjecterror1').hide();}
		}else if(examtype=='2'){
			
			if(($('#subject').length)==0 || $('#subject').val()=='' || $('#subject').val()=='0'){
				count_err=count_err+1;
				$('#stusubjecterror').show();
				$('#stusubjecterror').html("Subject can not be empty");
				$('#stusubjecterror').css("color","red");
				$('#stusubjecterror').css("font-size",10);
			}else{ $('#stusubjecterror').hide();}
		
			if(($('#chapter').length)==0 || $('#chapter').val()=='' || $('#chapter').val()=='0'){
				count_err=count_err+1;
				$('#stuchaptererror').show();
				$('#stuchaptererror').html("Chapter can not be empty");
				$('#stuchaptererror').css("color","red");
				$('#stuchaptererror').css("font-size",10);
			}else{ $('#stuchaptererror').hide();}
		}else{

			if(($('#previous_set').length)==0 || $('#previous_set').val()=='' || $('#previous_set').val()=='0'){
				count_err=count_err+1;
				$('#questionseterror').show();
				$('#questionseterror').html("Chapter can not be empty");
				$('#questionseterror').css("color","red");
				$('#questionseterror').css("font-size",10);
			}else{ $('#questionseterror').hide();}
		}

		if(($('#otp').length)==0 || $('#otp').val()=='' || $('#otp').val()=='0'){
				count_err=count_err+1;
				$('#otperror').show();
				$('#otperror').html("Please enter OTP");
				$('#otperror').css("color","red");
				$('#otperror').css("font-size",10);
			}else{ $('#otperror').hide();}
		
		if(count_err==0){
			var firstname = $('#firstname').val();
            var lastname=$('#lastname').val();
			var email = $('#email').val();
            var mobile=$('#mobile').val();
			var exam = $('#exam').val();
			var examtype = $('#examtype').val();
            var class1=$('#class').val();
			var subject = $('#subject').val();
            var subject1=$('#subject1').val();
			var chapter = $('#chapter').val();
            var previous_set=$('#previous_set').val();
			 var otp=$('#otp').val();
			var datas = `validateform=1&firstname=${firstname}&lastname=${lastname}&email=${email}&mobile=${mobile}&exam=${exam}&examtype=${examtype}&class=${class1}&subject=${subject}&subject1=${subject1}&chapter=${chapter}&previous_set=${previous_set}&otp=${otp}`;
			
			console.log(datas);
                    $htmlObj=$.ajax({
                    type:"POST",
                    url:"http://localhost:81/neetjee/assist-your-self/ajax.php",
                    ifModified:true,
                    dataType:"html",
                    data: datas,
                    cache: false,		 
                    success: function(result) {
                          console.log("Success");
                        }
                    });
		}
        
    }
</script>
    <!-- /.preloader -->
    <div class="page-wrapper">
        <header class="site-header header-one ">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="index.html">
                            <img src="images/logo.png" class="main-logo" width="97" alt="Awesome Image" />
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box one-page-scroll-menu ">
                            <li class="current scrollToLink">
                                <a href="../index.html">Home</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="../index.html#services">Features</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="../index.html#screens">App View</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="https://rizee.in/support/">Support</a>
                            </li>
                            <!-- <li class="scrollToLink">
                                <a href="https://rizee.in/registrations/">Registrations</a>
                            </li> -->
                            <li class="scrollToLink">
                                <a href="https://rizee.in/blog/">Blog</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="../index.html#pricing">Registrations</a>
                            </li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <a href="https://play.google.com/store/apps/details?id=in.rizee.mylearningplus"
                            class="thm-btn header-one__btn">try now</a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.header-one -->

        <section class="support-banner">
            <div class="container" id="myForm">
                <div class="row">
                    <div class="col-lg-4 offset-lg-4 mb-4">
                        <h3 class="support-title">Assist your self</h3>
                        <p>Assist now to check your readiness to competitive exams.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="white-block">
                            <form class="assist-form" action="">
                                <div class="row">
                                    <div class="col-md-6 mb-2">
                                        <label for="firstName" class="form-label">First name</label>
                                        <input type="text" class="form-control" id="firstname" placeholder="" value=""  onkeyup="stufirstnameCheck();"
                                            required autocomplete="off">
                                        <div id="stufirstname_error" ></div>
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label for="lastName" class="form-label">Last name</label>
                                        <input type="text" class="form-control" id="lastname" placeholder="" value=""  onkeyup="stulastnameCheck();"
                                            required autocomplete="off">
                                        <div id="stulastname_error"  ></div>
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label for="Mobile" class="form-label">Mobile</label>
                                        <input type="text" class="form-control" id="mobile" placeholder="" value="" onkeyup="stumobilenoCheck();"  onkeypress="return isNumber(event,$(this),10)"  maxlength="10"
                                            required autocomplete="off">
                                        <div id="stumobileno_error"> </div>
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label for="lastName" class="form-label">Email</label>
                                        <input type="text" class="form-control" id="email" placeholder="" value="" onkeyup="stuemailCheck();" onchange="emailsendfunction();"
                                            required autocomplete="off">
                                        <div  id="stuemail_error"></div>
                                    </div>
                                    <div class="col-md-6 mb-2">
                                        <label class="form-label">Exam</label>
                                        <ul class="list-inline support-app-buttons getStudentAppCls">
                                            <li class="active" attr_type="NEET" 
                                                    onclick="studentexamtype('1')">NEET</li>
                                            <li attr_type="JEE" 
                                                    onclick="studentexamtype('2')">JEE</li>
                                        </ul>
                                    </div>
									<input type="hidden" name="exam" id="exam" value="1">
                                    <div class="col-md-6 mb-2">
                                        <label class="form-label">Year</label>
                                        <ul class="list-inline support-app-buttons getStudentAppCls">
                                            <li class="active" attr_type="All" 
                                                    onclick="studentexamyear('1,2')">All YEARS</li>
                                            <li attr_type="1" 
                                                    onclick="studentexamyear('1')">1ST YEAR</li>
                                            <li attr_type="2" 
                                                    onclick="studentexamyear('2')">2ND YEAR</li>
                                        </ul>
                                    </div>
									<input type="hidden" name="class" id="class" value="1,2">
                                </div>
                                <div class="assist-form-tabs">
                                    <div class="row">
                                        <div class="col-md-6 mb-2">
                                            <label class="form-label">Exam</label>
                                            <ul class="nav nav-pills nav-tabs" id="myTab" role="tablist">
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link active" id="chapter-tab" data-toggle="tab"
                                                        href="#chapter2" role="tab" aria-controls="chapter2"
                                                        aria-selected="true" onclick="studentexamtype1('2')">CHAPTER</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="subject-tab" data-toggle="tab"
                                                        href="#subject2" role="tab" aria-controls="subject2"
                                                        aria-selected="false" onclick="studentexamtype1('1')">SUBJECT</a>
                                                </li>
                                                <li class="nav-item" role="presentation">
                                                    <a class="nav-link" id="preiouspaper-tab" data-toggle="tab"
                                                        href="#preiouspaper" role="tab" aria-controls="preiouspaper"
                                                        aria-selected="false" onclick="studentexamtype1('3')">PREIOUS PAPER</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
									<input type="hidden" name="examtype" id="examtype" value="2">
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" id="chapter2" role="tabpanel"
                                            aria-labelledby="chapter-tab">
                                            <div class="row">
                                                <div class="col-md-6 mb-3">
                                                    <label for="selectSubject" class="form-label">Subject</label>
                                                    <select class="custom-select d-block w-100" id="subject"
                                                       onchange="setState('chapterdiv','<?php echo SECURE_PATH;?>assist-your-self/ajax.php','getchapterid=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'')" >
														<option value=''>-Select-</option>
                                                        <?php
														$sqlsubject=query("select * from subject where estatus='1'");
														while($rowsubject=mysqli_fetch_array($sqlsubject)){
															?>
															<option value="<?php echo $rowsubject['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$rowsubject['id']) { echo 'selected="selected"'; }  } ?> ><?php echo $rowsubject['subject']; ?></option>
															<?php
														}
														?>
                                                    </select>
                                                    <div id="stusubjecterror"></div>
                                                </div>
                                                <div class="col-md-6 mb-3" id="chapterdiv">
                                                    <label for="selectChapter" class="form-label">Chapter</label>
                                                    <select class="custom-select d-block w-100" id="chapter"
                                                        required>
                                                       <option value=''>-Select-</option>
                                                        <?php
														$sqlchapter=query("select * from chapter where estatus='1'");
														while($rowchapter=mysqli_fetch_array($sqlchapter)){
															?>
															<option value="<?php echo $rowchapter['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if($_REQUEST['chapter']==$rowchapter['id']) { echo 'selected="selected"'; }  } ?> ><?php echo $rowchapter['chapter']; ?></option>
															<?php
														}
														?>
                                                    </select>
                                                    <div id="stuchaptererror"></div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="subject2" role="tabpanel"
                                            aria-labelledby="subject-tab">
                                            <div class="row">
                                               <div class="col-md-6 mb-3">
                                                    <label for="selectSubject" class="form-label">Subject</label>
                                                    <select class="custom-select d-block w-100" id="subject1"
                                                       >
														<option value=''>-Select-</option>
                                                        <?php
														$sqlsubject=query("select * from subject where estatus='1'");
														while($rowsubject=mysqli_fetch_array($sqlsubject)){
															?>
															<option value="<?php echo $rowsubject['id'];?>" <?php if(isset($_REQUEST['subject1'])) { if($_REQUEST['subject1']==$rowsubject['id']) { echo 'selected="selected"'; }  } ?> ><?php echo $rowsubject['subject']; ?></option>
															<?php
														}
														?>
                                                    </select>
                                                    <div id="stusubjecterror1"></div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="preiouspaper" role="tabpanel"
                                            aria-labelledby="preiouspaper-tab">
                                            <div class="row">
                                                <div class="col-md-6 mb-3">
                                                    <label for="selectPreiousPaper" class="form-label">Preious
                                                        Paper</label>
                                                    <select class="custom-select d-block w-100" id="previous_set"
                                                        required>
                                                        <option value=''>-Select-</option>
                                                        <?php
														$sqlsubject=query("select * from previous_sets where estatus='1'");
														while($rowsubject=mysqli_fetch_array($sqlsubject)){
															$sqlsubject1=query("select * from previous_questions where estatus='1' and id='".$rowsubject['pid']."'");
															$rowsubject1=mysqli_fetch_array($sqlsubject1);
															?>
															<option value="<?php echo $rowsubject['id'];?>" <?php if(isset($_REQUEST['previous_set'])) { if($_REQUEST['previous_set']==$rowsubject['id']) { echo 'selected="selected"'; }  } ?> ><?php echo $rowsubject1['year']." - ".$rowsubject['qset']; ?></option>
															<?php
														}
														?>
                                                    </select>
                                                    <div id="questionseterror"></div>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
								<div class="row">
                                    <div class="col-md-6 mb-2">
										<label for="subjectOTP" class="form-label">OTP</label>
										<input type="text" class="form-control" id="otp"
											placeholder="" value="" required maxlength="6" autocomplete="off">
										<small class="text-primary">Resend OTP in 0:36 Sec</small>
										<div id="otperror"></div>
									</div>
									<div class="col-md-6 mb-2">
										<label for="note" class="form-label">Note :<br />Please check you
											email inbox or messages for OTP</label>
									</div>
								</div>
                                <div class="form-group text-center">
                                   <button type="button" class="btn text-white btn-primry py-2 px-5 rounded" onclick="validate()" >submit &amp; Start exam</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="support-footer">
            <p class="site-footer__copy-text text-center mt-2"><i class="fa fa-copyright"></i>Copyright 2020 by <a
                    href="//www.rizee.in">Rizee</a></p>
        </footer><!-- /.site-footer -->




        <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
        <!-- /.scroll-to-top -->
        <script src="js/jquery.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/waypoints.min.js"></script>
        <script src="js/jquery.counterup.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/swiper.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/jquery.easing.min.js"></script>
        <script src="js/chosen.jquery.js"></script>
        <script src="js/theme.js"></script>
		 <script src="js/ajaxfunction.js"></script>
		  <script src="js/nprogress.js"></script>


		 <script type="text/javascript">
        $(".chosen-select").chosen();
        $(document).on("click",".getChangeCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            $(".getStudentAppCls li").removeClass("active")
	        $(".getStudentAppCls li:nth-child(1)").addClass("active");	
            var type = $(this).attr("attr_type");
            if(type == "sales"){
                $(".salesBlockCls").show();
                $(".supportBlockCls").hide();
            }else{
                $(".salesBlockCls").hide();
                $(".supportBlockCls").show();
            }
        });
        
        $(document).on("click",".getStudentAppCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            $(".getIssueTypeCls li").removeClass("active")
	        $(".getIssueTypeCls li:nth-child(1)").addClass("active");	
            var type = $(this).attr("attr_type");

            if(type == "student"){
                $(".supportStudentAppBlockCls").show();
                $(".supportInsAppBlockCls").hide();
            }else{
                $(".supportStudentAppBlockCls").hide();
                $(".supportInsAppBlockCls").show();
            }
        });
        $(document).on("click",".getIssueTypeCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            var activeClass= $(".getStudentAppCls").find(".active").attr("attr_type");
            var type = $(this).attr("attr_type");

            if(activeClass == "student"){
                if(type == "technical"){
                    $(".FMStudentAppCls").show();
                    $(".transationStudentAppCls").hide();
                }else{
                    $(".FMStudentAppCls").hide();
                    $(".transationStudentAppCls").show();
                }
            }else{
                if(type == "technical"){
                    $(".FMInsAppCls").show();
                    $(".transactionInsAppCls").hide();
                }else{
                    $(".FMInsAppCls").hide();
                    $(".transactionInsAppCls").show();
                }
            }

        });
        
    </script>
</body>

</html>

<script>
	function studentexamtype(val){
		if(val=='1'){
			$("#exam").val("1");
		}else if(val=='2'){
			$("#exam").val("2");
		}

	}
	function studentexamyear(value){
		$("#class").val(value);

	}

	function studentexamtype1(val){
		
		$("#examtype").val(val);
		

	}


	function stufirstnameCheck(){
		var stufirstnameval = $('#firstname').val();
		
		if(stufirstnameval.length==0){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname should not be empty");
			 $('#stufirstname_error').css("color","red");
             $('#stufirstname_error').css("font-size",10);
		}else if(stufirstnameval.length < 4 || stufirstnameval.length > 20){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname name must be between 4 and 20 characters");
			 $('#stufirstname_error').css("color","red");
                    $('#stufirstname_error').css("font-size",10);
		}else
		{
			$('#stufirstname_error').hide();
		}

	}

	function stulastnameCheck(){
		var stufirstnameval = $('#firstname').val();
		
		if(stufirstnameval.length==0){
			$('#stulastname_error').show();
			$('#stulastname_error').html("Last should not be empty");
			 $('#stulastname_error').css("color","red");
                    $('#stulastname_error').css("font-size",10);
		}else
		{
			$('#stulastname_error').hide();
		}

	}
	function stuemailCheck(){
		var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
		);
		var stuemail_errorval = $('#email').val();
		if(stuemail_errorval.length < 4){
			$('#stuemail_error').show();
			$('#stuemail_error').html("email cannot be less than 5 chars");
			$('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		}else if(!pattern.test(stuemail_errorval)){
			$('#stuemail_error').show();
			$('#stuemail_error').html("Invalid email");
			 $('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		   
		}
		else
		{
			$('#stuemail_error').hide();
		}

	}
	function stumobilenoCheck(){
		var pattern = new RegExp("^[6-9][0-9]{9}$");
		var stumobileno_errorval = $('#mobile').val();
		if(stumobileno_errorval.length=""){
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Mobile No. Cannot Be Empty");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
			
		}else if(!pattern.test(stumobileno_errorval)){
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Invalid Mobile No.");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
		}
		else
		{
			$('#stumobileno_error').hide();
		}

	}
	function emailsendfunction(){
		var email=$('#email').val();
		var datas = `sendemail=1&email=${email}`;

                    $htmlObj=$.ajax({
                    type:"POST",
                    url:"http://localhost:81/neetjee/assist-your-self/ajax1.php",
                    ifModified:true,
                    dataType:"html",
                    data: datas,
                    cache: false,		 
                    success: function(result) {
                       // $('#result').html(result);
					   console.log("success");
                        }
                    });
	}

	function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }

	function setState(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"POST",
		url:rul,
        ifModified:true,
	    dataType:"html",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
			 NProgress.start();
			  			  
                             $(id).addClass('disabledbutton');

		 },
		success: function(result) {
                    
			 if(id != '#statistics')
			NProgress.done();
                 $(id).removeClass('disabledbutton');


				 $(id).html(result);
				//alert(id);<img src="js/loading.gif" />
			},
				 error: function() {
					 NProgress.done();
					 //setStateGet(id,url,datas);
				 }
		 });
}
</script>

