// JavaScript Document
function setState(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"POST",
		url:rul,
        ifModified:true,
	    dataType:"html",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
			 NProgress.start();
			  			  
                             $(id).addClass('disabledbutton');

		 },
		success: function(result) {
                    
			 if(id != '#statistics')
			NProgress.done();
                 $(id).removeClass('disabledbutton');


				 $(id).html(result);
				//alert(id);<img src="js/loading.gif" />
			},
				 error: function() {
					 NProgress.done();
					 //setStateGet(id,url,datas);
				 }
		 });
}
function setStateEnc(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"POST",
		url:rul,
        ifModified:true,
	    dataType:"html",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
		//	 NProgress.start();
			 
			  NProgress.start();
           //$('#formLoader').fadeIn();
		 },
		success: function(result) {
			 if(id != '#statistics')
			NProgress.done();
			         //   $('#formLoader').fadeOut();
				 $(id).html(result);
				//alert(id);<img src="js/loading.gif" />
				 }
		 });
}


function setStateGet(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"GET",
		url:rul,
        ifModified:true,
	    dataType:"html",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
		
			   NProgress.start();
                        $(id).addClass('disabledbutton');

				 },
		success: function(result) {
			 if(id != '#statistics')
			NProgress.done();
                 $(id).removeClass('disabledbutton');
				 $(id).html(result);
			 },
				 error: function() {
					 NProgress.done();
					 //setStateGet(id,url,datas);
				 }
		 });
}

function setStateVal(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"GET",
		url:rul,
        ifModified:true,
	    dataType:"text",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
			 NProgress.start();
                 $(id).addClass('disabledbutton');
        //   $('#formLoader').fadeIn();
				 },
		success: function(result) {
			NProgress.done();
                 $(id).removeClass('disabledbutton');

			        //    $('#formLoader').fadeOut();
				 $(id).val(result);
				 }
		 });
}


function confirmDelete(id,path,datas){

if(confirm("Are you sure You want to delete this?")){
  setStateGet(id,path,datas);
}
}


//Form Animations
function animateForm(url,formData,tableData){

	$('.panel-body .btn-success').hide();

	 $("html:not(:animated),body:not(:animated)").animate({ scrollTop: 0}, 1000 );

	setTimeout(function()
            {

      
	$('#adminForm').fadeIn(  390,function(){

	$(this).slideUp(200,function(){

	setStateGet('adminForm',url,formData);

	 });

	  });
	},900);

	$('.panel-body .btn-success').fadeIn(2000);


     setStateGet('adminTable',url,tableData);

}

function countUp(count,id,div_by,cnt_speed)
{
    var speed = Math.round(count / div_by),
        $display = $(id),
        run_count = 1,
        int_speed = cnt_speed;

    var int = setInterval(function() {
        if(run_count < div_by){
            $display.text(speed * run_count);
            run_count++;
        } else if(parseInt($display.text()) < count) {
            var curr_count = parseInt($display.text()) + 1;
            $display.text(curr_count);
        } else {
            clearInterval(int);
        }
    }, int_speed);
}

function navigate(rul,id){


	$('#navigation a').removeClass('active');
	$('#'+id).addClass('active');

$("#loading").css('display','inline');

//$("#l_bar").css('display','inline');

$('#content').slideUp("1000",function (){

  htmlobj=$.ajax({url:rul,cache:false,async:false,dataType:"html",timeout:4000,ifModified:true,


				 error: function( objAJAXRequest, strError,strError1 )
				 {
				 alert(strError1);	  }


				 });


  $('#content').html(htmlobj.responseText);

$('#content').slideDown("3000");
 // $("#l_bar").css('display','none');
	    $("#loading").css('display','none');

								});

//setStateGet('rightDiv',url,'displayPage=1')
//	alert($('#'+id).text());

}



function addslashes(str) {

str  = encodeURIComponent(str);


  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
}


// American Numbering System
var th = ['','thousand','million', 'billion','trillion'];
// uncomment this line for English Number System
// var th = ['','thousand','million', 'milliard','billion'];

var dg = ['zero','one','two','three','four', 'five','six','seven','eight','nine']; var tn = ['ten','eleven','twelve','thirteen', 'fourteen','fifteen','sixteen', 'seventeen','eighteen','nineteen']; var tw = ['twenty','thirty','forty','fifty', 'sixty','seventy','eighty','ninety']; function toWords(s){s = s.toString(); s = s.replace(/[\, ]/g,''); if (s != String(parseFloat(s))) return 'not a number'; var x = s.indexOf('.'); if (x == -1) x = s.length; if (x > 15) return 'too big'; var n = s.split(''); var str = ''; var sk = 0; for (var i=0; i < x; i++) {if ((x-i)%3==2) {if (n[i] == '1') {str += tn[Number(n[i+1])] + ' '; i++; sk=1;} else if (n[i]!=0) {str += tw[n[i]-2] + ' ';sk=1;}} else if (n[i]!=0) {str += dg[n[i]] +' '; if ((x-i)%3==0) str += 'hundred ';sk=1;} if ((x-i)%3==1) {if (sk) str += th[(x-i-1)/3] + ' ';sk=0;}} if (x != s.length) {var y = s.length; str += 'point '; for (var i=x+1; i<y; i++) str += dg[n[i]] +' ';} return str.replace(/\s+/g,' ');}


        var fontsize = '';
	$(document).ready(function(){
		 $("#landingpopup").modal({
		 backdrop: 'static',
    keyboard: false
		 });

		 $('.btn-submit').click(function(){
		 $("#landingpopup").hide();
		 $('.fade.in').css({'display':'none'});
		  $('.modal-backdrop').css({'opacity':'0'});

		 });

        $("#text_resize_increase").click(function (){
        	var maxsize ='21px';
        	var size = $("#textDiv *").css('font-size');

        	if(maxsize<=size)
        	{

        	}
        	else
        	{
        	 $("#textDiv *").css("font-size", function(i, value) {
                return parseInt(value) + 1.1;
            });
        	}
        });

        //  font size
        	$("#text_resize_decrease").click(function (){
        	var minsize ='12px';
        	var size = $("#textDiv *").css('font-size');

        	if(minsize>=size)
        	{

        	}
        	else{
        	$("#textDiv *").css("font-size", function(i, value) {
                return parseInt(value) - 1.1;
            });
        	}
        });

         // Reset font size
         var originalFontSize = $('#textDiv *').css('font-size');
          $("#text_resize_reset").click(function(){


          $('#textDiv *').css('font-size', originalFontSize);
          });
            });

$(function () {
    var _btn = $('.font-settings li.has-dropdown > span.plus');
    _btn.on('click', function (e) {
    e.stopPropagation();
    $(this).next('.dropdown-font-settings').toggleClass('active');
    });
    $('.font-settings').on('click', function (e) {
    e.stopPropagation();
    });
    $('body').on('click', function (e) {
    $('.dropdown-font-settings').removeClass('active');
    });
    });







