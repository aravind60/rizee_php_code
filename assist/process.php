<?php
include('../include/session.php');
//$conn=mysqli_connect('localhost','root','','qsbank') ;
$conn=mysqli_connect('localhost','rspace','Rsp@2019','qsbank') ;

function query($sql)
{
    global $conn;
	return mysqli_query($conn,$sql);
}
//$conn1=mysqli_connect('localhost','root','','neetjee') ;
$conn1=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
function query1($sql)
{
    global $conn1;
	return mysqli_query($conn1,$sql);
}

if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_REQUEST['rid'])){
		 $data_sel = query1("SELECT * FROM assist_examdetails WHERE id = '".$_REQUEST['rid']."'");
		if(mysqli_num_rows($data_sel) > 0){
			$data = mysqli_fetch_array($data_sel);
			$_REQUEST = array_merge($_REQUEST,$data);
		}
		
	}
	
	if(isset($_REQUEST['rid'])){
		if($_REQUEST['rid']!=''){
			$styledetails="";
			$styledetails1="style='display:none;'";
		}else{
			$styledetails="style='display:none;'";
		$styledetails1="";
		}
	}else{
		$styledetails="style='display:none;'";
		$styledetails1="";
	}
?>
		<div class="row">
                    <div class="col-lg-4 offset-lg-4">
                        <h3 class="assit-title">Assess Your Self</h3>
                        <p>Assess now to check your readiness to competitive exams</p>
                        
                    </div>
                </div>
				<?php
				if(isset($_REQUEST['exam'])){
					if($_REQUEST['exam']!=''){
						if($_REQUEST['exam']=='1'){
							$examval="active";
							$examval1="";
							$exam="1";
						}else{
							$examval="";
							$examval1="active";
							$exam="2";
						}
					}else{
						$examval="active";
						$examval1="";
						$exam="1";
					}
				}else{
					$examval="active";
					$examval1="";
					$exam="1";
				}
				?>
                <div class="row">
                    <div class="col-lg-8 offset-lg-2">
                        <div class="white-block">
							
							 <div class="row" <?php echo $styledetails; ?>>
                                <div class="col-lg-6">
                                    <img src="images/assess_your_self.png" class="assess_image_align" alt="Awesome Image" />
                                </div>
                                <div class="col-lg-6">
                                   
                                        <div class="row">
                                            <div class="col-sm-12 mt-4">
                                                <h6 class="font12 font-weight-bold">FIRST NAME</h6>
                                                <input type="text" class="form-control" id="firstname" placeholder="" value="<?php if(isset($_REQUEST['firstname'])){ echo $_REQUEST['firstname'];}?>"  onkeyup="stufirstnameCheck();" required autocomplete="off">
												<div id="stufirstname_error"  class="text-danger"><?php if(isset($_SESSION['error']['firstname'])){ echo $_SESSION['error']['firstname'];}?></div>
                                            </div>
                                            <div class="col-sm-12 mt-3">
                                                <h6 class="font12 font-weight-bold">LAST NAME</h6>
                                                <input type="text" class="form-control" id="lastname" placeholder="" value="<?php if(isset($_REQUEST['lastname'])){ echo $_REQUEST['lastname'];}?>"  onkeyup="stulastnameCheck();" required autocomplete="off"/>
												<div id="stulastname_error" class="text-danger"  ><?php if(isset($_SESSION['error']['lastname'])){ echo $_SESSION['error']['lastname'];}?></div>
                                            </div>
                                            <div class="col-sm-12 mt-3">
                                                <h6 class="font12 font-weight-bold">MOBILENO</h6>
                                                <input type="text" class="form-control" id="mobile" placeholder="" value="<?php if(isset($_REQUEST['mobile'])){ echo $_REQUEST['mobile'];}?>" onkeyup="stumobilenoCheck();"  onkeypress="return isNumber(event,$(this),10)"  maxlength="10" required autocomplete="off">
												<div id="stumobileno_error" class="text-danger"> <?php if(isset($_SESSION['error']['mobile'])){ echo $_SESSION['error']['mobile'];}?></div>
                                            </div>
                                            <div class="col-sm-12 mt-3">
                                                <h6 class="font12 font-weight-bold">EMAIL</h6>
                                                <input type="text" class="form-control"  id="email" placeholder="" value="<?php if(isset($_REQUEST['email'])){ echo $_REQUEST['email'];}?>" onkeyup="stuemailCheck();" required autocomplete="off">
												<div  id="stuemail_error" class="text-danger"><?php if(isset($_SESSION['error']['email'])){ echo $_SESSION['error']['email'];}?></div>
                                            </div>
											<?php
											if(isset($_REQUEST['otpstatus'])){
												if($_REQUEST['otpstatus']=='1'){
												
												$style13='style="display:none;"';
													$style14='';
											?>
											<script>
												emailsendfunction();
											</script>
												<div class="col-sm-12 mt-3" <?php echo $styledetails; ?> >
													 <h6 class="font12 font-weight-bold">OTP</h6>
													
														<input type="text" class="form-control" id="otp"
															placeholder="" value="" required maxlength="6" autocomplete="off">
														<small class="text-primary" id="resendtype" style="display:none;" ><a   style="cursor:pointer;" onclick="emailsendfunction();" >Resend OTP  </a></small>
														 <small id="timingtype" class="text-primary" <?php echo $style14; ?>><img src="<?php echo SECURE_PATH;?>assist/images/download.png" width="30px"></img> <span id="time">02:00</span> Minutes</small>
														<div id="otperror" class="text-danger"><?php if(isset($_SESSION['error']['otp'])){ echo $_SESSION['error']['otp'];}?></div>
														<small class="helpblock">Note :Please check you
														email inbox or messages for OTP</small>
												</div>
												
											
											<?php
												}
											}
											
											?>
                                        </div>
                                   
                                </div>
                            </div>
                            <div class="row" <?php echo $styledetails1; ?>>
                                <div class="col-sm-4 border-right responsive-img-none">
                                    <img src="images/assist_img.png" class="assist-logo" alt="Awesome Image" />
                                </div>
                                <div class="col-sm-8">
                                  <ul class="list-inline assist-app-buttons getStudentAppCls">
                                    <li   class="<?php echo $examval; ?>" attr_type="NEET" 
                                                    onclick="studentexamtype('1');setState('subjectdivdata','<?php echo SECURE_PATH;?>assist/ajax.php','subjectdata=1&exam=1&class='+$('#class').val()+'&exam_short_full='+$('#exam_short_full').val()+'');setState('previouspaperdiv','<?php echo SECURE_PATH;?>assist/ajax.php','previousdata=1&exam=1&class='+$('#class').val()+'&examtype='+$('#examtype').val()+'&exam_short_full='+$('#exam_short_full').val()+'');setState('imagedatadiv','<?php echo SECURE_PATH;?>assist/ajax.php','imagedatadiv=1&exam=1&exam_short_full='+$('#exam_short_full').val()+'');" >NEET</li>
                                    <li class="<?php echo $examval1; ?>" attr_type="JEE" 
                                                    onclick="studentexamtype('2');setState('subjectdivdata','<?php echo SECURE_PATH;?>assist/ajax.php','subjectdata=1&exam=2&class='+$('#class').val()+'&exam_short_full='+$('#exam_short_full').val()+'');setState('previouspaperdiv','<?php echo SECURE_PATH;?>assist/ajax.php','previousdata=1&exam=2&class='+$('#class').val()+'&examtype='+$('#examtype').val()+'&exam_short_full='+$('#exam_short_full').val()+'');setState('imagedatadiv','<?php echo SECURE_PATH;?>assist/ajax.php','imagedatadiv=1&exam=2&exam_short_full='+$('#exam_short_full').val()+'');">JEE</li>    
                                  </ul>
                                </div>
                            </div>
							<input type="hidden" name="exam" id="exam" value="<?php echo $exam; ?>">
							<?php
							if(isset($_REQUEST['class'])){
								if($_REQUEST['class']!=''){
									if($_REQUEST['class']=='1'){
										$examclass="active";
										$examclass1="";
										$examclass2="";
										$class="1";
									}else if($_REQUEST['class']=='2'){
										$examclass="";
										$examclass1="active";
										$examclass2="";
										$class="2";
									}else{
										$examclass="";
										$examclass1="";
										$examclass2="active";
										$class="1,2";
									}
								}else{
									$examclass="";
									$examclass1="";
									$examclass2="active";
									$class="1,2";
								}
							}else{
								$examclass="";
								$examclass1="";
								$examclass2="active";
								$class="1,2";
							}
							?>
                             <div class="row" <?php echo $styledetails1; ?>>
                                <div class="col-sm-12">
									<ul class="list-inline assist-year-buttons getStudentAppCls">
                                        <li class="<?php echo $examclass2; ?>" attr_type="All"  onclick="studentexamyear('1,2');">CUMULATIVE</li>
                                        <li class="<?php echo $examclass; ?>" attr_type="1"  onclick="studentexamyear('1');">1ST YEAR</li>
                                        <li class="<?php echo $examclass1; ?>" attr_type="2"  onclick="studentexamyear('2');;">2ND YEAR</li>  
                                      </ul>
                                </div>
                            </div>
							<input type="hidden" name="class" id="class" value="<?php echo $class; ?>">
							<?php
								if(isset($_REQUEST['examtype'])){
									if($_REQUEST['examtype']!=''){
										if($_REQUEST['examtype']=='1'){
											$examtypeclass="checked";
											$examtypeclass1="";
											$examtypeclass2="";
											$examtypeshow="show";
											$examtypeshow1="";
											$examtypeshow2="";
											$examtypeval=1;
										}else if($_REQUEST['examtype']=='2'){
											$examtypeclass="";
											$examtypeclass1="checked";
											$examtypeclass2="";
											$examtypeshow="";
											$examtypeshow1="show";
											$examtypeshow2="";
											$examtypeval=2;
										}else if($_REQUEST['examtype']=='3'){
											$examtypeclass="";
											$examtypeclass1="";
											$examtypeclass2="checked";
											$examtypeshow="";
											$examtypeshow1="";
											$examtypeshow2="show";
											$examtypeval=3;
										}
									}else{
										$examtypeclass="checked";
											$examtypeclass1="";
											$examtypeclass2="";
											$examtypeshow="show";
											$examtypeshow1="";
											$examtypeshow2="";
											$examtypeval=1;
									}
								}else{
									$examtypeclass="checked";
									$examtypeclass1="";
									$examtypeclass2="";
									$examtypeshow="show";
									$examtypeshow1="";
									$examtypeshow2="";
									$examtypeval=1;
								}
								?>
									 <?php
									if(isset($_REQUEST['exam_short_full'])){
										if($_REQUEST['exam_short_full']!=''){
											if($_REQUEST['exam_short_full']=='1'){
												$examcat="active";
												$examcat1="";
												$examcategory=1;
											}else if($_REQUEST['exam_short_full']=='2'){
												$examcat="";
												$examcat1="active";
												$examcategory=2;
											}else{
												$examcat="active";
												$examcat1="";
												$examcategory=1;
											}
										}else{
											$examcat="active";
											$examcat1="";
											$examcategory=1;
										}
									}else{
										$examcat="active";
										$examcat1="";
										$examcategory=1;
									}
									if(isset($_REQUEST['exam'])){
										if(isset($_REQUEST['exam_short_full'])){
											if($_REQUEST['exam']=='1' && $_REQUEST['exam_short_full']==1){
												$duration="45 Mins";
												$styleshort='';
												$styleshort1='style="display:none;"';
												$styleshort2='style="display:none;"';
											}else if($_REQUEST['exam']=='1' && $_REQUEST['exam_short_full']==2){
												$duration="3 Hours";
												$styleshort='style="display:none;"';
												$styleshort1='';
												$styleshort2='style="display:none;"';
											}else if($_REQUEST['exam']=='2' && $_REQUEST['exam_short_full']==1){
												$duration="1 Hour";
												$styleshort='style="display:none;"';
												$styleshort1='style="display:none;"';
												$styleshort2='';
											}else if($_REQUEST['exam']=='2' && $_REQUEST['exam_short_full']==2){
												$duration="3 Hours";
												$styleshort='style="display:none;"';
												$styleshort1='';
												$styleshort2='style="display:none;"';
											}else{
												$duration="45 Mins";
												$styleshort='';
												$styleshort1='style="display:none;"';
												$styleshort2='style="display:none;"';
											}
										}else{
											$duration="45 Mins";
											$styleshort='';
												$styleshort1='style="display:none;"';
												$styleshort2='style="display:none;"';
										}
									}else{
										$duration="45 Mins";
										$styleshort='';
										$styleshort1='style="display:none;"';
										$styleshort2='style="display:none;"';
									}
										
									
									?>
								<input type="hidden" name="duration" id="duration"  value="<?php echo $duration; ?>">
								<div class="row" <?php echo $styledetails1; ?>>
                                <div class="col-sm-12">
                                    <h6>Select Your Assessment Type</h6>
                                    <div class="block_border_green">
                                        <div class="row">
                                            <div class="col-lg-8 col-md-12 col-sm-12">
                                                <ul class="list-inline assist-year-exam mb-0">
                                                    <li class="<?php echo $examcat; ?> text-uppercase" attr_type="short" 
														onclick="studentexamtype2('1');setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&exam_short_full=1')">Quick Assessment</li>
														<li class="<?php echo $examcat1; ?> text-uppercase" attr_type="full" 
														onclick="studentexamtype2('2');setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&exam_short_full=2')">Comprehensive Assessment</li>
                                                  </ul> 
												  <input type="hidden" name="exam_short_full" id="exam_short_full" value="<?php echo $examcategory; ?>">
												  <?php
													if(isset($_REQUEST['exam'])){
														if($_REQUEST['exam']=='1'){
															$examid=" AND id in (1,2,3,5)";
															 $examp=" AND exam='1'";
															 $examdata="1,2,3,5";
													   }else if($_REQUEST['exam']=='2'){
														   $examid=" AND id in (2,3,4)";
														   $examp=" AND exam='2'";
														   $examdata="2,3,4";
													   }else{
															  $examid=" AND id in (1,2,3,5)";
															 $examp=" AND exam='1'";
															 $examdata="1,2,3,5";
													   }
													}else{
														  $examid=" AND id in (1,2,3,5)";
															 $examp=" AND exam='1'";
															 $examdata="1,2,3,5";
													}
													
												   ?>
													   <?php
													 if(isset($_REQUEST['exam_short_full'])){
														if($_REQUEST['exam_short_full']=='2'){
															if(isset($_REQUEST['subject'])){
																$subjectval=$examdata;
															}else{
																 $subjectval=$examdata;
															}
															
														}else{
															if(isset($_REQUEST['subject'])){
																if($_REQUEST['subject']!='' && $_REQUEST['subject']!='undefined'){
																	 $subjectval=$_REQUEST['subject'];
															   }else{
																	$subjectval="";
															   }
															}else{
																  $subjectval="";
															}
														}
													}else{
														if(isset($_REQUEST['subject'])){
															if($_REQUEST['subject']!='' && $_REQUEST['subject']!='undefined'){
																 $subjectval=$_REQUEST['subject'];
														   }else{
																$subjectval="";
														   }
														}else{
															  $subjectval="";
														}
													}
													/*if(isset($_REQUEST['subject'])){
														if($_REQUEST['subject']!=''){
															 $subjectval=$_REQUEST['subject'];
													   }else{
															$subjectval="";
													   }
													}else{
														  $subjectval="";
													}*/
												   ?>
                                                  <div class="row">
                                                    <div class="col-sm-4" id="subjectdivdata">
													<?php
														if(isset($_REQUEST['exam_short_full'])){
															if($_REQUEST['exam_short_full']!='2'){
																?>
																<small style="font-size:12px;color:red;"  >Please select one subject</small>
																<?php
															$s=1;
															$sqlsubject=query("select * from subject where estatus='1' ".$examid."");
															while($rowsubject=mysqli_fetch_array($sqlsubject)){
																?>
																<div class="form-check">
																<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios<?php echo $s; ?>" value="<?php echo $rowsubject['id']; ?>"  <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$rowsubject['id']) { echo 'checked'; }  } ?> onclick="subjectdatadiv(<?php echo $rowsubject['id']; ?>);" >
																<label class="form-check-label" for="exampleRadios<?php echo $s; ?>">
																  <?php echo $rowsubject['subject']; ?>
																</label>
															  </div>
																<?php
																	$s++;
															
																
														}
													   }
													}else{
														?>
														<small style="font-size:12px;color:red;"  >Please select one subject</small>
														<?php
														 $s=1;
														$sqlsubject=query("select * from subject where estatus='1' ".$examid." ");
														while($rowsubject=mysqli_fetch_array($sqlsubject)){ ?>
																 <div class="form-check">
																	<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios<?php echo $s; ?>" value="<?php echo $rowsubject['id']; ?>"  <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$rowsubject['id']) { echo 'checked'; }  } ?> onclick="subjectdatadiv(<?php echo $rowsubject['id']; ?>);" >
																	<label class="form-check-label" for="exampleRadios<?php echo $s; ?>">
																	  <?php echo $rowsubject['subject']; ?>
																	</label>
																  </div>
														<?php $s++; } } ?>
														<input type="hidden" name="subject" id="subject" value="<?php echo $subjectval; ?>">
														<div id="stusubjecterror" class="text-danger" style="font-size:12px;"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></div>
                                                    </div>
													 
													 <?php
														
														if($exam=='2'){
															$year=" AND pexamtype='1' AND year in (2020) ";
															//$year="";
														}else{
															$year=" AND year in (2019) ";
															//$year="";
														}
														if(isset($_REQUEST['class'])){
															if($_REQUEST['class']!=''){
																if($_REQUEST['class']=='1,2'){
																	$stylet="";
																}else{
																	$stylet="disabled";
																}
															}else{
																$stylet="";
															}
														}else{
															$stylet="";
														}
														?>
														<input type="hidden" name="examtype" id="examtype" value="1">
														
														
                                                   
													
												  </div> 
												  <div class="row">
												  <div class="offset-lg-3 col-lg-6 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
											        <a class="btn btn-primary btn-lg middle mt-3 assist-submit-button text-white" style="cursor:pointer;" onClick="setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','validateForm=1&firstname='+$('#firstname').val()+'&lastname='+$('#lastname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&exam='+$('#exam').val()+'&class='+$('#class').val()+'&examtype='+$('#examtype').val()+'&subject='+$('#subject').val()+'&exam_short_full='+$('#exam_short_full').val()+'&duration='+$('#duration').val()+'&otp='+$('#otp').val()+'&rid=')">Start Your Self Assessment</a>
									              </div>
												  </div> 
                                            </div>
                                            <div class="col-lg-4 col-md-12 col-sm-12" id="imagedatadiv">
												<img src="images/assist_45.png" class="assist45Min" <?php echo $styleshort; ?> alt="Awesome Image" />
                                                <img src="images/assist_3h.png" class="assist3Hrs" <?php echo $styleshort1; ?> alt="Awesome Image" />
												<img src="images/assist_1h.png" class="assist_1h" <?php echo $styleshort2; ?> alt="Awesome Image" />
                                            </div>
                                        </div>
                                    </div>
									
                                </div>
                            </div>
							
                            <div class="row">
                               
                                    <?php
									if(isset($_REQUEST['otpstatus'])){
									if($_REQUEST['otpstatus']=='1'){
										?>
									
											<div class="offset-lg-3 col-lg-6 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
												<a class="btn btn-primary btn-lg middle mt-3 assess-submit-button text-white" style="width:30%" onClick="setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','validateForm2=1&firstname='+$('#firstname').val()+'&lastname='+$('#lastname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&otp='+$('#otp').val()+'&rid=<?php echo $_REQUEST['rid']; ?>&otpstatus=1')">Submit</a>
										 </div>
										<?php
									}else{
										?>
									
											 <div class="offset-lg-3 col-lg-6 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
												<a class="btn btn-primary btn-lg middle mt-3 assess-submit-button text-white" style="width:30%" onClick="setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','validateForm1=1&firstname='+$('#firstname').val()+'&lastname='+$('#lastname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&rid=<?php echo $_REQUEST['rid']; ?>')">Submit</a>
										 </div>
										<?php
									}
								}else{
									if(isset($_REQUEST['rid'])){
										if($_REQUEST['rid']!=''){
										?>
											 <div class="offset-lg-3 col-lg-6 offset-md-3 col-md-6 offset-sm-2 col-sm-8">
												<a class="btn btn-primary btn-lg middle mt-3 assess-submit-button text-white" style="width:30%" onClick="setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','validateForm1=1&firstname='+$('#firstname').val()+'&lastname='+$('#lastname').val()+'&mobile='+$('#mobile').val()+'&email='+$('#email').val()+'&otp='+$('#otp').val()+'&rid=<?php echo $_REQUEST['rid']; ?>')">Submit</a>
												
											</div>
										<?php
										}else{?>
										
									<?php } 
									}else{?>
										 
									<?php }
								}
								
								
								?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php  
	unset($_SESSION['error']);
	} 

	//Process and Validate POST data
if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	//$mobile = $post['mobile'];
	//$email = $post['email'];
 
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  /*$field = 'firstname';
	if(!$post['firstname'] || strlen(trim($post['firstname'])) == 0){
	  $_SESSION['error'][$field] = "* Firstname cannot be empty";
	}
	 $field = 'lastname';
	if(!$post['lastname'] || strlen(trim($post['lastname'])) == 0){
	  $_SESSION['error'][$field] = "* Lastname cannot be empty";
	}
	
  
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }


	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}*/
	
 if($post['examtype']=='1'){
	  $field = 'subject';
	if(!$post['subject'] || strlen(trim($post['subject'])) == 0){
	  $_SESSION['error'][$field] = "* Subject cannot be empty";
	}
 }
 if($post['examtype']!=''){
	 $examtypet=$post['examtype'];
 }else{
	 $examtypet=0;
 }
/*$field = 'otp';
	if(!$post['otp'] || strlen(trim($post['otp'])) == 0){
	  $_SESSION['error'][$field] = "* OTP cannot be empty";
	}
	if($post['otp']!=''){
		$sql=query1("select count(id)  as cnt,timestamp,exp_timestamp from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' ");
		$rowl=mysqli_fetch_array($sql);
		
		if($rowl['cnt']==0){
			$_SESSION['error']['otp'] = "* Invalid OTP";
		}
		$expirytime=$rowl['timestamp'] + 2*60;
		$sql1=query1("select count(id)  as cnt from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' and  exp_timestamp between ".time()." and ".$rowl['exp_timestamp']." ");
		$rowl1=mysqli_fetch_array($sql1);
		
		if($rowl1['cnt']==0){
			$_SESSION['error']['otp'] = "*  OTP has been expired";
		}
	}
	*/
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
     setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=1&firstname=<?php echo $post['firstname'];?>&lastname=<?php echo $post['lastname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&exam=<?php echo $post['exam'];?>&class=<?php echo $post['class'];?>&examtype=<?php echo $post['examtype'];?>&subject=<?php echo $post['subject'];?>&subject=<?php echo $post['subject'];?>&exam_short_full=<?php echo $post['exam_short_full'];?>&duration=<?php echo $post['duration'];?>&otp=<?php echo $post['otp'];?>');
    </script>
  <?php
	}
	else{
		
		
		if($post['exam_short_full']='1'){
			$exam_short_full=$post['exam_short_full'];
			if($post['examtype']=='1'){
				$subjectval=$post['subject'];
			}else if($post['examtype']=='3'){
				if($post['exam_short_full']!=1){
					if($post['exam']==1){
						$subjectval="1,2,3,5";
					}else{
						$subjectval="2,3,4";
					}
				}else{
					$subjectval=$post['subject'];
				}
			}
		}else{
			$exam_short_full=0;
		}
		
			
			$result=query1('insert   assist_examdetails set firstname="",lastname="",mobile="",email="",exam="'.$post['exam'].'",examtype="'.$examtypet.'",class_id="'.$post['class'].'",subject="'.$subjectval.'",chapter="0",previous_paper="0",exam_short_full="'.$post['exam_short_full'].'",duration="'.$post['duration'].'",ip_address="'.$_SERVER['REMOTE_ADDR'].'",is_completed="0",correct="0",wrong="0",not_answered="0",total_questions="0",correct_marks="0",negative_marks="0",total_marks="0",in_time="0",less_time="0",over_time="0",accuracy="0",speed="0",estatus="1",timestamp="'.time().'"');
			$sid= mysqli_insert_id($conn1);
			if($result){ 
				if($post['exam']==1){
					$examname="NEET";
				}else{
					$examname="JEE";
				}
				?>
				<script>
				window.location.href = 'http://college.rizee.in/student/student-assist-exam/:<?php echo $sid; ?>/:<?php echo $examname; ?>';
				</script>
				<?php
				
				}
			}
		
	}
	if(isset($_POST['validateForm1'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	$mobile = $post['mobile'];
	$email = $post['email'];
 
  
	$field = 'firstname';
	if(!$post['firstname'] || strlen(trim($post['firstname'])) == 0){
	  $_SESSION['error'][$field] = "* Firstname cannot be empty";
	}
	 $field = 'lastname';
	if(!$post['lastname'] || strlen(trim($post['lastname'])) == 0){
	  $_SESSION['error'][$field] = "* Lastname cannot be empty";
	}
	
  
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }


	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}
  /*
$field = 'otp';
	if(!$post['otp'] || strlen(trim($post['otp'])) == 0){
	  $_SESSION['error'][$field] = "* OTP cannot be empty";
	}
	if($post['otp']!=''){
		$sql=query1("select count(id)  as cnt,timestamp,exp_timestamp from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' ");
		$rowl=mysqli_fetch_array($sql);
		
		if($rowl['cnt']==0){
			$_SESSION['error']['otp'] = "* Invalid OTP";
		}
		$expirytime=$rowl['timestamp'] + 2*60;
		$sql1=query1("select count(id)  as cnt from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' and  exp_timestamp between ".time()." and ".$rowl['exp_timestamp']." ");
		$rowl1=mysqli_fetch_array($sql1);
		
		if($rowl1['cnt']==0){
			$_SESSION['error']['otp'] = "*  OTP has been expired";
		}
	}
	*/
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm1'] == 2){
	?>
    <script type="text/javascript">
     setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=1&firstname=<?php echo $post['firstname'];?>&lastname=<?php echo $post['lastname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&rid=<?php echo $post['rid']; ?>');
    </script>
  <?php
	}
	else{
		
		
			
			
			$result=query1('update   assist_examdetails set firstname="'.$post['firstname'].'",lastname="'.$post['lastname'].'",mobile="'.$post['mobile'].'",email="'.$post['email'].'" where id="'.$post['rid'].'"');
			
			if($result){ 
				$url="<?php echo SECURE_PATH;?>assist/ajax1.php?sendemail=1&email=".$post['email']."&firstname=".$post['firstname']."";
				@file($url);
				?>
				<script type="text/javascript">
					
					setStateGet('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=2&rid=<?php echo $post['rid']; ?>&otpstatus=1');
						//	window.location.href="<?php echo SECURE_PATH;?>assist/result.php";
				</script>
					
				<?php
				
			}
		}
		
	}

	if(isset($_POST['validateForm2'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	$mobile = $post['mobile'];
	$email = $post['email'];
 
  
	$field = 'firstname';
	if(!$post['firstname'] || strlen(trim($post['firstname'])) == 0){
	  $_SESSION['error'][$field] = "* Firstname cannot be empty";
	}
	 $field = 'lastname';
	if(!$post['lastname'] || strlen(trim($post['lastname'])) == 0){
	  $_SESSION['error'][$field] = "* Lastname cannot be empty";
	}
	
  
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }


	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}
  
$field = 'otp';
	if(!$post['otp'] || strlen(trim($post['otp'])) == 0){
	  $_SESSION['error'][$field] = "* OTP cannot be empty";
	}
	if($post['otp']!=''){
		$sql=query1("select count(id)  as cnt,timestamp,exp_timestamp from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' ");
		$rowl=mysqli_fetch_array($sql);
		
		if($rowl['cnt']==0){
			$_SESSION['error']['otp'] = "* Invalid OTP";
		}
		$expirytime=$rowl['timestamp'] + 2*60;
		$sql1=query1("select count(id)  as cnt from assist_verify_students where  email='".$post['email']."' and email_otp='".$post['otp']."' and  exp_timestamp between ".time()." and ".$rowl['exp_timestamp']." ");
		$rowl1=mysqli_fetch_array($sql1);
		
		if($rowl1['cnt']==0){
			$_SESSION['error']['otp'] = "*  OTP has been expired";
		}
	}
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm2'] == 2){
	?>
    <script type="text/javascript">
     setState('adminForm','<?php echo SECURE_PATH;?>assist/process.php','addForm=1&firstname=<?php echo $post['firstname'];?>&lastname=<?php echo $post['lastname'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&otp=<?php echo $post['otp']; ?>&otpstatus=1&rid=<?php echo $post['rid']; ?>');
    </script>
  <?php
	}
	else{
		
			$result=query1('update   assist_examdetails set firstname="'.$post['firstname'].'",lastname="'.$post['lastname'].'",mobile="'.$post['mobile'].'",email="'.$post['email'].'",email_verify="1" where id="'.$post['rid'].'"');
			
			if($result){ 
				$sqlc=query1("select * from assist_examdetails where id='".$post['rid']."'");
				$rowc=mysqli_fetch_array($sqlc);
				$correct=$rowc['correct'];
				$wrong=$rowc['wrong'];
				$tot=($correct*4)-$wrong;
				$url="http://admin.mylearningplus.in/assist/generate.php?id=".$post['rid']."&mobile=".$post['mobile']."&email=".$post['email']."&firstname=".$post['firstname']."&correct=".$tot."&totalquestions=".$rowc['total_marks']."";
				@file($url);
				?>
				<script type="text/javascript">
					
					window.location.href="http://admin.mylearningplus.in/assist/result.php";
				</script>
					
				<?php
				
			}
		}
		
	}
	?>
	<script type="text/javascript">
        $(".chosen-select").chosen();
        $(document).on("click",".getChangeCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            $(".getStudentAppCls li").removeClass("active")
	        $(".getStudentAppCls li:nth-child(1)").addClass("active");	
            var type = $(this).attr("attr_type");
            if(type == "sales"){
                $(".salesBlockCls").show();
                $(".supportBlockCls").hide();
            }else{
                $(".salesBlockCls").hide();
                $(".supportBlockCls").show();
            }
        });
        
        $(document).on("click",".getStudentAppCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            $(".getIssueTypeCls li").removeClass("active")
	        $(".getIssueTypeCls li:nth-child(1)").addClass("active");	
            var type = $(this).attr("attr_type");

            if(type == "student"){
                $(".supportStudentAppBlockCls").show();
                $(".supportInsAppBlockCls").hide();
            }else{
                $(".supportStudentAppBlockCls").hide();
                $(".supportInsAppBlockCls").show();
            }
        });
        $(document).on("click",".getIssueTypeCls li",function(){
            $(this).closest("ul").find("li").removeClass("active");
	        $(this).addClass("active");	
            var activeClass= $(".getStudentAppCls").find(".active").attr("attr_type");
            var type = $(this).attr("attr_type");

            if(activeClass == "student"){
                if(type == "technical"){
                    $(".FMStudentAppCls").show();
                    $(".transationStudentAppCls").hide();
                }else{
                    $(".FMStudentAppCls").hide();
                    $(".transationStudentAppCls").show();
                }
            }else{
                if(type == "technical"){
                    $(".FMInsAppCls").show();
                    $(".transactionInsAppCls").hide();
                }else{
                    $(".FMInsAppCls").hide();
                    $(".transactionInsAppCls").show();
                }
            }

        });
        
    </script>
</body>

</html>

<script>
	function studentexamtype(val){
		
		if(val=='1'){
			$("#exam").val("1");
		}else if(val=='2'){
			$("#exam").val("2");
		}

	}
	function studentexamtype2(val){
		if(val=='1'){
			$("#exam_short_full").val("1");
			$(".assist45Min").show();
            $(".assist3Hrs").hide();
            
		}else if(val=='2'){
			$("#exam_short_full").val("2");
			$(".assist45Min").hide();
            $(".assist3Hrs").show();
		}

	}
	function studentexamyear(value){
		
		$("#class").val(value);

	}
	function subjectdatadiv(value){
		//alert(value);
		$("#subject").val(value);

	}

	function studentexamtype1(val){
		
		$("#examtype").val(val);
		

	}


	function stufirstnameCheck(){
		var stufirstnameval = $('#firstname').val();
		
		if(stufirstnameval.length==0){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname should not be empty");
			 $('#stufirstname_error').css("color","red");
             $('#stufirstname_error').css("font-size",10);
		}else if(stufirstnameval.length < 4 || stufirstnameval.length > 20){
			$('#stufirstname_error').show();
			$('#stufirstname_error').html("Firstname name must be between 4 and 20 characters");
			 $('#stufirstname_error').css("color","red");
                    $('#stufirstname_error').css("font-size",10);
		}else
		{
			$('#stufirstname_error').hide();
		}

	}

	function stulastnameCheck(){
		var stufirstnameval = $('#firstname').val();
		
		if(stufirstnameval.length==0){
			$('#stulastname_error').show();
			$('#stulastname_error').html("Last should not be empty");
			 $('#stulastname_error').css("color","red");
                    $('#stulastname_error').css("font-size",10);
		}else
		{
			$('#stulastname_error').hide();
		}

	}
	function stuemailCheck(){
		var pattern = new RegExp(
			/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i
		);
		var stuemail_errorval = $('#email').val();
		if(stuemail_errorval.length ==0){
			$('#stuemail_error').show();
			$('#stuemail_error').html("Please enter email");
			$('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		}else if(stuemail_errorval.length < 4){
			$('#stuemail_error').show();
			$('#stuemail_error').html("email cannot be less than 5 chars");
			$('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		}else if(!pattern.test(stuemail_errorval)){
			$('#stuemail_error').show();
			$('#stuemail_error').html("Invalid email");
			 $('#stuemail_error').css("color","red");
			$('#stuemail_error').css("font-size",12);
		   
		   
		}
		else
		{
			$('#stuemail_error').hide();
		}

	}
	function stumobilenoCheck(){
		var pattern = new RegExp("^[6-9][0-9]{9}$");
		var stumobileno_errorval = $('#mobile').val();
		if(stumobileno_errorval.length=""){
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Mobile No. Cannot Be Empty");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
			
		}else if(!pattern.test(stumobileno_errorval)){
			$('#stumobileno_error').show();
			$('#stumobileno_error').html("Invalid Mobile No.");
			$('#stumobileno_error').css("color","red");
			$('#stumobileno_error').css("font-size",10);
		}
		else
		{
			$('#stumobileno_error').hide();
		}

	}
	function emailsendfunction(){
		var email=$('#email').val();
		var firstname=$('#firstname').val();
		if(email==''){
			
			stuemailCheck();
		}else{
			var datas = `sendemail=1&email=${email}&firstname=${firstname}`;

						$htmlObj=$.ajax({
						type:"POST",
						url:"<?php echo SECURE_PATH;?>assist/ajax1.php",
						ifModified:true,
						dataType:"html",
						data: datas,
						cache: false,		 
						success: function(result) {
						// $('#result').html(result);
							countdowninterval();
							$('#resendtype').hide();
							$('#timingtype').show();
							}
						});
		}
	}

	function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }

	function setState(id,url,datas){
	id='#'+id;
    rul=url;
		$htmlObj=$.ajax({
		 type:"POST",
		url:rul,
        ifModified:true,
	    dataType:"html",
		data: datas,
        cache: false,
		 beforeSend: function() {
             // Show your spinner
			 NProgress.start();
			  			  
                             $(id).addClass('disabledbutton');

		 },
		success: function(result) {
                    
			 if(id != '#statistics')
			NProgress.done();
                 $(id).removeClass('disabledbutton');


				 $(id).html(result);
				//alert(id);<img src="js/loading.gif" />
			},
				 error: function() {
					 NProgress.done();
					 //setStateGet(id,url,datas);
				 }
		 });
}
</script>
<script>

var __timer = null;

function startTimer(duration, display) {
	
    var timer;
	if (timer) clearInterval(timer);
	var timer = duration, minutes, seconds;

	if (__timer) clearInterval(__timer);
	
     __timer = setInterval(function () {
		  minutes = parseInt(timer / 60, 10)
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        display.textContent = minutes + ":" + seconds;

        if (--timer < 0) {
			 timer = duration;
			$('#resendtype').show();
			$('#timingtype').hide();
        }
    }, 1000);
	
}

 function countdowninterval() {
	 
    var fiveMinutes = 60 * 2;
	
	 display = document.querySelector('#time');
	
    startTimer(fiveMinutes, display);
}
</script>
