<?php
include('../include/session.php');
error_reporting(0);
//$conn=mysqli_connect('localhost','root','','neetjee') ;
$conn=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
function query($sql)
{
    global $conn;
    return mysqli_query($conn,$sql);
}
//$conn1=mysqli_connect('localhost','root','','qsbank') ;
$conn1=mysqli_connect('localhost','rspace','Rsp@2019','qsbank') ;
function query1($sql)
{
    global $conn1;
    return mysqli_query($conn1,$sql);
}
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<!-- 
<script async src="../edut/tinymce4/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>

<script async src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>

Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo SECURE_PATH;?>vendor/bootstrap/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
		<style >

          svg{
 vertical-align: -5px;
 
}

            @media print {
                @page { margin: 0.5cm 0cm;
                 size: A4 }
                body { margin: 1.6cm; }
            }

            p:empty {
                display: none;
            }


			.d-flex {
				display: -webkit-box!important;
				display: -ms-flexbox!important;
				display: flex!important;
			}
			.pdf-content .q-no {
				font-size:20px;
				font-family: 'Source Sans Pro', sans-serif !important;
			}

			.pdf-content  .question-header {
				padding-left:5px;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                padding-bottom: 16px
			}
			
			.pdf-content  .question-header p ,
			.pdf-content  .question-header p span{
                margin-block-end: 0.1em;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size: 18px !important;
				font-weight: 600 !important;
			}

            .question-header img{
                max-width:75%;
            }

			.pdf-content p{
			margin-block-start: 0;
    /*margin-block-end: 0;*/
			}

			.question-list  {

			padding-left:5px;
			font-family: 'Source Sans Pro', sans-serif !important;
			font-size:14px !important;
				}
			.question-list .options {
				/* align-items: center; */
				justify-content: center;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                display:inline-block !important;
				width:45%;
				float:left;
                margin-right:16px;
			}
			.pdf-content p, span,
			.pdf-content .question-list p {
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;

			}

			.question-list .options p{
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                margin-block-end: 0em;
			}
            .question-list .options table{
                min-width:240px;
            }

            .question-list .options p img{
                max-width:77%;
            }
			.MsoListParagraph span span[style] {
				display:none;
			}


			.option{
			font-weight:bold;
			padding-left: 8px;
			}

            .question{
                padding-bottom:16px;
            }

            .clearall{
                clear:both;
                padding:0;margin:0;
            }

            tbody,tr,.question{
                page-break-inside:avoid;
                page-break-after:auto;
                position:relative;
                display:block;
            }
		</style>
	</head>
	<body>
	
	<body>
	<div class="pdf-content py-3">
		<div class="container">
			<h2 class="text-center">Zoology Assessment Questions</h2>
	
			<?php
			$k=1;
			$sel=query("select * from question_paper where estatus='1' and institution_id='1' and subject_id='3'  order by subject_id ");
			?>
            <table class="table qtable">

            <?php
			while($data=mysqli_fetch_array($sel)){
				$sel1=query1("select * from questiondata_app1 where estatus='1' and question_id='".$data['question_id']."'");
				$value=mysqli_fetch_array($sel1);

				$sel2=query1("select * from createquestion where estatus='1' and id='".$data['question_id']."'");
				$value1=mysqli_fetch_array($sel2);
			?>
			<tr>
               <td valign="top" style="width:50px;">
                   <div class="col-form-label pt-1 q-no"><?php echo $k ;?>.</div>

               </td>
                <td valign="top">
					<?php



			//	else
			
                    if($value1['qtype']=='7') { ?>
					<div class="question">
						<div class="questionid">Q.Id:	<?php echo $data['question_id']; ?></div>
						<div class="question-header">	<?php echo $value['question']; ?></div>
							<div class="question-list">
								<div class="options ">
								<table>
								<tr>
								<td valign="top" class="option">A)</td>
								<td valign="bottom"><?php echo $value['option1']; ?></td>
</tr>
</table>

								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">B)</td>
								<td valign="bottom"><?php echo $value['option2']; ?></td>
</tr>
</table>
								</div>
                                <div class="clearall"></div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">C)</td>
								<td valign="bottom"><?php echo $value['option3']; ?></td>
</tr>
</table>
								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">D)</td>
								<td valign="bottom"><?php echo $value['option4']; ?></td>
</tr>
</table>
								</div>

								<div class="clearall"></div>
							</div>
						</div>
								
					</div>
				
				<?php	}else  if($value1['qtype']=='3' || $value1['qtype']=='9') { 
					$obj=json_decode($value['question'],true);
					if($value['list1type']=='roman'){
						$list1type="upper-roman";
					}else if($value['list1type']=='alphabets'){
						$list1type="upper-alpha";
					}else if($value['list1type']=='numbers'){
						$list1type="decimal";
					}else{
						$list1type="upper-alpha";
					}
					if($value['list2type']=='roman'){
						$list2type="upper-roman";
					}else if($value['list2type']=='alphabets'){
						$list2type="upper-alpha";
					}else if($value['list2type']=='numbers'){
						$list2type="decimal";
					}else{
						$list2type="upper-alpha";
					}
					?>
					<div class="question">
						<div class="questionid">Q.Id:	<?php echo $value['question_id']; ?></div>
						<div class="question-header"><?php echo $value['mat_question']; ?></div>
							<br />
							<div class="questionlist-types d-flex">
									<div>
									<h5>List1</h5>
										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
											<?php
											foreach($obj as $qqlist)
											{
											
												if(strlen($qqlist['qlist1'])>0)
												{
													echo '<li style="width:150px;">'.$qqlist['qlist1'].'</li>';
												}
											}
											?>
										
										</ul>
									</div>
									<div class="pl-5">
										<h5>List2</h5>
										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
											<?php
											foreach($obj as $qqlist2)
											{
											
												if(strlen($qqlist2['qlist2'])>0)
												{
													echo '<li style="width:150px;">'.$qqlist2['qlist2'].'</li>';
												}
											}
											?>
										</ol>
									</div>
								</div>
							<div class="question-list">
								<div class="options ">
								<table>
								<tr>
								<td valign="top" class="option">A)</td>
								<td valign="bottom"><?php echo $value['option1']; ?></td>
</tr>
</table>

								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">B)</td>
								<td valign="bottom"><?php echo $value['option2']; ?></td>
</tr>
</table>
								</div>
                                <div class="clearall"></div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">C)</td>
								<td valign="bottom"><?php echo $value['option3']; ?></td>
</tr>
</table>
								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">D)</td>
								<td valign="bottom"><?php echo $value['option4']; ?></td>
								<?php
								if($value['option5']!=''){
								
								?>
									<td valign="top" class="option">E)</td>
								<td valign="bottom"><?php echo $value['option5']; ?></td>
								<?php }
								?>
</tr>
</table>
								</div>

								<div class="clearall"></div>
							</div>
						</div>
								
					</div>

			<?php	
			}else if($value1['qtype']=='8') { ?>
				<div class="questionid">Q.Id:	<?php echo $value['question_id']; ?></div>
				<div class="question">
					<div class="question-header">	<?php echo $value['question']; ?><span style="text-decoration: underline; white-space: pre;"></div>
						
						
					</div>
							
				</div>
			<?php
			}else  if($value1['qtype']=='5') { 
					$obj=json_decode($value['question'],true);
					
					?>
					<div class="question">
						<div class="questionid">Q.Id:	<?php echo $value['question_id']; ?></div>
						<div class="question-header">	<?php echo $value['compquestion']; ?></div>
						<br />
							<div><?php echo $value['question']; ?></div>
							
							<div class="question-list">
								<div class="options ">
								<table>
								<tr>
								<td valign="top" class="option">A)</td>
								<td valign="bottom"><?php echo $value['option1']; ?></td>
</tr>
</table>

								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">B)</td>
								<td valign="bottom"><?php echo $value['option2']; ?></td>
</tr>
</table>
								</div>
                                <div class="clearall"></div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">C)</td>
								<td valign="bottom"><?php echo $value['option3']; ?></td>
</tr>
</table>
								</div>
								<div class="options ">
									<table>
								<tr>
								<td valign="top" class="option">D)</td>
								<td valign="bottom"><?php echo $value['option4']; ?></td>
								<?php
								if($value['option5']!=''){
								
								?>
									<td valign="top" class="option">E)</td>
								<td valign="bottom"><?php echo $value['option5']; ?></td>
								<?php }
								?>
</tr>
</table>
								</div>

								<div class="clearall"></div>
							</div>
						</div>
								
					</div>

			<?php	
			}
			$k++;

			}
			
			?>
		</table>
	</div>
</div>


	</body>
</html>