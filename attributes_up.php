<?php

ini_set('display_errors','0');

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Question Id</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Topic</th>
							<th style="text-align:center;">Complexity</th>
							<th style="text-align:center;">Usageset</th>
							<th style="text-align:center;">Question Type</th>
						</tr>
                        <?php
                            $k=1;
                           
                               
                                $chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1  and subject='".$_REQUEST['sub']."'  ORDER BY id ASC");
								
                                while($chapter = mysqli_fetch_array($chapter_sel)){
                                  
									$sqll=query("select id,chapter,topic,complexity,usageset,inputquestion from createquestion where estatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0  and FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and complexity!=0 and usageset!=0 order by id ASC ");
									while($rowl=mysqli_fetch_array($sqll)){
									$topic_sel = query("SELECT * FROM topic WHERE estatus=1  and id='".$rowl['topic']."' ORDER BY subject ASC");
									$topic=mysqli_fetch_array($topic_sel);

									$compl_sel = query("SELECT * FROM complexity WHERE estatus=1 and id='".$rowl['complexity']."' ORDER BY id ASC");
									$comlexity=mysqli_fetch_array($compl_sel);

									$useset_sel = query("SELECT * FROM question_useset WHERE estatus=1 and id='".$rowl['usageset']."' ORDER BY id ASC");
									$usageset=mysqli_fetch_array($useset_sel);
									$qtype='';
									$qt_sel = query("SELECT * FROM questiontype WHERE estatus=1 and id In (".$rowl['inputquestion'].") ORDER BY id ASC");
									while($qtset=mysqli_fetch_array($qt_sel)){
										$qtype.=$qtset['questiontype'].",";
									}
									
									echo "<tr>";
										?>	
										
										<td ><?php echo $rowl['id'];?></td>
										<td><?php echo $chapter['chapter']; ?></td>
										<td><?php echo $topic['topic']; ?></td>
										<td><?php echo $comlexity['complexity']; ?></td>
										<td><?php echo $usageset['usageset']; ?></td>
										<td><?php echo rtrim($qtype,","); ?></td>
									<?php
									
									echo "</tr>";
										 
									}     
                                        
                                    
                                }
                                
                           

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>