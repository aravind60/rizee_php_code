<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Advanced set Report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Total Advanced</th>
							<th style="text-align:center;">Matrix / Matching</th>
							<th style="text-align:center;">Comprehension </th>
							<th style="text-align:center;">Numerical </th>
							<th style="text-align:center;">Multi Answer</th>
							<th style="text-align:center;">Practice</th>
							<!-- <th style="text-align:center;">Cumulative </th>
							<th style="text-align:center;">Semi Grand </th>
							<th style="text-align:center;"> Grand </th> -->
						</tr>
                        <?php
							/*$j=1;
                                $sql=query("select * from chapter where estatus='1' and subject in (2,3,4)      order by subject asc");
								while($row=mysqli_fetch_array($sql)){
									$sql1=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and  find_in_set(2,exam)>0 and pexamtype='2' and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset in (3,4,5)");
									$row1=mysqli_fetch_array($sql1);
									$sql2=query("select count(id) as cnt from createquestion where estatus='1'  and vstatus1='1'   and  find_in_set(2,exam)>0 and pexamtype='2' and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and qtype in (3,9) and usageset in (3,4,5)");
									$row2=mysqli_fetch_array($sql2);
									
									$sql3=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and qtype='5' and usageset in (3,4,5)");
									$row3=mysqli_fetch_array($sql3);
									$sql4=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and find_in_set(10,inputquestion)>0 and complexity in (3,5) and usageset in (3,4,5)");
									$row4=mysqli_fetch_array($sql4);
									

									$sql5=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and LENGTH(answer)>2 and complexity in (3,5) and usageset in (3,4,5)");
									$row5=mysqli_fetch_array($sql5);
//echo "select count(id) as cnt from createquestion where estatus='1' and  find_in_set(2,exam)>0 and pexamtype='2' and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0  and usageset='3'  AND  (find_in_set(3,inputquestion)>0 OR find_in_set(5,inputquestion)>0 OR find_in_set(9,inputquestion)>0 OR find_in_set(10,inputquestion)>0  OR complexity in (3,5) OR  LENGTH(answer)>2) ";"
									$sql6=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='3' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row6=mysqli_fetch_array($sql6);

									$sql7=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='4' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row7=mysqli_fetch_array($sql7);

									$sql8=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='5' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row8=mysqli_fetch_array($sql8);


										$sub= query("SELECT id,subject FROM subject WHERE estatus=1 AND id = '".$row['subject']."'");
										$rowsub = mysqli_fetch_array($sub);

										$qset= query("SELECT * FROM chapter WHERE estatus=1 AND id = '".$row['qset']."'");
										$rowqset = mysqli_fetch_array($qset);

										$total=$row6['cnt']+$row7['cnt']+$row8['cnt'];
													echo "<tr>";
														?>	
														
														<td><?php echo $j;?></td>
														<td><?php echo $rowsub['subject']; ?></td>
														<td ><?php echo $row['chapter'];?></td>
														<td ><?php echo $total;?></td>
														<td ><?php echo $row2['cnt'];?></td>
														<td ><?php echo $row3['cnt'];?></td>
														<td ><?php echo $row4['cnt'];?></td>
														<td ><?php echo $row5['cnt'];?></td>
														<td ><?php echo $row6['cnt'];?></td>
														<td ><?php echo $row7['cnt'];?></td>
														<td ><?php echo $row8['cnt'];?></td>
														<?php
														
														echo "</tr>";
														
																
										$j++;           
                                            
									//}
									
									
                               // }
                                
							}*/
							$j=1;
                                $sql=query("select * from chapter where estatus='1' and subject in (2,3,4)      order by subject asc");
								while($row=mysqli_fetch_array($sql)){
									$sql1=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset in (1)");
									$row1=mysqli_fetch_array($sql1);
									$sql2=query("select count(id) as cnt from createquestion where estatus='1'  and vstatus1='1'   and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and qtype in (3,9) and usageset in (1)");
									$row2=mysqli_fetch_array($sql2);
									
									$sql3=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and qtype='5' and usageset in (1)");
									$row3=mysqli_fetch_array($sql3);
									$sql4=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and find_in_set(10,inputquestion)>0 and complexity in (3,5) and usageset in (1)");
									$row4=mysqli_fetch_array($sql4);
									

									$sql5=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and  find_in_set(2,exam)>0  and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and LENGTH(answer)>2 and complexity in (3,5) and usageset in (1)");
									$row5=mysqli_fetch_array($sql5);
									//echo "select count(id) as cnt from createquestion where estatus='1' and  find_in_set(2,exam)>0 and pexamtype='2' and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0  and usageset='3'  AND  (find_in_set(3,inputquestion)>0 OR find_in_set(5,inputquestion)>0 OR find_in_set(9,inputquestion)>0 OR find_in_set(10,inputquestion)>0  OR complexity in (3,5) OR  LENGTH(answer)>2) ";"
									$sql6=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='1' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row6=mysqli_fetch_array($sql6);

								/*	$sql7=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='4' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row7=mysqli_fetch_array($sql7);

									$sql8=query("SELECT count(id) as cnt FROM `createquestion` WHERE estatus=1 AND vstatus1=1 and subject='".$row['subject']."' and find_in_set(".$row['id'].",chapter)>0 and usageset='5' AND (qtype IN(3,9,5) OR (find_in_set(10,inputquestion)>0 AND complexity in (3,5)) OR (LENGTH(answer)>2 AND complexity in (3,5)))");
									$row8=mysqli_fetch_array($sql8);*/


										$sub= query("SELECT id,subject FROM subject WHERE estatus=1 AND id = '".$row['subject']."'");
										$rowsub = mysqli_fetch_array($sub);

										$qset= query("SELECT * FROM chapter WHERE estatus=1 AND id = '".$row['qset']."'");
										$rowqset = mysqli_fetch_array($qset);

										$total=$row6['cnt']+$row7['cnt']+$row8['cnt'];
													echo "<tr>";
														?>	
														
														<td><?php echo $j;?></td>
														<td><?php echo $rowsub['subject']; ?></td>
														<td ><?php echo $row['chapter'];?></td>
														<td ><?php echo $total;?></td>
														<td ><?php echo $row2['cnt'];?></td>
														<td ><?php echo $row3['cnt'];?></td>
														<td ><?php echo $row4['cnt'];?></td>
														<td ><?php echo $row5['cnt'];?></td>
														<td ><?php echo $row6['cnt'];?></td>
														<?php
														
														echo "</tr>";
														
																
										$j++;           
                                            
									//}
									
									
                               // }
                                
							}

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>