<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter Name</th>
							<th style="text-align:center;">Chapter Id</th>
							<th style="text-align:center;">Questions Count</th>
							<th style="text-align:center;">Verified Questions</th>
							<th style="text-align:center;">Rejected Questions</th>
							<th style="text-align:center;">Pending Questions</th>
							<th style="text-align:center;">Total Questions with  Topic </th>
							<th style="text-align:center;">Verified Questions with Topic </th>
							<th style="text-align:center;">Rejected Questions with Topic </th>
							<th style="text-align:center;">Pending Questions with topic </th>
							<th style="text-align:center;">Total Questions with out Topic </th>
							<th style="text-align:center;">Verified Questions without Topic </th>
							<th style="text-align:center;">Rejected Questions without Topic </th>
							<th style="text-align:center;">Pending Questions without topic </th>
							<?php
							$sel=query("select * from customcontent_types where estatus='1'");
							while($row=mysqli_fetch_array($sel)){ ?>
								<th style="text-align:center;"><?php echo $row['customcontent']; ?></th>
							<?php }
							?>
						</tr>
                        <?php
                            $k=1;
                           
                               
                                $chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1 ORDER BY subject ASC");
								
                                while($chapter = mysqli_fetch_array($chapter_sel)){
                                    $class_sel=query("SELECT id,class FROM class WHERE estatus=1 and id ='".$chapter['class']."'");
									$class_row = mysqli_fetch_array($class_sel);
											
									$sub= query("SELECT * FROM subject WHERE estatus=1 AND id ='".$chapter['subject']."'");
									$rowsub = mysqli_fetch_array($sub);

									$sqll=query("select count(id) as count from createquestion where estatus='1'   and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")  order by subject ASC ");
									$rowlcount=mysqli_fetch_array($sqll);

									$sqlltv=query("select count(id) as count from createquestion where estatus='1' and vstatus='1'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")  order by subject ASC ");
									$rowlcounttv=mysqli_fetch_array($sqlltv);

									$sqlltr=query("select count(id) as count from createquestion where estatus='1' and vstatus='2'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")  order by subject ASC ");
									$rowlcounttr=mysqli_fetch_array($sqlltr);

									$sqlltp=query("select count(id) as count from createquestion where estatus='1' and vstatus='0'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")  order by subject ASC ");
									$rowlcounttp=mysqli_fetch_array($sqlltp);

									$topic_sqll=query("select count(id) as count from createquestion where estatus='1'   and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic='' order by subject ASC ");
									$topictrow=mysqli_fetch_array($topic_sqll);

									$sqlltv1=query("select count(id) as count from createquestion where estatus='1' and vstatus='1'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic=''  order by subject ASC ");
									$rowlcounttv1=mysqli_fetch_array($sqlltv1);

									$sqlltr1=query("select count(id) as count from createquestion where estatus='1' and vstatus='2'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic=''  order by subject ASC ");
									$rowlcounttr1=mysqli_fetch_array($sqlltr1);

									$sqlltp1=query("select count(id) as count from createquestion where estatus='1' and vstatus='0'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")   and topic='' order by subject ASC ");
									$rowlcounttp1=mysqli_fetch_array($sqlltp1);


									$topic_sqllw=query("select count(id) as count from createquestion where estatus='1'   and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic!='' order by subject ASC ");
									$topictroww=mysqli_fetch_array($topic_sqllw);

									$sqlltv2=query("select count(id) as count from createquestion where estatus='1' and vstatus='1'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic!=''  order by subject ASC ");
									$rowlcounttv2=mysqli_fetch_array($sqlltv2);

									$sqlltr2=query("select count(id) as count from createquestion where estatus='1' and vstatus='2'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].") and topic!=''  order by subject ASC ");
									$rowlcounttr2=mysqli_fetch_array($sqlltr2);

									$sqlltp2=query("select count(id) as count from createquestion where estatus='1' and vstatus='0'  and class in (".$chapter['class'].") and subject='".$chapter['subject']."' and chapter in (".$chapter['id'].")   and topic!='' order by subject ASC ");
									$rowlcounttp2=mysqli_fetch_array($sqlltp2);
									
									//	$rowl=mysqli_fetch_array($sqll);
									//	if($rowl['count']!=0){
										echo "<tr>";
											?>	
											
											<td><?php echo $k;?></td>
											<td ><?php echo $class_row['class'];?></td>
											<td><?php echo $rowsub['subject']; ?></td>
											<td><?php echo $chapter['chapter'];?></td>
											<td><?php echo $chapter['id'];?></td>
											<td><?php echo $rowlcount['count']; ?></td>
											<td><?php echo $rowlcounttv['count']; ?></td>
											<td><?php echo $rowlcounttr['count']; ?></td>
											<td><?php echo $rowlcounttp['count']; ?></td>

											<td><?php echo $topictroww['count']; ?></td>
											<td><?php echo $rowlcounttv2['count']; ?></td>
											<td><?php echo $rowlcounttr2['count']; ?></td>
											<td><?php echo $rowlcounttp2['count']; ?></td>

											<td><?php echo $topictrow['count']; ?></td>
											<td><?php echo $rowlcounttv1['count']; ?></td>
											<td><?php echo $rowlcounttr1['count']; ?></td>
											<td><?php echo $rowlcounttp1['count']; ?></td>
											<?php
												$sell= query("SELECT * FROM customcontent_types WHERE estatus=1 ");
												while($rowll = mysqli_fetch_array($sell)){ 
													
													//$sell1= query("SELECT count(id) as cnt  FROM createquestion WHERE estatus=1  and FIND_IN_SET(".$chapter['class'].",class)  > 0 and FIND_IN_SET(".$chapter['subject'].",subject)  > 0 and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$rowll['id'].",conttype)  > 0  ");
												//$rowll1 = mysqli_fetch_array($sell1); 
												$sell1= query("SELECT count(id) as cnt  FROM customcontent WHERE estatus=1  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$chapter['class'].",class)  > 0 and FIND_IN_SET(".$chapter['subject'].",subject)  > 0 and conttype='".$rowll['id']."' ");
												$rowll1 = mysqli_fetch_array($sell1); 
												?>
													<td ><?php echo $rowll1['cnt'];?></td>
												<?php }   ?>
										<?php
										echo "</tr>";
										 $k++;
									               
                                                            
                                        
									//	}       
                                        
                                    
                                }
                                
                           

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>