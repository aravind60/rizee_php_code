<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*
define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "customcontenttypes".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sl.No.</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter Name</th>
							<th style="text-align:center;">Topic Name</th>
							<?php
							$sel=query("select * from customcontent_types where estatus='1'");
							while($row=mysqli_fetch_array($sel)){ ?>
								<th style="text-align:center;"><?php echo $row['customcontent']; ?></th>
							<?php }
							?>
							
						</tr>
						
						<?php
						$k=1;
							$topic_sel = query("SELECT id,class,subject,chapter,topic FROM topic WHERE estatus=1 ORDER BY class,subject ASC");
							 while($topic = mysqli_fetch_array($topic_sel)){
								 $class_sel=query("SELECT id,class FROM class WHERE estatus=1 and id ='".$topic['class']."'");
								$class_row = mysqli_fetch_array($class_sel);
										
								$sub= query("SELECT * FROM subject WHERE estatus=1 AND id ='".$topic['subject']."'");
								$rowsub = mysqli_fetch_array($sub);

								$chapter= query("SELECT * FROM chapter WHERE estatus=1 AND id ='".$topic['chapter']."'");
								$rowchap = mysqli_fetch_array($chapter);

								
								?>
									<tr>
										<td><?php echo $k;?></td>
										<td ><?php echo $class_row['class'];?></td>
										<td ><?php echo $rowsub['subject'];?></td>
										<td ><?php echo $rowchap['chapter'];?></td>
										<td ><?php echo $topic['topic'];?></td>
										<?php
										$sell= query("SELECT * FROM customcontent_types WHERE estatus=1 ");
										while($rowll = mysqli_fetch_array($sell)){ 

											$sell1= query("SELECT count(id) as cnt  FROM customcontent WHERE estatus=1  and FIND_IN_SET(".$topic['chapter'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and conttype='".$rowll['id']."' ");
										$rowll1 = mysqli_fetch_array($sell1); 
										?>
											<td ><?php echo $rowll1['cnt'];?></td>
										<?php }   ?>
									</tr>
								<?php
									$k++;
							 }
						?>
						
                        
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>