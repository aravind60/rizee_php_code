<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}

$conn1=mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
function query1($sql)
{
    global $conn1;


    return mysqli_query($conn1,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;

$fileName = "zoology Linkage_Questions.xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							 <th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter </th>
							<th style="text-align:center;">Topic</th>
							<th style="text-align:center;">Question ID</th>
							<th style="text-align:center;">Question Type</th>
							<th style="text-align:center;">Verification Status</th>
							 <th style="text-align:center;">Verifier </th>
							<th style="text-align:center;">Review Status</th>
							<th style="text-align:center;">Reviewer</th>

							<!-- <th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Question ID</th> -->
						</tr>
						</tr>
						<?php
						
					

						$k=1;
						 $sel=query("select id,subject,chapter,topic,vusername1,vstatus1,vtimestamp1,rusername,review_status,rtimestamp,inputquestion from createquestion where estatus='1' and find_in_set(1,inputquestion)>0 and vstatus1='1' and subject='5'   order by subject asc ");
						 while($rowdata=mysqli_fetch_array($sel)){
							
								$sub= query("SELECT id,subject FROM subject WHERE estatus=1 AND id = '".$rowdata['subject']."'");
								$rowsub = mysqli_fetch_array($sub);
								$chapdata='';	
									$chap= query("SELECT id,subject,chapter FROM chapter WHERE estatus=1 AND id IN (".rtrim($rowdata['chapter'],",").")");
									while($rowchap = mysqli_fetch_array($chap)){
										$chapdata.=$rowchap['chapter'].",";
									}
									$topicdata='';	
									$chap1= query("SELECT * FROM topic WHERE estatus=1 AND id IN (".rtrim($rowdata['topic'],",").")");
									while($rowchap1 = mysqli_fetch_array($chap1)){
										$topicdata.=$rowchap1['topic'].",";
									}

									$qtype='';	
									$qtyped= query("SELECT * FROM questiontype WHERE estatus=1 AND id IN (".rtrim($rowdata['inputquestion'],",").")");
									while($rowqty1 = mysqli_fetch_array($qtyped)){
										$qtype.=$rowqty1['questiontype'].",";
									}

									if($rowdata['vstatus1']=='1'){
										$vstatus1="Verified";
									}else if($rowdata['vstatus1']=='2'){
										$vstatus1="Rejected";
									}else{
										$vstatus1="Pending";
									}

									if($rowdata['review_status']=='1'){
										$review_status="Verified";
									}else if($rowdata['review_status']=='2'){
										$review_status="Rejected";
									}else{
										$review_status="Pending";
									}
									if($rowdata['vstatus1']=='1'){
										$vstatus1="Verified";
									}else if($rowdata['vstatus1']=='2'){
										$vstatus1="Rejected";
									}else{
										$vstatus1="Pending";
									}
									?>
								<tr>
									<td ><?php echo $k;?></td>
									<td><?php echo $rowsub['subject'];?></td>
									<td><?php echo rtrim($chapdata,",");?></td>
									<td><?php echo rtrim($topicdata,",");?></td>
									<td ><?php echo $rowdata['id'];?></td>
									<td ><?php echo $vstatus1;?></td>
									<td ><?php echo rtrim($qtype,",");?></td>
									<td ><?php echo $rowdata['vusername1'];?></td>
									
									<td ><?php echo $review_status;?></td>
									<td ><?php echo $rowdata['rusername'];?></td>
								</tr> 
									<?php
									$k++;
							
						 }
							/* $k=1;
							 $chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1  ORDER BY id ASC");
							 while($chapter = mysqli_fetch_array($chapter_sel)){
							 $sel=query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(1,inputquestion)>0 and  find_in_set(".$chapter['id'].",chapter)>0");
							 $row=mysqli_fetch_array($sel);
							 if($row['cnt']>0){
								$chap=array();
								$i=0;
								
								$sel1=query("select id,chapter from createquestion where estatus='1' and find_in_set(1,inputquestion)>0 and  find_in_set(".$chapter['id'].",chapter)>0");
								while($row1=mysqli_fetch_array($sel1)){
									$chapter_sel1 = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1 and id in (".$row1['chapter'].") ORDER BY id ASC");
										while($chapter1 = mysqli_fetch_array($chapter_sel1)){
											$chap[]=$chapter1['id'];
											$i++;
										}

								}
								$class_sel=query("SELECT id,class FROM class WHERE estatus=1 and id='".$chapter['class']."' ORDER BY id ASC");
								$class_row = mysqli_fetch_array($class_sel);

								$sub= query("SELECT id,subject FROM subject WHERE estatus=1 AND id = '".$chapter['subject']."'");
								$rowsub = mysqli_fetch_array($sub);
								
								
								$data=array_unique($chap);
								sort($data);
								
								$chapters1=implode(",",$data);
								
								$chapterid='';
								$chapterdata='';
								$jk=0;
								$j=1;
								$selch=query("select * from chapter where estatus='1' and id in (".rtrim($chapters1,",").") and id!='".$chapter['id']."'");
								while($rowch=mysqli_fetch_array($selch)){
									$chapterid.=$j.". ".$rowch['chapter'].",<br />";
									$chapterdata.=$rowch['id'].",";
									$jk++;
									$j++;
								}
								echo "<tr>";
								?>	
								
								<td><?php echo $k;?></td>
									<td ><?php echo $class_row['class'];?></td>
									<td><?php echo $rowsub['subject']; ?></td>
									<td ><?php echo $chapter['chapter'];?></td>
									<td ><?php echo $chapter['id'];?></td>
									 <td ><?php echo $row['cnt'];?></td>
									 <td ><?php echo $jk;?></td>
									 <td ><?php echo rtrim($chapterdata,",");?></td>
									 <td nowrap><?php echo rtrim($chapterid,",");?></td>
								<?php
								echo "</tr>";
								$k++;

							 }
						}*/
                          
                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>