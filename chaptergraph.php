<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');


//error_reporting(-1);
ini_set('max_execution_time', '-1');
//ini_set('display_errors',"on");

$conn=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;


function query($sql)
{
    global $conn;
    return mysqli_query($conn,$sql);
}


if(isset($_REQUEST['practiceTimeline'])){

    $unix = 0;
    $practiceitem = [];

        $timeline = 1;//$_REQUEST['timeline'];
        $mobile = $_REQUEST['mobile'];


      $unix = time() - 7 * 24 * 3600;


      $topic=0;
      $chapter = 0;
      $subject = 0;


      if(isset($_REQUEST['chapter'])){
          $chapter = $_REQUEST['chapter'];
      }

      if(isset($_REQUEST['topic'])){
          $topic = $_REQUEST['topic'];
      }

      if(isset($_REQUEST['subject'])){
          $subject = $_REQUEST['subject'];
      }


      $graphqdata = array();

      for($time = $unix,$i=0; $time <= time(); $time+= 24*3600,$i++){


          $val = 0;
          $cval = 0;
          $wval = 0;

          if($topic == 0) {

             $sql = 'select COALESCE(SUM(correct),0) as correct,COALESCE(SUM(wrong),0) as wrong from student_sessions where mobile='.$mobile.' AND timestamp >= '.$time.' AND timestamp < '.($time +  24 * 3600).' AND subject='.$subject;

            if ($chapter > 0) {

                $sql = 'select COALESCE(SUM(correct),0) as correct,COALESCE(SUM(wrong),0) as wrong from student_sessions where mobile='.$mobile.' AND timestamp >= '.$time.' AND timestamp < '.($time +  24 * 3600).' AND subject='.$subject.' AND chapter='.$chapter.' LIMIT 0,50';
            }

            $data_sel = query($sql);

            if(mysqli_num_rows($data_sel) > 0){
                $data = mysqli_fetch_assoc($data_sel);
                $cval = $data['correct'];
                $wval = $data['wrong'];
            }

         }
         else{

             $sql = 'SELECT COUNT(DISTINCT question_id) as cnt,status FROM student_questions WHERE session_id !=0 AND estatus=1 AND mobile='.$mobile.' AND FIND_IN_SET('.$topic.', topic) > 0 AND status IN(1,2) AND timestamp >= '.$time.' AND timestamp < '.($time + 24 * 3600).'  AND subject='.$subject.' GROUP BY status';

             $data_sel = query($sql);
             if(mysqli_num_rows($data_sel) > 0){

                 while($data = mysqli_fetch_assoc($data_sel)){
                     if($data['status'] == 1)
                         $wval = $data['cnt'];
                     if($data['status'] == 2)
                         $cval = $data['cnt'];

                 }
             }
         }

         $val = $cval+$wval;


          $graphqdata[$i]['value'] = $val;
          $graphqdata[$i]['correct'] = $cval;
          $graphqdata[$i]['wrong'] = $wval;
          $graphqdata[$i]['timestamp'] = $time+24*3600;



      }


      echo json_encode($graphqdata);
}

?>