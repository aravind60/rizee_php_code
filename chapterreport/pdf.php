<?php
include('../include/session.php');
error_reporting(0);


function filterqa($str){
    $str = urldecode($str);
    $res = str_replace("&nbsp;","",$str);

    return strip_tags($res,'<p><img><maction><math><menclose><merror><mfenced><mfrac><mglyph><mi><mlabeledtr><mmultiscripts><mn><mo><mover><mpadded><mphantom><mroot><mrow><ms><mspace><msqrt><mstyle> <msub><msubsup><msup><mtable><mtd><mtext><mtr><munder><munderover><semantics>');

}

$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;



if(isset($_GET['class'])){
	if($_GET['class']!=""){
		$class=" AND class IN (".$_GET['class'].")";
	} else {
		$class="";
	}
}else{
	$class="";
}
if(isset($_GET['subject'])){
	if($_GET['subject']!=""){
		$subject=" AND subject='".$_GET['subject']."'";
	} else {
		$subject="";
	}
}else{
	$subject="";
}

if(isset($_GET['chapter'])){
	if($_GET['chapter']!=""){
		$chapter=" AND id IN (".$_GET['chapter'].")";
	} else {
		$chapter="";
	}
}else{
	$chapter="";
}


	
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<script async src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>


		<!-- 
<script async src="../edut/tinymce4/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>

Bootstrap CSS -->
		<link href="<?php echo SECURE_PATH;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
		<style >

            @media print {
                @page { margin: 0.5cm 0cm;
                 size: A4 }
                body { margin: .3cm; }
            }
			.page{
				width:26cm;
				margin:auto;
				padding:15px;

			}
			.table td, .table th {
    padding: .3rem;
}
.table thead th {
    border-bottom:2px solid #000;
}
            p:empty {
                display: none;
            }


			.d-flex {
				display: -webkit-box!important;
				display: -ms-flexbox!important;
				display: flex!important;
			}
			.pdf-content .q-no {
				font-size:20px;
				font-family: 'Source Sans Pro', sans-serif !important;
			}

			.pdf-content  .question-header {
				padding-left:5px;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                padding-bottom: 16px
			}
			
			.pdf-content  .question-header p ,
			.pdf-content  .question-header p span{
                margin-block-end: 0.1em;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size: 18px !important;
				font-weight: 600 !important;
			}

            .question-header img{
                max-width:75%;
            }

			.pdf-content p{
			margin-block-start: 0;
    /*margin-block-end: 0;*/
			}

			.question-list  {

			padding-left:5px;
			font-family: 'Source Sans Pro', sans-serif !important;
			font-size:14px !important;
				}
			.question-list .options {
				/* align-items: center; */
				justify-content: center;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                display:inline-block !important;
				width:45%;
				float:left;
                margin-right:16px;
			}
			.pdf-content p, span,
			.pdf-content .question-list p {
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;

			}

			.question-list .options p{
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                margin-block-end: 0em;
			}
            .question-list .options table{
                min-width:240px;
            }

            .question-list .options p img{
                max-width:77%;
            }
			.MsoListParagraph span span[style] {
				display:none;
			}


			.option{
			font-weight:bold;
			padding-left: 8px;
			}

            .question{
                padding-bottom:16px;
            }

            .clearall{
                clear:both;
                padding:0;margin:0;
            }
			.chapterborder{
				text-align:center;
			}
			.table-bordered td, .table-bordered th {
    border: 2px solid #101923;
}

           
		</style>
	</head>
	<body>
	<?php
		if(isset($_GET['exam'])){
			if($_GET['exam']!=""){
				$exam=" AND exam in (".$_GET['exam'].") ";
			} else {
				$exam="";
			}
		}else{
			$exam="";
		}
	?>

		<div class="page">
			<div class="text-center pdf-content">
				<h4 class="" >Chapter Wise Report</h4>
			</div>
			<?php
			$k=1;
			$sel=$database->query("select * from chapter where estatus='1'".$class.$subject.$chapter." order by class ASC");
			$rowcount=mysqli_num_rows($sel);
			if($rowcount>0){
			?>

				<table class="table table-bordered chapter">
					<thead>
						<tr class="chapterborder">
							<th rowspan="3" >Sr.No</th>
							<th rowspan="3" >Class</th>
							<th rowspan="3" >Subject</th>
							<th rowspan="3">Chapter</th>
							<th colspan="3"> Questions Count</th>
							
							<th colspan="2" >Assigned Users</th>
						</tr>
						<tr class="chapterborder">
							<th>Total</th>
							<th>Verified</th>
							<th>Pending</th>
							<th>Dataentry</th>
							<th>Lecturer</th>
						</tr>
					</thead>
					<tbody>
						<?php
					$i=1;
						while($row=mysqli_fetch_array($sel)){
							$seltot=$database->query("select chapter_total,chapter_verify,chapter_pen from syllabus_totals_new where  FIND_IN_SET(".$row['id'].",chapter)  order by chapter ASC");
							$rowtot=mysqli_fetch_array($seltot);
						?>
							<tr class="table-data-rows">
								 <td><?php echo $i;?></td>
								 <td ><?php echo $database->get_name('class','id',$row['class'],'class'); ?> </td>
								<td><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?> </td>
								<td style="width:40%"><?php echo $row['chapter'];?></td>
								<td><?php  echo $rowtot['chapter_total']; ?></td>
								<td><?php echo $rowtot['chapter_verify']; ?></td>
								<td><?php echo $rowtot['chapter_pen']; ?></td>
								<td>
								<?php 
									$j=1;
									$selusers=$database->query("select * from users where valid='1'  and FIND_IN_SET(".$row['id'].",chapter)  > 0 and userlevel='7' ");
									while($rowusers=mysqli_fetch_array($selusers)){
										$user=$rowusers['username'];
										echo $j.".".$user."<br />";
										$j++;

									}
								?>
								 </td>
								 <td>
								<?php 
									$jk=1;
									$selusers=$database->query("select * from users where valid='1'  and FIND_IN_SET(".$row['id'].",chapter)  > 0 and userlevel='3' ");
									while($rowusers=mysqli_fetch_array($selusers)){
										$user=$rowusers['username'];
										echo $jk.".".$user."<br />";
										$jk++;

									}
								?>
								 </td>
								
								
							  </tr>
						<?php
								  $i++;
						}
						?>
					</tbody>
				</table>
			<?php } ?>
			
	</div>



	</body>
</html>