<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

?>


		<script>
		
		
		function search_report() {
		
			var class1 = $('#class').val();
			var subject = $('#subject').val();
			var chapter = $('#chapter').val();
			var username = $('#username').val();
			setStateGet('adminTable','<?php echo SECURE_PATH;?>chapterreport/process.php','tableDisplay=1&class='+class1+'&subject='+subject+'&chapter='+chapter+'&username='+username);
		}
	</script>
		</script>
	<?php
                                 
if(isset($_GET['tableDisplay'])){
	$_REQUEST['class1']=$_REQUEST['class']; 
	$_REQUEST['subject1']=$_REQUEST['subject']; 
	$_REQUEST['chapter1']=$_REQUEST['chapter']; 
	?>
		<div class="card shadow-sm mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Chapter Wise Report</h6>
		</div>
        <div class="card-body">
			<div class="row">
				<?php //?>
				<div class="col-lg-3">
					
				 <label for="inputTopi" class="label">Class</label>
				
					<select class="form-control selectpicker1" data-live-search="true"  name="class" value=""  multiple id="class" onchange="search_report();">
						<?php
						$_REQUEST['class']=explode(",",$_REQUEST['class']); 
						$sel=$database->query("select * from class where estatus='1' ");
						while($row=mysqli_fetch_array($sel)){
						?>
							<option value="<?php echo $row['id'];?>"  <?php if(isset($_REQUEST['class'])) { if(in_array($row['id'], $_REQUEST['class'])) { echo 'selected="selected"'; } } ?>><?php echo $row['class'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
						
				</div>
				<?php $_REQUEST['subject']=explode(",",$_REQUEST['subject']); ?>
				<div class="col-lg-3">
					
						<label for="inputTopi" class="label">Subject</label>
						
							<select class="form-control selectpicker2"  data-live-search="true" name="subject" value=""  id="subject"  multiple onchange="search_report();setState('aaa','<?php echo SECURE_PATH;?>chapterreport/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
								<?php
								$sel=$database->query("select * from subject where estatus='1' ");
								while($row=mysqli_fetch_array($sel)){
								?>
									<option value="<?php echo $row['id'];?>"  <?php if(isset($_REQUEST['subject'])) { if(in_array($row['id'], $_REQUEST['subject'])) { echo 'selected="selected"'; } } ?>><?php echo $row['subject'];?></option>
								<?php
								}
								?>
							</select>
								
							</select>
							<span class="error"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
						
				</div>
				<?php $_REQUEST['chapter']=explode(",",$_REQUEST['chapter']); ?>
				<div class="col-lg-3" id='aaa'>
					
						<label for="inputTopi" class="label">Chapter</label>
					
							<select class="form-control selectpicker2" data-live-search="true"  name="chapter" value=""   id="chapter" multiple onchange="search_report();">
								<?php
								$sel=$database->query("select * from chapter where estatus='1' ");
								while($row=mysqli_fetch_array($sel)){
								?>
									<option value="<?php echo $row['id'];?>"  <?php if(isset($_REQUEST['chapter'])) { if(in_array($row['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>><?php echo $row['chapter'];?></option>
								<?php
								}
								?>
								
							</select>
							<span class="error"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
						
				</div>
<?php //$_REQUEST['username']=explode(",",$_REQUEST['username']); ?>
				<div class="col-lg-3" >
					
						<label for="inputTopi" class="label">Username</label>
					
							<select class="form-control selectpicker2" data-live-search="true"  name="username" value=""   id="username" multiple onchange="search_report();">
								<?php
								$sel=$database->query("select * from users where valid='1' and userlevel='3'");
								while($row=mysqli_fetch_array($sel)){
								?>
									<option value="<?php echo $row['username'];?>"  <?php if(isset($_REQUEST['username'])) { if(in_array($row['username'], explode(",",$_REQUEST['username']))) { echo 'selected="selected"'; } } ?>><?php echo $row['username'];?></option>
								<?php
								}
								?>
								
							</select>
							<span class="error"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
						
				</div>
				
				
			</div>
	<?php
$limit=50;
if(isset($_GET['page']))
{
	if($_GET['page']!='0'){
		$start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
		$page=$_GET['page'];
	}else{
		$start = 0;      //if no page var is given, set start to 0
		$page=0;
	}
}
else
{
	$start = 0;      //if no page var is given, set start to 0
	$page=0;
}


//Search Form

$tableName = 'chapter';
$condition=" estatus='1'";
if(isset($_GET['keyword'])){
}

if(isset($_REQUEST['class1'])){
		if($_REQUEST['class1']!=""){
			$class=" AND class  IN (".$_REQUEST['class1'].")";
		} else {
			$class="";
		}
	}else{
		$class="";
	}
	if(isset($_REQUEST['subject1'])){
		if($_REQUEST['subject1']!=""){
			$subject=" AND subject  IN (".$_REQUEST['subject1'].")";
		} else {
			$subject="";
		}
	}else{
		$subject="";
	}
	if(isset($_REQUEST['chapter1'])){
		if($_REQUEST['chapter1']!=""){
			$chapter=" AND id  IN (".$_REQUEST['chapter1'].")";
		} else {
			$chapter="";
		}
	}else{
		$chapter="";
	}
	$chaptercon='';
	if(isset($_REQUEST['username'])){
		if($_REQUEST['username']!=""){
			$username=" AND username  IN (".$_REQUEST['username'].")";
			$user1='';
			$user=explode(",",$_REQUEST['username']);
			foreach($user as $userdata1){
				$user1.="'".$userdata1."'".",";
				
			}
			$user2=rtrim($user1,",");
			$chap='';
			$sello1=$database->query("select * from users where valid='1' and username in (".$user2.") ");
			while($rowlo=mysqli_fetch_array($sello1)){
				$chap.=$rowlo['chapter'].",";
			}
			$chapdata3=explode(",",rtrim($chap,","));
			$chapdata4=array_unique($chapdata3);
			foreach($chapdata4 as $chapdata41){
				$user1chap.=$chapdata41.",";
				
			}
			$chaptercon=" AND id in (".rtrim($user1chap,",").")";
		} else {
			$username="";
			$chaptercon="";
		}
	}else{
		$username="";
		$chaptercon="";
	}
	
	
	$con=$class.$subject.$chapter;
if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition.$con.$chaptercon;
  }
 
$pagination = $session->showPagination(SECURE_PATH."chapterreport/process.php?tableDisplay=1&pagerows=".$_REQUEST['pagerows']."&start=".$start."&limit=".$limit."&",$tableName,$start,$limit,$page,$condition);
$q = "SELECT * FROM $tableName ".$condition."";
$result_sel = $database->query($q);
$numres = mysqli_num_rows($result_sel);

$query = "SELECT * FROM $tableName ".$condition." order by subject ASC ";

$data_sel = $database->query($query);
 if(($start+$limit) > $numres){
     $onpage = $numres;
     }
     else{
      $onpage = $start+$limit;
     }
 if($numres > 0){ 

?>
	<script type="text/javascript">
  $('#dataTable').DataTable({
    "pageLength": 50
    
  });
   
</script>
	
				<div class="col-md-12 pt-3">
					
					<a href="<?php echo SECURE_PATH;?>chapterreport/report.php?class=<?php echo $_REQUEST['class1']; ?>&subject=<?php echo $_REQUEST['subject1']; ?>&chapter=<?php echo $_REQUEST['chapter1']; ?>"target="_blank"  title="PDF Export" ><i class="fa fa-file-pdf-o" style="float:right;font-size: 25px;color:red;"></i></a>
				</div>
                        <div class="table_data table-responsive">
                            <table class="table table-bordered dashboard-table mb-0"  id="dataTable">
                                <thead class="thead-light thead-style">
                                    <tr>
                                        <th scope="col">Sr.No</th>
										<th scope="col">Class</th>
										<th scope="col">Subject</th>
                                        <th scope="col">Chapter</th>
										<th scope="col">Total Questions</th>
										<th scope="col">Verified</th>
										<th scope="col">Pending</th>
										<th>Assigned Users</th>
                                       
                                    </tr>
									
                                </thead>
                                <tbody>
                                  <?php
                              
									if(isset($_GET['page'])){
										if($_GET['page']!='0'){
											if($_GET['page']==1)
											$i=1;
											else
											$i=(($_GET['page']-1)*($limit))+1;
										}else{
											$i=1;
										}
									}else $i=1;
										while($row= mysqli_fetch_array($data_sel)){
											

											$seltot=$database->query("select chapter_total,chapter_verify,chapter_pen from syllabus_totals_new where  FIND_IN_SET(".$row['id'].",chapter)  order by chapter ASC");
											$rowtot=mysqli_fetch_array($seltot);
						
											
										   ?>      
											<tr class="table-data-rows">
												 <td><?php echo $i;?></td>
												 <td class="text-left"><?php echo $database->get_name('class','id',$row['class'],'class'); ?> </td>
												<td class="text-left"><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?> </td>
												<td><?php echo $row['chapter'];?></td>
												<td><?php  echo $rowtot['chapter_total']; ?></td>
												<td><?php echo $rowtot['chapter_verify']; ?></td>
												<td><?php echo $rowtot['chapter_pen']; ?></td>
												
												 <td>
												<?php 
													$jk=1;
													$selusers=$database->query("select * from users where valid='1'  and FIND_IN_SET(".$row['id'].",chapter)  > 0 and userlevel='3' ");
													while($rowusers=mysqli_fetch_array($selusers)){
														$user=$rowusers['username'];
														echo $jk.".".$user."<br />";
														$jk++;

													}
												?>
												 </td>
												
												
											  </tr> 
                                    <?php  $i++; } ?>
                                </tbody>
                            </table>
                        </div>
                       </div>
                    </div> 

          
<?php 
 }
}


?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
 
	
		<label for="inputTopic">Chapter</label>
		<!-- <div class="col-sm-8"> -->
   			<select name="chapter" id="chapter"  style="width:45%"  multiple data-live-search="true"  class="form-control selectpicker4" >
    			

            		<?php
					if($_REQUEST['class']!='' && $_REQUEST['subject']!=''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($_REQUEST['class'],",").") and subject IN (".rtrim($_REQUEST['subject'],",").") "); 
					}else if($_REQUEST['class']!='' && $_REQUEST['subject']==''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($_REQUEST['class'],",").") "); 
					}else if($_REQUEST['class']=='' && $_REQUEST['subject']!=''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and  subject IN (".rtrim($_REQUEST['subject'],",").")"); 
					}else{
						$zSql = $database->query("SELECT * FROM `chapter` WHERE  estatus='1'");
					}

					
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'];?>"  <?php if(isset($_REQUEST['chapter'])) { if(in_array($zData['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>><?php echo $zData['chapter'];?></option>
							
							<?php
						}
					}
					?>
   				</select>
			<!-- </div>
					 -->
    <?php
}
?>
<script>
	$(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
 $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
 </script>




