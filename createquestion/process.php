<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
header("Content-type:application/json"); 
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>
<script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	$_SESSION['error1'] = array();
		$class1=trim($_REQUEST['class']);
		$class2=rtrim($class1,",");
		$exam1=trim($_REQUEST['exam']);
		$exam2=rtrim($exam1,",");
		$data_seluser =  $database->query("select * from users where username='".$session->username."'");
		$rowuser = mysqli_fetch_array($data_seluser);
		if(!$_REQUEST['class'] || strlen(trim($_REQUEST['class'])) == 0){

			$_SESSION['error1']['class'] = "*Please Select Class";

		}
		if(!$_REQUEST['exam'] || strlen(trim($_REQUEST['exam'])) == 0){

			$_SESSION['error1']['exam'] = "*Please Select Exam";

		}
		
		if(!$_REQUEST['subject'] || strlen(trim($_REQUEST['subject'])) == 0){

			$_SESSION['error1']['subject'] = "*Please Select subject";

		}
		if($_REQUEST['ptype']==''){
			if($rowuser['nochapter']!='1'){
				if(!$_REQUEST['chapter'] || strlen(trim($_REQUEST['chapter'])) == 0){

					$_SESSION['error1']['chapter'] = "*Please Select chapter";

				}
				if($class2=='1,2'){
					$c=explode(",",$_REQUEST['chapter']);
					if(count($c)<2){
						$_SESSION['error1']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
			}
		}else{
		}
		
		
		if(isset($_POST['class'])) {
			if($_POST['class']!=''){
				$_POST['class']=rtrim($_POST['class'],",");
			}else{
				$_POST['class']=rtrim($rowuser['class'],",");
			}
		}else{
			$_POST['class']=rtrim($rowuser['class'],",");
		}
		if(isset($_POST['subject'])) {
			if($_POST['subject']!=''){
				$_POST['subject']=$_POST['subject'];
			}else{
			}
		}else{
		}
		if(isset($_POST['chapter'])) {
			if($_POST['chapter']!=''){
				$_POST['chapter']=rtrim($_POST['chapter'],",");
			}else{
			}
		}else{
		}
		?>
			<script>
			getFields4();
			getFields5();
			</script>
		<?php
		
		if(count($_SESSION['error1']) > 0 || $post['addForm'] == 2){
			
			
		?>
			<script>
			$('#formdata').hide();
			$('#adminTable').show();
			$('#chapter').selectpicker5();
			$('#exam1').exampicker1();
			</script>
			<div class="container">	
				<div class="row align-items-top">
					<div class="col-md-2">
						<label for="inputExam">Class<span class="text-danger">*</span></label>
						<div class="form-group">
						
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline8" name="customcheckboxInline8" class="class5 custom-control-input" onChange="getFields5();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', explode(",",$_POST['class']))) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', explode(",",$_POST['class']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline8">XI</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline7" name="customcheckboxInline7" class="class5 custom-control-input" onChange="getFields5();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', explode(",",$_POST['class']))) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', explode(",",$_POST['class']))) { echo 'checked'; }} ?> >
								<label class="custom-control-label" for="customcheckboxInline7">XII</label>
							</div>
							<input type="hidden" class="class5" id="class1" value="<?php echo $_POST['class']; ?>" >
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['class'])){ echo $_SESSION['error1']['class'];}?></span>
						</div>
					</div>
					<div class="col-md-2">
						<label for="inputExam">Exam<span class="text-danger">*</span></label>
						<div class="form-group">
						
						
							<!--<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline5" name="customcheckboxInline5" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')" id="exam1" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('1', explode(",",$_POST['exam']))) { echo '1'; }else { echo '1'; } } else { echo '1'; } } else { echo '1'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('1', explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline5">NEET</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline6" name="customcheckboxInline6" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')" id="exam2" value="<?php if(isset($_POST['exam'])) { if(in_array('2', explode(",",$_POST['exam']))) { echo '2'; }else { echo '2'; } } else { echo '2'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('2', explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline6">JEE</label>
							</div>
							<input type="hidden" class="exam1" id="exam1" value="<?php echo $_POST['exam']; ?>" >-->
							<?php
							/*$sel=$database->query("select * from exam where estatus='1'");
							$i=1;
							while($row=mysqli_fetch_array($sel)){
							?>
								<div class="custom-control custom-checkbox custom-control-inline">
									<input type="checkbox" id="customcheckboxInline1<?php echo $i; ?>" name="customcheckboxInline1<?php echo $i; ?>" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')" id="exam1" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array($row['id'], explode(",",$_POST['exam']))) { echo $row['id']; }else { echo $row['id']; } } else { echo $row['id']; } } else { echo $row['id']; } ?>" <?php if(isset($_POST['exam'])) { if(in_array($row['id'], explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
									<label class="custom-control-label" for="customcheckboxInline1<?php echo $i; ?>"><?php echo $row['exam']; ?></label>
								</div>
							<?php
								$i++;
							}*/

							?>
							
							<select  name="exam" id="exam1" class="form-control exampicker1 exam" multiple  onChange="setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">
								<option value=''>--Select--</option>
								<?php
								$sel=$database->query("select * from exam where estatus='1'");
								while($data = mysqli_fetch_array($sel))
								{
									?>
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['exam'])) { if(in_array($data['id'], explode(",",$_POST['exam']))) { echo 'selected="selected"'; } } ?>  ><?php echo $data['exam'];?></option>
								<?php
								}
								?>
							</select>
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['exam'])){ echo $_SESSION['error1']['exam'];}?></span>
						</div>
					</div>
					
					<div class="form-group col-md-2" id="aaa1">
						 <label for="inputSub">Subjects</label>
						 <select class="form-control" name="subject" value=""   id="subject1" onChange="setState('ccc1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getchapter1=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">
							<option value=''>-- Select --</option>
							
							<?php
							$row = $database->query("select * from subject where estatus='1' and id IN(".rtrim($rowuser['subject'],",").")");
							while($data = mysqli_fetch_array($row))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
							<?php
							}
							?>
						</select>
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['subject'])){ echo $_SESSION['error1']['subject'];}?></span> 
					</div>
					<?php
					$_POST['chapter']=explode(",",$_POST['chapter']);
							?>
					<div class="form-group col-md-3" id="ccc1" >
						<label for="inputTopic">Chapter</label>
						<select class="form-control selectpicker5" name="chapter1" value="" data-live-search="true"  multiple  id="chapter1"  >
							<option value=''>-- Select --</option>
							<?php
							
							$row = $database->query("select * from chapter where estatus='1' and  class IN  (".$_POST['class'].") and subject IN (".$_POST['subject'].") and id IN(".rtrim($rowuser['chapter'],",").")");
							while($data = mysqli_fetch_array($row))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
							<?php
							}
							?>
						</select> 
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['chapter'])){ echo $_SESSION['error1']['chapter'];}?></span>
					</div>
					
					<div class="col-md-3 mt-3 " id="dummy1">
													
						<!-- <a href="#" class="btn btn-primary text-white"  onClick="$('#adminForm').slideToggle();$('#dummy').hide();">Add Question</a> -->
						<a href="#" class="btn btn-md btn-theme-2 mt-3" onClick="$('#adminForm').show();$('#adminTable').hide();$('#dummy1').hide();setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&ptype=&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">Add Question</a>
						<a href="#" class="btn btn-md btn-theme-2 mt-3" onClick="$('#adminForm').show();$('#adminTable').hide();$('#dummy1').hide();setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&ptype=pp&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">Add Previous Question</a>

					</div>
					
					
					
				</div>
			</div>
			

		<?php
		}else{
			?>
			
			<script>
			$('#formdata').hide();
			$('#chapter').selectpicker5();
			
			</script>
			
			<?php
		
				$_SESSION['class']=$_POST['class'];
				$_SESSION['exam']=$_POST['exam'];
				$_SESSION['subject']=$_POST['subject'];
				$_SESSION['chapter']=$_POST['chapter'];
				if($_REQUEST['ptype']=='pp'){
					$_POST['ppaper']=1;
					$_POST['ptype']=$_REQUEST['ptype'];
					$disabled="disabled";
				}else{
					$disabled="";
				}
				if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
				$data_sel = $database->query("SELECT * FROM createquestion WHERE id = '".$_POST['editform']."'");
				if(mysqli_num_rows($data_sel) > 0){
				$data = mysqli_fetch_array($data_sel);
				$_POST = array_merge($_POST,$data);
					 ?>
						
					 <script type="text/javascript">
					 //$('#adminForm').slideDown();
					 </script>
					 
					 <?php
					}
				}

			if(isset($_REQUEST['errordisplay'])){
				if($_REQUEST['qtype']=='3'){
					$_POST['question']=$_SESSION['question'];
					$_POST['mat_question']=$_SESSION['mat_question'];
					$_POST['explanation']=$_SESSION['explanation'];
					$_POST['mat_que_paragraph']=$_SESSION['mat_que_paragraph'];
					$_POST['mat_que_footer']=$_SESSION['mat_que_footer'];
					$_POST['data5']=$_SESSION['data5'];
					$_POST['option1']=urldecode($_POST['option1']);
					$_POST['option2']=urldecode($_POST['option2']);
					$_POST['option3']=urldecode($_POST['option3']);
					$_POST['option4']=urldecode($_POST['option4']);
					$_POST['option5']=urldecode($_POST['option5']);
				}else if($_REQUEST['qtype']=='9'){
					$_POST['question']=$_SESSION['question'];
					$_POST['mat_question']=$_SESSION['mat_question'];
					$_POST['explanation']=$_SESSION['explanation'];
					$_POST['mat_que_paragraph']=$_SESSION['mat_que_paragraph'];
					$_POST['mat_que_footer']=$_SESSION['mat_que_footer'];
					$_POST['data5']=$_SESSION['data5'];
					$_POST['option1']=urldecode($_POST['option1']);
					$_POST['option2']=urldecode($_POST['option2']);
					$_POST['option3']=urldecode($_POST['option3']);
					$_POST['option4']=urldecode($_POST['option4']);
					$_POST['option5']=urldecode($_POST['option5']);
				}
				else if($_REQUEST['qtype']=='5'){
					$_POST['compquestion']=$_SESSION['compquestion'];
					$_POST['data4']=$_SESSION['data4'];
				}else if($_REQUEST['qtype']=='8'){
					$_POST['question']=$_SESSION['question'];
					$_POST['explanation']=$_SESSION['explanation'];
				}
				else if($_REQUEST['qtype']=='7'){
					$_POST['question']=$_SESSION['question'];
					$_POST['option1']=$_SESSION['option1'];
					$_POST['option2']=$_SESSION['option2'];
					$_POST['option3']=$_SESSION['option3'];
					$_POST['option4']=$_SESSION['option4'];
					$_POST['explanation']=$_SESSION['explanation'];
				}
			}
	
	 ?>
			<div class="container">
				<div class="row align-items-top">
					<div class="col-md-2">
						<label for="inputExam">Class<span class="text-danger">*</span></label>
						<div class="form-group">
						
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline8" name="customcheckboxInline8" class="class5 custom-control-input" onChange="getFields5();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', explode(",",$_POST['class']))) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', explode(",",$_POST['class']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline8">XI</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline7" name="customcheckboxInline7" class="class5 custom-control-input" onChange="getFields5();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', explode(",",$_POST['class']))) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', explode(",",$_POST['class']))) { echo 'checked'; }} ?> >
								<label class="custom-control-label" for="customcheckboxInline7">XII</label>
							</div>
							<input type="hidden" class="class5" id="class1" value="<?php echo $_POST['class']; ?>" >
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['class'])){ echo $_SESSION['error1']['class'];}?></span>
						</div>
					</div>
					<div class="col-md-2">
						<label for="inputExam">Exam<span class="text-danger">*</span></label>
						<div class="form-group">
						
						
							<!--<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline5" name="customcheckboxInline5" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexamtype1=1&exam='+$('#exam1').val()+'')" id="exam2" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('1', explode(",",$_POST['exam']))) { echo '1'; }else { echo '1'; } } else { echo '1'; } } else { echo '1'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('1', explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline5">NEET</label>
							</div>
							<div class="custom-control custom-checkbox custom-control-inline">
								<input type="checkbox" id="customcheckboxInline6" name="customcheckboxInline6" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexamtype1=1&exam='+$('#exam1').val()+'')" id="exam3" value="<?php if(isset($_POST['exam'])) { if(in_array('2', explode(",",$_POST['exam']))) { echo '2'; }else { echo '2'; } } else { echo '2'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('2', explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
								<label class="custom-control-label" for="customcheckboxInline6">JEE</label>
							</div>
							<input type="hidden" class="exam1" id="exam1" value="<?php echo $_POST['exam']; ?>" >-->
							<?php
							/*$sel=$database->query("select * from exam where estatus='1'");
							$i=1;
							while($row=mysqli_fetch_array($sel)){
							?>
							
								<div class="custom-control custom-checkbox custom-control-inline">
									<input type="checkbox" id="customcheckboxInline5<?php echo $i; ?>" name="customcheckboxInline5<?php echo $i; ?>" class="exam1 custom-control-input" onChange="getFields4();setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexamtype1=1&exam='+$('#exam1').val()+'')" id="exam2<?php echo $i; ?>" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array($row['id'], explode(",",$_POST['exam']))) { echo $row['id']; }else { echo $row['id']; } } else { echo $row['id']; } } else { echo $row['id']; } ?>" <?php if(isset($_POST['exam'])) { if(in_array($row['id'], explode(",",$_POST['exam']))) { echo 'checked'; } } ?> >
									<label class="custom-control-label" for="customcheckboxInline5<?php echo $i; ?>"><?php echo $row['exam']; ?></label>
								</div>
							<?php
								$i++;
							}*/
							?>
							<select  name="exam" id="exam1" class="form-control exampicker1"  multiple onChange="setState('aaa1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexam1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexamtype1=1&exam='+$('#exam1').val()+'')">
								<option value=''>--Select--</option>
								<?php
								$sel=$database->query("select * from exam where estatus='1'");
								while($data = mysqli_fetch_array($sel))
								{
									?>
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['exam'])) { if(in_array($data['id'], explode(",",$_POST['exam']))) { echo 'selected="selected"'; } } ?>  ><?php echo $data['exam'];?></option>
								<?php
								}
								?>
							</select>
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['exam'])){ echo $_SESSION['error1']['exam'];}?></span>
						</div>
					</div>
					
					<div class="form-group col-md-2" id="aaa1">
						 <label for="inputSub">Subjects</label>
						 <select class="form-control" name="subject" value=""   id="subject1" onChange="setState('ccc1','<?php echo SECURE_PATH;?>createquestion/ajax.php','getchapter1=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">
							<option value=''>-- Select --</option>
							
							<?php
							if($_REQUEST['exam']=='1'){
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and id!='4'");
							}else if($_REQUEST['exam']=='2'){
								$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") and id!=1 and id!=5 ");
							}else if($_REQUEST['exam']=='3'){
								$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") ");

							}else if($_REQUEST['exam']=='4'){
								$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") ");

							}else if($_REQUEST['exam']=='1,2' ||  $_REQUEST['exam']=='1,2,3' ||  $_REQUEST['exam']=='1,2,3,4'){
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and  id in (2,3)");
							}else if($_REQUEST['exam']=='1,3' || $_REQUEST['exam']=='1,4' || $_REQUEST['exam']=='1,3,4'){
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and  id!=4 ");
							}else if($_REQUEST['exam']=='2,3' || $_REQUEST['exam']=='2,4'){
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and  id in (2,3,4)");
							}else{
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") ");
							}
							while($data = mysqli_fetch_array($row1))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
							<?php
							}
							?>
						</select>
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['subject'])){ echo $_SESSION['error1']['subject'];}?></span> 
					</div>
					<?php
					$_POST['chapter']=explode(",",$_POST['chapter']);
							?>
					<div class="form-group col-md-3" id="ccc1" >
						<label for="inputTopic">Chapter</label>
						<select class="form-control selectpicker5" name="chapter1" value=""  multiple  id="chapter1"  >
							<option value=''>-- Select --</option>
							<?php
							
							$row = $database->query("select * from chapter where estatus='1' and  class IN  (".$_POST['class'].") and subject IN (".$_POST['subject'].") and id IN(".rtrim($rowuser['chapter'],",").")");
							while($data = mysqli_fetch_array($row))
							{
								?>
									<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
							<?php
							}
							?>
						</select> 
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['chapter'])){ echo $_SESSION['error1']['chapter'];}?></span>
					</div>
					
				</div>
		</div>
			
			
<style>
/*#dates_list .mce-tinymce.mce-container{
display:none !important;
 }*/
 .correctanswer .mce-tinymce.mce-container{
	display:none !important;
 }
</style>

	<script>
	getFields();
	getFields3();
	
	tinymce.remove(".example");
	radiofunction('<?php echo $_POST['ppaper']; ?>');
	</script>

  <div class="col-lg-12 col-md-12">
	  
	<?php// print_r($_SESSION); ?>
	<?php
		if(isset($_POST['exam'])){
			if($_POST['exam']!=''){
				if($_POST['exam']!=''){
					$style="";
				}else{
					$style='style="display:none;"';
				}
				if(rtrim($_POST['exam'],",")=='1'){
					$style9='style="display:none;"';
				}else if(rtrim($_POST['exam'],",")=='2'){
					$style9="";
					$_POST['pexamtype']='1';
				}else if(rtrim($_POST['exam'],",")=='1,2'){
					$style9="";
					$_POST['pexamtype']='1';
				}else{
					$style9='style="display:none;"';
				}
				
			}else{
				$style9='style="display:none;"';
			}
		}else{
			$style9='style="display:none;"';
		}

		
		?>
	<div class="row" <?php echo $style9; ?> >
		<div class="col-lg-4"  >
			<div class="form-group row" id="pexamtype1">
				<label class="col-md-6 col-form-label">Mains/Advance<span style="color:red;">*</span></label>
				<div class="col-md-6">
					<select name="pexamtype" id="pexamtype"  class="form-control"   >
						<option value=''>-- Select --</option>
						<option value='1' <?php if(isset($_POST['pexamtype'])){ if($_POST['pexamtype']=='1'){ echo 'selected';}else{}} ?> >Mains</option>
						<option value='2' <?php if(isset($_POST['pexamtype'])){ if($_POST['pexamtype']=='2'){ echo 'selected';}else{}} ?> >Advance</option>
						
						
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['pexamtype'])){ echo $_SESSION['error']['pexamtype'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4">
			<div class="form-group row">
				<label class="col-sm-6 col-form-label">Previous Paper</label>
				<div class="col-sm-6 mt-2">
					
						<input tabindex="7"  type="radio"  class="ppaper"  id="idyes" onclick="radiofunction('1');$('.ppaper').val('1')"  name="ppaper" value="1" <?php if(isset($_POST['ppaper'])){ if($_POST['ppaper'] == '1'){ echo 'checked="checked"';}}else{ echo '';}?>  /> <label for="idyes">Yes</label>
						<input tabindex="8"  type="radio"  class="ppaper" id="idno" onclick="radiofunction('0');$('.ppaper').val('0')" name="ppaper" value="0" <?php if(isset($_POST['ppaper'])){ if($_POST['ppaper'] == '0'){ echo 'checked="checked"';}}else{ echo 'checked="checked"';}?> <?php echo $disabled; ?> />
						<label for="idno">No</label>
						<input type="hidden"   class="ppaper"  id="ppaper" name="ppaper" value="<?php if(isset($_POST['ppaper'])){ if($_POST['ppaper']){ echo $_POST['ppaper']; }else { echo '0';} }else { echo '0'; } ?>"  /> <br />
						<span class="error text-danger" id="gender_err" style="display:none;">Please Select Previous Paper</span>
				</div>
			</div>
		</div>
		
	</div>
	<div class="row">
		
		<div class="col-lg-4 yeardiv" <?php echo $style; ?> >
			<div class="form-group row">
				<label class="col-md-6 col-form-label">Year<span style="color:red;">*</span></label>
				<div class="col-md-6">
					<select name="year" id="year"  class="form-control yeardivval" onChange="setStateGet('examsetid','<?php echo SECURE_PATH;?>createquestion/ajax.php','getexamset=1&year='+$('#year').val()+'&exam=<?php echo $_POST['exam']; ?>')" >
						<option value="" >-Select-</option>
						<?php
						$sel=$database->query("select * from previous_questions where estatus='1'  and exam in (".rtrim($_POST['exam'],",").") group by year ORDER by id DESC");
						while($row=mysqli_fetch_array($sel)){
							?>
								<option value="<?php echo $row['year'];?>" <?php if(isset($_POST['year'])) { if($_POST['year']==$row['year']) { echo 'selected="selected"'; }  } ?>><?php echo $row['year'];?></option>
						<?php
						}
						?>
						
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['year'])){ echo $_SESSION['error']['year'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-4 yeardiv"  >
			<div class="form-group row" id="examsetid" >
				<label class="col-md-6 col-form-label">Exam Set ID<span style="color:red;">*</span></label>
				<div class="col-md-6">
					<select name="qset" id="qset"  class="form-control"  >
						<option value=''>-- Select --</option>
						<?php
						
						$sel=$database->query("select * from previous_questions where estatus='1'  and exam in (".rtrim($_POST['exam'],",").") and year='".$_POST['year']."'  ORDER by id DESC");
						while($row=mysqli_fetch_array($sel)){
							$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
							while($row1=mysqli_fetch_array($sel1)){
							?>
								<option value="<?php echo $row1['id'];?>" <?php if(isset($_POST['qset'])) { if($_POST['qset']==$row1['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row1['qset'];?></option>
						<?php
							}
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['qset'])){ echo $_SESSION['error']['qset'];}?></span>
				</div>
			</div>
		</div>
		
		
	</div>
 	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Question Type<span style="color:red;">*</span></label>
				<div class="col-sm-10 mt-2">
					<?php
					$sel=$database->query("select * from questiontype where  estatus='1' and id in (7,5,8,3,9) order by orderid ASC");
					while($row=mysqli_fetch_array($sel)){
						?>
							<input   type="radio"  class="qtype"  id="<?php echo $row['questiontype']; ?>" onchange="$('.qtype').val('<?php echo $row['id']; ?>');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>&ptype=<?php echo $_POST['ptype']; ?>&ppaper='+$('#ppaper').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&qset='+$('#qset').val()+'');"  name="qtype" value="<?php echo $row['id']; ?>" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == $row['id']){ echo 'checked="checked"';}}else if($row['id']=='7'){ echo 'checked="checked"'; }?>  /> <label for="<?php echo $row['questiontype']; ?>"><?php echo $row['questiontype']; ?></label>
					<?php
					}
					?>
					<input type="hidden"   class="qtype"  id="qtype" name="qtype" value="<?php  if(isset($_POST['qtype'])){ echo $_POST['qtype']; }else{ echo "7"; } ?>"  />
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qtype'])){ echo $_SESSION['error']['qtype'];}?></span>
					<!-- <input tabindex="7"  type="radio"  class="qtype"  id="simple" onchange="$('.qtype').val('simple');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>');"  name="qtype" value="simple" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == 'simple'){ echo 'checked="checked"';}}else{ echo 'checked="checked"'; }?>  /> <label for="simple">Simple</label>
					<input tabindex="8"  type="radio"  class="qtype" id="comprehension" onchange="$('.qtype').val('comprehension');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>');" name="qtype" value="comprehension" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == 'comprehension'){ echo 'checked="checked"';}}else{ echo '';}?>/>
					<label for="comprehension">Comprehension</label>
					<input tabindex="8"  type="radio"  class="qtype" id="integer" onchange="$('.qtype').val('integer');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>');" name="qtype" value="integer" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == 'integer'){ echo 'checked="checked"';}}else{ echo '';}?>/>
					<label for="integer">Integer</label>
					<input tabindex="8"  type="radio"  class="qtype" id="matching" onchange="$('.qtype').val('matching');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>');" name="qtype" value="matching" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == 'matching'){ echo 'checked="checked"';}}else{ echo '';}?>/>
					<label for="matching">Matching</label>
					<input tabindex="8"  type="radio"  class="qtype" id="matrix" onchange="$('.qtype').val('matrix');setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&getquestiontype=1&qtype='+$('#qtype').val()+'&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&topic=<?php echo $_SESSION['topic']; ?>');" name="qtype" value="matrix" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == 'matrix'){ echo 'checked="checked"';}}else{ echo '';;}?>/>
					<label for="matrix">Matrix</label>
					<input type="hidden"   class="qtype"  id="qtype" name="qtype" value="<?php  if(isset($_POST['qtype'])){ echo $_POST['qtype']; }else{ echo "simple"; } ?>"  /> <br /> -->
					
				</div>
			</div>
		</div>
	</div>
	
	<?php
	if(isset($_POST['qtype'])){
		if($_POST['qtype']=='5'){
		$count = 1;
		
		?>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Q.Explanation<span style="color:red;">*</span></label>
					<div class="col-sm-10">
						<div class="wrs_container">
							<div class="wrs_row">
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="compquestion" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['compquestion'])) { echo urldecode($_POST['compquestion']); }else{} ?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['compquestion'])){ echo $_SESSION['error']['compquestion'];}?></span>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-end">
					
				<div class="col-lg-2">
					<?php
					if($count == 1){
					?>
						<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
					<?php
					}
					else{
					?>
						<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
						<a class="btn btn-outline-info px-3" onclick="removeList('<?php echo $count; ?>')">Remove Question</a>
					<?php
					}
					?>
					
				
			</div>
		  
		</div>
		<hr />
		<div id="dates_list" >
			<?php
			if(isset($_REQUEST['data4']))
			{
				$posdata1 = $_POST['data4'];
				foreach($posdata1 as $rowpost)
				{
					
						?>
							<div class="list" id="list<?php echo $count;?>">
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<!-- <textarea name="question" rows="3" id="question" class="form-control question "  ><?php if(isset($rowpost[0])) { echo $rowpost[0]; }?></textarea>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span> -->
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['question'])) { echo urldecode($rowpost['question']); }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<!-- <textarea name="option1" rows="2" id="option1" value="" class="form-control option1"  ><?php if(isset($rowpost[1])) { echo $rowpost[1]; }?></textarea>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span> -->
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option1'])) { echo urldecode($rowpost['option1']); }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<!-- <textarea name="option2" rows="2" id="option2" value="" class="form-control option2"  ><?php if(isset($rowpost[2])) { echo $rowpost[2]; }?></textarea>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span> -->
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option2'])) { echo urldecode($rowpost['option2']); }?></textarea> 
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<!-- <textarea name="option3" rows="2" id="option3" value="" class="form-control option3"  ><?php if(isset($rowpost[3])) { echo $rowpost[3]; }?></textarea>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span> -->
												<div class="wrs_col wrs_s12">
													 <div id="editorContainer">
														<div id="toolbarLocation"></div>
														<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option3'])) { echo urldecode($rowpost['option3']); }?></textarea> 
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<!-- <textarea name="option4" rows="2" id="option4" value="" class="form-control option4"  ><?php if(isset($rowpost[4])) { echo $rowpost[4]; }?></textarea>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span> -->
												<div class="wrs_col wrs_s12">
													 <div id="editorContainer">
														<div id="toolbarLocation"></div>
														<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option4'])) { echo urldecode($rowpost['option4']); }?></textarea> 
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row correctanswer">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
											
											<div class="col-sm-8">
											<?php
											
											if(isset($rowpost['answer'])){
												if($rowpost['answer']!=''){
													$_POST['answer']=explode(",",$rowpost['answer']);
												}
												
											}else{
												
											}
											?>
											
											<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer1<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
							
											<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer2<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
											<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer3<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;

											<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer4<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; }  } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
	

											<input type="hidden" class="tests<?php echo $count; ?> answer " id="correctanswer<?php echo $count; ?>" value="<?php echo $rowpost['answer']; ?>" >
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'.$count])){ echo $_SESSION['error']['answer'.$count];}?></span>
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Explanation</label>
											<div class="col-sm-10">
												<div class="col-sm-10">
													<!-- <textarea name="explanation" rows="3" id="explanation" value="" class="form-control explanation"  ><?php if(isset($rowpost[6])) { echo $rowpost[6]; }?></textarea>
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span> -->
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['explanation'])) { echo urldecode($rowpost['explanation']); }?></textarea> 
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
														</div>
													</div>
												</div>
												
											</div>
										</div>
									</div>
								</div>
								
								
							</div>
						
						
						<?php
						$count++;
					
					
				}
				?>
					<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
			<?php
			}else{
			?>
			<div class="list" id="list">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="question" rows="3" id="question" value="<?php if(isset($_POST['question'])) { echo $_POST['question']; }?>" class="form-control question "  ></textarea>
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo $_POST['question']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option1" rows="2" id="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>" class="form-control option1"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option1'])) { echo urldecode($_POST['option1']); }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option2" rows="2" id="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>" class="form-control option2"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option3" rows="2" id="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>" class="form-control option3"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option4" rows="2" id="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>" class="form-control option4"  ></textarea>
								<span class="error"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row correctanswer">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
							
							<div class="col-sm-8">
							<?php
							
							if(isset($_POST['answer'])){
								if($_POST['answer']!=''){
									$_POST['answer']=explode(",",$_POST['answer']);
								}
								
							}else{
								
							}
							?>
							
							<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer1" id="ans1" value="<?php if(isset($_POST['answer'])) { if($_POST['answer']!=''){ if(in_array('A', $_POST['answer'])) { echo 'A'; } } else { echo '0'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A &nbsp;&nbsp;
				 										
							<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer2" id="ans2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo ''; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
							<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer3" id="ans3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo ''; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
							<input type="checkbox" class="tests<?php echo $count; ?>"   onclick="getFields3(<?php echo $count; ?>)" name="answer4" id="ans4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo ''; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

							<input type="hidden" class="tests<?php echo $count; ?> answer " id="correctanswer<?php echo $count; ?>" value="<?php echo $_POST['answer']; ?>" >
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Explanation</label>
							<div class="col-sm-10">
								<div class="col-sm-10">
									<!-- <textarea name="explanation" rows="3" id="explanation" value="<?php if(isset($_POST['explanation'])) { echo $_POST['question']; }?>" class="form-control explanation"  ></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span> -->
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo $_POST['explanation']; }?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
			<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
			<?php } ?>
		</div>
		
	  <hr />
		<?php
		}else if($_POST['qtype']=='8'){
		?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
						<div class="col-sm-10">
							
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Correct Answer</label>
						<div class="col-sm-4">
							<input type="text" id="correctanswer" onkeypress="return NumAndTwoDecimals(event);" onkeyup="numberrange();" onchange="" value="<?php if(isset($_POST['answer'])) { echo $_POST['answer']; }?>" class="form-control"  >
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Explanation</label>
					<div class="col-sm-10">
						<div class="wrs_container">
							<div class="wrs_row">
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="explanation" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>

		</div>
		
		<?php
			
		}else if($_POST['qtype']=='3'){
			$count1=1;
		?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Question</label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_question"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['mat_question'])) { echo urldecode($_POST['mat_question']); }else{} ?></textarea>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_question'])){ echo $_SESSION['error']['mat_question'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Q.Paragraph<span style="color:red;">*</span></label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_que_paragraph" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_paragraph'])) { echo urldecode($_POST['mat_que_paragraph']); }else{} ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_paragraph'])){ echo $_SESSION['error']['mat_que_paragraph'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<!-- <label class="col-form-label">Question<span style="color:red;">*</span></label> -->
					</div>
					<div class="col-md-10">
						<div class="list-types pb-3">	
							<div class="row">
								<div class="col-md-5">
									<label for="exampleInputEmail1">List1 Type<span style="color:red;">*</span></label>
									<select class="form-control" name="list1type" id="list1type" >
										<option value='' >--Select--</option>
										<option value='alphabets'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='alphabets') { echo 'selected="selected"'; }  } ?>>Alphabets</option>
										<option value='roman'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
										<option value='numbers' <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
									</select>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list1type'])){ echo $_SESSION['error']['list1type'];}?></span>
								</div>
								<div class="col-md-5">
									<label for="exampleInputEmail1">List2 Type<span style="color:red;">*</span></label>
									<select class="form-control" name="list2type" id="list2type" >
										<option value=''>--Select--</option>
										<option value='alphabets' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='alphabets') { echo 'selected="selected"'; }  } ?> >Alphabets</option>
										<option value='roman' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
										<option value='numbers' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
									</select>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list2type'])){ echo $_SESSION['error']['list2type'];}?></span>
								</div>
							</div>
						</div>
					
		
			
					<div id="dates_list1">
						<?php
						if(isset($_POST['data5']))
						{
							$posdata1 = $_POST['data5'];
							
							foreach($posdata1 as $rowpost)
							{
							?>
								<div class="list1" id="list1<?php echo $count1;?>">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
												<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
												<div class="">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group">
												<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
												<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
												<div class="">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
											</div>
										</div>
										<div class="col-md-2 form-group" style="padding-top: 6rem;">
											<?php
											if($count1 == 1){
											?>
												<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
											<?php
											}
											else{
											?>
												<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
												<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
											<?php
											}
											?>
											
										</div>
									</div>
								</div>
								
								
								<?php
								$count1++;
							
								
							}
							?>
								<input type="hidden" id="session_list1"  value="<?php echo  $count1-1;?>"/>
							<?php
						}else{
							
						?>
							<div class="list1" id="list1">
							
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-form-label" for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
											<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
											<div class="">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-form-label">List2 Option<?php echo $count1; ?> <span style="color:red;">*</span></label>
											<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($_POST['qlist2'])) { echo $_POST['list1']; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
											<div class="">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo $_POST['qlist2']; }?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
										</div>
									</div>
									<div class="col-md-2 form-group" style="padding-top: 6rem;">
										<?php
										if($count1 == 1){
										?>
											<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
										<?php
										}
										else{
										?>
											<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
											<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
										<?php
										}
										?>
										
									</div>
								</div>
							
							
							
							</div>
						<input type="hidden" id="session_list1"  value="<?php echo  $count1;?>"/>
						<?php } ?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Q.Footer Text<span style="color:red;">*</span></label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_que_footer" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_footer'])) { echo urldecode($_POST['mat_que_footer']); }else{} ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_footer'])){ echo $_SESSION['error']['mat_que_footer'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-lg-12 col-md-12">
				<div class="row align-items-center">
					<div class="col-lg-2">
						Options
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">A)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option1" name="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">B)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option2" name="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-5 offset-lg-2">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">C)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option3" name="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">D)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option4" name="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
							</div>
						</div>
					</div>
				</div>
					
			</div>
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-5 offset-lg-2">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">E)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option5" name="option5" value="<?php if(isset($_POST['option5'])) { echo $_POST['option5']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option5'])){ echo $_SESSION['error']['option5'];}?></span>
							</div>
						</div>
					</div>
					
				</div>
					
			</div>
			
			<div class="row correctanswer">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
						
						<div class="col-sm-8">
						<?php
						
						if(isset($_POST['answer'])){
							if($_POST['answer']!=''){
								$_POST['answer']=explode(",",$_POST['answer']);
							}
							
						}else{
							
						}
						?>
						
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
							
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
						<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
						<input type="checkbox" class="tests"   onclick="getFields()"  id="answer5" value="<?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'E'; }else { echo 'E'; } } else { echo 'E'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'checked'; } } ?>/>E &nbsp;&nbsp;

						<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Explanation</label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}else if($_POST['qtype']=='9'){
			$count2=1;
		?>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Question</label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_question"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['mat_question'])) { echo urldecode($_POST['mat_question']); }else{} ?></textarea>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_question'])){ echo $_SESSION['error']['mat_question'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Q.Paragraph<span style="color:red;">*</span></label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_que_paragraph" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_paragraph'])) { echo urldecode($_POST['mat_que_paragraph']); }else{} ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_paragraph'])){ echo $_SESSION['error']['mat_que_paragraph'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-2">
					<!-- <label class="col-form-label">Question<span style="color:red;">*</span></label> -->
					</div>
					<div class="col-md-10">
					<div class="list-types pb-3">
					<div class="row">
						<div class="col-md-4">
							<label for="exampleInputEmail1">List1 Type<span style="color:red;">*</span></label>
							<select class="form-control" name="list1type" id="list1type" >
								<option value='' >--Select--</option>
								<option value='alphabets'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='alphabets') { echo 'selected="selected"'; }  } ?>>Alphabets</option>
								<option value='roman'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
								<option value='numbers' <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
							</select>
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list1type'])){ echo $_SESSION['error']['list1type'];}?></span>
						</div>
						<div class="col-md-4">
							<label for="exampleInputEmail1">List2 Type<span style="color:red;">*</span></label>
							<select class="form-control" name="list2type" id="list2type" >
								<option value=''>--Select--</option>
								<option value='alphabets'  <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='alphabets') { echo 'selected="selected"'; }  } ?>>Alphabets</option>
								<option value='roman' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
								<option value='numbers' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
							</select>
							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list2type'])){ echo $_SESSION['error']['list2type'];}?></span>
						</div>
					</div>
					</div>
				</div>
			
			
				<div id="dates_list2" class="col-md-10 offset-md-2">
					<?php
					if(isset($_POST['data5']))
					{
						$posdata1 = $_POST['data5'];
						foreach($posdata1 as $rowpost)
						{
							?>
								<div class="list2" id="list2<?php echo $count2;?>">
									<div class="row">
										<div class="col-md-5">
											<div class="form-group">
												<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
												<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
												<div class="">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode(stripslashes($rowpost['qlist1'])); }?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
											</div>
										</div>
										<div class="col-md-5">
											<div class="form-group">
												<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
												<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
												<div class="">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode(stripslashes($rowpost['qlist2'])); }?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
											</div>
										</div>
										<div class="col-md-2 form-group" style="padding-top: 20px">
											<?php
											if($count2 == 1){
											?>
												<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
											<?php
											}
											else{
											?>
												<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
												<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
											<?php
											}
											?>
											
										</div>
									</div>
								</div>
								
								
								<?php
								$count2++;
							
								
							}
							?>
							<input type="hidden" id="session_list2"  value="<?php echo  $count2;?>"/>
						<?php
					}else{
					?>
						<div class="list2" id="list2">
						
							<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-form-label" for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
											<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
											<div class="">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label class="col-form-label">List2 Option<?php echo $count2; ?> <span style="color:red;">*</span></label>
											<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($_POST['qlist2'])) { echo $_POST['list1']; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
											<div class="">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo $_POST['qlist2']; }?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
										</div>
									</div>
									<div class="col-md-2 form-group" style="padding-top: 6rem;">
										<?php
										if($count2 == 1){
										?>
											<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
										<?php
										}
										else{
										?>
											<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
											<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
										<?php
										}
										?>
										
									</div>
								</div>
						
						
						
						</div>
					<input type="hidden" id="session_list2"  value="<?php echo  $count2;?>"/>
					<?php } ?>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Q.Footer Text<span style="color:red;">*</span></label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="mat_que_footer" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_footer'])) { echo urldecode($_POST['mat_que_footer']); }else{} ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_footer'])){ echo $_SESSION['error']['mat_que_footer'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12">
				<div class="row align-items-center">
					<div class="col-lg-2">
						Options
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">A)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option1" name="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">B)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option2" name="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-5 offset-lg-2">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">C)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option3" name="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
							</div>
						</div>
					</div>
					<div class="col-lg-5">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">D)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option4" name="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
							</div>
						</div>
					</div>
				</div>
					
			</div>
			<div class="col-lg-12 col-md-12">
				<div class="row">
					<div class="col-lg-5 offset-lg-2">
						<div class="form-group row align-items-center">
							<label class="col-sm-2 col-form-label">E)</label>
							<div class="col-sm-10 mt-2">
								<input type="text"   class="form-control"  id="option5" name="option5" value="<?php if(isset($_POST['option5'])) { echo $_POST['option5']; }?>"  /> <br />
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option5'])){ echo $_SESSION['error']['option5'];}?></span>
							</div>
						</div>
					</div>
					
				</div>
					
			</div>

			<div class="row correctanswer">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
						
						<div class="col-sm-8">
						<?php
						
						if(isset($_POST['answer'])){
							if($_POST['answer']!=''){
								$_POST['answer']=explode(",",$_POST['answer']);
							}
							
						}else{
							
						}
						?>
						
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
							
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
						<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
						<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
						<input type="checkbox" class="tests"   onclick="getFields()"  id="answer5" value="<?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'E'; }else { echo 'E'; } } else { echo 'E'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'checked'; } } ?>/>E &nbsp;&nbsp;
						<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="form-group row">
						<label class="col-sm-2 col-form-label">Explanation</label>
						<div class="col-sm-10">
							<div class="wrs_container">
								<div class="wrs_row">
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}else{
		?>
		<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									 
									<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	 	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									
									<textarea id="option1"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option1'])) { echo urldecode($_POST['option1']); }else{} ?> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	 	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									 <!-- <div id="option2" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
									 
									 										</div> -->
									<textarea id="option2"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option2'])) { echo urldecode($_POST['option2']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	  	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<!--  <div id="option3" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
									 
									 									</div> -->
									<textarea id="option3"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo urldecode($_POST['option3']); }else{} ?> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	   	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									 <!-- <div id="option4" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
									 
									 									</div> -->
									<textarea id="option4"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option4'])) { echo urldecode($_POST['option4']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	   	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Explanation</label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	
	  <div class="row">
		
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Explanation Video link</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="expla_vlink" value="<?php if(isset($_POST['expla_vlink'])) { echo $_POST['expla_vlink']; }else{} ?>"  autocomplete="off" id="expla_vlink" >
					<span class="error"><?php if(isset($_SESSION['error']['expla_vlink'])){ echo $_SESSION['error']['expla_vlink'];}?></span>
				</div>
			</div>
		</div>
	</div>
	 <div class="row correctanswer">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
				
				<div class="col-sm-8">
				<?php
				
				if(isset($_POST['answer'])){
					if($_POST['answer']!=''){
						$_POST['answer']=explode(",",$_POST['answer']);
					}
					
				}else{
					
				}
				?>
				
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
							
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
				<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

				<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
				</div>
			</div>
		</div>
    </div>
	
		<?php
		}
		?>
				
	<?php
	}else{
		?>
		<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									  <!-- <div id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> 
									
									</div> -->
									<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	 	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									 
									<textarea id="option1"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['option1']); }else{} ?> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	 	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									
									<textarea id="option2"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option2'])) { echo urldecode($_POST['option2']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	  	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									
									<textarea id="option3"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo urldecode($_POST['option3']); }else{} ?> </textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	   	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									
									<textarea id="option4"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option4'])) { echo urldecode($_POST['option4']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	   	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Explanation</label>
				<div class="col-sm-10">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
      
	 </div>
	
	  <div class="row">
		
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Explanation Video link</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="expla_vlink" value="<?php if(isset($_POST['expla_vlink'])) { echo $_POST['expla_vlink']; }else{} ?>"  autocomplete="off" id="expla_vlink" >
					<span class="error"><?php if(isset($_SESSION['error']['expla_vlink'])){ echo $_SESSION['error']['expla_vlink'];}?></span>
				</div>
			</div>
		</div>
	</div>
	 <div class="row correctanswer">
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
				
				<div class="col-sm-8">
				<?php
				
				if(isset($_POST['answer'])){
					if($_POST['answer']!=''){
						$_POST['answer']=explode(",",$_POST['answer']);
					}
					
				}else{
					
				}
				?>
				
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if($_POST['answer']!=''){ if(in_array('A', $_POST['answer'])) { echo 'A'; } } else { echo '0'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A &nbsp;&nbsp;
							
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo ''; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
				<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo ''; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
				<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo ''; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

				<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
				</div>
			</div>
		</div>
    </div>
	
	
    <?php 
	} 
	?>

    
	

	<script>
			
			function rand(){
						
				var qtype=$('#qtype').val();
				var data2;
				if(qtype=='5'){

					var newarray = [];
					i=1;
					$('#dates_list .list').each(function(){
						var retval = {};
						console.log(tinymce.get('question'+i).getContent());
						retval.question= encodeURIComponent(tinymce.get('question'+i).getContent());
						retval.option1= encodeURIComponent(tinymce.get('option1'+i).getContent());
						retval.option2= encodeURIComponent(tinymce.get('option2'+i).getContent());
						retval.option3= encodeURIComponent(tinymce.get('option3'+i).getContent());
						retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
						retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
						
						retval.comid= $(this).find('.comid').val();
						retval.answer= $(this).find('.answer').val();
						newarray.push(retval);
						console.log('new',newarray);
						i++;
					});
					console.log('new',newarray);

					 $.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>createquestion/img2.php',
						data:JSON.stringify(newarray),
						contentType: 'application/json; charset=utf-8',
								
						dataType: 'json',
						success: function (data) {
								data2 = '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "","compquestion" : "' + encodeURIComponent(tinymce.get('compquestion').getContent())+ '","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
							 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>createquestion/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',		
								dataType: 'json',
								
								success: function (result,xhr) {
										console.log("reply");
										
											setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','validateForm=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
										
										
								},
									error: function(e){	

									console.log("ERROR: ", e);
								}
							}); 
								
						},
							error: function(e){	

						console.log("ERROR: ", e);
						}
					});

				}else if(qtype=='3'){

					var newarray = [];
					i=1;
					$('#dates_list1 .list1').each(function(){
						var retval = {};
						//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
						retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
						retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

						newarray.push(retval);
						console.log("kkk"+retval);
						i++;

					});

					console.log('new',newarray);

					 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
					data:JSON.stringify(newarray),
					contentType: 'application/json; charset=utf-8',
							
					dataType: 'json',
					success: function (data) {
						data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '","compquestion" : ""}';
						 $.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>createquestion/img.php',
						data: data2,
						contentType: 'application/json; charset=utf-8',		
						dataType: 'json',
						
						success: function (result,xhr) {
								console.log("reply");
								setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','validateForm=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
								
							},
							error: function(e){	

							console.log("ERROR: ", e);
						}
					});
							
					},
						error: function(e){	

					console.log("ERROR: ", e);
					}
				}); 
			}else if(qtype=='9'){
					var newarray = [];
					i=1;
					$('#dates_list2 .list2').each(function(){
						var retval = {};
						//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
						retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
						retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

						newarray.push(retval);
						console.log("kkk"+retval);
						i++;

					});

				console.log('new',newarray);

				 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
					data:JSON.stringify(newarray),
					contentType: 'application/json; charset=utf-8',
							
					dataType: 'json',
					success: function (data) {
						data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '","compquestion" : ""}';

						 $.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>createquestion/img.php',
						data: data2,
						contentType: 'application/json; charset=utf-8',		
						dataType: 'json',
						
						success: function (result,xhr) {
								console.log("reply");
								
									
									setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','validateForm=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
								
							},
							error: function(e){	

							console.log("ERROR: ", e);
						}
					});
							
					},
						error: function(e){	

					console.log("ERROR: ", e);
					}
				}); 
			}else{
				if(qtype=='7'){
					data2 = '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
				}else if(qtype=='8'){
					data2 ='{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
				}else{
					data2=  '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
				}

				$.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>createquestion/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							console.log("reply");
							setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','validateForm=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
							
					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				});
				
			}

		}

			function view(){
						
					var qtype=$('#qtype').val();
					var data2;
					if(qtype=='3'){

						var newarray = [];
						i=1;
						$('#dates_list1 .list1').each(function(){
							var retval = {};
							//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
							retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
							retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

							newarray.push(retval);
							console.log("kkk"+retval);
							i++;

						});

						console.log('new',newarray);

						 $.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
						data:JSON.stringify(newarray),
						contentType: 'application/json; charset=utf-8',
								
						dataType: 'json',
						success: function (data) {
							data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';
							 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>createquestion/img.php',
							data: data2,
							contentType: 'application/json; charset=utf-8',		
							dataType: 'json',
							
							success: function (result,xhr) {
									console.log("reply");
									$('#messageDetails').modal('show');
									setState('viewDetails1','<?php echo SECURE_PATH;?>createquestion/process1.php','viewDetails1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
									
								},
								error: function(e){	

								console.log("ERROR: ", e);
							}
						});
								
						},
							error: function(e){	

						console.log("ERROR: ", e);
						}
					}); 
				}else if(qtype=='9'){
						var newarray = [];
						i=1;
						$('#dates_list2 .list2').each(function(){
							var retval = {};
							//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
							retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
							retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

							newarray.push(retval);
							console.log("kkk"+retval);
							i++;

						});

					console.log('new',newarray);

					 $.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
						data:JSON.stringify(newarray),
						contentType: 'application/json; charset=utf-8',
								
						dataType: 'json',
						success: function (data) {
							data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';

							 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>createquestion/img.php',
							data: data2,
							contentType: 'application/json; charset=utf-8',		
							dataType: 'json',
							
							success: function (result,xhr) {
									console.log("reply");
									
									$('#messageDetails').modal('show');
									setState('viewDetails1','<?php echo SECURE_PATH;?>createquestion/process1.php','viewDetails1=1&class='+$('#class1').val()+'&exam='+$('#exam1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&ptype=<?php echo $_REQUEST['ptype']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
									
								},
								error: function(e){	

								console.log("ERROR: ", e);
							}
						});
								
						},
							error: function(e){	

						console.log("ERROR: ", e);
						}
					}); 
				}

			}
				</script>
	
		<div class="row mx-auto">
			<div class="col-lg-12 text-center">
				
				<a class="radius-20 btn btn-theme px-5" onClick="rand();"> Save</a>
				<?php if($_POST['qtype']=='3' || $_POST['qtype']=='9'){
				?>
				<a class="radius-20 btn btn-info px-5 text-white" onClick="view();"> Preview</a>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php
			unset($_SESSION['error']);

			unset($_SESSION['question']);
			unset($_SESSION['option1']);
			unset($_SESSION['option2']);
			unset($_SESSION['option3']);
			unset($_SESSION['option4']);
			unset($_SESSION['explanation']);
			unset($_SESSION['mat_question']);
			unset($_SESSION['mat_que_paragraph']); 
	 unset($_SESSION['mat_que_footer']);
			unset($_SESSION['data5']);
			unset($_SESSION['data4']);
			
		}
		
	}
?>
<script type="text/javascript">


function getFields()
{
    var ass='';
    $('.tests').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#correctanswer').val(ass);
	});
}
function getFields3(count)
{
    var ass='';
    $('.tests'+count).each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#correctanswer'+count).val(ass);
		console.log(ass);
		//alert(ass);
		
	});
}

</script>


<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		if(isset($post['editform'])){
	  		$id = $post['editform'];

		}
		if(isset($post['qtype'])){
			if($post['qtype']=='5'){
				if(!$_SESSION['compquestion']){

					$_SESSION['error']['compquestion'] = "*Please Enter Explanation";

				}
				$abcd = 1;
				//$list = explode("^",rtrim($_REQUEST['data4'],'^'));
				$list=$_SESSION['data4'];
					if(count($list)>0)
					{
					foreach ($list as $r)
					{
						if(strlen(trim($r['question'])) == 0 ){
						  $_SESSION['error']['question'.$abcd] = "Please Enter Question";
						}
						if(strlen(trim($r['option1'])) == 0 ){
						  $_SESSION['error']['option1'.$abcd] = "Please Enter Answer A";
						}
						if(strlen(trim($r['option2'])) == 0 ){
						  $_SESSION['error']['option2'.$abcd] = "Please Enter Answer B";
						}
						if(strlen(trim($r['option3'])) == 0 ){
						  $_SESSION['error']['option3'.$abcd] = "Please Enter Answer C";
						}
						if(strlen(trim($r['option4'])) == 0 ){
						  $_SESSION['error']['option4'.$abcd] = "Please Enter Answer D";
						}
						if(strlen(trim($r['answer'])) == 0 ){
						  $_SESSION['error']['answer'.$abcd] = "Please Enter Answer";
						}
						/*if(strlen(trim($r['6'])) == 0 ){
						  $_SESSION['error']['explanation'.$abcd] = "Please Enter Explanation";
						}*/

						$option1=$r['option1'];
						$option2=$r['option2'];
						$option3=$r['option3'];
						$option4=$r['option4'];
						if($option1!='' && $option2!='' && $option3!='' && $option4!=''){
							$post_array = [
								'option1' => $option1, 
								'option2' => $option2, 
								'option3' => $option3, 
								'option4' => $option4 
								
							];
							$keys = array_keys($post_array);
							foreach ($keys as $key) {
								$keys = array_keys($post_array);
								foreach ($post_array as $k => $v) {
									if ($key != $k) {
										if ($v == $post_array[$key]) {
											 $_SESSION['error'][$k.$abcd] = "Same options should not enter for one question";
										}
									}
								}
							}
						}
						$abcd++;
					}
					
					if($post['ppaper']=='1'){
						if(!$post['qset'] || strlen(trim($post['qset'])) == 0){

							$_SESSION['error']['qset'] = "*Question Set can not be empty";

						}
						if(!$post['year'] || strlen(trim($post['year'])) == 0){

							$_SESSION['error']['year'] = "*Year can not be empty";

						}

					}
				
					if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
						
					?>
						<script type="text/javascript">
						$('#adminForm').show();

						setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&ptype=<?php echo $post['ptype']; ?>&data4=<?php echo $_SESSION['data4']; ?>&compquestion=<?php echo $_SESSION['compquestion']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>');
						</script>

					<?php
					}
					else{
						if($id=='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}
							$compquestion=str_replace("'","&#39;",$_SESSION['compquestion']);
							$database->query("insert compquestion set compquestion='".$compquestion."',estatus='1',timestamp='".time()."'");
							$lid = mysqli_insert_id($database->connection);
							
							//$list = explode("^",rtrim($post['data4'],'^'));
							if(count($list)>0)
							{

								foreach ($list as $r)
								{
									//$r = explode('_',$k);
										if($_SESSION['username']!='Guest'){
											$question=str_replace("'","&#39;",$r['question']);
											$option1=str_replace("'","&#39;",$r['option1']);
											$option2=str_replace("'","&#39;",$r['option2']);
											$option3=str_replace("'","&#39;",$r['option3']);
											$option4=str_replace("'","&#39;",$r['option4']);
											$explanation=str_replace("'","&#39;",$r['explanation']);
											$result=$database->query('insert createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",qtype="'.$post['qtype'].'",compquestion="'.$lid.'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$r['answer'].'",username="'.$_SESSION['username'].'",conttype="0",complexity="0",timeduration="0",usageset="0",question_theory="0",estatus="1",vstatus="0",vreason="0",timestamp="'.time().'"');
									
											$lid1 = mysqli_insert_id($database->connection);
											$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='createquestion',estatus='1',timestamp='".time()."'");
										}else{
										?>
											<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Login Failed</div>
											
												<script type="text/javascript">
												alert("Login Failed");
												location.replace("<?php echo SECURE_PATH;?>admin/");
											</script>
										<?php
									}
								
								}
							}
							
							unset($_SESSION['question']);
							unset($_SESSION['option1']);
							unset($_SESSION['option2']);
							unset($_SESSION['option3']);
							unset($_SESSION['option4']);
							unset($_SESSION['explanation']);
							unset($_SESSION['compquestion']);
							unset($_SESSION['data4']);
				
				?>
					<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Create Question saved successfully!</div>
					
						<script type="text/javascript">
						alert("Data Created Successfully");
						$('#adminForm').show();
						$('#adminTable').hide();
						setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['ptype']; ?>');
						
					</script>
				<?php
						}
					}
					
			}
			}else if($post['qtype']=='8'){
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter Explanation";

				}*/
				if(!$_SESSION['question'] || strlen(trim($_SESSION['question'])) == 0){
				
					$_SESSION['error']['question'] = "*Please Enter Question";
				
				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter Answer";
				
				}
				if($post['ppaper']=='1'){
						if(!$post['qset'] || strlen(trim($post['qset'])) == 0){

							$_SESSION['error']['qset'] = "*Question Set can not be empty";

						}
						if(!$post['year'] || strlen(trim($post['year'])) == 0){

							$_SESSION['error']['year'] = "*Year can not be empty";

						}

					}
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					<script type="text/javascript">
					$('#adminForm').show();

					setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $_SESSION['class'];?>&exam=<?php echo $_SESSION['exam'];?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ppaper=<?php echo $post['ppaper']; ?>&year=<?php echo $post['year']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&ptype=<?php echo $post['ptype']; ?>&answer=<?php echo $post['answer']; ?>&explanation=<?php echo $_SESSION['explanation']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>');
					</script>

				<?php
				}else{
						if($id=='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}
							if($_SESSION['username']!='Guest'){
								$result=$database->query('insert createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",qtype="'.$post['qtype'].'",question="'.$_SESSION['question'].'",explanation="'.$_SESSION['explanation'].'",expla_vlink="",answer="'.$post['answer'].'",username="'.$_SESSION['username'].'",conttype="0",timeduration="0",complexity="0",usageset="0",question_theory="0",estatus="1",vstatus="0",vreason="0",timestamp="'.time().'"');

								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								unset($_SESSION['compquestion']);

								if($result){
									$lid1 = mysqli_insert_id($database->connection);
									$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='createquestion',estatus='1',timestamp='".time()."'");
									?>
									<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Create Question saved successfully!</div>
									
										<script type="text/javascript">
										alert("Data Created Successfully");
										$('#adminForm').show();
									$('#adminTable').hide();
										setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&pype=<?php echo $post['pype']; ?>');
										
									</script>
								<?php
								}else{
									?>
									<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i>  Question Insertion Failed!</div>
									
										<script type="text/javascript">
										alert("Insertion Failed!");
										$('#adminForm').show();
									$('#adminTable').hide();
										setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['tpype']; ?>');
										
									</script>
								<?php
								}
							}else{
								?>
									<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Login Failed</div>
									
										<script type="text/javascript">
										alert("Login Failed");
										location.replace("<?php echo SECURE_PATH;?>admin/");
									</script>
								<?php
							}
						}
					}
				
			
			}else if($post['qtype']=='3'){
				/*	if(!$_SESSION['question']){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}*/
				if(!$post['option1'] || strlen(trim($post['option1'])) == 0){
				
					$_SESSION['error']['option1'] = "*Please Enter Option1";
				
				}
				if(!$post['option2'] || strlen(trim($post['option2'])) == 0){
				
					$_SESSION['error']['option2'] = "*Please Enter Option2";
				
				}
				if(!$post['option3'] || strlen(trim($post['option3'])) == 0){
				
					$_SESSION['error']['option3'] = "*Please Enter Option3";
				
				}
				if(!$post['option4'] || strlen(trim($post['option4'])) == 0){
				
					$_SESSION['error']['option4'] = "*Please Enter Option4";
				
				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter Answer";
				
				}
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter Explanation";

				}*/
				if(!$post['list1type'] || strlen(trim($post['list1type'])) == 0){
				
					$_SESSION['error']['list1type'] = "*Please Select List1 Type";
				
				}
				if(!$post['list2type'] || strlen(trim($post['list2type'])) == 0){
				
					$_SESSION['error']['list2type'] = "*Please Select List2 Type";
				
				}
				if($post['ppaper']=='1'){
						if(!$post['qset'] || strlen(trim($post['qset'])) == 0){

							$_SESSION['error']['qset'] = "*Question Set can not be empty";

						}
						if(!$post['year'] || strlen(trim($post['year'])) == 0){

							$_SESSION['error']['year'] = "*Year can not be empty";

						}

					}
				$abcd = 1;
				$a = 1;
				$ab=array();
				$ab1=array();
				$ab2=array();
				
				$j = 0;
				$m = 0;
				$l= 0;
				//$list = explode("^",rtrim($_REQUEST['data5'],'^'));
				$list=$_SESSION['data5'];
				if(count($list)>0)
				{	
					foreach($list as $qqlist)
					{
					
						
						if(strlen(trim($qqlist['qlist1'])) == 0 ){
						  $_SESSION['error']['qlist1'.$abcd] = "Please Enter List1 Option".$abcd;
						}
						if(strlen(trim($qqlist['qlist2'])) == 0 ){
						  $_SESSION['error']['qlist2'.$abcd] = "Please Enter List2".$abcd;;
						}
						if (!in_array($qqlist['qlist1'], $ab))
						{
						         $ab[$j] = $qqlist['qlist1'];

								 $j++;
						} else {

                         $_SESSION['error']['qlist1'.$abcd] = "Same options should not enter for List1";        
						}
						if (!in_array($qqlist['qlist2'], $ab1))
						{
						         $ab1[$m] = $qqlist['qlist2'];

								 $m++;
						} else {

                         $_SESSION['error']['qlist2'.$abcd] = "Same options should not enter for List2";        
						}
						$abcd++;
					}
					
					
					foreach ($list as $qqlist1)
					{
						if (!in_array($qqlist1['qlist2'], $ab))
						{
						         $ab2[$l] = $qqlist1['qlist2']."_";
							

								 $l++;
						} else {

							$_SESSION['error']['qlist2'.$a] = "List1 Value should not enter for List2";   
							
						}
						$a++;
					}
					$option1=$post['option1'];
					$option2=$post['option2'];
					$option3=$post['option3'];
					$option4=$post['option4'];
					$option5=$post['option5'];
					if($option1!='' && $option2!='' && $option3!='' && $option4!='' && $option5!=''){
						$post_array = [
							'option1' => $option1, 
							'option2' => $option2, 
							'option3' => $option3, 
							'option4' => $option4,
							'option5' => $option5 
							
						];
						$keys = array_keys($post_array);
						foreach ($keys as $key) {
							$keys = array_keys($post_array);
							foreach ($post_array as $k => $v) {
								if ($key != $k) {
									if ($v == $post_array[$key]) {
										$_SESSION['error'][$k] = "Same options should not enter for one question";
									}
								}
							}
						}
					}
					
					
					if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
						
						
					?>
						<script type="text/javascript">
						$('#adminForm').show();

						setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $_SESSION['class'];?>&exam=<?php echo $_SESSION['exam'];?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&ptype=<?php echo $post['ptype']; ?>&year=<?php echo $post['year']; ?>&question=<?php echo $post['question']; ?>&option1=<?php echo urlencode($post['option1']); ?>&option2=<?php echo urlencode($post['option2']); ?>&option3=<?php echo urlencode($post['option3']); ?>&option4=<?php echo urlencode($post['option4']); ?>&option5=<?php echo urlencode($post['option5']); ?>&answer=<?php echo $post['answer']; ?>&list1type=<?php echo $post['list1type']; ?>&list2type=<?php echo $post['list2type']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>');
						</script>
						<?php
					}else
					{
						if($id=='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}
							$qdata=array();
								
								$jk = 0;
								if(count($list)>0)
								{
									foreach ($list as $k)
									{
										//$qdata[]=$k;
										$qdata[$jk]['qlist1']=str_replace("'","&#39;",$k['qlist1']);
										$qdata[$jk]['qlist2']=str_replace("'","&#39;",$k['qlist2']);
										$jk++;
									}
								}
							$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
							$mat_question=str_replace("'","&#39;",$_SESSION['mat_question']);
							$mat_que_paragraph=str_replace("'","&#39;",$_SESSION['mat_que_paragraph']);
							$mat_que_footer=str_replace("'","&#39;",$_SESSION['mat_que_footer']);
							
							if($_SESSION['username']!='Guest'){
								
								
								$result=$database->query("insert createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',username='".$_SESSION['username']."',conttype='0',timeduration='0',complexity='0',usageset='0',question_theory='0',estatus='1',vstatus='0',vreason='0',timestamp='".time()."'");
								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								unset($_SESSION['compquestion']);
								unset($_SESSION['mat_question']);
								unset($_SESSION['data5']);
								if($result){
								$lid1 = mysqli_insert_id($database->connection);
									$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='createquestion',estatus='1',timestamp='".time()."'");
								?>
									<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Create Question saved successfully!</div>
									
										<script type="text/javascript">
										alert("Data Created Successfully");
										$('#adminForm').show();
										$('#adminTable').hide();
										setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&pype=<?php echo $post['pype']; ?>');
										
									</script>
								<?php
								}else{
									?>
									<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i>  Insertion Failed</div>
									
										<script type="text/javascript">
										alert("Insertion Failed");
										$('#adminForm').show();
										$('#adminTable').hide();
										setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['ptype']; ?>');
										
									</script>
								<?php
								}
							}else{
								?>
									<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Login Failed</div>
									
										<script type="text/javascript">
										alert("Login Failed");
										location.replace("<?php echo SECURE_PATH;?>admin/");
									</script>
								<?php
							}
						}
					}
				}
				
			}else if($post['qtype']=='9'){
				/*if(!$_SESSION['question']){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}*/
				if(!$post['option1'] || strlen(trim($post['option1'])) == 0){
				
					$_SESSION['error']['option1'] = "*Please Enter Option1";
				
				}
				if(!$post['option2'] || strlen(trim($post['option2'])) == 0){
				
					$_SESSION['error']['option2'] = "*Please Enter Option2";
				
				}
				if(!$post['option3'] || strlen(trim($post['option3'])) == 0){
				
					$_SESSION['error']['option3'] = "*Please Enter Option3";
				
				}
				if(!$post['option4'] || strlen(trim($post['option4'])) == 0){
				
					$_SESSION['error']['option4'] = "*Please Enter Option4";
				
				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter Answer";
				
				}
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter Explanation";

				}*/
				if(!$post['list1type'] || strlen(trim($post['list1type'])) == 0){
				
					$_SESSION['error']['list1type'] = "*Please Select List1 Type";
				
				}
				if(!$post['list2type'] || strlen(trim($post['list2type'])) == 0){
				
					$_SESSION['error']['list2type'] = "*Please Select List2 Type";
				
				}
				if($post['ppaper']=='1'){
						if(!$post['qset'] || strlen(trim($post['qset'])) == 0){

							$_SESSION['error']['qset'] = "*Question Set can not be empty";

						}
						if(!$post['year'] || strlen(trim($post['year'])) == 0){

							$_SESSION['error']['year'] = "*Year can not be empty";

						}

					}
				$abcd = 1;
				$a = 1;
				$ab=array();
				$ab1=array();
				$ab2=array();
				
				$j = 0;
				$m = 0;
				$l= 0;
				//$list = explode("^",rtrim($_REQUEST['data6'],'^'));
				$list=$_SESSION['data5'];
				if(count($list)>0)
				{	
					foreach($list as $qqlist)
					{
					
						
						if(strlen(trim($qqlist['qlist1'])) == 0 ){
						  $_SESSION['error']['qlist1'.$abcd] = "Please Enter List1 Option".$abcd;
						}
						if(strlen(trim($qqlist['qlist2'])) == 0 ){
						  $_SESSION['error']['qlist2'.$abcd] = "Please Enter List2".$abcd;;
						}
						if (!in_array($qqlist['qlist1'], $ab))
						{
						         $ab[$j] = $qqlist['qlist1'];

								 $j++;
						} else {

                         $_SESSION['error']['qlist1'.$abcd] = "Same options should not enter for List1";        
						}
						if (!in_array($qqlist['qlist2'], $ab1))
						{
						         $ab1[$m] = $qqlist['qlist2'];

								 $m++;
						} else {

                         $_SESSION['error']['qlist2'.$abcd] = "Same options should not enter for List2";        
						}
						$abcd++;
					}
					
					
					foreach ($list as $qqlist1)
					{
						if (!in_array($qqlist1['qlist2'], $ab))
						{
						         $ab2[$l] = $qqlist1['qlist2']."_";
							

								 $l++;
						} else {

							$_SESSION['error']['qlist2'.$a] = "List1 Value should not enter for List2";   
							
						}
						$a++;
					}
					$option1=$post['option1'];
					$option2=$post['option2'];
					$option3=$post['option3'];
					$option4=$post['option4'];
					$option5=$post['option5'];
					if($option1!='' && $option2!='' && $option3!='' && $option4!='' && $option5!=''){
						$post_array = [
							'option1' => $option1, 
							'option2' => $option2, 
							'option3' => $option3, 
							'option4' => $option4,
							'option5' => $option5 
							
						];
						$keys = array_keys($post_array);
						foreach ($keys as $key) {
							$keys = array_keys($post_array);
							foreach ($post_array as $k => $v) {
								if ($key != $k) {
									if ($v == $post_array[$key]) {
										$_SESSION['error'][$k] = "Same options should not enter for one question";
									}
								}
							}
						}
					}
					
					
						if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
						?>
							<script type="text/javascript">
							$('#adminForm').show();

							setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&ptype=<?php echo $post['ptype']; ?>&year=<?php echo $post['year']; ?>&question=<?php echo $post['question']; ?>&option1=<?php echo urlencode($post['option1']); ?>&option2=<?php echo urlencode($post['option2']); ?>&option3=<?php echo urlencode($post['option3']); ?>&option4=<?php echo urlencode($post['option4']); ?>&option5=<?php echo urlencode($post['option5']); ?>&answer=<?php echo $post['answer']; ?>&list1type=<?php echo $post['list1type']; ?>&list2type=<?php echo $post['list2type']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>');
							</script>
							<?php
						}else{
							if($id=='NULL')
							{
								$class=$post['class'];
								$exam=$post['exam'];
								$subject=$post['subject'];
								$chapter=$post['chapter'];
								$topic=$post['topic'];
								if($post['ppaper']!=''){
									$ppaper=$post['ppaper'];
								}else{
									$ppaper=0;
								}
								if($post['qset']!=''){
									$qset=$post['qset'];
								}else{
									$qset=0;
								}
								if($post['pexamtype']!=''){
									$pexamtype=$post['pexamtype'];
								}else{
									$pexamtype=0;
								}
								if($post['year']!=''){
									$year=$post['year'];
								}else{
									$year=0;
								}

								$qdata=array();
								
								$jk = 0;
								if(count($list)>0)
								{
									foreach ($list as $k)
									{
										//$qdata[]=$k;
										$qdata[$jk]['qlist1']=str_replace("'","&#39;",$k['qlist1']);
										$qdata[$jk]['qlist2']=str_replace("'","&#39;",$k['qlist2']);
										$jk++;
									}
								}
								$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
								$mat_question=str_replace("'","&#39;",$_SESSION['mat_question']);
								$mat_que_paragraph=str_replace("'","&#39;",$_SESSION['mat_que_paragraph']);
								$mat_que_footer=str_replace("'","&#39;",$_SESSION['mat_que_footer']);
								
								if($_SESSION['username']!='Guest'){
									
									$result=$database->query("insert createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',username='".$_SESSION['username']."',conttype='0',timeduration='0',complexity='0',usageset='0',question_theory='0',estatus='1',vstatus='0',vreason='0',timestamp='".time()."'");
									
									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									unset($_SESSION['mat_question']);
									unset($_SESSION['data5']);

									if($result){
										$lid1 = mysqli_insert_id($database->connection);
										$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='createquestion',estatus='1',timestamp='".time()."'");
										?>
											<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Create Question saved successfully!</div>
											
												<script type="text/javascript">
												alert("Data Created Successfully");
												$('#adminForm').show();
												$('#adminTable').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&pype=<?php echo $post['pype']; ?>');
												
											</script>
										<?php
									}else{
										?>
										<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i>Insertion Failed</div>
										
											<script type="text/javascript">
											alert("Insertion Failed");
											$('#adminForm').show();
											$('#adminTable').hide();
											setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['ptype']; ?>');
											
										</script>
									<?php
									}
								}else{
									?>
									<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Login Failed</div>
									
										<script type="text/javascript">
										alert("Login Failed");
										location.replace("<?php echo SECURE_PATH;?>admin/");
									</script>
								<?php
								}
						}
					}
				}
				
			}else if($post['qtype']=='7'){
				if(!$_SESSION['question']){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}

				if(!$_SESSION['option1']){

					$_SESSION['error']['option1'] = "*Please Enter Option1";

				}
				if(!$_SESSION['option2']){

					$_SESSION['error']['option2'] = "*Please Enter Option2";

				}
				if(!$_SESSION['option3']){

					$_SESSION['error']['option3'] = "*Please Enter Option3";

				}
				if(!$_SESSION['option4']){

					$_SESSION['error']['option4'] = "*Please Enter Option4";

				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter answer";
				
				}
				if($post['ppaper']=='1'){
						if(!$post['qset'] || strlen(trim($post['qset'])) == 0){

							$_SESSION['error']['qset'] = "*Question Set can not be empty";

						}
						if(!$post['year'] || strlen(trim($post['year'])) == 0){

							$_SESSION['error']['year'] = "*Year can not be empty";

						}

					}
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter explanation";

				}*/
				$question=str_replace("'","&#39;",$_SESSION['question']);
				$option1=$_SESSION['option1'];
				$option2=$_SESSION['option2'];
				$option3=$_SESSION['option3'];
				$option4=$_SESSION['option4'];
				if($option1!='' && $option2!='' && $option3!='' && $option4!=''){
					$post_array = [
						'option1' => $option1, 
						'option2' => $option2, 
						'option3' => $option3, 
						'option4' => $option4 
						
					];
					$keys = array_keys($post_array);
					foreach ($keys as $key) {
						$keys = array_keys($post_array);
						foreach ($post_array as $k => $v) {
							if ($key != $k) {
								if ($v == $post_array[$key]) {
									$_SESSION['error'][$k] = "Same options should not enter for one question";
								}
							}
						}
					}
				}
				
			
				
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				
					?>
					<script type="text/javascript">
					$('#adminForm').show();
					
					setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $_SESSION['class'];?>&exam=<?php echo $_SESSION['exam'];?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&ptype=<?php echo $post['ptype']; ?>&year=<?php echo $post['year']; ?>&answer=<?php echo $post['answer']; ?>&expla_vlink=<?php echo $post['expla_vlink']; ?>')
					</script>

				<?php
				}else{
				
				
				$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
				$class=$post['class'];
				$exam=$post['exam'];
				$subject=$post['subject'];
				$chapter=$post['chapter'];
				$topic=$post['topic'];
				if($post['ppaper']!=''){
					$ppaper=$post['ppaper'];
				}else{
					$ppaper=0;
				}
				if($post['qset']!=''){
					$qset=$post['qset'];
				}else{
					$qset=0;
				}
				if($post['pexamtype']!=''){
					$pexamtype=$post['pexamtype'];
				}else{
					$pexamtype=0;
				}
				if($post['year']!=''){
					$year=$post['year'];
				}else{
					$year=0;
				}
				if($id=='NULL')
				{
					if($_SESSION['username']!='Guest'){
						$result=$database->query('insert createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",qtype="'.$post['qtype'].'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="'.$post['expla_vlink'].'",answer="'.$post['answer'].'",username="'.$_SESSION['username'].'",conttype="0",timeduration="0",complexity="0",usageset="0",question_theory="0",estatus="1",vstatus="0",vreason="0",timestamp="'.time().'"');
						$lid1 = mysqli_insert_id($database->connection);
						$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='createquestion',estatus='1',timestamp='".time()."'");
						unset($_SESSION['question']);
						unset($_SESSION['option1']);
						unset($_SESSION['option2']);
						unset($_SESSION['option3']);
						unset($_SESSION['option4']);
						unset($_SESSION['explanation']);
						if($result){
							?>
								<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Create Question saved successfully!</div>
								
									<script type="text/javascript">
									alert("Data Created Successfully");
									$('#adminForm').show();
									$('#adminTable').hide();
									//$_GET['page'];}else{ echo '1';}?>');
									setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['ptype']; ?>');
									
								</script>
							<?php
						}else{
							?>
								<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Insertion Failed!</div>
								
									<script type="text/javascript">
									alert(" Insertion Failed!");
									$('#adminForm').show();
									$('#adminTable').hide();
									//$_GET['page'];}else{ echo '1';}?>');
									setState('adminForm','<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1&class=<?php echo $_SESSION['class']; ?>&exam=<?php echo $_SESSION['exam']; ?>&subject=<?php echo $_SESSION['subject']; ?>&chapter=<?php echo $_SESSION['chapter']; ?>&ptype=<?php echo $post['ptype']; ?>');
									
								</script>
							<?php
						}
					}else{
						?>
						<div class="alert alert-danger"><i class="fa fa-thumbs-up fa-2x"></i> Login Failed</div>
						
							<script type="text/javascript">
							alert("Login Failed");
							location.replace("<?php echo SECURE_PATH;?>admin/");
						</script>
					<?php
					}
					
				}
				
			}
		}

	}
	
	
?>

 
	<?php
	

}
?>

<?php
//Delete createticket
if(isset($_GET['rowDelete'])){
	$database->query("DELETE FROM createquestion WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success">Create Question deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>createquestion/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>
<script>
    
	function daterangefunction(start1,end1) {
		//alert(1);
		////alert(start1);
		//alert(end1);
			setStateGet('adminTable','<?php echo SECURE_PATH;?>createquestion/process.php','tableDisplay=1&daterange=1&&fdate='+start1+'&tdate='+end1);
		}
	</script>
<?php
//Display bulkreport
if(isset($_GET['tableDisplay'])){
	if(isset($_REQUEST['report'])){
		$range=$_REQUEST['reportrange'];
		$range1=explode("-",$range);
		$date1=strtotime($range1[0]);
		$date2=strtotime($range1[1]);
		$dateto = strtotime(date('d-m-Y'));
		$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
		$dateyess7= strtotime(date('d-m-Y',strtotime("-7 days")));
		$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
		$dateyess90= strtotime(date('d-m-Y',strtotime("-90 days")));
		$dateyess180= strtotime(date('d-m-Y',strtotime("-180 days")));
		$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
		$dateyess364= strtotime(date('d-m-Y',strtotime("-364 days")));
		if($_REQUEST['type']=='Overall'){
			$con= " and timestamp between ".$dateyess364." and ".$dateto1." ";
		}else  if($_REQUEST['type']=='Today'){
			$con=" and timestamp between ".$dateto." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Last 7 Days'){
			$con=" and timestamp between ".$dateyess7." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Last 15 Days'){
			$con=" and timestamp between ".$dateyess15." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Last 30 Days'){
			$con=" and timestamp between ".$dateyess30." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Last 90 Days'){
			$con=" and timestamp between ".$dateyess90." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Last 180 Days'){
			$con=" and timestamp between ".$dateyess180." and ".$dateto1." ";
		}else if($_REQUEST['type']=='Custom Range'){
			$con=" and timestamp between ".$date1." and ".$date2." ";

		}
		
	}else{
		$con="";
	}
	
	 if(isset($_REQUEST['report'])){ 
		if($_REQUEST['type']!='Custom Range'){
			$datarange4=$_REQUEST['type'];
		}else{
			$datarange4=$_REQUEST['reportrange'];
		}
	} 
	
	?>
	<div class="container">
		
			<div class="ml-auto">
				<div id="reportrange" class="ml-auto bg-gray-color border-0"
					style="cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; width: 15%">
					<i class="far fa-calendar-alt"></i>&nbsp;
								<span><?php if(isset($_REQUEST['report'])){ echo $datarange4; }else { echo 'Days Wise';} ?></span><i class="fa fa-caret-down pl-1"></i>
				</div>
			
			</div> 
		<div class="card border-0 shadow-sm">
			<div class="card-body">
				<?php
				$data_seluser =  $database->query("select * from users where username='".$session->username."'");
				$rowuser = mysqli_fetch_array($data_seluser);
				
                //$class = 1;
				
				if(isset($_REQUEST['class'])) {
					if($_REQUEST['class']!=''){
						if(rtrim($_REQUEST['class'],",")=='1,2'){
							$class=1;
						}else{
							$class=rtrim($_REQUEST['class'],",");
						}
					}
				}else{
					
					if($rowuser['class']!=''){
						if(rtrim($rowuser['class'],",")=='1,2'){
							$class=1;
						}else{
							$class=rtrim($rowuser['class'],",");
						}
						
					}
				}


				 ?>
					
					<ul class="nav nav-pills nav-fill" id="pills-tab" role="tablist">
							<li class="nav-item mr-2">
								<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
									role="tab" aria-controls="pills-home" aria-selected="true">
									<div class="d-flex justify-content-between">
										<h6>NEET</h6>
										<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'','','estatus','1','username',$_SESSION['username'],'','',$con); ?></h6>
									</div>
									<div class="d-flex justify-content-between">
										<table class="table table-bordered mr-2 bg-white">
											<thead>
												<tr>
													<th colspan="4">
														<div class="d-flex justify-content-between">
															<h6>Class <?php if($class == 1) echo 'XI'; else echo 'XII';?></h6>
															<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'','','estatus','1','username',$_SESSION['username'],'','',$con); ?></h6>
														</div>
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<p>Botany</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'subject','1','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
													<td>
														<p>Zoology</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'subject','5','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
													<td>
														<p>Physics</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'subject','2','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
													<td>
														<p>Chemistry</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','1','class',$class,'subject','3','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>


												</tr>

											</tbody>
										</table>
										
									</div>
								</a>
							</li>


							<li class="nav-item">
								<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
									role="tab" aria-controls="pills-profile" aria-selected="false">
									<div class="d-flex justify-content-between">
										<h6>JEE</h6>
										<h6><?php echo $database->SrchReportCount1('createquestion','exam','2','class',$class,'','','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
									</div>
									<div class="d-flex justify-content-between">
										<table class="table table-bordered mr-2 bg-white">
											<thead>
												<tr>
													<th colspan="3">
														<div class="d-flex justify-content-between">
															<h6>Class <?php if($class == 1) echo 'XI'; else echo 'XII';?></h6>
															<h6><?php echo $database->SrchReportCount1('createquestion','exam','2','class',$class,'','','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
														</div>
													</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>
														<p>Mathematics</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','2','class',$class,'subject','4','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
													<td>
														<p>Physics</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','2','class',$class,'subject','2','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
													<td>
														<p>Chemistry</p>
														<h6><?php echo $database->SrchReportCount1('createquestion','exam','2','class',$class,'subject','3','estatus','1','username',$_SESSION['username'],'','',$con); ?> </h6>
													</td>
												</tr>
											</tbody>
										</table>
										
									</div>
								</a>
							</li>
						</ul>

<!--           NEET         -->
						<div class="tab-content date-content" id="pills-tabContent">
							<div class="tab-pane date-contentactive fade show active" id="pills-home"
								role="tabpanel" aria-labelledby="pills-home-tab">

                                <div class="d-flex">

                                    <?php
                                         $subjects = array('1' =>'Botony','5' => 'Zoology','2'=> 'Physics','3' => 'Chemistry');

                                    foreach($subjects as $key => $sub){

                                        ?>

                                        <table class="table table-bordered mr-1 bg-white" >
                                            <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">
                                            <tr>
                                                <th class="bg-gray-color" colspan="2">
                                                    <div class="d-flex justify-content-between">
                                                        <h5><?php  echo $sub;?></h5>
                                                        <h5><?php  echo $database->SrchReportCount1('createquestion','exam','1','subject',$key,'class',$class,'estatus','1','username',$_SESSION['username'],'','',$con); ?></h5>
                                                    </div>
                                                </th>
                                            </tr>

                                            </thead>


                                        <tbody style="display: none;" id="subject-<?php echo $key;?>">
                                        <tr>
                                            <td colspan="2">
                                                <div class="d-flex justify-content-between">
                                                    <h6>Chapters & Topics</h6>
                                                    <h6>Questions</h6>
                                                </div>
                                            </td>

                                        </tr>
                                            <?php
                                            $i=1;
                                            $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='".$class."' and subject='".$key."'  and id in (".$rowuser['chapter'].") ");

                                             while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                ?>
                                                <tr>
                                                    <th class="bg-gray-color" colspan="2" onclick="$('#chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                        <div class="d-flex justify-content-between">
                                                            <h6><?php echo $rowchapter['chapter']; ?></h6>
                                                            
															 <p><?php echo $database->SrchReportCount2('createquestion','exam','1','class',$class,'chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','username',$_SESSION['username'],'','',$con); ?> </p>
                                                        </div>
                                                    </th>
                                                </tr>

                                                 <tr>
                                                 <td colspan="2" style="padding:0;">

                                                     <table  width="100%" style="display: none;" id="chapter-<?php echo $rowchapter['id'];?>">
                                                <?php
                                                $j=1;
                                                $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='".$class."' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                    ?>

                                                    <tr>
                                                        <td width="70%">
                                                            <p><?php echo $rowchapter1['topic']; ?></p>
                                                        </td>
                                                        <td width="30%">
                                                            
															<p><?php echo $database->SrchReportCount2('createquestion','exam','1','class',$class,'chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'username',$_SESSION['username'],'estatus','1','','',$con); ?></p>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $j++;
                                                }
                                                ?>
                                                     </table>
                                        </td>
                                                 </tr>

                                                 <?php
                                                $i++;
                                            }


                                            ?>

                                            </tbody>
                                        </table>
                                        <?php

                                    }

                                    ?>






								</div>
							</div>



<!--                  JEE          -->

							<div class="tab-pane fade date-contentactive" id="pills-profile" role="tabpanel"
								aria-labelledby="pills-profile-tab">
								<div class="d-flex">

                                    <?php
                                    $subjects = array('4' =>'Mathematics','2'=> 'Physics','3' => 'Chemistry');

                                    foreach($subjects as $key => $sub){

                                        ?>

                                        <table class="table table-bordered mr-1 bg-white">
                                            <thead onclick="$('#subject-<?php echo $key;?>').toggle(200);">
                                            <tr>
                                                <th class="bg-gray-color" colspan="2"  onclick="$('#jee-subject-<?php echo $key;?>').toggle('200');">
                                                    <div class="d-flex justify-content-between">
                                                        <h5><?php  echo $sub;?></h5>
                                                        <h5><?php  echo $database->SrchReportCount1('createquestion','exam','2','subject',$key,'class',$class,'estatus','1','username',$_SESSION['username'],'','',$con); ?></h5>
                                                    </div>
                                                </th>
                                            </tr>
                                            </thead>

                                            <tbody style="display: none;" id="jee-subject-<?php echo $key;?>">
                                            <tr>
                                                <td colspan="2">
                                                    <div class="d-flex justify-content-between">
                                                        <h6>Chapters & Topics</h6>
                                                        <h6>Questions</h6>
                                                    </div>
                                                </td>

                                            </tr>
                                            <?php
                                            $i=1;
                                            $sqlchapters=$database->query("select * from  chapter where  estatus='1' and class='".$class."' and subject='".$key."'  and id in (".$rowuser['chapter'].") ");

                                            while($rowchapter=mysqli_fetch_array($sqlchapters)){

                                                ?>
                                                <tr>
                                                    <th class="bg-gray-color" colspan="2" onclick="$('#jee-chapter-<?php echo $rowchapter['id'];?>').toggle('200');">
                                                        <div class="d-flex justify-content-between">
                                                            <h6><?php echo $rowchapter['chapter']; ?></h6>
                                                            <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class',$class,'chapter',$rowchapter['id'],'','','subject',$key,'estatus','1','username',$_SESSION['username'],'','',$con); ?> </p>
                                                        </div>
                                                    </th>
                                                </tr>

                                            <tr>
                                                <td colspan="2" style="padding:0;">

                                                    <table  width="100%" style="display: none;" id="jee-chapter-<?php echo $rowchapter['id'];?>">

                                                    <?php
                                                $j=1;
                                                $sqlchapters1=$database->query("select * from  topic where  estatus='1' and class='".$class."' and subject='".$key."' and chapter='".$rowchapter['id']."'");
                                                while($rowchapter1=mysqli_fetch_array($sqlchapters1)){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <p><?php echo $rowchapter1['topic']; ?></p>
                                                        </td>
                                                        <td>
                                                            <p><?php echo $database->SrchReportCount2('createquestion','exam','2','class',$class,'chapter',$rowchapter['id'],'topic',$rowchapter1['id'],'subject',$key,'username',$_SESSION['username'],'estatus','1','','',$con); ?></p>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    $j++;
                                                }
                                                    ?>
                                                    </table>
                                                </td>
                                            </tr>

                                                <?php
                                                $i++;
                                            }


                                            ?>

                                            </tbody>
                                        </table>
                                        <?php

                                    }

                                    ?>
									</div>

									
							</div>
						</div>


				
			</div>
		</div>
	</div>
	
  <?php
	

}
?>
	


   <br  />
<?php
if(isset($_REQUEST['getchapter']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Chapter<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="chapter" id="chapter"   class="form-control" onChange="setState('ddd','<?php echo SECURE_PATH;?>createquestion/process.php','gettopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')" >
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `chapter` WHERE class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>"><?php echo $zData['chapter']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
    <?php
}
if(isset($_REQUEST['gettopic']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Topic<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="topic" id="topic"   class="form-control" >
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `topic` WHERE class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."' and chapter = '".$_REQUEST['chapter']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>"><?php echo $zData['topic']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
	
    <?php
}


if(isset($_REQUEST['getchapter1']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Chapter<span style="color:red;">*</span></label>
		<div class="col-sm-8">
		<select class="form-control" name="chapter1" value=""   id="chapter1" onChange="search_report();setState('ddd1','<?php echo SECURE_PATH;?>createquestion/process.php','gettopic1=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">
			<option value=''>-- Select --</option>
			<?php
			$row = $database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
			while($data = mysqli_fetch_array($row))
			{
				?>
			<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter1'])) { if($_POST['chapter1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
			<?php
			}
			?>
		</select>
			</div>
		
    <?php
}
if(isset($_REQUEST['gettopic1']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Topic<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="topic1" id="topic1"   class="form-control" onChange="search_report();">
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `topic` WHERE class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."' and chapter = '".$_REQUEST['chapter']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>" <?php if(isset($_POST['topic1'])) { if($_POST['topic1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $zData['topic']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
	
    <?php
}
if(isset($_REQUEST['add_listing']))
{
	$count = $_REQUEST['add_listing'];
?>
	<div class="row justify-content-end">
					
				<div class="col-lg-3 mb-4">
					<?php
					if($count == 1){
					?>
						<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
					<?php
					}
					else{
					?>
						<div class="btn-group">
							<a class="btn btn-outline-info px-4" onclick="addList()">Add Question</a>
							<a class="btn btn-outline-info px-4" onclick="removeList('<?php echo $count; ?>')"><i class="fas fa-minus"></i></a>
						</div>
					<?php
					}
					?>
					<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
				
			</div>
		  
		</div>
		
		
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="question" rows="3" id="question" value="<?php if(isset($_POST['question'])) { echo $_POST['question']; }?>" class="form-control question "  ></textarea>
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo $_POST['question']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option1" rows="2" id="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>" class="form-control option1"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option2" rows="2" id="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>" class="form-control option2"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option3" rows="2" id="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>" class="form-control option3"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option4" rows="2" id="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>" class="form-control option4"  ></textarea>
								<span class="error"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row correctanswer">
		
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Answer</label>
							<div class="col-sm-8">
								<?php
							
							if(isset($_POST['answer'])){
								if($_POST['answer']!=''){
									$_POST['answer']=explode(",",$_POST['answer']);
								}
								
							}else{
								
							}
							?>
							
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer1<?php echo $count; ?>" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if($_POST['answer']!=''){ if(in_array('A', $_POST['answer'])) { echo 'A'; } } else { echo '0'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A &nbsp;&nbsp;
											
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer2<?php echo $count; ?>" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo ''; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer3<?php echo $count; ?>" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo ''; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
								<input type="checkbox" class="tests<?php echo $count; ?>"   onclick="getFields3(<?php echo $count; ?>)"  name="answer4<?php echo $count; ?>"  id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo ''; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D

								<input type="hidden" name="answer<?php echo $count; ?>" class="tests<?php echo $count; ?> answer" id="correctanswer<?php echo $count; ?>" value="<?php echo $_POST['answer']; ?>" >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Explanation</label>
							<div class="col-sm-10">
								<div class="col-sm-10">
									<!-- <textarea name="explanation" rows="3" id="explanation" value="<?php if(isset($_POST['explanation'])) { echo $_POST['question']; }?>" class="form-control explanation"  ></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span> -->
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo $_POST['explanation']; }?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				
		
<!--Addrow end-->
<?php
}

if(isset($_REQUEST['add_listing1']))
{
	$count1 = $_REQUEST['add_listing1'];
?>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
				
				<div class="wrs_container">
					<div class="wrs_row">
						<div class="wrs_col wrs_s12">
							 <div id="editorContainer">
								<div id="toolbarLocation"></div>
								<textarea  id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
							</div>
						</div>

					</div>
				</div>
				
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($_POST['qlist2'])) { echo $_POST['list1']; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
				
				<div class="wrs_container">
					<div class="wrs_row">
						<div class="wrs_col wrs_s12">
							 <div id="editorContainer">
								<div id="toolbarLocation"></div>
								<textarea  id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo $_POST['qlist2']; }?></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
							</div>
						</div>

					</div>
				</div>
				
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
			</div>
		</div>
		<div class="col-md-2 form-group" style="padding-top:6rem">
			<?php
			if($count1 == 1){
			?>
				<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
			<?php
			}
			else{
			?>
				<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> 
				<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
			<?php
			}
			?>
			
		</div>
	</div>
				
	
<!--Addrow end-->
<?php
}
if(isset($_REQUEST['add_listing2']))
{
	$count2 = $_REQUEST['add_listing2'];
?>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
				
				<div class="wrs_container">
					<div class="wrs_row">
						<div class="wrs_col wrs_s12">
							 <div id="editorContainer">
								<div id="toolbarLocation"></div>
								<textarea  id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
							</div>
						</div>

					</div>
				</div>
				
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'])){ echo $_SESSION['error']['qlist1'];}?></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($_POST['qlist2'])) { echo $_POST['list1']; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
				
				<div class="wrs_container">
					<div class="wrs_row">
						<div class="wrs_col wrs_s12">
							 <div id="editorContainer">
								<div id="toolbarLocation"></div>
								<textarea  id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo $_POST['qlist2']; }?></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
							</div>
						</div>

					</div>
				</div>
				
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
			</div>
		</div>
		<div class="col-md-2 form-group" style="padding-top:6rem">
			<?php
			if($count2 == 1){
			?>
				<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
			<?php
			}
			else{
			?>
				<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
				<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
			<?php
			}
			?>
			
		</div>
	</div>
				
	
<!--Addrow end-->
<?php
}
?>
<script>
//$('adminTable').hide();

function radiofunction(it){
	if(it=='1'){
		$(".yeardiv").show();
	}else{
		$(".yeardiv").hide();
	}
}



</script>
<script type="text/javascript">

  $('input[name="daterange"]').daterangepicker({
      autoUpdateInput: false,
      locale: {
          cancelLabel: 'Clear'
      }
  });

  $('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
      $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
	  var start1=picker.startDate.format('MM/DD/YYYY');
	  var end1=picker.endDate.format('MM/DD/YYYY');
	  daterangefunction(start1,end1);
  });

  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
      $(this).val('');
  });


</script>
<script type="text/javascript">
$(function() {
    <?php
    if(isset($_REQUEST['type'])){

    if($_REQUEST['type'] == 'Overall'){
    ?>
    var start = moment().subtract('364','days');
    <?php
    }
    if($_REQUEST['type'] == 'Today'){
    ?>
    var start = moment();
    <?php
    }
    if($_REQUEST['type'] == 'Last 7 Days'){
    ?>
    var start = moment().subtract('6','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 15 Days'){
    ?>
    var start = moment().subtract('14','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 30 Days'){
    ?>
    var start = moment().subtract('29','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 90 Days'){
    ?>
    var start = moment().subtract('89','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 180 Days'){
    ?>
    var start = moment().subtract('179','days');
    <?php
    }
    }
    else{
    ?>
    var start = moment().subtract('364','days');



    <?php
    }
    ?>
    var end = moment();
	
    function cb(start, end,para) {
		console.log("startdate",start, "enddate",end,"para",para);
		if(para!=undefined){
			var a = $('#reportrange span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			//alert(a);
			setStateGet('adminTable','<?php echo SECURE_PATH;?>createquestion/process.php','tableDisplay=1&report=1&type='+para+'&reportrange='+$('#reportrange span').text()+'');
		}
		
       $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
    }
	
    $('#reportrange').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Overall': [moment().subtract(364,'days'),moment()],
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 90 Days': [moment().subtract(89, 'days'), moment()],
            'Last 180 Days': [moment().subtract(179, 'days'), moment()]
        }
    }, cb);

	//cb(start, end);
	
});
 
</script>
<script>


function numberrange(){

var number=$('#correctanswer').val();
  
  if(number<-9999999999.00 || number>9999999999.99 ){
	alert('Answer between -9999999999.00 to +9999999999.99 only');
     $('#correctanswer').val('');
  }
  else{
    $('#correctanswer').val(number);
  }

}
</script>
<script type="text/javascript" >
		 $(".selectpicker5").selectpicker('refresh');
		 $(".exampicker1").selectpicker('refresh');
	</script>
	<script type="text/javascript">
	function getFields5()
			{
				var ass='';
				$('.class5').each(function(element) {
					if($(this).is(':checked')) {
								$('#'+$(this).attr('id')).val();
						ass+=$(this).val()+",";
					}
					$('#class1').val(ass);
				});
					
					

				
			}
</script>
	<script type="text/javascript">


			function getFields4()
			{
				var ass='';
				$('.exam1').each(function(element) {
					if($(this).is(':checked')) {
								$('#'+$(this).attr('id')).val();
						ass+=$(this).val()+",";
					}
					$('#exam1').val(ass);
				});
					
					

				
			}
	</script>
<script type="text/javascript" >
		createEditorInstance("en", {});
	</script>