<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

?>
<script>

    $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
	function search_report() {
		
			var class1 = $('#class').val();
			var date = $('#date').val();
			var typewise = $('#typewise').val();
			var subject = $('#subject').val();
			var chapter = $('#chapter').val();
			setStateGet('adminTable','<?php echo SECURE_PATH;?>custom_overview/process.php','tableDisplay=1&class='+class1+'&date='+date+'&subject='+subject+'&chapter='+chapter+'&typewise='+typewise);
	}

	function datafunction(){
		var typewise = $('#typewise').val();
		if(typewise=='2'){
			$("#subjectdiv").show();
			$("#chapterdiv").show();
		}else{
			$("#subjectdiv").hide();
			$("#chapterdiv").hide();
		}
	}
	</script>
<?php
//Delete createticket

//Display bulkreport
if(isset($_REQUEST['tableDisplay'])){
?>
<!--<script src="../edut/js/wirisdisplay.js"></script>-->
<?php
	$sql=$database->query("select * from users where username='".$_SESSION['username']."'");
	$rowsel=mysqli_fetch_array($sql);
	$subject=explode(",",$rowsel['subject']);
	if(isset($_REQUEST['class'])){
		if($_REQUEST['class']!=''){
			$_REQUEST['class']=$_REQUEST['class'];
			$class=" AND  find_in_set(".$_REQUEST['class'].",class)>0";
		}else{
			$_REQUEST['class']='';
			$class="";
		}
	}else{
		$class="";
	}
	

	$dateto=strtotime(date('d-m-Y'));
	$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
	//$dateyess1 = strtotime(date('d-m-Y',strtotime("-1 days"). ' 00:00:01'));
	//$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days"). ' 23:59:59'));
	$dateyess1 = strtotime('-1 days',$dateto);
	 $datetoyes1 = strtotime(date('d-m-Y',$dateyess1).'23:59:59');
	 $dateyess2 = strtotime(date('d-m-Y',strtotime("-2 days")));
	$dateyess3 = strtotime(date('d-m-Y',strtotime("-3 days")));
	$dateyess5 = strtotime(date('d-m-Y',strtotime("-5 days")));
	$dateyess7 = strtotime(date('d-m-Y',strtotime("-7 days")));
	$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
	$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
	$dateyess364= strtotime(date('d-m-Y',strtotime("-364 days")));
	
if(isset($_REQUEST['type'])){
	
	if($_REQUEST['type']=="Today"){
		$vdate=" and rtimestamp between ".$dateto." and ".$dateto1." ";
	} else if($_REQUEST['type']=="Yesterday"){
		$vdate=" and rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
	}else if($_REQUEST['type']=="Last 2 Days"){
		$vdate=" and rtimestamp between ".$dateyess2." and ".$dateto1." ";
	}
	else if($_REQUEST['type']=="Last 3 Days"){
		$vdate=" and rtimestamp between ".$dateyess3." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 5 Days"){
		$vdate=" and rtimestamp between ".$dateyess5." and ".$dateto1." ";
	}
	else if($_REQUEST['type']=="Last 7 Days"){
		$vdate=" and rtimestamp between ".$dateyess7." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 15 Days"){
		$vdate=" and rtimestamp between ".$dateyess15." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 30 Days"){
		$vdate=" and rtimestamp between ".$dateyess30." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Overall"){
		if($_REQUEST['reportrange']!=''){
			$vdate=" and rtimestamp between ".$dateyess364." and ".$dateto1." ";
		}
	}else if($_REQUEST['type']=="Custom Range"){
		if($_REQUEST['reportrange']!=''){
			$dat=explode("-",$_REQUEST['reportrange']);
			$fdat=strtotime($dat[0]);
			$tdat=strtotime($dat[1]);
			$vdate=" and rtimestamp between ".$fdat." and ".$tdat." ";
		}
		
	}else{
		
			$fdate=date('m/d/Y', $dateyess1);
			$tdate=date('m/d/Y', $dateyess1);
			$_REQUEST['reportrange']=$fdate." - ".$tdate;
			$vdate=" and rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
			$vdate1=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
		
	}
}else{
	$vdate='';
}

	if(isset($_REQUEST['subject'])){
		if($_REQUEST['subject']!=""){
			$_REQUEST['subject']=$_REQUEST['subject'];
			$subject=" AND subject='".$_REQUEST['subject']."'";
		} else {
			$_REQUEST['subject']="";
			$subject="";
		}
	}else{
		$_REQUEST['subject']="";
		$subject="";
	}
	if(isset($_REQUEST['chapter'])){
		if($_REQUEST['chapter']!=""){
			$_REQUEST['chapter']=$_REQUEST['chapter'];
			$chapter="  AND chapter IN (".$_REQUEST['chapter'].")";
			$chap1=" AND id  IN (".$_REQUEST['chapter'].")";
		} else {
			$_REQUEST['chapter']="";
			$chapter="";
			$chap1="";
		}
	}else{
		$_REQUEST['chapter']="";
		$chapter="";
		$chap1="";
	}
	$con=$class.$vdate.$subject.$chapter;
	
	if(isset($_REQUEST['typewise'])){ 
		if($_REQUEST['typewise']!=''){
			$_REQUEST['typewise']=$_REQUEST['typewise'];
			if($_REQUEST['typewise']=='2'){
				$style1="";
			}else{
				$style1=" style='display:none;'";
			}
		}else{
			$_REQUEST['typewise']=1;
			$style1=" style='display:none;'";
		}
	}else{
		$_REQUEST['typewise']=1;
		$style1=" style='display:none;'";
	}
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Custom Content Overview</h6>
						</div>
						<div class="card-body">
							<div class="row">
							<?php 
									if(isset($_REQUEST['type'])){ 
										if($_REQUEST['type']!='Custom Range'){
											$datarange4=$_REQUEST['type'];
										}else{
											$datarange4=$_REQUEST['reportrange'];
										}
									} 
									if(isset($_REQUEST['type1'])){ 
										if($_REQUEST['type1']!='Custom Range'){
											$datarange5=$_REQUEST['type1'];
										}else{
											$datarange5=$_REQUEST['reportrange1'];
										}
									} 
									?>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label"> Date</label>
										<div class="form-control daterange p-2 col-sm-6" id="reportrange">
												<i class="far fa-calendar-alt"></i>&nbsp;
												<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type'])){ if($_REQUEST['type']!=''){echo $datarange4; } else{ echo 'Overall'; } }else { echo 'Overall';} ?><i class="fa fa-caret-down pl-1 pl-3"></i>
											</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Class</label>
										<div class="col-sm-8">
											<select class="form-control" name="class" value=""  id="class" onchange="search_report();setState('aaa','<?php echo SECURE_PATH;?>custom_overview/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'');">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from class where estatus='1'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class'])) { if($_REQUEST['class']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Type</label>
										<div class="col-sm-8">
											<select class="form-control" name="typewise" value=""  id="typewise" onchange="search_report();datafunction();">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['typewise'])) { if($_REQUEST['typewise']=='1') { echo 'selected="selected"'; }  } ?>>Subject Wise</option>
												<option value='2' <?php if(isset($_REQUEST['typewise'])) { if($_REQUEST['typewise']=='2') { echo 'selected="selected"'; }  } ?>>Chapter Wise</option>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['typewise'])){ echo $_SESSION['error']['typewise'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4" id="subjectdiv" <?php echo $style1; ?>>
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Subject</label>
										<div class="p-2 col-sm-8">
											
											<select class="form-control" name="subject" value=""   id="subject" onChange="search_report();setState('aaa','<?php echo SECURE_PATH;?>custom_overview/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'');">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from subject where estatus='1' ");
												
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
										
										<div class="col-lg-4" id="chapterdiv" <?php echo $style1; ?>>
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Chapter</label>
												<div class="p-2 col-sm-8" id="aaa">
													
													
													<select class="form-control selectpicker1" name="chapter" value=""  multiple data-actions-box="true" data-live-search="true"  id="chapter"  onChange="search_report()" >
														<?php
														$row = $database->query("select * from chapter where estatus='1' ".$class.$subject."  ");
														
														
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], explode(",",$_REQUEST['chapter']))) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
									
								
							</div>
								
							<?php
							if($_REQUEST['typewise']=='1'){ ?>
								<div class="d-flex justify-content-between dataoverviewtable pt-3">
									<table class="table table-bordered" id="dataTable3">
										<thead class="bg-light verification-header">
											
											<tr>
												<th colspan='2'></th>
												<?php
												
												$data3 = $database->query("select * from subject where estatus=1");
											   
												while($row3=mysqli_fetch_array($data3)){
													?>
													
														<th colspan="2">
															<p><?php echo  $row3['subject']; ?></p>
														</th>
														
												
												<?php
												}
													
												?>
											</tr>
											<tr>
												<th colspan="2"></th>
												 <?php
												
												$data3 = $database->query("select * from subject where estatus=1");
											   
												while($row3=mysqli_fetch_array($data3)){
													?>
														<th>Total</th>
														<th>Verified</th>
													<?php
												}
													
												?>
											</tr>
										</thead>
										<tbody>
								
											<?php
											$i=1;
											
											$sello=$database->query("select * from customcontent_types where estatus='1' ");
											while($rowllo=mysqli_fetch_array($sello)){
												
												?>
													<tr>
														<td colspan="2"><?php echo $rowllo['customcontent']; ?></td>
														<?php
														$j=1;
														
														$data31 = $database->query("select * from subject where estatus=1");
														while($row31=mysqli_fetch_array($data31)){
															$selloc=$database->query("select id from customcontent where estatus='1' and conttype='".$rowllo['id']."' and subject='".$row31['id']."' ".$con." ");
															$rowoc=mysqli_num_rows($selloc);
															$sello1=$database->query("select id from customcontent where estatus='1' and conttype='".$rowllo['id']."' and subject='".$row31['id']."' and rstatus='1' ".$con."");
															$rowo1=mysqli_num_rows($sello1);
															//$total=$rowo['count'];
															//$verified=$rowo1['count'];
															?>
																<td><?php echo $rowoc; ?></td>
																<td><?php echo $rowo1; ?></td>
															<?php
															$j++;
														 }  ?>
													</tr>
												<?php
												$i++;
											}
											?>
								
								
										</tbody>
									</table>
								</div>
							<?php }else{
							?>

								<!--<div class="d-flex justify-content-between  pt-3">
									<table class="table table-bordered table-responsive" id="dataTable">
										<thead class="bg-light verification-header">
											
											<tr>
												<th colspan='2'></th>
												<?php
												
												$data3 = $database->query("select * from customcontent_types where estatus=1");
											   
												while($row3=mysqli_fetch_array($data3)){
													?>
													
														<th colspan="2">
															<p><?php echo  $row3['customcontent']; ?></p>
														</th>
														
												
												<?php
												}
													
												?>
											</tr>
											<tr>
												<th colspan="2"></th>
												 <?php
												
												$data3 = $database->query("select * from customcontent_types where estatus=1");
											   
												while($row3=mysqli_fetch_array($data3)){
													?>
														<th>Total</th>
														<th>Verified</th>
													<?php
												}
													
												?>
											</tr>
										</thead>
										<tbody>
								
											<?php
											$i=1;
											
											$sello=$database->query("select * from chapter where estatus='1' ");
											while($rowllo=mysqli_fetch_array($sello)){
												
												?>
													<tr>
														<td colspan="2"><?php echo $rowllo['chapter']; ?></td>
														<?php
														$j=1;
														
														$data31 = $database->query("select * from customcontent_types where estatus=1");
														while($row31=mysqli_fetch_array($data31)){
															$selloc=$database->query("select id from customcontent where estatus='1' and conttype='".$row31['id']."' and chapter='".$rowllo['id']."' ".$con." ");
															$rowoc=mysqli_num_rows($selloc);
															$sello1=$database->query("select id from customcontent where estatus='1' and conttype='".$row31['id']."' and chapter='".$rowllo['id']."' and rstatus='1' ".$con."");
															$rowo1=mysqli_num_rows($sello1);
															//$total=$rowo['count'];
															//$verified=$rowo1['count'];
															?>
																<td><?php echo $rowoc; ?></td>
																<td><?php echo $rowo1; ?></td>
															<?php
															$j++;
														 }  ?>
													</tr>
												<?php
												$i++;
											}
											?>
								
								
										</tbody>
									</table>
								</div>-->
								<div class="pt-3">
								<table id="example" class="table table-bordered table-responsive" style="width:100%">
        <thead>
            <tr>
                <th></th>
					<?php
					
					$data3 = $database->query("select * from customcontent_types where estatus=1");
				   
					while($row3=mysqli_fetch_array($data3)){
						?>
						
							<th colspan="2">
								<?php echo  $row3['customcontent']; ?>
							</th>
							
					
					<?php
					}
						
					?>
            </tr>
			<tr>
												<th></th>
												 <?php
												
												$data3 = $database->query("select * from customcontent_types where estatus=1");
											   
												while($row3=mysqli_fetch_array($data3)){
													?>
														<th>Total</th>
														<th>Verified</th>
													<?php
												}
													
												?>
											</tr>
										</thead>
										<tbody>
											<?php
											$ij=1;
											$sello1=$database->query("select * from chapter where estatus='1' ".$class.$subject.$chap1." ");
											while($rowllo1=mysqli_fetch_array($sello1)){
											?>
												<tr>
												
													<td><?php echo $rowllo1['chapter']; ?></td>
													<?php
														
														
														$data31 = $database->query("select * from customcontent_types where estatus=1");
														while($row31=mysqli_fetch_array($data31)){
															$selloc=$database->query("select count(id) as cnt from customcontent where estatus='1' and conttype='".$row31['id']."' and find_in_set(".$rowllo1['id'].",chapter)>0 ".$con." ");
															$rowoc=mysqli_fetch_array($selloc);
															
															$sello2=$database->query("select count(id) as cnt from customcontent where estatus='1' and conttype='".$row31['id']."' and find_in_set(".$rowllo1['id'].",chapter)>0 and rstatus='1' ".$con."");
															$rowo1=mysqli_fetch_array($sello2);
															
															?>
																<td><?php if($rowoc['cnt']> 0){ echo $rowoc['cnt']; }else{ echo '0'; } ?></td>
																<td><?php if($rowo1['cnt']> 0){ echo $rowo1['cnt']; }else{ echo '0'; } ?></td>
															<?php
															
														 } 
														 ?>
													
												</tr>
											<?php 
													$ij++; 
												} 
											?>
										</tbody>
									</table>
									</div>
			
								 <script type="text/javascript">
 $('#example').DataTable({
    "pageLength": 50,
    "order": [[ 1, 'asc' ]]
  });
</script>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
	</section>
	

	<?php
	}
?>	
<?php
if(isset($_REQUEST['getchapter'])){
		$sql=$database->query("select * from users where username='".$session->username."'");
		$rowl=mysqli_fetch_array($sql);

		if(isset($_REQUEST['class'])){
		if($_REQUEST['class']!=''){
			$_REQUEST['class']=$_REQUEST['class'];
			$class=" AND  find_in_set(".$_REQUEST['class'].",class)>0";
		}else{
			$_REQUEST['class']='';
			$class="";
		}
	}else{
		$class="";
	}
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=''){
				$_REQUEST['subject']= $_REQUEST['subject'];
				$subject=" AND subject='".$_REQUEST['subject']."'";
			}else{
				$_REQUEST['subject']='';
				$subject="";
			}
		}else{
			$_REQUEST['subject']='';
			$subject="";
		}
		?>
			<select class="form-control selectpicker2 chapter" name="chapter" value=""  multiple data-actions-box="true" data-live-search="true"  id="chapter"  onChange="search_report()" >
				<option value=''>-- Select --</option>
				<?php
				$row = $database->query("select * from chapter where estatus='1'".$class.$subject."  ");
				
				
				while($data = mysqli_fetch_array($row))
				{
					?>
				<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
				<?php
				}
				?>
			</select>
		<?php

	}
	?>
		<script type="text/javascript">
	$(function() {
		<?php
		if(isset($_REQUEST['type'])){

		if($_REQUEST['type'] == 'Overall'){
		?>
		var start = moment().subtract('364','days');
		<?php
		}
		if($_REQUEST['type'] == 'Today'){
		?>
		var start = moment();
		<?php
		}
		if($_REQUEST['type'] == 'Yesterday'){
		?>
		var start = moment().subtract('1','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 2 Days'){
		?>
		var start = moment().subtract('2','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 3 Days'){
		?>
		var start = moment().subtract('2','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 5 Days'){
		?>
		var start = moment().subtract('4','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 7 Days'){
		?>
		var start = moment().subtract('6','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 15 Days'){
		?>
		var start = moment().subtract('14','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 30 Days'){
		?>
		var start = moment().subtract('29','days');
		<?php
		}
	   
		if($_REQUEST['type'] == 'Custom Range'){
			$date3=strtotime(date('m/d/Y'));
			$cudate=explode("-",$_REQUEST['reportrange']);
			$date4=strtotime($cudate[0]);
			$datef=$date3-$date4;
			$date6=round($datef / (60 * 60 * 24));
			
		?>
			var start = moment().subtract(<?php echo $date6; ?>,'days');
		
		<?php
		}
		}
		else{
		?>
		var start = moment().subtract('364','days');


		<?php
		}
		?>
		var end = moment();
		
		function cb(start, end,para) {
			console.log("startdate",start, "enddate",end,"para",para);
			if(para!=undefined){
				var a = $('#reportrange span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
				//alert(a);
				setStateGet('adminTable','<?php echo SECURE_PATH;?>custom_overview/process.php','tableDisplay=1&report=1&type='+para+'&reportrange='+$('#reportrange span').text()+'&class=<?php echo $_REQUEST['class']; ?>');
			}
			if(para=='Custom Range'){
				 $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			}else{
				 $('#reportrange span').html(para);
			}
		}
		
		$('#reportrange').daterangepicker({
			
			startDate: start,
			endDate: end,
			ranges: {

				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment()],
				'Last 3 Days': [moment().subtract(3, 'days'), moment()],
				'Last 5 Days': [moment().subtract(4, 'days'), moment()],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 15 Days': [moment().subtract(14, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'Overall': [moment().subtract(364,'days'),moment()]
			}
		}, cb);

		//cb(start, end);
		
	});
	 
	</script>
<script>
	$(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	</script>