<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
$users=$database->query("select * from users where valid='1' and username='".$_SESSION['username']."'");
	$rowusers1=mysqli_fetch_array($users);
	$rowee1=$database->query("select * from subject where estatus='1' and id in (".$rowusers1['subject'].") order by id asc limit 0,1");
	$rowee11=mysqli_fetch_array($rowee1);
		if(isset($_REQUEST['subject'])){ 
			if($_REQUEST['subject']!=''){
				$_REQUEST['subject']=$_REQUEST['subject'];
			}else{
				$_REQUEST['subject']=$rowee11['id'];
				
			}
		}else{
			$_REQUEST['subject']=$rowee11['id'];
		}
		
?>
 <link href="<?php echo SECURE_PATH;?>vendor/app/css/subjects.css" rel="stylesheet">
 <style>
 .right-content {
    text-align: right;
}
.right-content h6 {
    /* font-family: 'Quicksand', sans-serif !important; */
	font-family: 'Varela Round', sans-serif !important;
}
</style>
 <div class="breadcrumbs-area2" id="display">
		<div class="row">
		<div class="col-md-12">
		<div class="d-flex justify-content-between align-items-center border-bottom pb-3 mt-3 mb-4">
				<div class="title">
					<h1 class="text-uppercase" id="printpdf">Chapters <small>Lets get a quick Overview</small>
					</h1>
				</div>
				<div class="form-group mb-1 w-25">
				<select   id="subject" class="form-control"   onchange="setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1&subject='+$('#subject').val()+'')" >
				<?php $exam=$database->query("select * from subject where estatus='1' and id in (".$rowusers1['subject'].")");
					while($rowexam=mysqli_fetch_array($exam)){ ?>
						
						<option value="<?php echo $rowexam['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($rowexam['id']==$_REQUEST['subject']) { echo 'selected="selected"'; } } ?>  ><?php echo $rowexam['subject'];?></option>
					<?php } ?>
				</select>
			</div>
			</div>
			</div>
	</div>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	$selusers=$database->query("select * from users where valid='1' and username='".$_SESSION['username']."'");
	$rowusers=mysqli_fetch_array($selusers);
	$rowee=$database->query("select * from subject where estatus='1' and id in (".$rowusers['subject'].") order by id asc limit 0,1");
	$rowee1=mysqli_fetch_array($rowee);
		if(isset($_REQUEST['subject'])){ 
			if($_REQUEST['subject']!=''){
				$_REQUEST['subject']=$_REQUEST['subject'];
			}else{
				$_REQUEST['subject']=$rowee1['id'];
				
			}
		}else{
			$_REQUEST['subject']=$rowee1['id'];
		}
	?>
	
		
		<div class="row">
			<?php
			$i=1;
		$sell=$database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."' and id in (".rtrim($rowusers['chapter'],",").")");
		// $sell=$database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."'");
			while($rowl=mysqli_fetch_array($sell)){ 
				$sellt=$database->query("select count(id) as count from topic where estatus='1' and subject='".$_REQUEST['subject']."' and chapter='".$rowl['id']."'");
				$rowlt=mysqli_fetch_array($sellt);
			?>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
					<div class="single-card mb-4">
						<div class="card border-0 shadow-sm h-100">
							<div class="card-body">
								<div class="content d-flex flex-row align-items-start">
									<div class="counter"><?php echo $i; ?></div>
									<div class="ml-3 subjects-text">
										
											<a  class="learn-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>')" ><h6 class="card-title mb-0"><?php echo $rowl['chapter']; ?></h6>
											<p class="mb-2"> Topics: <span><?php echo $rowlt['count']; ?></span> </p></a>
											<!-- <a  class="text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getchapterdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>')" ><p class="mb-2"> Topics: <span><?php echo $rowlt['count']; ?></span> </p> -->
										</a>
										<a  class="learn-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>')" >Learn
											Revision Materials <i class="ml-1 fas fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php  $i++; } ?>
			
		</div>
 
	 <?php
	
	}
	if(isset($_REQUEST['getchapterdata'])){
		?>
		<div class="row">
			<?php
			$i=1;
			$sell=$database->query("select * from topic where estatus='1' and subject='".$_REQUEST['subject']."' and chapter='".$_REQUEST['chapter']."'");
			while($rowl=mysqli_fetch_array($sell)){ 
			?>
				  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
						<div class="single-card mb-4">
							<div class="card border-0 shadow-sm h-100">
								<div class="card-body">
									<div class="content flex-row d-flex justify-content-between align-items-center">
										<div class="mr-3 topics-text d-flex align-items-center">
											<div class="counter mr-3"><?php echo $i; ?></div>
											<h6 class="card-title mb-0 w-100"><?php echo $rowl['topic']; ?></h6>
										</div>
										<p class="card-text mb-0 percentage">10%</p>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php  $i++; } ?>
			
		</div>
 
	 <?php

	}
	if(isset($_REQUEST['getcustomdata'])){
		if(isset($_REQUEST['conttype'])){
			if($_REQUEST['conttype']!=''){
				$contype=" AND conttype='".$_REQUEST['conttype']."'";
			}else{
				$contype="";
			}
		}else{
			$contype="";
		}
		?>
		
			<?php
			$i=1;
			
			?>
				   <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <ul class="nav nav-tabs card-header-tabs tab-notes" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-shortnote-tab" data-toggle="pill" onclick="materialsfun(1);"
                                                href="#pills-shortnote" role="tab" aria-controls="pills-shortnote"
                                                aria-selected="true">Short Note</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-materials-tab" onclick="materialsfun(2);" data-toggle="pill"
                                                href="#pills-materials" role="tab" aria-controls="pills-materials"
                                                aria-selected="false">Materials</a>
												
                                        </li>
                                    </ul>
									
									<div >
										
											
										<div class="d-flex">		
											<div class="form-group mb-0 row">
												<label class="col-sm-4 col-form-label">Topic</label>
												<div class="col-sm-8">
													
													<select id="topic" class="form-control"  onchange="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&conttype='+$('#conttype').val()+'&topic='+$('#topic').val()+'&text=shortnote')"  >
													<option value=''>--Select--</option>
													<?php
														echo "select * from topic where estatus='1' and  chapter='".$_REQUEST['chapter']."'";
													$sel1=$database->query("select * from topic where estatus='1' and  chapter='".$_REQUEST['chapter']."'");
													while($rowl1=mysqli_fetch_array($sel1)){
														?>
															<option value="<?php echo $rowl1['id'];?>" <?php if(isset($_REQUEST['topic'])) { if($rowl1['id']==$_REQUEST['topic']) { echo 'selected="selected"'; } } ?>  ><?php echo $rowl1['topic'];?></option>
													<?php
													}
													?>
												</select>
													<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
												</div>
											</div>
										
											<div class="form-group materialdropdown mb-0 row px-2"  style="display:none;">
												<label class="col-sm-4 col-form-label">Type</label>
												<div class="col-sm-8">
													<select id="conttype" class="form-control"  onchange="setState('materialsNote','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getmaterialdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&conttype='+$('#conttype').val()+'&topic='+$('#topic').val()+'&text=material')"  >
													<option value=''>--Select--</option>
													<?php
													$sel1=$database->query("select * from customcontent_types where estatus='1' and id!=1");
													while($rowl1=mysqli_fetch_array($sel1)){
														$sqlc=$database->query("select count(id) as cnt  from customcontent where estatus='1' and subject='".$_REQUEST['subject']."' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and conttype='".$rowl1['id']."'  ");
														$rowc=mysqli_fetch_array($sqlc);
														if($rowc['cnt']>0){
														?>
															<option value="<?php echo $rowl1['id'];?>" <?php if(isset($_REQUEST['conttype'])) { if($rowl1['id']==$_REQUEST['conttype']) { echo 'selected="selected"'; } } ?>  ><?php echo $rowl1['customcontent'];?> (<?php echo $rowc['cnt']; ?>)</option>
													<?php
														}
													}
													?>
												</select>
													<span class="error"><?php if(isset($_SESSION['error']['class1'])){ echo $_SESSION['error']['class1'];}?></span>
												</div>
											</div>
										</div>		
											
											
									</div>
                                </div>
								
                                <div class="card-body tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-shortnote" role="tabpanel"
                                        aria-labelledby="pills-shortnote-tab">
                                        <div class="shortNote">
											<div class="right-content">
												<?php
												if(isset($_REQUEST['topic'])){
													if($_REQUEST['topic']!='' && $_REQUEST['topic']!='undefined'){
														$topic=" AND find_in_set(".$_REQUEST['topic'].",topic)>0 ";
													}else{
														$topic="";
													}
												}else{
													$topic="";
												}
											if(strlen($_REQUEST['topic'])>0){ ?>
												<h6>Topic: <?php echo $database->get_name('topic','id',$_REQUEST['topic'],'topic'); ?></h6>
												<?php } ?>
											</div>
                                            <ul class="list-unstyled shortnote-list">
												<?php
												$sql1=$database->query("select * from customcontent where estatus='1' and subject='".$_REQUEST['subject']."' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and conttype='1' ".$topic." ");
												while($rowl1=mysqli_fetch_array($sql1)){
													$string = urldecode($rowl1['description']);
													$yourText = urldecode($rowl1['description']);
													if (strlen($string) > 500) {
														$stringCut = substr($string, 0, 500);
														$doc = new DOMDocument();
														$doc->loadHTML($stringCut);
														$yourText = $doc->saveHTML();
													}
													
												?>
													<li>
														<div class="card">
															<div class="card-body">
																<h5 class="card-title"><?php echo $rowl1['title']; ?></h5>
																<p class="card-text shortnotesstyle"><?php echo $yourText; ?></p>
																<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&topic=<?php echo $rowl1['topic'];?>&id=<?php echo $rowl1['id']; ?>&type=Shortnotes')" >Read more</a>
																<!--<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none text-primary" onClick="setState('formsubmit','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&editform=<?php echo $rowl1['id']; ?>')" >Read more</a>-->
															</div>
														</div>
													</li>
												<?php } ?>
                                               
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-materials" role="tabpanel"
                                        aria-labelledby="pills-materials-tab">
                                        <div class="materialsNote" id="materialsNote">
                                            <ul class="list-unstyled materialsnote-list">
												<?php
											
												$sql11=$database->query("select * from customcontent where estatus='1' and subject='".$_REQUEST['subject']."' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and conttype!='1' ".$contype.$topic." ");
												while($rowl11=mysqli_fetch_array($sql11)){
													/*$string = urldecode($rowl1['description']);
													$yourText = urldecode($rowl1['description']);
													if (strlen($string) > 500) {
														$stringCut = substr($string, 0, 500);
														$doc = new DOMDocument();
														$doc->loadHTML($stringCut);
														$yourText = $doc->saveHTML();
													}*/
												?>
													<li>
														<div class="card">
															<div class="card-body">
																<h5 class="card-title"><?php echo $rowl11['title']; ?></h5>
																<p class="card-text"><?php echo urldecode($rowl11['description']); ?> </p>
																<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&topic=<?php echo $rowl11['topic'];?>&id=<?php echo $rowl11['id']; ?>&type=Materials')" >Read more</a>
																<!--<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none text-primary" onClick="setState('formsubmit','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&editform=<?php echo $rowl11['id']; ?>')" >Read more</a>-->
															</div>
														</div>
													</li>
                                               <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
			<?php   ?>
			
		
 
	 <?php

	}
	?>
	<script>
	function materialsfun(val){
		if(val==2){
			$('.materialdropdown').show();
		}else{
			$('.materialdropdown').hide();
		}
	}
	</script>
	<?php
	if(isset($_REQUEST['getfullshortnotesdata'])){
		?>
			<div class="breadcrumb-content d-md-flex justify-content-between align-items-center pr-3 mb-3"><h5 class="mb-0 pl-3 pt-3"><?php echo $_REQUEST['type']; ?></h5><a class="btn btn-link text-dark" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>')" ><i class="fa fa-long-arrow-alt-left"></i> Back</a></div>
			
		
		<?php
		
		if($_REQUEST['type']=='Shortnotes'){
				$i=1;
			$sell=$database->query("select * from customcontent where estatus='1'  and  find_in_set(".$_REQUEST['topic'].",topic)>0  and conttype='1'  ");
			while($rowl=mysqli_fetch_array($sell)){
			
				$sello=$database->query("select * from users where username='".$session->username."'");
				$row=mysqli_fetch_array($sello);
				$row['class']=rtrim($row['class'],',');
				$row['chapter']=rtrim($row['chapter'],',');
				$row['topic']=rtrim($row['topic'],',');
				
				if($row['class']=='1'){
					$class='XI';
				}else if($row['class']=='2'){
					$class='XII';
				}else  if($row['class']=='1,2'){
					$class='XI,XII';
				}
				$exam ='';
				$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$rowl['exam'].")"); 
				while($rowexam=mysqli_fetch_array($sqlexam)){
					$exam .= $rowexam['exam'].",";
				}
				$chapter ='';
				$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$rowl['chapter'].")"); 
				while($row2=mysqli_fetch_array($sql)){
					$chapter .= $row2['chapter'].",";
				}

				$topic ='';
				$k=1;
				$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$rowl['topic'].")"); 
				while($row1=mysqli_fetch_array($zSql1)){
					$topic .= $row1['topic'].",";
					$k++;
				}
				?>
				
				
				
				<div class="row pt-3">
					<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
						<div class="card border-0 shadow-sm">
							<div class="card-header border-0 d-flex justify-content-between align-items-center">
								
								<h5 class="card-title  text-primary mb-0"><?php echo $rowl['title']; ?></h5>
								<div class="d-flex align-items-center">
									<?php
									if($rowl['conttype']==1){?>
										<a  class="text-decoration-none mr-2" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&topic=<?php echo $rowl['topic'];?>&type=Shortnotes&editform=<?php echo $rowl['id']; ?>')" ><i
											class="fas fa-edit"></i></a>
									<?php
									}else{
									?>
									<a  class="text-decoration-none mr-2" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&topic=<?php echo $_REQUEST['topic'];?>&type=Shortnotes&editform=<?php echo $rowl['id']; ?>')" ><i
											class="fas fa-edit"></i></a>
									<?php } ?>
									<a class=" " style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>customcontent_lec/process1.php','viewDetails=1&id=<?php echo $_REQUEST['id'];?>')"><i class="fa fa-eye" style="color:green;"></i></a>
								</div>
							</div>
							<div class="card-body pt-0">
								
								<div class="row">
									<div class="col-md-1">
										<h6>Class</h6>
										<p class="text-dark"><?php echo $class; ?></p>
									</div>
									
									<div class="col-md-2">
										<h6>Exam</h6>
										<p class="text-dark"><?php echo rtrim($exam,","); ?></p>
									</div>
									<div class="col-md-2">
										<h6>Subject</h6>
										<p class="text-dark"><?php echo $database->get_name('subject','id',$rowl['subject'],'subject'); ?></p>
									</div>
									<div class="col-md-3">
										<h6>Chapter</h6>
										<p class="text-dark"><?php echo rtrim($chapter,","); ?></p>
									</div>
									<?php
									if(strlen($topic)>0){ ?>
										<div class="col-md-4">
											<h6>Topic</h6>
											<p class="text-dark"><?php echo rtrim($topic,","); ?></p>
										</div>
									<?php } ?>
								</div>
								<div class="row pt-4">
									<div class="col-md-12 ">
										<h6 class="text-warning">Custom Content ID :<?php echo $rowl['id']; ?></h6>
										
									</div>
									

								</div>
								<div class="row pt-2">
									<div class="col-md-12 ">
										<h6 class="text-success">Content Type</h6>
										<p class="text-dark"><?php echo $database->get_name('customcontent_types','id',$rowl['conttype'],'customcontent'); ?></p>
									</p>
									</div>
									

								</div>
								<div class="row pt-2">
									<div class="col-md-12">
										<h6 class="text-danger">Description</h6>
										<p class="card-text "><?php echo urldecode($rowl['description']); ?>
									</p>
									</div>
									

								</div>
								<div class="form-group row">
									<div class="col-lg-12 text-left pt-5" >
										<a class="radius-20 btn btn-theme px-5"  data-toggle="modal" data-target="#previewModal1"  style="pointer:cursor;"   onClick="setStateGet('mobilePreview1','<?php echo SECURE_PATH;?>customcontent_lec/ajax1.php','getmobileview=1&id=<?php echo $rowl['id']; ?>')" > MOBILE VIEW</a>
										<?php if($rowl['rstatus']=='0'){ ?>
											<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','validateForm1=1&id=<?php echo $rowl['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_REQUEST['topic']; ?>&status=0&conttype=<?php echo $rowl['conttype']; ?>&type=Shortnotes')"> Review</a>
											
										<?php }else if($rowl['rstatus']=='1'){ ?>
											<a class="fas fa-check-circle text-success pr-1" > Reviewed</a>
										<?php } ?>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>		 
		 
			 <?php
			}

		}else{
			$i=1;
			$sell=$database->query("select * from customcontent where estatus='1'  and  id='".$_REQUEST['id']."'");
			$rowl=mysqli_fetch_array($sell);
			
			$sello=$database->query("select * from users where username='".$session->username."'");
			$row=mysqli_fetch_array($sello);
			$row['class']=rtrim($row['class'],',');
			$row['chapter']=rtrim($row['chapter'],',');
			$row['topic']=rtrim($row['topic'],',');
			
			if($row['class']=='1'){
				$class='XI';
			}else if($row['class']=='2'){
				$class='XII';
			}else  if($row['class']=='1,2'){
				$class='XI,XII';
			}
			$exam ='';
			$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$rowl['exam'].")"); 
			while($rowexam=mysqli_fetch_array($sqlexam)){
				$exam .= $rowexam['exam'].",";
			}
			$chapter ='';
			$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$rowl['chapter'].")"); 
			while($row2=mysqli_fetch_array($sql)){
				$chapter .= $row2['chapter'].",";
			}

			$topic ='';
			$k=1;
			$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$rowl['topic'].")"); 
			while($row1=mysqli_fetch_array($zSql1)){
				$topic .= $row1['topic'].",";
				$k++;
			}
			?>
			<!--<div class="breadcrumb-content d-md-flex justify-content-between align-items-center pr-3 mb-3"><h5 class="mb-0 pl-3 pt-3"><?php echo $_REQUEST['type']; ?></h5><a class="btn btn-link text-dark" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>')" ><i class="fa fa-long-arrow-alt-left"></i> Back</a></div>-->
			
			
			<div class="row">
				<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
					<div class="card border-0 shadow-sm">
						<div class="card-header border-0 d-flex justify-content-between align-items-center">
							
							<h5 class="card-title  text-primary mb-0"><?php echo $rowl['title']; ?></h5>
							<div class="d-flex align-items-center">
								<a  class="text-decoration-none mr-2" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&editform=<?php echo $_REQUEST['id']; ?>')" ><i
										class="fas fa-edit"></i></a>
								
								<a class=" " style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>customcontent_lec/process1.php','viewDetails=1&id=<?php echo $_REQUEST['id'];?>')"><i class="fa fa-eye" style="color:green;"></i></a>
							</div>
						</div>
						<div class="card-body pt-0">
							
							<div class="row">
								<div class="col-md-1">
									<h6>Class</h6>
									<p class="text-dark"><?php echo $class; ?></p>
								</div>
								
								<div class="col-md-2">
									<h6>Exam</h6>
									<p class="text-dark"><?php echo rtrim($exam,","); ?></p>
								</div>
								<div class="col-md-2">
									<h6>Subject</h6>
									<p class="text-dark"><?php echo $database->get_name('subject','id',$rowl['subject'],'subject'); ?></p>
								</div>
								<div class="col-md-3">
									<h6>Chapter</h6>
									<p class="text-dark"><?php echo rtrim($chapter,","); ?></p>
								</div>
								<?php
								if(strlen($topic)>0){ ?>
									<div class="col-md-4">
										<h6>Topic</h6>
										<p class="text-dark"><?php echo rtrim($topic,","); ?></p>
									</div>
								<?php } ?>
							</div>
							<div class="row pt-4">
								<div class="col-md-12 ">
									<h6 class="text-warning">Custom Content ID :<?php echo $rowl['id']; ?></h6>
									
								</div>
								

							</div>
							<div class="row pt-2">
								<div class="col-md-12 ">
									<h6 class="text-success">Content Type</h6>
									<p class="text-dark"><?php echo $database->get_name('customcontent_types','id',$rowl['conttype'],'customcontent'); ?></p>
								</p>
								</div>
								

							</div>
							<div class="row pt-2">
								<div class="col-md-12">
									<h6 class="text-danger">Description</h6>
									<p class="card-text"><?php echo urldecode($rowl['description']); ?>
								</p>
								</div>
								

							</div>
							<!--<div class="form-group row">
								<div class="col-lg-12 text-left pt-5" >
									
									<?php if($rowl['rstatus']=='0'){ ?>
										<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','validateForm1=1&id=<?php echo $rowl['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_REQUEST['topic']; ?>&status=0&conttype=<?php echo $rowl['conttype']; ?>')"> Review and Next</a>
									<?php }else if($rowl['rstatus']=='1'){ ?>
										<a class="fas fa-check-circle text-success pr-1" > Reviewed</a>
									<?php } ?>
								</div>
							</div>-->

							<div class="form-group row">
								<div class="col-lg-12 text-left pt-5" >
									<a class="radius-20 btn btn-theme px-5"  data-toggle="modal" data-target="#previewModal1"  style="pointer:cursor;"   onClick="setStateGet('mobilePreview1','<?php echo SECURE_PATH;?>customcontent_lec/ajax1.php','getmobileview=1&id=<?php echo $rowl['id']; ?>')" > MOBILE VIEW</a>
									<?php if($rowl['rstatus']=='0'){ ?>
										<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','validateForm1=1&id=<?php echo $rowl['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_REQUEST['topic']; ?>&status=0&conttype=<?php echo $rowl['conttype']; ?>&type=')"> Review</a>
										
									<?php }else if($rowl['rstatus']=='1'){ ?>
										<a class="fas fa-check-circle text-success pr-1" > Reviewed</a>
									<?php } ?>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>		 
	 
		 <?php
		}
	}
	if(isset($_REQUEST['validateForm1'])){
		$id=$_REQUEST['id']; 
		$sqluser=$database->query("select * from users where valid='1' and username='".$_SESSION['username']."'");
		$rowuser=mysqli_fetch_array($sqluser);

		$sellr=$database->query("select * from customcontent where estatus='1' and subject IN (".rtrim($rowuser['subject'],",").") and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=0 and conttype='".$_POST['conttype']."'");
		$rowcountr=mysqli_fetch_array($sellr);
		
		$sellr1=$database->query("select * from customcontent where estatus='1' and subject IN (".rtrim($rowuser['subject'],",").") and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=1 and conttype='".$_POST['conttype']."'");
		$rowcountr1=mysqli_fetch_array($sellr1);
		
		if($_POST['status']=='0'){
			$result=$database->query('update customcontent set rstatus="1",rusername="'.$_SESSION['username'].'",rtimestamp="'.time().'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'" where id="'.$id.'" ');
			if($result){
				if($_POST['type']=='Shortnotes'){
					
					$sellr2=$database->query("select * from customcontent where estatus='1' and subject IN (".rtrim($rowuser['subject'],",").") and chapter in (".rtrim($rowuser['chapter'],",").") and find_in_set(".$_POST['topic'].",topic)>0 and id!='".$id."' and  id > '".$id."'  ");
					$rowcountr2=mysqli_fetch_array($sellr2);
					if($rowcountr2['id']!=''){
					?>
					<script type="text/javascript">
					
					
						alert("Data Reviewed Successfully");
						$('#adminForm').show();
						setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_POST['subject']; ?>&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=<?php echo $_POST['type']; ?>&id=<?php echo rowcountr2['id']; ?>');
						
					</script>
						<?php
					}else{
								
							?>
							<script type="text/javascript">
							 
								alert("Data Reviewed Successfully");
							//	$('#formsubmit').hide();
								$('#adminForm').show();
							setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
								
							</script>
					<?php
					}

				}else{
					if($rowcountr['id']!=''){
					?>
					<script type="text/javascript">
					
					
						alert("Data Reviewed Successfully");
						$('#adminForm').show();
						setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_POST['subject']; ?>&chapter=<?php echo $_POST['chapter']; ?>&id=<?php echo $rowcountr['id']; ?>&conttype=<?php echo $_POST['conttype;']; ?>');
						
					</script>
				<?php
				}else{
						
					?>
					<script type="text/javascript">
					 
						alert("Data Reviewed Successfully");
						$('#formsubmit').hide();
						$('#adminForm').show();
					setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
						
					</script>
				<?php
				}
				}
			}
				
		}else if($_POST['status']=='1'){
			$sello=$database->query("select rstatus from customcontent where estatus='1' and id='".$id."'");
			$rowllo=mysqli_fetch_array($sello);
		
		
		$result=$database->query('update customcontent set rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'" where id="'.$id.'" ');
				unset($_SESSION['description']);
				
				if($result){
					if($rowcountr1['id']!=''){
					?>
					<script type="text/javascript">
						alert("Data Saved Successfully");
						$('#adminForm').show();
						setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_POST['subject']; ?>&chapter=<?php echo $_POST['chapter']; ?>&id=<?php echo $rowcountr1['id']; ?>&conttype=<?php echo $_POST['conttype;']; ?>');
						
					</script>
				<?php
				}else{
						
					?>
					<script type="text/javascript">
						alert("Data Saved Successfully");
						$('#adminForm').show();
						setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
						
					</script>
				<?php
				}
					
			}
		}
	}

	if(isset($_REQUEST['geteditdata'])){
	
if(isset($_REQUEST['formsubmit'])){

	?>
		
	<?php
	
	if($_REQUEST['formsubmit'] == 2 && isset($_POST['editform'])){
		
	
	 $data_sel = $database->query("SELECT * FROM customcontent WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);

	
 ?>
 <script type="text/javascript">
 $('#formsubmit').slideDown();
 </script>
 
 <?php
    }
 }
	if(isset($_REQUEST['errordisplay'])){
		$_POST['description']=$_SESSION['description'];
		
		
	}
	
	$data_sel1 =  $database->query("select * from users where username='".$session->username."'");
	$rowu = mysqli_fetch_array($data_sel1);
	 $data_sel2 = $database->query("SELECT rstatus FROM customcontent WHERE id = '".$_POST['editform']."'");
	 $rowsel2=mysqli_fetch_array($data_sel2);
	 $_POST['rstatus']= $rowsel2['rstatus'];
 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<script>
		$('#chapter').selectpicker1();
		$('#topic').selectpicker2();
		getFields1();
		getFields2();
	</script>
	<script type="text/javascript">
	// $('#adminForm').show();
 
 </script>
<!--<div class="breadcrumb-content d-md-flex justify-content-between align-items-center pr-3 mb-3"><h5 class="mb-0 pl-3 pt-3"><?php echo $_REQUEST['type']; ?></h5><a class="btn btn-link text-dark" onClick=" setState('formsubmit','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>')" ><i class="fa fa-long-arrow-alt-left"></i> Back</a></div>-->
 <div class="card">

	<div class="card-body">
	
	<div class="col-lg-12 col-md-12" id="myDiv">
		
			<?php
			$i=1;
			$sell=$database->query("select * from customcontent where estatus='1'  and  id='".$_REQUEST['editform']."'");
			$rowl=mysqli_fetch_array($sell);
			
			$sello=$database->query("select * from users where username='".$session->username."'");
			$row=mysqli_fetch_array($sello);
			$row['class']=rtrim($row['class'],',');
			$row['chapter']=rtrim($row['chapter'],',');
			$row['topic']=rtrim($row['topic'],',');
			
			if($row['class']=='1'){
				$class='XI';
			}else if($row['class']=='2'){
				$class='XII';
			}else  if($row['class']=='1,2'){
				$class='XI,XII';
			}
			$exam ='';
			$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$rowl['exam'].")"); 
			while($rowexam=mysqli_fetch_array($sqlexam)){
				$exam .= $rowexam['exam'].",";
			}
			$chapter ='';
			$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$rowl['chapter'].")"); 
			while($row2=mysqli_fetch_array($sql)){
				$chapter .= $row2['chapter'].",";
			}

			$topic ='';
			$k=1;
			$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$rowl['topic'].")"); 
			while($row1=mysqli_fetch_array($zSql1)){
				$topic .= $row1['topic'].",";
				$k++;
			}
			?>
			
		
			
		<div  class="getquestionview" >
			<div class=" border-0 d-flex justify-content-between align-items-center">
						
				<div class="d-flex align-items-center " style="padding-left:900px;">
					
					<a  class="btn-link text-decoration-none mr-2" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&id=<?php echo $_REQUEST['editform']; ?>&topic=<?php echo $_REQUEST['topic']; ?>&type=<?php echo $_REQUEST['type']; ?>')" ><i class="fa fa-long-arrow-alt-left"></i> Back</a>
					<a class=" " style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>customcontent_lec/process1.php','viewDetails=1&id=<?php echo $_REQUEST['editform'];?>')"><i class="fa fa-eye" style="color:green;"></i></a>
				</div>
			</div>
			<div class="row align-items-top">
				<div class="col-md-2">
					<?php
					$_POST['class1']=$_POST['class'];						
					if(isset($_POST['class'])){
						if($_POST['class']!=''){
							$_POST['class']=explode(",",$_POST['class']);
						}
						
					}else{
						
					}

					
					?>
					<label for="inputExam">Class<span class="text-danger">*</span></label>
					<div class="form-group">
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="customcheckboxInline3" name="customcheckboxInline2" class="class3 custom-control-input" onChange="getFields2();setStateGet('aaa','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', $_POST['class'])) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', $_POST['class'])) { echo 'checked'; } } ?> >
							<label class="custom-control-label" for="customcheckboxInline3">XI</label>
						</div>
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="customcheckboxInline4" name="customcheckboxInline2" class="class3 custom-control-input" onChange="getFields2();setStateGet('aaa','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', $_POST['class'])) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', $_POST['class'])) { echo 'checked'; }} ?> >
							<label class="custom-control-label" for="customcheckboxInline4">XII</label>
						</div>
						<input type="hidden" class="class3" id="class" value="<?php echo $_POST['class1']; ?>" >
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['class'])){ echo $_SESSION['error1']['class'];}?></span>
					</div>
				</div>
				<?php
				$_POST['exam1']=$_POST['exam'];						
				if(isset($_POST['exam'])){
					if($_POST['exam']!=''){
						$_POST['exam']=explode(",",$_POST['exam']);
					}
					
				}else{
					
				}

				
				?>
				<div class="col-md-2">
					<label for="inputExam">Exam<span class="text-danger">*</span></label>
					<div class="form-group">
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="customcheckboxInline1" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam1" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('1', $_POST['exam'])) { echo '1'; }else { echo '1'; } } else { echo '1'; } } else { echo '1'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('1', $_POST['exam'])) { echo 'checked'; } } ?> >
							<label class="custom-control-label" for="customcheckboxInline1">NEET</label>
						</div>
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="customcheckboxInline2" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam2" value="<?php if(isset($_POST['exam'])) { if(in_array('2', $_POST['exam'])) { echo '2'; }else { echo '2'; } } else { echo '2'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('2', $_POST['exam'])) { echo 'checked'; } } ?> >
							<label class="custom-control-label" for="customcheckboxInline2">JEE</label>
						</div>
						<div class="custom-control custom-checkbox custom-control-inline">
							<input type="checkbox" id="customcheckboxInline5" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam3" value="<?php if(isset($_POST['exam'])) { if(in_array('3', $_POST['exam'])) { echo '3'; }else { echo '3'; } } else { echo '3'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('3', $_POST['exam'])) { echo 'checked'; } } ?> >
							<label class="custom-control-label" for="customcheckboxInline5">EAMCET</label>
						</div>
						<input type="hidden" class="exam" id="exam" value="<?php echo $_POST['exam1']; ?>" >
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['exam'])){ echo $_SESSION['error1']['exam'];}?></span>
					</div>
				</div>
									
				<div class="form-group col-md-2" id="aaa">
					<label for="inputSub">Subjects</label>
					 <select class="form-control" name="subject" value=""   id="subject" onChange="setState('ccc','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','getchapter=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')">
						<option value=''>-- Select --</option>
						<?php
						/*if($session->userlevel=='9'){
							$row = $database->query("select * from subject where estatus='1'");
						}else{
							$row = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').") ");

						}*/

						if($session->userlevel=='9'){
							if(rtrim($_POST['exam1'],",")=='1'){
								$row1 = $database->query("select * from subject where estatus='1'  and id!='4'");
							}else if(rtrim($_POST['exam1'],",")=='2' || rtrim($_POST['exam1'],",")=='3'){
								$row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5 ");
							}else if(rtrim($_POST['exam1'],",")=='1,2' || rtrim($_POST['exam1'],",")=='1,3'){
								$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3)");
							}else if(rtrim($_POST['exam1'],",")=='1,2,3'){
								$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3)");
							}else{
								$row1 = $database->query("select * from subject where estatus='1' ");
							}
						}else{
							if(rtrim($_POST['exam1'],",")=='1'){
								$row1 = $database->query("select * from subject where estatus='1' and id!='4' AND id IN(".rtrim($rowu['subject'],',').")");
							}else if(rtrim($_POST['exam1'],",")=='2' || rtrim($_POST['exam1'],",")=='3'){
								$row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5  AND id IN(".rtrim($rowu['subject'],',').") ");
							}else if(rtrim($_POST['exam1'],",")=='3'){
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').") ");
							}else if(rtrim($_POST['exam1'],",")=='1,2' || rtrim($_POST['exam1'],",")=='1,2,3' ){
								$row1 = $database->query("select * from subject where estatus='1' and  id in (2,3) AND id IN(".rtrim($rowu['subject'],',').")");
							}else if(rtrim($_POST['exam1'],",")=='1,3'){
								$row1 = $database->query("select * from subject where estatus='1'  and  id!=4  AND id IN(".rtrim($rowu['subject'],',').")");
							}else if(rtrim($_REQUEST['exam1'],",")=='2,3'){
								$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3,4) AND id IN(".rtrim($rowu['subject'],',').")");
							}else{
								$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').")  ");
							}
						}
						while($data = mysqli_fetch_array($row1))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
						<?php
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['subject'])){ echo $_SESSION['error1']['subject'];}?></span>
				</div>
				<?php
				$_POST['chapter1']=$_POST['chapter'];
				$_POST['chapter']=explode(",",$_POST['chapter']);
				$_POST['topic']=explode(",",$_POST['topic']);
				?>
				<div class="form-group col-md-3" id="ccc" >
					<label for="inputTopic">Chapter</label>
					<select class="form-control selectpicker1" name="chapter" value=""  multiple data-live-search="true"  id="chapter" onChange="setState('ddd','<?php echo SECURE_PATH;?>customcontent_lec/ajax.php','gettopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'')" >
						
						<?php
						//$row = $database->query("SELECT * FROM `chapter` WHERE class IN  (".rtrim($_POST['class1'],',').") and subject = '".$_POST['subject']."' AND id IN(".rtrim($_POST['chapter'],',').")"); 
						
						if($session->userlevel=='9'){
							$row = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") ");
						}else{
							$row = $database->query("select * from chapter where estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and id IN(".rtrim($rowu['chapter'],",").")");
							
						}
						if(mysqli_num_rows($row)>0)
						{
							while($data = mysqli_fetch_array($row))
							{
								?>
							
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
							<?php
							}
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['chapter'])){ echo $_SESSION['error1']['chapter'];}?></span>
				</div>
				<div class="form-group col-md-3" id="ddd">
					<label for="inputTopic">Topic</label>
					<select class="form-control selectpicker2 custom-drop-down" name="topic" multiple data-live-search="true" value=""   id="topic" >
						<?php
						if($session->userlevel=='9'){
							$zSql1 = $database->query("SELECT * FROM `topic` WHERE estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and chapter IN(".rtrim($_POST['chapter1'],",").") ");  
						}else{
							$zSql1 = $database->query("select * from topic where estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and chapter IN(".rtrim($_POST['chapter1'],",").")  ");
						}
						if(mysqli_num_rows($zSql1)>0)
						{ 
							while($data = mysqli_fetch_array($zSql1))
							{ 
								?> 
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['topic'])) { if(in_array($data['id'], $_POST['topic'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['topic'];?></option>
								
							<?php
							}
							
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
				</div>
				
			</div>
		
	
	
				<div class="col-lg-12 col-md-12">
					<div class="form-group row">
						<label class="col-sm-12" nowrap>CustomContent ID: <?php echo $_POST['editform']; ?></label>
						
					</div>
				</div>
			<?php
			
			
		?>
			<!-- <div class="col-lg-12 col-md-12">
			<div class="form-group row">
				<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
				<div class="col-sm-3 mt-2">
				<?php 
			$sql=$database->query("select * from customcontent_types where estatus='1' and id='".$_POST['conttype']."'");	
					$rol=mysqli_fetch_array($sql);
					?>
					
				<input type="text" class="form-control" readonly name="conttype" id="conttype1" value="<?php if(isset($_POST['conttype'])){ echo $rol['customcontent']; } ?>" >
				<input type="hidden" class="form-control" readonly name="conttype" id="conttype2" value="<?php if(isset($_POST['conttype'])){ echo $_POST['conttype']; } ?>" >
				</div>
			</div> -->
		<?php
		if($_POST['conttype']=='5'){
				?>
				<div class="col-lg-12 col-md-12">
				<div class="form-group row">
					<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
					<div class="col-sm-3 mt-2">
						
						<select class="form-control" name="conttype" value=""   id="conttype" >
						
						<?php
							
							$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							if(mysqli_num_rows($zSql12)>0)
							{ 
								while($data = mysqli_fetch_array($zSql12))
								{ 
									?> 
									<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
									
								<?php
								}
								
							}
							?>
					</select>
					</div>
				</div>
			<?php
			}else if($_POST['conttype']=='9'){
				?>
				<div class="col-lg-12 col-md-12">
				<div class="form-group row">
					<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
					<div class="col-sm-3 mt-2">
						
						<!-- <input type="text" class="form-control" readonly name="conttype" id="conttype" value="<?php if(isset($_POST['conttype'])){ echo $_POST['conttype']; } ?>" > -->
						<select class="form-control" name="conttype" value=""   id="conttype" >
						
						<?php
							
							//$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							if(mysqli_num_rows($zSql12)>0)
							{ 
								while($data = mysqli_fetch_array($zSql12))
								{ 
									?> 
									<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
									
								<?php
								}
								
							}
							?>
					</select>
					</div>
				</div>
			<?php
			}else{
			
		?>
			<div class="col-lg-12 col-md-12">
			<div class="form-group row">
				<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
				<div class="col-sm-3 mt-2">
					
					<!-- <input type="text" class="form-control" readonly name="conttype" id="conttype" value="<?php if(isset($_POST['conttype'])){ echo $_POST['conttype']; } ?>" > -->
					<select class="form-control" name="conttype" value=""   id="conttype" >
					
					<?php
						
						//$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
						$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and  id NOT IN (5,9) ");  
						if(mysqli_num_rows($zSql12)>0)
						{ 
							while($data = mysqli_fetch_array($zSql12))
							{ 
								?> 
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
								
							<?php
							}
							
						}
						?>
				</select>
				</div>
			</div>
		<?php
			}
		
	?>
	<?php
		
		if(isset($_POST['conttype'])){
			if($_POST['conttype']=='5'){
			?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value='<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Video Url<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="video_link" id="video_link" class="form-control video_link " value='<?php if(isset($_POST['video_link'])) { echo $_POST['video_link']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['video_link'])){ echo $_SESSION['error']['video_link'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>
											<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}else if($_POST['conttype']=='9'){
			?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value='<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="col-lg-6">
					<div class="form-group row">
					  <label class="col-sm-4 col-form-label">File Upload<span style="color:red;">*</span></label>
						<div class="col-sm-8">
						  <div id="file-uploader2" style="display:inline">
								<noscript>
									<p>Please enable JavaScript to use file uploader.</p>
									<!-- or put a simple form for upload here -->
								</noscript>

							</div>
						<script>

							function createUploader(){

								var uploader = new qq.FileUploader({
									element: document.getElementById('file-uploader2'),
									action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=file&upload_type=single&filetype=file',
									debug: true,
									multiple:false
								});
							}

							createUploader();



							// in your app create uploader as soon as the DOM is ready
							// don't wait for the window to load

						</script>
						<input type="hidden" name="file" id="file" value="<?php if(isset($_POST['file'])) { echo $_POST['file']; } ?>"/>
					<?php
					if(isset($_POST['file']))
					{
						if($_POST['file']!=''){
					?>
							<a href="<?php echo SECURE_PATH."files/".$_POST['file'];?>" target="_blank" >View Document</a>
					<?php
						}

					}
					?>

					<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['file'])){ echo $_SESSION['error']['file'];}?></span>
						</div>
					</div>
					
				  </div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>
											<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}else{
			?>
			
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value="<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>" > 
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>

<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}
		}else{
			
	?>
	
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
					<div class="col-sm-10">
						<input type="title" id="title" class="form-control title"  autocomplete="off" value="<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>"> 
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
					<div class="col-sm-10">
						<div class="wrs_container">
							<div class="wrs_row">
								<div class="wrs_col wrs_s12">
									<div id="editorContainer">
										<div id="toolbarLocation"></div>
											<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
									</div>
									<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	<?php
		}
	?>
		</div>
	</div>
		<script type="text/javascript" >
		createEditorInstance("en", {});
	</script>
	<script>
				function rand(){
		
					
					var conttype=$('#conttype').val();
					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';
					


				 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>customcontent_lec/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							console.log("reply");
							setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&conttype='+$('#conttype').val()+'&title='+$('#title').val()+'&video_link='+$('#video_link').val()+'&file='+$('#file').val()+'&status=0&type=<?php echo $_POST['type']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['class2'])) { echo '&class1='.$_POST['class2']; } ?><?php if(isset($_POST['exam2'])) { echo '&exam1='.$_POST['exam2']; } ?><?php if(isset($_POST['subject2'])) { echo '&subject1='.$_POST['subject2']; } ?><?php if(isset($_POST['chapter2'])) { echo '&chapter1='.$_POST['chapter2']; } ?><?php if(isset($_POST['conttype2'])) { echo '&conttype1='.$_POST['conttype2']; } ?>');
					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				}); 
			}

			function rand1(){
					
					var conttype=$('#conttype').val();
					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';
					


				 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>customcontent_lec/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							console.log("reply");
							setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&conttype='+$('#conttype').val()+'&title='+$('#title').val()+'&video_link='+$('#video_link').val()+'&file='+$('#file').val()+'&status=1&type=<?php echo $_POST['type']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['class2'])) { echo '&class1='.$_POST['class2']; } ?><?php if(isset($_POST['exam2'])) { echo '&exam1='.$_POST['exam2']; } ?><?php if(isset($_POST['subject2'])) { echo '&subject1='.$_POST['subject2']; } ?><?php if(isset($_POST['chapter2'])) { echo '&chapter1='.$_POST['chapter2']; } ?><?php if(isset($_POST['conttype2'])) { echo '&conttype1='.$_POST['conttype2']; } ?>');
					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				}); 
			}


   function mobilePreview(){

 var conttype=$('#conttype').val();
					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';
					

 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>customcontent_lec/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							

 console.log('description',tinymce.get('description').getContent());

                                               var html = data2.description;
                                               var iframe = $('#displayframe');
                                               
										     iframe[0].srcdoc =    (tinymce.get('description').getContent());
										 

console.log("iframe",iframe);

					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				}); 

}

 
				</script>

		<div class="form-group row">
			<div class="col-lg-12 text-center">
				<?php if($_POST['rstatus']=='0'){ ?>
					<a class="radius-20 btn btn-theme px-5 savebutton" onClick="rand1();$('.savebutton').hide();"> Save and Next</a>
					<a class="radius-20 btn btn-theme px-5 reviewbutton" onClick="rand();$('.reviewbutton').hide();"> Review and Next</a>
				<?php }else if($_POST['rstatus']=='1'){ ?>
					<a class="radius-20 btn btn-theme px-5 savebutton" onClick="rand1();$('.savebutton').hide();"> Save and Next</a>
				<?php } ?>
			</div>
		
		</div>
	</div>
	
<?php
unset($_SESSION['error']);
unset($_SESSION['description']);
}
	
	}
?>

<?php
	if(isset($_POST['validateForm'])){
	
		$_SESSION['error'] = array();
		
		$post = $session->cleanInput($_POST);

			$id = 'NULL';
				if(isset($post['editform'])){
		  $id = $post['editform'];

		}
		if(isset($post['conttype'])){
			 if($post['conttype']=='5'){
				
				if(!$post['video_link'] || strlen(trim($post['video_link'])) == 0){

					$_SESSION['error']['video_link'] = "Please Enter Video Link";

				}
				if(!$post['title'] || strlen(trim($post['title'])) == 0){

					$_SESSION['error']['title'] = "*Please Enter Title";

				}
				if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){

					$_SESSION['error']['description'] = "Please Enter Description";

				}
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				/*if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}*/
				$ytarray=explode("/", $post['video_link']);
				$ytendstring=end($ytarray);
				$ytendarray=explode("?v=", $ytendstring);
				$ytendstring=end($ytendarray);
				$ytendarray=explode("&", $ytendstring);
				$ytcode=$ytendarray[0];
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					
					
					<script type="text/javascript">
					$('#adminForm').show();
						
					setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&errordisplay=1&conttype=<?php echo $post['conttype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&title=<?php echo $post['title']; ?>&vedio_link=<?php echo $post['vedio_link']; ?>&file=<?php echo $post['file']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($post['class1'])) { echo '&class2='.$post['class1']; } ?><?php if(isset($post['exam1'])) { echo '&exam2='.$post['exam1']; } ?><?php if(isset($post['subject1'])) { echo '&subject2='.$post['subject1']; } ?><?php if(isset($post['chapter1'])) { echo '&chapter2='.$post['chapter1']; } ?><?php if(isset($post['conttype1'])) { echo '&conttype2='.$post['conttype1']; } ?>');
					</script>
					</script>
					<?php
				}else{
					
					
					if($post['class']!='undefined'){
						$sqluser=$database->query("select class,subject,chapter,username from users where valid='1' and username='".$_SESSION['username']."'");
						$rowuser=mysqli_fetch_array($sqluser);
						
						$sellr=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=0 and conttype='".$_POST['conttype']."' ");
						$rowcountr=mysqli_fetch_array($sellr);

						$sellr1=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."'  and conttype='".$_POST['conttype']."' ");
						$rowcountr1=mysqli_fetch_array($sellr1);

						if($post['status']=='0'){
						
						$selle=$database->query("select conttype,etimestamp from customcontent where estatus='1' and id='".$id."'");
						$rower1=mysqli_fetch_array($selle);

						$database->query("insert customcontent_log set cid='".$lid."',conttype_pre='".$rower1['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");
						

						$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",video_link="'.$ytcode.'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="1",rusername="'.$_SESSION['username'].'",rtimestamp="'.time().'" where id="'.$id.'" ');

						$lid = mysqli_insert_id($database->connection);

								
						unset($_SESSION['description']);
						
						if($result){
							if($_POST['type']=='Shortnotes'){
							$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									<!--<script type="text/javascript">
									
									
										alert("Data Reviewed Successfully");
										$('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										
									</script>-->
									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										},3000);
										
									</script>
								<?php
								}else{
										
									?>
									
									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										},3000);
										
									</script>
								<?php
								}
							}else{
								if($rowcountr['id']!=''){
								?>
								
								<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
										},3000);
										
									</script>
							<?php
							}else{
									
								?>
								
								<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										},3000);
										
									</script>
							<?php
							}
						}
							
					}
				}else if($post['status']=='1'){
					$sello=$database->query("select rstatus,conttype from customcontent where estatus='1' and id='".$id."'");
					$rowllo=mysqli_fetch_array($sello);
					$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rowllo['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");
					
					
				$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",video_link="'.$ytcode.'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'" where id="'.$id.'" ');
				unset($_SESSION['description']);

					
						if($result){
							if($_POST['type']=='Shortnotes'){
							$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
											},3000);
											
										</script>
								<?php
								}else{
										
									?>
									
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
											},3000);
											
										</script>
								<?php
								}
							}else{
								if($rowcountr1['id']!=''){
								?>
								<script type="text/javascript">
									alert("Data Saved Successfully");
									//$('#formsubmit').hide();
									$('#adminForm').show();
									setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&editform=<?php echo $rowcountr1['id']; ?>');
									
								</script>
							<?php
							}else{
									
								?>
								
								<script type="text/javascript">
										
								$("#savedtoast").toast({
									delay: 3000,
									autohide: true,

								});
								$('#savedtoast').toast('show');
								setTimeout(function(){ 
									 $('#adminForm').show();
									setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
								},3000);
								
							</script>
							<?php
							}
						}
						}
							
					}
				}else{
							?>
							
							<script type="text/javascript">
										
									$("#failedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
						<?php
								
						}
					
					
				}
				
			}else if($post['conttype']=='9'){
				if(!$post['title'] || strlen(trim($post['title'])) == 0){

					$_SESSION['error']['title'] = "*Please Enter Title";

				}
				if(!$post['file'] || strlen(trim($post['file'])) == 0){

					$_SESSION['error']['file'] = "*Please Enter file";

				}
				if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){

					$_SESSION['error']['description'] = "Please Enter Description";

				}
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				/*if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}*/

				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					<script type="text/javascript">
					$('#adminForm').show();

					setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&errordisplay=1&conttype=<?php echo $post['conttype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&title=<?php echo $post['title']; ?>&vedio_link=<?php echo $post['vedio_link']; ?>&file=<?php echo $post['file']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($post['class1'])) { echo '&class2='.$post['class1']; } ?><?php if(isset($post['exam1'])) { echo '&exam2='.$post['exam1']; } ?><?php if(isset($post['subject1'])) { echo '&subject2='.$post['subject1']; } ?><?php if(isset($post['chapter1'])) { echo '&chapter2='.$post['chapter1']; } ?><?php if(isset($post['conttype1'])) { echo '&conttype2='.$post['conttype1']; } ?>');
					</script>
					<?php
				}else{
					
					
					if($post['class']!='undefined'){
						$sqluser=$database->query("select class,subject,chapter,username from users where valid='1' and username='".$_SESSION['username']."'");
						$rowuser=mysqli_fetch_array($sqluser);
						
						$sellr=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=0 and conttype='".$_POST['conttype']."' ");
						$rowcountr=mysqli_fetch_array($sellr);

						$sellr1=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."'  and conttype='".$_POST['conttype']."' ");
						$rowcountr1=mysqli_fetch_array($sellr1);

						if($post['status']=='0'){
						
						$selle=$database->query("select conttype from customcontent where estatus='1' and id='".$id."'");
						$rower1=mysqli_fetch_array($selle);
						
						$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rower1['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");
						
						$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",file="'.$post['file'].'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="1",rusername="'.$_SESSION['username'].'",rtimestamp="'.time().'" where id="'.$id.'"');
								
						unset($_SESSION['description']);

						
						if($result){
							if($_REQUEST['type']=='Shortnotes'){
							$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									


									
									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										},3000);
										
									</script>
								<?php
								}else{
										
									?>
									

									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										},3000);
										
									</script>
								<?php
								}
							}else{
							if($rowcountr['id']!=''){
							?>
								
								<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
										},3000);
										
									</script>
							<?php
							}else{
									
								?>
								
								<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										},3000);
										
									</script>
							<?php
							}
						}
						
							
					}
				}else if($post['status']=='1'){
					$sello=$database->query("select rstatus,conttype from customcontent where estatus='1' and id='".$id."'");
					$rowllo=mysqli_fetch_array($sello);
					$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rowllo['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");

					echo 'update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",file="'.$post['file'].'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'" where id="'.$id.'"';
					$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",file="'.$post['file'].'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'" where id="'.$id.'"');
					
								
						unset($_SESSION['description']);

						
						if($result){
							if($_REQUEST['type']=='Shortnotes'){ 
								$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr['id']!=''){
									?>
									

									<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
									},3000);
									
							</script>
								<?php
								}else{
										
									?>
									

									<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
								<?php
								}
							}else{
								if($rowcountr1['id']!=''){
								?>
								

								<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr1['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
									},3000);
									
							</script>
							<?php
							}else{
									
								?>
								

								<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
							<?php
							}
						}
							
					}
				}else{
					?>
							
							<script type="text/javascript">
										
									$("#failedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
						<?php
								
						}
					}
					
				}
			}else{
				if(!$post['title'] || strlen(trim($post['title'])) == 0){

					$_SESSION['error']['title'] = "*Please Enter Title";

				}
				if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){

					$_SESSION['error']['description'] = "Please Enter Description";

				}
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
			/*	if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}*/

				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					<script type="text/javascript">
					$('#adminForm').show();

					setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&errordisplay=1&conttype=<?php echo $post['conttype'];?>&type=<?php echo $post['type'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&title=<?php echo $post['title']; ?>&vedio_link=<?php echo $post['vedio_link']; ?>&file=<?php echo $post['file']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($post['class1'])) { echo '&class2='.$post['class1']; } ?><?php if(isset($post['exam1'])) { echo '&exam2='.$post['exam1']; } ?><?php if(isset($post['subject1'])) { echo '&subject2='.$post['subject1']; } ?><?php if(isset($post['chapter1'])) { echo '&chapter2='.$post['chapter1']; } ?><?php if(isset($post['conttype1'])) { echo '&conttype2='.$post['conttype1']; } ?>');
					</script>
					<?php
				}else{
					
						
					if($post['class']!='undefined'){
						$sqluser=$database->query("select class,subject,chapter,username from users where valid='1' and username='".$_SESSION['username']."'");
						$rowuser=mysqli_fetch_array($sqluser);
						
						
						$sellr=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=0 and conttype='".$_POST['conttype']."' ");
						$rowcountr=mysqli_fetch_array($sellr);
						
						$sellr1=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."'  and conttype='".$_POST['conttype']."' ");
						$rowcountr1=mysqli_fetch_array($sellr1);

						$selle=$database->query("select conttype from customcontent where estatus='1' and id='".$id."'");
						$rower1=mysqli_fetch_array($selle);

						
						
							
						if($post['status']=='0'){

						$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rower1['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");

						
						
						$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$_POST['conttype'].'",title="'.$post['title'].'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="1",rusername="'.$_SESSION['username'].'",rtimestamp="'.time().'" where id="'.$id.'"');
								
						unset($_SESSION['description']);
						
						
						
						if($result){
							if($_REQUEST['type']=='Shortnotes'){ 
								
								$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									
									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										},3000);
										
									</script>
								<?php
								}else{
										
									?>
									
									<script type="text/javascript">
										
										$("#verifytoast").toast({
											delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										},3000);
										
									</script>
								<?php
								}
							}else{
							if($rowcountr['id']!=''){
							?>
							
							<script type="text/javascript">
										
								$("#verifytoast").toast({
									delay: 3000,
									autohide: true,

								});
								$('#verifytoast').toast('show');
								setTimeout(function(){ 
									 $('#adminForm').show();
									setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
								},3000);
								
							</script>
						<?php
						}else{
								
							?>
							<!-- <script type="text/javascript">
							 
								alert("Data Reviewed Successfully");
								$('#adminForm').show();
							setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
								
							</script> -->
							<script type="text/javascript">
										
								$("#verifytoast").toast({
									delay: 3000,
									autohide: true,

								});
								$('#verifytoast').toast('show');
								setTimeout(function(){ 
									 $('#adminForm').show();
									setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
								},3000);
								
							</script>
						<?php
						}
							}
							
					}
				}else if($post['status']=='1'){
					$sello=$database->query("select rstatus,conttype from customcontent where estatus='1' and id='".$id."'");
					$rowllo=mysqli_fetch_array($sello);

					$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rower1['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");
					
					$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",description="'.$_SESSION['description'].'",rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'" where id="'.$id.'"');
								
						unset($_SESSION['description']);
						

						
						if($result){
							if($_REQUEST['type']=='Shortnotes'){ 
								$sellr2=$database->query("select id from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
											},3000);
											
										</script>
								<?php
								}else{
										
									?>
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
											},3000);
											
										</script>
								<?php
								}
							}else{

								if($rowcountr1['id']!=''){
								?>
								
								<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr1['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
									},3000);
									
								</script>
							<?php
							}else{
									
								?>
								
								<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
								</script>
							<?php
							}
						}
							
					}
				}else{
							?>
							
							<script type="text/javascript">
										
									$("#savedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#savedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
						<?php
								
						}
					}else{
						?>
								
								

									<script type="text/javascript">
										
									$("#failedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
							<?php
					}
					
				}

			}

			}else{
				if(!$post['title'] || strlen(trim($post['title'])) == 0){

					$_SESSION['error']['title'] = "*Please Enter Title";

				}
				if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){

					$_SESSION['error']['description'] = "Please Enter Description";

				}
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}

				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					<script type="text/javascript">
					$('#adminForm').show();

					setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&errordisplay=1&conttype=<?php echo $post['conttype'];?>&type=<?php echo $post['type'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&title=<?php echo $post['title']; ?>&vedio_link=<?php echo $post['vedio_link']; ?>&file=<?php echo $post['file']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($post['class1'])) { echo '&class2='.$post['class1']; } ?><?php if(isset($post['exam1'])) { echo '&exam2='.$post['exam1']; } ?><?php if(isset($post['subject1'])) { echo '&subject2='.$post['subject1']; } ?><?php if(isset($post['chapter1'])) { echo '&chapter2='.$post['chapter1']; } ?><?php if(isset($post['conttype1'])) { echo '&conttype2='.$post['conttype1']; } ?>');
					</script>
					<?php
				}else{
					if($post['class']!='undefined'){
						$sqluser=$database->query("select * from users where valid='1' and username='".$_SESSION['username']."'");
						$rowuser=mysqli_fetch_array($sqluser);
						
						$sellr=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."' and rstatus=0 and conttype='".$_POST['conttype']."' ");
						$rowcountr=mysqli_fetch_array($sellr);
						
						$sellr1=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").")  and id!='".$id."' and  id > '".$id."'  and conttype='".$_POST['conttype']."' ");
						$rowcountr1=mysqli_fetch_array($sellr1);
						$selle=$database->query("select conttype from customcontent where estatus='1' and id='".$id."'");
						$rower1=mysqli_fetch_array($selle);

						$database->query("insert customcontent_log set cid='".$id."',conttype_pre='".$rower1['conttype']."',conttype_new='".$post['conttype']."',username='".$_SESSION['username']."',estatus='1',timestamp='".time()."'");
						

						if($post['status']=='0'){
						
						$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",description="'.$_SESSION['description'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'",rstatus="1",rusername="'.$_SESSION['username'].'",rtimestamp="'.time().'" where id="'.$id.'"');
								
						unset($_SESSION['description']);

						
						if($result){


							if($_REQUEST['type']=='Shortnotes'){ 
								$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									<script type="text/javascript">
									
									
										alert("Data Reviewed Successfully");
										//$('#formsubmit').show();
										$('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										
									</script>
								<?php
								}else{
										
									?>
									<script type="text/javascript">
									 
										alert("Data Reviewed Successfully");
										//$('#formsubmit').hide();
										$('#adminForm').show();
									setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
										
									</script>
								<?php
								}
							}else{
								if($rowcountr['id']!=''){
								?>
								<!-- <script type="text/javascript">
								
								
									alert("Data Reviewed Successfully");
									//$('#formsubmit').show();
									$('#adminForm').show();
									setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
									
								</script> -->

								<script type="text/javascript">
										
											$("#verifytoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#verifytoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
											},3000);
											
										</script>
							<?php
							}else{
									
								?>
								<!-- <script type="text/javascript">
								 
									alert("Data Reviewed Successfully");
									$('#formsubmit').hide();
									$('#adminForm').show();
								setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									
								</script> -->
								<script type="text/javascript">
										
											$("#verifytoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#verifytoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
											},3000);
											
										</script>
							<?php
							}
						}
							
					}
				}else if($post['status']=='1'){
					$sello=$database->query("select rstatus,conttype from customcontent where estatus='1' and id='".$id."'");
					$rowllo=mysqli_fetch_array($sello);

					$result=$database->query('update customcontent set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",conttype="'.$post['conttype'].'",title="'.$post['title'].'",description="'.$_SESSION['description'].'",rstatus="'.$rowllo['rstatus'].'",rusername="'.$rowllo['rusername'].'",rtimestamp="'.$rowllo['rtimestamp'].'",eusername="'.$_SESSION['username'].'",etimestamp="'.time().'" where id="'.$id.'"');
								
						unset($_SESSION['description']);
						
						

						
						if($result){
							if($_REQUEST['type']=='Shortnotes'){ 
								$sellr2=$database->query("select * from customcontent where estatus='1' and subject='".$post['subject']."' and chapter in (".rtrim($rowuser['chapter'],",").") and topic in (".rtrim($_POST['topic'],",").")  and id!='".$id."' and  id > '".$id."'  ");
								$rowcountr2=mysqli_fetch_array($sellr2);
								if($rowcountr2['id']!=''){
									?>
									<!-- <script type="text/javascript">
									
									
										alert("Data Saved Successfully");
										//$('#formsubmit').show();
										$('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
										
									</script> -->
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr2['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&type=Shortnotes');
											},3000);
											
										</script>
								<?php
								}else{
										
									?>
									

									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
											},3000);
											
										</script>
								<?php
								}
							}else{
									if($rowcountr1['id']!=''){
									?>
									<!-- <script type="text/javascript">
										alert("Data Saved Successfully");
										$('#adminForm').show();
										setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr1['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');
										
									</script> -->
									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												//$('#datafill').hide();
												setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&editform=<?php echo $rowcountr1['id']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>');;
											},3000);
											
										</script>
								<?php
								}else{
										
									?>
									

									<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
											},3000);
											
										</script>

								<?php
								}
							}
							
					}
				}else{
							?>
							<script type="text/javascript">
										
									$("#failedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
						<?php
								
						}
					}else{
						?>
								
									<script type="text/javascript">
										
									$("#failedtoast").toast({
										delay: 3000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										 $('#adminForm').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1');
									},3000);
									
							</script>
							<?php
					}
					
				}
				
			}
		
		
	
	}
	
	
	?>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
</script>
	<script type="text/javascript">


function getFields2()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.exam').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#exam').val(ass);
	});
        
        

    
}
</script>
<script>
function getquestionview1(){
	
		$('.getquestionview').show();
		$('.questionViewdata').hide();
	}
	function getquestionview2(){
		$('.getquestionview').hide();
		$('.questionViewdata').show();
		

	}
</script>
<script>

   
	function search_report() {
			var class1 = $('#class1').val();
			var exam1 = $('#exam1').val();
			var subject1 = $('#subject1').val();
			var chapter1 = $('#chapter1').val();
			var conttype1 = $('#conttype1').val();
			
			setStateGet('adminTable','<?php echo SECURE_PATH;?>customcontent_lec/process.php','tableDisplay=1&class1='+class1+'&exam1='+exam1+'&subject1='+subject1+'&chapter1='+chapter1+'&conttype1='+conttype1);
		}
	</script>
	<script>


 
				</script>

<?php

		/* function mobiledata3($id){
			 echo "alert('a')";
			
			$sql=$database->query("select * from customcontent where estatus='1'  and id='".$id."'");
			$row=mysqli_fetch_array($sql);

			?>
				<script>
				alert(<?php echo $id; ?>);
				var html = <?php echo $row['description']; ?>
				 var iframe = $('#displayframe');
				 iframe[0].srcdoc =    (<?php echo urldecode($row['description']); ?>);
			<?php
		}*/


?>