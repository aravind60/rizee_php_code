<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>
 <link href="<?php echo SECURE_PATH;?>vendor/app/css/subjects.css" rel="stylesheet">

<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	$selusers=$database->query("select * from users where valid='1' and username='".$_SESSION['username']."'");
	$rowusers=mysqli_fetch_array($selusers);
	$rowee=$database->query("select * from subject where estatus='1' and id in (".$rowusers['subject'].") order by id asc limit 0,1");
	$rowee1=mysqli_fetch_array($rowee);
		if(isset($_REQUEST['subject'])){ 
			if($_REQUEST['subject']!=''){
				$_REQUEST['subject']=$_REQUEST['subject'];
			}else{
				$_REQUEST['subject']=$rowee1['id'];
				
			}
		}else{
			$_REQUEST['subject']=$rowee1['id'];
		}
	?>
	 <div class="breadcrumbs-area2">
		<div class="row">
		<div class="col-md-12">
		<div class="d-flex justify-content-between align-items-center border-bottom pb-3 mt-3 mb-4">
				<div class="title">
					<h1 class="text-uppercase" id="printpdf">Chapters <small>Lets get a quick Overview</small>
					</h1>
				</div>
				<div class="form-group mb-1 w-25">
				<select   id="subject" class="form-control"   onchange="setStateGet('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','addForm=1&subject='+$('#subject').val()+'')" >
				<?php $exam=$database->query("select * from subject where estatus='1' and id in (".$rowusers['subject'].")");
					while($rowexam=mysqli_fetch_array($exam)){ ?>
						
						<option value="<?php echo $rowexam['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($rowexam['id']=$_REQUEST['subject']) { echo 'selected="selected"'; } } ?>  ><?php echo $rowexam['subject'];?></option>
					<?php } ?>
				</select>
			</div>
			</div>
			</div>
	</div>
		
		<div class="row">
			<?php
			$i=1;
			$sell=$database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."' and id in (".$rowusers['chapter'].")");
		// $sell=$database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."'");
			while($rowl=mysqli_fetch_array($sell)){ 
				$sellt=$database->query("select count(id) as count from topic where estatus='1' and subject='".$_REQUEST['subject']."' and chapter='".$rowl['id']."'");
				$rowlt=mysqli_fetch_array($sellt);
			?>
				<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
					<div class="single-card mb-4">
						<div class="card border-0 shadow-sm h-100">
							<div class="card-body">
								<div class="content d-flex flex-row align-items-start">
									<div class="counter"><?php echo $i; ?></div>
									<div class="ml-3 subjects-text">
										<a  class="text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getchapterdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>')" >
											<h6 class="card-title mb-0"><?php echo $rowl['chapter']; ?></h6>
											<p class="mb-2"> Topics: <span><?php echo $rowlt['count']; ?></span> </p>
										</a>
										<a  class="learn-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getcustomdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>')" >Learn
											Revision Materials <i class="ml-1 fas fa-angle-double-right"></i></a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php  $i++; } ?>
			
		</div>
 
	 <?php
	
	}
	if(isset($_REQUEST['getchapterdata'])){
		?>
		<div class="row">
			<?php
			$i=1;
			$sell=$database->query("select * from topic where estatus='1' and subject='".$_REQUEST['subject']."' and chapter='".$_REQUEST['chapter']."'");
			while($rowl=mysqli_fetch_array($sell)){ 
			?>
				  <div class="col-xl-4 col-lg-6 col-md-6 col-sm-12">
						<div class="single-card mb-4">
							<div class="card border-0 shadow-sm h-100">
								<div class="card-body">
									<div class="content flex-row d-flex justify-content-between align-items-center">
										<div class="mr-3 topics-text d-flex align-items-center">
											<div class="counter mr-3"><?php echo $i; ?></div>
											<h6 class="card-title mb-0 w-100"><?php echo $rowl['topic']; ?></h6>
										</div>
										<p class="card-text mb-0 percentage">10%</p>
									</div>
								</div>
							</div>
						</div>
					</div>
			<?php  $i++; } ?>
			
		</div>
 
	 <?php

	}
	if(isset($_REQUEST['getcustomdata'])){
		?>
		
			<?php
			$i=1;
			
			?>
				   <div class="row">
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
                            <div class="card">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <ul class="nav nav-tabs card-header-tabs tab-notes" id="pills-tab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="pills-shortnote-tab" data-toggle="pill"
                                                href="#pills-shortnote" role="tab" aria-controls="pills-shortnote"
                                                aria-selected="true">Short Note</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="pills-materials-tab" data-toggle="pill"
                                                href="#pills-materials" role="tab" aria-controls="pills-materials"
                                                aria-selected="false">Materials</a>
												
												<!-- <div class="col-lg-4">
													<div class="form-group row">
														<label class="col-sm-4 col-form-label">From Date</label>
														<div class="col-sm-8">
															
															<input type="text" name="fdate" id="fdate" class="datepicker form-control" value="<?php echo $_REQUEST['fdate'];?>" onblur="search_report();" placeholder="Select Date" >
															<span class="error"><?php if(isset($_SESSION['error']['fdate'])){ echo $_SESSION['error']['fdate'];}?></span>
														</div>
													</div>
												</div> -->
                                        </li>
                                    </ul>
									<div class="tab-content">
										<div class="tab-pane fade" id="pills-materials" role="tabpanel" aria-labelledby="pills-materials-tab">
											<div class="form-group mb-0 w-100">
												<input type="text" class="form-control">
											</div>
										</div>
									</div>
                                </div>
                                <div class="card-body tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade show active" id="pills-shortnote" role="tabpanel"
                                        aria-labelledby="pills-shortnote-tab">
                                        <div class="shortNote">
                                            <ul class="list-unstyled shortnote-list">
												<?php
												
												$sql1=$database->query("select * from customcontent where estatus='1' and subject='".$_REQUEST['subject']."' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and conttype='1' ");
												while($rowl1=mysqli_fetch_array($sql1)){
													$string = urldecode($rowl1['description']);
													$yourText = urldecode($rowl1['description']);
													if (strlen($string) > 500) {
														$stringCut = substr($string, 0, 500);
														$doc = new DOMDocument();
														$doc->loadHTML($stringCut);
														$yourText = $doc->saveHTML();
													}
													
												?>
													<li>
														<div class="card">
															<div class="card-body">
																<h5 class="card-title"><?php echo $rowl1['title']; ?></h5>
																<p class="card-text shortnotesstyle"><?php echo $yourText; ?></p>
																<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>&id=<?php echo $rowl1['id']; ?>')" >Read more</a>
															</div>
														</div>
													</li>
												<?php } ?>
                                               
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="pills-materials" role="tabpanel"
                                        aria-labelledby="pills-materials-tab">
                                        <div class="materialsNote">
                                            <ul class="list-unstyled materialsnote-list">
												<?php
												$sql11=$database->query("select * from customcontent where estatus='1' and subject='".$_REQUEST['subject']."' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and conttype!='1' ");
												while($rowl11=mysqli_fetch_array($sql11)){
													$string = urldecode($rowl1['description']);
													$yourText = urldecode($rowl1['description']);
													if (strlen($string) > 500) {
														$stringCut = substr($string, 0, 500);
														$doc = new DOMDocument();
														$doc->loadHTML($stringCut);
														$yourText = $doc->saveHTML();
													}
												?>
													<li>
														<div class="card">
															<div class="card-body">
																<h5 class="card-title"><?php echo $rowl11['title']; ?></h5>
																<p class="card-text"><?php echo $yourText; ?> </p>
																<a  class="mt-2 p-0 btn btn-link stretched-link text-decoration-none" onClick="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_lec/process.php','getfullshortnotesdata=1&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>&id=<?php echo $rowl11['id']; ?>')" >Read more</a>
															</div>
														</div>
													</li>
                                               <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
			<?php   ?>
			
		
 
	 <?php

	}
	if(isset($_REQUEST['getfullshortnotesdata'])){
		$i=1;
		$sell=$database->query("select * from customcontent where estatus='1'  and  id='".$_REQUEST['id']."'");
		$rowl=mysqli_fetch_array($sell);

		?>
		<div class="row">
			<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12">
				<div class="card border-0 shadow-sm">
					<div class="card-header border-0 d-flex justify-content-between align-items-center">
						<h5 class="card-title mb-0"><?php echo $rowl['title']; ?></h5>
						<div class="d-flex align-items-center">
							<a  class="text-decoration-none mr-2" onClick="setState('formsubmit','<?php echo SECURE_PATH;?>customcontent_lec/process.php','geteditdata=1&formsubmit=2&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowl['id'];?>&editform=<?php echo $_REQUEST['id']; ?>')" ><i
									class="fas fa-edit"></i></a>
							<a href="#" class="text-decoration-none"><i class="fas fa-trash-alt"></i></a>
						</div>
					</div>
					<div class="card-body pt-0">
						<p class="card-text"><?php echo urldecode($rowl['description']); ?>
						</p>
					</div>
				</div>
			</div>
		</div>		 
 
	 <?php

	}

	if(isset($_REQUEST['geteditdata'])){
	
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['formsubmit'])){
	if($_REQUEST['formsubmit'] == 2 && isset($_POST['editform'])){
		
	 ?>
	 <script>
		$('html, body').animate({
		   scrollTop: $("#myDiv").offset().top
	   }, 2000);
	  </script>
	 <?php
    $data_sel = $database->query("SELECT * FROM customcontent WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);

	
 ?>
 <script type="text/javascript">
 $('#formsubmit').slideDown();
 </script>
 
 <?php
    }
 }
	if(isset($_REQUEST['errordisplay'])){
		$_POST['description']=$_SESSION['description'];
		
		
	}
	
	$data_sel1 =  $database->query("select * from users where username='".$session->username."'");
	$rowu = mysqli_fetch_array($data_sel1);

 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<script>
		$('#chapter').selectpicker1();
		$('#topic').selectpicker2();
		getFields1();
		getFields2();
	</script>
	<script type="text/javascript">
	 $('#adminForm').hide();
 $('#formsubmit').show();
 </script>
 <div class="card">
	<div class="card-body">
	<div class="col-lg-12 col-md-12" id="myDiv">
		<div class="row align-items-top">
            <div class="col-md-2">
			<?php
			$_POST['class1']=$_POST['class'];						
			if(isset($_POST['class'])){
				if($_POST['class']!=''){
					$_POST['class']=explode(",",$_POST['class']);
				}
				
			}else{
				
			}

			
			?>
				<label for="inputExam">Class<span class="text-danger">*</span></label>
				<div class="form-group">
					<div class="custom-control custom-checkbox custom-control-inline">
						<input type="checkbox" id="customcheckboxInline3" name="customcheckboxInline2" class="class3 custom-control-input" onChange="getFields2();setStateGet('aaa','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', $_POST['class'])) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', $_POST['class'])) { echo 'checked'; } } ?> >
						<label class="custom-control-label" for="customcheckboxInline3">XI</label>
					</div>
					<div class="custom-control custom-checkbox custom-control-inline">
						<input type="checkbox" id="customcheckboxInline4" name="customcheckboxInline2" class="class3 custom-control-input" onChange="getFields2();setStateGet('aaa','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'');"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', $_POST['class'])) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', $_POST['class'])) { echo 'checked'; }} ?> >
						<label class="custom-control-label" for="customcheckboxInline4">XII</label>
					</div>
					<input type="hidden" class="class3" id="class" value="<?php echo $_POST['class1']; ?>" >
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['class'])){ echo $_SESSION['error1']['class'];}?></span>
				</div>
			</div>
			<?php
			$_POST['exam1']=$_POST['exam'];						
			if(isset($_POST['exam'])){
				if($_POST['exam']!=''){
					$_POST['exam']=explode(",",$_POST['exam']);
				}
				
			}else{
				
			}

			
			?>
			<div class="col-md-2">
				<label for="inputExam">Exam<span class="text-danger">*</span></label>
				<div class="form-group">
					<div class="custom-control custom-checkbox custom-control-inline">
						<input type="checkbox" id="customcheckboxInline1" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam1" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('1', $_POST['exam'])) { echo '1'; }else { echo '1'; } } else { echo '1'; } } else { echo '1'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('1', $_POST['exam'])) { echo 'checked'; } } ?> >
						<label class="custom-control-label" for="customcheckboxInline1">NEET</label>
					</div>
					<div class="custom-control custom-checkbox custom-control-inline">
						<input type="checkbox" id="customcheckboxInline2" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam2" value="<?php if(isset($_POST['exam'])) { if(in_array('2', $_POST['exam'])) { echo '2'; }else { echo '2'; } } else { echo '2'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('2', $_POST['exam'])) { echo 'checked'; } } ?> >
						<label class="custom-control-label" for="customcheckboxInline2">JEE</label>
					</div>
					<div class="custom-control custom-checkbox custom-control-inline">
						<input type="checkbox" id="customcheckboxInline5" name="customcheckboxInline1" class="exam custom-control-input" onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')" id="exam3" value="<?php if(isset($_POST['exam'])) { if(in_array('3', $_POST['exam'])) { echo '3'; }else { echo '3'; } } else { echo '3'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('3', $_POST['exam'])) { echo 'checked'; } } ?> >
						<label class="custom-control-label" for="customcheckboxInline5">EAMCET</label>
					</div>
					<input type="hidden" class="exam" id="exam" value="<?php echo $_POST['exam1']; ?>" >
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['exam'])){ echo $_SESSION['error1']['exam'];}?></span>
				</div>
			</div>
									
			<div class="form-group col-md-2" id="aaa">
				<label for="inputSub">Subjects</label>
				 <select class="form-control" name="subject" value=""   id="subject" onChange="setState('ccc','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','getchapter=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')">
					<option value=''>-- Select --</option>
					<?php
					/*if($session->userlevel=='9'){
						$row = $database->query("select * from subject where estatus='1'");
					}else{
						$row = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').") ");

					}*/

					if($session->userlevel=='9'){
						if(rtrim($_POST['exam1'],",")=='1'){
							$row1 = $database->query("select * from subject where estatus='1'  and id!='4'");
						}else if(rtrim($_POST['exam1'],",")=='2' || rtrim($_POST['exam1'],",")=='3'){
							$row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5 ");
						}else if(rtrim($_POST['exam1'],",")=='1,2' || rtrim($_POST['exam1'],",")=='1,3'){
							$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3)");
						}else if(rtrim($_POST['exam1'],",")=='1,2,3'){
							$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3)");
						}else{
							$row1 = $database->query("select * from subject where estatus='1' ");
						}
					}else{
						if(rtrim($_POST['exam1'],",")=='1'){
							$row1 = $database->query("select * from subject where estatus='1' and id!='4' AND id IN(".rtrim($rowu['subject'],',').")");
						}else if(rtrim($_POST['exam1'],",")=='2' || rtrim($_POST['exam1'],",")=='3'){
							$row1 = $database->query("select * from subject where estatus='1'  and id!=1 and id!=5  AND id IN(".rtrim($rowu['subject'],',').") ");
						}else if(rtrim($_POST['exam1'],",")=='3'){
							$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').") ");
						}else if(rtrim($_POST['exam1'],",")=='1,2' || rtrim($_POST['exam1'],",")=='1,2,3' ){
							$row1 = $database->query("select * from subject where estatus='1' and  id in (2,3) AND id IN(".rtrim($rowu['subject'],',').")");
						}else if(rtrim($_POST['exam1'],",")=='1,3'){
							$row1 = $database->query("select * from subject where estatus='1'  and  id!=4  AND id IN(".rtrim($rowu['subject'],',').")");
						}else if(rtrim($_REQUEST['exam1'],",")=='2,3'){
							$row1 = $database->query("select * from subject where estatus='1'  and  id in (2,3,4) AND id IN(".rtrim($rowu['subject'],',').")");
						}else{
							$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowu['subject'],',').")  ");
						}
					}
					while($data = mysqli_fetch_array($row1))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['subject']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['subject'])){ echo $_SESSION['error1']['subject'];}?></span>
			</div>
			<?php
				$_POST['chapter1']=$_POST['chapter'];
				$_POST['chapter']=explode(",",$_POST['chapter']);
				$_POST['topic']=explode(",",$_POST['topic']);
				?>
			<div class="form-group col-md-3" id="ccc" >
				<label for="inputTopic">Chapter</label>
				<select class="form-control selectpicker1" name="chapter" value=""  multiple data-live-search="true"  id="chapter" onChange="setState('ddd','<?php echo SECURE_PATH;?>customcontent_v1/ajax.php','gettopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'')" >
					
					<?php
					//$row = $database->query("SELECT * FROM `chapter` WHERE class IN  (".rtrim($_POST['class1'],',').") and subject = '".$_POST['subject']."' AND id IN(".rtrim($_POST['chapter'],',').")"); 
					
					if($session->userlevel=='9'){
						$row = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") ");
					}else{
						$row = $database->query("select * from chapter where estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and id IN(".rtrim($rowu['chapter'],",").")");
						
					}
					if(mysqli_num_rows($row)>0)
					{
						while($data = mysqli_fetch_array($row))
						{
							?>
						
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
						<?php
						}
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error1']['chapter'])){ echo $_SESSION['error1']['chapter'];}?></span>
			</div>
			<div class="form-group col-md-3" id="ddd">
					<label for="inputTopic">Topic</label>
					<select class="form-control selectpicker2 custom-drop-down" name="topic" multiple data-live-search="true" value=""   id="topic" >
						<?php
						if($session->userlevel=='9'){
							$zSql1 = $database->query("SELECT * FROM `topic` WHERE estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and chapter IN(".rtrim($_POST['chapter1'],",").") ");  
						}else{
							$zSql1 = $database->query("select * from topic where estatus='1' and  class IN  (".rtrim($_POST['class1'],',').") and subject IN (".$_POST['subject'].") and chapter IN(".rtrim($_POST['chapter1'],",").")  ");
						}
						if(mysqli_num_rows($zSql1)>0)
						{ 
							while($data = mysqli_fetch_array($zSql1))
							{ 
								?> 
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['topic'])) { if(in_array($data['id'], $_POST['topic'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['topic'];?></option>
								
							<?php
							}
							
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
				</div>
				
			</div>
		</div>
	</div>
	<?php
	if(isset($_POST['editform'])){
		if($_POST['editform']!=''){
			if($_POST['conttype']=='5'){
				?>
				<div class="col-lg-12 col-md-12">
				<div class="form-group row">
					<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
					<div class="col-sm-3 mt-2">
						
						<select class="form-control" name="conttype" value=""   id="conttype" >
						
						<?php
							
							$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							if(mysqli_num_rows($zSql12)>0)
							{ 
								while($data = mysqli_fetch_array($zSql12))
								{ 
									?> 
									<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
									
								<?php
								}
								
							}
							?>
					</select>
					</div>
				</div>
			<?php
			}else if($_POST['conttype']=='9'){
				?>
				<div class="col-lg-12 col-md-12">
				<div class="form-group row">
					<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
					<div class="col-sm-3 mt-2">
						
						<!-- <input type="text" class="form-control" readonly name="conttype" id="conttype" value="<?php if(isset($_POST['conttype'])){ echo $_POST['conttype']; } ?>" > -->
						<select class="form-control" name="conttype" value=""   id="conttype" >
						
						<?php
							
							//$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
							if(mysqli_num_rows($zSql12)>0)
							{ 
								while($data = mysqli_fetch_array($zSql12))
								{ 
									?> 
									<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
									
								<?php
								}
								
							}
							?>
					</select>
					</div>
				</div>
			<?php
			}else{
			
		?>
			<div class="col-lg-12 col-md-12">
			<div class="form-group row">
				<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
				<div class="col-sm-3 mt-2">
					
					<!-- <input type="text" class="form-control" readonly name="conttype" id="conttype" value="<?php if(isset($_POST['conttype'])){ echo $_POST['conttype']; } ?>" > -->
					<select class="form-control" name="conttype" value=""   id="conttype" >
					
					<?php
						
						//$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id='".$_POST['conttype']."'");  
						$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and  id NOT IN (5,9) ");  
						if(mysqli_num_rows($zSql12)>0)
						{ 
							while($data = mysqli_fetch_array($zSql12))
							{ 
								?> 
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
								
							<?php
							}
							
						}
						?>
				</select>
				</div>
			</div>
		<?php
			}
		}
	}else{
	?>
	<div class="col-lg-12 col-md-12">
		<div class="form-group row">
			<label class="col-sm-2">Content Type<span style="color:red;">*</span></label>
			<div class="col-sm-3 mt-2">
				<select class="form-control" name="conttype" value=""   id="conttype" onChange="setState('adminForm','<?php echo SECURE_PATH;?>customcontent_v1/process.php','addForm=1&getconttype=1&conttype='+$('#conttype').val()+'&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'')">
					<option value=''>-- Select --</option>
					
					<?php
						
						$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1'");  
						
						if(mysqli_num_rows($zSql12)>0)
						{ 
							while($data = mysqli_fetch_array($zSql12))
							{ 
								?> 
								<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['conttype'])) { if($_POST['conttype']==$data['id']) { echo 'selected="selected"'; } } ?>  ><?php echo $data['customcontent'];?></option>
								
							<?php
							}
							
						}
						?>
				</select>
			</div>
		</div>
			
	<?php
	}
	?>
	<?php
		if(isset($_POST['conttype'])){
			if($_POST['conttype']=='5'){
			?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value='<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Video Url<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="video_link" id="video_link" class="form-control video_link " value='<?php if(isset($_POST['video_link'])) { echo $_POST['video_link']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['video_link'])){ echo $_SESSION['error']['video_link'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>
											<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}else if($_POST['conttype']=='9'){
			?>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value='<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>' >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>
			
				<div class="row">
					<div class="col-lg-6">
					<div class="form-group row">
					  <label class="col-sm-4 col-form-label">File Upload<span style="color:red;">*</span></label>
						<div class="col-sm-8">
						  <div id="file-uploader2" style="display:inline">
								<noscript>
									<p>Please enable JavaScript to use file uploader.</p>
									<!-- or put a simple form for upload here -->
								</noscript>

							</div>
						<script>

							function createUploader(){

								var uploader = new qq.FileUploader({
									element: document.getElementById('file-uploader2'),
									action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=file&upload_type=single&filetype=file',
									debug: true,
									multiple:false
								});
							}

							createUploader();



							// in your app create uploader as soon as the DOM is ready
							// don't wait for the window to load

						</script>
						<input type="hidden" name="file" id="file" value="<?php if(isset($_POST['file'])) { echo $_POST['file']; } ?>"/>
					<?php
					if(isset($_POST['file']))
					{
						if($_POST['file']!=''){
					?>
							<a href="<?php echo SECURE_PATH."files/".$_POST['file'];?>" target="_blank" >View Document</a>
					<?php
						}

					}
					?>

					<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['file'])){ echo $_SESSION['error']['file'];}?></span>
						</div>
					</div>
					
				  </div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>
											<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}else{
			?>
			
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								
								<input type="title" id="title" class="form-control title" autocomplete="off" value="<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>" > 
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<div class="wrs_container">
									<div class="wrs_row">
										<div class="wrs_col wrs_s12">
											<div id="editorContainer">
												<div id="toolbarLocation"></div>
													<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
											</div>

<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			
			<?php
			}
		}else{
			
	?>
	
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Title<span style="color:red;">*</span></label>
					<div class="col-sm-10">
						<input type="title" id="title" class="form-control title"  autocomplete="off" value="<?php if(isset($_POST['title'])) { echo $_POST['title']; }?>"> 
						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-lg-12">
				<div class="form-group row">
					<label class="col-sm-2 col-form-label">Description / Text <span style="color:red;">*</span></label>
					<div class="col-sm-10">
						<div class="wrs_container">
							<div class="wrs_row">
								<div class="wrs_col wrs_s12">
									<div id="editorContainer">
										<div id="toolbarLocation"></div>
											<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
									</div>
									<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	
	<?php
		}
	?>
		<script type="text/javascript" >
		createEditorInstance("en", {});
	</script>
	<script>
				function rand(){
					
					var conttype=$('#conttype').val();
					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';
					


				 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>customcontent_v1/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							console.log("reply");
							setState('adminForm','<?php echo SECURE_PATH;?>customcontent_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&conttype='+$('#conttype').val()+'&title='+$('#title').val()+'&video_link='+$('#video_link').val()+'&file='+$('#file').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['class2'])) { echo '&class1='.$_POST['class2']; } ?><?php if(isset($_POST['exam2'])) { echo '&exam1='.$_POST['exam2']; } ?><?php if(isset($_POST['subject2'])) { echo '&subject1='.$_POST['subject2']; } ?><?php if(isset($_POST['chapter2'])) { echo '&chapter1='.$_POST['chapter2']; } ?><?php if(isset($_POST['conttype2'])) { echo '&conttype1='.$_POST['conttype2']; } ?>');
					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				}); 
			}


   function mobilePreview(){

 var conttype=$('#conttype').val();
					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';
					

 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>customcontent_v1/img.php',
					data: data2,
					contentType: 'application/json; charset=utf-8',		
					dataType: 'json',
					
					success: function (result,xhr) {
							

 console.log('description',tinymce.get('description').getContent());

                                               var html = data2.description;
                                               var iframe = $('#displayframe');
                                               
										     iframe[0].srcdoc =    (tinymce.get('description').getContent());
										 

console.log("iframe",iframe);

					},
						error: function(e){	

						console.log("ERROR: ", e);
					}
				}); 

}

 
				</script>

		<div class="form-group row">
			<div class="col-lg-12 text-center">
				
				<a class="radius-20 btn btn-theme px-5" onClick="rand();"> Verify and Next</a>
				<a class="radius-20 btn btn-theme px-5" onClick="rand();"> Save and Next</a>

			</div>
		</div>
	</div>
	</div>
<?php
unset($_SESSION['error']);
unset($_SESSION['description']);
}
	}
?>


<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
</script>
	<script type="text/javascript">


function getFields2()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.exam').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#exam').val(ass);
	});
        
        

    
}
</script>
<script>

   
	function search_report() {
			var class1 = $('#class1').val();
			var exam1 = $('#exam1').val();
			var subject1 = $('#subject1').val();
			var chapter1 = $('#chapter1').val();
			var conttype1 = $('#conttype1').val();
			
			setStateGet('adminTable','<?php echo SECURE_PATH;?>customcontent_v1/process.php','tableDisplay=1&class1='+class1+'&exam1='+exam1+'&subject1='+subject1+'&chapter1='+chapter1+'&conttype1='+conttype1);
		}
	</script>

