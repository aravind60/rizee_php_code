<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

//header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Question ID</th>
							<th style="text-align:center;">Previous Paper Year</th>
							<th style="text-align:center;">Set</th>
							<th style="text-align:center;">Deleted Time</th>
							<th style="text-align:center;">Username</th>
						</tr>
                        <?php
                             $k=1;
                            $sel=query("SELECT *  FROM createquestion  WHERE estatus=1 and ppaper='1' and estatus='0'   ORDER BY id ASC");
                            while($row = mysqli_fetch_array($sel)){
                               
								if($row['dusername']!=''){
									$username=$row['dusername'];
									  $dt1=date('d/m/Y H:i:s',$row['dtimestamp']);
								}else{
									$sqll=query("select * from question_log where estatus='1' and message='3' and question_id='".$row['id']."' ");
									$rowl=mysqli_fetch_array($sqll);
									$username=$rowl['username'];
									 $dt1=date('d/m/Y H:i:s',$rowl['timestamp']);
								}
                                    
                                 $checkset1v = query("select * from previous_sets where estatus=1 and id='".$row['qset']."'");
								 $checksetv = mysqli_fetch_array($checkset1v);
								 $checksetv['qset']!=''){
									echo "<tr>";
										?>	
										
										<td><?php echo $k;?></td>
											<td ><?php echo $row['id'];?></td>
											<td ><?php echo $row['year'];?></td>
											<td ><?php echo $checksetv['qset'];?></td>
											<td><?php echo $dt1; ?></td>
											<td ><?php echo $username;?></td>
											
										   
										<?php
										echo "</tr>";
									$k++;
								 }
                             }

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>