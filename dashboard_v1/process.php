<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(1);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);


if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>
<!-- <script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script> -->
<script src="../edut/js/wirisdisplay.js"></script>
<style type="text/css">
		
		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		.dashboardsub .sub-select{
			margin-top:-27px;
		}	
	    .sub-select .form-control {
		border-radius:0;
		background:#e5e6e6f0;
		}  
	</style>
	<style>
	.correctanswer .mce-container{
		display:none !important;
	 }
	  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
  /* .imagewrapper img{
  	height:160px !important;
  	
  } */
	 </style>
	  <style>
        .padding-0 {
            padding: 0.3rem 0.75rem !important;
        }
    </style>
 <script>
		$('.btn-group').on('click', '.btn', function() {
		  $(this).addClass('active').siblings().removeClass('active');
		});
		</script>
		<script>
		getFields();
		getFields1();
		//$('#chapter').selectpicker();
		$('#chapter').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 400,
			 buttonWidth: "100%",
			dropDown: true
		});
		$('#topic').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 400,
			 buttonWidth: "100%",
			dropDown: true
		});
		$('#inputquestion').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 400,
			 buttonWidth: '100%',
			dropDown: true
		});
		$('#conttype').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 400,
			 buttonWidth: '100%',
			dropDown: true
		});
		$("#exam").exampicker1();
		//	selectpicker3
		//$('#topic').selectpicker1();
		</script>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	
 ?>
 
 			<div >
	
		<section class="lecture description-table " id="tabledata"  >
			
			<div class="" >
						<?php 
						if(isset($_REQUEST['report'])){ 
							if($_REQUEST['type']!='Custom Range'){
								$datarange4=$_REQUEST['type'];
							}else{
								$datarange4=$_REQUEST['reportrange'];
							}
						} 
						
						
						?>
						
						<div class="ml-auto">
                        <div class="daterange bg-gray-color" id="reportrange">
                            <i class="far fa-calendar-alt"></i>&nbsp;
                            <span class="ml-4"><?php if(isset($_REQUEST['report'])){ echo $datarange4; }else { echo 'Days Wise';} ?><i class="fa fa-caret-down pl-1 pr-2"></i>
                        </div>
                    </div>
								<?php 
								$con="";
							$data_sel =  $database->query("select * from users where username='".$session->username."' and userlevel='3'");
							$row = mysqli_fetch_array($data_sel);
							$row['subject']=rtrim($row['subject'],",");
							$row['chapter']==rtrim($row['chapter'],",");
							if($row['chapter']!='')
								$row['chapter']=$row['chapter'];
							else
								$row['chapter']='0';
							
							$chapcon='';
							$chapcon1='';
							$chapdata=explode(",",$row['chapter']);
							foreach($chapdata as $chapterdata){
								if($chapterdata!=''){
									$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
									$chapcon1.="  find_in_set(".$chapterdata.",a.chapter)>0"." OR";
								}
							}
							//$chaptercon1=rtrim($chapcon,'OR');
							//$chaptercon2=" AND (".$chaptercon1.")";
							//$chaptercon1=rtrim($chapcon,'OR');
							$chaptercon2="";


							//$chaptercon11=rtrim($chapcon1,'OR');
							//$chaptercon22=" AND (".$chaptercon11.")";
							$chaptercon22="";
							
							$rowee=$database->query("select * from subject where estatus='1' and id in (".$row['subject'].") order by id asc limit 0,1");
							$rowee1=mysqli_fetch_array($rowee);
								if(isset($_REQUEST['sub'])){ 
									if($_REQUEST['sub']!=''){
										$sub=" AND subject=".$_REQUEST['sub']."";
										$sub1=$_REQUEST['sub'];
									}else{
										$sub='';
										$sub1='';
									}
								}else{
									$sub=" AND subject=".$rowee1['id']."";
									$sub1=$rowee1['id'];
								}
							$data = $database->query("select id from createquestion where estatus='1' and subject IN (".$row['subject'].") ".$chaptercon2."  ".$sub." "); 
							$rowd= mysqli_fetch_array($data);

							$count = mysqli_num_rows($data);
							
							$data1 = $database->query("select id from createquestion where estatus='1' and vstatus1=0   and subject IN (".$row['subject'].") ".$chaptercon2."  ".$sub." "); 
							$count1 = mysqli_num_rows($data1);
							
							$data2 = $database->query("select id from createquestion where estatus='1' and vstatus1=1   and subject IN (".$row['subject'].") ".$chaptercon2."  ".$sub." and vusername1='".$session->username."' "); 
							$count2 = mysqli_num_rows($data2);
							$date = strtotime(date('d-m-Y'));
							$date1 = strtotime(date('d-m-Y'). ' 23:59:59');
							
							
							$data3 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$date." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'"); 
							$count3= mysqli_num_rows($data3);
							$date2 = date('d-m-Y',strtotime('-3 days',$date));
							$date3 = strtotime($date2);
							$data4 = $database->query("select id from createquestion where estatus='1' and subject IN (".$row['subject'].")  and vstatus1=1 and vtimestamp1 between ".$date3." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'"); 
							
							$count4= mysqli_num_rows($data4); 
							$date4 = date('d-m-Y',strtotime('-7 days',$date));
							$date5 = strtotime($date4);
							$data5 = $database->query("select id from createquestion where estatus='1' and subject IN (".$row['subject'].")  and vstatus1=1 and vtimestamp1 between ".$date5." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'");
							$count5= mysqli_num_rows($data5); 

							
							
							$count12= mysqli_num_rows($data12);
							$data13 = $database->query("select id from createquestion where estatus='1' and subject IN (".$row['subject'].")  and vstatus1=2 and vtimestamp1 between ".$date." and ".$date1." ".$sub." ".$chaptercon2." "); 
							$count13 = mysqli_num_rows($data13);
							$dateyess9 = strtotime(date('d-m-Y',strtotime("-9 days")));
							$dateyess12= strtotime(date('d-m-Y',strtotime("-12 days")));
							$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
							$dateyess21= strtotime(date('d-m-Y',strtotime("-21 days")));
							$dateyess28= strtotime(date('d-m-Y',strtotime("-28 days")));
							$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
							$dateyess60= strtotime(date('d-m-Y',strtotime("-60 days")));
							$dateyess90= strtotime(date('d-m-Y',strtotime("-90 days")));
							$dateyess120= strtotime(date('d-m-Y',strtotime("-120 days")));
							$dateyess180= strtotime(date('d-m-Y',strtotime("-180 days")));
							$dateyess240= strtotime(date('d-m-Y',strtotime("-240 days")));
							$dateyess364= strtotime(date('d-m-Y',strtotime("-364 days")));

							$data15 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$dateyess15." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."' "); 
							$count15= mysqli_num_rows($data15);

							$data30 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$dateyess30." and ".$date1." ".$sub." ".$chaptercon2."  and vusername1='".$session->username."'"); 
							$count30= mysqli_num_rows($data30);

							$data60 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$dateyess60." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'"); 
							$count60= mysqli_num_rows($data60);

							

							$data90 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$dateyess60." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'"); 
							$count90= mysqli_num_rows($data90);

							$data120 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].")  and vtimestamp1 between ".$dateyess120." and ".$date1." ".$sub." ".$chaptercon2." and vusername1='".$session->username."'"); 
							$count120= mysqli_num_rows($data120);

							$data180 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].") ".$sub." and vtimestamp1 between ".$dateyess180." and ".$date1." ".$chaptercon2." and vusername1='".$session->username."' "); 
							$count180= mysqli_num_rows($data180);

						?>

                    <div class="bg-white border-0 shadow-sm mb-3">
                    	<div class="table-responsive-md">
                        <table class="table table-bordered mb-0">
                            <thead>
                                <tr>
									<th class="padding-0 bg-primary text-white">
										<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject']; ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" ><h5 class="text-white  mb-0">Total Entered</h5>
                                        <h3 class="text-white  mb-0"><?php  if($count!=''){ echo $count; } else{ echo '0'; } ?></h3></a>

									</th>
									<th class="padding-0 bg-light" colspan="1">
										 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=0')" ><h5 class="mb-0">Total Verification Pending</h5>
                                        <h3 class="text-danger  mb-0"><?php  if($count1!=''){ echo $count1; } else{ echo '0'; } ?></h3></a>
									</th>
									 <th class="padding-0" colspan="1">
										<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" > <h5 class="mb-0">Total Verified</h5>
                                        <h3 class="text-success mb-0"><?php  if($count2!=''){ echo $count2; } else{ echo '0'; } ?></h3></a>
									</th>
									<?php
									if(isset($_REQUEST['report'])){
										if($_REQUEST['type']=='Overall'){
											$typ="overall";
											$condd= " and  timestamp between ".$dateyess364." and ".$date1." ";
											$reviewcond=" and  a.rtimestamp between ".$dateyess364." and ".$date1." ";
											$reviewcond1=" and  rtimestamp between ".$dateyess364." and ".$date1." ";
											$data90 = $database->query("select id from createquestion where estatus='1' and vstatus1=1  and subject IN (".$row['subject'].") ".$sub."  and vtimestamp1 between ".$dateyess90." and ".$date1." ".$chaptercon2."  and vusername1='".$session->username."' "); 
											$todayt90count= mysqli_num_rows($data90);

											$data180 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].") ".$sub."  and vtimestamp1 between ".$dateyess180." and ".$date1." ".$chaptercon2." and vusername1='".$session->username."'"); 
											$todayt180count= mysqli_num_rows($data180);

											$data364 = $database->query("select id from createquestion where estatus='1' and vstatus1=1 and subject IN (".$row['subject'].") ".$sub."  and vtimestamp1 between ".$dateyess364." and ".$date1." ".$chaptercon2." and vusername1='".$session->username."'"); 
											$todayt364count= mysqli_num_rows($data364);
										?>
										 <th class="padding-0 bg-light" colspan="1">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=90days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" >
												<h5 class="mb-0">Last 90 days Verified</h5>
												<h3 class="mb-0"><?php  if($todayt90count!=''){ echo $todayt90count; } else{ echo '0'; } ?></h3>
											</a>
										</th>
									
										<th  rowspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=180days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" >
											<h5>Last 180 days Verified</h5>
											<h3><?php  if($todayt180count!=''){ echo $todayt180count; } else{ echo '0'; } ?></h3></a>
										</th>
										<th class="bg-light" rowspan="2" colspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=365days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 365 days Verified</h5>
											<h3><?php  if($todayt364count!=''){ echo $todayt364count; } else{ echo '0'; } ?></h3></a>
										</th>
									<?php
										}else if($_REQUEST['type']=='Today'){
											$typ="today";
											$condd= " and timestamp between ".$date." and ".$date1." ";
											$reviewcond=" and  a.rtimestamp between ".$date." and ".$date1." ";
											$reviewcond1=" and  rtimestamp between ".$date." and ".$date1." ";
										?>
										 <th class="padding-0 bg-light" colspan="1">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Today Verified</h5>
											<h3 class="mb-0"><?php  if($count3!=''){ echo $count3; } else{ echo '0'; } ?></h3></a>
										</th>
									
										<th  rowspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 3 days Verified</h5>
											<h3><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
										</th>
										<th rowspan="2"  class="bg-light" colspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 days Verified</h5>
											<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
										</th>
									<?php
										}else if($_REQUEST['type']=='Last 7 Days'){
											$typ="7days";
											$condd= " and timestamp between ".$date5." and ".$date1." ";
											$reviewcond=" and  a.rtimestamp between ".$date5." and ".$date1." ";
											$reviewcond1=" and  rtimestamp between ".$date5." and ".$date1." ";
											?>
											 <th class="padding-0 bg-light" colspan="1">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Today Verified</h5>
												<h3 class="mb-0"><?php  if($count3!=''){ echo $count3; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 3 Days Verified</h5>
												<h3><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  class="bg-light" rowspan="2" colspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 Days Verified</h5>
												<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
											</th>
										<?php
										}else if($_REQUEST['type']=='Last 15 Days'){
											$typ="15days";
											$condd= " and timestamp between ".$dateyess15." and ".$date1." ";
											$reviewcond=" and  a.rtimestamp between ".$dateyess15." and ".$date1." ";
											$reviewcond1=" and  rtimestamp between ".$dateyess15." and ".$date1." ";
											?>
											 <th class="padding-0 bg-light" colspan="1">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Last 3 Days Verified</h5>
												<h3 class="mb-0"><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
											</th>
											<th rowspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 Days Verified</h5>
												<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
											</th>
											<th class="bg-light"   rowspan="2" colspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=15days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 15 Days Verified</h5>
												<h3><?php  if($count15!=''){ echo $count15; } else{ echo '0'; } ?></h3></a>
											</th>
										<?php
										}else if($_REQUEST['type']=='Last 30 Days'){
												$typ="30days";
												$condd= " and timestamp between ".$dateyess30." and ".$date1." ";	
												$reviewcond=" and  a.rtimestamp between ".$dateyess30." and ".$date1." ";
												$reviewcond1=" and  rtimestamp between ".$dateyess30." and ".$date1." ";
											?>
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" >
											 <th class="padding-0 bg-light" colspan="1">
												<h5 class="mb-0">Last 7 Days Verified</h5>
												<h3 class="mb-0"><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=15days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 15 Days Verified</h5>
												<h3><?php  if($count15!=''){ echo $count15; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2" class="bg-light" colspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=30days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 30 Days Verified</h5>
												<h3><?php  if($count30!=''){ echo $count30; } else{ echo '0'; } ?></h3></a>
											</th>
										<?php
										}else if($_REQUEST['type']=='Last 90 Days'){
												$typ="90days";
												$condd= "  and timestamp between ".$dateyess90." and ".$date1." ";
												$reviewcond=" and  a.rtimestamp between ".$dateyess90." and ".$date1." ";
												$reviewcond1=" and  rtimestamp between ".$dateyess90." and ".$date1." ";
											?>
											 <th class="padding-0 bg-light" colspan="1">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=30days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Last 30 Days Verified</h5>
												<h3 class="mb-0"><?php  if($count30!=''){ echo $count30; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=60days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 60 Days Verified</h5>
												<h3><?php  if($count60!=''){ echo $count60; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  class="bg-light" rowspan="2" colspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=90days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 90 Days Verified</h5>
												<h3><?php  if($count90!=''){ echo $count90; } else{ echo '0'; } ?></h3></a>
											</th>
										<?php
										}else if($_REQUEST['type']=='Last 180 Days'){
											$typ="180days";
											$condd= "  and timestamp between ".$dateyess180." and ".$date1." ";
											$reviewcond=" and  a.rtimestamp between ".$dateyess180." and ".$date1." ";
											$reviewcond1=" and  rtimestamp between ".$dateyess180." and ".$date1." ";
											?>
											 <th class="padding-0 bg-light" colspan="1">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=60days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Last 60 Days Verified</h5>
												<h3 class="mb-0"><?php  if($count60!=''){ echo $count60; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=120days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 120 Days Verified</h5>
												<h3><?php  if($count120!=''){ echo $count120; } else{ echo '0'; } ?></h3></a>
											</th>
											<th  rowspan="2" class="bg-light" colspan="2">
												<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=180days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 180 Days Verified</h5>
												<h3><?php  if($count180!=''){ echo $count180; } else{ echo '0'; } ?></h3></a>
											</th>
										<?php
										}else if($_REQUEST['type']=='Custom Range'){
											
											//$typ="180days";
											if($_REQUEST['reportrange']!=''){
												$datefr= explode(" - ",$_REQUEST['reportrange']);
												$datefr1 = strtotime($datefr[0]);
												$datefr2 = strtotime($datefr[1]. ' 23:59:59'); 
												$condd= " and timestamp between ".$datefr1." and ".$datefr2." ";
												$reviewcond=" and  a.rtimestamp between ".$datefr1." and ".$datefr2." ";
												$reviewcond1=" and  rtimestamp between ".$datefr1." and ".$datefr2." ";
												$cudate=explode("-",$_REQUEST['reportrange']);
												$con= " and username='".$_SESSION['username']."'  and subject IN (".$row['subject'].") ".$sub." and timestamp between ".strtotime($cudate[0])." and ".strtotime($cudate[1])." ";
											?>
												 <th class="padding-0 bg-light" colspan="1">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Today Verified</h5>
													<h3 class="mb-0"><?php  if($count3!=''){ echo $count3; } else{ echo '0'; } ?></h3></a>
												</th>
											
												<th  rowspan="2">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 3 days Verified</h5>
													<h3><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
												</th>
												<th class="bg-light" rowspan="2" colspan="2">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 days Verified</h5>
													<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
												</th>
										<?php
											}
										}else{
											$typ="";
											$condd= "";
											$reviewcond="";
											?>
												 <th class="padding-0 bg-light" colspan="1">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Today Verified</h5>
													<h3 class="mb-0"><?php  if($count3!=''){ echo $count3; } else{ echo '0'; } ?></h3></a>
												</th>
											
												<th rowspan="2">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 3 days Verified</h5>
													<h3><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
												</th>
												<th class="bg-light" rowspan="2" colspan="2">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 days Verified</h5>
													<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
												</th>
										<?php
										}
									}else{ 
										$typ="";
										?>
										  <th class="padding-0 bg-light" colspan="1">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5 class="mb-0">Today Verified</h5>
											<h3 class="mb-0"><?php  if($count3!=''){ echo $count3; } else{ echo '0'; } ?></h3></a>
										</th>
									
										<th rowspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=3days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 3 days Verified</h5>
											<h3><?php  if($count4!=''){ echo $count4; } else{ echo '0'; } ?></h3></a>
										</th>
										<th class="bg-light" rowspan="2" colspan="2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&date=7days&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1')" ><h5>Last 7 days Verified</h5>
											<h3><?php  if($count5!=''){ echo $count5; } else{ echo '0'; } ?></h3></a>
										</th>
									<?php
									}
									$rowee=$database->query("select * from subject where estatus='1' and id in (".$row['subject'].") order by id asc limit 0,1");
									$rowee1=mysqli_fetch_array($rowee);
									$data6 = $database->query("select id from createquestion where estatus='1' and subject IN (".$row['subject'].") and chapter in (".$row['chapter'].") and vstatus1=2  ".$chaptercon2." and vusername1='".$session->username."' ".$condd.""); 
									$count6 = mysqli_num_rows($data6);
									?> 
                                    <th rowspan='2' colspan="2" class="dashboardsub p-0">
										 <div class="sub-select bg-gray-color">
                                                <select class="form-control" id="sub" name="sub" onChange="setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1&getsubjectselection=1&sub='+$('#sub').val()+'');">
													<option value=''>--Select--</option>
													<?php
													$sellosub=$database->query("select * from subject where estatus='1' and id in (".rtrim($row['subject'],",").") order by id ASC ");
													while($rowllo=mysqli_fetch_array($sellosub)){
														?>
														<option value="<?php echo $rowllo['id'];?>" <?php if(isset($_REQUEST['sub'])){ if($_REQUEST['sub'] == $rowllo['id']){ echo 'selected';}else{ '';}}else if($rowllo['id']==$rowee1['id']){ echo 'selected'; }?>
															 ?><?php echo $rowllo['subject'];?></option>
														<?php
													}
													?>

												</select>
                                            </div>
										<div rowspan='2'  class="p-2">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=2')" > <h5>Total Rejected</h5>
											<h3><?php  if($count6!=''){ echo $count6; } else{ echo '0'; } ?></h3></a>
										</div>
                                    </th>
                                </tr>
								<?php
												
									$rsel=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." and vusername1='".$session->username."' ".$reviewcond1." ");
									$row_rsel=mysqli_fetch_array($rsel);
									
									$rselp=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='0' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." and vusername1='".$session->username."'  ".$reviewcond1." ");
									$row_rselp=mysqli_fetch_array($rselp);
									$wc=0;
									$woutc=0;
									$timing=0;
									$qtype=0;
									$comple=0;
									$qtheory=0;
									$usage=0;
									$class=0;
									$exam=0;
									$chapter=0;
									$topic=0;
									$rselrc=$database->query("select b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1' and review_status='1'   and b.estatus='1' and a.id=b.question_id  and a.subject IN (".$row['subject'].")  and  a.vusername1='".$session->username."'  and b.message='1' ".$reviewcond."  ".$chaptercon22." group by a.id");
									$rowcn=mysqli_num_rows($rselrc);
									if($rowcn>0) {
										while($row_rselrc=mysqli_fetch_array($rselrc)){
											if($row_rselrc['changes_count']!=0){
												$wc++;
											}
											if($row_rselrc['changes_count']==0){
												$woutc++;
											}


											if($row_rselrc['changes']!='No Changes'){
												$changes=$row_rselrc['changes'];
												$data=explode("^",$changes);
												$a=1;
												
												if(count($data)>0){
													foreach($data as $dataa){
														$data1=explode("_",$dataa);
														if($data1[0]!=''){
															$new='';
															$old='';
															
															if($data1[0]=='complexity'){
																
																$comple++;
															}
															if($data1[0]=='timeduration'){
																
																$timing++;
																
															}
															if($data1[0]=='usageset'){
																
																$usage++;
															}
															if($data1[0]=='questiontheory'){
																
																$qtheory++;
															}
															if($data1[0]=='inputquestion'){
																
																$qtype++;
															}
															if($data1[0]=='class'){
																
																$class++;
															}
															if($data1[0]=='exam'){
																
																$exam++;
															}
															if($data1[0]=='chapter'){
																
																$chapter++;
															}
															if($data1[0]=='topic'){
																
																$topic++;
															}
															
															
															
															?>
																
															<?php
															
														
														$a++;
														}
													}
												}
											}
												
											
										}
														
									}
									?>
								 <tr>
                                         <th class="padding-0">
                                             <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>')" > <h5 class="mb-0">Total Reviewed</h5>
											<h3 class="mb-0"><?php  if($row_rsel['cnt']!=''){ echo $row_rsel['cnt']; } else{ echo '0'; } ?></h3></a>
                                        </th>
                                        <th class="padding-0">
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=0&date=<?php echo $typ; ?>')" > <h5 class="mb-0">Review Pending</h5>
											<h3 class="mb-0"><?php  if($row_rselp['cnt']!=''){ echo $row_rselp['cnt']; } else{ echo '0'; } ?></h3></a>
                                        </th>
                                        <th class="padding-0">
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&qwc=1&date=<?php echo $typ; ?>')" > <h5 class="mb-0">Reviewed with Corrections</h5>
											<h3 class="mb-0"><?php  if($wc!=''){ echo $wc; } else{ echo '0'; } ?></h3></a>
                                        </th>
                                        <th class="padding-0">
                                           <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&qwc=0&date=<?php echo $typ; ?>')" > <h5 class="mb-0">Reviewed with out Corrections</h5>
											<h3 class="mb-0"><?php  if($woutc!=''){ echo $woutc; } else{ echo '0'; } ?></h3></a>
                                        </th>
                                    </tr>
									<tr>
                                        <th>
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=timeduration')" > <h5>Timing Issue</h5>
											<h3><?php echo $timing; ?></h3></a>
                                        </th>
                                        <th>
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=inputquestion')" ><h5>QuestionType Issue</h5>
											<h3><?php echo $qtype; ?></h3></a>
                                        </th>
                                        <th>
                                            <h5>Complexity Issue</h5>
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=complexity')" ><h3><?php echo $comple; ?></h3></a>
                                        </th>
                                        <th>
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=questiontheory')" ><h5>Question Theory Issue</h5>
											<h3><?php echo $qtheory; ?></h3></a>
                                        </th>
                                        <th>
                                             <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=usageset')" ><h5>Usageset Issue</h5>
											<h3><?php echo $usage; ?></h3></a>
                                        </th>
                                        <th>
                                           <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=class')" > <h5>Class Issue</h5>
											<h3><?php echo $class; ?></h3></a>
                                        </th>
                                        <th>
                                           <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=exam')" > <h5>Exam Issue</h5>
											<h3><?php echo $exam; ?></h3></a>
                                        </th>
                                        <th>
                                             <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=chapter')" ><h5>Chapter Issue</h5>
											<h3><?php echo $chapter; ?></h3></a>
                                        </th>
										<th>
                                            <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=1&rstatus=1&date=<?php echo $typ; ?>&r_issue=topic')" > <h5>Topic Issue</h5>
											<h3><?php echo $topic; ?></h3></a>
                                        </th>
                                    </tr>
                            </thead>
							
							 <tbody class="body-bordered-table">
                               
                                    <?php
									
									

									$data7 = $database->query("select id from createquestion where estatus='1' and exam LIKE '%1%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." ".$condd." "); 
									$count7= mysqli_num_rows($data7);

									$data8 = $database->query("select id from createquestion where estatus='1' and  exam LIKE '%1%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." and class LIKE '%1%' ".$condd.""); 
									//echo "select * from createquestion where estatus='1' and  exam LIKE '%1%' and subject IN (".$row['subject'].") and chapter in (".$row['chapter'].") and class LIKE '%1% ".$condd."";
									
									$count8= mysqli_num_rows($data8);
									$data9 = $database->query("select id from createquestion where estatus='1' and exam LIKE '%1%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2."  and class LIKE '%2%' ".$condd.""); 
									
									$count9= mysqli_num_rows($data9);
									$data10 = $database->query("select id from createquestion where estatus='1' and exam LIKE '%2%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." ".$condd." ");
									$count10= mysqli_num_rows($data10);

									$data11 = $database->query("select id from createquestion where estatus='1' and exam LIKE '%2%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." and class LIKE '%1%' ".$condd." ");

									$count11= mysqli_num_rows($data11);
									$data12 = $database->query("select * from createquestion where estatus='1' and exam LIKE '%2%' and subject IN (".$row['subject'].") ".$sub." ".$chaptercon2." and class LIKE '%2%' ".$condd." ");
									$count12= mysqli_num_rows($data12);
									if($_REQUEST['sub']!=''){
										$row1['subject']=$_REQUEST['sub'];
									}else{
										$row1['subject']=$row['subject'];
									}
									if($row1['subject']=='1' ||  $row1['subject']=='5'){ 
										?>
										 <tr class="row-table">
                                        <th class="lecture-desc" colspan="7">
                                            <div class="bg-gray-color p-2">
                                                <div class="d-flex justify-content-between">
                                                   <h6 class="text-dark">NEET</h6>
                                                    <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h4><?php  if($count7!=''){ echo $count7; } else{ echo '0'; } ?></h4></a>
                                                </div>
                                                <div class="d-flex justify-content-between">
                                                    <table class="table table-bordered mr-2 mb-0 bg-white">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                    <div
                                                                        class="d-flex align-items-center justify-content-between">
																		
                                                                        <div>
                                                                            <h6>Class XI</h6>
                                                                            <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                        </div>
                                                                        <h3><?php  if($count8!=''){ echo $count8; } else{ echo '0'; } ?></h3>
                                                                    </div></a>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                    <table class="table table-bordered mb-0 bg-white">
                                                        <thead>
                                                            <tr>
                                                                <th colspan="4"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                    <div
                                                                        class="d-flex align-items-center justify-content-between">
                                                                        <div>
                                                                            <h6>Class XII</h6>
                                                                            <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                        </div>
                                                                        <h3><?php  if($count9!=''){ echo $count9; } else{ echo '0'; } ?></h3>
                                                                    </div></a>
                                                                </th>
                                                            </tr>
                                                        </thead>
                                                    </table>
                                                </div>
                                            </div>
                                        </th>

                                       
										<?php
									$ij=1;
									$ids="";
									$isql=$database->query("select a.question_id from question_issues As a inner join createquestion as b on a.estatus='1' and a.vstatus1=2 and b.estatus='1' and subject='".$row['subject']."' and resolve_issue='0' and a.question_id=b.id group by question_id ");
									
									$rowiql=mysqli_num_rows($isql);
									while($rowiql1=mysqli_fetch_array($isql)){
										$ids .=$rowiql1['question_id'].",";
										$ij++;
									}
									$ids1=rtrim($ids,",");
									?>
                                    <th class="last-child p-0 " colspan="3">
										 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&isstype=i&issueids=<?php echo $ids1; ?>&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h5 class="px-2 mb-0">Issue Questions</h5>
                                        <h3 class="px-2"><?php  if($rowiql!=''){ echo $rowiql; } else{ echo '0'; } ?></h3></a>
										<hr class="m-0 p-0"/>
                                       <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&status=2&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h5 class="px-2  mb-0">Today Rejected</h5>
                                        <h3 class="px-2"><?php  if($count13!=''){ echo $count13; } else{ echo '0'; } ?></h3></a>
                                    </th>
                                    </tr>
									
										<?php 
										} else if($row1['subject']=='2' ||  $row1['subject']=='3'){ ?>
									 <tr class="row-table">
                                    <th class="lecture-desc" colspan="3">
                                        <div class="bg-gray-color p-2">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="text-dark">NEET</h6>
                                               <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h4><?php  if($count7!=''){ echo $count7; } else{ echo '0'; } ?> </h4></a>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <table class="table table-bordered mr-2 mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&date=<?php echo $typ; ?>&exam=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XI</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count8!=''){ echo $count8; } else{ echo '0'; } ?> </h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-bordered mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&date=<?php echo $typ; ?>&exam=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XII</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count9!=''){ echo $count9; } else{ echo '0'; } ?> </h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </th>
                                    <th class="lecture-desc" colspan="4">
                                        <div class="p-2">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="text-dark">JEE</h6>
                                                <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=2&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" ><h4><?php  if($count10!=''){ echo $count10; } else{ echo '0'; } ?> </h4></a>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <table class="table table-bordered mr-2 mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&exam=2&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XI</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count11!=''){ echo $count11; } else{ echo '0'; } ?> </h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-bordered mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&date=<?php echo $typ; ?>&exam=2&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XII</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count12!=''){ echo $count12; } else{ echo '0'; } ?></h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </th>
                                    
									<?php
									$ij=1;
									$ids="";
									$isql=$database->query("select a.question_id from question_issues As a inner join createquestion as b on a.estatus='1' and a.vstatus1='2' and b.estatus='1' and subject IN (".$row['subject'].") ".$sub." and resolve_issue='0' and a.question_id=b.id group by question_id ");
									
									$rowiql=mysqli_num_rows($isql);
									while($rowiql1=mysqli_fetch_array($isql)){
										$ids .=$rowiql1['question_id'].",";
										$ij++;
									}
									$ids1=rtrim($ids,",");
									?>
                                    <th class="last-child p-0"  colspan="3">
										 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&isstype=i&issueids=<?php echo $ids1; ?>&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h5 class="px-2  mb-0">Issue Questions</h5>
                                        <h3 class="px-2"><?php  if($rowiql!=''){ echo $rowiql; } else{ echo '0'; } ?></h3></a>
										<hr class="m-0 p-0"/>
                                       <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&status=2&date=today&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>')" > <h5 class="px-2  mb-0">Today Rejected</h5>
                                        <h3 class="px-2"><?php  if($count13!=''){ echo $count13; } else{ echo '0'; } ?></h3></a>
                                    </th>
                                </tr>
									<?php } else if($row1['subject']=='4'){ ?> 
                                         <tr class="row-table">
                                    <th class="lecture-desc" colspan="7">
                                        <div class="bg-gray-color p-2">
                                            <div class="d-flex justify-content-between">
                                                <h6 class="text-dark">JEE</h6>
                                               <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=2&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h4><?php  if($count10!=''){ echo $count10; } else{ echo '0'; } ?></h4></a>
                                            </div>
                                            <div class="d-flex justify-content-between">
                                                <table class="table table-bordered mr-2 mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&date=<?php echo $typ; ?>&exam=2&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XI</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count11!=''){ echo $count11; } else{ echo '0'; } ?></h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                                <table class="table table-bordered mb-0 bg-white">
                                                    <thead>
                                                        <tr>
                                                            <th colspan="4"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&date=<?php echo $typ; ?>&exam=2&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
                                                                <div
                                                                    class="d-flex align-items-center justify-content-between">
                                                                    <div>
                                                                        <h6>Class XII</h6>
                                                                        <h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
                                                                    </div>
                                                                    <h3><?php  if($count12!=''){ echo $count12; } else{ echo '0'; } ?></h3>
                                                                </div></a>
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                        </div>
                                    </th>
									<?php
									$ij=1;
									$ids="";
									$isql=$database->query("select a.question_id from question_issues As a inner join createquestion as b on a.estatus='1' and a.vstatus1='2' and b.estatus='1' and resolve_issue='0' and subject IN (".$row['subject'].") ".$sub." and a.question_id=b.id group by question_id ");
									$rowiql=mysqli_num_rows($isql);
									while($rowiql1=mysqli_fetch_array($isql)){
										$ids .=$rowiql1['question_id'].",";
										$ij++;
									}
									$ids1=rtrim($ids,",");
									?>
                                    <th class="last-child p-0"  colspan="3">
										 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&isstype=i&issueids=<?php echo $ids1; ?>&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h5 class="px-2  mb-0">Issue Questions</h5>
                                        <h3 class="px-2"><?php  if($rowiql!=''){ echo $rowiql; } else{ echo '0'; } ?></h3></a>
										<hr class="m-0 p-0"/>
                                       <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&status=2&date=today&subject=<?php echo $row['subject'];  ?>')" > <h5 class="px-2  mb-0">Today Rejected</h5>
                                        <h3 class="px-2"><?php  if($count13!=''){ echo $count13; } else{ echo '0'; } ?></h3></a>
                                    </th>
									
									
                                </tr>

									<?php }else{ ?>
										<tr class="row-table">
											<th class="lecture-desc" colspan="3">
												<div class="bg-gray-color p-2">
													<div class="d-flex justify-content-between">
														<h6 class="text-dark">NEET</h6>
													   <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h4><?php  if($count7!=''){ echo $count7; } else{ echo '0'; } ?></h4></a>
													</div>
													<div class="d-flex justify-content-between">
														<table class="table table-bordered mr-2 mb-0 bg-white">
															<thead>
																<tr>
																	<th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&exam=1&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&status=')" >
																		<div
																			class="d-flex align-items-center justify-content-between">
																			<div>
																				<h6>Class XI</h6>
																				<h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
																			</div>
																			<h3><?php  if($count8!=''){ echo $count8; } else{ echo '0'; } ?></h3>
																		</div></a>
																	</th>
																</tr>
															</thead>
														</table>
														<table class="table table-bordered mb-0 bg-white">
															<thead>
																<tr>
																	<th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&date=<?php echo $typ; ?>&exam=1&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
																		<div
																			class="d-flex align-items-center justify-content-between">
																			<div>
																				<h6>Class XII</h6>
																				<h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
																			</div>
																			<h3><?php  if($count9!=''){ echo $count9; } else{ echo '0'; } ?></h3>
																		</div></a>
																	</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</th>
											<th class="lecture-desc" colspan="4">
												<div class="p-2">
													<div class="d-flex justify-content-between">
														<h6 class="text-dark">JEE</h6>
														<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&exam=2&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" ><h4><?php echo $count10; ?> </h4></a>
													</div>
													<div class="d-flex justify-content-between">
														<table class="table table-bordered mr-2 mb-0 bg-white">
															<thead>
																<tr>
																	<th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=1&date=<?php echo $typ; ?>&exam=2&subject=<?php echo $row['subject'];  ?>&status=')" >
																		<div
																			class="d-flex align-items-center justify-content-between">
																			<div>
																				<h6>Class XI</h6>
																				<h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
																			</div>
																			<h3><?php  if($count11!=''){ echo $count11; } else{ echo '0'; } ?></h3>
																		</div></a>
																	</th>
																</tr>
															</thead>
														</table>
														<table class="table table-bordered mb-0 bg-white">
															<thead>
																<tr>
																	<th colspan="2"><a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&class=2&date=<?php echo $typ; ?>&exam=2&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" >
																		<div
																			class="d-flex align-items-center justify-content-between">
																			<div>
																				<h6>Class XII</h6>
																				<h5 class="m-0"><b><?php echo $database->get_name('subject','id',$row1['subject'],'subject'); ?></b></h5>
																			</div>
																			<h3><?php  if($count12!=''){ echo $count12; } else{ echo '0'; } ?></h3>
																		</div></a>
																	</th>
																</tr>
															</thead>
														</table>
													</div>
												</div>
											</th>
											
											<?php
											$ij=1;
											$ids="";
											$isql=$database->query("select a.question_id from question_issues As a inner join createquestion as b on a.estatus='1' and a.vstatus1='2' and b.estatus='1' and resolve_issue='0' and subject IN (".$row['subject'].") ".$sub." and a.question_id=b.id group by question_id ");
											$rowiql=mysqli_num_rows($isql);
											while($rowiql1=mysqli_fetch_array($isql)){
												$ids .=$rowiql1['question_id'].",";
												$ij++;
											}
											$ids1=rtrim($ids,",");
											?>
											<th class="last-child p-0" colspan="3">
												 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&isstype=i&issueids=<?php echo $ids1; ?>&date=<?php echo $typ; ?>&subject=<?php echo $row['subject'];  ?>&sub=<?php echo $_REQUEST['sub']; ?>&status=')" > <h5 class="px-2  mb-0">Issue Questions</h5>
												<h3 class="px-2"><?php  if($rowiql!=''){ echo $rowiql; } else{ echo '0'; } ?></h3></a>
												<hr class="m-0 p-0"/>
											   <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&status=2&date=today&subject=<?php echo $row['subject'];  ?>&status=')" > <h5 class="px-2  mb-0">Today Rejected</h5>
												<h3 class="px-2"><?php  if($count13!=''){ echo $count13; } else{ echo '0'; } ?></h3></a>
											</th>
										</tr>
									<?php } ?>
                            </tbody>
                        </table>
                    </div>
                    </div>
					<div class="accordion" id="accordionExample1">
                        <div class="card mb-3 lecture-table-content">
                            <div class="card-header" id="headingOne">
                                <a href="" class="d-flex justify-content-between align-items-center"
                                    data-toggle="collapse" data-target="#collapseOne" aria-expanded="true"
                                    aria-controls="collapseOne">
                                    <h5 class="card-title mb-0"><b>Subject ,Chapter & Topic wise Overview</b></h5>
                                    <h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
                                </a>
                            </div>
							
                            <div class="row m-0">
                                <div class="col-md-6">
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                        data-parent="#accordionExample1">
                                        <div class="py-2">
                                            <div class="accordion" id="accordionExample2">
                                                <div class="card border-0">
                                                    <div id="headingOneInner">
                                                        <a href=""
                                                            class="d-flex justify-content-between align-items-center bg-gray-color p-3"
                                                            data-toggle="collapse" data-target="#collapseOneInner"
                                                            aria-expanded="true" aria-controls="collapseOneInner">
                                                            <h5>Class XI <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $database->get_name('subject','id',rtrim($row1['subject'],","),'subject'); ?></span></h5>
															 <?php 
															
															$dataf=$database->query("SELECT sum(DISTINCT(syllabus_totals_new.chapter_total)) as total FROM `chapter`,syllabus_totals_new WHERE chapter.id  in (".$row['chapter'].")  and chapter.id = syllabus_totals_new.chapter AND chapter.class = 1 AND chapter.estatus = 1 and syllabus_totals_new.subject='".$sub1."' order by syllabus_totals_new.chapter ASC ");
															
															$rowdataf=mysqli_fetch_array($dataf);
															$countf=$rowdataf['total'];
															?>
															 <h4><b><?php  if($countf!=''){ echo $countf; } else{ echo '0'; } ?></b></h4>
                                                        </a>
                                                    </div>

                                                    <div id="collapseOneInner" class="collapse"
                                                        aria-labelledby="headingOneInner"
                                                        data-parent="#accordionExample2">

                                                        <div class="accordion" id="accordionExample3">
                                                            <div class="card border-0">
                                                                <div id="headingOneInternal">
																	
                                                                    <a href=""
                                                                        class="d-flex justify-content-between align-items-center"
                                                                        data-toggle="collapse"
                                                                        data-target="#collapseOneInternal"
                                                                        aria-expanded="true"
                                                                        aria-controls="collapseOneInternal">
                                                                        <table
                                                                            class="table table-bordered mb-0 bg-white">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td>
                                                                                        <h6><b>Chapters & Topics</b>
                                                                                        </h6>
                                                                                    </td>
                                                                                    <td>
                                                                                        <h6><b>Questions</b></h6>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </a>
                                                                </div>

                                                                <div id=" "  
                                                                    aria-labelledby="headingOneInternal"
                                                                    data-parent="#accordionExample3">

                                                                    <div class="accordion" id="accordionExamplexi">
																		<?php  
                    													$dataw= $database->query("select id,subject,chapter from chapter where estatus='1' and subject IN (".$row['subject'].") ".$sub."  and id in (".$row['chapter'].") and class=1");
                    													$i=1;
                    													while($rowsel = mysqli_fetch_array($dataw)) { 
																			?>
                                                                            <div class="card">
                                                                              <div id="headingOnexi<?php echo $i; ?>">
                                                                                <h2 class="mb-0">
                                                                                  
																				  <a href="" data-toggle="collapse" data-target="#collapseOnexi<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOnexi<?php echo $i; ?>">
                                                                                        <table
                                                                                        class="table table-bordered mr-2 mb-0 bg-white">
                                                                                        <tbody>
                                                                                            <tr>
                                                                                                <th class="bg-gray-color"
                                                                                                    colspan="2">
                                                                                                    <div
                                                                                                        class="d-flex justify-content-between">
                                                                                                        <h6><b><?php echo $rowsel['chapter']; ?></b>
                                                                                                        </h6>
																										<?php
																										$dataf=$database->query("SELECT syllabus_totals_new.chapter_total as total FROM `chapter`,syllabus_totals_new WHERE chapter.id  in (".$rowsel['id'].")  and chapter.id = syllabus_totals_new.chapter AND chapter.class = 1 AND chapter.estatus = 1 and syllabus_totals_new.subject='".$rowsel['subject']."'  order by syllabus_totals_new.chapter ASC ");
															
																										$rowdataf=mysqli_fetch_array($dataf);
																										$countt=$rowdataf['total'];
																									   ?>
                                                                                                        <h6><b><?php  if($countt!=''){ echo $countt; } else{ echo '0'; } ?></b></p>
                                                                                                    </div>
                                                                                                </th>
                                                                                            </tr>
                                                                                            </tbody>
                                                                                            </table>
                                                                                        </a>
                                                                                </h2>
                                                                              </div>
                                                                          
                                                                              <div id="collapseOnexi<?php echo $i; ?>" class="collapse" aria-labelledby="headingOnexi<?php echo $i; ?>" data-parent="#accordionExamplexi">
                                                                                <table
                                                                                        class="table table-bordered mr-2 mb-0 bg-white">
                                                                                        <tbody>
																						<?php $datai = $database->query("select id,class,subject,chapter,topic from topic where estatus='1' and subject='".$rowsel['subject']."' and chapter='".$rowsel['id']."' and class=1");
																				  
																					   while($sel= mysqli_fetch_array($datai)){
					
																					?>
                                                                                                <tr>
                                                                                                        <td>
                                                                                                            <p><?php echo $sel['topic']; ?> </p>
                                                                                                        </td>
																										<?php 
																										
																										$datat=$database->query("SELECT sum(total) as total from syllabus_totals_new where subject='".$rowsel['subject']."' and chapter='".$rowsel['id']."' and topic='".$sel['id']."' ");
																										$rowdataq=mysqli_fetch_array($datat);
																										 $countq=$rowdataq['total'];
																						   ?>
                                                                                                        <td>
                                                                                                            <p><?php  if($countq!=''){ echo $countq; } else{ echo '0'; } ?></p>
                                                                                                        </td>
                                                                                                    </tr>
																									<?php
																					   }
																						   ?>
																								</tbody>
																							</table>
                                                                              </div>
                                                                            </div>
																		<?php 
																					  
																				  $i++;
																				  } 
																			  ?>
                                                                            
                                                                           
                                                                          </div>
                                                                    
                                                                </div>
                                                            </div>

                                                        </div>


                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne"
                                        data-parent="#accordionExample1">
                                        <div class="py-2">
                                            <div class="accordion" id="accordionExample3">
                                                <div class="card">
                                                    <div id="headingTwoInner">
                                                        <a href=""
                                                        class="d-flex justify-content-between align-items-center bg-gray-color p-3"
                                                        data-toggle="collapse" data-target="#collapseTwoInner"
                                                        aria-expanded="true" aria-controls="collapseTwoInner">
                                                        <h5>Class XII <span>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $database->get_name('subject','id',rtrim($row1['subject'],","),'subject'); ?></span></h5>
														 <?php
														$datat=$database->query("SELECT sum(DISTINCT(syllabus_totals_new.chapter_total)) as total FROM `chapter`,syllabus_totals_new WHERE chapter.id  in (".$row['chapter'].")  and chapter.id = syllabus_totals_new.chapter AND chapter.class = 2  AND chapter.estatus = 1 and  syllabus_totals_new.subject='".$sub1."' order by syllabus_totals_new.chapter ASC ");
														$rowdatat=mysqli_fetch_array($datat);
														$countt=$rowdatat['total'];
														?>
                                                        <h4><b><?php  if($countt!=''){ echo $countt; } else{ echo '0'; } ?></b></h4>
                                                    </a>
                                                    </div>

                                                    <div id="collapseTwoInner" class="collapse"
                                                        aria-labelledby="headingTwoInner"
                                                        data-parent="#accordionExample3">
                                                        <div class="accordion" id="accordionExample4">
                                                                <div class="card border-0">
                                                                    <div id="headingTwoInternal">
                                                                        <a href=""
                                                                            class="d-flex justify-content-between align-items-center"
                                                                            data-toggle="collapse"
                                                                            data-target="#collapseTwoInternal"
                                                                            aria-expanded="true"
                                                                            aria-controls="collapseTwoInternal">
                                                                            <table
                                                                                class="table table-bordered mb-0 bg-white">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h6><b>Chapters & Topics</b>
                                                                                            </h6>
                                                                                        </td>
                                                                                        <td>
                                                                                            <h6><b>Questions</b></h6>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </a>
                                                                    </div>
    
                                                                    <div id=" " class=""
                                                                        aria-labelledby="headingTwoInternal"
                                                                        data-parent="#accordionExample4">



                                                                        <div class="accordion" id="accordionExamplexii">
																			<?php  
																		
                    													$dataw= $database->query("select id,subject,chapter from chapter where estatus='1' and subject IN (".rtrim($row['subject'],",").") ".$sub." and id in (".$row['chapter'].") and class=2");
                    													$j=1;
                    													while($rowsel = mysqli_fetch_array($dataw)) { ?>
                                                                                <div class="card">
                                                                                  <div  id="headingOnexii<?php echo $j; ?>">
                                                                                    <h2 class="mb-0">
                                                                                     
																					  <a data-toggle="collapse" data-target="#collapseOnexii<?php echo $j; ?>" aria-expanded="true" aria-controls="collapseOnexii<?php echo $j; ?>">
                                                                                            <table
                                                                                            class="table table-bordered mr-2 mb-0 bg-white">
                                                                                            <tbody>
                                                                                                <tr>
                                                                                                    <th class="bg-gray-color"
                                                                                                        colspan="2">
                                                                                                        <div
                                                                                                            class="d-flex justify-content-between">
                                                                                                            <h6><b><?php echo $rowsel['chapter']; ?></b>
                                                                                                            </h6>
																											<?php 
																											
																											$dataf=$database->query("SELECT syllabus_totals_new.chapter_total as total FROM `chapter`,syllabus_totals_new WHERE  chapter.estatus =1  and chapter.id  in (".$rowsel['id'].")  and chapter.id = syllabus_totals_new.chapter AND chapter.class =2 and syllabus_totals_new.subject='".$rowsel['subject']."'  order by syllabus_totals_new.chapter ASC ");
																											$rowdatat=mysqli_fetch_array($dataf);
																											$countt= $rowdatat['total'];
																											 ?>
                                                                                                            <h6><b><?php  if($countt!=''){ echo $countt; } else{ echo '0'; } ?></b></p>
                                                                                                        </div>
                                                                                                    </th>
                                                                                                </tr>
                                                                                                </tbody>
                                                                                                </table>
                                                                                      </a>
                                                                                    </h2>
                                                                                  </div>
                                                                              
                                                                                  <div id="collapseOnexii<?php echo $j; ?>" class="collapse " aria-labelledby="headingOnexii<?php echo $j; ?>" data-parent="#accordionExamplexii">
																						<table
                                                                                        class="table table-bordered mr-2 mb-0 bg-white">
                                                                                        <tbody>
																							<?php $datai = $database->query("select id,class,subject,topic from topic where estatus='1' and subject='".rtrim($row['subject'],",")."' and chapter='".$rowsel['id']."' and class=2");
																						   
																							   while($sel= mysqli_fetch_array($datai)){
																								   $topic1=$rowsel['class']."_1_".$rowsel['chapter']."_".$sel['id'];
																									$topic2=$rowsel['chapter']."_2_".$rowsel['chapter']."_".$sel['id'];
																									
																									$datat=$database->query("SELECT sum(total) as total  from syllabus_totals_new where subject='".$rowsel['subject']."' and chapter='".$rowsel['id']."' and topic='".$sel['id']."' ");
																									$rowdataq=mysqli_fetch_array($datat);
																									 $countq=$rowdataq['total'];
																							?>
                                                                                                <tr>
                                                                                                        <td>
                                                                                                            <p><?php echo $sel['topic']; ?> </p>
                                                                                                        </td>
                                                                                                        <td>
                                                                                                            <p><?php  if($countq!=''){ echo $countq; } else{ echo '0'; } ?></p>
                                                                                                        </td>
                                                                                                    </tr>
																								<?php
																							   }
																							?>
																								</tbody>
																							</table>
                                                                                  </div>
                                                                                </div>
																				<?php
																					  $j++;
																				}
																				  ?>
                                                                               
                                                                                
                                                                        
                                                                    </div>
                                                                </div>
    
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>
			</section>

		</div>
	<?php
	}
	?>
			<?php
			//}
		if(isset($_REQUEST['getedit'])) {
			if($_REQUEST['submitform'] == 2 && isset($_POST['editform'])){
				
				$data_sel = $database->query("SELECT * FROM createquestion WHERE estatus='1' and id = '".$_POST['editform']."'");
				if(mysqli_num_rows($data_sel) > 0){
				$data = mysqli_fetch_array($data_sel);
				$_POST = array_merge($_POST,$data);
				}
			}
			if(isset($_REQUEST['errordisplay'])){
				if($_REQUEST['qtype']=='7'){
					$_POST['question']=$_SESSION['question'];
					$_POST['option1']=$_SESSION['option1'];
					$_POST['option2']=$_SESSION['option2'];
					$_POST['option3']=$_SESSION['option3'];
					$_POST['option4']=$_SESSION['option4'];
					$_POST['explanation']=$_SESSION['explanation'];
				}else if($_REQUEST['qtype']=='9'){
					$_POST['question']=$_SESSION['question'];
					$_POST['explanation']=$_SESSION['explanation'];
					$_POST['mat_question']=$_SESSION['mat_question'];
					$_POST['mat_que_paragraph']=$_SESSION['mat_que_paragraph'];
					$_POST['mat_que_footer']=$_SESSION['mat_que_footer'];
					$_POST['data5']=$_SESSION['data5'];
					$_POST['option1']=urldecode($_POST['option1']);
					$_POST['option2']=urldecode($_POST['option2']);
					$_POST['option3']=urldecode($_POST['option3']);
					$_POST['option4']=urldecode($_POST['option4']);
					$_POST['option5']=urldecode($_POST['option5']);
				}else if($_REQUEST['qtype']=='3'){
					$_POST['question']=$_SESSION['question'];
					$_POST['explanation']=$_SESSION['explanation'];
					$_POST['mat_question']=$_SESSION['mat_question'];
					$_POST['mat_que_paragraph']=$_SESSION['mat_que_paragraph'];
					$_POST['mat_que_footer']=$_SESSION['mat_que_footer'];
					$_POST['data5']=$_SESSION['data5'];
					$_POST['option1']=urldecode($_POST['option1']);
					$_POST['option2']=urldecode($_POST['option2']);
					$_POST['option3']=urldecode($_POST['option3']);
					$_POST['option4']=urldecode($_POST['option4']);
					$_POST['option5']=urldecode($_POST['option5']);
				}else if($_REQUEST['qtype']=='8'){
					$_POST['question']=$_SESSION['question'];
					$_POST['explanation']=$_SESSION['explanation'];
				}else if($_REQUEST['qtype']=='5'){
					$_POST['compquestion']=$_SESSION['compquestion'];
					$_POST['data4']=$_SESSION['data4'];
				}else{
					$_POST['question']=$_SESSION['question'];
					$_POST['option1']=$_SESSION['option1'];
					$_POST['option2']=$_SESSION['option2'];
					$_POST['option3']=$_SESSION['option3'];
					$_POST['option4']=$_SESSION['option4'];
					$_POST['explanation']=$_SESSION['explanation'];
				}
			}
				$data_status = $database->query("SELECT vstatus1 FROM createquestion WHERE estatus='1' and id = '".$_POST['editform']."'");
				$row_status = mysqli_fetch_array($data_status);
				$_POST['vstatus1']=$row_status['vstatus1'];
				$data_seluser =  $database->query("select * from users where username='".$session->username."'");
				$rowuser = mysqli_fetch_array($data_seluser);
				//$data_sel = $database->query("SELECT * FROM createquestion WHERE id = '".$_POST['editform']."'");
				//if(mysqli_num_rows($data_sel) > 0){
				//$data = mysqli_fetch_array($data_sel);
				//$_POST = array_merge($_POST,$data);
			?>
			<?php 
			if(isset($_POST['class'])){ 
				if($_POST['class']=='1'){ 	
					$active="active";
					$dis="";
					$dis1="disabled";
					$active1="";
				}else if($_POST['class']=='2'){  
					$active="";
					$active1="active";
					$dis="disabled";
					$dis1="";
				}else{
					$active="active";
					$active1="";
					$dis="";
					$dis1="disabled";
				}
			}
			?>
				 <script>
			$('html, body').animate({ scrollTop: "0" }); 
			
		</script>
			<script type="text/javascript">
				$('#tabledata').hide();
				$('#datafill').hide();
				radiofunction('<?php echo $_POST['ppaper']; ?>');
		 </script>
		
			<div class="lecture-details">
                    <div class="card border-0 shadow-sm">
                        <div class="card-body" >
                            <form id="" >
                                <div class="row align-items-center" id="qscroll" style="display:none;">
                                    
									<div class="form-group col-md-2">
                                        <label class="slabel" for="inputExam">Class<span style="color:red;">*</span></label>
                                        
										<div class="btn-group block" role="group" aria-label="Basic example">
										
										  <a href='#'  class="btn btn-outline-secondary <?php echo $dis; ?>  class <?php echo $active; ?>" onclick="$('.class').val('1');"   >XI</a>
										  <a href='#' class="btn btn-outline-secondary <?php echo $dis1; ?> class <?php echo $active1; ?>"  onclick="$('.class').val('2');" <?php echo $dis1; ?>  >XII</a>
										  <input type="hidden" id="class" class="class"  value="<?php if(isset($_POST['class'])){ echo $_POST['class'] ;}else{ echo '1'; }?> "   />
										</div>
                                    </div>
									<?php 
										$_POST['exam3']=$_POST['exam']; 
										if(rtrim($_POST['exam3'],',')=='1'){
											$exam="NEET";
										}else if(rtrim($_POST['exam3'],',')=='2'){
											$exam="JEE";
										}else if(rtrim($_POST['exam3'],',')=='1,2'){
											$exam="NEET,JEE";
										}
										$_POST['subject3']=$_POST['subject'];
										$chapter3=$_POST['chapter'];
										$topic3=$_POST['topic'];
										$_POST['chapter3']=explode(",",$_POST['chapter']);
										$_POST['topic3']=explode(",",$_POST['topic']);
									?>
                                    <div class="form-row col-md-10">
										<div class="form-group col-md-2">
											<label for="inputExam">Exam<span style="color:red;">*</span></label>
											
											<input type="text" id="inputExam" class="form-control bg-light"
                                                placeholder="NEET" value="<?php if(isset($_POST['exam3'])){ echo $exam;}else{ echo '';} ?>" disabled>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam3'])){ echo $_SESSION['error']['exam3'];}?></span>
										</div>
										<div class="form-group col-md-3" >
										<label for="inputSub">Subjects</label>
										 <select class="form-control" name="subject3" value="" readonly  id="subject3"  disabled>
											<option value=''>-- Select --</option>
											<?php
											$row = $database->query("select * from subject where estatus='1'");
											while($data = mysqli_fetch_array($row))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject3'])) { if($_POST['subject3']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
											<?php
											}
											?>
										</select>
										
									</div>
									
                                    <div class="form-group col-md-3">
                                            <label for="inputTopi" class="label">Chapter</label>
											<select class="form-control selectpicker8" name="chapter3" value="" readonly multiple  id="chapter3"  >
											<option value=''>-- Select --</option>
											<?php
											$row = $database->query("SELECT * FROM `chapter` WHERE class IN  (".rtrim($_POST['class'],',').") and subject = '".$_POST['subject3']."' AND id IN(".rtrim($rowuser['chapter'],',').") AND id IN  (".$chapter3.")");
											while($data = mysqli_fetch_array($row))
											{
												?>
											<option value="<?php echo $data['id'];?>" disabled <?php if(isset($_POST['chapter3'])) { if(in_array($data['id'], $_POST['chapter3'])) { echo 'selected="selected"'; } } ?>><?php echo $data['chapter'];?></option>
											<?php
											}
											?>
										</select>
										</div>
										
										<div class="form-group col-md-3">
                                            <label for="" class="label">Topic</label>
											<select class="form-control selectpicker9" name="topic3" value="" multiple readonly id="topic3"  >
											<option value=''>-- Select --</option>
											<?php
											
											$zSql = $database->query("SELECT * FROM `topic` WHERE class IN  (".rtrim($_POST['class'],",").") and subject = '".$_POST['subject3']."' and chapter IN (".$chapter3.") and id IN (".$topic3.") "); 
											
											if(mysqli_num_rows($zSql)>0)
											{ 
												while($data = mysqli_fetch_array($zSql))
												{ 
													?> 
													<option value="<?php echo $data['id'];?>" disabled <?php if(isset($_POST['topic3'])) { if(in_array($data['id'], $_POST['topic3'])) { echo 'selected="selected"'; } } ?>><?php echo $data['topic'];?></option>
												<?php
												}
												
											}
											?>
										</select>
                                        </div>
                                    </div>
                                </div>
                            </form>
							
							 <div class="d-flex">
								<div class="pr-4">
									<div class="form-group d-flex align-items-center">
										<label class="col-form-label pr-2">Q.Id:</label>
										
										<?php echo $_POST['editform']; ?>
									</div>
								</div>
								<?php  
										
											if(isset($_POST['qtype'])){ 
											$qtype=$database->get_name('questiontype','id',$_POST['qtype'],'questiontype'); 
										}else{ 
											$qtype=$database->get_name('questiontype','id','7','questiontype');
										} 
										?>
								<div>
									<div class="form-group d-flex align-items-center">
										<label class="col-form-label pr-2">Question Type:</label>
										<?php  echo $qtype; ?>
											<input type="hidden"   class="qtype form-control" readonly id="qtype" name="qtype" value="<?php  if(isset($_POST['qtype'])){ echo $_POST['qtype']; }else{ echo "7"; } ?>"  />
										</div>
								</div>
							</div>
							<hr />
							
                            
							
							
								<!-- <div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Question Type<span style="color:red;">*</span></label>
											<div class="col-sm-10 mt-2">
												<?php
												$sel=$database->query("select * from questiontype where  estatus='1' and id in (7,5,8,3,9) order by orderid ASC");
												while($row=mysqli_fetch_array($sel)){
													?>
														<input   type="radio"  class="qtype"  id="<?php echo $row['questiontype']; ?>" onchange="$('.qtype').val('<?php echo $row['id']; ?>');setState('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&addForm=2&getquestiontype=1&qtype='+$('#qtype').val()+'&editform=<?php echo $_POST['editform'];?>');"  name="qtype" value="<?php echo $row['id']; ?>" <?php if(isset($_POST['qtype'])){ if($_POST['qtype'] == $row['id']){ echo 'checked="checked"';}else { echo 'disabled';}}else if($row['id']=='7'){ echo 'checked="checked"'; }else{ echo 'disabled';}?>  /> <label for="<?php echo $row['questiontype']; ?>"><?php echo $row['questiontype']; ?></label>
												<?php
												}
												?>
												<input type="hidden"   class="qtype"  id="qtype" name="qtype" value="<?php  if(isset($_POST['qtype'])){ echo $_POST['qtype']; }else{ echo "7"; } ?>"  />
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qtype'])){ echo $_SESSION['error']['qtype'];}?></span>
												
											</div>
										</div>
									</div>
								</div> -->
								<?php
							if(isset($_POST['qtype'])){
								if($_POST['qtype']=='5'){
									$count = 1;
									
									if($_REQUEST['submitform'] == 2){
										$comp=urldecode($database->get_name('compquestion','id',$_POST['compquestion'],'compquestion'));
										$question1=$_POST['question'];
										$option1=$_POST['option1'];
										$option2=$_POST['option2'];
										$option3=$_POST['option3'];
										$option4=$_POST['option4'];
										$explanation=$_POST['explanation'];
										$answer=$_POST['answer'];
										
										
									}else{
										$comp=urldecode($_POST['compquestion']);
										$posdata1 = $_POST['data4'];
										foreach($posdata1 as $rowpost)
										{
											$question1=$rowpost['question'];
											$option1=$rowpost['option1'];
											$option2=$rowpost['option2'];
											$option3=$rowpost['option3'];
											$option4=$rowpost['option4'];
											$explanation=$rowpost['explanation'];
											$answer=$rowpost['answer'];
										}
									}
								?>
									<div class="questionViewdata">
										<div class="row mt-2 align-items-center ">
											<div class="col-lg-12">
												<div class="d-flex justify-content-between align-items-center">
													<h6 class="text-danger" >Description</h6>
													<a class="btn  btn btn-outline-primary text-primary" onclick="getquestionview1();"><i class="fas fa-edit"></i> Edit</a>
												</div>
												<div><?php echo $comp; ?></div>
											</div>
										</div>
										<div class="row mt-4 align-items-center ">
											<div class="col-lg-12">
												<h6 class="text-primary">Question</h6>
												<div><?php echo urldecode($question1); ?></div>
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A) </div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($option1); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($option2); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($option3); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($option4); ?>
													</div>
												</div>
											</div>
											
										</div>
										<div class="mt-4">
											<h6 class="text-success">Correct Answer</h6>
											<p><?php echo rtrim($answer,","); ?></p>
												

										</div>
										<?php
										if($explanation!=''){
										?>
											<div class="mt-4">
												<h6 class="text-warning">Explanation</h6>
												<div><?php echo urldecode($explanation); ?></div>
													

											</div>
										<?php } ?>
										
									</div>
									

									<div style="display:none;" class="getquestionview" >
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Q.Explanation<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<div class="wrs_container">
															<div class="wrs_row">
																<div class="wrs_col wrs_s12">
																	<div id="editorContainer">
																		<div id="toolbarLocation"></div>
																			<textarea id="compquestion" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['compquestion'])) { echo $comp; }else{ } ?></textarea> 
																			
																	</div>
																</div>

															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row justify-content-end">
											
											<div class="col-lg-2">
												<?php
												if($count == 1){
												?>
													<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
												<?php
												}
												else{
												?>
													<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
													<a class="btn btn-outline-info px-3" onclick="removeList('<?php echo $count; ?>')">Remove Question</a>
												<?php
												}
												?>
												
											
											</div>
								  
										</div>
										<hr />
										<div id="dates_list">
										<?php
											if(isset($_POST['editform']))
											{
												$dataec = $database->query("select * from  createquestion where estatus='1' and id='".$_POST['id']."'");
												$rowe = mysqli_num_rows($dataec);
												if($rowe > 0)
												{
												while($rowec = mysqli_fetch_array($dataec))
												{
													
												?>
													<div class="list" id="list<?php echo $count;?>">
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['question'])) { echo urldecode($rowec['question']); }?></textarea> 
																						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													<div class="row">
														<div class="col-lg-12">
															<div class="form-group row">
																<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
																<div class="col-sm-10">
																	
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['option1'])) { echo urldecode($rowec['option1']); }?></textarea> 
																					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
														<div class="col-sm-10">
															
															<div class="wrs_container">
																<div class="wrs_row">
																	<div class="wrs_col wrs_s12">
																		<div id="editorContainer">
																			<div id="toolbarLocation"></div>
																			<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['option2'])) { echo urldecode($rowec['option2']); }?></textarea> 
																			<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span> 
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
														<div class="col-sm-10">
															
															<div class="wrs_container">
																<div class="wrs_row">
																	<div class="wrs_col wrs_s12">
																		 <div id="editorContainer">
																			<div id="toolbarLocation"></div>
																			<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['option3'])) { echo urldecode($rowec['option3']); }?></textarea> 
																			<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
														<div class="col-sm-10">
															
															<div class="wrs_container">
																<div class="wrs_row">
																	<div class="wrs_col wrs_s12">
																		 <div id="editorContainer">
																			<div id="toolbarLocation"></div>
																			<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['option4'])) { echo urldecode($rowec['option4']); }?></textarea> 
																			<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
																		</div>
																	</div>

																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="row correctanswer">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
														
														<div class="col-sm-8">
														<?php
														
														if(isset($rowec['answer'])){
															if($rowec['answer']!=''){
																$_POST['answer']=explode(",",$rowec['answer']);
															}
															
														}else{
															
														}
														?>
														
														

														<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer1<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
														<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer2<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
														<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer3<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;

														<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer4<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; }  } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

														<input type="hidden" name="answer<?php echo $count; ?>" class="tests<?php echo $count; ?> answer" id="correctanswer<?php echo $count; ?>" value="<?php echo $rowec['answer']; ?>" >
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'.$count])){ echo $_SESSION['error']['answer'.$count];}?></span>
														</div>
													</div>
												</div>
											</div>
											
											<div class="row">
												<div class="col-lg-12">
													<div class="form-group row">
														<label class="col-sm-2 col-form-label">Explanation</label>
														<div class="col-sm-10">
															<div class="col-sm-10">
																
																<div class="wrs_container">
																	<div class="wrs_row">
																		<div class="wrs_col wrs_s12">
																			 <div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowec['explanation'])) { echo urldecode($rowec['explanation']); }?></textarea> 
																				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
																			</div>
																		</div>

																	</div>
																</div>
															</div>
															
														</div>
													</div>
												</div>
											</div>
											
											<input type="hidden" name="comid" id="comid" class=" form-control comid" value="<?php if(isset($rowec['id'])){ echo $rowec['id'];}?>" >
										</div>
										
									<?php
									$count++;
									}
									?>
									<input type="hidden" id="session_list"  value="<?php echo  $count-1;?>"/>
									<?php
										}else{
										
											$posdata1 = $_POST['data4'];
											foreach($posdata1 as $rowpost)
											{
												
												?>
													<div class="list" id="list<?php echo $count;?>">
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="question" rows="3" id="question" class="form-control question "  ><?php if(isset($rowpost[0])) { echo $rowpost[0]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span> -->
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['question'])) { echo urldecode($rowpost['question']); }?></textarea> 
																						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">A)1<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option1" rows="2" id="option1" value="" class="form-control option1"  ><?php if(isset($rowpost[1])) { echo $rowpost[1]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span> -->
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option1'])) { echo urldecode($rowpost['option1']); }?></textarea> 
																						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option2" rows="2" id="option2" value="" class="form-control option2"  ><?php if(isset($rowpost[2])) { echo $rowpost[2]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span> -->
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option2'])) { echo urldecode($rowpost['option2']); }?></textarea> 
																					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option3" rows="2" id="option3" value="" class="form-control option3"  ><?php if(isset($rowpost[3])) { echo $rowpost[3]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span> -->
																		<div class="wrs_col wrs_s12">
																			 <div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option3'])) { echo urldecode($rowpost['option3']); }?></textarea> 
																				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option4" rows="2" id="option4" value="" class="form-control option4"  ><?php if(isset($rowpost[4])) { echo $rowpost[4]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span> -->
																		<div class="wrs_col wrs_s12">
																			 <div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option4'])) { echo urldecode($rowpost['option4']); }?></textarea> 
																				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row correctanswer">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
																	
																	<div class="col-sm-8">
																	<?php
																	
																	if(isset($rowpost['answer'])){
																		if($rowpost['answer']!=''){
																			$_POST['answer']=explode(",",$rowpost['answer']);
																		}
																		
																	}else{
																		
																	}
																	?>
																	
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer1<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer2<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer3<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;

																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer4<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; }  } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;


																	<input type="hidden" class="tests<?php echo $count; ?> answer " id="correctanswer<?php echo $count; ?>" value="<?php echo $rowpost['answer']; ?>" >
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'.$count])){ echo $_SESSION['error']['answer'.$count];}?></span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Explanation</label>
																	<div class="col-sm-10">
																		<div class="col-sm-10">
																			<!-- <textarea name="explanation" rows="3" id="explanation" value="" class="form-control explanation"  ><?php if(isset($rowpost[6])) { echo $rowpost[6]; }?></textarea>
																			<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span> -->
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['explanation'])) { echo urldecode($rowpost['explanation']); }?></textarea> 
																					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
																				</div>
																			</div>
																		</div>
																		
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												
													<input type="hidden" name="comid" id="comid" class="form-control comid" value="<?php echo $rowpost['comid']; ?>" >
												<?php
												$count++;
												
												
											}
										}
										?>
											<input type="hidden" id="session_list"  value="<?php echo  $count-2;?>"/>
										
										<?php
										
									}else if(isset($_REQUEST['data4']))
									{
										$posdata1 = $_POST['data4'];
										print_r($posdata1);
										foreach($posdata1 as $rowpost)
										{
											
												?>
													<div class="list" id="list<?php echo $count;?>">
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="question" rows="3" id="question" class="form-control question "  ><?php if(isset($rowpost[0])) { echo $rowpost[0]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span> -->
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['question'])) { echo urldecode($rowpost['question']); }?></textarea> 
																						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">A)1<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option1" rows="2" id="option1" value="" class="form-control option1"  ><?php if(isset($rowpost[1])) { echo $rowpost[1]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span> -->
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option1'])) { echo urldecode($rowpost['option1']); }?></textarea> 
																						<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option2" rows="2" id="option2" value="" class="form-control option2"  ><?php if(isset($rowpost[2])) { echo $rowpost[2]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span> -->
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option2'])) { echo urldecode($rowpost['option2']); }?></textarea> 
																					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option3" rows="2" id="option3" value="" class="form-control option3"  ><?php if(isset($rowpost[3])) { echo $rowpost[3]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span> -->
																		<div class="wrs_col wrs_s12">
																			 <div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option3'])) { echo urldecode($rowpost['option3']); }?></textarea> 
																				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
																	<div class="col-sm-10">
																		<!-- <textarea name="option4" rows="2" id="option4" value="" class="form-control option4"  ><?php if(isset($rowpost[4])) { echo $rowpost[4]; }?></textarea>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span> -->
																		<div class="wrs_col wrs_s12">
																			 <div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['option4'])) { echo urldecode($rowpost['option4']); }?></textarea> 
																				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row correctanswer">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
																	
																	<div class="col-sm-8">
																	<?php
																	
																	if(isset($rowpost['answer'])){
																		if($rowpost['answer']!=''){
																			$_POST['answer']=explode(",",$rowpost['answer']);
																		}
																		
																	}else{
																		
																	}
																	?>
																	
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer1<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer2<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer3<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;

																	<input type="checkbox" class="tests<?php echo $count; ?>"  name="answer4<?php echo $count; ?>" onclick="getFields3(<?php echo $count; ?>)" id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; }  } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
							

																	<input type="hidden" class="tests<?php echo $count; ?> answer " id="correctanswer<?php echo $count; ?>" value="<?php echo $rowpost['answer']; ?>" >
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'.$count])){ echo $_SESSION['error']['answer'.$count];}?></span>
																	</div>
																</div>
															</div>
														</div>
														
														<div class="row">
															<div class="col-lg-12">
																<div class="form-group row">
																	<label class="col-sm-2 col-form-label">Explanation</label>
																	<div class="col-sm-10">
																		<div class="col-sm-10">
																			<!-- <textarea name="explanation" rows="3" id="explanation" value="" class="form-control explanation"  ><?php if(isset($rowpost[6])) { echo $rowpost[6]; }?></textarea>
																			<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span> -->
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($rowpost['explanation'])) { echo urldecode($rowpost['explanation']); }?></textarea> 
																					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
																				</div>
																			</div>
																		</div>
																		
																	</div>
																</div>
															</div>
														</div>
														
														
													</div>
												
												<input type="hidden" name="comid" id="comid" class=" form-control comid" value="<?php if(isset($rowpost['comid'])){ echo $rowpost['comid'];}?>" >
												<?php
												$count++;
											
											
										}
										?>
											<input type="hidden" id="session_list"  value="<?php echo  $count-2;?>"/>
									<?php
									}else{
									?>
									<div class="list" id="list">
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<!-- <textarea name="question" rows="3" id="question" value="<?php if(isset($_POST['question'])) { echo $_POST['question']; }?>" class="form-control question "  ></textarea>
														<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span> -->
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo $_POST['question']; }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<!-- <textarea name="option1" rows="2" id="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>" class="form-control option1"  ></textarea>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span> -->
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option1'])) { echo urldecode($_POST['option1']); }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<!-- <textarea name="option2" rows="2" id="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>" class="form-control option2"  ></textarea>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span> -->
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<!-- <textarea name="option3" rows="2" id="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>" class="form-control option3"  ></textarea>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span> -->
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
													<div class="col-sm-10">
														<!-- <textarea name="option4" rows="2" id="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>" class="form-control option4"  ></textarea>
														<span class="error"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span> -->
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row correctanswer">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
													
													<div class="col-sm-8">
													<?php
													
													if(isset($_POST['answer'])){
														if($_POST['answer']!=''){
															$_POST['answer']=explode(",",$_POST['answer']);
														}
														
													}else{
														
													}
													?>
													
													<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer1" id="ans1" value="<?php if(isset($_POST['answer'])) { if($_POST['answer']!=''){ if(in_array('A', $_POST['answer'])) { echo 'A'; } } else { echo '0'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A &nbsp;&nbsp;
																				
													<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer2" id="ans2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo ''; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
													<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)" name="answer3" id="ans3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo ''; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
													<input type="checkbox" class="tests<?php echo $count; ?>"   onclick="getFields3(<?php echo $count; ?>)" name="answer4" id="ans4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo ''; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

													<input type="hidden" class="tests<?php echo $count; ?> answer " id="correctanswer<?php echo $count; ?>" value="<?php echo $_POST['answer']; ?>" >
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
													</div>
												</div>
											</div>
										</div>
										
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group row">
													<label class="col-sm-2 col-form-label">Explanation</label>
													<div class="col-sm-10">
														<div class="col-sm-10">
															<!-- <textarea name="explanation" rows="3" id="explanation" value="<?php if(isset($_POST['explanation'])) { echo $_POST['question']; }?>" class="form-control explanation"  ></textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span> -->
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo $_POST['explanation']; }?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
																</div>
															</div>
														</div>
														
													</div>
												</div>
											</div>
										</div>
										
										<input type="hidden" name="comid" id="comid" class=" form-control comid" value="" >
									</div>
									
									<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
									<?php } ?>
								</div>
							</div>
						 
							<?php
							}else if($_POST['qtype']=='8'){
							 
							  ?>
								<div class="questionViewdata" >
									<div class="row mt-2 align-items-center ">
										<div class="col-lg-12">
											
											<div class="d-flex justify-content-between align-items-center">
												<h6 class="text-primary" >Question</h6>
												<a class="btn  btn btn-outline-primary text-primary" onclick="getquestionview1();"><i class="fas fa-edit"></i> Edit</a>
											</div>
											<div><?php echo urldecode($_POST['question']); ?></div>
										</div>
										<div class="col-md-12 mt-4">
												<h6 class="text-success"> Correct answer</h6>
												<div ><?php echo rtrim($_POST['answer'],","); ?></div>
										</div>
										<?php if($_POST['explanation']!=''){ ?>
											<div class="col-md-12 mt-4">
												<h6 class="text-danger" >Explanation</h6>
												<div><?php echo urldecode($_POST['explanation']); ?></div>
													

											</div>
										<?php } ?>
										
									</div>
									
								</div>
								<div style="display:none;" class="getquestionview"  >
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
												<div class="col-sm-10">
													<!-- <textarea name="question" rows="3" id="question" value="" class="form-control question "  ><?php if(isset($_POST['question'])) { echo $_POST['question']; }?></textarea>
													<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span> -->
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Correct Answer</label>
													<div class="col-sm-4 mt-2">
													<input type="text" name="correctanswer"  id="correctanswer" onkeypress="return NumAndTwoDecimals(event);"  onkeyup="numberrange();" value="<?php if(isset($_POST['answer'])) { echo $_POST['answer']; }?>" class="form-control"  >
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Explanation</label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="explanation" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
								</div>
								
							<?php
						  }else if($_POST['qtype']=='3'){
								$count1=1;
								if(isset($_POST['errordisplay'])){
									$obj1=$_POST['data5'];
								}else{
									$obj1=json_decode($_POST['question'],true);
								}
								if($_POST['list1type']=='roman'){
									$list1type="upper-roman";
								}else if($_POST['list1type']=='alphabets'){
									$list1type="upper-alpha";
								}else if($_POST['list1type']=='numbers'){
									$list1type="decimal";
								}else{
									$list1type="upper-alpha";
								}
								if($_POST['list2type']=='roman'){
									$list2type="upper-roman";
								}else if($_POST['list2type']=='alphabets'){
									$list2type="upper-alpha";
								}else if($_POST['list2type']=='numbers'){
									$list2type="decimal";
								}else{
									$list2type="upper-alpha";
								}
								?>
								<div class="questionViewdata" >
									<div class="row mt-2 align-items-center ">
										<div class="col-lg-12">
											
											<div class="d-flex justify-content-between align-items-center">
												<h6 class="text-primary" >Question</h6>
												<a class="btn  btn btn-outline-primary text-primary" onclick="getquestionview1();"><i class="fas fa-edit"></i> Edit</a>
											</div>
											<div><?php echo urldecode($_POST['mat_question']); ?></div>
											<?php if($_POST['mat_que_paragraph']!=''){ ?>
							
													<?php echo urldecode($_POST['mat_que_paragraph']); ?>
														
													<br />
												
												<?php } ?>
											<div class="mt-4 questionView">
													
												<div class="row questionlist-types">
													<div class="col-lg-6">
														<h5 class="text-dark" >List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
															<?php
															foreach($obj1 as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														</ul>
													</div>
													<div class="col-lg-6">
														<h5  class="text-dark">List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
															<?php
															foreach($obj1 as $qqlist2)
															{
															
																if(strlen($qqlist2['qlist2'])>0)
																{
																	echo '<li style="width:150px;">'.urldecode($qqlist2['qlist2']).'</li>';
																}
															}
															?>
														</ol>
													</div>
												</div>
											</div>
											<?php if($_POST['mat_que_footer']!=''){ ?>
												<div>
													<?php echo urldecode($_POST['mat_que_footer']); ?>
													
												</div>
												<br />
											<?php } ?>
											<div class="row">

												<div class="col-md-1 py-2 ">
													(A)</div>

												<div class="col-md-5 py-2 ">
													<?php echo $_POST['option1']; ?>
												</div>

												<div class="col-md-1 py-2 ">
													(B)						</div>

												<div class="col-md-5 py-2 ">
													<?php echo $_POST['option2']; ?>
												</div>

												<div class="col-md-1 py-2 ">
													(C)						</div>

												<div class="col-md-5 py-2 ">
													<?php echo $_POST['option3']; ?>
												</div>

												<div class="col-md-1 py-2 ">
													(D)						</div>

												<div class="col-md-5 py-2 ">
													<?php echo $_POST['option4']; ?>
												</div>
												<?php if($_POST['option5']!=''){ ?>
												<div class="col-md-1 py-2 ">
														(E)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $_POST['option5']; ?>
													</div>
												<?php } ?>
											</div>
										</div>
												
										<div class="col-md-12 mt-4">
											<h6 class="text-success">Correct Answer</h6>
											<p><?php echo rtrim($_POST['answer'],","); ?></p>
												

										</div>
										<?php if($_POST['explanation']!=''){ ?>
											<div class="col-md-12 mt-4">
												<h6 class="text-warning">Explanation</h6>
												<div><?php echo urldecode($_POST['explanation']); ?></div>
													

											</div>
										<?php } ?>
										</div>
										
									</div>
									
								</div>
								<div style="display:none;" class="getquestionview" >
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Question</label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="mat_question"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['mat_question'])) { echo urldecode($_POST['mat_question']); }else{} ?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_question'])){ echo $_SESSION['error']['mat_question'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Q.Paragraph<span style="color:red;">*</span></label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="mat_que_paragraph" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_paragraph'])) { echo urldecode($_POST['mat_que_paragraph']); }else{} ?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_paragraph'])){ echo $_SESSION['error']['mat_que_paragraph'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<!-- <label class="col-form-label">Question<span style="color:red;">*</span></label> -->
										</div>
										<div class="col-md-10">
											<div class="list-types pb-3">
											<div class="row">
												<div class="col-md-4">
												<label for="exampleInputEmail1">List1 Type<span style="color:red;">*</span></label>
												<select class="form-control" name="listtype" id="list1type" >
													<option value='alphabets'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='alphabets') { echo 'selected="selected"'; }  } ?>>Alphabets</option>
													<option value='roman'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
													<option value='numbers' <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
												</select>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list1type'])){ echo $_SESSION['error']['list1type'];}?></span>
												</div>
												<div class="col-md-4">
													<label for="exampleInputEmail1">List2 Type<span style="color:red;">*</span></label>
													<select class="form-control" name="list2type" id="list2type" >
														<option value=''>--Select--</option>
														<option value='alphabets' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='alphabets') { echo 'selected="selected"'; }  } ?> >Alphabets</option>
														<option value='roman' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
														<option value='numbers' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
													</select>
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list2type'])){ echo $_SESSION['error']['list2type'];}?></span>
												</div>
											</div>
										</div>
									</div>
									
										<div id="dates_list1" class="col-md-10 offset-md-2">
											<?php
											if(isset($_POST['editform']))
											{
												$dataec = $database->query("select * from  createquestion where estatus='1' and id='".$_POST['id']."'");
												$rowe = mysqli_num_rows($dataec);
												if($rowe > 0)
												{
												while($rowec = mysqli_fetch_array($dataec))
												{
													$list1='';
													$list2='';
													$obj=json_decode($rowec['question'],true);
													foreach($obj as $rowpost)
													{
													?>
														<div class="list1" id="list1<?php echo $count1;?>">
															<div class="row">
																<div class="col-md-5">
																	<div class="form-group">
																		<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																		<div class="">
																			<div class="wrs_container">
																				<div class="wrs_row">
																					<div class="wrs_col wrs_s12">
																						 <div id="editorContainer">
																							<div id="toolbarLocation"></div>
																							<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																							
																						</div>
																					</div>

																				</div>
																			</div>
																		</div>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
																	</div>
																</div>
																<div class="col-md-5">
																	<div class="form-group">
																		<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																		<div class="">
																			<div class="wrs_container">
																				<div class="wrs_row">
																					<div class="wrs_col wrs_s12">
																						 <div id="editorContainer">
																							<div id="toolbarLocation"></div>
																							<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																							<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'])){ echo $_SESSION['error']['qlist2'];}?></span>
																						</div>
																					</div>

																				</div>
																			</div>
																		</div>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
																	</div>
																</div>
																<div class="col-md-2 form-group" style="padding-top: 6rem;">
																	<?php
																	if($count1 == 1){
																	?>
																		<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
																	<?php
																	}
																	else{
																	?>
																		<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
																	<?php
																	}
																	?>
																	
																</div>
															</div>

														</div>
																
														<?php
														$count1++;
														//}
														
													}
												?>
												
												<!--Addrow start -->
												
											<?php
											
											}
											?>
												<input type="hidden" id="session_list1"  value="<?php echo  $count1-1;?>"/>
											<?php
											}else{
												
												$posdata1 = $_POST['data5'];
												foreach($posdata1 as $rowpost)
												{
											?>
													<div class="list1" id="list1<?php echo $count1;?>">
														
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																	<div class="">
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																						
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
																</div>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																	<div class="">
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																						
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
																</div>
															</div>
															<div class="col-md-2 form-group" style="padding-top: 6rem;">
																<?php
																if($count1 == 1){
																?>
																	<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
																<?php
																}
																else{
																?>
																	<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
																<?php
																}
																?>
																
															</div>
														</div>

														
													</div>
												<?php
													$count1++;
												}
												?>
														<input type="hidden" id="session_list1"  value="<?php echo  $count1-2;?>"/>
												<?php
												}
													
											}else if(isset($_REQUEST['data5']))
											{
												$posdata1 = $_POST['data5'];
												foreach($posdata1 as $rowpost)
												{
													?>
														<div class="list1" id="list1<?php echo $count1;?>">
															<div class="row">
																<div class="col-md-5">
																	<div class="form-group">
																		<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																		<div class="">
																			<div class="wrs_container">
																				<div class="wrs_row">
																					<div class="wrs_col wrs_s12">
																						 <div id="editorContainer">
																							<div id="toolbarLocation"></div>
																							<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																							
																						</div>
																					</div>

																				</div>
																			</div>
																		</div>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
																	</div>
																</div>
																<div class="col-md-5">
																	<div class="form-group">
																		<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																		<div class="">
																			<div class="wrs_container">
																				<div class="wrs_row">
																					<div class="wrs_col wrs_s12">
																						 <div id="editorContainer">
																							<div id="toolbarLocation"></div>
																							<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																							
																						</div>
																					</div>

																				</div>
																			</div>
																		</div>
																		<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
																	</div>
																</div>
																<div class="col-md-2 form-group" style="padding-top: 6rem;">
																	<?php
																	if($count1 == 1){
																	?>
																		<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
																	<?php
																	}
																	else{
																	?>
																		<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
																	<?php
																	}
																	?>
																	
																</div>
															</div>
														</div>
													
													
													<?php
													$count1++;
													
													
												}
												?>
													
												<?php
											}else{
											?>
												<div class="list1" id="list1">
													
													<div class="row">
														<div class="col-md-5">
															<div class="form-group">
																<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																<div class="">
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo urldecode($_POST['qlist1']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
															</div>
														</div>
														<div class="col-md-5">
															<div class="form-group">
																<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
																<div class="">
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo urldecode($_POST['qlist2']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
																</div>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
															</div>
														</div>
														<div class="col-md-2 form-group" style="padding-top: 20px">
															<?php
															if($count1 == 1){
															?>
																<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
															<?php
															}
															else{
															?>
																<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
															<?php
															}
															?>
															
														</div>
													</div>
													
												</div>
											<input type="text" id="session_list1"  value="<?php echo  $count1;?>"/>
											<?php } ?>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Q.Footer Text<span style="color:red;">*</span></label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="mat_que_footer" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_footer'])) { echo urldecode($_POST['mat_que_footer']); }else{} ?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_footer'])){ echo $_SESSION['error']['mat_que_footer'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-lg-12 col-md-12">
										<div class="row align-items-center">
											<div class="col-lg-2">
												Options
											</div>
											<div class="col-lg-5">
												<div class="form-group row align-items-center">
													<label class="col-sm-2 col-form-label">A)</label>
													<div class="col-sm-10 mt-2">
														<input type="text"   class="form-control"  id="option1" name="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>"  /> <br />
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
													</div>
												</div>
											</div>
											<div class="col-lg-5">
												<div class="form-group row align-items-center">
													<label class="col-sm-2 col-form-label">B)</label>
													<div class="col-sm-10 mt-2">
														<input type="text"   class="form-control"  id="option2" name="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>"  /> <br />
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
													</div>
												</div>
											</div>
										</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row">
										<div class="col-lg-5 offset-lg-2">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">C)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option3" name="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">D)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option4" name="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
												</div>
											</div>
										</div>
									</div>
										
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row">
										<div class="col-lg-5 offset-lg-2">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">E)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option5" name="option5" value="<?php if(isset($_POST['option5'])) { echo $_POST['option5']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option5'])){ echo $_SESSION['error']['option5'];}?></span>
												</div>
											</div>
										</div>
										
									</div>
										
								</div>
								<div class="container-fluid">
									<div class="row correctanswer">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
												
												<div class="col-sm-8">
												<?php
												
												if(isset($_POST['answer'])){
													if($_POST['answer']!=''){
														$_POST['answer']=explode(",",$_POST['answer']);
													}
													
												}else{
													
												}
												?>
												
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
												<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
												<input type="checkbox" class="tests"   onclick="getFields()"  id="answer5" value="<?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'E'; }else { echo 'E'; } } else { echo 'E'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'checked'; } } ?>/>E &nbsp;&nbsp;
												<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Explanation</label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							}else if($_POST['qtype']=='9'){
								$count2=1;
								if(isset($_POST['errordisplay'])){
									$obj1=$_POST['data5'];
								}else{
									$obj1=json_decode($_POST['question'],true);
								}
								if($_POST['list1type']=='roman'){
									$list1type="upper-roman";
								}else if($_POST['list1type']=='alphabets'){
									$list1type="upper-alpha";
								}else if($_POST['list1type']=='numbers'){
									$list1type="decimal";
								}else{
									$list1type="upper-alpha";
								}
								if($_POST['list2type']=='roman'){
									$list2type="upper-roman";
								}else if($_POST['list2type']=='alphabets'){
									$list2type="upper-alpha";
								}else if($_POST['list2type']=='numbers'){
									$list2type="decimal";
								}else{
									$list2type="upper-alpha";
								}
								?>
									<div class="questionViewdata" >
										<div class="row mt-2 align-items-center ">
											<div class="col-lg-12">
												
												<div class="d-flex justify-content-between align-items-center">
													<h6 class="text-primary" >Question</h6>
													<a class="btn  btn btn-outline-primary text-primary" onclick="getquestionview1();"><i class="fas fa-edit"></i> Edit</a>
												</div>
												<div><?php echo urldecode($_POST['mat_question']); ?></div>
												<?php if($_POST['mat_que_paragraph']!=''){ ?>
							
													<?php echo urldecode($_POST['mat_que_paragraph']); ?>
														
													<br />
												
												<?php } ?>
												<div class="mt-4 questionView">
														
													<div class="row questionlist-types">
														<div class="col-lg-6">
															<h5 class="text-dark" >List1</h5>
															<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
																<?php
																foreach($obj1 as $qqlist)
																{
																
																	if(strlen($qqlist['qlist1'])>0)
																	{
																		echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
																	}
																}
																?>
															
															</ul>
														</div>
														<div class="col-lg-6">
															<h5  class="text-dark">List2</h5>
															<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
																<?php
																foreach($obj1 as $qqlist2)
																{
																
																	if(strlen($qqlist2['qlist2'])>0)
																	{
																		echo '<li style="width:150px;">'.urldecode($qqlist2['qlist2']).'</li>';
																	}
																}
																?>
															</ol>
														</div>
													</div>
												</div>
												<?php if($_POST['mat_que_footer']!=''){ ?>
												<div>
													<?php echo urldecode($_POST['mat_que_footer']); ?>
													
												</div>
												<br />
											<?php } ?>
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A)</div>

													<div class="col-md-5 py-2 ">
														<?php echo $_POST['option1']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $_POST['option2']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $_POST['option3']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $_POST['option4']; ?>
													</div>
													<?php if($_POST['option5']!=''){ ?>
														<div class="col-md-1 py-2 ">
															(E)						
														</div>

														<div class="col-md-5 py-2 ">
															<?php echo $_POST['option5']; ?>
														</div>
													<?php } ?>
												</div>
											</div>
													
											<div class="col-md-12 mt-4">
												<h6 class="text-success">Correct Answer</h6>
												<p><?php echo rtrim($_POST['answer'],","); ?></p>
													

											</div>
											<?php if($_POST['explanation']!=''){ ?>
												<div class="col-md-12 mt-4">
													<h6 class="text-warning">Explanation</h6>
													<div><?php echo urldecode($_POST['explanation']); ?></div>
														

												</div>
											<?php } ?>
											</div>
											
										</div>
										
									</div>
									<div style="display:none;" class="getquestionview" >
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Question</label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="mat_question"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['mat_question'])) { echo urldecode($_POST['mat_question']); }else{} ?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_question'])){ echo $_SESSION['error']['mat_question'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Q.Paragraph<span style="color:red;">*</span></label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="mat_que_paragraph" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_paragraph'])) { echo urldecode($_POST['mat_que_paragraph']); }else{} ?></textarea> 
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_paragraph'])){ echo $_SESSION['error']['mat_que_paragraph'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<!-- <label class="col-form-label">Question<span style="color:red;">*</span></label> -->
										</div>
										<div class="col-md-10">
										<div class="list-types pb-3">
										<div class="row">
											<div class="col-md-4">
											<label for="exampleInputEmail1">List1 Type<span style="color:red;">*</span></label>
											<select class="form-control" name="listtype" id="list1type" >
												<option value='alphabets'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='alphabets') { echo 'selected="selected"'; }  } ?>>Alphabets</option>
												<option value='roman'  <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
												<option value='numbers' <?php if(isset($_POST['list1type'])) { if($_POST['list1type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
											</select>
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list1type'])){ echo $_SESSION['error']['list1type'];}?></span>
											</div>
											<div class="col-md-4">
												<label for="exampleInputEmail1">List2 Type<span style="color:red;">*</span></label>
												<select class="form-control" name="list2type" id="list2type" >
													<option value=''>--Select--</option>
													<option value='alphabets' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='alphabets') { echo 'selected="selected"'; }  } ?> >Alphabets</option>
													<option value='roman' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='roman') { echo 'selected="selected"'; }  } ?>>Roman</option>
													<option value='numbers' <?php if(isset($_POST['list2type'])) { if($_POST['list2type']=='numbers') { echo 'selected="selected"'; }  } ?> >Numbers</option>
												</select>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['list2type'])){ echo $_SESSION['error']['list2type'];}?></span>
											</div>
										</div>
										</div>
									</div>
									<div id="dates_list2" class="col-md-10 offset-md-2">
									<?php
									if(isset($_POST['editform']))
									{
										$dataec = $database->query("select * from  createquestion where estatus='1' and id='".$_POST['id']."'");
										$rowe = mysqli_num_rows($dataec);
										if($rowe > 0)
										{
										while($rowec = mysqli_fetch_array($dataec))
										{
											$obj=json_decode($rowec['question'],true);
											foreach($obj as $rowpost)
											{
												
												
												?>
													<div class="list2" id="list2<?php echo $count2;?>">
									
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	
																	<div class="">
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																						
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" >
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span> -->
																	<div class="">
																		<div class="wrs_container">
																			<div class="wrs_row">
																				<div class="wrs_col wrs_s12">
																					 <div id="editorContainer">
																						<div id="toolbarLocation"></div>
																						<textarea id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																						
																					</div>
																				</div>

																			</div>
																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-2 form-group" style="padding-top: 6rem;">
																<?php
																if($count2 == 1){
																?>
																	<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
																<?php
																}
																else{
																?>
																	<!-- <a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a> -->
																	<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
																<?php
																}
																?>
																
															</div>
														</div>
													</div>
													
												<?php
												$count2++;
												
												
											}
										}
											
										?>
										
									
									<input type="hidden" id="session_list2"  value="<?php echo  $count2-1;?>"/>
									<?php
									}
									else{
												
												$posdata1 = $_POST['data5'];
												foreach($posdata1 as $rowpost)
												{
													
											?>
													<div class="list2" id="list2<?php echo $count2;?>">
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-2 form-group" style="padding-top: 6rem;">
																<?php
																if($count2 == 1){
																?>
																	<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
																<?php
																}
																else{
																?>
																	<!-- <a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a> -->
																	<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
																<?php
																}
																?>
																
															</div>
														</div>
													</div>
												
											<!--Addrow end-->
											
														<?php
														
														$count2++;
													}
													?>
														<input type="hidden" id="session_list2"  value="<?php echo  $count2-2;?>"/>
												<?php
												}
										
									}else if(isset($_REQUEST['data5']))
									{
										$posdata1 = $_POST['data5'];
										foreach($posdata1 as $rowpost)
										{
											
												
												?>
													<div class="list2" id="list2<?php echo $count2;?>">
														<div class="row">
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
																	<div class="wrs_container">
																				<div class="wrs_row">
																					<div class="wrs_col wrs_s12">
																						 <div id="editorContainer">
																							<div id="toolbarLocation"></div>
																							<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
																							
																						</div>
																					</div>

																				</div>
																			</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-5">
																<div class="form-group">
																	<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
																	<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
																	<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
																</div>
															</div>
															<div class="col-md-2 form-group" style="padding-top: 6rem;">
																<?php
																if($count2 == 1){
																?>
																	<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
																<?php
																}
																else{
																?>
																	<!-- <a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a> -->
																	<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
																<?php
																}
																?>
																
															</div>
														</div>
													</div>
												
												
												<?php
												$count2++;
											
											
										}
										?>
											<input type="hidden" id="session_list2"  value="<?php echo  $count2;?>"/>
										<?php
									}else{
									?>
										<div class="list2" id="list2">
										
											<div class="row">
												<div class="col-md-4">
													<div class="form-group">
														<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
														<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($_POST['qlist1'])) { echo $_POST['qlist1']; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
														<div class="wrs_container">
																		<div class="wrs_row">
																			<div class="wrs_col wrs_s12">
																				 <div id="editorContainer">
																					<div id="toolbarLocation"></div>
																					<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist1'])) { echo urldecode($_POST['qlist1']); }?></textarea>
																					
																				</div>
																			</div>

																		</div>
																	</div>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
														<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
														<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($_POST['qlist2'])) { echo $_POST['list1']; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
														<div class="wrs_container">
															<div class="wrs_row">
																<div class="wrs_col wrs_s12">
																	 <div id="editorContainer">
																		<div id="toolbarLocation"></div>
																		<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['qlist2'])) { echo urldecode($_POST['qlist2']); }?></textarea>
																		
																	</div>
																</div>

															</div>
														</div>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
													</div>
												</div>
												<div class="col-md-2 form-group" style="padding-top: 6rem;">
													<?php
													if($count2 == 1){
													?>
														<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
													<?php
													}
													else{
													?>
														<!-- <a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a> -->
														<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
													<?php
													}
													?>
													
												</div>
											</div>
										
										
										
										</div>
									<input type="hidden" id="session_list2"  value="<?php echo  $count2;?>"/>
									<?php }
										?>
									
									</div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Q.Footer Text<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<textarea id="mat_que_footer" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['mat_que_footer'])) { echo urldecode($_POST['mat_que_footer']); }else{} ?></textarea> 
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['mat_que_footer'])){ echo $_SESSION['error']['mat_que_footer'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row align-items-center">
										<div class="col-lg-2">
											Options
										</div>
										<div class="col-lg-5">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">A)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option1" name="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">B)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option2" name="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row">
										<div class="col-lg-5 offset-lg-2">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">C)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option3" name="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-5">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">D)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option4" name="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
												</div>
											</div>
										</div>
									</div>
										
								</div>
								<div class="col-lg-12 col-md-12">
									<div class="row">
										<div class="col-lg-5 offset-lg-2">
											<div class="form-group row align-items-center">
												<label class="col-sm-2 col-form-label">E)</label>
												<div class="col-sm-10 mt-2">
													<input type="text"   class="form-control"  id="option5" name="option5" value="<?php if(isset($_POST['option5'])) { echo $_POST['option5']; }?>"  /> <br />
													<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option5'])){ echo $_SESSION['error']['option5'];}?></span>
												</div>
											</div>
										</div>
										
									</div>
										
								</div>
								<div class="container-fluid">
									<div class="row correctanswer">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
												
												<div class="col-sm-8">
												<?php
												
												if(isset($_POST['answer'])){
													if($_POST['answer']!=''){
														$_POST['answer']=explode(",",$_POST['answer']);
													}
													
												}else{
													
												}
												?>
												
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
												<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
												<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;
												<input type="checkbox" class="tests"   onclick="getFields()"  id="answer5" value="<?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'E'; }else { echo 'E'; } } else { echo 'E'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('E', $_POST['answer'])) { echo 'checked'; } } ?>/>E &nbsp;&nbsp;
												<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-2 col-form-label">Explanation</label>
												<div class="col-sm-10">
													<div class="wrs_container">
														<div class="wrs_row">
															<div class="wrs_col wrs_s12">
																 <div id="editorContainer">
																	<div id="toolbarLocation"></div>
																	<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
																	<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
																</div>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php
							}else{
								$type="General";
								
							?>
							<div class="questionViewdata" id="questionviewrow">
							
								
								<div class="row mt-2 align-items-center ">
									<div class="col-lg-12">
										<div class="d-flex justify-content-between align-items-center">
											<h6 class="text-primary" >Question</h6>
											<a class="btn  btn btn-outline-primary text-primary" onclick="getquestionview1();"><i class="fas fa-edit"></i> Edit</a>
										</div>

										
										<div class="imagewrapper"><?php echo urldecode($_POST['question']); ?></div>
										<div class="row">

											<div class="col-md-1 py-2 ">
												(A) 						</div>

											<div class="col-md-5 py-2 ">
												<?php echo urldecode($_POST['option1']); ?>
											</div>

											<div class="col-md-1 py-2 ">
												(B)						</div>

											<div class="col-md-5 py-2 ">
												<?php echo urldecode($_POST['option2']); ?>
											</div>

											<div class="col-md-1 py-2 ">
												(C)						</div>

											<div class="col-md-5 py-2 ">
												<?php echo urldecode($_POST['option3']); ?>
											</div>

											<div class="col-md-1 py-2 ">
												(D)						</div>

											<div class="col-md-5 py-2 ">
												<?php echo urldecode($_POST['option4']); ?>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="mt-4">
									<h6 class="text-success" >Correct Answer</h6>
									<p><?php echo rtrim($_POST['answer'],","); ?></p>
										

								</div>
								<div class="mt-4">
									<h6 class="text-warning">Explanation</h6>
									<div><?php echo urldecode($_POST['explanation']); ?></div>
										

								</div>
							</div>
							
							<div style="display:none;" class="getquestionview" >
								<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																  
																<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly ><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> </textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
									<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																 
																<textarea id="option1"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly ><?php if(isset($_POST['question'])) { echo urldecode($_POST['option1']); }else{} ?> </textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
									<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																 
																<textarea id="option2"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly > <?php if(isset($_POST['option2'])) { echo urldecode($_POST['option2']); }else{} ?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
									<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																<!--  <div id="option3" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
																 
																									</div> -->
																<textarea id="option3"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly ><?php if(isset($_POST['option3'])) { echo urldecode($_POST['option3']); }else{} ?> </textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
									<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																
																<textarea id="option4"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly > <?php if(isset($_POST['option4'])) { echo urldecode($_POST['option4']); }else{} ?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
									<div class="row">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Explanation</label>
											<div class="col-sm-10">
												<div class="wrs_container">
													<div class="wrs_row">
														<div class="wrs_col wrs_s12">
															 <div id="editorContainer">
																<div id="toolbarLocation"></div>
																
																<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example" readonly > <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
																
															</div>
														</div>

													</div>
												</div>
											</div>
											
										</div>
									</div>
								  
								 </div>
								
								  <div class="row">
									
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Explanation Video link</label>
											<div class="col-sm-8">
												<input type="text" class="form-control" name="expla_vlink" value="<?php if(isset($_POST['expla_vlink'])) { echo urldecode($_POST['expla_vlink']); }else{} ?>"  autocomplete="off" id="expla_vlink" >
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['expla_vlink'])){ echo $_SESSION['error']['expla_vlink'];}?></span>
											</div>
										</div>
									</div>
								</div>
								 <div class="row correctanswer">
									<div class="col-lg-12">
										<div class="form-group row">
											<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
											
											<div class="col-sm-8">
											<?php
											if(isset($_POST['answer'])){
												if($_POST['answer']!=''){
													$_POST['answer']=explode(",",$_POST['answer']);
												}
												
											}else{
												
											}
											?>		
											<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
														
											<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
											<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
											<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

											<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<?php
							}
						}else{
							
							?>
							
						<div style="display:none;">
							<div class="row">
							<div class="col-lg-12">
								<div class="form-group row">
									<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
									<div class="col-sm-10">
										<div class="wrs_container">
											<div class="wrs_row">
												<div class="wrs_col wrs_s12">
													 <div id="editorContainer">
														<div id="toolbarLocation"></div>
														  
														<textarea id="question" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['question']); }else{} ?></textarea> </textarea>
														<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span>
													</div>
												</div>

											</div>
										</div>
									</div>
								</div>
							</div>
						  
							 </div>
								<div class="row">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
										<div class="col-sm-10">
											<div class="wrs_container">
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															 
															<textarea id="option1"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo urldecode($_POST['option1']); }else{} ?> </textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							  
							 </div>
								<div class="row">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
										<div class="col-sm-10">
											<div class="wrs_container">
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															 
															<textarea id="option2"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option2'])) { echo urldecode($_POST['option2']); }else{} ?></textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							  
							 </div>
								<div class="row">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
										<div class="col-sm-10">
											<div class="wrs_container">
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															<!--  <div id="option3" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
															 
																								</div> -->
															<textarea id="option3"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo urldecode($_POST['option3']); }else{} ?> </textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							  
							 </div>
								<div class="row">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
										<div class="col-sm-10">
											<div class="wrs_container">
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															 <!-- <div id="option4" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example">
															 
																								</div> -->
															<textarea id="option4"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['option4'])) { echo urldecode($_POST['option4']); }else{} ?></textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							  
							 </div>
								<div class="row">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Explanation<span style="color:red;">*</span></label>
										<div class="col-sm-10">
											<div class="wrs_container">
												<div class="wrs_row">
													<div class="wrs_col wrs_s12">
														 <div id="editorContainer">
															<div id="toolbarLocation"></div>
															 <!-- <div id="explanation" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> -->
															<textarea id="explanation"  class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($_POST['explanation'])) { echo urldecode($_POST['explanation']); }else{} ?></textarea>
															<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
															
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</div>
							  
							 </div>
							
							  <div class="row">
								
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Explanation Video link</label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="expla_vlink" value="<?php if(isset($_POST['expla_vlink'])) { echo urldecode($_POST['expla_vlink']); }else{} ?>"  autocomplete="off" id="expla_vlink" >
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['expla_vlink'])){ echo $_SESSION['error']['expla_vlink'];}?></span>
										</div>
									</div>
								</div>
							</div>
							 <div class="row correctanswer">
								<div class="col-lg-12">
									<div class="form-group row">
										<label class="col-sm-2 col-form-label">Answer<span style="color:red;">*</span></label>
										
										<div class="col-sm-8">
										<?php
										if(isset($_POST['answer'])){
											if($_POST['answer']!=''){
												$_POST['answer']=explode(",",$_POST['answer']);
											}
											
										}else{
											
										}
										?>		
										
										

										<input type="checkbox" class="tests"  onclick="getFields()" id="answer1" value="<?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'A'; }else { echo 'A'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A&nbsp;&nbsp;
													
										<input type="checkbox" class="tests"  onclick="getFields()" id="answer2" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo 'B'; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
										<input type="checkbox" class="tests"  onclick="getFields()" id="answer3" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo 'C'; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
										<input type="checkbox" class="tests"   onclick="getFields()"  id="answer4" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo 'D'; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D &nbsp;&nbsp;

										<input type="hidden" class="tests" id="correctanswer" value="<?php echo $_POST['answer']; ?>" >
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php 
				
						} 
						?>
							<div class="getquestionview pb-5" style="display:none;" >
								<a class="btn btn-info text-white" onclick="getquestionview2();">Back</a>
							</div>
                           <hr />
							<div class="container-fluid">
                                <div class="row">
                                   
								<div class="form-group col-md-2">
									<?php
									
									?>
										<label for="inputExam">Class<span class="text-danger">*</span></label>
									<div class="custom-control custom-checkbox ">
										<input type="checkbox" id="customcheckboxInline3" name="customcheckboxInline3" class="class3 " onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', explode(",",$_POST['class']))) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', explode(",",$_POST['class']))) { echo 'checked'; } } ?> >
										<label class="" for="customcheckboxInline3">XI</label>
									</div>
									<div class="custom-control custom-checkbox ">
										<input type="checkbox" id="customcheckboxInline4" name="customcheckboxInline3" class="class3 " onChange="getFields2();setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', explode(",",$_POST['class']))) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', explode(",",$_POST['class']))) { echo 'checked'; }} ?> >
										<label class="" for="customcheckboxInline4">XII</label>
									</div>
									<input type="hidden" class="class3" id="class" value="<?php echo $_POST['class']; ?>" >
                                   <span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
									</div>
									<?php
									
									if(isset($_POST['exam'])){
										if($_POST['exam']!=''){
											$_POST['exam']=explode(",",$_POST['exam']);
										}
										
									}else{
										
									}

									
									?>
                                   
                                        
										<div class="col-md-2">
											<!--<label for="inputExam">Exam<span class="text-danger">*</span></label>
											<div class="custom-control  ">
													<input type="checkbox" id="exam1" name="customcheckboxInline1" class="exam " onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexamtype1=1&exam='+$('#exam').val()+'')" id="exam1"
												value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('1', $_POST['exam'])) { echo '1'; }else{ echo '1'; } } else { echo '1'; } } else { echo '1'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('1', $_POST['exam'])) { echo 'checked'; } } ?> >
													<label >NEET</label>
												</div>
												<div class="custom-control  ">
													<input type="checkbox" id="exam2" name="customcheckboxInline1" class="exam " onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'');;setState('pexamtype1','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexamtype1=1&exam='+$('#exam').val()+'')"  value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('2', $_POST['exam'])) { echo '2'; }else{ echo '2'; } } else { echo '2'; } } else { echo '2'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('2', $_POST['exam'])) { echo 'checked'; } } ?> >
													<label >JEE</label>
												</div>
												<div class="custom-control  ">
													<input type="checkbox" id="exam3" name="customcheckboxInline1" class="exam " onChange="getFields1();setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'');;setState('pexamtype1','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexamtype1=1&exam='+$('#exam').val()+'')" value="<?php if(isset($_POST['exam'])) { if($_POST['exam']!=''){ if(in_array('3', $_POST['exam'])) { echo '3'; }else{ echo '3'; } } else { echo '3'; } } else { echo '3'; } ?>" <?php if(isset($_POST['exam'])) { if(in_array('3', $_POST['exam'])) { echo 'checked'; } } ?> >
													<label >EAMCET</label>
												</div>
												
												<input type="hidden" class="exam" id="exam" value="<?php echo $_POST['exam']; ?>" >
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>-->
												<label class="slabel" for="inputExam">Exam<span style="color:red;">*</span></label>
												<select  name="exam" id="exam" class="form-control exampicker1"  multiple onChange="setState('aaa','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexam=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'');setState('pexamtype1','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexamtype1=1&exam='+$('#exam').val()+'')">
													<option value=''>--Select--</option>
													<?php
													$sel=$database->query("select * from exam where estatus='1'");
													while($data = mysqli_fetch_array($sel))
													{
														?>
													<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['exam'])) { if(in_array($data['id'], $_POST['exam'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['exam'];?></option>
													<?php
													}
													?>
												</select>
												<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
											</div>
										 <div class="form-row col-md-8">
                                        <div class="form-group col-md-2" id="aaa">
                                            <label for="inputSub">Subjects</label>
                                            <select class="form-control" name="subject" value=""   id="subject" onChange="setState('ccc','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getchapter=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'')">
											<option value=''>-- Select --</option>
											<?php
											
											if($_REQUEST['exam']=='1'){
												$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and id!='4'");
											}else if($_REQUEST['exam']=='2'){
												$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") and id!=1 and id!=5 ");
											}else if($_REQUEST['exam']=='3'){
												$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") ");

											}else if($_REQUEST['exam']=='4'){
												$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") ");

											}else if($_REQUEST['exam']=='1,2' ||  $_REQUEST['exam']=='1,2,3' ||  $_REQUEST['exam']=='1,2,3,4'){
												$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and  id in (2,3)");
											}else if($_REQUEST['exam']=='1,3' || $_REQUEST['exam']=='1,4' || $_REQUEST['exam']=='1,3,4'){
												$row1 = $database->query("select * from subject where estatus='1'  AND id IN(".rtrim($rowuser['subject'],',').") and  id!=4 ");
											}else if($_REQUEST['exam']=='2,3' || $_REQUEST['exam']=='2,4'){
												$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") and  id in (2,3,4)");
											}else{
												$row1 = $database->query("select * from subject where estatus='1' AND id IN(".rtrim($rowuser['subject'],',').") ");
											}
											while($data = mysqli_fetch_array($row1))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['subject'])) { if($_POST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
											<?php
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
                                        </div>
                                       <?php
										$chapter=$_POST['chapter'];
										$_POST['chapter']=explode(",",$_POST['chapter']);
										$_POST['topic']=explode(",",$_POST['topic']);
										?>
										<div class="form-group col-md-5" id="ccc" >
											<label for="inputTopic">Chapter</label>
											 <select class="form-control" name="chapter" value=""    multiple  id="chapter" onChange="setState('ddd','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','gettopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&exam='+$('#exam').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'')" >
												<?php
												//$zSql = $database->query("SELECT * FROM `chapter` WHERE class IN  (".rtrim($_POST['class'],',').") and subject = '".$_POST['subject']."' AND id IN(".rtrim($rowuser['chapter'],',').")"); 
												$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($_POST['class'],',').") and subject = '".$_POST['subject']."'  "); 
												while($data = mysqli_fetch_array($zSql))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter'])) {if(in_array($data['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error" style="display:block;color:red;font-size:9px;"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
											
                                      
                                  
										</div>
										<div class="form-group col-md-5" id="ddd">
											<label for="inputTopic" style="display:block;">Topic<span style="color:red;">*</span></label>
											<select class="form-control " name="topic" multiple value=""     id="topic" >
												<?php
												
												$zSql1 = $database->query("SELECT * FROM `topic` WHERE estatus='1' and class IN  (".rtrim($_POST['class'],",").") and subject = '".$_POST['subject']."' and chapter IN (".$chapter.") "); 
												
												if(mysqli_num_rows($zSql1)>0)
												{ 
													while($data = mysqli_fetch_array($zSql1))
													{ 
														?> 
														<option value="<?php echo $data['id'];?>"  <?php if(isset($_POST['topic'])) { if(in_array($data['id'], $_POST['topic'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['topic'];?></option>
													<?php
													}
													
												}
												?>
											</select>
											<span class="error" style="display:block;color:red;font-size:9px;"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
										</div>
                                    </div>
                                </div>
								</div>
								<?php 
									if(isset($_POST['ppaper'])){
										
										if($_POST['ppaper']=='1'){
											$dis="disabled";
											$sty='style=""';
										}else{
											$dis="";
											$sty='style="display:none;"';
										}
										
										
									}
									
								?>
								<?php
									if(isset($_POST['exam3'])){
										if($_POST['exam3']!=''){
											if($_POST['exam3']!=''){
												$style="";
											}else{
												$style='style="display:none;"';
											}
											if(rtrim($_POST['exam3'],",")=='1'){
												$style9='style="display:none;"';
												$_POST['pexamtype']='0';
												
											}else if(rtrim($_POST['exam3'],",")=='2'){
												$style9="";
												
											}else if(rtrim($_POST['exam3'],",")=='1,2' || rtrim($_POST['exam3'],",")=='2,3' || rtrim($_POST['exam3'],",")=='1,2,3'){
												$style9="";
												$_POST['pexamtype']='1';
											}else if(rtrim($_POST['exam3'],",")=='3'){
												$style9='style="display:none;"';
												$_POST['pexamtype']='0';
											}else{
												$style9='style="display:none;"';
												$_POST['pexamtype']='0';
											}
											
										}else{
											$style9='style="display:none;"';
											$_POST['pexamtype']='0';
										}
									}else{
										$style9='style="display:none;"';
										$_POST['pexamtype']='0';
									}
									
								?>
								<div class="container-fluid">
									<div class="row" id="pexamtype1">
										<div class="col-lg-4"   <?php echo $style9; ?> >
											<div class="form-group row" >
												<label class="col-md-6 col-form-label">Mains/Advance<span style="color:red;">*</span></label>
												<div class="col-md-6">
													<select name="pexamtype" id="pexamtype"  class="form-control"  >
														<option value=''>-- Select --</option>
														<option value='1' <?php if(isset($_POST['pexamtype'])){ if($_POST['pexamtype']=='1'){ echo 'selected';}else{}} ?> >Mains</option>
														<option value='2' <?php if(isset($_POST['pexamtype'])){ if($_POST['pexamtype']=='2'){ echo 'selected';}else{}} ?> >Advance</option>
														
														
													</select>
													<span class="error"><?php if(isset($_SESSION['error']['pexamtype'])){ echo $_SESSION['error']['pexamtype'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row" <?php echo $sty; ?>>
										<div class="col-lg-4">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Paper</label>
												<div class="col-sm-6 mt-2">
													<input tabindex="7"  type="radio"  class="ppaper"  id="idyes" onclick="radiofunction('1');$('.ppaper').val('1')"  name="ppaper" value="1" <?php if(isset($_POST['ppaper'])){ if($_POST['ppaper'] == '1'){ echo 'checked="checked"';}}else{ echo '';}?>  /> <label for="idyes">Yes</label>
													<input tabindex="8"  type="radio"  class="ppaper" id="idno" onclick="radiofunction('0');$('.ppaper').val('0')" name="ppaper" value="0" <?php if(isset($_POST['ppaper'])){ if($_POST['ppaper'] == '0'){ echo 'checked="checked"';}}else{ echo 'checked="checked"';;}?> <?php echo $dis; ?> />
													<label for="idno">No</label>
													<input type="hidden"   class="ppaper"  id="ppaper" name="ppaper" value="<?php echo $_POST['ppaper']; ?>"  /> <br />
													<span class="error text-danger" id="gender_err" style="display:none;">Please Select Previous Paper</span>
												</div>
											</div>
										</div>
										
									</div>
									<div class="row" <?php echo $sty; ?> >
										<div class="col-lg-4 yeardiv" <?php echo $style; ?> >
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Year<span style="color:red;">*</span></label>
												<div class="col-sm-6">
													<select name="year" id="year"  class="form-control yeardivval" onChange="setStateGet('examsetid','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getexamset=1&year='+$('#year').val()+'&exam=<?php echo $_POST['exam3']; ?>')" >
														<option value="" >-Select-</option>
														<?php
											 
														$sel=$database->query("select * from previous_questions where estatus='1'  and exam in (".rtrim($_POST['exam3'],",").") group by year ORDER by id DESC");
														while($row=mysqli_fetch_array($sel)){
															?>
																<option value="<?php echo $row['year'];?>" <?php if(isset($_POST['year'])) { if($_POST['year']==$row['year']) { echo 'selected="selected"'; }  } ?>><?php echo $row['year'];?></option>
														<?php
														}
														?>
													</select>
													<span class="error"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-4 yeardiv"  >
											<div class="form-group row" id="examsetid" >
												<label class="col-md-6 col-form-label">Exam Set ID<span style="color:red;">*</span></label>
												<div class="col-md-6">
													<select name="qset" id="qset"  class="form-control"  >
														<option value=''>-- Select --</option>
														<?php
														
														$sel=$database->query("select * from previous_questions where estatus='1'  and exam in (".rtrim($_POST['exam3'],",").") and year='".$_POST['year']."' group by year ORDER by id DESC");
														while($row=mysqli_fetch_array($sel)){
															$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
															while($row1=mysqli_fetch_array($sel1)){
															?>
																<option value="<?php echo $row1['id'];?>" <?php if(isset($_POST['qset'])) { if($_POST['qset']==$row1['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row1['qset'];?></option>
														<?php
															}
														}
														?>
													</select>
													<span class="error"><?php if(isset($_SESSION['error']['qset'])){ echo $_SESSION['error']['qset'];}?></span>
												</div>
											</div>
										</div>
									
									</div>
							
									 <div class="row">
										<div class="col-md-3">
											<div class="form-group">
												<label for="complexity">Complexity</label>
												
												 <select class="form-control" name="complexity" value=""   id="complexity" >
													<option value=''>-- Select --</option>
													<?php
													$row = $database->query("select * from complexity where estatus='1'");
													while($data = mysqli_fetch_array($row))
													{
														?>
													<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['complexity'])) { if($_POST['complexity']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['complexity']);?></option>
													<?php
													}
													?>
												</select>
												<span class="error text-danger"><?php if(isset($_SESSION['error']['complexity'])){ echo $_SESSION['error']['complexity'];}?></span>
											</div>
											
										</div>
										<div class="col-md-3">
											<div class="form-group">
													<h6>Time Duration(Seconds)</h6>
														<div class="input-group">

														<input type="text" class="form-control border-right-0"
															id="timeduration" value='<?php if(isset($_POST['timeduration'])){ if($_POST['timeduration']!='0'){ echo $_POST['timeduration'] ; } }else{ echo ''; }?>' placeholder="In Seconds" onkeypress="return onlyNumbers(event);" maxlength="4"
															aria-describedby="inputGroupAppend" required autocomplete="off"  >
														<div class="input-group-append">
															<span class="input-group-text bg-transparent" id="inputGroupAppend"><i
																	class="fas fa-clock"></i></span>
														</div>
													</div>
												</div>
												<span class="error text-danger"><?php if(isset($_SESSION['error']['timeduration'])){ echo $_SESSION['error']['timeduration'];}?></span>
										</div>
										<?php $_POST['inputquestion']=explode(",",$_POST['inputquestion']); ?>
										<div class="col-md-3">
											<div class="form-group">
												<label for="inputquestion">Type of Question</label>
												
												 <select class="form-control " name="inputquestion" value=""  multiple    id="inputquestion" >
													<?php
													$row = $database->query("select * from questiontype where estatus='1'");
													while($data = mysqli_fetch_array($row))
													{
														?>
													
													<option value="<?php echo $data['id'];?>"  <?php if(isset($_POST['inputquestion'])) { if(in_array($data['id'], $_POST['inputquestion'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['questiontype'];?></option>
													<?php
													}
													?>
												</select>
												<span class="error text-danger"><?php if(isset($_SESSION['error']['inputquestion'])){ echo $_SESSION['error']['inputquestion'];}?></span>
											</div>
										</div>
										<div class="col-md-3">
											<div class="form-group">
												<label for="complexity">Usage Set</label>
											   
												<select class="form-control" name="usageset" value=""   id="usageset" >
													<option value=''>-- Select --</option>
													<?php
													$row = $database->query("select * from question_useset where estatus='1'");
													while($data = mysqli_fetch_array($row))
													{
														?>
													<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['usageset'])) { if($_POST['usageset']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['usageset']);?></option>
													<?php
													}
													?>
												</select>
												<span class="error text-danger"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
											</div>
											
										</div>
                                
									</div>
									<div class="row">
									   <div class="form-group col-md-3" id="aaa">
											<label for="inputSub">Question Theory</label>
											<select class="form-control" name="question_theory" value=""   id="question_theory" >
											<option value=''>-- Select --</option>
											<?php
											$row1 = $database->query("select * from question_theory where estatus='1' ");
											while($data = mysqli_fetch_array($row1))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['question_theory'])) { if($_POST['question_theory']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['question_theory'];?></option>
											<?php
											}
											?>
										</select>
										<span class="error text-danger" ><?php if(isset($_SESSION['error']['question_theory'])){ echo $_SESSION['error']['question_theory'];}?></span>
										</div>
										<?php $_POST['conttype']=explode(",",$_POST['conttype']); ?>
										<!--<div class="form-group col-md-3">
											<label for="inputSub">Link To Content</label>
											<select class="form-control" name="conttype" value="" multiple  id="conttype"   onchange="contentfunction();" >
											<?php
											$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id!='1' and id!='5' and id!='9'");  
											if(mysqli_num_rows($zSql12)>0)
											{ 
												while($data = mysqli_fetch_array($zSql12))
												{ 
													?> 
													<option value="<?php echo $data['id'];?>"  <?php if(isset($_POST['conttype'])) { if(in_array($data['id'], $_POST['conttype'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['customcontent'];?></option>
												<?php
												}
												
											}
											?>
											</select>
										
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['conttype'])){ echo $_SESSION['error']['conttype'];}?></span>
										</div>
										<div class="form-group col-md-3" id="fff">
											<label for="inputSub">Content ID</label>
					
											<input type="text" class="form-control" name="conttypenos" id="conttypenos" value="<?php if(isset($_POST['conttypeids'])){ if($_POST['conttypeids']!='' && $_POST['conttypeids']!='undefined'){ echo rtrim($_POST['conttypeids'],","); }else{ echo ''; } } ?>" readonly >
											
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['conttypenos'])){ echo $_SESSION['error']['conttypenos'];}?></span>
										</div>
										<div class="form-group view col-md-3" >
											<label for="inputSub"></label>
											<a class="view-tag pt-4" style="color:blue;cursor:pointer;" data-toggle="modal" data-target="#exampleModal2" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>dashboard_v1/process.php','viewDetails2=1&quesid=<?php echo $_REQUEST['editform']; ?>&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'')">View</a> 
										</div>-->
									</div>
									
								</div>
							</div>
					
					<div class="p-3 text-right">
					<script>
					function rand(){
						
						var qtype=$('#qtype').val();
						var data2;
						if(qtype=='5'){

							var newarray = [];
							i=1;
							$('#dates_list .list').each(function(){
								var retval = {};
								console.log(tinymce.get('question'+i).getContent());
								//retval+= $(this).find('.question').val()+'_'+$(this).find('.option1').val()+'_'+$(this).find('.option2').val()+'_'+$(this).find('.option3').val()+'_'+$(this).find('.option4').val()+'_'+$(this).find('.answer').val()+'_'+$(this).find('.explanation').val()+'^';
								retval.question= encodeURIComponent(tinymce.get('question'+i).getContent());
								retval.option1= encodeURIComponent(tinymce.get('option1'+i).getContent());
								retval.option2= encodeURIComponent(tinymce.get('option2'+i).getContent());
								retval.option3= encodeURIComponent(tinymce.get('option3'+i).getContent());
								retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
								retval.explanation= encodeURIComponent(tinymce.get('explanation'+i).getContent());
								retval.comid= $(this).find('.comid').val();
								retval.answer= $(this).find('.answer').val();
								newarray.push(retval);
								console.log('new',newarray);
								i++;
							});
							console.log('new',newarray);

							 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img2.php',
								data:JSON.stringify(newarray),
								contentType: 'application/json; charset=utf-8',
										
								dataType: 'json',
								success: function (data) {
										data2 = '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "","compquestion" : "' + encodeURIComponent(tinymce.get('compquestion').getContent())+ '","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
									 $.ajax({
										type: 'POST',
										url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
										//url: 'http://107.178.223.50/qsbank/dashboard_v1/img.php',
										//url: 'http://neetjeeguru.com/dashboard_v1/img.php',
										data: data2,
										contentType: 'application/json; charset=utf-8',		
										dataType: 'json',
										
										success: function (result,xhr) {
												console.log("reply");
												
													setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=0&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
												
												
										},
											error: function(e){	

											console.log("ERROR: ", e);
										}
									}); 
										
								},
									error: function(e){	

								console.log("ERROR: ", e);
								}
							});

						}else if(qtype=='3'){

							var newarray = [];
							i=1;
							$('#dates_list1 .list1').each(function(){
								var retval = {};
								//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
								retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
								retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

								newarray.push(retval);
								console.log("kkk"+retval);
								i++;

							});

							console.log('new',newarray);

							 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img1.php',
							data:JSON.stringify(newarray),
							contentType: 'application/json; charset=utf-8',
									
							dataType: 'json',
							success: function (data) {
								data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';
								 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',		
								dataType: 'json',
								
								success: function (result,xhr) {
										console.log("reply");
										
											

											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=0&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
										
									},
									error: function(e){	

									console.log("ERROR: ", e);
								}
							});
									
							},
								error: function(e){	

							console.log("ERROR: ", e);
							}
						}); 
					}else if(qtype=='9'){
							var newarray = [];
							i=1;
							$('#dates_list2 .list2').each(function(){
								var retval = {};
								//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
								retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
								retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

								newarray.push(retval);
								console.log("kkk"+retval);
								i++;

							});

						console.log('new',newarray);

						 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img1.php',
							data:JSON.stringify(newarray),
							contentType: 'application/json; charset=utf-8',
									
							dataType: 'json',
							success: function (data) {
								data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';

								 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',		
								dataType: 'json',
								
								success: function (result,xhr) {
										console.log("reply");
										
											
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=0&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
										
									},
									error: function(e){	

									console.log("ERROR: ", e);
								}
							});
									
							},
								error: function(e){	

							console.log("ERROR: ", e);
							}
						}); 
					}else{
						if(qtype=='7'){
							data2 = '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}else if(qtype=='8'){
							data2 ='{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}else{
							data2=  '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}

						$.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
							//url: 'http://107.178.223.50/qsbank/dashboard_v1/img.php',
							//url: 'http://neetjeeguru.com/dashboard_v1/img.php',
							data: data2,
							contentType: 'application/json; charset=utf-8',		
							dataType: 'json',
							
							success: function (result,xhr) {
									console.log("reply");
									setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=0&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
									
							},
								error: function(e){	

								console.log("ERROR: ", e);
							}
						});
						
					}

				}

				function rand1(){
						
						var qtype=$('#qtype').val();
						var data2;
						if(qtype=='5'){

							var newarray = [];
							i=1;
							$('#dates_list .list').each(function(){
								var retval = {};
								console.log(tinymce.get('question'+i).getContent());
								//retval+= $(this).find('.question').val()+'_'+$(this).find('.option1').val()+'_'+$(this).find('.option2').val()+'_'+$(this).find('.option3').val()+'_'+$(this).find('.option4').val()+'_'+$(this).find('.answer').val()+'_'+$(this).find('.explanation').val()+'^';
								retval.question= encodeURIComponent(tinymce.get('question'+i).getContent());
								retval.option1= encodeURIComponent(tinymce.get('option1'+i).getContent());
								retval.option2= encodeURIComponent(tinymce.get('option2'+i).getContent());
								retval.option3= encodeURIComponent(tinymce.get('option3'+i).getContent());
								retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
								retval.explanation= encodeURIComponent(tinymce.get('explanation'+i).getContent());
								retval.comid= $(this).find('.comid').val();
								retval.answer= $(this).find('.answer').val();
								newarray.push(retval);
								console.log('new',newarray);
								i++;
							});
							console.log('new',newarray);

							 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img2.php',
								data:JSON.stringify(newarray),
								contentType: 'application/json; charset=utf-8',
										
								dataType: 'json',
								success: function (data) {
										data2 = '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "","compquestion" : "' + encodeURIComponent(tinymce.get('compquestion').getContent())+ '","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
									 $.ajax({
										type: 'POST',
										url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
										//url: 'http://107.178.223.50/qsbank/dashboard_v1/img.php',
										//url: 'http://neetjeeguru.com/dashboard_v1/img.php',
										data: data2,
										contentType: 'application/json; charset=utf-8',		
										dataType: 'json',
										
										success: function (result,xhr) {
												console.log("reply");
												
													setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=1&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
												
												
										},
											error: function(e){	

											console.log("ERROR: ", e);
										}
									}); 
										
								},
									error: function(e){	

								console.log("ERROR: ", e);
								}
							});

						}else if(qtype=='3'){

							var newarray = [];
							i=1;
							$('#dates_list1 .list1').each(function(){
								var retval = {};
								//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
								retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
								retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

								newarray.push(retval);
								console.log("kkk"+retval);
								i++;

							});

							console.log('new',newarray);

							 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img1.php',
							data:JSON.stringify(newarray),
							contentType: 'application/json; charset=utf-8',
									
							dataType: 'json',
							success: function (data) {
								data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';
								 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',		
								dataType: 'json',
								
								success: function (result,xhr) {
										console.log("reply");
										
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=1&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
										
									},
									error: function(e){	

									console.log("ERROR: ", e);
								}
							});
									
							},
								error: function(e){	

							console.log("ERROR: ", e);
							}
						}); 
					}else if(qtype=='9'){
							var newarray = [];
							i=1;
							$('#dates_list2 .list2').each(function(){
								var retval = {};
								//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
								retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
								retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

								newarray.push(retval);
								console.log("kkk"+retval);
								i++;

							});

						console.log('new',newarray);

						 $.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img1.php',
							data:JSON.stringify(newarray),
							contentType: 'application/json; charset=utf-8',
									
							dataType: 'json',
							success: function (data) {
								data2= '{ "question" : "","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","mat_question" : "' + encodeURIComponent(tinymce.get('mat_question').getContent())+ '","compquestion" : "","mat_que_paragraph" : "' + encodeURIComponent(tinymce.get('mat_que_paragraph').getContent())+ '","mat_que_footer" : "' + encodeURIComponent(tinymce.get('mat_que_footer').getContent())+ '"}';

								 $.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',		
								dataType: 'json',
								
								success: function (result,xhr) {
										console.log("reply");
										
											
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&option5='+escape($('#option5').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=1&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
										
									},
									error: function(e){	

									console.log("ERROR: ", e);
								}
							});
									
							},
								error: function(e){	

							console.log("ERROR: ", e);
							}
						}); 
					}else{
						if(qtype=='7'){
							data2 = '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}else if(qtype=='8'){
							data2 ='{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "","option2" : "","option3" : "","option4" : "","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}else{
							data2=  '{ "question" : "' + encodeURIComponent(tinymce.get('question').getContent())+ '","option1" : "' + encodeURIComponent(tinymce.get('option1').getContent())+ '","option2" : "' + encodeURIComponent(tinymce.get('option2').getContent())+ '","option3" : "' + encodeURIComponent(tinymce.get('option3').getContent())+ '","option4" : "' + encodeURIComponent(tinymce.get('option4').getContent())+ '","explanation" : "' + encodeURIComponent(tinymce.get('explanation').getContent())+ '","compquestion" : "","mat_question" : "","mat_question" : "","mat_que_paragraph" : "","mat_que_footer" : ""}';
						}

						$.ajax({
							type: 'POST',
							url: '<?php echo SECURE_PATH;?>dashboard_v1/img.php',
							//url: 'http://107.178.223.50/qsbank/dashboard_v1/img.php',
							//url: 'http://neetjeeguru.com/dashboard_v1/img.php',
							data: data2,
							contentType: 'application/json; charset=utf-8',		
							dataType: 'json',
							
							success: function (result,xhr) {
									console.log("reply");
									setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateForm=1&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&qtype='+$('#qtype').val()+'&ppaper='+$('#ppaper').val()+'&qset='+$('#qset').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&expla_vlink='+$('#expla_vlink').val()+'&question='+escape($('#question').val())+'&option1='+escape($('#option1').val())+'&option2='+escape($('#option2').val())+'&option3='+escape($('#option3').val())+'&option4='+escape($('#option4').val())+'&answer='+$('#correctanswer').val()+'&complexity='+$('#complexity').val()+'&timeduration='+$('#timeduration').val()+'&inputquestion='+$('#inputquestion').val()+'&usageset='+$('#usageset').val()+'&question_theory='+$('#question_theory').val()+'&list1type='+$('#list1type').val()+'&list2type='+$('#list2type').val()+'&conttype='+$('#conttype').val()+'&conttypeids='+$('#conttypenos').val()+'&version=1&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>');
									
							},
								error: function(e){	

								console.log("ERROR: ", e);
							}
						});
						
					}

				}
				

					</script>
					
				
				<?php
				
                    if(isset($_POST['vstatus1'])){ if($_POST['vstatus1']=='1'){
					?>
						<a data-toggle="modal" data-target="#exampleModal5"  class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Delete and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','skipnext=1&id=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Skip and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand1();"> Save and Next</a>
						
					<?php
				}else if($_POST['vstatus1']=='2'){
					$sqll4=$database->query("select * from question_issues where estatus='1' and resolve_issue='1' and question_id='".$_REQUEST['editform']."'");
					$rowll4=mysqli_num_rows($sqll4);

					$sqll41=$database->query("select * from question_issues where estatus='1'  and question_id='".$_REQUEST['editform']."'");
					$rowll41=mysqli_num_rows($sqll41);
					if($rowll41==$rowll4){
						?>
							<a data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-danger text-uppercase mr-5 my-1 col-md-2 text-white" onClick="setState('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_POST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>')"> Report</a>
							<a data-toggle="modal" data-target="#exampleModal5"  class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Delete and Next</a>
							<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','skipnext=1&id=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Skip and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand1();">Save and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand();"> Verify and Next</a>
						<?php
					}else{
					?>
						<!--<a data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-danger text-uppercase mr-5 my-1 col-md-2 text-white" onClick="setState('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>')"> Rejected</a>-->
						<a data-toggle="modal" data-target="#exampleModal5"  class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Delete and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','skipnext=1&id=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Skip and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand1();">Save and Next</a>
					<?php
					}
				}else{
					?>
						<!-- <a data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-danger text-uppercase mr-5 my-1 col-md-2 text-white" onClick="setState('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_POST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>')" > Report</a>-->
						 <a data-toggle="modal" data-target="#exampleModal5"  class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_POST['search'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_REQUEST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Delete and Next</a>
						<!--<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand1();"> Save and Next</a>-->
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','skipnext=1&id=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Skip and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand();"> Verify and Next</a>
					<?php } ?>
				<?php
				}  else{  ?> 
					<!-- <a data-toggle="modal" data-target="#exampleModalCenter" class="btn btn-danger text-uppercase mr-5 my-1 col-md-2 text-white" onClick="setState('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_POST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['isstype'])) { echo '&isstype='.$_POST['isstype']; } ?><?php if(isset($_POST['issueids'])) { echo '&issueids='.$_POST['issueids']; } ?>')"> Report</a>-->
						<a data-toggle="modal" data-target="#exampleModal5"  class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_POST['question_id'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Delete and Next</a>
						<!--<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand1();"> Save and Next</a>-->
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','skipnext=1&id=<?php echo $_REQUEST['editform'];?>&question_id=<?php echo $_REQUEST['editform'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')"> Skip and Next</a>
						<a class="btn btn-primary text-uppercase px-5 my-1 col-md-3 text-white" onClick="rand();"> Verify and Next</a>
					<?php } ?>
					</div>

	<?php
		unset($_SESSION['error']);
		unset($_SESSION['question']);
		unset($_SESSION['option1']);
		unset($_SESSION['option2']);
		unset($_SESSION['option3']);
		unset($_SESSION['option4']);
		unset($_SESSION['explanation']); 
		unset($_SESSION['compquestion']);
		 unset($_SESSION['mat_que_paragraph']); 
		unset($_SESSION['mat_que_footer']);
		unset($_SESSION['data5']);
		unset($_SESSION['data4']);
	}
	

	?>

<?php
if(isset($_REQUEST['skipnext'])){
	$id=$_REQUEST['id'];
	$sello=$database->query("select * from users where username='".$session->username."'");
	$rowese=mysqli_fetch_array($sello);
	if(isset($_REQUEST['status'])){
		if($_REQUEST['status']!=''){
			$status=" AND vstatus1='".$_REQUEST['status']."'";
		}else{
			$status='';
		}
	}
	
	$cond="  AND subject IN(".$rowese['subject'].") AND chapter in (".trim($rowese['chapter'],",").")";
	$sellr1=$database->query("select id from createquestion where estatus='1'".$cond.$status." and id!='".$id."' and id<'".$id."' and ppaper!=1 order by id desc limit 0,1");
	$rowcountr1=mysqli_fetch_array($sellr1);
	if($rowcountr1['id']!='' && $_REQUEST['search']==''){
	?>
		<script type="text/javascript">
			 $('#adminForm').show();
		
			$('#datafill').hide();
			setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($_REQUEST['page'])){echo $_REQUEST['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['status'])) { echo '&status='.$_REQUEST['status']; } ?><?php if(isset($_REQUEST['search'])) { echo '&search='.$_REQUEST['search']; } ?><?php if(isset($_REQUEST['isstype'])) { echo '&isstype='.$_REQUEST['isstype']; } ?><?php if(isset($_REQUEST['issueids'])) { echo '&issueids='.$_REQUEST['issueids']; } ?>');
			
			
		</script>
	<?php
	}else{
			
		?>
		<script type="text/javascript">
			$('#adminForm').show();
			$('#datafill').show();
			
			setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
			setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($_REQUEST['page'])){echo $_REQUEST['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['status'])) { echo '&status='.$_REQUEST['status']; } ?><?php if(isset($_REQUEST['search'])) { echo '&search='.$_REQUEST['search']; } ?><?php if(isset($_REQUEST['isstype'])) { echo '&isstype='.$_REQUEST['isstype']; } ?><?php if(isset($_REQUEST['issueids'])) { echo '&issueids='.$_REQUEST['issueids']; } ?>');
			
			
		</script>
	<?php
	}
								
}
if(isset($_REQUEST['rowReject'])){

	$sell=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
	$rowl=mysqli_fetch_array($sell);
	if($rowl['vstatus1']=='0'){
		$status="Review Pending";
	}else if($rowl['vstatus1']=='1'){
		$status="Verified";
	}else if($rowl['vstatus1']=='2'){
		$status="Rejected";
	}else{
		$status="";
	}
	
	
	$count3=1;
	?>
		<div class="questionreport mt-3" id="tabs" >
			
			 <nav>
				<div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
					<a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Overall Issues</a>
					<a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Question Logs</a>
					<a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Version</a>
				</div>
			</nav>

            <div class="tab-content" id="nav-tabContent">
				<div class="tab-pane fade show active py-3" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
					<div id="issuediv">
						
						<div class="d-flex justify-content-between align-items-center my-4">
							<div class="question-idname">
								<h6>Question Id: <?php echo $_REQUEST['question_id']; ?></h6>
							</div>
							<div class="status-name">
								<h6>Status:<?php echo $status; ?> </h6>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="table-responsive">
									<table class="table table-bordered">
									  <thead>
										<tr>
										  <th scope="col">Issue ID</th>
										  <th scope="col">Issue Section</th>
										  <th scope="col">Issue Type</th>
										  <th scope="col">Remarks</th>
										  <th scope="col">Status</th>
										</tr>
									  </thead>
									  <tbody>
									  	<?php
										$j=1;
									  	$sql=$database->query("select * from question_issues where estatus='1' and question_id='".$_REQUEST['question_id']."'");
										$rcount=mysqli_num_rows($sql);
										while($row=mysqli_fetch_array($sql)){
											if($row['resolve_issue']=='1'){
												$status="Resolved";
											}else{
												$status="Pending";
											}
										?>
											<tr>
												<td><?php echo $row['issue_id']; ?></td>
												<td><?php echo $database->get_name('issue_section','id',$row['issue_section'],'issue_section'); ?></td>
												<td><?php echo $database->get_name('issue_type','id',$row['issue_type'],'issue_type'); ?></td>
												<td><?php echo $row['vremarks']; ?></td>
												<td><?php echo $status; ?></td>
												<?php
												if($row['resolve_issue']=='0'){
												?>
													<td><a class="btn btn-sm btn-primary text-white" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedatava2=1&question_id=<?php echo $_REQUEST['question_id']; ?>&issue=<?php echo $row['id']; ?>&page=<?php if(isset($_REQUEST['page'])){echo $_REQUEST['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['editform'])) { echo '&editform='.$_REQUEST['editform']; } ?>');" >Update</a></td>
												<?php
												} 
												?>
											</tr>
										<?php $j++; } ?>
									  </tbody>
									</table>
								</div>
							</div>
						</div>
						
						<?php
						//if($rowl['vstatus1']!='2'){
						?>
						<div class="row">
							
							<div class="col-md-12">
								<div id="issuelist">
									<?php
									if(isset($_REQUEST['issuedata']))
									{
										$posdata1 = explode('^',rtrim($_REQUEST['issuedata']));
										$m=1;
										foreach($posdata1 as $rowpost2)
										{
											if(strlen($rowpost2)>0)
											{
												$rowpost = explode('_',$rowpost2);
											?>
												<div class="ilist" id="ilist<?php echo $count3;?>">
													<form class="form">
														<div class="row">
															<div class="col-md-9 col-sm-12 form-group">
																<label for="inputReason">Select Issue With Section</label>
																<select id="issue_section" class="form-control issue_section">
																	<option value=''>-Select-</option>
																	<?php
																	$sel=$database->query("select * from issue_section where estatus='1'");
																	while($data=mysqli_fetch_array($sel)){
																		?>
																		<option value="<?php echo $data['id'];?>" <?php if(isset($rowpost[0])) { if($rowpost[0]==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_section'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['issue_section'.$count3])){ echo $_SESSION['error']['issue_section'.$count3];}?></span>
															</div>
														</div>
														<div class="row">
															<div class="col-md-9 col-sm-12 form-group">
																<label for="inputReason">Select Issue Type</label>
																<select id="issue_type" class="form-control issue_type">
																	<option value=''>-Select-</option>
																	<?php
																	$sel=$database->query("select * from issue_type where estatus='1'");
																	while($data=mysqli_fetch_array($sel)){
																		?>
																		<option value="<?php echo $data['id'];?>" <?php if(isset($rowpost[1])) { if($rowpost[1]==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_type'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['issue_type'.$count3])){ echo $_SESSION['error']['issue_type'.$count3];}?></span>
															</div>
														</div>
														<div class="row">
															<div class="col-md-9 col-sm-12 form-group">
																<label for="inputReason">Are you resolving the issue?</label>
																<div class="custom-control custom-radio custom-control-inline">
																  <input type="radio" id="resolve_issue1<?php echo $count3; ?>" name="customRadioInline1<?php echo $count3; ?>" value="1" class="custom-control-input resolve_issue" onclick="$('.resolve_issue').val('1')" <?php if(isset($rowpost[2])){ if($rowpost[2] == '1'){ echo 'checked="checked"';}}else{ echo '';}?> >
																  <label class="custom-control-label" for="resolve_issue1<?php echo $count3; ?>">Yes</label>
																</div>
																<div class="custom-control custom-radio custom-control-inline">
																  <input type="radio" id="resolve_issue2<?php echo $count3; ?>" name="customRadioInline1<?php echo $count3; ?>"  value="0" class="custom-control-input resolve_issue" onclick="$('.resolve_issue').val('0')" <?php if(isset($rowpost[2])){ if($rowpost[2] == '0'){ echo 'checked="checked"';}}else{ echo 'checked';}?> >
																  <label class="custom-control-label" for="resolve_issue2<?php echo $count3; ?>">No</label>
																</div>
																<input type="hidden" class="resolve_issue rissue"  name="resolve_issue" id="resolve_issue"  value="<?php if(isset($rowpost[2])) { echo $rowpost[2]; }else{ echo '0'; } ?>" >
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['resolve_issue'.$count3])){ echo $_SESSION['error']['resolve_issue'.$count3];}?></span>
															</div>
														</div>
														<div class="row">
															<div class="col-md-9 col-sm-12 form-group">
																<label for="inputReason">Remarks</label>
																<textarea type="text" id="vremarks" rows="4" class="form-control vremarks"><?php if(isset($rowpost[3])) { echo $rowpost[3]; }else{ } ?></textarea>
																<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['vremarks'.$count3])){ echo $_SESSION['error']['vremarks'.$count3];}?></span>
															</div>
															<div class="col-md-3 form-group pt-5">
															<?php
															if($count3 == 1){
															?>
																<a class="btn btn-success text-white" onclick="addissueList()"><i class="fa fa-plus"></i></a>
															<?php
															}
															else{
															?>
																<a class="btn btn-success text-white" onclick="addissueList()"><i class="fa fa-plus"></i></a>
																<a class="btn btn-danger text-white" onclick="removeissueList('<?php echo $count3; ?>')"><i class="fa fa-minus "></i></a>
															<?php
															}
															?>
														</div>
														</div>
													</form>
												</div>
												<?php
												$count3++;
												}
												
											}
											?>
												<input type="hidden" id="issue_llist"  value="<?php echo  $count3;?>"/>
											<?php
										
									}else{
									?>
										<div class="ilist" id="ilist">
											<form class="form">
												<div class="row">
													<div class="col-md-9 col-sm-12 form-group">
														<label for="inputReason">Select Issue With Section</label>
														<select id="issue_section" class="form-control issue_section">
															<option value=''>-Select-</option>
															<?php
															$sel=$database->query("select * from issue_section where estatus='1'");
															while($data=mysqli_fetch_array($sel)){
																?>
																<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['issue_section'])) { if($_REQUEST['issue_section']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_section'];?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-md-9 col-sm-12 form-group">
														<label for="inputReason">Select Issue Type</label>
														<select id="issue_type" class="form-control issue_type">
															<option value=''>-Select-</option>
															<?php
															$sel=$database->query("select * from issue_type where estatus='1'");
															while($data=mysqli_fetch_array($sel)){
																?>
																<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['issue_type'])) { if($_REQUEST['issue_type']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_type'];?></option>
															<?php
															}
															?>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-md-12 col-sm-12 form-group">
														<label for="inputReason">Are you resolving the issue?</label>
														<div class="custom-control custom-radio custom-control-inline">
														  <input type="radio" id="resolve_issue1" name="customRadioInline1" class="custom-control-input resolve_issue"  value="1" onclick="$('.resolve_issue').val('1')">
														  <label class="custom-control-label" for="resolve_issue1">Yes</label>
														</div>
														<div class="custom-control custom-radio custom-control-inline">
														  <input type="radio" id="resolve_issue2" name="customRadioInline1" class="custom-control-input resolve_issue" value="0" onclick="$('.resolve_issue').val('0')" checked >
														  <label class="custom-control-label" for="resolve_issue2">No</label>
														</div>
														<input type="hidden" class="resolve_issue rissue"  name="resolve_issue" id="resolve_issue" value="<?php if(isset($_POST['resolve_issue'])) { echo $_POST['resolve_issue']; }else{ echo '0'; } ?>" >
													</div>
												</div>
												<div class="row">
													<div class="col-md-9 col-sm-12 form-group">
														<label for="inputReason">Remarks</label>
														<textarea type="text" id="vremarks" rows="4" class="form-control vremarks"><?php if(isset($_POST['vremarks'])) { echo $_POST['vremarks']; }else{ } ?></textarea>
													</div>
													<div class="col-md-3 form-group pt-5">
														
														<?php
														if($count3 == 1){
														?>
															<a class="btn btn-success text-white" onclick="addissueList()"><i class="fa fa-plus"></i></a>
														<?php
														}
														else{
														?>
															<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
															<a class="btn btn-danger text-white" onclick="removeissueList('<?php echo $count3; ?>')"><i class="fa fa-minus "></i></a>
														<?php
														}
														?>
													</div>
												</div>
											</form>
											
										</div>
										
										<input type="hidden" id="issue_llist"  value="<?php echo  $count3;?>"/>
									
									<?php
									}
									?>
								</div>
								<div class="row">
									<div class="col-md-12 col-sm-12 form-group">
										<!-- <a  class="btn btn-primary text-white px-5" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedataval=1&question_id=<?php echo $_REQUEST['question_id']; ?>&issuedata='+issuedata()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');" >Submit</a> -->
										<a  class="btn btn-primary text-white px-5" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedataval=1&question_id=<?php echo $_REQUEST['question_id']; ?>&issuedata='+issuedata()+'&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');" >Submit</a>
									</div>
								</div>
							</div>
							<!-- <div class="col-md-4 text-right">
								<a href="#" class="btn btn-warning text-white" onclick="addissueList()" >Add Issues</a>
							</div> -->
						</div>
							
					<?php //} ?>		
						
						<!-- <div class="row">
							<div class="col-md-12 text-right border-top">
								<div class="modal-footer-buttons mt-5">
									<a type="button" class="btn btn-danger text-white" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedataval1=1&question_id=<?php echo $_REQUEST['question_id']; ?>&report=2&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');" >Report & Reject</a>
									<?php
									$sql=$database->query("select resolve_issue  from question_issues where estatus='1' and question_id='".$_REQUEST['question_id']."' and resolve_issue ='0' ");
									$rcount1=mysqli_num_rows($sql);
									if($rcount==0){ ?>
										<a type="button" class="btn btn-primary text-white" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedataval1=1&question_id=<?php echo $_REQUEST['question_id']; ?>&report=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');">Verify</a>
									<?php }else if($rcount1==0){ ?>
										<a type="button" class="btn btn-primary text-white" onclick="setState('issuediv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','issuedataval1=1&question_id=<?php echo $_REQUEST['question_id']; ?>&report=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');">Verify</a>
									<?php } ?>
								</div>
							</div>
						</div> -->
						<div class="modal-footer-buttons mt-5">
						</div>
					</div>
				</div>
			
			<div class="tab-pane fade py-3" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
				<div class="d-flex justify-content-between align-items-center my-4">
					<div class="question-idname">
						<h6>Question Id:<?php echo $_REQUEST['question_id']; ?> </h6>
					</div>
					
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<table class="table table-bordered">
								  <thead>
									<tr>
										<th scope="col">Sr.No.</th>
										<th scope="col">Date</th>
										<th scope="col">Username</th>
										<th scope="col">Message</th>
									</tr>
								  </thead>
								  <tbody>
									<?php
									$k=1;
									$sell=$database->query("select * from question_log where estatus='1' and question_id='".$_REQUEST['question_id']."'");
									while($rowl1=mysqli_fetch_array($sell)){
										if($rowl1['message']=='1'){
											$message="Question Inserted Successfully";
										}else if($rowl1['message']=='2'){
											$message="Question Updated Successfully";
										}else if($rowl1['message']=='3'){
											$message="Question Deleted Successfully";
										}else if($rowl1['message']=='4'){
											$message="Question Review Completed";
										}else{
										}
									?>
										<tr>
											<td><?php echo $k; ?></td>
											<td><?php echo date("d/m/Y H:i:s",$rowl1['timestamp']); ?></td>
											<td><?php echo $rowl['username']; ?></td>
											<td><?php echo $message; ?></td>
										</tr>
									<?php
									$k++;
									}
									?>

								  </tbody>
								</table>
							</div>
						</div>
				</div>  
			</div>
			<div class="tab-pane fade py-3" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
			    <form>
					<div class="d-flex justify-content-between align-items-left my-4">
						<div class="col-md-4 col-sm-12">
							<h6>Question Id:<?php echo $_REQUEST['question_id']; ?> </h6>
						</div>
						<div class="col-md-6 col-sm-12">
							<div class="form-group">
								<label for="exampleFormControlSelect1">Version</label>
								<?php
								$sql1=$database->query("select * from version_question where estatus='1' and question_id='".$_REQUEST['question_id']."'");
								$rcount1=mysqli_num_rows($sql1);

								?>
								<select class="form-control" id="version" onChange="setStateGet('versiondiv','<?php echo SECURE_PATH;?>dashboard_v1/process.php','versionval=1&question_id=<?php echo $_REQUEST['question_id']; ?>&version='+$('#version').val()+'')" >
									<?php 
									if($rcount1>0){
										for($i=1;$i<=$rcount1+1; $i++){ ?>
											<option value='V<?php echo  $i; ?>'  <?php if(isset($_REQUEST['version'])){ if($_REQUEST['version']='V<?php echo $i; ?>'){ echo 'selected'; } }?> >V<?php echo $i; ?></option>
									<?php } }else{ ?>
										<option value='V1'  <?php if(isset($_REQUEST['version'])){ if($_REQUEST['version']=='V1'){  echo 'selected'; } } ?> >V1</option>
									<?php } ?>
								</select>
							</div>
						</div>
						
					</div>
					<div id="versiondiv">
						<?php
						
						if($_REQUEST['version']=='V1'){
							$sello=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
							$rowcount1=mysqli_num_rows($sello);
						}else if($_REQUEST['version']=='V2'){
							$sello=$database->query("select * from version_question where estatus='1' and question_id='".$_REQUEST['question_id']."'");
							$rowcount1=mysqli_num_rows($sello);
						}else{
							$sello=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
							$rowcount1=mysqli_num_rows($sello);
						}
						if($rowcount1>0){
							$row=mysqli_fetch_array($sello);

							if($row['list1type']=='roman'){
								$list1type="upper-roman";
							}else if($row['list1type']=='alphabets'){
								$list1type="upper-alpha";
							}else if($row['list1type']=='numbers'){
								$list1type="decimal";
							}else{
								$list1type="upper-alpha";
							}
							if($row['list2type']=='roman'){
								$list2type="upper-roman";
							}else if($row['list2type']=='alphabets'){
								$list2type="upper-alpha";
							}else if($row['list2type']=='numbers'){
								$list2type="decimal";
							}else{
								$list2type="upper-alpha";
							}
							$row['class']=rtrim($row['class'],',');
							$row['chapter']=rtrim($row['chapter'],',');
							$row['topic']=rtrim($row['topic'],',');
							
							if($row['class']=='1'){
								$class='XI';
							}else if($row['class']=='2'){
								$class='XII';
							}else  if($row['class']=='1,2'){
								$class='XI,XII';
							}
							$exam ='';
							$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$row['exam'].")"); 
							while($rowexam=mysqli_fetch_array($sqlexam)){
								$exam .= $rowexam['chapter'].",";
							}
							$chapter ='';
							$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
							while($row2=mysqli_fetch_array($sql)){
								$chapter .= $row2['chapter'].",";
							}

								$topic ='';
								$k=1;
								$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
								while($row1=mysqli_fetch_array($zSql1)){
									$topic .= $row1['topic'].",";
									$k++;
								}
								$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
								while($row2=mysqli_fetch_array($zSql2)){
									$questiontype .= $row2['questiontype'].",";
									$k++;
								}
								?>
								<div class="container">
									<div class="row">
										<?php
										$row['exam']=rtrim($row['exam'],',');
										$row['class']=rtrim($row['class'],',');
										$row['chapter']=rtrim($row['chapter'],',');
										$row['topic']=rtrim($row['topic'],',');
										
										if($row['class']=='1'){
											$class='XI';
										}else if($row['class']=='2'){
											$class='XII';
										}else  if($row['class']=='1,2'){
											$class='XI,XII';
										}
										$exam ='';
										$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$row['exam'].")"); 
										while($rowexam=mysqli_fetch_array($sqlexam)){
											$exam.= $rowexam['exam'].",";
										}
										$chapter ='';
										$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
										while($row2=mysqli_fetch_array($sql)){
											$chapter .= $row2['chapter'].",";
										}

											$topic ='';
											$k=1;
											$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
											while($row1=mysqli_fetch_array($zSql1)){
												$topic .= $row1['topic'].",";
												$k++;
											}
											?>
											<div class="col-md-1">
												<h6>Class</h6>
												<p><?php echo $class; ?></p>
											</div>
											
											<div class="col-md-2">
												<h6>Exam</h6>
												<p><?php echo rtrim($exam,","); ?></p>
											</div>
											<div class="col-md-2">
												<h6>Subject</h6>
												<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
											</div>
											<div class="col-md-3">
												<h6>Chapter</h6>
												<p><?php echo rtrim($chapter,","); ?></p>
											</div>
											<div class="col-md-4">
												<h6>Topic</h6>
												<p><?php echo rtrim($topic,","); ?></p>
											</div>
										
										<?php
										if($row['qtype']=='5'){
											$type="Comprehension";
										?>
											<div class="col-md-6">
												<h6>Question Type</h6>
												<p><?php echo $type; ?></p>
											</div>
											<div class="col-md-12 mt-2 questionView">
												<h6>Explanation</h6>
												<div class="imagewrapper"><?php echo urldecode($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
											</div>
											<div class="col-md-12 mt-2 questionView">
												<h6>Question</h6>
												<div class="imagewrapper"><?php echo $row['question']; ?></div>
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A)</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option1']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option2']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option3']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option4']; ?>
													</div>
												</div>
											</div>
											<?php if($row['explanation']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation</h6>
													<div><?php echo $row['explanation']; ?></div>
														

												</div>
											<?php } ?>
											
											<div class="col-md-12 mt-2">
												<h6>Answer</h6>
												<p><?php echo rtrim($row['answer'],","); ?></p>
													

											</div>
										<?php
										}else if($row['qtype']=='8'){
											$type="Integer";
											?>
											<div class="col-md-6">
												<h6>Question Type</h6>
												<p><?php echo $type; ?></p>
											</div>
											<div class="col-md-12 mt-2 questionView">
												<h6>Question</h6>
												<div class="imagewrapper"><?php echo $row['question']; ?></div>
											</div>
											
											
											<div class="col-md-12 mt-2 questionView">
												<h6>answer</h6>
												<div><?php echo rtrim($row['answer'],","); ?></div>
											</div>
											<?php if($row['explanation']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation</h6>
													<div><?php echo urldecode($row['explanation']); ?></div>
														

												</div>
											<?php } ?>
										<?php
										}else if($row['qtype']=='3'){
											$type="Matching";
										
											$list1='';
											$list2='';
											$obj=json_decode($row['question'],true);
											
											?>
											<div class="col-md-6">
												<h6>Question Type</h6>
												<p><?php echo $type; ?></p>
											</div>
											
											<div class="col-md-12 mt-2 questionView">
												<h6>Question</h6>
												<div class="imagewrapper"><?php echo urldecode($row['mat_question']); ?></div>
												<br />
												<div class="questionlist-types ">
													<div>
													<h5>List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
															<?php
															foreach($obj as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														</ul>
													</div>
													<div class="pl-5">
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
															<?php
															foreach($obj as $qqlist1)
															{
																if(strlen($qqlist1['qlist2'])>0)
																{
																	echo '<li style="width:150px;">'.urldecode($qqlist1['qlist2']).'</li>';
																}
															}
															?>
														</ol>
													</div>
												</div>
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A) 						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option1']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option2']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option3']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option4']; ?>
													</div>
												</div>
											</div>
											<?php if($row['explanation']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation</h6>
													<div><?php echo urldecode($row['explanation']); ?></div>
														

												</div>
											<?php } ?>
											
											<div class="col-md-12 mt-2">
												<h6>Answer</h6>
												<p><?php echo rtrim($row['answer'],","); ?></p>
													

											</div>
										<?php
										}else if($row['qtype']=='9'){
											$type="Matrix";
											$list1='';
											$list2='';
											$obj=json_decode($row['question'],true);
											foreach($obj as $rowpost2)
											{
												if(strlen($rowpost2)>0)
												{
													$rowpost = explode("_",$rowpost2);
													$list1.=$rowpost[0]."_";
													$list2.=$rowpost[1]."_";
												}
											}
											$qlist1 = explode("_",$list1);
											$qlist2 = explode("_",$list2);
											?>
											<div class="col-md-6">
												<h6>Question Type</h6>
												<p><?php echo $type; ?></p>
											</div>
											
											<div class="col-md-12 mt-2 questionView">
												<h6>Question</h6>
												
													<div class="questionlist-types ">
														<div>
														<h5>List1</h5>
															<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
																<?php
																foreach($obj as $qqlist)
																{
																
																	if(strlen($qqlist['qlist1'])>0)
																	{
																		echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
																	}
																}
																?>
															
															</ul>
														</div>
														<div class="pl-5">
															<h5>List2</h5>
															<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
																<?php
																foreach($obj as $qqlist1)
																{
																
																	if(strlen($qqlist1['qlist2'])>0)
																	{
																		echo '<li style="width:150px;">'.urldecode($qqlist1['qlist2']).'</li>';
																	}
																}
																?>
															</ol>
														</div>
													</div>
												
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A) 						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option1']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option2']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option3']; ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo $row['option4']; ?>
													</div>
												</div>
											</div>
											<?php if($row['explanation']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation</h6>
													<div><?php echo urldecode($row['explanation']); ?></div>
														

												</div>
											<?php } ?>
											<?php if($row['expla_vlink']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation Video Link</h6>
													<div><?php echo $row['expla_vlink']; ?></div>
														

												</div>
											<?php } ?>
											<div class="col-md-12 mt-2">
												<h6>Answer</h6>
												<p><?php echo rtrim($row['answer'],","); ?></p>
													

											</div>
										<?php
										}else{
											$type="General";
										?>
											<div class="col-md-3">
												<h6>Question Type</h6>
												<p><?php echo $type; ?></p>
											</div>
											
											<div class="col-md-12 mt-2 questionView">
												<h6>Question</h6>
												<div class="imagewrapper"><?php echo urldecode($row['question']); ?></div>
												<div class="row">

													<div class="col-md-1 py-2 ">
														(A) 						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($row['option1']); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(B)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($row['option2']); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(C)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($row['option3']); ?>
													</div>

													<div class="col-md-1 py-2 ">
														(D)						</div>

													<div class="col-md-5 py-2 ">
														<?php echo urldecode($row['option4']); ?>
													</div>
												</div>
											</div>
											<?php if($row['explanation']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation</h6>
													<div><?php echo urldecode($row['explanation']); ?></div>
														

												</div>
											<?php } ?>
											<?php if($row['expla_vlink']!=''){ ?>
												<div class="col-md-12 mt-2">
													<h6>Explanation Video Link</h6>
													<div><?php echo $row['expla_vlink']; ?></div>
														

												</div>
											<?php } ?>
											<div class="col-md-12 mt-2">
												<h6>Answer</h6>
												<p><?php echo rtrim($row['answer'],","); ?></p>
													

											</div>
										<?php
										}
										?>
										<?php if($row['complexity']!='' && $row['complexity']!='0'){ ?>
											<div class="col-md-12 mt-2">
												<h6>Complexity</h6>
												<p><?php echo $database->get_name('complexity','id',$row['complexity'],'complexity'); ?></p>
													

											</div>
										<?php } ?>
										<?php if($row['timeduration']!=''){ ?>
											<div class="col-md-12 mt-2">
												<h6>Answering Time Duration</h6>
												<p><?php echo $row['timeduration']; ?></p>
													

											</div>
										<?php } ?>
										<?php if($row['inputquestion']!='' && $row['inputquestion']!='0'){
										$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
										while($row2=mysqli_fetch_array($zSql2)){
											$questiontype .= $row2['questiontype'].",";
											$k++;
										}
										?>
											<div class="col-md-12 mt-2">
												<h6>Type of Question</h6>
												<p><?php echo rtrim($questiontype,","); ?></p>
													

											</div>
										<?php } ?>
										<?php if($row['usageset']!='' && $row['usageset']!='0'){ ?>
											<div class="col-md-12 mt-2">
												<h6>Usage Set</h6>
												<p><?php echo $database->get_name('question_useset','id',$row['usageset'],'usageset'); ?></p>
													

											</div>
										<?php } ?>
										<?php if($row['question_theory']!='' && $row['question_theory']!='0'){ ?>
											<div class="col-md-12 mt-2">
												<h6>Question Theory</h6>
												<p><?php echo $database->get_name('question_theory','id',$row['question_theory'],'question_theory'); ?></p>
													

											</div>
										<?php } ?>
										
									</div>
								</div>
							<?php
							}
							?>
						</div>
						<div class="row">
							<div class="col-md-12 text-right border-top">
								<div class="modal-footer-buttons mt-5">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
									
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	
	<?php
	unset($_SESSION['error']);
}
if(isset($_REQUEST['rowReject1'])){

	$sell=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
	$rowl=mysqli_fetch_array($sell);
	if($rowl['vstatus1']=='0'){
		$status="Review Pending";
	}else if($rowl['vstatus1']=='1'){
		$status="Verified";
	}else if($rowl['vstatus1']=='2'){
		$status="Rejected";
	}else{
		$status="";
	}
	
	
	?>
	
			<div class="d-flex justify-content-between align-items-center my-4">
				<div class="question-idname">
					<h6>Question Id: <?php echo $_REQUEST['question_id']; ?></h6>
				</div>
				<div class="status-name">
					<h6>Status:<?php echo $status; ?> </h6>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-bordered">
						  <thead>
							<tr>
							  <th scope="col">Issue ID</th>
							  <th scope="col">Issue Section</th>
							  <th scope="col">Issue Type</th>
							  <th scope="col">Remarks</th>
							  <th scope="col">Status</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
							$j=1;
							$sql=$database->query("select * from question_issues where estatus='1' and question_id='".$_REQUEST['question_id']."'");
							$rcount=mysqli_num_rows($sql);
							while($row=mysqli_fetch_array($sql)){
								if($row['resolve_issue']=='1'){
									$status="Resolved";
								}else{
									$status="Pending";
								}
							?>
								<tr>
									<td><?php echo $row['issue_id']; ?></td>
									<td><?php echo $database->get_name('issue_section','id',$row['issue_section'],'issue_section'); ?></td>
									<td><?php echo $database->get_name('issue_type','id',$row['issue_type'],'issue_type'); ?></td>
									<td><?php echo $row['vremarks']; ?></td>
									<td><?php echo $status; ?></td>
								</tr>
							<?php $j++; } ?>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
			
	
	<?php
	
}
if(isset($_REQUEST['validateForm1'])){
	if($_REQUEST['rowdata']!=''){
		$result=$database->query('update createquestion set issue_section="'.$_REQUEST['issue_section'].'",issue_type="'.$_REQUEST['issue_type'].'",resolve_issue="'.$_REQUEST['resolve_issue'].'",vremarks="'.$_REQUEST['vremarks'].'",estatus="2",vstatus1="2",vtimestamp1="'.time().'" where id="'.$_REQUEST['rowdata'].'"');
		
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Question Rejected Successfully!</div>
			
			<script type="text/javascript">
				$('#tabledata').show();
				$('#datafill').show();
				
			//	alert("Data Updated Successfully");
				setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
		</script>
	<?php
	}
}

//Delete createQuestion
/*if(isset($_GET['rowDelete'])){
	$database->query("update  createquestion set estatus='0',dtimestamp='".time()."',dusername='".$_SESSION['username']."' WHERE id = '".$_GET['rowDelete']."'");
	$database->query("insert question_log set question_id='".$_GET['rowDelete']."',username='".$_SESSION['username']."',message='3',estatus='1',timestamp='".time()."'");
	?>
    <div class="alert alert-success"> Question deleted successfully!</div>

  <script type="text/javascript">
animateForm('<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>
<?php
if(isset($_REQUEST['rowDeletedata'])){
	$post = $session->cleanInput($_POST);
	$id=$_REQUEST['editform'];
	$result=$database->query("update createquestion set estatus='0',dtimestamp='".time()."',dusername='".$_SESSION['username']."'  where estatus='1' and id='".$_REQUEST['editform']."'");
	$database->query("insert question_log set question_id='".$_REQUEST['rowDeletedata']."',username='".$_SESSION['username']."',message='3',estatus='1',timestamp='".time()."'");
	if($result){
		$sello=$database->query("select * from users where username='".$session->username."'");
		$rowese=mysqli_fetch_array($sello);
		$cond="  AND subject IN(".$rowese['subject'].") AND chapter in (".trim($rowese['chapter'],",").")";
		if($_REQUEST['search']==''){
		$sellr1=$database->query("select id from createquestion where estatus='1'".$cond."  and id!='".$id."'  and id < '".$id."' and ppaper!=1 order by timestamp desc limit 0,1");
		$rowcountr2=mysqli_fetch_array($sellr1);

			if($rowcountr2['id']!=''){
				?>
				<script type="text/javascript">
					$("#rowdeletetoast").toast({
					 delay: 2000,
						autohide: true,

					});
					$('#rowdeletetoast').toast('show');
					setTimeout(function(){
						$('#adminForm').show();
						$('#datafill').hide();
						setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr2['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
					},2000);
					
				</script>
			<?php
			}else{
				?>
				<script type="text/javascript">
													
					$("#rowdeletetoast").toast({
					 delay: 2000,
						autohide: true,

					});
					$('#rowdeletetoast').toast('show');
					setTimeout(function(){
						$('#adminForm').show();
						$('#datafill').show();
						setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
					},2000);
					
				</script>
			<?php
			}

		}else{
				?>
				<script type="text/javascript">
													
					$("#rowdeletetoast").toast({
					 delay: 2000,
						autohide: true,

					});
					$('#rowdeletetoast').toast('show');
					setTimeout(function(){
						$('#adminForm').show();
						$('#datafill').show();
						setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
					},2000);
					
				</script>
			<?php
			}
		}else{
			?>
			<script type="text/javascript">
				$("#failedtoast").toast({
					 delay: 2000,
					autohide: true,

				});
				$('#failedtoast').toast('show');
				setTimeout(function(){ 
					$('#adminForm').show();
					$('#datafill').show();
					setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
					setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $_REQUEST['editform']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
				
				},2000);
			</script>
			<?php
			
		}
	
}*/
if(isset($_REQUEST['rowDelete'])){
	?>
	<form>
		<div class="form-group">
		<label for="recipient-name" class="col-form-label">Question Id:</label>
		<?php echo $_REQUEST['rowDelete']; ?>
	  </div>
	  <div class="row">
		<div class="col-lg-10">
			<div class="form-group row">
				<label class="col-sm-5 col-form-label">Reason For Question Deletion</label>
				<div class="col-sm-7">
  
					<select id="dremarks" class="form-control selectpickerd" name="dremarks" multiple >
						<?php
						$sql=$database->query("select * from delete_qtype where estatus='1'");
						while($row=mysqli_fetch_array($sql)){
						?>
							<option value='<?php echo $row['id'];?>' <?php if(isset($_POST['dremarks'])) { if(in_array($row['id'], explode(",",$_POST['dremarks']))) { echo 'selected="selected"'; } } ?>><?php echo $row['delete_qtype'];?></option>
						<?php
						}
						?>
						<!--<option value='Typing Mistakes'>Typing Mistakes</option>
						<option value='Question inappropriate'>Question inappropriate</option>
						<option value='Answer Options are not correct'>Answer Options are not correct</option>
						<option value='Explanation is not Correct,'>Explanation is not Correct</option>
						<option value='Explanation is Required'>Explanation is Required</option>-->
					</select>
					<span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['dremarks'])){ echo $_SESSION['error']['dremarks'];}?></span>
				</div>
			</div>
		</div>
	  </div>
	   <div class="row">
			<div class="col-lg-10">
				<div class="form-group row">
					<label class="col-sm-5 col-form-label">Remarks <span class="text-danger">*</span></label>
					<div class="col-sm-7">
	  
						<textarea id="dremarks1" rows="3" class="form-control"  name="dremarks1" ><?php if(isset($_REQUEST['dremarks1'])){ if($_REQUEST['dremarks1']!=''){ echo $_REQUEST['dremarks1'];}} ?></textarea>
						<span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['dremarks1'])){ echo $_SESSION['error']['dremarks1'];}?></span>
					</div>
				</div>
			</div>
		  </div>
	  
	 <div class="center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateFormdelete=1&dremarks='+$('#dremarks').val()+'&dremarks1='+$('#dremarks1').val()+'&rowDelete=<?php echo $_REQUEST['rowDelete'];?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status1'])){ echo '&status1='.$_REQUEST['status1'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>')">Delete</a>
		</div>
    </form>	

   <?php
	 unset($_SESSION['error']);
	}
   	if(isset($_REQUEST['validateFormdelete'])){
		$post = $session->cleanInput($_POST);
		 $field = 'dremarks';
		if(!$post['dremarks'] || strlen(trim($post['dremarks'])) == 0){
		  $_SESSION['error'][$field] = "* Delete Type cannot be empty";
		}
		 $field = 'dremarks1';
		if(!$post['dremarks1'] || strlen(trim($post['dremarks1'])) == 0){
		  $_SESSION['error'][$field] = "* Remarks cannot be empty";
		}
		if(count($_SESSION['error']) > 0 ){
		?>
		<script type="text/javascript">
		 $('#exampleModal5').modal('show');
			setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status1'])){ echo '&status1='.$_REQUEST['status1'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
			
		  setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDelete=<?php echo $_REQUEST['rowDelete'];?>&dremarks=<?php echo $_REQUEST['dremarks'];?>&dremarks1=<?php echo $_REQUEST['dremarks1'];?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status1'])){ echo '&status1='.$_REQUEST['status1'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
		</script>
	  <?php
		}
		else{
			
			$database->query("update  createquestion set estatus='0',dremarks='".$post['dremarks']."',dremarks1='".$post['dremarks1']."',dtimestamp='".time()."',dusername='".$_SESSION['username']."' WHERE id = '".$_REQUEST['rowDelete']."'");
			$database->query("insert question_log set question_id='".$_REQUEST['rowDelete']."',username='".$_SESSION['username']."',message='3',module='dashboard',estatus='1',timestamp='".time()."'");
				?>
			   
			  <script type="text/javascript">
				 $('#exampleModal5').modal('hide');
				alert("Question Deleted  Successfully!");
				setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status1'])){ echo '&status1='.$_REQUEST['status1'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
			
			</script>
		<?php
		}
		
	}
	?>
<?php
	if(isset($_REQUEST['rowDeletedata'])){
		?>
		<form>
			<div class="form-group">
			<label for="recipient-name" class="col-form-label">Question Id:</label>
			<?php echo $_REQUEST['rowDeletedata']; ?>
		  </div>
		  <div class="row">
			<div class="col-lg-10">
				<div class="form-group row">
					<label class="col-sm-5 col-form-label">Reason For Question Deletion</label>
					<div class="col-sm-7">
	  
						<select id="dremarks" class="form-control selectpickerd" name="dremarks" multiple>
							<!--<option value='Typing Mistakes'>Typing Mistakes</option>
							<option value='Question inappropriate'>Question inappropriate</option>
							<option value='Answer Options are not correct'>Answer Options are not correct</option>
							<option value='Explanation is not Correct,'>Explanation is not Correct</option>
							<option value='Explanation is Required'>Explanation is Required</option>-->
							<?php
							$sql=$database->query("select * from delete_qtype where estatus='1'");
							while($row=mysqli_fetch_array($sql)){
							?>
								<option value='<?php echo $row['id'];?>' <?php if(isset($_POST['dremarks'])) { if(in_array($row['id'], explode(",",$_POST['dremarks']))) { echo 'selected="selected"'; } } ?>><?php echo $row['delete_qtype'];?></option>
							<?php
							}
							?>
						</select>
						<span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['dremarks'])){ echo $_SESSION['error']['dremarks'];}?></span>
					</div>
				</div>
			</div>
		  </div>
		  <div class="row">
			<div class="col-lg-10">
				<div class="form-group row">
					<label class="col-sm-5 col-form-label">Remarks <span class="text-danger">*</span></label>
					<div class="col-sm-7">
	  
						<textarea id="dremarks1" rows="3" class="form-control"  name="dremarks1" ><?php if(isset($_REQUEST['dremarks1'])){ if($_REQUEST['dremarks1']!=''){ echo $_REQUEST['dremarks1'];}} ?></textarea>
						<span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['dremarks1'])){ echo $_SESSION['error']['dremarks1'];}?></span>
					</div>
				</div>
			</div>
		  </div>
	  
		 <div class="center">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			<a class="btn btn-primary text-white" onClick="setState('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','validateFormdelete1=1&dremarks='+$('#dremarks').val()+'&dremarks1='+$('#dremarks1').val()+'&rowDeletedata=<?php echo $_REQUEST['rowDeletedata'];?>&page=<?php if(isset($_POST['page'])){echo $_POST['page'];}else{ echo '1';}?><?php if(isset($_POST['class1'])) { echo '&class1='.$_POST['class1']; } ?><?php if(isset($_POST['exam1'])) { echo '&exam1='.$_POST['exam1']; } ?><?php if(isset($_POST['subject1'])) { echo '&subject1='.$_POST['subject1']; } ?><?php if(isset($_POST['exam1'])) { echo '&exam1='.$_POST['exam1']; } ?><?php if(isset($_POST['chapter1'])) { echo '&chapter1='.$_POST['chapter1']; } ?><?php if(isset($_POST['topic1'])) { echo '&topic1='.$_POST['topic1']; } ?><?php if(isset($_POST['question_id'])) { echo '&question_id='.$_POST['question_id']; } ?><?php if(isset($_POST['fdate1'])) { echo '&fdate1='.$_POST['fdate1']; } ?><?php if(isset($_POST['tdate1'])) { echo '&tdate1='.$_POST['tdate1']; } ?><?php if(isset($_POST['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?><?php if(isset($_POST['usageset1'])) { echo '&usageset1='.$_POST['usageset1']; } ?><?php if(isset($_POST['complexity1'])) { echo '&complexity1='.$_POST['complexity1']; } ?><?php if(isset($_POST['questiontype'])) { echo '&questiontype='.$_POST['questiontype']; } ?>')">Delete</a>
			</div>
		</form>	
		<?php
		unset($_SESSION['error']);
	}
	if(isset($_REQUEST['validateFormdelete1'])){
		$post = $session->cleanInput($_POST);
		$id=$_REQUEST['rowDeletedata'];
		 $field = 'dremarks';
		if(!$post['dremarks'] || strlen(trim($post['dremarks'])) == 0){
		  $_SESSION['error'][$field] = "* Delete Type cannot be empty";
		}
		 $field = 'dremarks1';
		if(!$post['dremarks1'] || strlen(trim($post['dremarks1'])) == 0){
		  $_SESSION['error'][$field] = "* Remarks cannot be empty";
		}
		if(count($_SESSION['error']) > 0 ){
		?>
		<script type="text/javascript">
		 $('#exampleModal5').modal('show');
		setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $_REQUEST['rowDeletedata'];?>&dremarks=<?php echo $_REQUEST['dremarks'];?>&dremarks1=<?php echo $_REQUEST['dremarks1'];?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['search'])){ echo '&search='.$_REQUEST['search'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
			
		setState('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDeletedata=<?php echo $_REQUEST['rowDeletedata'];?>&dremarks=<?php echo $_REQUEST['dremarks'];?>&dremarks1=<?php echo $_REQUEST['dremarks1'];?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam1'])){ echo '&exam1='.$_REQUEST['exam1'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['search'])){ echo '&search='.$_REQUEST['search'];}?><?php if(isset($_REQUEST['complexity1'])){ echo '&complexity1='.$_REQUEST['complexity1'];}?><?php if(isset($_REQUEST['usageset1'])){ echo '&usageset1='.$_REQUEST['usageset1'];}?><?php if(isset($_REQUEST['questiontype1'])){ echo '&questiontype1='.$_REQUEST['questiontype1'];}?><?php if(isset($_REQUEST['fdate1'])){ echo '&fdate1='.$_REQUEST['fdate1'];}?><?php if(isset($_REQUEST['tdate1'])){ echo '&tdate1='.$_REQUEST['tdate1'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
		</script>
	  <?php
		}
		else{
			
			$database->query("update  createquestion set estatus='0',dremarks='".$post['dremarks']."',dremarks1='".$post['dremarks1']."',dtimestamp='".time()."',dusername='".$_SESSION['username']."' WHERE id = '".$_REQUEST['rowDeletedata']."'");
			$database->query("insert question_log set question_id='".$_REQUEST['rowDeletedata']."',username='".$_SESSION['username']."',message='3',module='dashboard',estatus='1',timestamp='".time()."'");
				
			 if(isset($_REQUEST['class1'])){
			if($_REQUEST['class1']!=""){
				$class1=" AND class LIKE '%".$_REQUEST['class1']."%'";
			} else {
				$class1="";
			}
		}else{
			$class1="";
		}
		if(isset($_REQUEST['subject1'])){
			if($_REQUEST['subject1']!=""){
				$subject1=" AND subject='".$_REQUEST['subject1']."'";
			} else {
				$subject1="";
			}
		}else{
			$subject1="";
		}
		if(isset($_REQUEST['chapter1'])){
			if($_REQUEST['chapter1']!=""){
				$chapter1=" AND FIND_IN_SET(".$_REQUEST['chapter1'].",chapter)";
			} else {
				$chapter1="";
			}
		}else{
			$chapter1="";
		}

		if(isset($_REQUEST['topic1'])){
			if($_REQUEST['topic1']!=""){
				$topic1=" AND FIND_IN_SET(".$_REQUEST['topic1'].",topic) ";
			} else {
				$topic1="";
			}
		}else{
			$topic1="";
		}
		
		if(isset($_REQUEST['complexity1'])){
			if($_REQUEST['complexity1']!=""){
				$complexity1=" AND complexity ='".$_REQUEST['complexity1']."'";
			} else {
				$complexity1="";
			}
		}else{
			$complexity1="";
		}
		if(isset($_REQUEST['usageset1'])){
			if($_REQUEST['usageset1']!=""){
				$usageset1=" AND usageset ='".$_REQUEST['usageset1']."'";
			} else {
				$usageset1="";
			}
		}else{
			$usageset1="";
		}
		if(isset($_REQUEST['questiontype1'])){
			if($_REQUEST['questiontype1']!=""){
				$questiontype1=" AND inputquestion  In (".$_REQUEST['questiontype1'].")";
			} else {
				$questiontype1="";
			}
		}else{
			$questiontype1="";
		}
		if(isset($_REQUEST['exam1'])){
			if($_REQUEST['exam1']!=""){
				$exam1=" AND exam LIKE '%".$_REQUEST['exam']."%'";
			} else {
				$exam1="";
			}
		}else{
			$exam1="";
		}
		if(isset($_REQUEST['search'])){
			if($_REQUEST['search']!=""){
				$search=" AND id='".$_REQUEST['search']."'";
			} else {
				$search="";
			}
		}else{
			$search="";
		}
		
		if(isset($_REQUEST['status'])){
			if($_REQUEST['status1']!=""){
				$status=" AND vstatus1='".$_REQUEST['status']."'";
			} else {
				$status="";
			}
		}else{
			$status="";
		}
		$sello=$database->query("select * from users where username='".$session->username."'");
		$rowese=mysqli_fetch_array($sello);
		$cond1 = $dt.$class1.$subject1.$chapter1.$topic1.$exam1.$status.$complexity1.$usageset1.$questiontype1.$search;
		$cond=" AND class in (".trim($rowese['class'],",").")  AND subject IN(".$rowese['subject'].") ";
		if($_REQUEST['search']==''){
			echo "select id from createquestion where estatus='1'".$cond.$cond1."  and id!='".$id."'  and id < '".$id."' and ppaper!=1 order by timestamp desc limit 0,1";
			$sellr1=$database->query("select id from createquestion where estatus='1'".$cond.$cond1."  and id!='".$id."'  and id < '".$id."' and ppaper!=1 order by timestamp desc limit 0,1");
		$rowcountr2=mysqli_fetch_array($sellr1);

			if($rowcountr2['id']!=''){
				?>
				<script type="text/javascript">
					 $('#exampleModal5').modal('hide');
					alert("Question Deleted Successfully");
						
					$('#adminForm').show();
						$('#datafill').hide();
						setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr2['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
					
				</script>
			<?php
			}else{
				?>
				<script type="text/javascript">
					 $('#exampleModal5').modal('hide');								
					alert("Question Deleted Successfully");
					$('#adminForm').show();
					$('#formSubmit').hide();
						$('#datafill').show();
						setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
					
					
				</script>
			<?php
			}

		
		}else{
			?>
			<script type="text/javascript">
				 $('#exampleModal5').modal('hide');
				alert("Question Deleted Successfully");
				$('#adminForm').show();
				$('#formSubmit').hide();
				$('#datafill').show();
				setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
				setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?>');
				
				
			</script>
			<?php
			
		}
	}
	}
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		if(isset($post['editform'])){
	  		$id = $post['editform'];

		}
		$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
		$rowquestion=mysqli_fetch_array($sqlquestion);
		if(isset($post['qtype'])){
			if($post['qtype']=='5'){
				
				if(!$_SESSION['compquestion']){

					$_SESSION['error']['compquestion'] = "*Please Enter Explanation";

				}
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				$abcd = 1;
				$list=$_SESSION['data4'];
				if(count($list)>0)
				{
					foreach ($list as $r)
					{
						if(strlen(trim($r['question'])) == 0 ){
						  $_SESSION['error']['question'.$abcd] = "Please Enter Question";
						}
						if(strlen(trim($r['option1'])) == 0 ){
						  $_SESSION['error']['option1'.$abcd] = "Please Enter Answer A";
						}
						if(strlen(trim($r['option2'])) == 0 ){
						  $_SESSION['error']['option2'.$abcd] = "Please Enter Answer B";
						}
						if(strlen(trim($r['option3'])) == 0 ){
						  $_SESSION['error']['option3'.$abcd] = "Please Enter Answer C";
						}
						if(strlen(trim($r['option4'])) == 0 ){
						  $_SESSION['error']['option4'.$abcd] = "Please Enter Answer D";
						}
						if(strlen(trim($r['answer'])) == 0 ){
						  $_SESSION['error']['answer'.$abcd] = "Please Enter Answer";
						}
						/*if(strlen(trim($r['6'])) == 0 ){
						  $_SESSION['error']['explanation'.$abcd] = "Please Enter Explanation";
						}*/

						$option1=$r['option1'];
						$option2=$r['option2'];
						$option3=$r['option3'];
						$option4=$r['option4'];
						if($option1!='' && $option2!='' && $option3!='' && $option4!=''){
							$post_array = [
								'option1' => $option1, 
								'option2' => $option2, 
								'option3' => $option3, 
								'option4' => $option4 
								
							];
							$keys = array_keys($post_array);
							foreach ($keys as $key) {
								$keys = array_keys($post_array);
								foreach ($post_array as $k => $v) {
									if ($key != $k) {
										if ($v == $post_array[$key]) {
											 $_SESSION['error'][$k.$abcd] = "Same options should not enter for one question";
										}
									}
								}
							}
						}
						$abcd++;
					}
					if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

						$_SESSION['error']['complexity'] = "*Please Select Complexity";

					}
					if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

						$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

					}
					if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

						$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

					}
					if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

						$_SESSION['error']['usageset'] = "*Please Select Usageset";

					}
					if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

						$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

					}
				
					if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
					
					?>
						<script type="text/javascript">
						$('#formSubmit').show();

						setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>');
						
						</script>

					<?php
					}
					else{
						if($id!='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}

							if($post['usageset']!=''){
								$usageset=$post['usageset'];
							}else{
								$usageset=0;
							}
							if($post['timeduration']!=''){
								$timeduration=$post['timeduration'];
							}else{
								$timeduration=0;
							}

							if($post['question_theory']!=''){
									$question_theory=$post['question_theory'];
							}else{
								$question_theory=0;
							}

							if($post['complexity']!=''){
									$complexity=$post['complexity'];
							}else{
								$complexity=0;
							}
							if($post['conttype']!=''){
									$conttype=$post['conttype'];
							}else{
								$conttype=0;
							}
							$compquestion=str_replace("'","&#39;",$_SESSION['compquestion']);
							$sql=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
							$rowl=mysqli_fetch_array($sql);
							$database->query("update  compquestion set compquestion='".$compquestion."' where estatus='1' and id='".$rowl['compquestion']."'");
							$lid = $rowl['compquestion'];


							$sello=$database->query("select * from users where username='".$session->username."'");
							$rowese=mysqli_fetch_array($sello);

							$chapcon='';
							$chapdata=explode(",",$rowese['chapter']);
							foreach($chapdata as $chapterdata){
								if($chapterdata!=''){
									$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
								}
							}
							$chaptercon1=rtrim($chapcon,'OR');
							$chaptercon2=" AND (".$chaptercon1.")";
							$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
							$sellr=$database->query("select id from createquestion where estatus='1'".$cond." and id!='".$id."' and id<'".$id."' and vstatus1='0' and ppaper!=1  order by id desc limit 0,1");
							$rowcountr=mysqli_fetch_array($sellr);

							$sellr1=$database->query("select id from createquestion where estatus='1'".$cond." and id!='".$id."' and id<'".$id."' and ppaper!=1 order by id desc limit 0,1");
							$rowcountr1=mysqli_fetch_array($sellr1);
							if(count($list)>0)
							{
								
								if($post['version']=='1'){
									foreach ($list as $r)
									{
										$question=str_replace("'","&#39;",$r['question']);
										$option1=str_replace("'","&#39;",$r['option1']);
										$option2=str_replace("'","&#39;",$r['option2']);
										$option3=str_replace("'","&#39;",$r['option3']);
										$option4=str_replace("'","&#39;",$r['option4']);
										$explanation=str_replace("'","&#39;",$r['explanation']);
										if($r['comid']!='' && $r['comid']!='undefined'){
											$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$r['comid']."'");
											$rowllcount=mysqli_num_rows($sqllo);
											if($rowllcount>0){
												$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$r['comid']."'");
											}
											$sqlquestion1=$database->query("select * from createquestion where estatus='1' and id='".$r['comid']."'");
											$rowquestion1=mysqli_fetch_array($sqlquestion1);
											if($rowquestion1['vstatus1']!=0){
												$rowquestion1['vstatus1']=$rowquestion1['vstatus1'];
											}else{
												$rowquestion1['vstatus1']=0;
											}
											$database->query('insert version_question  set question_id="'.$r['comid'].'",class="'.$rowquestion1['class'].'",exam="'.$rowquestion1['exam'].'",subject="'.$rowquestion1['subject'].'",ppaper="'.$rowquestion1['ppaper'].'",qset="'.$rowquestion1['qset'].'",pexamtype="'.$rowquestion1['pexamtype'].'",year="'.$rowquestion1['year'].'",chapter="'.$rowquestion1['chapter'].'",topic="'.$rowquestion1['topic'].'",qtype="'.$rowquestion1['qtype'].'",compquestion="'.$rowquestion1['compquestion'].'",question="'.$rowquestion1['question'].'",option1="'.$rowquestion1['option1'].'",option2="'.$rowquestion1['option2'].'",option3="'.$rowquestion1['option3'].'",option4="'.$rowquestion1['option4'].'",explanation="'.$rowquestion1['explanation'].'",expla_vlink="",answer="'.$rowquestion1['answer'].'",complexity="'.$rowquestion1['complexity'].'",timeduration="'.$rowquestion1['timeduration'].'",inputquestion="'.$rowquestion1['inputquestion'].'",usageset="'.$rowquestion1['usageset'].'",question_theory="'.$rowquestion1['question_theory'].'",conttype="'.$rowquestion1['conttype'].'",conttypeids="'.$rowquestion1['conttypeids'].'",vusername1="'.$rowquestion1['vusername1'].'",vstatus1="'.$rowquestion1['vstatus1'].'",vtimestamp1="'.$rowquestion1['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');

											
											$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",topic="'.$topic.'",qtype="'.$post['qtype'].'",compquestion="'.$lid.'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$r['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vusername1="'.$rowquestion1['vusername1'].'",vstatus1="'.$rowquestion1['vstatus1'].'",vtimestamp1="'.$rowquestion1['vtimestamp1'].'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$r['comid'].'"');

											
											$database->query("insert question_log set question_id='".$r['comid']."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");

											
											
										}else{
											$result=$database->query('insert createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",topic="'.$topic.'",qtype="'.$post['qtype'].'",compquestion="'.$lid.'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$r['answer'].'",username="'.$_SESSION['username'].'",question_theory="0",estatus="1",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",conttype="'.$conttype.'",conttypeids="'.$post['conttypeids'].'",vusername1="'.$_SESSION['username'].'",vstatus1="0",vtimestamp1="'.time().'"');
											$lid1 = mysqli_insert_id($database->connection);
											$database->query("insert question_log set question_id='".$lid1."',username='".$_SESSION['username']."',message='1',module='dashboard',estatus='1',timestamp='".time()."'");
										}
									}
									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									unset($_SESSION['data4']);
									if($rowcountr1['id']!=''){
									?>
										<script type="text/javascript">
										
											$("#savedtoast").toast({
												delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												 $('#adminForm').show();
												$('#datafill').hide();
												setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											},3000);
											
										</script>
									<?php
									}else{
										?>
										<script type="text/javascript">
										
											$("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){
												$('#adminForm').show();
												$('#datafill').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
												setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											},3000);
											
										</script>
									<?php

									}
								}else if($post['version']=='0'){
									foreach ($list as $r)
									{ 
										$question=str_replace("'","&#39;",$r['question']);
										$option1=str_replace("'","&#39;",$r['option1']);
										$option2=str_replace("'","&#39;",$r['option2']);
										$option3=str_replace("'","&#39;",$r['option3']);
										$option4=str_replace("'","&#39;",$r['option4']);
										$explanation=str_replace("'","&#39;",$r['explanation']);
										if($r['comid']!='' && $r['comid']!='undefined'){
											$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",topic="'.$topic.'",qtype="'.$post['qtype'].'",compquestion="'.$lid.'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$r['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vusername1="'.$_SESSION['username'].'",vstatus1="1",vtimestamp1="'.time().'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$r['comid'].'"');
											$database->query("insert question_log set question_id='".$r['comid']."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");

										}
									}
									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									unset($_SESSION['data4']);
				
									?>
					
									<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Question updated successfully!</div>
									
									<?php 
									if($rowcountr['id']!=''){
									?>
										<script type="text/javascript">
										 $('#formSubmit').show();
										
											$('#datafill').hide();
											$("#verifytoast").toast({
											 delay: 3000,
												autohide: true,

											});
											$('#verifytoast').toast('show');
											setTimeout(function(){ 
											
												setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											},3000);
											
										</script>
									<?php
									}else{
										?>
										<script type="text/javascript">
										 $('#adminForm').show();
										
											$('#datafill').show();
											$("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											$('#savedtoast').toast('show');
											setTimeout(function(){ 
												setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
												setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');


											},3000);
											
										</script>
									<?php
									}
									?>
								<?php
								}

								
							}
							
						}
				?>
					
				<?php
						
				}
					
			}
		}else if($post['qtype']=='8'){
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter Explanation";

				}*/
				if(!$_SESSION['question'] || strlen(trim($_SESSION['question'])) == 0){
				
					$_SESSION['error']['question'] = "*Please Enter Question";
				
				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter Answer";
				
				}
				if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

					$_SESSION['error']['complexity'] = "*Please Select Complexity";

				}
				if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

					$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

				}
				if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

					$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

				}
				if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

					$_SESSION['error']['usageset'] = "*Please Select Usageset";

				}
				if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

					$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

				}
				
				
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
				?>
					<script type="text/javascript">
					$('#formSubmit').slideDown();

					setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&answer=<?php echo $post['answer']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>');
					</script>
					

				<?php
				}else{
						if($id!='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}
							if($post['timeduration']!=''){
								$timeduration=$post['timeduration'];
							}else{
								$timeduration=0;
							}
							if($post['usageset']!=''){
								$usageset=$post['usageset'];
							}else{
								$usageset=0;
							}

							if($post['question_theory']!=''){
									$question_theory=$post['question_theory'];
							}else{
								$question_theory=0;
							}

							if($post['complexity']!=''){
									$complexity=$post['complexity'];
							}else{
								$complexity=0;
							}
							if($post['conttype']!=''){
									$conttype=$post['conttype'];
							}else{
								$conttype=0;
							}
							$question=str_replace("'","&#39;",$_SESSION['question']);
							$explanation=str_replace("'","&#39;",$_SESSION['explanation']);

							$sello=$database->query("select * from users where username='".$session->username."'");
							$rowese=mysqli_fetch_array($sello);
							$chapcon='';
							$chapdata=explode(",",$rowese['chapter']);
							foreach($chapdata as $chapterdata){
								if($chapterdata!=''){
									$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
								}
							}
							$chaptercon1=rtrim($chapcon,'OR');
							$chaptercon2=" AND (".$chaptercon1.")";
							$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
							$sellr=$database->query("select id from createquestion where estatus='1'".$cond." and vstatus1='0' and id!='".$id."' and id<'".$id."' and ppaper!=1 order by id desc limit 0,1");
							$rowcountr=mysqli_fetch_array($sellr);
							$sellr1=$database->query("select id from createquestion where estatus='1'".$cond." and id!='".$id."' and id<'".$id."' and ppaper!=1 order by id desc limit 0,1");
							$rowcountr1=mysqli_fetch_array($sellr1);
							if($post['version']=='0'){
								$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",topic="'.$topic.'",qtype="'.$post['qtype'].'",question="'.$question.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vusername1="'.$_SESSION['username'].'",vstatus1="1",vtimestamp1="'.time().'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');
								$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");
								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								unset($_SESSION['compquestion']);
								if($rowcountr['id']!=''){
								?>
									<script type="text/javascript">
									 $('#adminForm').show();
									
										$('#datafill').hide();
										$("#verifytoast").toast({
										 delay: 3000,
											autohide: true,

										});
										$('#verifytoast').toast('show');
										setTimeout(function(){ 
										
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										},3000);
										
									</script>
								<?php
								}else{
										
									?>
									<script type="text/javascript">
									 
										$("#savedtoast").toast({
										 delay: 3000,
											autohide: true,

										});
										$('#savedtoast').toast('show');
										setTimeout(function(){ 
											$('#adminForm').show();
											$('#datafill').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
											setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										},3000);
										
									</script>
								<?php
								}
								?>
							<?php
							}else{
								$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$id."'");
								$rowllcount=mysqli_num_rows($sqllo);

								if($rowllcount>0){
									$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$id."'");
								}
								$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
								$rowquestion=mysqli_fetch_array($sqlquestion);
								if($rowquestion['vstatus1']!=0){
									$rowquestion['vstatus1']=$rowquestion['vstatus1'];
								}else{
									$rowquestion['vstatus1']=0;
								}
								$database->query('insert version_question  set question_id="'.$id.'",class="'.$rowquestion['class'].'",exam="'.$rowquestion['exam'].'",subject="'.$rowquestion['subject'].'",ppaper="'.$rowquestion['ppaper'].'",qset="'.$rowquestion['qset'].'",pexamtype="'.$rowquestion['pexamtype'].'",year="'.$rowquestion['year'].'",chapter="'.$rowquestion['chapter'].'",topic="'.$rowquestion['topic'].'",qtype="'.$rowquestion['qtype'].'",compquestion="'.$rowquestion['compquestion'].'",question="'.$rowquestion['question'].'",option1="'.$rowquestion['option1'].'",option2="'.$rowquestion['option2'].'",option3="'.$rowquestion['option3'].'",option4="'.$rowquestion['option4'].'",explanation="'.$rowquestion['explanation'].'",expla_vlink="",answer="'.$rowquestion['answer'].'",complexity="'.$rowquestion['complexity'].'",timeduration="'.$rowquestion['timeduration'].'",inputquestion="'.$rowquestion['inputquestion'].'",usageset="'.$rowquestion['usageset'].'",question_theory="'.$rowquestion['question_theory'].'",conttype="'.$rowquestion['conttype'].'",conttypeids="'.$rowquestion['conttypeids'].'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');
								if($post['class']!='undefined'){
									$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$chapter.'",topic="'.$topic.'",qtype="'.$post['qtype'].'",question="'.$question.'",explanation="'.$explanation.'",expla_vlink="",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');
									$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");

									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									if($rowcountr1['id']!=''){
										?>
										<script type="text/javascript">
											$("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#savedtoast').toast('show');
											
											setTimeout(function(){ 
												$('#adminForm').show();
												$('#datafill').hide();
												setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
												
											},3000);
										</script>
									<?php
									}else{
										?>
										<script type="text/javascript">
											$("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#savedtoast').toast('show');
											
											setTimeout(function(){ 
												$('#adminForm').show();
												$('#datafill').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
												setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
												
											},3000);
										</script>
									<?php
									}
								}else{
								?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},2000);
								</script>
								<?php
								}

							}
							
							
							
				
				
						}
					}
			}else if($post['qtype']=='3'){
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}
				/*if(!$_SESSION['question']){

							$_SESSION['error']['question'] = "*Please Enter Question";

						}*/
						if(!$post['option1'] || strlen(trim($post['option1'])) == 0){
						
							$_SESSION['error']['option1'] = "*Please Enter Option1";
						
						}
						if(!$post['option2'] || strlen(trim($post['option2'])) == 0){
						
							$_SESSION['error']['option2'] = "*Please Enter Option2";
						
						}
						if(!$post['option3'] || strlen(trim($post['option3'])) == 0){
						
							$_SESSION['error']['option3'] = "*Please Enter Option3";
						
						}
						if(!$post['option4'] || strlen(trim($post['option4'])) == 0){
						
							$_SESSION['error']['option4'] = "*Please Enter Option4";
						
						}
						if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
						
							$_SESSION['error']['answer'] = "*Please Enter Answer";
						
						}
						/*if(!$_SESSION['explanation']){

							$_SESSION['error']['explanation'] = "*Please Enter Explanation";

						}*/
						if(!$post['list1type'] || strlen(trim($post['list1type'])) == 0){
				
							$_SESSION['error']['list1type'] = "*Please Select List1 Type";
						
						}
						if(!$post['list2type'] || strlen(trim($post['list2type'])) == 0){
						
							$_SESSION['error']['list2type'] = "*Please Select List2 Type";
						
						}

						$abcd = 1;
						$a = 1;
						$ab=array();
						$ab1=array();
						$ab2=array();
						$j = 0;
						$m = 0;
						$l= 0;
						$list=$_SESSION['data5'];
						if(count($list)>0)
						{	
							foreach($list as $qqlist)
							{
							
								if(strlen(trim($qqlist['qlist1'])) == 0 ){
								  $_SESSION['error']['qlist1'.$abcd] = "Please Enter List1 Option".$abcd;
								}
								if(strlen(trim($qqlist['qlist2'])) == 0 ){
								  $_SESSION['error']['qlist2'.$abcd] = "Please Enter List2".$abcd;;
								}
								if (!in_array($qqlist['qlist1'], $ab))
								{
										 $ab[$j] = $qqlist['qlist1'];

										 $j++;
								} else {

								 $_SESSION['error']['qlist1'.$abcd] = "Same options should not enter for List1";        
								}
								if (!in_array($qqlist['qlist2'], $ab1))
								{
										 $ab1[$m] = $qqlist['qlist2'];

										 $m++;
								} else {

								 $_SESSION['error']['qlist2'.$abcd] = "Same options should not enter for List2";        
								}
								$abcd++;
							}
							
							
							foreach ($list as $qqlist1)
							{
								if (!in_array($qqlist1['qlist2'], $ab))
								{
										 $ab2[$l] = $qqlist1['qlist2']."_";
									

										 $l++;
								} else {

									$_SESSION['error']['qlist2'.$a] = "List1 Value should not enter for List2";   
									
								}
								$a++;
							}
							$option1=$post['option1'];
							$option2=$post['option2'];
							$option3=$post['option3'];
							$option4=$post['option4'];
							$option5=$post['option5'];
							if($option1!='' && $option2!='' && $option3!='' && $option4!=''  && $option5!=''){
								$post_array = [
									'option1' => $option1, 
									'option2' => $option2, 
									'option3' => $option3, 
									'option4' => $option4,
									'option5' => $option5
									
								];
								$keys = array_keys($post_array);
								foreach ($keys as $key) {
									$keys = array_keys($post_array);
									foreach ($post_array as $k => $v) {
										if ($key != $k) {
											if ($v == $post_array[$key]) {
												$_SESSION['error'][$k] = "Same options should not enter for one question";
											}
										}
									}
								}
							}

						if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

						$_SESSION['error']['complexity'] = "*Please Select Complexity";

						}
						if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

							$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

						}
						if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

							$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

						}
						if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

							$_SESSION['error']['usageset'] = "*Please Select Usageset";

						}
						if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

							$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

						}
						
						
						if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
						?>
							<script type="text/javascript">
							$('#formSubmit').slideDown();

							setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&option1=<?php echo urlencode($post['option1']); ?>&option2=<?php echo urlencode($post['option2']); ?>&option3=<?php echo urlencode($post['option3']); ?>&option4=<?php echo urlencode($post['option4']); ?>&option5=<?php echo urlencode($post['option5']); ?>&answer=<?php echo $post['answer']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?>&list1type=<?php echo $post['list1type']; ?>&list2type=<?php echo $post['list2type']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>');

							
							</script>
							<?php
						}else{
							if($id!='NULL')
							{
								$class=$post['class'];
								$exam=$post['exam'];
								$subject=$post['subject'];
								$chapter=$post['chapter'];
								$topic=$post['topic'];
								if($post['ppaper']!=''){
									$ppaper=$post['ppaper'];
								}else{
									$ppaper=0;
								}
								if($post['qset']!=''){
									$qset=$post['qset'];
								}else{
									$qset=0;
								}
								if($post['pexamtype']!=''){
									$pexamtype=$post['pexamtype'];
								}else{
									$pexamtype=0;
								}
								if($post['year']!=''){
									$year=$post['year'];
								}else{
									$year=0;
								}
								if($post['timeduration']!=''){
									$timeduration=$post['timeduration'];
								}else{
									$timeduration=0;
								}

								if($post['usageset']!=''){
								$usageset=$post['usageset'];
								}else{
									$usageset=0;
								}

								if($post['question_theory']!=''){
										$question_theory=$post['question_theory'];
								}else{
									$question_theory=0;
								}

								if($post['complexity']!=''){
										$complexity=$post['complexity'];
								}else{
									$complexity=0;
								}
								if($post['conttype']!=''){
										$conttype=$post['conttype'];
								}else{
									$conttype=0;
								}
								
								

								$qdata=array();
								$jk = 0;
								if(count($list)>0)
								{
									foreach ($list as $k)
									{
										//$qdata[]=$k;
										$qdata[$jk]['qlist1']=str_replace("'","&#39;",$k['qlist1']);
										$qdata[$jk]['qlist2']=str_replace("'","&#39;",$k['qlist2']);
										$jk++;
									}
								}
								$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
								$mat_question=str_replace("'","&#39;",$_SESSION['mat_question']);
								$mat_que_paragraph=str_replace("'","&#39;",$_SESSION['mat_que_paragraph']);
								$mat_que_footer=str_replace("'","&#39;",$_SESSION['mat_que_footer']);
								
								$sello=$database->query("select * from users where username='".$session->username."'");
								$rowese=mysqli_fetch_array($sello);
								$chapcon='';
								$chapdata=explode(",",$rowese['chapter']);
								foreach($chapdata as $chapterdata){
									if($chapterdata!=''){
										$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
									}
								}
								$chaptercon1=rtrim($chapcon,'OR');
								$chaptercon2=" AND (".$chaptercon1.")";
								$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
								$sellr=$database->query("select id from createquestion where estatus='1'".$cond." and id!='".$id."'  and id<'".$id."' and vstatus1='0' and ppaper!=1  order by id desc limit 0,1");
								$rowcountr=mysqli_fetch_array($sellr);

								$sellr1=$database->query("select id from createquestion where estatus='1'".$cond." and id!='".$id."' and id < '".$id."' and ppaper!=1  order by id desc limit 0,1");
								$rowcountr1=mysqli_fetch_array($sellr1);
								if($post['version']=='0'){
									
									$result=$database->query("update createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',topic='".$topic."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',complexity='".$complexity."',timeduration='".$timeduration."',inputquestion='".$post['inputquestion']."',usageset='".$usageset."',question_theory='".$question_theory."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',conttype='".$conttype."',conttypeids='".rtrim($post['conttypeids'],",")."',vusername1='".$_SESSION['username']."',vstatus1='1',vtimestamp1='".time()."',etimestamp='".time()."',eusername='".$_SESSION['username']."' where id='".$id."'");

									$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");
									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									unset($_SESSION['mat_question']);
									unset($_SESSION['mat_que_paragraph']);
										unset($_SESSION['mat_que_footer']);
									unset($_SESSION['data5']);
								
								if($rowcountr['id']!=''){
						
					?>
						
							
							<script type="text/javascript">
											
								//	alert("Question Updated Successfully");
									 $("#verifytoast").toast({
									 delay: 3000,
										autohide: true,

									});
									 $('#verifytoast').toast('show');
									
									setTimeout(function(){
										 $('#formSubmit').show();
										$('#datafill').hide();
										setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									},3000);
								</script>
						<?php }else{
						?>
							<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>  Question updated successfully!</div>
							
							<script type="text/javascript">
								
								//alert("Question Updated Successfully");
								 $("#savedtoast").toast({
								 delay: 3000,
									autohide: true,

								});
								 $('#savedtoast').toast('show');
								
								setTimeout(function(){
									$('#adminForm').show();
									$('#datafill').show();
									setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
									setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									//animateForm('<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
								},3000);
								
							</script>
						<?php
						}
					}else{
						$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$id."'");
						$rowllcount=mysqli_num_rows($sqllo);
						if($rowllcount>0){
							$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$id."'");
						}
						$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
						$rowquestion=mysqli_fetch_array($sqlquestion);
						if($rowquestion['vstatus1']!=0){
							$rowquestion['vstatus1']=$rowquestion['vstatus1'];
						}else{
							$rowquestion['vstatus1']=0;
						}
						$database->query('insert version_question  set question_id="'.$id.'",class="'.$rowquestion['class'].'",exam="'.$rowquestion['exam'].'",subject="'.$rowquestion['subject'].'",ppaper="'.$rowquestion['ppaper'].'",qset="'.$rowquestion['qset'].'",pexamtype="'.$rowquestion['pexamtype'].'",year="'.$rowquestion['year'].'",chapter="'.$rowquestion['chapter'].'",topic="'.$rowquestion['topic'].'",qtype="'.$rowquestion['qtype'].'",compquestion="'.$rowquestion['compquestion'].'",question="'.$rowquestion['question'].'",option1="'.$rowquestion['option1'].'",option2="'.$rowquestion['option2'].'",option3="'.$rowquestion['option3'].'",option4="'.$rowquestion['option4'].'",option5="'.$rowquestion['option5'].'",explanation="'.$rowquestion['explanation'].'",mat_question="'.$rowquestion['mat_question'].'",mat_que_paragraph="'.$rowquestion['mat_que_paragraph'].'",mat_que_footer="'.$rowquestion['mat_que_footer'].'",expla_vlink="",answer="'.$rowquestion['answer'].'",complexity="'.$rowquestion['complexity'].'",timeduration="'.$rowquestion['timeduration'].'",inputquestion="'.$rowquestion['inputquestion'].'",usageset="'.$rowquestion['usageset'].'",question_theory="'.$rowquestion['question_theory'].'",conttype="'.$rowquestion['conttype'].'",conttypeids="'.$rowquestion['conttypeids'].'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');
						if($post['class']!='undefined'){
							$result=$database->query("update createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',topic='".$topic."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',complexity='".$complexity."',timeduration='".$timeduration."',inputquestion='".$post['inputquestion']."',usageset='".$usageset."',question_theory='".$question_theory."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',conttype='".$conttype."',conttypeids='".rtrim($post['conttypeids'],",")."',vusername1='".$rowquestion['vusername1']."',vstatus1='".$rowquestion['vstatus1']."',vtimestamp1='".$rowquestion['vtimestamp1']."',etimestamp='".time()."',eusername='".$_SESSION['username']."' where id='".$id."'");
						
							$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");
							unset($_SESSION['question']);
							unset($_SESSION['option1']);
							unset($_SESSION['option2']);
							unset($_SESSION['option3']);
							unset($_SESSION['option4']);
							unset($_SESSION['explanation']);
							unset($_SESSION['compquestion']);
							unset($_SESSION['mat_question']);
							unset($_SESSION['mat_que_paragraph']);
					unset($_SESSION['mat_que_footer']);
							unset($_SESSION['data5']);
							if($rowcountr1['id']!=''){
								?>
								<script type="text/javascript">
									$("#savedtoast").toast({
									 delay: 3000,
										autohide: true,

									});
									 $('#savedtoast').toast('show');
									
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').hide();
										setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										
									},3000);
								</script>
							<?php
							}else{
								?>
								<script type="text/javascript">
									$("#savedtoast").toast({
									 delay: 3000,
										autohide: true,

									});
									 $('#savedtoast').toast('show');
									
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										
									},3000);
								</script>
							<?php
							}
						}else{
							?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},2000);
								</script>
								<?php
								}
									
							}

								
						}
					}
				}
			}else if($post['qtype']=='9'){
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}
				/*if(!$_SESSION['question']){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}*/
				if(!$post['option1'] || strlen(trim($post['option1'])) == 0){
				
					$_SESSION['error']['option1'] = "*Please Enter Option1";
				
				}
				if(!$post['option2'] || strlen(trim($post['option2'])) == 0){
				
					$_SESSION['error']['option2'] = "*Please Enter Option2";
				
				}
				if(!$post['option3'] || strlen(trim($post['option3'])) == 0){
				
					$_SESSION['error']['option3'] = "*Please Enter Option3";
				
				}
				if(!$post['option4'] || strlen(trim($post['option4'])) == 0){
				
					$_SESSION['error']['option4'] = "*Please Enter Option4";
				
				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter Answer";
				
				}
				/*if(!$_SESSION['explanation']){

					$_SESSION['error']['explanation'] = "*Please Enter Explanation";

				}*/
				if(!$post['list1type'] || strlen(trim($post['list1type'])) == 0){
				
					$_SESSION['error']['list1type'] = "*Please Select List1 Type";
				
				}
				if(!$post['list2type'] || strlen(trim($post['list2type'])) == 0){
				
					$_SESSION['error']['list2type'] = "*Please Select List2 Type";
				
				}
				$abcd = 1;
				$a = 1;
				$ab=array();
				$ab1=array();
				$ab2=array();
				$j = 0;
				$m = 0;
				$l= 0;
				$list=$_SESSION['data5'];
						if(count($list)>0)
						{	
							foreach($list as $qqlist)
							{
							
								if(strlen(trim($qqlist['qlist1'])) == 0 ){
								  $_SESSION['error']['qlist1'.$abcd] = "Please Enter List1 Option".$abcd;
								}
								if(strlen(trim($qqlist['qlist2'])) == 0 ){
								  $_SESSION['error']['qlist2'.$abcd] = "Please Enter List2".$abcd;;
								}
								if (!in_array($qqlist['qlist1'], $ab))
								{
										 $ab[$j] = $qqlist['qlist1'];

										 $j++;
								} else {

								 $_SESSION['error']['qlist1'.$abcd] = "Same options should not enter for List1";        
								}
								if (!in_array($qqlist['qlist2'], $ab1))
								{
										 $ab1[$m] = $qqlist['qlist2'];

										 $m++;
								} else {

								 $_SESSION['error']['qlist2'.$abcd] = "Same options should not enter for List2";        
								}
								$abcd++;
							}
							
							
						foreach ($list as $qqlist1)
						{
							if (!in_array($qqlist1['qlist2'], $ab))
							{
									 $ab2[$l] = $qqlist1['qlist2']."_";
								

									 $l++;
							} else {

								$_SESSION['error']['qlist2'.$a] = "List1 Value should not enter for List2";   
								
							}
							$a++;
						}
						$option1=$post['option1'];
						$option2=$post['option2'];
						$option3=$post['option3'];
						$option4=$post['option4'];
						$option5=$post['option5'];
						if($option1!='' && $option2!='' && $option3!='' && $option4!='' && $option5!=''){
							$post_array = [
								'option1' => $option1, 
								'option2' => $option2, 
								'option3' => $option3, 
								'option4' => $option4,
								'option5' => $option5
								
							];
							$keys = array_keys($post_array);
							foreach ($keys as $key) {
								$keys = array_keys($post_array);
								foreach ($post_array as $k => $v) {
									if ($key != $k) {
										if ($v == $post_array[$key]) {
											$_SESSION['error'][$k] = "Same options should not enter for one question";
										}
									}
								}
							}
						}
					if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

						$_SESSION['error']['complexity'] = "*Please Select Complexity";

					}
					if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

						$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

					}
					if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

						$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

					}
					if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

						$_SESSION['error']['usageset'] = "*Please Select Usageset";

					}
					if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

						$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

					}
						
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
					/*echo '<pre>';
					print_r($_SESSION);
					echo '</pre>';*/
				?>
					<script type="text/javascript">
					$('#formSubmit').slideDown();

					setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&option1=<?php echo urlencode($post['option1']); ?>&option2=<?php echo urlencode($post['option2']); ?>&option3=<?php echo urlencode($post['option3']); ?>&option4=<?php echo urlencode($post['option4']); ?>&option5=<?php echo urlencode($post['option5']); ?>&answer=<?php echo $post['answer']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?>&list1type=<?php echo $post['list1type']; ?>&list2type=<?php echo $post['list2type']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>');
					</script>
					<?php
				}else{
						if($id!='NULL')
						{
							$class=$post['class'];
							$exam=$post['exam'];
							$subject=$post['subject'];
							$chapter=$post['chapter'];
							$topic=$post['topic'];
							if($post['ppaper']!=''){
								$ppaper=$post['ppaper'];
							}else{
								$ppaper=0;
							}
							if($post['qset']!=''){
								$qset=$post['qset'];
							}else{
								$qset=0;
							}
							if($post['pexamtype']!=''){
								$pexamtype=$post['pexamtype'];
							}else{
								$pexamtype=0;
							}
							if($post['year']!=''){
								$year=$post['year'];
							}else{
								$year=0;
							}

							if($post['usageset']!=''){
								$usageset=$post['usageset'];
							}else{
								$usageset=0;
							}
							if($post['timeduration']!=''){
								$timeduration=$post['timeduration'];
							}else{
								$timeduration=0;
							}

							if($post['question_theory']!=''){
									$question_theory=$post['question_theory'];
							}else{
								$question_theory=0;
							}

							if($post['complexity']!=''){
									$complexity=$post['complexity'];
							}else{
								$complexity=0;
							}
							if($post['conttype']!=''){
									$conttype=$post['conttype'];
							}else{
								$conttype=0;
							}
							
							$qdata=array();
							$jk = 0;
							if(count($list)>0)
							{
								foreach ($list as $k)
								{
									//$qdata[]=$k;
									$qdata[$jk]['qlist1']=str_replace("'","&#39;",$k['qlist1']);
									$qdata[$jk]['qlist2']=str_replace("'","&#39;",$k['qlist2']);
									$jk++;
								}
							}
							$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
							$mat_question=str_replace("'","&#39;",$_SESSION['mat_question']);
							$mat_que_paragraph=str_replace("'","&#39;",$_SESSION['mat_que_paragraph']);
							$mat_que_footer=str_replace("'","&#39;",$_SESSION['mat_que_footer']);
							$sello=$database->query("select * from users where username='".$session->username."'");
							$rowese=mysqli_fetch_array($sello);
							$chapcon='';
								$chapdata=explode(",",$rowese['chapter']);
								foreach($chapdata as $chapterdata){
									if($chapterdata!=''){
										$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
									}
								}
								$chaptercon1=rtrim($chapcon,'OR');
								$chaptercon2=" AND (".$chaptercon1.")";
							$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
							$sellr=$database->query("select id from createquestion where estatus='1'".$cond."  and id!='".$id."' and id< '".$id."'  and vstatus1='0' and ppaper!=1 order by id desc limit 0,1");
							$rowcountr=mysqli_fetch_array($sellr);
							$sellr1=$database->query("select id from createquestion where estatus='1'".$cond."  and id!='".$id."'  and id < '".$id."'  and ppaper!=1 order by id desc limit 0,1");
							$rowcountr1=mysqli_fetch_array($sellr1);
							if($post['version']=='0'){
								$result=$database->query("update createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',topic='".$topic."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',complexity='".$complexity."',timeduration='".$timeduration."',inputquestion='".$post['inputquestion']."',usageset='".$usageset."',question_theory='".$question_theory."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',conttype='".$conttype."',conttypeids='".rtrim($post['conttypeids'],",")."',vusername1='".$_SESSION['username']."',vstatus1='1',vtimestamp1='".time()."',etimestamp='".time()."',eusername='".$_SESSION['username']."' where id='".$id."'");
								
								$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");
								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								unset($_SESSION['mat_question']);
								unset($_SESSION['mat_que_paragraph']);
					unset($_SESSION['mat_que_footer']);
								unset($_SESSION['data5']);
								if($rowcountr['id']!=''){
										?>
										<script type="text/javascript">
											
										//	alert("Question Updated Successfully");
											 $("#verifytoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#verifytoast').toast('show');
											
											setTimeout(function(){
												 $('#formSubmit').show();
												$('#datafill').hide();
												setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											},3000);
										</script>
										
								
								<?php
								}else{
									
									?>
										
										
											<script type="text/javascript">
											
											 $("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#savedtoast').toast('show');
											
											setTimeout(function(){ 
												$('#adminForm').show();
												$('#datafill').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
												setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											},3000);
											
										</script>
										
								
								<?php
								}

							}else{
								$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$id."'");
								$rowllcount=mysqli_num_rows($sqllo);
								if($rowllcount>0){
									$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$id."'");
								}
								$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
								$rowquestion=mysqli_fetch_array($sqlquestion);
								if($rowquestion['vstatus1']!=0){
									$rowquestion['vstatus1']=$rowquestion['vstatus1'];
								}else{
									$rowquestion['vstatus1']=0;
								}

								$database->query('insert version_question  set question_id="'.$id.'",class="'.$rowquestion['class'].'",exam="'.$rowquestion['exam'].'",subject="'.$rowquestion['subject'].'",ppaper="'.$rowquestion['ppaper'].'",qset="'.$rowquestion['qset'].'",pexamtype="'.$rowquestion['pexamtype'].'",year="'.$rowquestion['year'].'",chapter="'.$rowquestion['chapter'].'",topic="'.$rowquestion['topic'].'",qtype="'.$rowquestion['qtype'].'",compquestion="'.$rowquestion['compquestion'].'",question="'.$rowquestion['question'].'",option1="'.$rowquestion['option1'].'",option2="'.$rowquestion['option2'].'",option3="'.$rowquestion['option3'].'",option4="'.$rowquestion['option4'].'",option5="'.$rowquestion['option5'].'",explanation="'.$rowquestion['explanation'].'",mat_question="'.$rowquestion['mat_question'].'",mat_que_paragraph="'.$rowquestion['mat_que_paragraph'].'",mat_que_footer="'.$rowquestion['mat_que_footer'].'",expla_vlink="",answer="'.$rowquestion['answer'].'",complexity="'.$rowquestion['complexity'].'",timeduration="'.$rowquestion['timeduration'].'",inputquestion="'.$rowquestion['inputquestion'].'",usageset="'.$rowquestion['usageset'].'",question_theory="'.$rowquestion['question_theory'].'",conttype="'.$rowquestion['conttype'].'",conttypeids="'.$rowquestion['conttypeids'].'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');
								if($post['class']!='undefined'){
									$result=$database->query("update createquestion set class='".rtrim($class,",")."',exam='".rtrim($exam,",")."',subject='".$subject."',ppaper='".$ppaper."',qset='".$qset."',pexamtype='".$pexamtype."',year='".$year."',chapter='".$chapter."',topic='".$topic."',qtype='".$post['qtype']."',question='".json_encode($qdata)."',option1='".$post['option1']."',option2='".$post['option2']."',option3='".$post['option3']."',option4='".$post['option4']."',option5='".$post['option5']."',explanation='".$explanation."',mat_question='".$mat_question."',mat_que_paragraph='".$mat_que_paragraph."',mat_que_footer='".$mat_que_footer."',expla_vlink='',answer='".$post['answer']."',complexity='".$complexity."',timeduration='".$timeduration."',inputquestion='".$post['inputquestion']."',usageset='".$usageset."',question_theory='".$question_theory."',list1type='".$post['list1type']."',list2type='".$post['list2type']."',conttype='".$conttype."',conttypeids='".rtrim($post['conttypeids'],",")."',vusername1='".$rowquestion['vusername1']."',vstatus1='".$rowquestion['vstatus1']."',vtimestamp1='".$rowquestion['vtimestamp1']."',etimestamp='".time()."',eusername='".$_SESSION['username']."' where id='".$id."'");

								

									$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");

									unset($_SESSION['question']);
									unset($_SESSION['option1']);
									unset($_SESSION['option2']);
									unset($_SESSION['option3']);
									unset($_SESSION['option4']);
									unset($_SESSION['explanation']);
									unset($_SESSION['compquestion']);
									unset($_SESSION['mat_question']);
									unset($_SESSION['mat_que_paragraph']);
									unset($_SESSION['mat_que_footer']);
									unset($_SESSION['data5']);
									if($rowcountr1['id']!=''){
									?>
								
										<script type="text/javascript">
										 
											 $("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#savedtoast').toast('show');
											
											setTimeout(function(){ 
												$('#adminForm').show();
												$('#datafill').hide();
												setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
												
											},3000);
										</script>
									<?php
									}else{
										?>
								
										<script type="text/javascript">
										 
											 $("#savedtoast").toast({
											 delay: 3000,
												autohide: true,

											});
											 $('#savedtoast').toast('show');
											
											setTimeout(function(){ 
												$('#adminForm').show();
												$('#datafill').show();
												setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
												setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
												
											},3000);
										</script>
									<?php
									}
								}else{
									?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},2000);
								</script>
								<?php
								}
							}

						}
					}
				}
			}else{
				if(!$_SESSION['question'] || strlen(trim($_SESSION['question'])) == 0){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}

				if(!$_SESSION['option1'] || strlen(trim($_SESSION['option1'])) == 0){

					$_SESSION['error']['option1'] = "*Please Enter Option1";

				}
				if(!$_SESSION['option2'] || strlen(trim($_SESSION['option2'])) == 0){

					$_SESSION['error']['option2'] = "*Please Enter Option2";

				}
				if(!$_SESSION['option3'] || strlen(trim($_SESSION['option3'])) == 0){

					$_SESSION['error']['option3'] = "*Please Enter Option3";

				}
				if(!$_SESSION['option4'] || strlen(trim($_SESSION['option4'])) == 0){

					$_SESSION['error']['option4'] = "*Please Enter Option4";

				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter answer";
				
				}
				
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}
				if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

					$_SESSION['error']['complexity'] = "*Please Select Complexity";

				}
				if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

					$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

				}
				if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

					$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

				}
				if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

					$_SESSION['error']['usageset'] = "*Please Select Usageset";

				}
				if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

					$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}
				$question=$_SESSION['question'];
				$option1=$_SESSION['option1'];
				$option2=$_SESSION['option2'];
				$option3=$_SESSION['option3'];
				$option4=$_SESSION['option4'];
				if($option1!='' && $option2!='' && $option3!='' && $option4!=''){
					$post_array = [
						'option1' => $option1, 
						'option2' => $option2, 
						'option3' => $option3, 
						'option4' => $option4 
						
					];
					$keys = array_keys($post_array);
					foreach ($keys as $key) {
						$keys = array_keys($post_array);
						foreach ($post_array as $k => $v) {
							if ($key != $k) {
								if ($v == $post_array[$key]) {
									$_SESSION['error'][$k] = "Same options should not enter for one question";
								}
							}
						}
					}
				}
				
				//Check if any errors exist
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
					
				?>
					<script type="text/javascript">
					$('#formSubmit').slideDown();

					setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&answer=<?php echo $post['answer']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?>&editform=<?php echo $id; ?>&expla_vlink=<?php echo $post['expla_vlink']; ?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')

					</script>

				<?php
				}
				else{
					
					if($id!='NULL')
					{
						
						$question=str_replace("'","&#39;",$_SESSION['question']);
						$option1=str_replace("'","&#39;",$_SESSION['option1']);
						$option2=str_replace("'","&#39;",$_SESSION['option2']);
						$option3=str_replace("'","&#39;",$_SESSION['option3']);
						$option4=str_replace("'","&#39;",$_SESSION['option4']);
						$explanation=str_replace("'","&#39;",$_SESSION['explanation']);

						$class=$post['class'];
						$exam=$post['exam'];
						$subject=$post['subject'];
						$chapter=$post['chapter'];
						$topic=$post['topic'];


						if($post['ppaper']!=''){
							$ppaper=$post['ppaper'];
						}else{
							$ppaper=0;
						}
						if($post['qset']!=''){
							$qset=$post['qset'];
						}else{
							$qset=0;
						}
						if($post['pexamtype']!=''){
							$pexamtype=$post['pexamtype'];
						}else{
							$pexamtype=0;
						}
						if($post['year']!=''){
							$year=$post['year'];
						}else{
							$year=0;
						}
						if($post['timeduration']!=''){
							$timeduration=$post['timeduration'];
						}else{
							$timeduration=0;
						}


						if($post['usageset']!='' && $post['usageset']!='undefined'){
								$usageset=$post['usageset'];
						}else{
							$usageset=0;
						}

						if($post['question_theory']!='' && $post['question_theory']!='undefined'){
								$question_theory=$post['question_theory'];
						}else{
							$question_theory=0;
						}

						if($post['complexity']!='' && $post['complexity']!='undefined'){
								$complexity=$post['complexity'];
						}else{
							$complexity=0;
						}
						if($post['conttype']!='' && $post['conttype']!='undefined'){
								$conttype=$post['conttype'];
						}else{
							$conttype=0;
						}
						$sello=$database->query("select * from users where username='".$session->username."'");
						$rowese=mysqli_fetch_array($sello);
						$chapcon='';
						$chapdata=explode(",",$rowese['chapter']);
						foreach($chapdata as $chapterdata){
							if($chapterdata!=''){
								$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
							}
						}
						$chaptercon1=rtrim($chapcon,'OR');
						$chaptercon2=" AND (".$chaptercon1.")";
						$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
						
						$sellr=$database->query("select id from createquestion where estatus='1'".$cond." and vstatus1='0' and id!='".$id."'  and id<'".$id."' and ppaper!=1 order by id desc limit 0,1");
						$rowcountr=mysqli_fetch_array($sellr);
						$sellr1=$database->query("select id from createquestion where estatus='1'".$cond."  and id!='".$id."' and id < '".$id."' and ppaper!=1 order by id desc limit 0,1");
						$rowcountr1=mysqli_fetch_array($sellr1);
						if($post['version']=='0'){
							if($post['class']!='undefined'){

								$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.rtrim($chapter,",").'",topic="'.rtrim($topic,",").'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="'.$post['expla_vlink'].'",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",vusername1="'.$_SESSION['username'].'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vstatus1="1",vtimestamp1="'.time().'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');

								
								$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");

								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								

								if($rowcountr['id']!=''){
								?>
									<!--  <div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>  Question updated successfully!</div> -->
									
										<script type="text/javascript">
										$("#verifytoast").toast({
											 delay: 3000,
											autohide: true,

										});
										 $('#verifytoast').toast('show');
										
										setTimeout(function(){ 
											 $('#formSubmit').show();
											 $('#datafill').hide();
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										}, 3000);	
											/// 
										
										
									</script>
								<?php
								}else{
									?>
									<script type="text/javascript">
										$("#savedtoast").toast({
											 delay: 3000,
											autohide: true,

										});
										$('#savedtoast').toast('show');
										
										setTimeout(function(){ 
											$('#adminForm').show();
											$('#datafill').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
											setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										//animateForm('<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
										},3000);
									</script>
								<?php
								}
							}else{
								?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},3000);
								</script>
							<?php
							}
						}else{
							
							$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$id."'");
							$rowllcount=mysqli_num_rows($sqllo);
							if($rowllcount>0){
								$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$id."'");
							}
							
							$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
							$rowquestion=mysqli_fetch_array($sqlquestion);
							if($rowquestion['vstatus1']!=0){
								$rowquestion['vstatus1']=$rowquestion['vstatus1'];
							}else{
								$rowquestion['vstatus1']=0;
							}
							$database->query('insert version_question  set question_id="'.$id.'",class="'.$rowquestion['class'].'",exam="'.$rowquestion['exam'].'",subject="'.$rowquestion['subject'].'",ppaper="'.$rowquestion['ppaper'].'",qset="'.$rowquestion['qset'].'",pexamtype="'.$rowquestion['pexamtype'].'",year="'.$rowquestion['year'].'",chapter="'.$rowquestion['chapter'].'",topic="'.$rowquestion['topic'].'",qtype="'.$rowquestion['qtype'].'",compquestion="'.$rowquestion['compquestion'].'",question="'.$rowquestion['question'].'",option1="'.$rowquestion['option1'].'",option2="'.$rowquestion['option2'].'",option3="'.$rowquestion['option3'].'",option4="'.$rowquestion['option4'].'",explanation="'.$rowquestion['explanation'].'",expla_vlink="",answer="'.$rowquestion['answer'].'",complexity="'.$rowquestion['complexity'].'",timeduration="'.$rowquestion['timeduration'].'",inputquestion="'.$rowquestion['inputquestion'].'",usageset="'.$rowquestion['usageset'].'",question_theory="'.$rowquestion['question_theory'].'",conttype="'.$rowquestion['conttype'].'",conttypeids="'.$rowquestion['conttypeids'].'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');
							if($post['class']!='undefined'){

								$result=$database->query('update createquestion set class="'.rtrim($class,",").'",exam="'.rtrim($exam,",").'",subject="'.$subject.'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.rtrim($chapter,",").'",topic="'.rtrim($topic,",").'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="'.$post['expla_vlink'].'",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",vusername1="'.$rowquestion['vusername1'].'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');
								$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");

								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								if($rowcountr1['id']!=''){
									?>
								
										<script type="text/javascript">
									
										 $("#savedtoast").toast({
												 delay: 3000,
												autohide: true,

										});
										$('#savedtoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											$('#datafill').hide();
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											
										}, 3000);
									</script>
								<?php
								}else{
									?>
								
										<script type="text/javascript">
									
										 $("#savedtoast").toast({
												 delay: 3000,
												autohide: true,

										});
										$('#savedtoast').toast('show');
										setTimeout(function(){ 
											 $('#adminForm').show();
											$('#datafill').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
											setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											
										}, 3000);
									</script>
								<?php
								}
							}else{
								?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},2000);
								</script>
								<?php
							}
						}
					}
				}
			}	
		}else{
			
			if(!$_SESSION['question'] || strlen(trim($_SESSION['question'])) == 0){

					$_SESSION['error']['question'] = "*Please Enter Question";

				}

				if(!$_SESSION['option1'] || strlen(trim($_SESSION['option1'])) == 0){

					$_SESSION['error']['option1'] = "*Please Enter Option1";

				}
				if(!$_SESSION['option2'] || strlen(trim($_SESSION['option2'])) == 0){

					$_SESSION['error']['option2'] = "*Please Enter Option2";

				}
				if(!$_SESSION['option3'] || strlen(trim($_SESSION['option3'])) == 0){

					$_SESSION['error']['option3'] = "*Please Enter Option3";

				}
				if(!$_SESSION['option4'] || strlen(trim($_SESSION['option4'])) == 0){

					$_SESSION['error']['option4'] = "*Please Enter Option4";

				}
				if(!$post['answer'] || strlen(trim($post['answer'])) == 0){
				
					$_SESSION['error']['answer'] = "*Please Enter answer";
				
				}
				/*if(!$_SESSION['explanation'] || strlen(trim($_SESSION['explanation'])) == 0){

					$_SESSION['error']['explanation'] = "*Please Enter explanation";

				}*/
				if(!$post['class'] || strlen(trim($post['class'])) == 0){

					$_SESSION['error']['class'] = "*Please Select Class";

				}
				if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

					$_SESSION['error']['exam'] = "*Please Select Exam";

				}
				if(!$post['subject'] || strlen(trim($post['subject'])) == 0){

					$_SESSION['error']['subject'] = "*Please Select Subject";

				}
				if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){

					$_SESSION['error']['chapter'] = "*Please Select chapter";

				}
				$class1=trim($post['class']);
				$class2=rtrim($class1,",");
				if($class2=='1,2'){
					$c=explode(",",$post['chapter']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please select multiple  chapters for two classes";
					}
				}
				if(!$post['topic'] || strlen(trim($post['topic'])) == 0){

					$_SESSION['error']['topic'] = "*Please Select Topic";

				}

				if(!$post['complexity'] || strlen(trim($post['complexity'])) == 0){

					$_SESSION['error']['complexity'] = "*Please Select Complexity";

				}
				if(!$post['timeduration'] || strlen(trim($post['timeduration'])) == 0){

					$_SESSION['error']['timeduration'] = "*Please Select Timeduration";

				}
				if(!$post['inputquestion'] || strlen(trim($post['inputquestion'])) == 0){

					$_SESSION['error']['inputquestion'] = "*Please Select Question Type";

				}
				if(!$post['usageset'] || strlen(trim($post['usageset'])) == 0){

					$_SESSION['error']['usageset'] = "*Please Select Usageset";

				}
				if(!$post['question_theory'] || strlen(trim($post['question_theory'])) == 0){

					$_SESSION['error']['question_theory'] = "*Please Select Question Theory";

				}
				
				if($post['inputquestion']=='1'){
					$c=explode(",",$post['chapter']);
					$t=explode(",",$post['topic']);
					if(count($c)<2){
						$_SESSION['error']['chapter'] = "*Please Select Multiple  chapters for questiontype linkage";
					}
					if(count($t)<2){
						$_SESSION['error']['topic'] = "*Please Select Multiple  Topics for questiontype linkage";
					}
				}

				$question=$_SESSION['question'];
				$option1=$_SESSION['option1'];
				$option2=$_SESSION['option2'];
				$option3=$_SESSION['option3'];
				$option4=$_SESSION['option4'];
				if($option1!='' && $option2!='' && $option3!='' && $option4!=''){
					$post_array = [
						'option1' => $option1, 
						'option2' => $option2, 
						'option3' => $option3, 
						'option4' => $option4 
						
					];
					$keys = array_keys($post_array);
					foreach ($keys as $key) {
						$keys = array_keys($post_array);
						foreach ($post_array as $k => $v) {
							if ($key != $k) {
								if ($v == $post_array[$key]) {
									$_SESSION['error'][$k] = "Same options should not enter for one question";
								}
							}
						}
					}
				}

				
				//Check if any errors exist
				if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
					
				?>
					<script type="text/javascript">
					$('#formSubmit').slideDown();

					setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=1&getedit=1&errordisplay=1&qtype=<?php echo $post['qtype'];?>&class=<?php echo $post['class'];?>&exam=<?php echo $post['exam'];?>&subject=<?php echo $post['subject']; ?>&chapter=<?php echo $post['chapter']; ?>&topic=<?php echo $post['topic']; ?>&ppaper=<?php echo $post['ppaper']; ?>&qset=<?php echo $post['qset']; ?>&pexamtype=<?php echo $post['pexamtype']; ?>&year=<?php echo $post['year']; ?>&answer=<?php echo $post['answer']; ?>&complexity=<?php echo $post['complexity']; ?>&timeduration=<?php echo $post['timeduration']; ?>&inputquestion=<?php echo $post['inputquestion']; ?>&conttype=<?php echo $post['conttype']; ?>&conttypeids=<?php echo $post['conttypeids']; ?>&usageset=<?php echo $post['usageset']; ?>&question_theory=<?php echo $post['question_theory']; ?>&editform=<?php echo $id; ?>&expla_vlink=<?php echo $post['expla_vlink']; ?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?><?php if(isset($_POST['search'])) { echo '&search='.$_POST['search']; } ?>')

					</script>

				<?php
				}
				else{
					
					if($id!='NULL')
					{
						
						
						$question=str_replace("'","&#39;",$_SESSION['question']);
						$option1=str_replace("'","&#39;",$_SESSION['option1']);
						$option2=str_replace("'","&#39;",$_SESSION['option2']);
						$option3=str_replace("'","&#39;",$_SESSION['option3']);
						$option4=str_replace("'","&#39;",$_SESSION['option4']);
						$explanation=str_replace("'","&#39;",$_SESSION['explanation']);
						$class=$post['class'];
						$exam=$post['exam'];
						$subject=$post['subject'];
						$chapter=$post['chapter'];
						$topic=$post['topic'];
						if($post['ppaper']!=''){
							$ppaper=$post['ppaper'];
						}else{
							$ppaper=0;
						}
						if($post['qset']!=''){
							$qset=$post['qset'];
						}else{
							$qset=0;
						}
						if($post['pexamtype']!=''){
							$pexamtype=$post['pexamtype'];
						}else{
							$pexamtype=0;
						}
						if($post['year']!=''){
							$year=$post['year'];
						}else{
							$year=0;
						}

						if($post['usageset']!=''){
								$usageset=$post['usageset'];
						}else{
							$usageset=0;
						}
						
						if($post['timeduration']!=''){
								$timeduration=$post['timeduration'];
							}else{
								$timeduration=0;
							}
						if($post['question_theory']!=''){
								$question_theory=$post['question_theory'];
						}else{
							$question_theory=0;
						}

						if($post['complexity']!=''){
								$complexity=$post['complexity'];
						}else{
							$complexity=0;
						}
						if($post['conttype']!=''){
								$conttype=$post['conttype'];
						}else{
							$conttype=0;
						}
						$sello=$database->query("select * from users where username='".$session->username."'");
						$rowese=mysqli_fetch_array($sello);
						$chapcon='';
						$chapdata=explode(",",$rowese['chapter']);
						foreach($chapdata as $chapterdata){
							if($chapterdata!=''){
								$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
							}
						}
						$chaptercon1=rtrim($chapcon,'OR');
						$chaptercon2=" AND (".$chaptercon1.")";
						$cond="  AND subject IN(".$rowese['subject'].") ".$chaptercon2."";
						$sellr=$database->query("select id from createquestion where estatus='1'".$cond." and vstatus1='0' and id!='".$id."' and id<='".$id."'  and ppaper!=1 order by id desc limit 0,1");
						$rowcountr=mysqli_fetch_array($sellr);

						$sellr1=$database->query("select id from createquestion where estatus='1'".$cond."  and id!='".$id."' and id<='".$id."'  and ppaper!=1 order by id desc limit 0,1");
						$rowcountr1=mysqli_fetch_array($sellr1);
						if($post['version']=='0'){
							$result=$database->query('update createquestion set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="'.$post['expla_vlink'].'",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",vusername1="'.$_SESSION['username'].'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vstatus1="1",vtimestamp1="'.time().'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');

							$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='5',module='dashboard',estatus='1',timestamp='".time()."'");

							unset($_SESSION['question']);
							unset($_SESSION['option1']);
							unset($_SESSION['option2']);
							unset($_SESSION['option3']);
							unset($_SESSION['option4']);
							unset($_SESSION['explanation']);
							
							if($rowcountr['id']!=''){
							?>
								<!--  <div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>  Question updated successfully!</div> -->
								
									<script type="text/javascript">
									
									 $("#verifytoast").toast({
										 delay: 3000,
										autohide: true,

									});
									 $('#verifytoast').toast('show');
									
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').hide();
										setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									}, 3000);
								</script>
						<?php
							}else{
									?>
								<!--  <div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>  Question updated successfully!</div> -->
								
									<script type="text/javascript">
									
									 $("#savedtoast").toast({
										 delay: 3000,
										autohide: true,

									});
									 $('#savedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									}, 3000);
									
									
								</script>
							<?php
							}
							?>
					<?php	
						}else{
							$sqllo=$database->query("select * from version_question  where estatus='1' and question_id='".$id."'");
							$rowllcount=mysqli_num_rows($sqllo);
							if($rowllcount>0){
								$database->query("update version_question set estatus='0' where  estatus='1' and question_id='".$id."'");
							}
							$sqlquestion=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
							$rowquestion=mysqli_fetch_array($sqlquestion);
							if($rowquestion['vstatus1']!=0){
								$rowquestion['vstatus1']=$rowquestion['vstatus1'];
							}else{
								$rowquestion['vstatus1']=0;
							}
							$database->query('insert version_question  set question_id="'.$id.'",class="'.$rowquestion['class'].'",exam="'.$rowquestion['exam'].'",subject="'.$rowquestion['subject'].'",ppaper="'.$rowquestion['ppaper'].'",qset="'.$rowquestion['qset'].'",pexamtype="'.$rowquestion['pexamtype'].'",year="'.$rowquestion['year'].'",chapter="'.$rowquestion['chapter'].'",topic="'.$rowquestion['topic'].'",qtype="'.$rowquestion['qtype'].'",compquestion="'.$rowquestion['compquestion'].'",question="'.$rowquestion['question'].'",option1="'.$rowquestion['option1'].'",option2="'.$rowquestion['option2'].'",option3="'.$rowquestion['option3'].'",option4="'.$rowquestion['option4'].'",explanation="'.$rowquestion['explanation'].'",expla_vlink="",answer="'.$rowquestion['answer'].'",complexity="'.$rowquestion['complexity'].'",timeduration="'.$rowquestion['timeduration'].'",inputquestion="'.$rowquestion['inputquestion'].'",usageset="'.$rowquestion['usageset'].'",question_theory="'.$rowquestion['question_theory'].'",conttype="'.$rowquestion['conttype'].'",conttypeids="'.$rowquestion['conttypeids'].'",vusername1="'.$rowquestion['vusername1'].'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",timestamp="'.time().'",username="'.$_SESSION['username'].'"');
							if($post['class']!='undefined'){
								$result=$database->query('update createquestion set class="'.rtrim($post['class'],",").'",exam="'.rtrim($post['exam'],",").'",subject="'.$post['subject'].'",ppaper="'.$ppaper.'",qset="'.$qset.'",pexamtype="'.$pexamtype.'",year="'.$year.'",chapter="'.$post['chapter'].'",topic="'.$post['topic'].'",question="'.$question.'",option1="'.$option1.'",option2="'.$option2.'",option3="'.$option3.'",option4="'.$option4.'",explanation="'.$explanation.'",expla_vlink="'.$post['expla_vlink'].'",answer="'.$post['answer'].'",complexity="'.$complexity.'",timeduration="'.$timeduration.'",inputquestion="'.$post['inputquestion'].'",usageset="'.$usageset.'",question_theory="'.$question_theory.'",vusername1="'.$rowquestion['vusername1'].'",conttype="'.$conttype.'",conttypeids="'.rtrim($post['conttypeids'],",").'",vstatus1="'.$rowquestion['vstatus1'].'",vtimestamp1="'.$rowquestion['vtimestamp1'].'",etimestamp="'.time().'",eusername="'.$_SESSION['username'].'" where id="'.$id.'"');

							

								$database->query("insert question_log set question_id='".$id."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");

								unset($_SESSION['question']);
								unset($_SESSION['option1']);
								unset($_SESSION['option2']);
								unset($_SESSION['option3']);
								unset($_SESSION['option4']);
								unset($_SESSION['explanation']);
								if($rowcountr1['id']!=''){
							?>
						
							
								<script type="text/javascript">
									
									//alert("Question Updated Successfully");
									 $("#savedtoast").toast({
											 delay: 3000,
											autohide: true,

										});
										 $('#savedtoast').toast('show');
										setTimeout(function(){ 
											$('#adminForm').show();
											$('#datafill').hide();
											setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&editform=<?php echo $rowcountr1['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
										}, 3000);
									
								</script>
						
								<?php
								}else{
									?>
						
							
								<script type="text/javascript">
									$("#savedtoast").toast({
											 delay: 3000,
											autohide: true,

										});
										 $('#savedtoast').toast('show');
										setTimeout(function(){ 
											$('#adminForm').show();
											$('#datafill').show();
											setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
											setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$_POST['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
											
										}, 3000);
									
								</script>
						
								<?php
								}
							}else{
								?>
								<script type="text/javascript">
									$("#failedtoast").toast({
										 delay: 2000,
										autohide: true,

									});
									$('#failedtoast').toast('show');
									setTimeout(function(){ 
										$('#adminForm').show();
										$('#datafill').show();
										setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
										setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&editform=<?php echo $rowcountr['id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?><?php if(isset($post['status'])) { echo '&status='.$post['status']; } ?><?php if(isset($post['search'])) { echo '&search='.$post['search']; } ?><?php if(isset($post['isstype'])) { echo '&isstype='.$post['isstype']; } ?><?php if(isset($post['issueids'])) { echo '&issueids='.$post['issueids']; } ?>');
									
									},2000);
								</script>
								<?php
							}
						}
						
					}


			?>


				<?php
				}
		}
		
	

?>

 
	<?php
	}
?>
<script> 
    
function search_report9() {
	
        var search = $('#search').val();
       
        setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&search='+search+'&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>');
    }
</script>

<?php
//Delete createticket

//Display bulkreport
if(isset($_GET['tableDisplay'])){

//print_r($row_sel);
    $limit=5;

	if(isset($_REQUEST['page_no'])){
		if($_REQUEST['page_no']!=""){
			$_GET['page']=$_REQUEST['page_no'];
		}else{
			
		}
	}
   if(isset($_GET['page']))
   {
	   if($_GET['page']!=''){
		  $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
		  $page=$_GET['page'];
	   }else{
		   $_GET['page']=1;
		   $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
		  $page=$_GET['page'];
	   }
   }
   else
   {
       $start = 0;      //if no page var is given, set start to 0
        $page=0;
   }


   $data_sel =  $database->query("select * from users where username='".$session->username."'");
   //echo "select * from users where username='".$session->username."'";
   $row = mysqli_fetch_array($data_sel);
    $tableName = 'createquestion';
	$dateto=strtotime(date('d-m-Y'));
	$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
	$dateyess1 = strtotime(date('d-m-Y',strtotime("-1 days")));
	$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days"). ' 23:59:59'));
	$dateyess3 = strtotime(date('d-m-Y',strtotime("-3 days")));
	$dateyess5 = strtotime(date('d-m-Y',strtotime("-5 days")));
	$dateyess7 = strtotime(date('d-m-Y',strtotime("-7 days")));
	$dateyess9 = strtotime(date('d-m-Y',strtotime("-9 days")));
	$dateyess12= strtotime(date('d-m-Y',strtotime("-12 days")));
	$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
	$dateyess21= strtotime(date('d-m-Y',strtotime("-21 days")));
	$dateyess28= strtotime(date('d-m-Y',strtotime("-28 days")));
	$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
	$dateyess60= strtotime(date('d-m-Y',strtotime("-60 days")));
	$dateyess90= strtotime(date('d-m-Y',strtotime("-90 days")));
	$dateyess120= strtotime(date('d-m-Y',strtotime("-120 days")));
	$dateyess180= strtotime(date('d-m-Y',strtotime("-180 days")));
	if(isset($_REQUEST['date'])){
		if($_REQUEST['date']=="today"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateto." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateto." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateto." and ".$dateto1."   ";
				$date1=" and a.vtimestamp1 between ".$dateto." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateto." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateto." and ".$dateto1." ";
			}
		} else if($_REQUEST['date']=="yesterday"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
				$date1=" and a.rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
			}else{
				$date=" and timestamp between ".$dateyess1." and ".$datetoyes1." ";
				$date1=" and a.timestamp between ".$dateyess1." and ".$datetoyes1." ";
			}
		}
		else if($_REQUEST['date']=="3days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess3." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess3." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess3." and ".$dateto1." ";
				$date1=" and timestamp between ".$dateyess3." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="5days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess5." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess5." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess5." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess5." and ".$dateto1." ";
			}
		}
		else if($_REQUEST['date']=="7days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess7." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess7." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess7." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess7." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="9days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess9." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess9." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess9." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess9." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess9." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess9." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="15days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess15." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess15." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess15." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess15." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="21days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess21." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess21." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess21." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess21." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess21." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess21." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="28days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess28." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess28." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess28." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess28." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess28." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess28." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="30days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess30." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess30." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess30." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess30." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="60days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess60." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess60." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess60." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess60." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess60." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess60." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="90days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess90." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess90." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess90." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess90." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess90." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess90." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="120days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and rtimestamp between ".$dateyess120." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess120." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and vtimestamp1 between ".$dateyess120." and ".$dateto1."  ";
				$date1=" and a.vtimestamp1 between ".$dateyess120." and ".$dateto1." ";
			}else{
				$date=" and timestamp between ".$dateyess120." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess120." and ".$dateto1." ";
			}
		}else if($_REQUEST['date']=="180days"){
			if($_REQUEST['status']!="" && $_REQUEST['rstatus']!=""){
				$date=" and a.rtimestamp between ".$dateyess180." and ".$dateto1." ";
				$date1=" and a.rtimestamp between ".$dateyess180." and ".$dateto1." ";
			}else if($_REQUEST['status']!=""){
				$date=" and a.vtimestamp1 between ".$dateyess180." and ".$dateto1." ";
				$date1=" and a.vtimestamp1 between ".$dateyess180." and ".$dateto1." ";
			}else{
				$date=" and a.timestamp between ".$dateyess180." and ".$dateto1." ";
				$date1=" and a.timestamp between ".$dateyess180." and ".$dateto1." ";
			}
		}else{
			$date="";
		}
	}else{
	}
	if(isset($_REQUEST['status'])){
		if($_REQUEST['status']!=""){
			if($_REQUEST['status']=="1"){
				$status=" and vstatus1='".$_REQUEST['status']."' and vusername1='".$session->username."' ";
				$_REQUEST['status']=$_REQUEST['status'];
			}else{
				$status=" and vstatus1='".$_REQUEST['status']."'";
				$_REQUEST['status']=$_REQUEST['status'];
			}
			
		}else{
			$status="";
			$_REQUEST['status']="";
		}
	}else{
		$status=" and vstatus1='0'";
		$_REQUEST['status']=0;
		
	}

	if(isset($_REQUEST['rstatus'])){
		if($_REQUEST['rstatus']!=""){
			$rstatus=" and review_status='".$_REQUEST['rstatus']."' and vusername1='".$session->username."' ";
		}else{
			$rstatus="";
		}
	}else{
		$rstatus="";
	}
	if(isset($_REQUEST['subject'])){
		if($_REQUEST['subject']!=""){
			$subject=" and subject IN (".$_REQUEST['subject'].")";
		}else{
			$subject="";
		}
	}else{
		$subject="";
	}
	$rowee=$database->query("select * from subject where estatus='1' and id in (".$row['subject'].") order by id asc limit 0,1");
	$rowee1=mysqli_fetch_array($rowee);
	if(isset($_REQUEST['sub'])){
		if($_REQUEST['sub']!=""){
			$sub=" and subject='".$_REQUEST['sub']."'";
			$sub1=" and a.subject='".$_REQUEST['sub']."'";
		}else{
			$sub="";
			$sub1="";
		}
	}else{
		$sub=" and subject='".$rowee1['id']."'";
		$sub1=" and a.subject='".$rowee1['id']."'";
	}
	if(isset($_REQUEST['class'])){
		if($_REQUEST['class']=="1"){
			$class=" and find_in_set(1,class)>0 ";
		}else if($_REQUEST['class']=="2"){
			$class=" and find_in_set(2,class)>0 ";
		}else{
			$class="";
		}
	}else{
		$class="";
	}
	if(isset($_REQUEST['exam'])){
		if($_REQUEST['exam']!=""){
			$exam=" and find_in_set(".$_REQUEST['exam'].",exam)>0";
		}
	}else{
		$exam="";
	}
	
	
	
	if(isset($_REQUEST['search'])){
		if($_REQUEST['search']!=""){
			
			$search=" AND id = '".$_REQUEST['search']."' ";
			$search1=" AND a.id = '".$_REQUEST['search']."' ";
			
		} else {
			$search="";
			$search1="";
		}
	}else{
		$search="";
		$search1="";
	}
	$chapcon='';
	$chapcon1='';
	$chapdata=explode(",",$row['chapter']);
	foreach($chapdata as $chapterdata){
		if($chapterdata!=''){
			$chapcon.="  find_in_set(".$chapterdata.",chapter)>0"." OR";
			$chapcon1.="  find_in_set(".$chapterdata.",a.chapter)>0"." OR";
		}
	}
	$chaptercon1=rtrim($chapcon,'OR');
	$chaptercon2=" AND (".$chaptercon1.")";
	//$chaptercon2="";

	$chaptercon11=rtrim($chapcon1,'OR');
	$chaptercon22=" AND (".$chaptercon11.")";
	//$chaptercon22="";
	
	if(isset($_REQUEST['isstype'])){
		if($_REQUEST['isstype']=="i"){
			$condition= " where estatus='1'  and ppaper!='1'".$date.$class.$exam.$subject.$sub.$status.$rstatus.$search." and subject IN (".rtrim($row['subject'],",").")  and id in (".$_REQUEST['issueids'].")  ".$chaptercon2."";
		}else{
			if($rstatus!=''){
				$condition= " where estatus='1' and ppaper!='1'".$date.$class.$exam.$subject.$sub.$status.$rstatus.$search." and subject IN (".rtrim($row['subject'],",").")";
			}else{
				$condition= " where estatus='1' and ppaper!='1'".$date.$class.$exam.$subject.$sub.$status.$rstatus.$search." and subject IN (".rtrim($row['subject'],",").") ".$chaptercon2." ";
			}
			
		}
	}else{
		if($rstatus!=''){
			$condition= " where estatus='1' and ppaper!='1'".$date.$class.$exam.$subject.$sub.$status.$rstatus.$search." and subject IN (".rtrim($row['subject'],",").")  ".$chaptercon2." ";
		}else{
			$condition= " where estatus='1' and ppaper!='1'".$date.$class.$exam.$subject.$sub.$status.$rstatus.$search." and subject IN (".rtrim($row['subject'],",").") ".$chaptercon2." ";
		}
	}
	if(strlen($conditon) > 0){
		$condition = $conidition;
	}
	
	 
	
	  
		   $xyz = mysqli_fetch_array($result_sel);
	
		
		if($_REQUEST['r_issue']!=''){
				$query="select a.*,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' and b.changes like '%".$_REQUEST['r_issue']."%'  ".$sub1.$date1.$search1." ".$chaptercon22." group by a.id  order by a.id desc LIMIT $start,$limit";
				
				$q1 = "select a.id from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' and b.changes like '%".$_REQUEST['r_issue']."%'  ".$sub1.$date1.$search1."  ".$chaptercon22." ";
				
				 $q = "select count(a.id) as num from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' and b.changes like '%".$_REQUEST['r_issue']."%' ".$sub1.$date1.$search1." ".$chaptercon22." ";

			
		}else if($_REQUEST['qwc']!=""){
			if($_REQUEST['qwc']==1){
				$query="select a.*,b.changes_count from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' ".$sub1.$date1.$search1."  ".$chaptercon22."  group by a.id order by a.id desc LIMIT $start,$limit";
				$q1 = "select count(a.id) as count from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' ".$sub1.$date1.$search1." ".$chaptercon22." group by a.id";
				$q = "select count(a.id) as num from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' ".$sub1.$date1.$search1." ".$chaptercon22." ";
			}else  if($_REQUEST['qwc']==0){
				$query="select a.*,b.changes_count from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count='0' and a.id=b.question_id and a.vusername1='".$session->username."' ".$sub1.$date1.$search1." ".$chaptercon22." group by a.id  order by a.id desc LIMIT $start,$limit";
				$q1 = "select a.id from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count='0' and a.id=b.question_id and a.vusername1='".$session->username."'  ".$sub1.$date1.$search1." ".$chaptercon22." group by a.id ";
				$q = "select count(a.id) as num from createquestion as a inner join reviewer_log as b on a.estatus='1'  and b.estatus='1' and a.vstatus1='1' and a.review_status='1' and b.message='1' and b.changes_count!='0' and a.id=b.question_id and a.vusername1='".$session->username."' ".$sub1.$date1.$search1." ".$chaptercon22."";
			}

		}else{
			$query = "SELECT * from createquestion ".$condition." ORDER BY id DESC LIMIT $start,$limit ";
			$q1 = "SELECT id FROM createquestion  ".$condition." ORDER BY id DESC  ";
			 $q = "SELECT count(id) as num FROM $tableName ".$condition." ORDER BY id DESC  ";
			 
		}
		
		$pagination = $session->showPagination99(SECURE_PATH."dashboard_v1/process.php?tableDisplay=1&date=".$_REQUEST['date']."&status=".$_REQUEST['status']."&rstatus=".$_REQUEST['rstatus']."&qwc=".$_REQUEST['qwc']."&r_issue=".$_REQUEST['r_issue']."&subject=".$_REQUEST['subject']."&class=".$_REQUEST['class']."&exam=".$_REQUEST['exam']."&search=".$_REQUEST['search']."&isstype=".$_REQUEST['isstype']."&issueids=".$_REQUEST['issueids']."&",$tableName,$start,$limit,$page,$q);
		
		
	 $data_sel1 = $database->query($query);
		$numres1 = mysqli_num_rows($data_sel1);
		 $result_sel = $database->query($q1);
	   $numres = mysqli_num_rows($result_sel);
	   $data_q1 = $database->query($q1 );
		$rq1=mysqli_fetch_array($data_q1);
		
	
	if(($start+$limit) > $numres){
		 $onpage = $numres;
		 }
		 else{
		  $onpage = $start+$limit;
		 }
		
		 if($numres > 0){

		  

		 
		?>

	   
		<div class="container">
						
						<div class="questions-section mb-4">
							
							<div class="d-flex justify-content-between ">
								<form class="form-inline">
							  
							  <div class="form-group mb-2">
								<label for="inputPassword2" class="sr-only">Page No.</label>
								<input type="text" id="page_no" class="form-control mr-3"  placeholder="Page No." autocomplete="off" value="<?php if(isset($_REQUEST['page_no'])){ echo $_REQUEST['page_no'];}else{}?>" >
							  </div>
							  <a class="btn btn-primary mb-2" onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page_no='+$('#page_no').val()+'&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>')"> Go</a>

							  
							</form>
								<div class="form-group d-flex  mb-2">
									<label for="search" class="col-form-label">Search:</label>
									<input class="form-control" id="search" type="search" placeholder="Question Id" autocomplete="off"  value="<?php if(isset($_REQUEST['search'])){ echo $_REQUEST['search'];}else{}?>" placeholder="" onchange="search_report9();"
									 />
								</div>
							</div>
							
					   <!--  <div class="card shadow-sm mb-4">
												<div class="table_data"> -->
						<?php if($numres1 > 0){ ?>
						<div class="card border-0">
							<div class="card-body table-responsive table_data p-0">
								<table class="table table-bordered dashboard_v1-table mb-0" id="">
									<thead class="thead-light">
										<tr>
											<th scope="col">Q.Id</th>
											<th scope="col">Actions</th><th scope="col">Verification Status</th>
											<th scope="col">Question Title</th>
											<th scope="col">Class</th>
											<th scope="col">Exam</th>
											<th scope="col">Subject</th>
											<th scope="col">Chapter</th>
											<th>Issues<br />
											(Solved/Total)</th>
											
										</tr>
									</thead>
									<tbody>
									<?php
									if(isset($_GET['page'])){
									
										if($_GET['page']==1)
										$i=1;
										else
										$i=(($_GET['page']-1)*5)+1;
										}else $i=1;

										$k = 1;
									
										while($value=mysqli_fetch_array($data_sel1)){
											$dtime=$value['timestamp'];
											//$date=date("d-m-Y  H:i:s ", $value["time"]);
											//$date1 = new DateTime("@$dtime");
											$dt=date("d-m-Y  H:i:s ", $value["time"]);
											$value['class']=rtrim($value['class'],',');
											$value['exam']=rtrim($value['exam'],",");
											  if($value['vstatus1']==1){
												$status='<i class="fas fa-check-circle text-success pr-1"></i>Verified';
											  }
											  elseif($value['vstatus1']==2) {
												$status='<i class="fa fa-times-circle text-danger pr-1"></i>Rejected';

											  }
											  else{
												$status='<i class="fas fa-pause-circle text-warning pr-1"></i>Pending';
											  }
											$mn=1;
											$exam='';
											$zSqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$value['exam'].")"); 
											while($rowexam=mysqli_fetch_array($zSqlexam)){
												$exam .= $rowexam['exam'].",";
												$mn++;
											}
											if($value['class']=='1'){
											$class='XI';
											}else if($value['class']=='2'){
												$class='XII';
											}else  if($value['class']=='1,2'){
												$class='XI,XII';
											}
											$chapter2 ='';
											$m=1;
											$zSqll3 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and id IN(".$value['chapter'].")"); 
											
												while($row3=mysqli_fetch_array($zSqll3)){
													$chapter2 .= $row3['chapter'].",";
													$m++;
												}
											$topic ='';
											$k=1;
											$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$value['topic'].")"); 
											while($row1=mysqli_fetch_array($zSql1)){
												$topic .= $row1['topic'].",";
												$k++;
											}
											if($value['list1type']=='roman'){
												$list1type="upper-roman";
											}else if($value['list1type']=='alphabets'){
												$list1type="upper-alpha";
											}else if($value['list1type']=='numbers'){
												$list1type="decimal";
											}else{
												$list1type="upper-alpha";
											}
											if($value['list2type']=='roman'){
												$list2type="upper-roman";
											}else if($value['list2type']=='alphabets'){
												$list2type="upper-alpha";
											}else if($value['list2type']=='numbers'){
												$list2type="decimal";
											}else{
												$list2type="upper-alpha";
											}
											$sqlissue=$database->query("select * from question_issues where estatus='1' and question_id='".$value['id']."'");
											$rowcissue=mysqli_num_rows($sqlissue);

											$sqlissue1=$database->query("select * from question_issues where estatus='1' and question_id='".$value['id']."' and resolve_issue='1'");
											$rowcissue1=mysqli_num_rows($sqlissue1);
										?>
											<tr>
												<?php
												if($_REQUEST['qwc']==1){
												?>
													<td ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#exampleModal4" onClick="setStateGet('viewDetails4','<?php echo SECURE_PATH;?>dashboard_v1/process2.php','viewDetails4=1&type=qwc&id=<?php echo $value['id'];?>')" ><?php echo $value['id']; ?></a></td>
												<?php
												}else if($_REQUEST['r_issue']!=''){
												?>
													<td ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#exampleModal4" onClick="setStateGet('viewDetails4','<?php echo SECURE_PATH;?>dashboard_v1/process2.php','viewDetails4=1&issue=<?php echo $_REQUEST['r_issue']; ?>&id=<?php echo $value['id'];?>')" ><?php echo $value['id']; ?></a></td>
												<?php
												}else{
													?>
													<td><?php echo $value['id']; ?></td>
												<?php
												}
												?>
												<td nowrap class="web-enable-buttons">
													<a  style="cursor:pointer;" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&page=<?php echo $_GET['page']; ?>&editform=<?php echo $value['id'];?>&search=<?php echo $_REQUEST['search'];?>&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>')"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;
													<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>dashboard_v1/process1.php','viewDetails=1&id=<?php echo $value['id'];?>&search=<?php echo $_REQUEST['search'];?>&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>')"><i class="fa fa-eye" style="color:green;"></i></a>&nbsp;
													
													<a data-toggle="modal" data-target="#exampleModal5"  onClick="setStateGet('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a>
												</td>
												<td nowrap class="mobile-enable-buttons d-none">
													<a  style="cursor:pointer;" class="btn btn-primary" onClick="setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','getedit=1&submitform=2&page=<?php echo $_GET['page']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>&editform=<?php echo $value['id'];?>&search=<?php echo $_REQUEST['search'];?>')"><i class="fa fa-pencil-square-o" style="color:white;"></i></a>&nbsp;&nbsp;

													<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal" class="btn btn-success" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>dashboard_v1/process1.php','viewDetails=1&id=<?php echo $value['id'];?>&search=<?php echo $_REQUEST['search'];?>&subject=<?php echo $_REQUEST['subject']; ?>&date=<?php echo $_REQUEST['date']; ?>&qwc=<?php echo $_REQUEST['qwc']; ?>&status=<?php echo $_REQUEST['status']; ?>&rstatus=<?php echo $_REQUEST['rstatus']; ?>&r_issue=<?php echo $_REQUEST['r_issue']; ?>')"><i class="fa fa-eye" style="color:white;"></i></a>&nbsp;
													

													<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal5"  class="btn btn-danger " onClick="setStateGet('viewDetails5','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:white;"></i></a>
												</td>

<td><?php echo $status; ?></td>

											<?php
											if($value['qtype']=='7'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='5'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='8'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left imagewrapper" width="33%"><div  id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='3'){
											
												$list1='';
												$list2='';
												$obj=json_decode($value['question'],true);
												foreach($obj as $rowpost2)
												{
													if(strlen($rowpost2)>0)
													{
														$rowpost = explode("_",$rowpost2);
														$list1.=$rowpost[0]."_";
														$list2.=$rowpost[1]."_";
													}
												}
												$qlist1 = explode("_",$list1);
												$qlist2 = explode("_",$list2);
											?>
												<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none">
													<div class="d-flex justify-content-between">
													<div>
													<h5>List1</h5>
													<ul style="list-style-type: <?php echo $list1type; ?>;padding-left:14px">
													<?php
													foreach($obj as $qqlist)
													{
													
														if(strlen($qqlist['qlist1'])>0)
														{
															echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
														}
													}
													?>
													
													</ul>
													</div>
													<div>
													<h5>List2</h5>
													<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist1)
													{
													
														if(strlen($qqlist1['qlist2'])>0)
														{
															echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
														}
													}
													?>
													</ol>
													</div>
													</div>
													</div>
												</td>
												<?php
												}else if($value['qtype']=='9'){
													$obj=json_decode($value['question'],true);
													?>	
													<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none">
														<div class="d-flex justify-content-between">
														<div>
														<h5>List1</h5>
														<ul style="list-style-type: upper-alpha;padding-left:14px">
														<?php
														foreach($obj as $qqlist)
														{
														
															if(strlen($qqlist['qlist1'])>0)
															{
																echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
															}
														}
														?>
														
														</ul>
														</div>
														<div>
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
														<?php
														foreach($obj as $qqlist2)
														{
														
															if(strlen($qqlist2['qlist2'])>0)
															{
																echo '<li>'.urldecode($qqlist2['qlist2']).'</li>';
															}
														}
														?>
														</ol>
														</div>
														</div>
														</div>
													</td>
													<?php
												}else{
													$question=urldecode($value['question']);
												?>
													<td class="text-left imagewrapper" width="33%"><div  id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}
												?>
												<td> <?php echo $class; ?></td>
												<td><?php echo rtrim($exam,",");?></td>
												<td><?php echo $database->get_name('subject','id',$value['subject'],'subject'); ?></td>
												<td class="text-left"><?php echo rtrim($chapter2,","); ?></td>
												<td class="text-left"><?php echo $rowcissue1; ?>/<?php echo $rowcissue; ?></td>
												
											</tr>
											<?php }?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
						<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
						<?php echo $pagination ;?></div>
				<?php }else{ ?><div style="text-align:center"> No Data Found.</div><?php } ?>		
			
		
				   </div>
			
	   
		<?php
		
    }else{
		?><div style="text-align:center"> No Data Found.</div>
	<?php

	}
	
	}
	

?>
<?php
if(isset($_REQUEST['add_listing']))
{
	$count = $_REQUEST['add_listing'];
?>
	<div class="row justify-content-end">
					
				<div class="col-lg-3 mb-4">
					<?php
					if($count == 1){
					?>
						<a class="btn btn-outline-info px-3" onclick="addList()">Add Question</a>
					<?php
					}
					else{
					?>
						<div class="btn-group">
							<a class="btn btn-outline-info px-4" onclick="addList()">Add Question</a>
							<a class="btn btn-outline-info px-4" onclick="removeList('<?php echo $count; ?>')"><i class="fas fa-minus"></i></a>
						</div>
					<?php
					}
					?>
					<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
				
			</div>
		  
		</div>
		
		
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Question<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="question" rows="3" id="question" value="<?php if(isset($_POST['question'])) { echo $_POST['question']; }?>" class="form-control question "  ></textarea>
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'])){ echo $_SESSION['error']['question'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="question<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['question'])) { echo $_POST['question']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['question'.$count])){ echo $_SESSION['error']['question'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">A)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option1" rows="2" id="option1" value="<?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?>" class="form-control option1"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'])){ echo $_SESSION['error']['option1'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option1<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option1'])) { echo $_POST['option1']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option1'.$count])){ echo $_SESSION['error']['option1'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">B)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option2" rows="2" id="option2" value="<?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?>" class="form-control option2"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'])){ echo $_SESSION['error']['option2'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option2<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option2'])) { echo $_POST['option2']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option2'.$count])){ echo $_SESSION['error']['option2'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">C)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option3" rows="2" id="option3" value="<?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?>" class="form-control option3"  ></textarea>
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'])){ echo $_SESSION['error']['option3'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option3<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option3'])) { echo $_POST['option3']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option3'.$count])){ echo $_SESSION['error']['option3'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">D)<span style="color:red;">*</span></label>
							<div class="col-sm-10">
								<!-- <textarea name="option4" rows="2" id="option4" value="<?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?>" class="form-control option4"  ></textarea>
								<span class="error"><?php if(isset($_SESSION['error']['option4'])){ echo $_SESSION['error']['option4'];}?></span> -->
								<div class="wrs_col wrs_s12">
									 <div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="option4<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['option4'])) { echo $_POST['option4']; }?></textarea> 
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['option4'.$count])){ echo $_SESSION['error']['option4'.$count];}?></span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row correctanswer">
		
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Answer</label>
							<div class="col-sm-8">
								<?php
							
							if(isset($_POST['answer'])){
								if($_POST['answer']!=''){
									$_POST['answer']=explode(",",$_POST['answer']);
								}
								
							}else{
								
							}
							?>
							
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer1<?php echo $count; ?>" id="ans1<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if($_POST['answer']!=''){ if(in_array('A', $_POST['answer'])) { echo 'A'; } } else { echo '0'; } } else { echo 'A'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('A', $_POST['answer'])) { echo 'checked'; } } ?>/> A &nbsp;&nbsp;
											
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer2<?php echo $count; ?>" id="ans2<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'B'; }else { echo ''; } } else { echo 'B'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('B', $_POST['answer'])) { echo 'checked'; } } ?>/> B&nbsp;&nbsp;
								<input type="checkbox" class="tests<?php echo $count; ?>"  onclick="getFields3(<?php echo $count; ?>)"  name="answer3<?php echo $count; ?>" id="ans3<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'C'; }else { echo ''; }  } else { echo 'C'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('C', $_POST['answer'])) { echo 'checked'; } } ?>/> C &nbsp;&nbsp;
								<input type="checkbox" class="tests<?php echo $count; ?>"   onclick="getFields3(<?php echo $count; ?>)"  name="answer4<?php echo $count; ?>"  id="ans4<?php echo $count; ?>" value="<?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'D'; }else { echo ''; } } else { echo 'D'; } ?>" <?php if(isset($_POST['answer'])) { if(in_array('D', $_POST['answer'])) { echo 'checked'; } } ?>/>D

								<input type="hidden" name="answer<?php echo $count; ?>" class="tests<?php echo $count; ?> answer" id="correctanswer<?php echo $count; ?>" value="<?php echo $_POST['answer']; ?>" >
								<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['answer'])){ echo $_SESSION['error']['answer'];}?></span>
							</div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group row">
							<label class="col-sm-2 col-form-label">Explanation</label>
							<div class="col-sm-10">
								<div class="col-sm-10">
									<!-- <textarea name="explanation" rows="3" id="explanation" value="<?php if(isset($_POST['explanation'])) { echo $_POST['question']; }?>" class="form-control explanation"  ></textarea>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span> -->
									<div class="wrs_col wrs_s12">
										 <div id="editorContainer">
											<div id="toolbarLocation"></div>
											<textarea id="explanation<?php echo $count; ?>" class="example wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['explanation'])) { echo $_POST['explanation']; }?></textarea> 
											<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['explanation'.$count])){ echo $_SESSION['error']['explanation'.$count];}?></span>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
				
				
		
<!--Addrow end-->
<?php
}
if(isset($_REQUEST['add_listing1']))
{
	$count1 = $_REQUEST['add_listing1'];
?>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List1 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
				<div class="">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="qlist1<?php echo $count1; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
									
								</div>
							</div>

						</div>
					</div>
				</div>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count1])){ echo $_SESSION['error']['qlist1'.$count1];}?></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List2 Option<?php echo $count1; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
				<div class="">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="qlist2<?php echo $count1; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
									
								</div>
							</div>

						</div>
					</div>
				</div>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count1])){ echo $_SESSION['error']['qlist2'.$count1];}?></span>
			</div>
		</div>
		<div class="col-md-2 form-group" style="padding-top: 6rem;">
			<?php
			if($count1 == 1){
			?>
				<a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a>
			<?php
			}
			else{
			?>
				<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
				<a class="btn btn-danger text-white" onclick="removeList1('<?php echo $count1; ?>')"><i class="fa fa-minus "></i></a>
			<?php
			}
			?>
			
		</div>
	</div>
				
	
<!--Addrow end-->
<?php
}
if(isset($_REQUEST['add_listing2']))
{
	$count2 = $_REQUEST['add_listing2'];
?>

	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List1 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist1" id="qlist1" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qlist1" placeholder="Enter List1 Option" > -->
				<div class="">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="qlist1<?php echo $count2; ?>"  class="example1 wrs_div_box qlist1" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist1'])) { echo urldecode($rowpost['qlist1']); }?></textarea>
									
								</div>
							</div>

						</div>
					</div>
				</div>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist1'.$count2])){ echo $_SESSION['error']['qlist1'.$count2];}?></span>
			</div>
		</div>
		<div class="col-md-5">
			<div class="form-group">
				<label for="exampleInputEmail1">List2 Option<?php echo $count2; ?><span style="color:red;">*</span></label>
				<!-- <input type="text" name="qlist2" id="qlist2" value="<?php if(isset($rowpost[1])) { echo $rowpost[1]; }?>" class="form-control qlist2"  placeholder="Enter List2 Option" > -->
				<div class="">
					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								 <div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="qlist2<?php echo $count2; ?>"  class="example1 wrs_div_box qlist2" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"> <?php if(isset($rowpost['qlist2'])) { echo urldecode($rowpost['qlist2']); }?></textarea>
									
								</div>
							</div>

						</div>
					</div>
				</div>
				<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qlist2'.$count2])){ echo $_SESSION['error']['qlist2'.$count2];}?></span>
			</div>
		</div>
		<div class="col-md-2 form-group" style="padding-top: 6rem;">
			<?php
			if($count2 == 1){
			?>
				<a class="btn btn-success text-white" onclick="addList2()"><i class="fa fa-plus"></i></a>
			<?php
			}
			else{
			?>
				<!-- <a class="btn btn-success text-white" onclick="addList1()"><i class="fa fa-plus"></i></a> -->
				<a class="btn btn-danger text-white" onclick="removeList2('<?php echo $count2; ?>')"><i class="fa fa-minus "></i></a>
			<?php
			}
			?>
			
		</div>
	</div>
	
				
	
<!--Addrow end-->
<?php
}

?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Chapter<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="chapter" id="chapter"   class="form-control" onChange="setState('ddd','<?php echo SECURE_PATH;?>createquestion/process.php','gettopic=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')" >
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>"><?php echo $zData['chapter']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
    <?php
}
if(isset($_REQUEST['gettopic']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Topic<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="topic" id="topic"   class="form-control" >
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `topic` WHERE estatus='1' and class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."' and chapter = '".$_REQUEST['chapter']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>"><?php echo $zData['topic']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
	
    <?php
}


if(isset($_REQUEST['getchapter1']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Chapter<span style="color:red;">*</span></label>
		<div class="col-sm-8">
		<select class="form-control" name="chapter1" value=""   id="chapter1" onChange="search_report();setState('ddd1','<?php echo SECURE_PATH;?>createquestion/process.php','gettopic1=1&class='+$('#class1').val()+'&subject='+$('#subject1').val()+'&chapter='+$('#chapter1').val()+'')">
			<option value=''>-- Select --</option>
			<?php
			$row = $database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
			while($data = mysqli_fetch_array($row))
			{
				?>
			<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['chapter1'])) { if($_POST['chapter1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
			<?php
			}
			?>
		</select>
			</div>
		
    <?php
}
if(isset($_REQUEST['gettopic1']))
{	
	?>
 
	
		<label class="col-sm-4 col-form-label">Topic<span style="color:red;">*</span></label>
		<div class="col-sm-8">
   			<select name="topic1" id="topic1"   class="form-control" onChange="search_report();">
    			<option value="">Select</option>

            		<?php
		
					$zSql = $database->query("SELECT * FROM `topic` WHERE estatus='1' and class = '".$_REQUEST['class']."' and subject = '".$_REQUEST['subject']."' and chapter = '".$_REQUEST['chapter']."'"); 
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'] ?>" <?php if(isset($_POST['topic1'])) { if($_POST['topic1']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $zData['topic']?></option>
							
							<?php
						}
					}
					?>
   				</select>
			</div>
		
	
    <?php
}

?>

	<script type="text/javascript">
function daterangefunction(){
	setState('reportrange','<?php echo SECURE_PATH;?>dashboard_v1/ajax1.php','report=1&reportrange='+$('#reportrange').val()+'');
	
}


</script>
<script type="text/javascript"> 
    
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode

		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		return true;
	}
</script>
<script type="text/javascript">

function getFields1()
{
    var ass='';
    $('.exam').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#exam').val(ass);
	});
        
        

    
}
function getFields()
{
    var ass='';
    $('.tests').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#correctanswer').val(ass);
	});
}
function getFields3(count)
{
    var ass='';
    $('.tests'+count).each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#correctanswer'+count).val(ass);
		console.log(ass);
		//alert(ass);
		
	});
}

function getFields2()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
<script type="text/javascript">
$(function() {
    <?php
    if(isset($_REQUEST['type'])){

    if($_REQUEST['type'] == 'Overall'){
    ?>
    var start = moment().subtract('364','days');
    <?php
    }
    if($_REQUEST['type'] == 'Today'){
    ?>
    var start = moment();
    <?php
    }
    if($_REQUEST['type'] == 'Last 7 Days'){
    ?>
    var start = moment().subtract('6','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 15 Days'){
    ?>
    var start = moment().subtract('14','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 30 Days'){
    ?>
    var start = moment().subtract('29','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 90 Days'){
    ?>
    var start = moment().subtract('89','days');
    <?php
    }
    if($_REQUEST['type'] == 'Last 180 Days'){
    ?>
    var start = moment().subtract('179','days');
    <?php
    }
	if($_REQUEST['type'] == 'Custom Range'){
		$date3=strtotime(date('m/d/Y'));
		$cudate=explode("-",$_REQUEST['reportrange']);
		$date4=strtotime($cudate[0]);
		$datef=$date3-$date4;
		$date6=round($datef / (60 * 60 * 24));
		
    ?>
		var start = moment().subtract(<?php echo $date6; ?>,'days');
	
    <?php
    }
    }
    else{
    ?>
    var start = moment().subtract('364','days');



    <?php
    }
    ?>
    var end = moment();
	
    function cb(start, end,para) {
		console.log("startdate",start, "enddate",end,"para",para);
		if(para!=undefined){
			var a = $('#reportrange span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			//alert(a);
			setState('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1&report=1&type='+para+'&reportrange='+$('#reportrange span').text()+'');
		}
		if(para=='Custom Range'){
			 $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
		}else{
			 $('#reportrange span').html(para);
		}
    }
	
    $('#reportrange').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Overall': [moment().subtract(364,'days'),moment()],
            'Today': [moment(), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'Last 90 Days': [moment().subtract(89, 'days'), moment()],
            'Last 180 Days': [moment().subtract(179, 'days'), moment()]
        }
    }, cb);

	//cb(start, end);
	
});
 
</script>

<script type="text/javascript">

function getFields1()
{
    var ass='';
    $('.exam').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#exam').val(ass);
	});
        
        

    
}
</script>

 <script>
 $(".selectpicker6").selectpicker('refresh');
 $(".selectpicker1").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
   $(".selectpicker8").selectpicker('refresh');
 $(".selectpicker9").selectpicker('refresh');
</script>
<script>
//$('adminTable').hide();
function radiofunction(it){
	//alert(it);
	if(it=='1'){
		$(".yeardiv").show();
	}else{
		$(".yeardiv").hide();
		$("#year").val('');
	}

	
			

 }
  function getexamtype(){
	var examtype=$('#examtype').val();
	if(examtype=='2'){
		$("#pexamtype1").show();
	}else{
		$("#pexamtype1").hide();
	}
}
</script>
<script>
	function contentfunction(){
		var conttype1=$('#conttype').val();
		var conttypenos=$('#conttypenos').val();
		if(conttype1!=''){
			$('#exampleModal1').modal('toggle');
			setStateGet('viewDetails1','<?php echo SECURE_PATH;?>dashboard_v1/process.php','viewDetails1=1&conttype='+$('#conttype').val()+'&conttypenos='+$('#conttypenos').val()+'&class='+$('#class').val()+'&exam='+$('#exam').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&editform=<?php echo $_REQUEST['editform']; ?>');
		}
	}
</script>
<script>
function selectCheckBox()
{

var services='';
    $('.checkBox').each(function(){
    if($(this).is(':checked')) {

   services+=$(this).val()+",";
 } else{
   $('#check').prop('checked', false);
     $('#check').val(0);
     // checkAll();
 }
  });

$('#conttypeids').val(services);


}
    </script>
<?php
if(isset($_REQUEST['viewDetails1']))
{
	$class=trim($_REQUEST['class']);
	$class1=rtrim($class,",");
	$exam=rtrim($_REQUEST['exam'],",");
	if($_REQUEST['topic']!=''){
	
		$sqll2=$database->query("select * from customcontent where estatus='1'  and conttype in (".$_REQUEST['conttype'].") and class='".$class1."'  and exam='".$exam."' and subject='".$_REQUEST['subject']."' and chapter IN (".$_REQUEST['chapter'].") and topic IN (".$_REQUEST['topic'].") ");
		
	}else{
		$sqll2=$database->query("select * from customcontent where estatus='1'  and conttype in (".$_REQUEST['conttype'].") and class='".$class1."'  and exam='".$exam."' and subject='".$_REQUEST['subject']."' and chapter IN (".$_REQUEST['chapter'].") ");
		
	}
	$rowcount=mysqli_num_rows($sqll2);
	if($rowcount>0){
		
			$row['exam']=rtrim($_REQUEST['exam'],',');
			$row['class']=rtrim($class,',');
			$row['chapter']=rtrim($_REQUEST['chapter'],',');
			$row['topic']=rtrim($_REQUEST['topic'],',');
			$row['subject']=$_REQUEST['subject'];
			$row['conttype']=$_REQUEST['conttype'];
			if($row['class']=='1'){
				$class='XI';
			}else if($row['class']=='2'){
				$class='XII';
			}else  if($row['class']=='1,2'){
				$class='XI,XII';
			}
			if($row['exam']=='1'){
				$exam="NEET";
			}else if($row['exam']=='2'){
				$exam="JEE";
			}else if($row['exam']=='1,2'){
				$exam="NEET,JEE";
			}
			$chapter ='';
			$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
			while($row2=mysqli_fetch_array($sql)){
				$chapter .= $row2['chapter'].",";
			}

				$topic ='';
				$k=1;
				$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
				while($row1=mysqli_fetch_array($zSql1)){
					$topic .= $row1['topic'].",";
					$k++;
				}
				$conttype ='';
				$km=1;
				$zSql12 = $database->query("SELECT * FROM customcontent_types WHERE estatus='1' and id IN(".$row['conttype'].")"); 
				while($row2=mysqli_fetch_array($zSql12)){
					$conttype .= $row2['customcontent'].",";
					$km++;
				}
				?>
				<?php
					if($_REQUEST['conttypenos']!='' && $_REQUEST['conttypenos']!='undefined'){
						$_REQUEST['conttypenos']=$_REQUEST['conttypenos'];
					}else{
						$_REQUEST['conttypenos']='';
					}
					?>
				<div class="container">
					<div class="row">
						<div class="col-md-1">
							<h6>Class</h6>
							<p><?php echo $class; ?></p>
						</div>
						
						<div class="col-md-2">
							<h6>Exam</h6>
							<p><?php echo $exam; ?></p>
						</div>
						<div class="col-md-2">
							<h6>Subject</h6>
							<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
						</div>
						<div class="col-md-3">
							<h6>Chapter</h6>
							<p><?php echo rtrim($chapter,","); ?></p>
						</div>
						<div class="col-md-4">
							<h6>Topic</h6>
							<p><?php echo rtrim($topic,","); ?></p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<h6>Content Type</h6>
							<p><?php echo $conttype; ?></p>
						</div>
						

					</div>
					<br />
					<div class="row">
					<table class="table table-bordered">
						<tr>
							<th>Check</th>
							<th>Title</th>
							<th>Description</th>
							
						</tr>
						<?php
						$i=1;
						while($rowl2=mysqli_fetch_array($sqll2)){
							$sqll=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['editform']."' ");
							$rowl=mysqli_fetch_array($sqll);
							//$checkid=explode(",",$rowl['conttypeids']);
							$checkid=explode(",",$_REQUEST['conttypenos']);
							
						//conttypeids
						?>
							<tr>
								
								<td><div class="custom-control custom-checkbox"><input type="checkbox" class="custom-control-input checkBox" onclick="selectCheckBox()" id="check<?php echo $i; ?>" value="<?php echo $rowl2['id'];?>" name="check<?php echo $k; ?>" <?php if(isset($_REQUEST['conttypenos'])) { if(in_array($rowl2['id'], $checkid)) { echo 'checked'; } } ?> ><label class="custom-control-label" for="check<?php echo $i; ?>"   ></label></div></td>
								<td><p><?php echo $rowl2['title']; ?></p></td>
								<td><p><?php echo urldecode($rowl2['description']); ?></p></td>
							</tr>
						<?php  $i++; } ?>
					</table>
					
					<input type="hidden" id="conttypeids" value="<?php echo $_REQUEST['conttypenos']; ?>" >
				</div>	
				<div class="col-lg-6">
					<div class="form-group row">
						<a class="radius-20 btn btn-theme px-5" onClick="$('#exampleModal1').modal('hide');setState('fff','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getconttype=1&conttype=<?php echo $row['conttype']; ?>&conttypeids='+$('#conttypeids').val()+'')"> Save</a>
					</div>
				</div>
		<?php
		
	}else{
		echo "No Data Found";
	}
}
?>

<?php
if(isset($_REQUEST['viewDetails2']))
{
	//$var=explode(",",$_REQUEST['conttypeids']);

	?>
				<div class="container">
					
					<div class="row">
						<div class="col-md-6">
							<h6>Content Type</h6>
							<p><?php echo $database->get_name('customcontent_types','id',$_REQUEST['conttype'],'customcontent'); ?></p>
						</div>
						

					</div>
					<br />
					<div class="row">
					<?php
					$sello=$database->query("select * from customcontent where estatus='1' and id in (".$_REQUEST['conttypeids'].")");
					$rowcount=mysqli_num_rows($sello);
					if($rowcount>0){
					?>
						<table class="table table-bordered">
							<tr>
								<th>ID</th>
								<th>Title</th>
								<th>Description</th>
								<th>Action</th>
								
							</tr>
							<?php
							$i=1;
							while($rowl2=mysqli_fetch_array($sello)){
							?>
								<tr>
									<td><p><?php echo $rowl2['id']; ?></p></td>
									<td><p><?php echo $rowl2['title']; ?></p></td>
									<td><p><?php echo urldecode($rowl2['description']); ?></p></td>
									<td><a class=""  style="cursor:pointer;"  onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>dashboard_v1/process.php','customcontentiddel=<?php echo $rowl2['id'];?>&conttype=<?php echo $_REQUEST['conttype']; ?>&ids=<?php echo $_REQUEST['conttypeids']; ?>')"><i class="fa fa-trash" style="color:red;"  ></i></a></td>
								</tr>
							<?php  $i++; } ?>
						</table>
					<?php }else{
					?>
						<table class="table table-bordered">
							<tr>
								<th>ID</th>
								<th>No Content Available</th>
								
							</tr>
							
						</table>
					<?php
					}
					?>
				</div>	
				
		<?php
		
	
}
?>
<?php
if(isset($_REQUEST['add_issuelist']))
{
	$count3 = $_REQUEST['add_issuelist'];
?>
	
			<form class="form">
				<div class="row">
					<div class="col-md-9 col-sm-12 form-group">
						<label for="inputReason">Select Issue With Section</label>
						<select id="issue_section" class="form-control issue_section">
							<option value=''>-Select-</option>
							<?php
							$sel=$database->query("select * from issue_section where estatus='1'");
							while($data=mysqli_fetch_array($sel)){
								?>
								<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['issue_section'])) { if($_REQUEST['issue_section']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_section'];?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 col-sm-12 form-group">
						<label for="inputReason">Select Issue Type</label>
						<select id="issue_type" class="form-control issue_type">
							<option value=''>-Select-</option>
							<?php
							$sel=$database->query("select * from issue_type where estatus='1'");
							while($data=mysqli_fetch_array($sel)){
								?>
								<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['issue_type'])) { if($_REQUEST['issue_type']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['issue_type'];?></option>
							<?php
							}
							?>
						</select>
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 col-sm-12 form-group">
						<label for="inputReason">Are you resolving the issue?</label>
						<div class="custom-control custom-radio custom-control-inline">
						  <input type="radio" id="resolve_issue1<?php echo $count3; ?>" name="customRadioInline2" class="custom-control-input resolve_issue" value="1" onclick="$('.resolve_issue').val('1')" <?php if(isset($_POST['resolve_issue'])){ if($_POST['resolve_issue'] == '1'){ echo 'checked="checked"';}}else{ echo '';}?> >
						  <label class="custom-control-label" for="resolve_issue1<?php echo $count3; ?>">Yes</label>
						</div>
						<div class="custom-control custom-radio custom-control-inline">
						  <input type="radio" id="resolve_issue2<?php echo $count3; ?>" name="customRadioInline2" class="custom-control-input resolve_issue" value="0" onclick="$('.resolve_issue').val('0')" <?php if(isset($_POST['resolve_issue'])){ if($_POST['resolve_issue'] == '0'){ echo 'checked="checked"';}}else{ echo 'checked';}?> >
						  <label class="custom-control-label" for="resolve_issue2<?php echo $count3; ?>">No</label>
						</div>
						<input type="hidden" class="resolve_issue rissue"  name="resolve_issue" id="resolve_issue" value="<?php if(isset($_POST['resolve_issue'])) { echo $_POST['resolve_issue']; }else{ echo '0'; } ?>" >
					</div>
				</div>
				<div class="row">
					<div class="col-md-9 col-sm-12 form-group">
						<label for="inputReason">Remarks</label> 
						<textarea type="text" id="vremarks" rows="4" class="form-control vremarks"><?php if(isset($_POST['vremarks'])) { echo $_POST['vremarks']; }else{ } ?></textarea>
					</div>
					<div class="col-md-3 form-group pt-5">
						<?php
						if($count3 == 1){
						?>
							<a class="btn btn-success text-white" onclick="addissueList()"><i class="fa fa-plus"></i></a>
						<?php
						}
						else{
						?>
							<a class="btn btn-success text-white" onclick="addissueList()"><i class="fa fa-plus"></i></a>
							<a class="btn btn-danger text-white" onclick="removeissueList('<?php echo $count3; ?>')"><i class="fa fa-minus "></i></a>
						<?php
						}
						?>
					</div>
				</div>
				
			</form>
			
		<input type="hidden" id="issue_llist"  value="<?php echo  $count3;?>"/>
				
	
<!--Addrow end-->
<?php
}
	if(isset($_REQUEST['issuedataval'])){
		$_SESSION['error'] = array();

		$post = $session->cleanInput($_POST);
		$abcd = 1;
			$list = explode("^",rtrim($post['issuedata'],'^'));
			if(count($list)>0)
			{
			foreach ($list as $k)
			{
				
				$r = explode('_',$k);
				if(strlen(trim($r['0'])) == 0 ){
				  $_SESSION['error']['issue_section'.$abcd] = "Please Select Issue Section";
				}
				if(strlen(trim($r['1'])) == 0 ){
				  $_SESSION['error']['issue_type'.$abcd] = "Please Select Issue Type";
				}
				if(strlen(trim($r['2'])) == '' ){
				  $_SESSION['error']['resolve_issue'.$abcd] = "Please Select Resolving Issue";
				}
				if(strlen(trim($r['3'])) == 0 ){
				  $_SESSION['error']['vremarks'.$abcd] = "Please Enter Remarks";
				}
				
				
				$abcd++;
			}
		
			if(count($_SESSION['error']) > 0 ){
			
			?>
				<script type="text/javascript">
				//$('#adminForm').show();

				setState('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=1&issueparams=1&question_id=<?php echo $post['question_id'];?>&issuedata=<?php echo $post['issuedata'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				
				</script>

			<?php
			}
			else{
				if($id!='NULL')
				{
					
					$list = explode("^",rtrim($post['issuedata'],'^'));
					
					if(count($list)>0)
					{
						foreach ($list as $k)
						{
							$r = explode('_',$k);
							
							$sql=$database->query("select max(issue_id) as issue_id from question_issues where estatus='1' and question_id='".$post['question_id']."'");
							$rcount=mysqli_num_rows($sql);
							if($rcount>0){
								$row=mysqli_fetch_array($sql);
								$i1=explode("_",$row['issue_id']);
								$i2=$i1[1]+1;
								$issue_id="Q".$post['question_id']."_".$i2;
								
								
							}else{
								$issue_id="Q".$post['question_id']."_1";
								
							}
							
							$database->query("insert question_issues set question_id='".$post['question_id']."',issue_id='".$issue_id."',issue_section='".$r[0]."',issue_type='".$r[1]."',resolve_issue='".$r[2]."',vremarks='".$r[3]."',username='".$_SESSION['username']."',timestamp='".time()."'");
							$lid = mysqli_insert_id($database->connection);
							
							
						}
						$database->query("update createquestion set vstatus1='2',vtimestamp1='".time()."',vusername1='".$_SESSION['username']."' where id='".$post['question_id']."'");

						$database->query("insert question_log set question_id='".$post['question_id']."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");
					
					}
					?>
						<!-- <div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Issue  Created Successfully</div> -->
						<!-- <script type="text/javascript">
							$('#datafill').hide();
							alert("Issue  Created Successfully");
							setStateGet('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=1&question_id=<?php echo $post['question_id']; ?>');
							
						</script> -->
						<script type="text/javascript">
							$('#datafill').hide();
							alert("Issue  Created Successfully");
							setStateGet('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=1&question_id=<?php echo $post['question_id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
							setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=2&getedit=1&editform=<?php echo $post['question_id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');

						</script>
						<!--<script type="text/javascript">
							$('#adminForm').show();
							$('#datafill').show();
							$('#exampleModalCenter').modal('hide');
							alert(" Question Reporting Completed Sucessfully");
							setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
							setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
						</script>-->
					<?php
				}	
			}
				
		}
	}



	if(isset($_REQUEST['issuedatava2'])){
		$_SESSION['error'] = array();

		$post = $session->cleanInput($_POST);
		
			
			$database->query("update question_issues set question_id='".$post['question_id']."',resolve_issue='1' where estatus='1' and id='".$post['issue']."'");

			$sqll4=$database->query("select * from question_issues where estatus='1' and resolve_issue='1' and question_id='".$_REQUEST['editform']."'");
			$rowll4=mysqli_num_rows($sqll4);

			$sqll41=$database->query("select * from question_issues where estatus='1'  and question_id='".$_REQUEST['editform']."'");
			$rowll41=mysqli_num_rows($sqll41);
			
			
			?>
			 <!--<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Issue  Created Successfully</div> -->
			 <script type="text/javascript">
				$('#datafill').hide();
				alert("Issue  Resolved Successfully");
				setStateGet('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=1&question_id=<?php echo $post['question_id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				setState('formSubmit','<?php echo SECURE_PATH;?>dashboard_v1/process.php','submitform=2&getedit=1&editform=<?php echo $post['question_id']; ?>&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				
				</script> 
				
			<?php
				
			
				
		
	}

	if(isset($_REQUEST['issuedataval1'])){
		$_SESSION['error'] = array();

		$post = $session->cleanInput($_POST);
		
		$sql=$database->query("select * from question_issues where estatus='1' and question_id='".$_REQUEST['question_id']."'");
		$rcount=mysqli_num_rows($sql);
		if($rcount>0){
			$database->query("update createquestion set vstatus1='".$post['report']."',vtimestamp1='".time()."',vusername1='".$_SESSION['username']."' where id='".$post['question_id']."'");

			$database->query("insert question_log set question_id='".$post['question_id']."',username='".$_SESSION['username']."',message='2',module='dashboard',estatus='1',timestamp='".time()."'");
			if($post['report']=='2'){
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Question Reporting Completed Sucessfully</div>
				<script type="text/javascript">
					$('#adminForm').show();
					$('#datafill').show();
					$('#exampleModalCenter').modal('hide');
					alert(" Question Reporting Completed Sucessfully");
					setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
					setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				</script>
			<?php }else{ ?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Question Verified Sucessfully</div>
				<script type="text/javascript">
					$('#adminForm').show();
					$('#datafill').show();
					$('#exampleModalCenter').modal('hide');
					alert(" Question Reporting Completed Sucessfully");
					setStateGet('adminForm','<?php echo SECURE_PATH;?>dashboard_v1/process.php','addForm=1');
					setStateGet('adminTable','<?php echo SECURE_PATH;?>dashboard_v1/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				</script>
			<?php 
				} 
		}else{
		?>
			
				<script type="text/javascript">
					$('#datafill').hide();
					alert(" Please fill the issue");
					setStateGet('rowReject','<?php echo SECURE_PATH;?>dashboard_v1/process.php','rowReject=1&question_id=<?php echo $_REQUEST['question_id']; ?>');
					
				</script>
		<?php
		}
	}
	?>

	<?php

	if(isset($_REQUEST['versionval'])){
		if($_REQUEST['version']=='V1'){
		$sello=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
		$rowcount1=mysqli_num_rows($sello);
	}else if($_REQUEST['version']=='V2'){
		$sello=$database->query("select * from version_question where estatus='1' and question_id='".$_REQUEST['question_id']."'");
		
		$rowcount1=mysqli_num_rows($sello);
	}else{
		$sello=$database->query("select * from createquestion where estatus='1' and id='".$_REQUEST['question_id']."'");
		$rowcount1=mysqli_num_rows($sello);
	}
	if($rowcount1>0){
		$row=mysqli_fetch_array($sello);

		if($row['list1type']=='roman'){
			$list1type="upper-roman";
		}else if($row['list1type']=='alphabets'){
			$list1type="upper-alpha";
		}else if($row['list1type']=='numbers'){
			$list1type="decimal";
		}else{
			$list1type="upper-alpha";
		}
		if($row['list2type']=='roman'){
			$list2type="upper-roman";
		}else if($row['list2type']=='alphabets'){
			$list2type="upper-alpha";
		}else if($row['list2type']=='numbers'){
			$list2type="decimal";
		}else{
			$list2type="upper-alpha";
		}
		$row['exam']=rtrim($row['exam'],',');
		$row['class']=rtrim($row['class'],',');
		$row['chapter']=rtrim($row['chapter'],',');
		$row['topic']=rtrim($row['topic'],',');
		
		if($row['class']=='1'){
			$class='XI';
		}else if($row['class']=='2'){
			$class='XII';
		}else  if($row['class']=='1,2'){
			$class='XI,XII';
		}
		if($row['exam']=='1'){
			$exam="NEET";
		}else if($row['exam']=='2'){
			$exam="JEE";
		}else if($row['exam']=='1,2'){
			$exam="NEET,JEE";
		}
		$chapter ='';
		$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
		while($row2=mysqli_fetch_array($sql)){
			$chapter .= $row2['chapter'].",";
		}

			$topic ='';
			$k=1;
			$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
			while($row1=mysqli_fetch_array($zSql1)){
				$topic .= $row1['topic'].",";
				$k++;
			}
			$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
			while($row2=mysqli_fetch_array($zSql2)){
				$questiontype .= $row2['questiontype'].",";
				$k++;
			}
			?>
			<div class="container">
				<div class="row">
					<?php
					$row['exam']=rtrim($row['exam'],',');
					$row['class']=rtrim($row['class'],',');
					$row['chapter']=rtrim($row['chapter'],',');
					$row['topic']=rtrim($row['topic'],',');
					
					if($row['class']=='1'){
						$class='XI';
					}else if($row['class']=='2'){
						$class='XII';
					}else  if($row['class']=='1,2'){
						$class='XI,XII';
					}
					if($row['exam']=='1'){
						$exam="NEET";
					}else if($row['exam']=='2'){
						$exam="JEE";
					}else if($row['exam']=='1,2'){
						$exam="NEET,JEE";
					}
					$chapter ='';
					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
					while($row2=mysqli_fetch_array($sql)){
						$chapter .= $row2['chapter'].",";
					}

						$topic ='';
						$k=1;
						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
						while($row1=mysqli_fetch_array($zSql1)){
							$topic .= $row1['topic'].",";
							$k++;
						}
						?>
						<div class="col-md-1">
							<h6>Class</h6>
							<p><?php echo $class; ?></p>
						</div>
						
						<div class="col-md-2">
							<h6>Exam</h6>
							<p><?php echo $exam; ?></p>
						</div>
						<div class="col-md-2">
							<h6>Subject</h6>
							<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
						</div>
						<div class="col-md-3">
							<h6>Chapter</h6>
							<p><?php echo rtrim($chapter,","); ?></p>
						</div>
						<div class="col-md-4">
							<h6>Topic</h6>
							<p><?php echo rtrim($topic,","); ?></p>
						</div>
					
					<?php
					if($row['qtype']=='5'){
						$type="Comprehension";
					?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Explanation</h6>
							<div><?php echo urldecode($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo $row['question']; ?></div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A)</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option4']; ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo $row['explanation']; ?></div>
									

							</div>
						<?php } ?>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else if($row['qtype']=='8'){
						$type="Integer";
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo $row['question']; ?></div>
						</div>
						
						
						<div class="col-md-12 mt-2 questionView">
							<h6>answer</h6>
							<div><?php echo rtrim($row['answer'],","); ?></div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
					<?php
					}else if($row['qtype']=='3'){
						$type="Matching";
					
						$list1='';
						$list2='';
						$obj=json_decode($row['question'],true);
						foreach($obj as $rowpost2)
						{
							if(strlen($rowpost2)>0)
							{
								$rowpost = explode("_",$rowpost2);
								$list1.=$rowpost[0]."_";
								$list2.=$rowpost[1]."_";
							}
						}
						$qlist1 = explode("_",$list1);
						$qlist2 = explode("_",$list2);
						if($row['list1type']=='roman'){
							$list1type="upper-roman";
						}else if($row['list1type']=='alphabets'){
							$list1type="upper-alpha";
						}else if($row['list1type']=='numbers'){
							$list1type="decimal";
						}else{
							$list1type="upper-alpha";
						}
						if($row['list2type']=='roman'){
							$list2type="upper-roman";
						}else if($row['list2type']=='alphabets'){
							$list2type="upper-alpha";
						}else if($row['list2type']=='numbers'){
							$list2type="decimal";
						}else{
							$list2type="upper-alpha";
						}
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							
							<div class="questionlist-types d-flex">
								<div>
								<h5>List1</h5>
									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
										<?php
										foreach($qlist1 as $qqlist)
										{
										
											if(strlen($qqlist)>0)
											{
												echo '<li style="width:150px;">'.$qqlist.'</li>';
											}
										}
										?>
									
									</ul>
								</div>
								<div class="pl-5">
									<h5>List2</h5>
									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
										<?php
										foreach($qlist2 as $qqlist1)
										{
											if(strlen($qqlist1)>0)
											{
												echo '<li style="width:150px;">'.$qqlist1.'</li>';
											}
										}
										?>
									</ol>
								</div>
							</div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option4']; ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo $row['explanation']; ?></div>
									

							</div>
						<?php } ?>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else if($row['qtype']=='9'){
						$type="Matrix";
						$list1='';
						$list2='';
						$obj=json_decode($row['question'],true);
						foreach($obj as $rowpost2)
						{
							if(strlen($rowpost2)>0)
							{
								$rowpost = explode("_",$rowpost2);
								$list1.=$rowpost[0]."_";
								$list2.=$rowpost[1]."_";
							}
						}
						$qlist1 = explode("_",$list1);
						$qlist2 = explode("_",$list2);
						if($row['list1type']=='roman'){
							$list1type="upper-roman";
						}else if($row['list1type']=='alphabets'){
							$list1type="upper-alpha";
						}else if($row['list1type']=='numbers'){
							$list1type="decimal";
						}else{
							$list1type="upper-alpha";
						}
						if($row['list2type']=='roman'){
							$list2type="upper-roman";
						}else if($row['list2type']=='alphabets'){
							$list2type="upper-alpha";
						}else if($row['list2type']=='numbers'){
							$list2type="decimal";
						}else{
							$list2type="upper-alpha";
						}
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							
								<div class="questionlist-types d-flex">
									<div>
									<h5>List1</h5>
										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
											<?php
											foreach($qlist1 as $qqlist)
											{
											
												if(strlen($qqlist)>0)
												{
													echo '<li style="width:150px;">'.$qqlist.'</li>';
												}
											}
											?>
										
										</ul>
									</div>
									<div class="pl-5">
										<h5>List2</h5>
										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
											<?php
											foreach($qlist2 as $qqlist1)
											{
												if(strlen($qqlist1)>0)
												{
													echo '<li style="width:150px;">'.$qqlist1.'</li>';
												}
											}
											?>
										</ol>
									</div>
								</div>
							
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option4']; ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo $row['explanation']; ?></div>
									

							</div>
						<?php } ?>
						<?php if($row['expla_vlink']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation Video Link</h6>
								<div><?php echo $row['expla_vlink']; ?></div>
									

							</div>
						<?php } ?>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else{
						$type="General";
					?>
						<div class="col-md-3">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo urldecode($row['question']); ?></div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option1']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option2']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option3']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option4']); ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
						<?php if($row['expla_vlink']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation Video Link</h6>
								<div><?php echo $row['expla_vlink']; ?></div>
									

							</div>
						<?php } ?>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}
					?>
					<?php if($row['complexity']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Complexity</h6>
							<p><?php echo $database->get_name('complexity','id',$row['complexity'],'complexity'); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['timeduration']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Answering Time Duration</h6>
							<p><?php echo $row['timeduration']; ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['inputquestion']!=''){
					$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
					while($row2=mysqli_fetch_array($zSql2)){
						$questiontype .= $row2['questiontype'].",";
						$k++;
					}
					?>
						<div class="col-md-12 mt-2">
							<h6>Type of Question</h6>
							<p><?php echo rtrim($questiontype,","); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['usageset']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Usage Set</h6>
							<p><?php echo $database->get_name('question_useset','id',$row['usageset'],'usageset'); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['question_theory']!='' && $row['question_theory']!='0'){ ?>
						<div class="col-md-12 mt-2">
							<h6>Question Theory</h6>
							<p><?php echo $database->get_name('question_theory','id',$row['question_theory'],'question_theory'); ?></p>
								

						</div>
					<?php } ?>
					
				</div>
			</div>
		<?php
		}
							
	}
	?>
	
<?php
//Delete createticket
if(isset($_GET['customcontentiddel'])){
	$array=explode(",",$_REQUEST['ids']);
	
	if (($key = array_search($_REQUEST['customcontentiddel'], $array)) !== false) {
		unset($array[$key]);
	}
	$array1=implode(",",$array);
	?>
    
	<script type="text/javascript">

		setStateGet('viewDetails2','<?php echo SECURE_PATH;?>dashboard_v1/process.php','viewDetails2=1&conttypeids=<?php echo $array1; ?>');
		setState('fff','<?php echo SECURE_PATH;?>dashboard_v1/ajax.php','getconttype=1&conttype=<?php echo $_REQUEST['conttype']; ?>&conttypeids=<?php echo $array1; ?>');
	</script>
   <?php
}
?>
	<script>
function numberrange(){

var number=$('#correctanswer').val();
  
  if(number<-9999999999.00 || number>9999999999.99 ){
	alert('Answer between -9999999999.00 to +9999999999.99 only');
     $('#correctanswer').val('');
  }
  else{
    $('#correctanswer').val(number);
  }

}
</script>
	<script>
	$(".exampicker1").selectpicker('refresh');
	$(".selectpickerd").selectpicker('refresh');
	function getquestionview1(){
		$('.getquestionview').show();
		$('.questionViewdata').hide();
	}
	function getquestionview2(){
		$('.getquestionview').hide();
		$('.questionViewdata').show();
		

	}
	</script>
	<script type="text/javascript" >
		//
		createEditorInstance("en", {});
	
	</script>

