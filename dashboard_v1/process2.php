<?php
include('../include/session.php');
error_reporting(0);
?>
<script src="../edut/js/wirisdisplay.js"></script>
<!-- <script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script> -->
<?php

	if(isset($_REQUEST['viewDetails4']))
	{
		if(isset($_REQUEST['r_issue'])){
			if($_REQUEST['r_issue']!=''){
				$rissue=" AND changes like '%".$_REQUEST['r_issue']."%'";
			}else{
				$rissue="";
			}
		}else{
			$rissue="";
		}
	?>	
		<div class="container">
			<table class="table table-bordered">
			  <thead>
				<tr>
				  <th scope="col">Sr.No</th>
				  <th scope="col">Attribute</th>
				  <th scope="col">New Value</th>
				  <th scope="col">Old Value</th>
				</tr>
			  </thead>
			  <tbody>
			  	<?php
		
				$sql=$database->query("select * from reviewer_log where estatus='1' and message='1' and question_id='".$_REQUEST['id']."' ".$rissue." ");
				$rowl=mysqli_fetch_array($sql);
				$changes=$rowl['changes'];
				$data=explode("^",$changes);
				$a=1;
				if(count($data)>0){
					foreach($data as $dataa){
						$data1=explode("_",$dataa);
						if($data1[0]!=''){
							if($data1[0]=='inputquestion'){
								$type="Question Type";
							}else{
								$type=$data1[0];
							}
							$new='';
							$old='';
							if($data1[0]=='class'){
								$classn='';
								$classo='';
								$sel=$database->query("select * from class where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$classn.=$rowe['class'].",";
								}
								$new.=rtrim($classn,",");
								$sel=$database->query("select * from class where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$classo.=$rowe['class'].",";
								}
								$old.=rtrim($classo,",");
							}
							if($data1[0]=='exam'){
								$examn='';
								$examo='';
								$sel=$database->query("select * from exam where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$examn.=$rowe['exam'].",";
								}
								$new.=rtrim($examn,",");
								$sel=$database->query("select * from exam where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$examo.=$rowe['exam'].",";
								}
								$old.=rtrim($examo,",");
							}
							if($data1[0]=='chapter'){
								$chaptern='';
								$chaptero='';
								$sel=$database->query("select * from chapter where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$chaptern.=$rowe['chapter'].",";
								}
								$new.=rtrim($chaptern,",");
								$sel=$database->query("select * from chapter where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$chaptero.=$rowe['chapter'].",";
								}
								$old.=rtrim($chaptero,",");
							}
							if($data1[0]=='topic'){
								$topicn='';
								$topico='';
								$sel=$database->query("select * from topic where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$topicn.=$rowe['topic'].",";
								}
								$new.=rtrim($topicn,",");
								$sel=$database->query("select * from topic where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$topico.=$rowe['topic'].",";
								}
								$old.=rtrim($topico,",");
							}
							if($data1[0]=='complexity'){
								
								$sel=$database->query("select * from complexity where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$new.=$rowe['complexity'];
								}
								$sel=$database->query("select * from complexity where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$old.=$rowe['complexity'];
								}
							}
							if($data1[0]=='timeduration'){
								
								$new.=$data1[4];
								
								$old.=$data1[2];
								
							}
							if($data1[0]=='usageset'){
								
								$sel=$database->query("select * from question_useset where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$new.=$rowe['usageset'];
								}
								$sel=$database->query("select * from question_useset where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$old.=$rowe['usageset'];
								}
							}
							if($data1[0]=='questiontheory'){
								
								$sel=$database->query("select * from question_theory where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$new.=$rowe['question_theory'];
								}
								$sel=$database->query("select * from question_theory where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$old.=$rowe['question_theory'];
								}
							}
							if($data1[0]=='inputquestion'){
								$qtypen='';
								$qtypeo='';
								$sel=$database->query("select * from questiontype where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$qtypen.=$rowe['questiontype'].",";
								}
								$new.=rtrim($qtypen,",");
								$sel=$database->query("select * from questiontype where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$qtypeo.=$rowe['questiontype'].",";
								}
								$old.=rtrim($qtypeo,",");
							}
							if($data1[0]=='conttype'){
								
								$sel=$database->query("select * from customcontent_types where estatus='1' and id in (".$data1[4].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$new.=$rowe['customcontent'];
								}
								$sel=$database->query("select * from customcontent_types where estatus='1' and id in (".$data1[2].") ");
								while($rowe=mysqli_fetch_array($sel)){
									$old.=$rowe['customcontent'];
								}
							}
							
							?>
								<tr>
									<td><?php echo $a; ?></td>
								  <td><?php echo $type; ?></td>
								  <td><?php echo $new; ?></td>
								  <td><?php echo $old; ?></td>
								 
								</tr>
							<?php
							
						
						$a++;
						}
					}
				}
					?>
				
				
			  </tbody>
			</table>
		</div>
		
	<?php 
		
	}

 ?>