<?php
include('../include/session.php');
if(isset($_REQUEST['report']))
{
	
	?>
		
		<table class="table table-bordered mb-0">
                        <thead>
                            <tr>
                                <th class="bg-primary">
                                    <h5 class="text-white">Total Entered</h5>
                                    <h3 class="text-white">20301</h3>
                                </th>
                                <th class="bg-light" colspan="2">
                                    <h5>Today</h5>
                                    <h3>250</h3>
                                </th>
                                <th colspan="2">
                                    <h5>Yesterday</h5>
                                    <h3>150</h3>
                                </th>
                                <th class="bg-light" colspan="2">
                                    <h5>Last 3 Days</h5>
                                    <h3>500</h3>
                                </th>
                                <th colspan="2">
                                    <h5>Last 5 Days</h5>
                                    <h3>1500</h3>
                                </th>
                                <th colspan="2" class="bg-light">
                                    <h5>Last 7 Days</h5>
                                    <h3>2010</h3>
                                </th>
                                <th rowspan='2' class="p-0">
                                    <div class="date-dropdown ">
										<i class="far fa-calendar-alt"></i>
										
										<input  type="text" id="reportrange1"  class="form-control"   value='' onchange="setState('adminForm','<?php echo SECURE_PATH;?>datadashboard/ajax1.php','addForm=1&report=1&reportrange='+$('#reportrange').val()+'');" >
										
										</div>
                                    <div class="p-2">
                                        <h6>Total Rejected</h6>
                                        <h3>1823</h3>
                                    </div>
                                </th>
                            </tr>
                        </thead>
                        <tbody class="body-bordered-table">
                            <tr class="row-table">
                                <td>
                                    <h6>NEET</h6>
                                    <h4>8,120 </h4>
                                </td>
                                <td rowspan='2' class="rotate-td bg-gray-color">
                                    <h4>Class XI</h4>
                                </td>
                                <td>
                                    <h6>Biology</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>physics</h6>
                                    <h4>5,075</h4>
                                </td>
                                <td>
                                    <h6>Chemistry</h6>
                                    <h4>5,075</h4>
                                </td>
                                <td>
                                    <h6>Zoology</h6>
                                    <h4>5,075</h4>
                                </td>
                                <td rowspan="2" class="rotate-td bg-gray-color">
                                    <h4>Class XII</h4>
                                </td>
                                <td>
                                    <h6>Biology</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>physics</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>Chemistry</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>Zoology</h6>
                                    <h4>5,075</h4>
                                </td>
                                <td rowspan='2' class='py-0 last-child'>
                                    <h6>Today Rejected</h6>
                                    <h3>18</h3>
                                </td>

                            </tr>
                            <tr>
                                <td>
                                    <h6>JEE</h6>
                                    <h4>12,180</h4>
                                </td>
                                <td>
                                    <h6>Mathematics</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td colspan="2">
                                    <h6>physics</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>Chemistry</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>Mathematics</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td colspan="2">
                                    <h6>physics</h6>
                                    <h4>5,075 </h4>
                                </td>
                                <td>
                                    <h6>Chemistry</h6>
                                    <h4>5,075 </h4>
                                </td>

                            </tr>
                        </tbody>
                    </table>
<?php
}

	?>

