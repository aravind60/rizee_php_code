<?php
include('../include/session.php');
error_reporting(0);
?>

<script src="../edut/js/wirisdisplay.js"></script>
<?php

if(isset($_REQUEST['viewDetails1']))
{
	if($_REQUEST['list1type']=='roman'){
			$list1type="upper-roman";
		}else if($_REQUEST['list1type']=='alphabets'){
			$list1type="upper-alpha";
		}else if($_REQUEST['list1type']=='numbers'){
			$list1type="decimal";
		}else{
			$list1type="upper-alpha";
		}
		if($_REQUEST['list2type']=='roman'){
			$list2type="upper-roman";
		}else if($_REQUEST['list2type']=='alphabets'){
			$list2type="upper-alpha";
		}else if($_REQUEST['list2type']=='numbers'){
			$list2type="decimal";
		}else{
			$list2type="upper-alpha";
		}
		$list=$_SESSION['data5'];
		
	?>
	<div class="container">
				<div class="row">
					<?php
					$_REQUEST['exam']=rtrim($_REQUEST['exam'],',');
					$_REQUEST['class']=rtrim($_REQUEST['class'],',');
					$_REQUEST['chapter']=rtrim($_REQUEST['chapter'],',');
					$_REQUEST['topic']=rtrim($_REQUEST['topic'],',');
					
					if($_REQUEST['class']=='1'){
						$class='XI';
					}else if($_REQUEST['class']=='2'){
						$class='XII';
					}else  if($_REQUEST['class']=='1,2'){
						$class='XI,XII';
					}
					if($_REQUEST['exam']=='1'){
						$exam="NEET";
					}else if($_REQUEST['exam']=='2'){
						$exam="JEE";
					}else if($_REQUEST['exam']=='1,2'){
						$exam="NEET,JEE";
					}
					$chapter ='';
					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$_REQUEST['chapter'].")"); 
					while($row2=mysqli_fetch_array($sql)){
						$chapter .= $row2['chapter'].",";
					}

						$topic ='';
						$k=1;
						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$_REQUEST['topic'].")"); 
						while($row1=mysqli_fetch_array($zSql1)){
							$topic .= $row1['topic'].",";
							$k++;
						}
						?>
					<div class="col-md-2">
						<h6>Class</h6>
						<p><?php echo $class; ?></p>
					</div>
					
					<div class="col-md-2">
						<h6>Exam</h6>
						<p><?php echo $exam; ?></p>
					</div>
					<div class="col-md-2">
						<h6>Subject</h6>
						<p><?php echo $database->get_name('subject','id',$_REQUEST['subject'],'subject'); ?></p>
					</div>
					<div class="col-md-6">
						<h6>Chapter</h6>
						<p><?php echo rtrim($chapter,","); ?></p>
					</div>
					<?php
					 if($_REQUEST['qtype']=='3'){
						$type="Matching";
					
						$list1='';
						$list2='';
						
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<?php echo urldecode($_SESSION['mat_question']); ?>
							<br />
							<?php
							if($_SESSION['mat_que_paragraph']!=''){ 
								echo urldecode($_SESSION['mat_que_paragraph']);
								echo '<br />';
							} 
							
							?>
							<div class="questionlist-types d-flex">
								<div>
								<h5>List1</h5>
									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
										<?php
							
										foreach($list as $qqlist)
										{
										
											if(strlen($qqlist['qlist1'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
											}
										}
										?>
									
									</ul>
								</div>
								<div class="pl-5">
									<h6>List2</h6>
									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
										<?php
							
										foreach($list as $qqlist1)
										{
										
											if(strlen($qqlist1['qlist2'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist1['qlist2']).'</li>';
											}
										}
										?>
									</ol>
								</div>
							</div>
							<?php
							if($_SESSION['mat_que_footer']!=''){ 
								$mat_quefooter=urldecode($_SESSION['mat_que_footer']);
								?>
								<?php echo $mat_quefooter; ?>
								
							<br />
							<?php
							} 
							?>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option4']; ?>
								</div>
								<?php if($_REQUEST['option5']!=''){ ?>
									<div class="col-md-1 py-2 ">
										(E)						</div>

									<div class="col-md-5 py-2 ">
										<?php echo $_REQUEST['option5']; ?>
									</div>
								<?php } ?>
							</div>
						</div>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($_REQUEST['answer'],","); ?></p>
								

						</div>
						<div class="col-md-12 mt-2">
							<h6>Explanation</h6>
							<p><?php echo urldecode($_SESSION['explanation']); ?></p>
								

						</div>
					<?php
					}else if($_REQUEST['qtype']=='9'){
						$type="Matrix";
					
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<?php echo urldecode($_SESSION['mat_question']); ?>
							<br />
							<?php
							if($_SESSION['mat_que_paragraph']!=''){ 
								echo urldecode($_SESSION['mat_que_paragraph']);
								echo '<br />';
							} 
							
							?>
								<div class="questionlist-types d-flex">
									<div>
									<h6>List1</h6>
										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
											<?php
							
										foreach($list as $qqlist)
										{
										
											if(strlen($qqlist['qlist1'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
											}
										}
										?>
										
										</ul>
									</div>
									<div class="pl-5">
										<h5>List2</h5>
										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
											<?php
							
												foreach($list as $qqlist)
												{
												
													if(strlen($qqlist['qlist2'])>0)
													{
														echo '<li style="width:150px;">'.urldecode($qqlist['qlist2']).'</li>';
													}
												}
												?>
										</ol>
									</div>
								</div>
							<?php
							if($_SESSION['mat_que_footer']!=''){ 
								$mat_quefooter=urldecode($_SESSION['mat_que_footer']);
								?>
								<?php echo $mat_quefooter; ?>
							<br />
							<?php
							} 
							?>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option4']; ?>
								</div>
								
								<?php if($_REQUEST['option5']!=''){ ?>
									<div class="col-md-1 py-2 ">
										(E)						</div>

									<div class="col-md-5 py-2 ">
										<?php echo $_REQUEST['option5']; ?>
									</div>
								<?php } ?>
							</div>
						</div>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($_REQUEST['answer'],","); ?></p>
								

						</div>
						<div class="col-md-12 mt-2">
							<h6>Explanation</h6>
							<p><?php echo urldecode($_SESSION['explanation']); ?></p>
								

						</div>
					<?php
					}
						?>
					
				</div>
			</div>
			
			

	
<?php }

 ?>