<?php
    error_reporting(0);
    include('../include/session.php');

	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
        //unset($_SESSION['image']);  
    ?>
    
   
		
		<script type="text/javascript">
		if (window.location.search !== '') {
		    var urlParams = window.location.search;
		    if (urlParams[0] == '?') {
		        urlParams = urlParams.substr(1, urlParams.length);
		        urlParams = urlParams.split('&');
		        for (i = 0; i < urlParams.length; i = i + 1) {
		            var paramVariableName = urlParams[i].split('=')[0];
		            if (paramVariableName === 'language') {
		                _wrs_int_langCode = urlParams[i].split('=')[1];
		                break;
		            }
		        }
		    }
		}
    </script>
	 
	

        <script>
			if(typeof urlParams !== 'undefined') {
				var selectLang = document.getElementById('lang_select');
				selectLang.value = urlParams[1];
			}
			
			function getContent(){
			
			
			
			
			    var inst, contents = new Object();
				
for (inst in tinyMCE.editors) {
    if (tinyMCE.editors[inst].getContent)
        contents[inst] = tinyMCE.editors[inst].getContent();
		}
			
			
				   console.log('editor 1', contents[0]);
			   console.log('editor 2', contents[1]);
			   console.log('editor 3', contents[2]);
			
		
			
			
			}
			
			
			
		</script>
		
		
		<div class="content-wrapper">
<!--             <section class="breadcrumbs-area2 my-3">
				<div class="container">
					<div class="d-flex justify-content-between align-items-center">
						<div class="title">
							<h1 class="text-uppercase">Users</h1>
						</div>
					</div>
					<hr>
				</div>
			</section>  -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">

							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body reject-modal text-left" id="rowReject" >
							
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Timeline Overview</h5>
						<button type="button"  onClick="$('#viewDetails').html('')" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="viewDetails">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary"  onClick="$('#viewDetails').html('')" data-dismiss="modal">Close</button>
						<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
					</div>
					</div>
				</div>
			</div>
			<div class="modal fade " id="messageDetails1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Users assigned by Chapters</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails1">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
							<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
						</div>
						</div>
					</div>
				</div>
          <section class="content-area" id="start_div" style="display: none;">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="card border-0 shadow mb-4">
                                        <div
                                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                            <h6 class="m-0 font-weight-bold text-primary">Add User</h6>
                                        </div>
                                        <div class="card-body">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-8">
                                                              
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="adminForm">
                                                            <div class="col-sm-8">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

       
			<section>
                <div class="container">
                    
                            <div class="form-group m-0" id="adminForm2">
								<div class="col-sm-8">
									<script type="text/javascript">
										setStateGet('adminForm2','<?php echo SECURE_PATH;?>dataentryusers/process.php','addForm=1');
									</script>
								</div>
							</div>
                      
                </div>
            </section>
			 <section id="datafill" class="container">

				<div id="adminTable">
					<script type="text/javascript">
					setStateGet('adminTable','<?php echo SECURE_PATH;?>dataentryusers/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
				</div>
			</section>
		</div>
		<a class="scroll-to-top rounded" href="#page-top">
                <i class="material-icons">navigation</i>
            </a>
		
	<?php
		
		}
	?>
	<script type="text/javascript">
	function addList(){
		cnt = $('#session_list').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list').val(new_cnt);
	
		html = '<div class="list" id="list'+new_cnt+'"></div>';	
   
		$('#dates_list').append(html);
   
		setStateGet('list'+new_cnt,'<?php echo SECURE_PATH;?>dataentryusers/process.php','add_listing='+new_cnt);
	}
	function removeList(id){

		$('#list'+id).remove();
	}

	function calconditions(){
		var retval = '';
		i=0;
        $('#dates_list .list').each(function(){
			retval+= $(this).find('.question').val()+'_'+$(this).find('.option1').val()+'_'+$(this).find('.option2').val()+'_'+$(this).find('.option3').val()+'_'+$(this).find('.option4').val()+'_'+$(this).find('.answer').val()+'_'+$(this).find('.explanation').val()+'_'+$(this).find('.comid').val()+'^';
		});
		//alert(retval);
		return retval;
		
	}
		function addList1(){
		cnt = $('#session_list1').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list1').val(new_cnt);
	
		html = '<div class="list1" id="list1'+new_cnt+'"></div>';	
   
		$('#dates_list1').append(html);
   
		setStateGet('list1'+new_cnt,'<?php echo SECURE_PATH;?>dataentryusers/process.php','add_listing1='+new_cnt);
	}
	function removeList1(id){

		$('#list1'+id).remove();
	}
	function calconditions1(){
		var retval = '';
		i=0;
        $('#dates_list1 .list1').each(function(){
			retval+= $(this).find('.qlist1').val()+'_'+$(this).find('.qlist2').val()+'^';
		});
		return retval;
		
	}
	function addList2(){
		cnt = $('#session_list2').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list2').val(new_cnt);
	
		html = '<div class="list2" id="list2'+new_cnt+'"></div>';	
   
		$('#dates_list2').append(html);
   
		setStateGet('list2'+new_cnt,'<?php echo SECURE_PATH;?>dataentryusers/process.php','add_listing2='+new_cnt);
	}
	function removeList2(id){

		$('#list2'+id).remove();
	}
	function calconditions2(){
		var retval = '';
		i=0;
        $('#dates_list2 .list2').each(function(){
			retval+= $(this).find('.qlist1').val()+'_'+$(this).find('.qlist2').val()+'^';
		});
		return retval;
		
	}
	function NumAndTwoDecimals(e) {
		//alert(e);
		if (e.which != 46 && e.which != 45 && e.which != 46 &&
			  !(e.which >= 48 && e.which <= 57)) {
			return false;
		  }
		}
	</script>
