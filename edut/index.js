const express = require("express");
const fs = require("fs");
const path = require("path");
const cors = require("cors");

const app = express();
const port = 3000;

 

var mjAPI = require("mathjax-node");

mjAPI.config({
    MathJax : {
        SVG : {
            font : "STIX-Web"
        }
    },
    displayErrors : true,
    displayMessages : false
});

mjAPI.start();

 

app.use(express.urlencoded());
app.use(express.json());

app.use(cors());
app.use("/", express.static("formulas"));


app.use("/mml",(req,res) =>{


   // console.log("Req",req.body,req.query);

  res.setHeader('Content-Type', 'application/json');
mjAPI.typeset({
        math : req.query.mml,
        format : "MathML",

        svg : true,
        width: 100,
        linebreaks: false
    }, function (results) {
        if (!results.errors) {

  // fs.writeFileSync('formulas/results.svg', results.svg);

            const svgstr = results.svg.replace('style=\"', 'style=\"font-family:Arial;');

          
let obj = { result: "ok", svg: svgstr };
                    res.send(JSON.stringify(obj));
 

        }
else{
let obj = { result: "failed", svg: "" };
                    res.send(JSON.stringify(obj));

}
 

    });

});


app.use((req, res) => {
  res.status(404).send("404 Not found");
});



app.listen(port, () => console.log(`Example app listening on port ${port}!`));
