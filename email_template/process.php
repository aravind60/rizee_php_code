<style type="text/css">
.btn-group-sm>.btn, .btn-sm {
    padding: .2rem .2rem;}
    i.fa.fa-key.black {
    padding: 0.3rem 0.1rem;
}
    </style>
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
	//$imagepath="http://localhost:81/neetjee/files/";
	$imagepath="https://admin.rizee.in/files/";
	//$imagepath="http://student.mylearningplus.in/neetjee_testing/files/";
?>
<script>
$(document).ready(function(){
  //alert("sdfgh"+userlevelval);
  $('#userlevel').change(function(){
    var userlevelval = $("#userlevel option:selected").val();
    
    if(userlevelval == 3 || userlevelval == 2 || userlevelval == 1){
    $('.department').show();
    $('.designation').show();
    
  }
  else{
    $('.department').hide();
    $('.designation').hide();
  }
  });
  
});
</script>

<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query1("SELECT * FROM email_template WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	 $var2='<div class="webimage" ><img src="'.$data['image'].'"  width="100%" style="width:100%;height:auto;" /><br /><br /></div>';
	 $var3=urlencode($var2);
	$var4=str_replace('+','%20',$var3);
		
	$_POST['image']=ltrim($data['image'],$imagepath);
	
	//$_POST['email_text']=str_replace($var4,"<body>",$data['email_text']);
	$_POST['email_text']=str_replace($var4,"<body>",$data['email_text']);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 if(isset($_POST['errordisplay'])){
	$_POST['email_text']=$_SESSION['email_text'];
 }else{
	 $_POST['email_text']=$_POST['email_text'];
 }
 ?>


 <div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
			  <label class="col-sm-2 col-form-label">Email Title<span style="color:red;">*</span></label>
				<div class="col-sm-10">
				  <textarea name="title" id="email_title" class="form-control"><?php if(isset($_POST['email_title'])){ echo $_POST['email_title'];}?></textarea>
				  <span class="text-danger"><?php if(isset($_SESSION['error']['email_title'])){ echo $_SESSION['error']['email_title'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-12">
	
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Email Subject<span style="color:red;">*</span></label>
				<div class="col-sm-10">
					
					 <textarea name="email_subject" id="email_subject" class="form-control"><?php if(isset($_POST['email_subject'])){ echo $_POST['email_subject'];}?></textarea>
				 <span class="text-danger"><?php if(isset($_SESSION['error']['email_subject'])){ echo $_SESSION['error']['email_subject'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-12" >
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Email Text<span style="color:red;">*</span></label>
				<div class="col-sm-10">

					<div class="wrs_container">
						<div class="wrs_row">
							<div class="wrs_col wrs_s12">
								<div id="editorContainer">
									<div id="toolbarLocation"></div>
									<textarea id="email_text" class="emaileditor wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, emaileditor" title="Rich Text Editor, emaileditor"><?php if(isset($_POST['email_text'])) { echo urldecode($_POST['email_text']); }else{  } ?></textarea>
									<span class="text-danger"><?php if(isset($_SESSION['error']['email_text'])){ echo $_SESSION['error']['email_text'];}?></span>
								</div>
							</div>

						</div>
					</div>


				</div>
			</div>
		</div>
		
		<div class="col-lg-12">
			<div class="form-group row">
				<label class="col-sm-2 col-form-label">Image</label>
				<div class="col-sm-10">

					<div id="file-uploader" style="display:inline">
						<noscript>
							<p>Please enable JavaScript to use file uploader.</p>
							<!-- or put a simple form for upload here -->
						</noscript>

					</div>
					<script>

						function createUploader(){

							var uploader = new qq.FileUploader({
								element: document.getElementById('file-uploader'),
								action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=image&upload_type=single&filetype=file',
								debug: true,
								multiple:false
							});
						}

						createUploader();



						// in your app create uploader as soon as the DOM is ready
						// don't wait for the window to load

					</script>
					<input type="hidden" name="image" id="image" value="<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>"/>
					<div class="pics"></div>
					<?php
					if(isset($_POST['image']))
					{
						if($_POST['image']!=''){
							?>
							<img src="<?php echo SECURE_PATH."files/".$_POST['image'];?>" style="width:15%;height:auto;" />
							<?php
						}

					}
					?>

					<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['image'])){ echo $_SESSION['error']['image'];}?></span>
				</div>
			</div>

		</div>
	</div>
	
	
   <script type="text/javascript" >
					createEditorInstance("en", {});
				</script>
					<script>
						function rand(){

							var conttype=$('#conttype').val();
							var data2;
							data2 = '{ "email_text" : "' + encodeURIComponent(tinymce.get('email_text').getContent())+ '"}';



							$.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>email_template/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',
								dataType: 'json',

								success: function (result,xhr) {
									console.log("reply");
									

									setState('adminForm','<?php echo SECURE_PATH;?>email_template/process.php','validateForm=1&email_title='+escape($('#email_title').val())+'&email_subject='+escape($('#email_subject').val())+'&image='+escape($('#image').val())+'<?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>');
								},
								error: function(e){

									console.log("ERROR: ", e);
								}
							});
						}


						function mobilePreview(){

							var conttype=$('#conttype').val();
							var data2;
							data2 = '{ "email_text" : "' + encodeURIComponent(tinymce.get('email_text').getContent())+ '"}';


							$.ajax({
								type: 'POST',
								url: '<?php echo SECURE_PATH;?>email_template/img.php',
								data: data2,
								contentType: 'application/json; charset=utf-8',
								dataType: 'json',

								success: function (result,xhr) {


									console.log('email_text',tinymce.get('email_text').getContent());

									var html = data2.email_text;
									var iframe = $('#displayframe');

									iframe[0].srcdoc =    (tinymce.get('email_text').getContent());

									console.log("iframe",iframe);

								},
								error: function(e){

									console.log("ERROR: ", e);
								}
							});

						}


					</script>




			<div class="form-group row pl-3">
		
				<div class="col-lg-12">
					<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="rand();">Save Email Template</a>
				</div>
			</div>
</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}


//Process and Validate POST data
if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  $field = 'email_title';
	if(!$post['email_title'] || strlen(trim($post['email_title'])) == 0){
	  $_SESSION['error'][$field] = "* Email Title name cannot be empty";
	}
	 $field = 'email_subject';
	if(!$post['email_subject'] || strlen(trim($post['email_subject'])) == 0){
	  $_SESSION['error'][$field] = "* Email Subject cannot be empty";
	}
	 $field = 'email_text';
	if(!$_SESSION['email_text'] || strlen(trim($_SESSION['email_text'])) == 0){
	  $_SESSION['error'][$field] = "* Email Text cannot be empty";
	}
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
		//print_r($_SESSION);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>email_template/process.php','addForm=1&errordisplay=1&email_title=<?php echo $post['email_title'];?>&email_subject=<?php echo $post['email_subject'];?>&image=<?php echo $post['image'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		//$email_text=str_replace("'","&#39;",$_SESSION['email_text']);
		$ldesc=str_replace("'","&#39;",$_SESSION['email_text']);
		
		if($post['image']!=''){
			$image = SECURE_PATH."files/".$post['image'];
			 $image = $imagepath.$post['image'];
			$var2='<div class="webimage" ><img src="'.$image.'"  width="600" style="width:100%;height:auto;" /><br /><br /></div>';
			 $var3=urlencode($var2);
			$var4=str_replace('+','%20',$var3);
		}else{
			$image='';
			$var2='';
			$var4='';
		}
		$email_text=filterqa($ldesc);
		
		$header='<!DOCTYPE html><html lang="en"><head><style>body{width:100%;background-color:#ccc;margin:0;padding:0;-webkit-font-smoothing:antialiased;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px}@media (max-width:991.98px) { .webimage { display: block;} } @media (min-width:992px) { .webimage { display: block;} }.wrapper{ max-width: 800px;width: 100%;margin: 0 auto;background-color:#fff;}.wrapper p {padding:0 15px;}</style></head>
		<body style="font-family: Times New Roman, Georgia, Serif;font-size: 14px;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
		<div class="wrapper">';
		 $footer='</div></body></html>';
		 $imagediv='<div class="webimage" ><img src="'.$image.'"  width="100%" height="auto" /><br /><br /></div><div class="mobileimage"  ><img src="'.$image.'"  width="100%" height="auto" /><br /><br /></div>';
		 $long_descri1=$header.$email_text.$footer;
		 $long_description=str_replace('white-space: pre-wrap;','',$long_descri1);
if($post['email_title']!='' && $post['email_title']!='undefined'){
	if($id=='NULL')
	{
		
		
		
      $database->query1("INSERT email_template SET email_title='".$post['email_title']."',email_subject='".$post['email_subject']."',email_text='".$long_description."',image='".$image."',estatus='1',timestamp='".time()."'");
	  
	 ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Email Template Saved Successfully!
			</div>
		</div>
	</div>
	<script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>email_template/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
	 <?php
    }
		else
		{
		 $database->query1("update email_template SET email_title='".$post['email_title']."',email_subject='".$post['email_subject']."',email_text='".$long_description."',image='".$image."' where id='".$id."'");
	  ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Email Template Updated successfully!
			</div>
		</div>
	</div>
	<script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>email_template/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
	  <?php
    }
}else{
  ?>
 
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> Email Template Saving Failed!
			</div>
		</div>
	</div>
	<script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>email_template/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
	  <?php
  
 
	
}
  }
}
//Delete users
if(isset($_GET['rowDelete'])){
	mysqli_query($database->connection1,"update email_template set estatus='0' where id='".$_GET['rowDelete']."'");
?>
<div class="alert alert-success">Template deleted successfully!</div>
<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>email_template/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
<?php
}
if(isset($_REQUEST['viewdetails'])){
	?>
	<form>
		<div class="form-group">
		<label for="recipient-name" class="col-form-label">Username</label>
		<input type="username1" id="username1" class="form-control" value="<?php echo $_REQUEST['viewdetails']; ?>" readonly >
	  </div>
	  <div class="form-group">
		<label for="recipient-name" class="col-form-label">Password</label>
		<input type="password" id="password" class="form-control" >
	  </div>
	  
	 <div class="center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary" data-dismiss="modal" onClick="setState('adminForm','<?php echo SECURE_PATH;?>users/process.php','validateForm1=1&password='+$('#password').val()+'&resetid=<?php echo $_REQUEST['viewdetails'];?>')">Update</a>
		</div>
    </form>	
<?php
}

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'email_template';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  // $pagination = $session->showPagination(SECURE_PATH."users/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query1($q);
  $numres = mysqli_num_rows($result_sel);
 
    $query = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  
  
  //echo $query;
  $data_sel = $database->query1($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Email Template Details</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered"  width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Email Title</th>
											  <th class="text-left" nowrap>Email Subject</th>
											  <th class="text-left" nowrap>Email Text</th>
											  <th class="text-left" nowrap>Image</th>
											  <th class="text-left" nowrap>Actions</th>
											</tr>
									</thead>
									<tbody>
									<?php
									
									$i=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										 
									?>
									<tr>
										<td class="text-left"><?php echo $i;?></td>
										<td class="text-left" width="10%"><?php echo $value['email_title'];?></td>
										<td class="text-left" width="20%"><?php echo $value['email_subject'];?></td>
										<td class="text-left" width="100%"><?php echo urldecode($value['email_text']);?></td>
										<td class="text-left"><img src=<?php echo $value['image']; ?> alt="Image" width="60px" height="60px"></td>
										<td nowrap>
											
										  <a href="#" class="btn btn-sm btn-primary" onClick="setState('adminForm','<?php echo SECURE_PATH;?>email_template/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><i
												  class="material-icons md-16 pt-1">edit</i></a>
										  <a href="#" class="btn btn-sm btn-danger" onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>email_template/process.php','rowDelete=<?php echo $value['id'];?>')"><i
												  class="material-icons md-16 pt-1">delete</i></a>
										
										  <?php
											
										  ?>
										</td>
									  </tr>
									<?php
									$i++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>

  </section>
 
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}
	?>
<script type="text/javascript">
  $('#dataTable').DataTable({
    "pageLength": 50,
    "order": [[ 0, 'asc' ]]
    
  });
   
</script>
<?php }
?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
		
			<label class="col-lg-4 col-form-label">Chapter<span style="color:red;">*</span></label>
			<div class="col-lg-6 ml-5">
				<select name="chapter" class="selectpicker1" id="chapter" multiple data-actions-box="true" data-live-search="true"  onchange="chapterfunction();setState('chapview','<?php echo SECURE_PATH;?>users/process.php','getchaptercview=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
					<!-- <option value="All"   >All</option> -->
					<?php
					//echo "SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').") ";
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					  ?>
					<option value="<?php echo $data1['id'];?>"  <?php if(isset($_REQUEST['chapter'])) {if(in_array($data1['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?> ><?php echo $data1['chapter'];?></option>
					<?php
					 $i++;
					}
					?>
				</select>
				
				<span class="text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
			</div>
		   
		
    <?php
}?>
<?php
if(isset($_REQUEST['getchaptercview']))
{	
	$chapter=str_replace("All,","",$_REQUEST['chapter']);
	if($chapter!=''){
		$con=" AND id IN(".rtrim($chapter,',').") ";
	}else{
		$con=" AND id IN(".rtrim($post['chapter'],',').") ";
	}
	
	?>
		
					<?php
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' ".$con.""); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					 
						  $count=$count+$rowdcount;
					 $i++;
					}
					
					?>
					<a href='#'  style="cursor:pointer;color: #007bff;" title="Print"  data-toggle="modal" data-target="#messageDetails1" onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>users/process1.php','viewDetails1=1&chapter=<?php echo $_REQUEST['chapter']; ?>');"><?php echo $count; ?> Users</a>
		
    <?php
} ?>
		<?php
	if(isset($_REQUEST['getexamset']))
	{	
		$_REQUEST['qset']=explode(",",$_REQUEST['qset']);
	?>
		<label for="inputTopi" class="label">Set ID</label>
		<select class="form-control selectpicker4" multiple data-live-search="true" name="qset" value=""  id="qset"  >
			<?php
			
			$sel=$database->query("select * from previous_questions where estatus='1' and year in (".$_POST['year'].")  ORDER by id DESC");
			while($row=mysqli_fetch_array($sel)){
				$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
				while($row1=mysqli_fetch_array($sel1)){
				?>
					
					<option value="<?php echo $row1['id'];?>"  <?php if(isset($_REQUEST['qset'])) {if(in_array($row1['id'], $_REQUEST['qset'])) { echo 'selected="selected"'; } } ?> ><?php echo $row1['qset'];?></option>
			<?php
				}
			}
			?>
	</select>
    <?php
}


			function filterqa($str){
  $str = urldecode($str);
//$res = str_replace("&nbsp;","",$str);

  return $res= getAllMath($str);

}


function getAllMath($text){



  $result = "";
  $part1 = explode("<math",$text);

  if(count($part1) > 0){

      $result.= $part1[0];//strip_tags($part1[0],'<p><img><b>');

      foreach($part1 as $first){
          $part2 = explode("</math>",$first);

          if(count($part2) > 1){
              $mml = "<math ".$part2[0]."</math>";
             
              $fields = array(
                  'mml' => $mml,
                  'lang'=>urlencode('en')
          );
          
          
          $url="http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimagenew.php";
          
         $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $retval = curl_exec($ch);
        curl_close($ch);
  
        $svg = json_decode($retval,true);


        $alt = "MathML Question";
        if(isset($svg['result']['alt']))
            $alt = $svg['result']['alt'];

		  $imgtext=str_replace(" ","+",$svg['result']['content']);
		 
       // $img= '<img src="'.($svg['result']['content']).'"  class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; " width="'.$svg['result']['width'].'" height="'.$svg['result']['height'].'"></img>';
       $img= "<img src='".($imgtext)."'  class='Wirisformula' role='math' alt='.$alt.' style='vertical-align: -5px; ' width='".$svg['result']['width']."' height='".$svg['result']['height']."'></img>";
        $result.= $img.$part2[1];//strip_tags($part2[1],'<p><img>');
          
         
          }
      }
  }

  return $result;
}
			?>

<script>
function selectfunction(){
	
	var userlevel=$('#userlevel').val();
	var ppaper=$('#ppaper').val();
	if(userlevel=='6' || userlevel=='7' || userlevel=='8' || userlevel=='2' || userlevel=='3'){
		$('.subjectdiv').show();
		if(userlevel=='7'){
			$('.contentd').show();
			$('.contentd1').show();
			$('.previous1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').show();
			$('.update_chapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
			$('.previous2').hide();
		}else if(userlevel=='8'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
		
		}else if(userlevel=='3'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').show();
			$('.questionanalysis1').hide();
		
		}else if(userlevel=='2'){
			$('.contentd').show();
			$('.contentd1').hide();
			$('.previous1').show();
			$('.update_chapter1').show();
			$('.question_wchapter1').show();
				if(ppaper=='1'){
					$('.previous2').show();
				}else{
					$('.previous2').hide();
				}
			$('.nochapter1').hide();
			$('.lecturerdashboard1').show();
			$('.questionsearch1').show();
			$('.questionanalysis1').show();
		
		}else{
			$('.contentd').hide();
			$('.previous1').hide();
			$('.previous2').hide();
			$('.contentd1').hide();
			$('.nochapter1').hide();
			$('.update_chapter1').hide();
			$('.lecturerdashboard1').hide();
			$('.questionsearch1').hide();
			$('.questionanalysis1').hide();
		}

	}else{
		$('.subjectdiv').hide();
        $('.contentd').hide();
		$('.previous1').hide();
		$('.previous2').hide();
		$('.nochapter1').hide();
		$('.update_chapter1').hide();
		$('.lecturerdashboard1').hide();
		$('.questionsearch1').hide();
		$('.questionanalysis1').hide();
		

	}
}
</script>

	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
<script type="text/javascript">
	function getFields5()
    {
    	
    var ass='';
    $('.classn').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
      ass+=$(this).val()+",";
	 
    }else{
      $('#'+$(this).attr('id')).val(0);
      ass+=$(this).val()+",";
	 
    }
//alert(ass);

  });
        
        

    
}
	function getFields6()
    {
    	var userlevel=$('#userlevel').val();
    $('.ppaper').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 if(userlevel=='8' || userlevel=='7' || userlevel=='3' || userlevel=='2'){
		$('.previous2').show();
	 }else{
		$('.previous2').hide();
	 }

    }else{
      $('#'+$(this).attr('id')).val(0);
     if(userlevel=='8' || userlevel=='7' || userlevel=='3' || userlevel=='2'){
		$('.previous2').hide();
	  }else{
		$('.previous2').hide();
	 }
    }
//alert(ass);

  });
        
        

    
}
	function getFields7()
    {
    	var userlevel=$('#userlevel').val();
    $('.attributes').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 

    }else{
      $('#'+$(this).attr('id')).val(0);
    
    }
//alert(ass);

  });
        
        

    
}

	function getFields8()
    {
		var userlevel=$('#userlevel').val();
		$('.update_chapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields9()
    {
		$('.lecturerdashboard').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields10()
    {
		$('.questionsearch').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields11()
    {
		$('.questionanalysis').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields12()
    {
		$('.nochapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields13()
    {
		$('.ppaper_analysis').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields14()
    {
		$('.question_forum').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields15()
    {
		$('.institute_epaper').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	
	function getFields16()
    {
		$('.attributeoverview').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields17()
    {
		$('.fitment_report').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	function getFields18()
    {
		$('.exam_paper_generation').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
    </script>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>

