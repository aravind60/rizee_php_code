<?php

ini_set('display_errors','0');

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","neetjee");

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Student Name</th>
							<th style="text-align:center;">Section</th>
							<th style="text-align:center;">Branch</th>
							<th style="text-align:center;">Total Marks</th>
							<?php
							$sql=query("SELECT question_id FROM `student_questions` WHERE `schedule_exam_id` = 473 group by question_id ORDER BY `timestamp` DESC");
							while($row=mysqli_fetch_array($sql)){ ?>
								<th style="text-align:center;"><?php echo $row['question_id']; ?></th>
							
							<?php
							}
							
							?>
						</tr>
						<?php
							
							$sql=query("SELECT mobile FROM `student_questions` WHERE `schedule_exam_id` = 473 group by mobile ");
							while($row=mysqli_fetch_array($sql)){ 
								$sqll=query("SELECT * FROM `students` WHERE contact_no='".$row['mobile']."'");
								$rowl=mysqli_fetch_array($sqll); 

								$sqlls=query("SELECT * FROM sections WHERE id='".$rowl['section_id']."'");
								$rowls=mysqli_fetch_array($sqlls); 
								$sqllsb=query("SELECT * FROM branches WHERE id='".$rowl['branch_id']."'");
								$rowlsb=mysqli_fetch_array($sqllsb); 
								$total=0;
								$sqltot=query("SELECT id,status FROM `student_questions` WHERE `schedule_exam_id` = 473 and mobile='".$row['mobile']."' and status='2'  ORDER BY `timestamp` DESC");
								while($rowtot=mysqli_fetch_array($sqltot)){
									$total++;
								}
								
								$twrong=0;
								$sqltotw=query("SELECT id,status FROM `student_questions` WHERE `schedule_exam_id` = 473 and mobile='".$row['mobile']."' and status='1'  ORDER BY `timestamp` DESC");
								while($rowtotw=mysqli_fetch_array($sqltotw)){
									$twrong++;
								}
								$skipq=0;
								$sqltotskip=query("SELECT id,status FROM `student_questions` WHERE `schedule_exam_id` = 473 and mobile='".$row['mobile']."' and status='0'  ORDER BY `timestamp` DESC");
								while($rowtotskip=mysqli_fetch_array($sqltotskip)){
									$skipq++;
								}
								$totaltot=($total*4)-$twrong;
								//$total=$rowtot*4+($rowtotw*1-)
								//echo "correct-".$total."Worng".$twrong;
								?>
								<tr style="text-align:center;">
									<td><?php echo $rowl['student_name']; ?></td>
									<td><?php echo $rowls['section_name']; ?></td>
									<td><?php echo $rowlsb['branch_name']; ?></td>
									<td><?php echo $totaltot; ?></td>
									<?php
										$sql2=query("SELECT status FROM `student_questions` WHERE `schedule_exam_id` = 473 and mobile='".$row['mobile']."' ORDER BY `timestamp` DESC");
										while($row=mysqli_fetch_array($sql2)){ 
											if($row['status']=='2'){
												$status="Correct";
											}else if($row['status']=='1'){
												$status="Wrong";
											}else if($row['status']=='0'){
												$status="Skipped";
											}
											?>
											<td><?php echo $status; ?></td>
										
										<?php
										}
										
									?>
								</tr>
							
							<?php
								}
							?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>