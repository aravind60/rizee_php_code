<?php
    error_reporting(0);
    include('../include/session.php');
	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
		
		unset($_SESSION['thumbnail']);
    ?>
    
   
		<style>
         /* .wrs_editor .wrs_formulaDisplay {
             height: 250px !important;
         } */
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}
		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		/* .nav-pills .nav-link.active, .nav-pills .show>.nav-link {
			color: #fff;
			background-color: #dee2e6 !important;
			} */
		
		</style>
		
	
    </script>
    
    <script type="text/javascript">
		if (window.location.search !== '') {
		    var urlParams = window.location.search;
		    if (urlParams[0] == '?') {
		        urlParams = urlParams.substr(1, urlParams.length);
		        urlParams = urlParams.split('&');
		        for (i = 0; i < urlParams.length; i = i + 1) {
		            var paramVariableName = urlParams[i].split('=')[0];
		            if (paramVariableName === 'language') {
		                _wrs_int_langCode = urlParams[i].split('=')[1];
		                break;
		            }
		        }
		    }
		}
    </script>
	 
	

        <script>
			if(typeof urlParams !== 'undefined') {
				var selectLang = document.getElementById('lang_select');
				selectLang.value = urlParams[1];
			}
			
			function getContent(){
			
			
			
			
			    var inst, contents = new Object();
				
for (inst in tinyMCE.editors) {
    if (tinyMCE.editors[inst].getContent)
        contents[inst] = tinyMCE.editors[inst].getContent();
		}
			
			
				   console.log('editor 1', contents[0]);
			   console.log('editor 2', contents[1]);
			   console.log('editor 3', contents[2]);
			
		
			
			
			}
			
			
			
		</script>
		<script>
		$('.btn-group').on('click', '.btn', function() {
		  $(this).addClass('active').siblings().removeClass('active');
		});
		</script>
		<script>
		$('#chapter').selectpicker();
		$('.exam').exampicker();
		</script>
			
			<div class="content-wrapper" id="data1">
				<!-- Breadcrumbs-->
				
				<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-md" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewdetails">
							...
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							
						</div>
						</div>
					</div>
				</div>

				
				<div class="modal fade " id="messageDetails" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" id="viewDetails1">
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
								<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
							</div>
						</div>
					</div>
				</div>
			
                <!-- End Breadcrumbs-->
				<!-- Content Area-->
				<section class="content-area">
					<div class="container" >
						<section class="breadcrumbs-area2 my-3">
					<div class="container" id="errordisplaydiv">
						<div class="d-flex justify-content-between align-items-center">
							<div class="title">
								<h1 class="text-uppercase">Create Exampaper <small>Lets you a create Exampaper</small></h1>
							</div>
						</div>
						<hr>
					</div>
				</section>
				
				<section class="content-area" id="examdataid">
					<div class="container">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
								<div class="card border-0 shadow mb-4">
									<div
										class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
										<h6 class="m-0 font-weight-bold text-primary">Create Exam Paper</h6>
									</div>
									<div class="card-body">
									  
											<div class="row">
												<div class="col-lg-12 col-md-12">
													<div class="form-group row">
														<div class="col-sm-8">
															<a href="#" class="btn btn-md btn-theme-2"onClick="$('#adminForm').slideToggle()">Create Exam Paper</a>
														</div>
													</div>
													<div class="form-group row" id="adminForm" style="display:none;">
														<div class="col-sm-8">
															<script type="text/javascript">
																setStateGet('adminForm','<?php echo SECURE_PATH;?>exampaper_generate/process.php','addForm=1');
															</script>
														</div>
													</div>
												</div>
											</div>
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				 <section>
					<div id="adminTable">
						<script type="text/javascript">
							setStateGet('adminTable','<?php echo SECURE_PATH;?>exampaper_generate/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					</div>
				</section>
			</div>
				
			 
		</div>

		
 
	<?php
		
		}
	?>
	<script type="text/javascript">
	function addList(){
		cnt = $('#session_list').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list').val(new_cnt);
	
		html = '<div class="list" id="list'+new_cnt+'"></div>';	
   
		$('#dates_list').append(html);
   
		setStateGet('list'+new_cnt,'<?php echo SECURE_PATH;?>createquestion/process.php','add_listing='+new_cnt);
	}
	function removeList(id){

		$('#list'+id).remove();
	}
	function calconditions(){
		var newarray = [];
		i=1;
        $('#dates_list .list').each(function(){
			var retval = {};
			//retval+= $(this).find('.question').val()+'_'+$(this).find('.option1').val()+'_'+$(this).find('.option2').val()+'_'+$(this).find('.option3').val()+'_'+$(this).find('.option4').val()+'_'+$(this).find('.answer').val()+'_'+$(this).find('.explanation').val()+'^';
			retval.question= encodeURIComponent(tinymce.get('question'+i).getContent());
			retval.option1= encodeURIComponent(tinymce.get('option1'+i).getContent());
			retval.option2= encodeURIComponent(tinymce.get('option2'+i).getContent());
			retval.option3= encodeURIComponent(tinymce.get('option3'+i).getContent());
			retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
			retval.explanation= encodeURIComponent(tinymce.get('explanation'+i).getContent());
			retval.answer= $(this).find('.answer').val();
			newarray.push(retval);
			console.log('new',newarray);
			i++;
		});
		console.log('new',newarray);

		 $.ajax({
			type: 'POST',
			url: '<?php echo SECURE_PATH;?>createquestion/img2.php',
			data:JSON.stringify(newarray),
			contentType: 'application/json; charset=utf-8',
					
			dataType: 'json',
			success: function (data) {
					console.log("data4");
					
			},
				error: function(e){	

			console.log("ERROR: ", e);
			}
		});
		
	}

	function addList1(){
		cnt = $('#session_list1').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list1').val(new_cnt);
	
		html = '<div class="list1" id="list1'+new_cnt+'"></div>';	
   
		$('#dates_list1').append(html);
   
		setStateGet('list1'+new_cnt,'<?php echo SECURE_PATH;?>createquestion/process.php','add_listing1='+new_cnt);
	}
	function removeList1(id){

		$('#list1'+id).remove();
	}
	function calconditions1(){
		
		var newarray = [];
		i=1;
        $('#dates_list1 .list1').each(function(){
			var retval = {};
			//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
			retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
			retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());
			newarray.push(retval);
			console.log("kkk"+retval);
			i++;

		});

		console.log('new',newarray);

		 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
					data:JSON.stringify(newarray),
					contentType: 'application/json; charset=utf-8',
							
					dataType: 'json',
					success: function (data) {
							console.log("data4");
							
					},
						error: function(e){	

					console.log("ERROR: ", e);
					}
				});
		//return newarray;
		
	}
	function addList2(){
		cnt = $('#session_list2').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list2').val(new_cnt);
	
		html = '<div class="list2" id="list2'+new_cnt+'"></div>';	
   
		$('#dates_list2').append(html);
   
		setStateGet('list2'+new_cnt,'<?php echo SECURE_PATH;?>createquestion/process.php','add_listing2='+new_cnt);
	}
	function removeList2(id){

		$('#list2'+id).remove();
	}
	function calconditions2(){
		var newarray = [];
		i=1;
        $('#dates_list2 .list2').each(function(){
			//retval+= $(this).find('.qlist1').val()+'_'+$(this).find('.qlist2').val()+'^';
			var retval = {};
			
			retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
			retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());
			newarray.push(retval);
			console.log("kkk"+retval);
			i++;
			

		});
		//return retval;
		console.log('new',newarray);

		 $.ajax({
			type: 'POST',
			url: '<?php echo SECURE_PATH;?>createquestion/img1.php',
			data:JSON.stringify(newarray),
			contentType: 'application/json; charset=utf-8',
					
			dataType: 'json',
			success: function (data) {
					console.log("data4");
					
			},
				error: function(e){	

			console.log("ERROR: ", e);
			}
		});
	}

	
	
	</script>
	<script type="text/javascript">


function getFields2()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.exam').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#exam').val(ass);
	});
        
        

    
}
</script>
<script>
 $(".selectpicker1").selectpicker('refresh');
  $(".exampicker").selectpicker('refresh');
</script>
<script>
	function NumAndTwoDecimals(e) {
	//alert(e);
	if (e.which != 46 && e.which != 45 && e.which != 46 &&
		  !(e.which >= 48 && e.which <= 57)) {
		return false;
	  }
	}
</script>
 