<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

?>


		<script>
		
		
		function search_report() {
		
			var class1 = $('#class').val();
			var subject = $('#subject').val();
			var chapter = $('#chapter').val();
			
			setStateGet('adminTable','<?php echo SECURE_PATH;?>excelreports/process.php','tableDisplay=1&class='+class1+'&subject='+subject+'&chapter='+chapter);
		}
	</script>
		</script>
	<?php
                                 
if(isset($_GET['tableDisplay'])){
	$_REQUEST['class1']=$_REQUEST['class']; 
	$_REQUEST['subject1']=$_REQUEST['subject']; 
	$_REQUEST['chapter1']=$_REQUEST['chapter']; 
	?>
		<div class="card shadow-sm mb-4">
		<div class="card-header py-3">
			<h6 class="m-0 font-weight-bold text-primary">Download Excels</h6>
		</div>
        <div class="card-body">
			<div class="row">
				<div class="col-lg-12" >
					<a href='<?php echo SECURE_PATH;?>chapter_count4.php' target='_blank' class="btn btn-primary text-white" >chapters, topics with questions </a>
				</div>
			</div>

			 <div class="row pt-5">
				<div class="col-lg-12" >
					<a  href='<?php echo SECURE_PATH;?>questionsoverview.php' target='_blank' class="btn btn-primary text-white " >Question attribute overview </a>
				</div>
			</div> 

			<div class="row pt-5">
				<div class="col-lg-12" >
					<a href='<?php echo SECURE_PATH;?>chapter_count6.php' target='_blank' class="btn btn-primary text-white " >Chapters with out topic Questions</a>
				</div>
			</div>

			<!-- <div class="row pt-5">
				<div class="col-lg-12" >
					<a href='<?php echo SECURE_PATH;?>chapter_count5.php' target='_blank' class="btn btn-primary text-white " >Custom Content different types with topic wise with chapter names </a>
				</div>
			</div> -->
	<?php
$limit=50;
if(isset($_GET['page']))
{
	if($_GET['page']!='0'){
		$start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
		$page=$_GET['page'];
	}else{
		$start = 0;      //if no page var is given, set start to 0
		$page=0;
	}
}
else
{
	$start = 0;      //if no page var is given, set start to 0
	$page=0;
}


//Search Form

$tableName = 'chapter';
$condition=" estatus='1'";
if(isset($_GET['keyword'])){
}

if(isset($_REQUEST['class1'])){
		if($_REQUEST['class1']!=""){
			$class=" AND class  IN (".$_REQUEST['class1'].")";
		} else {
			$class="";
		}
	}else{
		$class="";
	}
	if(isset($_REQUEST['subject1'])){
		if($_REQUEST['subject1']!=""){
			$subject=" AND subject  IN (".$_REQUEST['subject1'].")";
		} else {
			$subject="";
		}
	}else{
		$subject="";
	}
	if(isset($_REQUEST['chapter1'])){
		if($_REQUEST['chapter1']!=""){
			$chapter=" AND id  IN (".$_REQUEST['chapter1'].")";
		} else {
			$chapter="";
		}
	}else{
		$chapter="";
	}
	$con=$class.$subject.$chapter;
if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition.$con;
  }
$pagination = $session->showPagination(SECURE_PATH."chapterreport/process.php?tableDisplay=1&pagerows=".$_REQUEST['pagerows']."&start=".$start."&limit=".$limit."&",$tableName,$start,$limit,$page,$condition);
$q = "SELECT * FROM $tableName ".$condition."";
$result_sel = $database->query($q);
$numres = mysqli_num_rows($result_sel);
$query = "SELECT * FROM $tableName ".$condition." order by subject ASC ";

$data_sel = $database->query($query);
 if(($start+$limit) > $numres){
     $onpage = $numres;
     }
     else{
      $onpage = $start+$limit;
     }
 if($numres > 0){ 

?>
	<script type="text/javascript">
  $('#dataTable').DataTable({
    "pageLength": 50
    
  });
   
</script>
	
				
                    </div> 

          
<?php 
 }
}


?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
 
	
		<label for="inputTopic">Chapter</label>
		<!-- <div class="col-sm-8"> -->
   			<select name="chapter" id="chapter"  style="width:45%"  multiple data-live-search="true"  class="form-control selectpicker4" >
    			

            		<?php
					if($_REQUEST['class']!='' && $_REQUEST['subject']!=''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($_REQUEST['class'],",").") and subject IN (".rtrim($_REQUEST['subject'],",").") "); 
					}else if($_REQUEST['class']!='' && $_REQUEST['subject']==''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($_REQUEST['class'],",").") "); 
					}else if($_REQUEST['class']=='' && $_REQUEST['subject']!=''){
						$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and  subject IN (".rtrim($_REQUEST['subject'],",").")"); 
					}else{
						$zSql = $database->query("SELECT * FROM `chapter` WHERE  estatus='1'");
					}

					
					if(mysqli_num_rows($zSql)>0)
					{ 
						while($zData = mysqli_fetch_array($zSql))
						{ 
							?> 
							<option value="<?php echo $zData['id'];?>"  <?php if(isset($_REQUEST['chapter'])) { if(in_array($zData['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>><?php echo $zData['chapter'];?></option>
							
							<?php
						}
					}
					?>
   				</select>
			<!-- </div>
					 -->
    <?php
}
?>
<script>
	$(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
 $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
 </script>




