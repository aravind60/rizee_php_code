<?php

define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
			
			<tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Lecturer User Id</th>
							<th style="text-align:center;">Verified date</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Question Id</th>
							<th style="text-align:center;">Type of question while creating question</th>
							<th style="text-align:center;">Type of question in attribution</th>
							
							
						</tr>
                        <?php
                            $k=1;
                           
                               $sel=query("select id,class,subject,chapter,vusername,vtimestamp,qtype,inputquestion from createquestion where estatus='1' and vstatus='1' and qtype!=inputquestion and inputquestion!='' and qtype!=7");
								while($row=mysqli_fetch_array($sel)){ 
									
								$chap='';
                                $chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1 AND id IN (".$row['chapter'].") ORDER BY subject ASC");
								
                                while($chapter = mysqli_fetch_array($chapter_sel)){
									$chap.=$chapter['chapter'].",";
								}
                                   		
									$sub= query("SELECT * FROM subject WHERE estatus=1 AND id ='".$row['subject']."'");
									$rowsub = mysqli_fetch_array($sub);

									$qtypesel= query("SELECT * FROM questiontype WHERE estatus=1 AND id ='".$row['qtype']."'");
									$qtyperow = mysqli_fetch_array($qtypesel);
									$qtypesel1= query("SELECT * FROM questiontype WHERE estatus=1 AND id ='".$row['inputquestion']."'");
									$qtyperow1 = mysqli_fetch_array($qtypesel1);

									
											$date=date("d/m/Y",$row['vtimestamp']);
								   
											echo "<tr>";
												?>	
												
												<td><?php echo $k;?></td>
												<td><?php echo $row['vusername']; ?></td>
												<td><?php echo $date;?></td>
												<td><?php echo $rowsub['subject']; ?></td>
												<td><?php echo rtrim($chap,",");?></td>
												<td><?php echo $row['id']; ?></td>
												<td><?php echo $qtyperow['questiontype']; ?></td>
												<td><?php echo $qtyperow1['questiontype']; ?></td>
											<?php
											
											echo "</tr>";
										  $k++;
										}
									                       
									
                           

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>