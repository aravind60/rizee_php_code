<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<title>Title</title>


<link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"  />
<link type="text/css" rel="stylesheet" href="css/font-awesome.min.css"  />
<link type="text/css" rel="stylesheet" href="css/custom.css"  />

</head>

<body>


  
<!-- Modal -->
          <div aria-hidden="true" aria-labelledby="annoModal" role="dialog" tabindex="-1" id="annoModal" class="modal fade">
              <div class="modal-dialog">
                  <div class="modal-content">
                      <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title">New Ticket</h4>
                      </div>
                      <div class="modal-body" >
                           
                           
						   <div style="width:100%; height:180px;text-align:center;">
                           <img src=​"" id="cropresult" style="width:auto;" />​
						   
						   </div>

                           <div id="ticketform">
						       
						   </div>
                            
                           
                      </div>
                      <div class="modal-footer">
                          <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
                          
                      </div>
                  </div>
              </div>
          </div>
          <!-- modal -->
          

 

<div class="container-fluid appholder">
<input type="hidden" id="newspage" value="ap0509" />

<input type="hidden" id="currentimg" value="0" />
<input type="hidden" id="maximg" value="13" />


 
 <button id="zoomIn" class="btn btn-small btn-warning"><i class="fa fa-plus"></i></button>
<button id="zoomOut" class="btn btn-small btn-warning"><i class="fa fa-minus"></i></button>&nbsp;
<button id="cropit" class="btn btn-small btn-success"><i class="fa fa-crop"></i> Clip</button>
<button id="completecrop" class="btn btn-small btn-danger" style="display:none;"><i class="fa fa-check"></i> Save Clip</button>
<button id="cancelcrop" class="btn btn-small" style="display:none;"><i class="fa fa-close"></i> Cancel</button>


<button id="prev" class="btn btn-small btn-info" ><i class="fa fa-chevron-circle-left"></i> Prev</button>
<button id="next" class="btn btn-small btn-info">Next <i class="fa fa-chevron-circle-right"></i></button>

<div id="canvasContainer">
    <canvas id="c" width="720" height="720"></canvas>
</div>
 
 
</div>

<script type="text/javascript" src="jquery.js"></script>
<script type="text/javascript" src="fabric.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="customf.js"></script>

</body>
</html>
