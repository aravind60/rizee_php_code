<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Swamy">
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <title>succesful-page</title>
    <style>
        body {
            font-family: 'Poppins', sans-serif;
            font-size: 12px;
            background: white;
            display: flex;
            align-items: flex-start;
            justify-content: center;
        }

        .container {
            color: white;
            position: relative;
        }

        .content-mask {
            max-width: 400px;
            margin: 50% auto;
            text-align: center;
            vertical-align: middle;
        }

        .title {
	    color:#F05D70;
            margin-bottom: 0;
            text-transform: uppercase;
        }
        .descrption {
color:#707070;
            margin: 0;
            padding: 0;
            text-transform: capitalize;
        }

        .succesful-logo img {
            display: block;
            margin: 0 auto;
            width: 50%;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="content-mask">
            <div class="succesful-logo">
                <img src="http://admin.mylearningplus.in/app_images/failed.png" alt="logo">

            </div>
            <h1 class="title">Payment Failed</h1>
            <h5 class="title" style="margin-bottom: 10px">Order ID : <?php echo $_REQUEST['transaction_id'];?></h5>
            <p class="descrption">Your payment transaction for Rizee is failed. Please Try again later</p>
        </div>
    </div>

</body>

</html>