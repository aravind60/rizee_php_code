
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php

	if(isset($_REQUEST['addForm'])){
		
		if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
		?>
	 <script>
		$('html, body').animate({
		   scrollTop: $("#myDiv").offset().top
	   }, 2000);
	  </script>
	 <?php
    $data_sel = $database->query("SELECT * FROM faqs WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}

			  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
		</style>
		<style>
		
		</style>
	
	<script>
		('#class_ids').selectpicker3();
		$('#exam_ids').selectpicker1();
		$('#username').selectpicker2();
		//$('#chapter').selectpicker();
		/*$('#username').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 300,
			 buttonWidth: "100%",
			dropDown: true
		});*/
	 </script>
		
								

							
									
	<div class="col-lg-12 col-md-12">
		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Category</label>
					<div class="col-sm-8">
						<select class="form-control "  name="category" value=""  id="category" >
							<option value=''>-- Select --</option>
							<?php
							$row = $database->query("select * from faq_categories where estatus='1'");
							while($data = mysqli_fetch_array($row))
							{
								?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['category'])) { if($_POST['category']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['category'];?></option>
							<?php
							}
							?>
						</select>
						
						<span class="error text-danger"><?php if(isset($_SESSION['error']['category'])){ echo $_SESSION['error']['category'];}?></span>
					</div>
				</div>
			</div>
		
			
			
		</div>
		
		
		
		<div class="row">	
			<div class="col-lg-6" >
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Title</label>
					<div class="col-sm-8">
					<textarea id="title" id="title" class="form-control" ><?php if(isset($_POST['title'])){ if($_POST['title']!=''){ echo $_POST['title']; }} ?></textarea>
						<span class="error text-danger"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
					</div>
				</div>
			</div>

			

		</div>


		<div class="row">
			<div class="col-lg-12" >
				<div class="form-group row">
					<label class="col-sm-2 col-form-label"> Description</label>
					<div class="col-sm-10">

						<div class="wrs_container">
							<div class="wrs_row">
								<div class="wrs_col wrs_s12">
									<div id="editorContainer">
										<div id="toolbarLocation"></div>
										<textarea id="description" class="faqcustomdata wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea>

									</div>
									<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
								</div>

							</div>
						</div>


					</div>
				</div>
			</div>

		</div>


		



			<script type="text/javascript" >
				createEditorInstance("en", {});
			</script>
			<script>
				function rand(){

					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';



					$.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>faqs/img.php',
						data: data2,
						contentType: 'application/json; charset=utf-8',
						dataType: 'json',

						success: function (result,xhr) {
							console.log("reply");

							setState('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','validateFormc=1&title='+$('#title').val()+'&category='+$('#category').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?>')
						},
						error: function(e){

							console.log("ERROR: ", e);
						}
					});
				}


				function mobilePreview(){

					var data2;
					data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';


					$.ajax({
						type: 'POST',
						url: '<?php echo SECURE_PATH;?>faqs/img.php',
						data: data2,
						contentType: 'application/json; charset=utf-8',
						dataType: 'json',

						success: function (result,xhr) {


							console.log('description',tinymce.get('description').getContent());

							var html = data2.description;
							var iframe = $('#displayframe');

							iframe[0].srcdoc =    (tinymce.get('description').getContent());

							console.log("iframe",iframe);

						},
						error: function(e){

							console.log("ERROR: ", e);
						}
					});

				}


			</script>




			<div class="form-group row pl-3">
		
		<div class="col-lg-12">
			<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="rand();">Submit</a>
		</div>
										   

										   
										
									
								
							
		
			
	<?php
		unset($_SESSION['error']);

        unset($_SESSION['description']);
	}
	if(isset($_POST['validateFormc'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';

  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  
	$field = 'category';
	if(!$post['category'] || strlen(trim($post['category'])) == 0){
	  $_SESSION['error'][$field] = "* Category cannot be empty";
	}
	$field = 'title';
	if(!$post['title'] || strlen(trim($post['title'])) == 0){
	  $_SESSION['error'][$field] = "* Title cannot be empty";
	}
	
	$field = 'description';
	if(!$_SESSION['description'] || strlen(trim($_SESSION['description'])) == 0){
	  $_SESSION['error'][$field] = "* Description cannot be empty";
	}

	if(!isset($_SESSION['description']))
        $_SESSION['description'] = "";

	
	
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateFormc'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','addForm=1&category=<?php echo $post['category'];?>&title=<?php echo $post['title'];?>&description=<?php echo $post['description'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		
		if($id=='NULL')
		{
		
	
			$result=$database->query("INSERT INTO  faqs  set title='".$post['title']."',category='".$post['category']."',description ='".str_replace("'","&#39;",$_SESSION['description'])."',estatus='1',status='1',timestamp='".time()."'" );

 


			$sid= mysqli_insert_id($con);
			if($result){
				
			

			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i>Faq Created Successfully
					</div>
				</div>
			</div>
			<script type="text/javascript">
				
				animateForm('<?php echo SECURE_PATH;?>faqs/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			
		  </script>
	 <?php
		}else{
			?>
			<div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> Faq Insetion Failed.
					</div>
				</div>
			</div>
			<script type="text/javascript">
			
			  animateForm('<?php echo SECURE_PATH;?>faqs/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
		  </script>
	 	<?php

		}
    
	}else{

		$result=$database->query("update   faqs  set title='".$post['title']."',category='".$post['category']."',description ='".str_replace("'","&#39;",$_SESSION['description'])."' where id='".$id."'" );

 


			$sid= mysqli_insert_id($con);
			if($result){
				
			

			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i>Faq Updated Successfully
					</div>
				</div>
			</div>
			<script type="text/javascript">
				
				animateForm('<?php echo SECURE_PATH;?>faqs/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			
		  </script>
	 <?php
		}else{
			?>
			<div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> Faq Updation Failed.
					</div>
				</div>
			</div>
			<script type="text/javascript">
				
			  animateForm('<?php echo SECURE_PATH;?>faqs/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
		  </script>
	 	<?php

		}
    
	}
}
}
	?>
<?php


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	?>
	
	<?php
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'faqs';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."faqs/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." ";
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
 <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50,
    "order": [[ 1, 'asc' ]]
    
  });

</script>
	

  	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Frequently Asked Questions Report</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered dashboard-table" width="100%" cellspacing="0"  id="dataTable">
									<thead>
										<tr>
											<th class="text-left">Sr.No.</th>
											<th class="text-left">Category</th>
											<th class="text-left">Title</th>
											<th class="text-left">Description</th>
											<th class="text-left">Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
									
									$k=1;
									while($value = mysqli_fetch_array($data_sel))
									{
					 
                  					?>
										<tr>
										<td class="text-left"><?php echo $k;?></td>
												<td class="text-left" ><?php echo $database->get_name('faq_categories','id',$value['category'],'category'); ?></td>
												<td class="text-left" ><?php echo $value['title']; ?></td>
												<td class="text-left"  width="32%><?php echo urldecode($value['description']);?></td>
												<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
												<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a>&nbsp;&nbsp;
												<?php
												if($value['status']==1){ ?>
													<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','enablestatus=1&id=<?php echo $value['id']; ?>&status=disable')">Disable </a>
												<?php }else{ ?>
													<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','enablestatus=1&id=<?php echo $value['id']; ?>&status=enable')">Enable </a>
												<?php } ?>
											</td>
											</tr>
									<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
  </section>
  
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}


if(isset($_REQUEST['enablestatus'])){
	if($_REQUEST['status']=='enable'){
		$result=$database->query("update  faqs  set status='1' where  id='".$_REQUEST['id']."'");
		if($result){
			?>
			

			<script type="text/javascript">
			
				alert("Faq Enabled Successfully");
				setStateGet('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','addForm=1');
				setStateGet('adminTable','<?php echo SECURE_PATH;?>faqs/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				
				
			</script>
			<?php
		}

	}else if($_REQUEST['status']=='disable'){
		$result=$database->query("update  faqs  set status='0' where  id='".$_REQUEST['id']."'");
		if($result){
			?>

			<script type="text/javascript">
			
				alert("User Disabled Successfully");
					setStateGet('adminForm','<?php echo SECURE_PATH;?>faqs/process.php','addForm=1');
					setStateGet('adminTable','<?php echo SECURE_PATH;?>faqs/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				
			</script>
			
			<?php
		}
	}
	

}
?>
	

<?php
if(isset($_GET['rowDelete'])){
	$database->query("update  faqs set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Faq Deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>faqs/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>

<?php
if(isset($_POST['mobilePreview'])){
    echo urldecode($_SESSION['description']);
}
?>
	<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	<script>
	 $(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	$(".selectpicker3").selectpicker('refresh');
	</script>
	
