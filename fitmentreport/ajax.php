<?php
error_reporting(0);
include('../include/session.php');
?>

<?php

if(isset($_REQUEST['gettypev']))
{	
	$selchapter=$database->query("select * from chapter where estatus='1'   order by id asc limit 0,1");
	$rowchapter=mysqli_fetch_array($selchapter);
	if(isset($_POST['chapter'])){
		if($_POST['chapter']!=''){
			$_POST['chapter']=$_POST['chapter'];
			$chaptercon=" AND id IN (".$_POST['chapter'].") ";
			$chaptercon1="  AND chapter IN (".$_POST['chapter'].") ";
		}else{
			$_POST['chapter']=$rowchapter['id'];
			$chaptercon="  AND id IN (".$rowchapter['id'].") ";
			$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
		}
	}else{
		$_POST['chapter']=$rowchapter['id'];
		$chaptercon="  AND id IN (".$rowchapter['id'].") ";
		$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
	}
	if($_POST['typev']=='horizontal'){
	?>
 
	<div class="table-responsive pt-3" >
		<table class="table table-bordered">
			<thead>
				<tr>
					<th rowspan="2" colspan="2">
						<div class="col-lg-6">
							<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&typev='+$('#typev').val()+'')" >	
								<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
								<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
							</select>
						</div>
					</th>
					<th colspan="2"></th>
					<th colspan="2">Easy</th>
					<th colspan="2">Moderate</th>
					<th colspan="2">Difficult</th>
					<th colspan="2">Highly Difficult</th>
					
				</tr>
				<tr>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
				</tr>

			</thead>
			<tbody>
				<?php
				$j=1;
				$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
				while($rowchapter=mysqli_fetch_array($selchapter)){
					
					?>
					<tr>
						<th rowspan="6"><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
					</tr>
					<?php
					$totval=0;
					$totval_per=0;
					$sqllo=$database->query("select * from question_useset where estatus='1' ");
					$i=1;
					while($rowllo=mysqli_fetch_array($sqllo)){
						$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
						$rowtot=mysqli_fetch_array($seltot);

						$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!='0' ");
						$rowtot1=mysqli_fetch_array($seltot1);
						
					
						$pertot = $rowtot['cnt']/$rowtot1['cnt'];
						if(is_nan($pertot))
							$pertot=0;
						else
							$pertot=$pertot;
						$percenttot= number_format( $pertot * 100) . '%';
						$pecento= number_format( $pertot * 100);
						$totval=$totval+$rowtot['cnt'];
						$totval_per=$totval_per+$pecento;
						?>
							<tr>
								<td><?php echo $rowllo['usageset']; ?></td>
								<td><?php echo $rowtot['cnt']; ?></td>
								<td><?php echo $percenttot; ?></td>
								<?php
								$sqllo1=$database->query("select * from complexity where estatus='1' ");
								while($rowllo1=mysqli_fetch_array($sqllo1)){
									
									$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo1['id']."' and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
									$row=mysqli_fetch_array($sel);
									
									$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset='".$rowllo['id']."' and complexity!=0  ");
									$row1=mysqli_fetch_array($sel1);
									$percentp = $row['cnt']/$row1['cnt'];
									if(is_nan($percentp))
										$percentp=0;
									else
										$percentp=$percentp;
									$percent_vp= number_format( $percentp * 100) . '%';
									?>

								<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $rowchapter['id'];?>&usageset=<?php echo $rowllo['id'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
								<td><?php echo $percent_vp; ?></td>
								<?php } ?>
								
							</tr>
						<?php
						
					$i++;
					}
					?>
						<tr>
						<td>Totals</td>
						<td><?php echo $totval; ?></td>
						<td><?php echo $totval_per; ?></td>
						<?php
						$sqllo1=$database->query("select * from complexity where estatus='1' ");
						while($rowllo1=mysqli_fetch_array($sqllo1)){
							
							$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo1['id']."' and usageset!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
							$row=mysqli_fetch_array($sel);
							
							$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!=0 and complexity!=0  ");
							$row1=mysqli_fetch_array($sel1);
							$percentp = $row['cnt']/$row1['cnt'];
							if(is_nan($percentp))
								$percentp=0;
							else
								$percentp=$percentp;
							$percent_vp= number_format( $percentp * 100) . '%';
						?>
							<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
							<td><?php echo $percent_vp; ?></td>
						<?php  } ?>
					</tr>
					<?php
				}
					?>
					
				
			</tbody>
		</table>
	</div>
  <?php
	}else if($_POST['typev']=='vertical'){
				
		?>
			<div class="table-responsive pt-3" >
				<table class="table table-bordered">
					<thead>
						<tr>
							<th rowspan="2" colspan="2">
								<div class="col-lg-6">
									<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&typev='+$('#typev').val()+'')" >	
										<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
										<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
									</select>
								</div>
							</th>
							<th colspan="2"></th>
							<?php
							$sel1=$database->query("select * from question_useset where estatus='1'");
							while($row1=mysqli_fetch_array($sel1)){
							?>
								<th colspan="2"><?php echo $row1['usageset']; ?></th>
							<?php
							}
							?>
							
							
						</tr>
						<tr>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
						</tr>

					</thead>
					<tbody>
						<?php
						$j=1;
						$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
						while($rowchapter=mysqli_fetch_array($selchapter)){
							?>
							<tr>
								<th rowspan="6"><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
							</tr>
							<?php
							$totval=0;
							$totval_per=0;
							$totval1=0;
							$totval_per1=0;
							$sqllo=$database->query("select * from complexity where estatus='1' ");
							$i=1;
							while($rowllo=mysqli_fetch_array($sqllo)){
								$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
								$rowtot=mysqli_fetch_array($seltot);

								$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0 and complexity!='0'  ");
								$rowtot1=mysqli_fetch_array($seltot1);
								
								
								$pertot = $rowtot['cnt']/$rowtot1['cnt'];
								if(is_nan($pertot))
									$pertot=0;
								else
									$pertot=$pertot;
								
								$percenttot= number_format( $pertot * 100) . '%';
								$pecento= number_format( $pertot * 100);
								$totval=$totval+$rowtot['cnt'];
								$totval_per=$totval_per+$pecento;
								?>
									<tr>
										<td><?php echo $rowllo['complexity']; ?></td>
										<td><?php echo $rowtot['cnt']; ?></td>
										<td><?php echo $percenttot; ?></td>
										<?php
										$sqllo1=$database->query("select * from question_useset where estatus='1' ");
										while($rowllo1=mysqli_fetch_array($sqllo1)){
											
											$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo['id']."' and usageset='".$rowllo1['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
											$row=mysqli_fetch_array($sel);
											
											//$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and complexity!=0 and usageset!=0  ");
											
											$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0   and complexity!=0 and usageset='".$rowllo1['id']."' ");
											$row1=mysqli_fetch_array($sel1);
											
											$percentp = $row['cnt']/$row1['cnt'];
											
											if(is_nan($percentp))
												$percentp=0;
											else
												$percentp=$percentp;
											$percent_vp= number_format( $percentp * 100) . '%';
											$totval1=$totval1+$row['cnt'];
											$totval_per1=$totval_per1+$percentp;
											?>

										<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $rowchapter['id'];?>&usageset=<?php echo $rowllo1['id'];?>&complexity=<?php echo $rowllo['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
										<td><?php echo $percent_vp; ?></td>
										<?php } ?>
										
									</tr>
								<?php
								
							$i++;
							}
							?>
								<tr>
								<td>Totals</td>
								<td><?php echo $totval; ?></td>
								<td><?php echo $totval_per; ?></td>
								<?php
								$sqllo1=$database->query("select * from question_useset where estatus='1' ");
								while($rowllo1=mysqli_fetch_array($sqllo1)){
									
									$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo1['id']."' and complexity!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
									$row=mysqli_fetch_array($sel);
									$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and  usageset='".$rowllo1['id']."' and complexity!=0  ");
									$row1=mysqli_fetch_array($sel1);
									$percentp = $row['cnt']/$row1['cnt'];
									if(is_nan($percentp))
										$percentp=0;
									else
										$percentp=$percentp;
									$percent_vp= number_format( $percentp * 100) . '%';
								?>
									<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&usageset=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
									<td><?php echo $percent_vp; ?></td>
									
								<?php  } ?>
							</tr>
							<?php
						}
							?>
							
						
					</tbody>
				</table>
			</div>
		<?php
	}
}


//Topic Wise horizontal and vertical

if(isset($_REQUEST['gettypev1']))
{	
	$selchapter=$database->query("select * from topic where estatus='1'   order by id asc limit 0,1");
	$rowchapter=mysqli_fetch_array($selchapter);
	if(isset($_POST['chapter'])){
		if($_POST['chapter']!=''){
			$_POST['chapter']=$_POST['chapter'];
			$chaptercon=" AND id IN (".$_POST['chapter'].") ";
			$chaptercon1="  AND chapter IN (".$_POST['chapter'].") ";
		}else{
			$_POST['chapter']=$rowchapter['id'];
			$chaptercon="  AND id IN (".$rowchapter['id'].") ";
			$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
		}
	}else{
		$_POST['chapter']=$rowchapter['id'];
		$chaptercon="  AND id IN (".$rowchapter['id'].") ";
		$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
	}
	if(isset($_POST['topic'])){
		if($_POST['topic']!=''){
			$_POST['topic']=$_POST['topic'];
			$topiccon="  AND id='".$_POST['topic']."'";
			
		}else{
			$_POST['topic']="";
			$topiccon="";
		}
	}else{
		$_POST['topic']="";
		$topiccon="";
	}
	if($_POST['typev']=='horizontal'){
	?>
 
	<div class="table-responsive pt-5" >
		<table class="table table-bordered">
			<thead>
				<tr>
					<th rowspan="2" colspan="2">
						<div class="col-lg-6">
								<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical1','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev1=1&typev='+$('#typev').val()+'&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>')" >	
									<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
									<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
								</select>
							</div>
					</th>
					<th colspan="2"></th>
					<th colspan="2">Easy</th>
					<th colspan="2">Moderate</th>
					<th colspan="2">Difficult</th>
					<th colspan="2">Highly Difficult</th>
				</tr>
				<tr>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
					<th>Total</th>
					<th>%</th>
				</tr>

			</thead>
			<tbody>
				<?php
				$j=1;
				$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
				while($rowtopic=mysqli_fetch_array($seltopic)){
					?>
					<tr>
						<th rowspan="5" ><?php echo $rowtopic['topic']; ?> Summary</th>
					</tr>
					<?php
					$totval=0;
					$totval_per=0;
					$sqllo=$database->query("select * from question_useset where estatus='1' ");
					while($rowllo=mysqli_fetch_array($sqllo)){
						$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
						$rowtot=mysqli_fetch_array($seltot);

						$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0  and usageset!='0' ");
						$rowtot1=mysqli_fetch_array($seltot1);
						
						
						$pertot = $rowtot['cnt']/$rowtot1['cnt'];
						if(is_nan($pertot))
							$pertot=0;
						else
							$pertot=$pertot;
						$percenttot= number_format( $pertot * 100) . '%';
						$pecento= number_format( $pertot * 100);
						$totval=$totval+$rowtot['cnt'];
						$totval_per=$totval_per+$pecento;
						?>
							<tr>
								<td><?php echo $rowllo['usageset']; ?></td>
								<td><?php echo $rowtot['cnt']; ?></td>
								<td><?php echo $percenttot; ?></td>
								
								<?php

								$sqllo1=$database->query("select * from complexity where estatus='1' ");
								while($rowllo1=mysqli_fetch_array($sqllo1)){
									
									
									$sel=$database->query("select count(id) as cnt from createquestion where estatus='1' and complexity='".$rowllo1['id']."' and usageset='".$rowllo['id']."'  and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
									$row=mysqli_fetch_array($sel);
									$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$_POST['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 and usageset='".$rowllo['id']."' and complexity!=''  ");
									$row1=mysqli_fetch_array($sel1);
									$percentp = $row['cnt']/$row1['cnt'];
									if(is_nan($percentp))
										$percentp=0;
									else
										$percentp=$percentp;
									$percent_vp= number_format( $percentp * 100) . '%';
									?>

								<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['id'];?>&topic=<?php echo $rowtopic['id'];?>&usageset=<?php echo $rowllo['id'];?>&complexity=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
								<td><?php echo $percent_vp; ?></td>
								<?php } ?>
								
							</tr>
						<?php
						
					
					}
					?>
						
					
				<?php
					$j++;
				}
				?>
				
				
			</tbody>
		</table>
	</div>
  <?php
	}else if($_POST['typev']=='vertical'){
		?>
			<div class="table-responsive pt-3" >
				<table class="table table-bordered">
					<thead>
						<tr>
							<th rowspan="2" colspan="2">
								<div class="col-lg-6">
									<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical1','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypev1=1&chapter=<?php echo $_POST['chapter']; ?>&topic=<?php echo $_POST['topic']; ?>&typev='+$('#typev').val()+'')" >	
										<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
										<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
									</select>
								</div>
							</th>
							<th colspan="2"></th>
							<?php
							$sel1=$database->query("select * from question_useset where estatus='1'");
							while($row1=mysqli_fetch_array($sel1)){
							?>
								<th colspan="2"><?php echo $row1['usageset']; ?></th>
							<?php
							}
							?>
							
							
						</tr>
						<tr>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
							<th>Total</th>
							<th>%</th>
						</tr>

					</thead>
					<tbody>
						<?php
						$j=1;
						$seltopic=$database->query("select * from topic where estatus='1'".$chaptercon1."  ".$topiccon."");
						while($rowtopic=mysqli_fetch_array($seltopic)){
							?>
							<tr>
								<th rowspan="6"><?php echo $rowtopic['topic']; ?> Chapter Summary</th>
							</tr>
							<?php
							$sqllo=$database->query("select * from complexity where estatus='1' ");
							$i=1;
							$totval=0;
							$totval_per=0;
							while($rowllo=mysqli_fetch_array($sqllo)){
								$seltot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
								$rowtot=mysqli_fetch_array($seltot);

								$seltot1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowchapter['id'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0  and complexity!='0' ");
								$rowtot1=mysqli_fetch_array($seltot1);
								
								
								$pertot = $rowtot['cnt']/$rowtot1['cnt'];
								if(is_nan($pertot))
									$pertot=0;
								else
									$pertot=$pertot;
								$percenttot= number_format( $pertot * 100) . '%';
								$pecento= number_format( $pertot * 100);
								$totval=$totval+$rowtot['cnt'];
								$totval_per=$totval_per+$pecento;
								?>
									<tr>
										<td><?php echo $rowllo['complexity']; ?></td>
										<td><?php echo $rowtot['cnt']; ?></td>
										<td><?php echo $percenttot; ?></td>
										<?php
										$sqllo1=$database->query("select * from question_useset where estatus='1' ");
										while($rowllo1=mysqli_fetch_array($sqllo1)){
											
											$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and complexity='".$rowllo['id']."' and usageset='".$rowllo1['id']."' and find_in_set(".$rowtopic['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0 ");
											$row=mysqli_fetch_array($sel);
											
											$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowtopic['chapter'].",chapter)>0  and find_in_set(".$rowtopic['id'].",topic)>0  and complexity!=0 and  usageset='".$rowllo1['id']."'  ");
											$row1=mysqli_fetch_array($sel1);
											$percentp = $row['cnt']/$row1['cnt'];
											if(is_nan($percentp))
												$percentp=0;
											else
												$percentp=$percentp;
											$percent_vp= number_format( $percentp * 100) . '%';
											?>

										<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $rowtopic['chapter'];?>&topic=<?php echo $rowtopic['id'];?>&usageset=<?php echo $rowllo1['id'];?>&complexity=<?php echo $rowllo['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
										<td><?php echo $percent_vp; ?></td>
										<?php } ?>
										
									</tr>
								<?php
								
							$i++;
							}
							?>
								
							<?php
						}
							?>
							
						
					</tbody>
				</table>
			</div>
		<?php
	}
}

if(isset($_REQUEST['gettypevqtype']))
{	
	$selchapter=$database->query("select * from chapter where estatus='1'   order by id asc limit 0,1");
	$rowchapter=mysqli_fetch_array($selchapter);
	if(isset($_POST['chapter'])){
		if($_POST['chapter']!=''){
			$_POST['chapter']=$_POST['chapter'];
			$chaptercon=" AND id IN (".$_POST['chapter'].") ";
			$chaptercon1="  AND chapter IN (".$_POST['chapter'].") ";
		}else{
			$_POST['chapter']=$rowchapter['id'];
			$chaptercon="  AND id IN (".$rowchapter['id'].") ";
			$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
		}
	}else{
		$_POST['chapter']=$rowchapter['id'];
		$chaptercon="  AND id IN (".$rowchapter['id'].") ";
		$chaptercon1="  AND chapter IN (".$rowchapter['id'].") ";
	}
	if($_POST['typev']=='horizontal'){
	?>
 
	<div class="table-responsive pt-5">
		<div id="table-scroll" class="table-scroll">
			 <div class="table-wrap">
				<table class="main-table table table-bordered">
					<thead>
						<tr>
							<th class="fixed-side" rowspan="2" colspan="2">
								<div class="col-lg-6">
									<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical2','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypevqtype=1&chapter=<?php echo $_POST['chapter']; ?>&typev='+$('#typev').val()+'')" >	
										<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
										<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
									</select>
								</div>
							</th>
							<?php
							$sel=$database->query("select * from questiontype where estatus='1'");
							while($row=mysqli_fetch_array($sel)){
							?>
								<th  colspan="2"><?php echo $row['questiontype']; ?></th>
							<?php
							}
							?>
							
						</tr>
						<tr>
							<?php
							$sel=$database->query("select * from questiontype where estatus='1'");
							while($row=mysqli_fetch_array($sel)){
							?>
								<th>Total</th>
								<th>%</th>
							<?php } ?>
						</tr>

					</thead>
					<tbody>
						<?php
						$j=1;
						$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
						while($rowchapter=mysqli_fetch_array($selchapter)){
							?>
							<tr>
								<th rowspan="6" class="fixed-side" ><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
							</tr>
							<?php
							$sqllo=$database->query("select * from question_useset where estatus='1' ");
							$i=1;
							while($rowllo=mysqli_fetch_array($sqllo)){
								?>
									<tr>
										<td class="fixed-side"><?php echo $rowllo['usageset']; ?></td>
										<?php
										$sqllo1=$database->query("select * from questiontype where estatus='1' ");
										while($rowllo1=mysqli_fetch_array($sqllo1)){
											
											$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
											$row=mysqli_fetch_array($sel);
											
											$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset='".$rowllo['id']."' and inputquestion!=''  ");
											$row1=mysqli_fetch_array($sel1);
											$percentp = $row['cnt']/$row1['cnt'];
											if(is_nan($percentp))
												$percentp=0;
											else
												$percentp=$percentp;
											$percent_vp= number_format( $percentp * 100) . '%';
											?>

										<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&usageset=<?php echo $rowllo['id'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
										<td><?php echo $percent_vp; ?></td>
										<?php } ?>
										
									</tr>
								<?php
								
							$i++;
							}
							?>
								<tr>
								<td class="fixed-side">Totals</td>
								<?php
								$sqllo1=$database->query("select * from questiontype where estatus='1' ");
								while($rowllo1=mysqli_fetch_array($sqllo1)){
									
									$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
									$row=mysqli_fetch_array($sel);
									
									$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!=0 and inputquestion!=''  ");
									$row1=mysqli_fetch_array($sel1);
									$percentp = $row['cnt']/$row1['cnt'];
									if(is_nan($percentp))
										$percentp=0;
									else
										$percentp=$percentp;
									$percent_vp= number_format( $percentp * 100) . '%';
								?>
									<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
									<td><?php echo $percent_vp; ?></td>
								<?php  } ?>
							</tr>
							<?php
						}
							?>
							
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
  <?php
	}else if($_POST['typev']=='vertical'){
		?>
			<div class="table-responsive pt-5">
		<div id="table-scroll" class="table-scroll">
			 <div class="table-wrap">
				<table class="main-table table table-bordered">
					<thead>
						<tr>
							<th class="fixed-side" rowspan="2" colspan="2">
								<div class="col-lg-6">
									<select id="typev" class="form-control" name="typev" onChange="setState('Horizontalvertical2','<?php echo SECURE_PATH;?>fitmentreport/ajax.php','gettypevqtype=1&chapter=<?php echo $_POST['chapter']; ?>&typev='+$('#typev').val()+'')" >	
										<option value='horizontal' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'horizontal'){ echo ' selected="selected"';}}else { echo 'selected';} ?>>Horizontal </option>
										<option value='vertical' <?php if(isset($_POST['typev'])){ if($_POST['typev'] == 'vertical'){ echo ' selected="selected"';}}?>>Vertical</option>
									</select>
								</div>
							</th>
							<?php
							$sel=$database->query("select * from questiontype where estatus='1'");
							while($row=mysqli_fetch_array($sel)){
							?>
								<th  colspan="2"><?php echo $row['questiontype']; ?></th>
							<?php
							}
							?>
							
						</tr>
						<tr>
							<?php
							$sel=$database->query("select * from questiontype where estatus='1'");
							while($row=mysqli_fetch_array($sel)){
							?>
								<th>Total</th>
								<th>%</th>
							<?php } ?>
						</tr>

					</thead>
					<tbody>
						<?php
						$j=1;
						$selchapter=$database->query("select * from chapter where estatus='1' ".$chaptercon." ");
						while($rowchapter=mysqli_fetch_array($selchapter)){
							?>
							<tr>
								<th rowspan="6" class="fixed-side" ><?php echo $rowchapter['chapter']; ?> Chapter Summary</th>
							</tr>
							<?php
							$sqllo=$database->query("select * from question_useset where estatus='1' ");
							$i=1;
							while($rowllo=mysqli_fetch_array($sqllo)){
								?>
									<tr>
										<td class="fixed-side"><?php echo $rowllo['usageset']; ?></td>
										<?php
										$sqllo1=$database->query("select * from questiontype where estatus='1' ");
										while($rowllo1=mysqli_fetch_array($sqllo1)){
											
											$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset='".$rowllo['id']."' and find_in_set(".$rowchapter['id'].",chapter)>0  ");
											$row=mysqli_fetch_array($sel);
											
											$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset='".$rowllo['id']."' and inputquestion!=''  ");
											$row1=mysqli_fetch_array($sel1);
											$percentp = $row['cnt']/$row1['cnt'];
											if(is_nan($percentp))
												$percentp=0;
											else
												$percentp=$percentp;
											$percent_vp= number_format( $percentp * 100) . '%';
											?>

										<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&usageset=<?php echo $rowllo['id'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php  if($row['cnt']!=''){ echo $row['cnt']; } else{ echo '0'; } ?></a></td>
										<td><?php echo $percent_vp; ?></td>
										<?php } ?>
										
									</tr>
								<?php
								
							$i++;
							}
							?>
								<tr>
								<td class="fixed-side">Totals</td>
								<?php
								$sqllo1=$database->query("select * from questiontype where estatus='1' ");
								while($rowllo1=mysqli_fetch_array($sqllo1)){
									
									$sel=$database->query("select count(id) as cnt from createquestion where estatus='1'  and find_in_set(".$rowllo1['id'].",inputquestion)>0  and usageset!=0 and find_in_set(".$rowchapter['id'].",chapter)>0  ");
									$row=mysqli_fetch_array($sel);
									
									$sel1=$database->query("select count(id) as cnt from createquestion where estatus='1'   and find_in_set(".$rowchapter['id'].",chapter)>0  and usageset!=0 and inputquestion!=''  ");
									$row1=mysqli_fetch_array($sel1);
									$percentp = $row['cnt']/$row1['cnt'];
									if(is_nan($percentp))
										$percentp=0;
									else
										$percentp=$percentp;
									$percent_vp= number_format( $percentp * 100) . '%';
								?>
									<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>fitmentreport/process1.php?getreport=1&chapter=<?php echo $_POST['chapter'];?>&questiontype=<?php echo $rowllo1['id'];?>")' ><?php echo $row['cnt']; ?></a></td>
									<td><?php echo $percent_vp; ?></td>
								<?php  } ?>
							</tr>
							<?php
						}
							?>
							
						
					</tbody>
				</table>
			</div>
		</div>
	</div>
		<?php
	}
}
?>