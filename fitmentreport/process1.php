<?php
include('../include/session.php');
error_reporting(0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>

 <?php echo $session->commonJS1();?>
 <?php echo $session->commonAdminCSS();?>
   <style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				/*margin:auto;*/
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}

			.list-Unstyles{
				position:absolute;
				z-index:30 !important;
				cursor:pointer;
			}
		</style>
	<script>
		function search_report() {
		
			var class1 = $('#class').val();
			var subject = $('#subject').val();
			var chapter = $('#chapter').val();
			var topic = $('#topic').val();
			var fdate = $('#fdate').val();
			var tdate = $('#tdate').val();
			var exam = $('#exam').val();
			var examtype = $('#examtype').val();
			var status = $('#status').val();
			var complexity = $('#complexity').val();
			var usageset = $('#usageset').val();
			var questiontype = $('#questiontype').val();
			var question_theory = $('#question_theory').val();
			var qset = $('#qset').val();
			var question_id = $('#question_id').val();
			var page = $('#page').val();
			setStateGet('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getreport=1&class='+class1+'&fdate='+fdate+'&tdate='+tdate+'&exam='+exam+'&examtype='+examtype+'&status='+status+'&subject='+subject+'&chapter='+chapter+'&topic='+topic+'&complexity='+complexity+'&usageset='+usageset+'&questiontype='+questiontype+'&qset='+qset+'&question_id='+question_id+'&page='+page+'&questions=<?php echo $_REQUEST['questions']; ?>');
		}
	</script>
	<div class="modal" id="exampleModal5" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					  <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<h5 class="modal-title">Change Usageset</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							  <span aria-hidden="true">&times;</span>
							</button>
						  </div>
						  <div class="modal-body" id="viewDetails5">
							
						  </div>
						  <div class="modal-footer">
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
						  </div>
						</div>
					  </div>
					</div>
	<div class="modal fade" id="reviewexampleModal" tabindex="-1"
				role="dialog" aria-labelledby="exampleModalLabel"
				aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">
								Question Details
							</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="reviewviewDetails">
							
						</div>
						
					</div>
				</div>
			</div>
	<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Question Review
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>	
			<?php
			if(isset($_GET['rowDelete'])){
	$database->query("update  createquestion set estatus='0',dtimestamp='".time()."',dusername='".$_SESSION['username']."' WHERE id = '".$_GET['rowDelete']."'");
	$database->query("insert question_log set question_id='".$_GET['rowDelete']."',username='".$_SESSION['username']."',message='3',estatus='1',timestamp='".time()."'");
	?>
   
  <script type="text/javascript">

location.reload();
  setStateGet('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getreport=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class'])){ echo '&class='.$_REQUEST['class'];}?><?php if(isset($_REQUEST['subject'])){ echo '&subject='.$_REQUEST['subject'];}?><?php if(isset($_REQUEST['chapter'])){ echo '&chapter='.$_REQUEST['chapter'];}?><?php if(isset($_REQUEST['topic'])){ echo '&topic='.$_REQUEST['topic'];}?><?php if(isset($_REQUEST['exam'])){ echo '&exam='.$_REQUEST['exam'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['complexity'])){ echo '&complexity='.$_REQUEST['complexity'];}?><?php if(isset($_REQUEST['usageset'])){ echo '&usageset='.$_REQUEST['usageset'];}?><?php if(isset($_REQUEST['questiontype'])){ echo '&questiontype='.$_REQUEST['questiontype'];}?><?php if(isset($_REQUEST['fdate'])){ echo '&fdate='.$_REQUEST['fdate'];}?><?php if(isset($_REQUEST['tdate'])){ echo '&tdate='.$_REQUEST['tdate'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];}?>');
</script>
   <?php
}
?>
	<div id="adminTable">
		<?php
		if(isset($_REQUEST['getreport']))
		{
			
			
			
		
		
		$sql=$database->query("select * from users where username='".$_SESSION['username']."'");
	$rowsel=mysqli_fetch_array($sql);
	$subject=explode(",",$rowsel['subject']);
   $chapter=explode(",",$rowsel['chapter']);
   if(isset($_REQUEST['subject'])){
	   if($_REQUEST['subject']!=''){
		   $_REQUEST['subject']=$_REQUEST['subject'];
	   }else{
		    $_REQUEST['subject']=$rowsel['subject'];
	   }
   }else{
	    $_REQUEST['subject']=$rowsel['subject'];
   }

	?>
		<input type="hidden" id="que" value="<?php if(isset($_REQUEST['questions'])) { echo $_REQUEST['questions']; } ?>"/>
		<input type="hidden" id="que1" value="<?php if(isset($_REQUEST['questions'])) { echo $_REQUEST['questions']; } ?>"/>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Question Details</h6>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">From Date</label>
										<div class="col-sm-8">
											
											<input type="text" name="fdate" id="fdate" class="datepicker form-control" value="<?php echo $_REQUEST['fdate'];?>" onblur="search_report();" placeholder="Select Date" >
											<span class="error"><?php if(isset($_SESSION['error']['fdate'])){ echo $_SESSION['error']['fdate'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">To Date</label>
										<div class="col-sm-8">
											
											<input type="text" name="tdate" id="tdate" class="datepicker form-control" value="<?php echo $_REQUEST['tdate'];?>" onblur="search_report();" placeholder="Select Date" >
											<span class="error"><?php if(isset($_SESSION['error']['tdate'])){ echo $_SESSION['error']['tdate'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Class</label>
										<div class="col-sm-8">
											<select class="form-control" name="class" value=""  id="class" onchange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from class where estatus='1'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class'])) { if($_REQUEST['class']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Exam</label>
										<div class="col-sm-8">
											<select class="form-control" name="exam" value=""  id="exam" onchange="search_report();">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='1') { echo 'selected="selected"'; }  } ?>>NEET</option>
												<option value='2' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='2') { echo 'selected="selected"'; }  } ?>>JEE</option>
												<option value='3' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']=='3') { echo 'selected="selected"'; }  } ?>>EAMCET</option>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
										</div>
									</div>
								</div>
								<?php
								if(isset($_REQUEST['exam'])) { 
									if($_REQUEST['exam']=='1') {
										$style="style='display:none;'";
									}else{
										$style="";
									}
								}
								?>
								<div class="col-lg-4" <?php echo $style; ?> >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Exam Type</label>
										<div class="col-sm-8">
											<select class="form-control" name="examtype" value=""  id="examtype" onchange="search_report();">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']=='1') { echo 'selected="selected"'; }  } ?>>Mains</option>
												<option value='2' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']=='2') { echo 'selected="selected"'; }  } ?>>Advance</option>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['examtype'])){ echo $_SESSION['error']['examtype'];}?></span>
										</div>
									</div>
								</div> 
								<div class="col-lg-4" >
									<div class="form-group row" id="aaa1">
										<label class="col-sm-4 col-form-label">Subject</label>
										<div class="col-sm-8">
										<select class="form-control" name="subject" value=""   id="subject" onChange="search_report();">
											<option value=''>-- Select --</option>
											<?php
											$row = $database->query("select * from subject where estatus='1' and id ='".$_REQUEST['subject']."' ");
											while($data = mysqli_fetch_array($row))
											{
												?>
											<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
											<?php
											}
											?>
										</select>
											<span class="error"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
										</div>
									</div>
								</div>
								<?php
								?>
								<div class="col-lg-4" >
									<div class="form-group row" id="fff">
										<label class="col-sm-4 col-form-label">Chapter</label>
										<div class="col-sm-8">
										<select class="form-control" name="chapter" value=""   id="chapter" onChange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from chapter where estatus='1' and id='".$_REQUEST['chapter']."'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if($_REQUEST['chapter']==$data['id']) { echo 'selected="selected"'; }   } ?>><?php echo $data['chapter'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="fff">
										<label class="col-sm-4 col-form-label">Topic</label>
										<div class="col-sm-8">
										<select class="form-control" name="topic" value=""   id="topic" onChange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from topic where estatus='1' and id='".$_REQUEST['topic']."'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['topic'])) { if($_REQUEST['topic']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['topic'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Status</label>
										<div class="col-sm-8">
										<select class="form-control" name="status" value=""   id="status" onChange="search_report();">
												<option value=''>-- Select --</option>
												<option value='0' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='0') { echo 'selected="selected"'; }  } ?>>Pending</option>
												<option value='1' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='1') { echo 'selected="selected"'; }  } ?>>Verified</option>
												<option value='2' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='2') { echo 'selected="selected"'; }  } ?>>Rejected</option>
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['status'])){ echo $_SESSION['error']['status'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Usage Set</label>
										<div class="col-sm-8">
										<select class="form-control" name="usageset" value=""   id="usageset" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$uageset = $database->query("SELECT * FROM question_useset WHERE estatus='1' "); 
												
												while($datauageset = mysqli_fetch_array($uageset))
												{
													?>
												<option value="<?php echo $datauageset['id'];?>" <?php if(isset($_REQUEST['usageset'])) { if($_REQUEST['usageset']==$datauageset['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datauageset['usageset'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Complexity</label>
										<div class="col-sm-8">
										<select class="form-control" name="complexity" value=""   id="complexity" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$dcomplexity = $database->query("SELECT * FROM complexity WHERE estatus='1' "); 
												
												while($datacomplexity = mysqli_fetch_array($dcomplexity))
												{
													?>
												<option value="<?php echo $datacomplexity['id'];?>" <?php if(isset($_REQUEST['complexity'])) { if($_REQUEST['complexity']==$datacomplexity['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datacomplexity['complexity'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Type of Question</label>
										<div class="col-sm-8">
										<select class="form-control" name="questiontype" value=""   id="questiontype" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$dquestion = $database->query("SELECT * FROM questiontype WHERE estatus='1' "); 
												
												while($datadquestion = mysqli_fetch_array($dquestion))
												{
													?>
												<option value="<?php echo $datadquestion['id'];?>" <?php if(isset($_REQUEST['questiontype'])) { if($_REQUEST['questiontype']==$datadquestion['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datadquestion['questiontype'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['questiontype'])){ echo $_SESSION['error']['questiontype'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row" >
										<label class="col-sm-4 col-form-label">Question Paper Set</label>
										<div class="col-sm-8">
										<select class="form-control" name="qset" value=""   id="qset" onChange="search_report();">
											<option value=''>-- Select --</option>
											<option value='All' <?php if(isset($_REQUEST['qset'])) { if($_REQUEST['qset']=='All') { echo 'selected="selected"'; }  } ?>>All</option>
												<?php
												$previous_sets = $database->query("SELECT * FROM previous_sets WHERE estatus='1' "); 
												
												while($datasets = mysqli_fetch_array($previous_sets))
												{
													?>
												<option value="<?php echo $datasets['id'];?>" <?php if(isset($_REQUEST['qset'])) { if($_REQUEST['qset']==$datasets['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datasets['qset'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['qset'])){ echo $_SESSION['error']['qset'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Page No.</label>
										<div class="col-sm-8">
											<input type="text" name="page" id="page" class="form-control" autocomplete="off" onchange="search_report();"  onkeypress="return onlyNumbers(event);" value="<?php if(isset($_REQUEST['page'])){ echo $_REQUEST['page'];}else{ echo '1'; }?>" >	
										</div>
									</div>
								</div>
								<div class="col-lg-4" >
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Question Id</label>
										<div class="col-sm-8">
											<input type="text" name="question_id" id="question_id" class="form-control" autocomplete="off" onchange="search_report();" value="<?php if(isset($_REQUEST['question_id'])){ echo $_REQUEST['question_id'];}else{}?>" >	
										</div>
									</div>
								</div>
								
							</div>
										<?php	//Pagination code
										$limit=50;
										if(isset($_REQUEST['page']))
										{
											$start = ($_REQUEST['page'] - 1) * $limit;     //first item to display on this page
											$page=$_REQUEST['page'];
										}
										else
										{
											$start = 0;      //if no page var is given, set start to 0
										$page=0;
										}
										//Search Form

										$tableName = 'createquestion';
										
										if(isset($_REQUEST['keyword'])){
										}
										
										if(isset($_REQUEST['class'])){
											if($_REQUEST['class']!=""){
												$class=" AND find_in_set(".$_REQUEST['class'].",class) >0 ";
											} else {
												$class="";
											}
										}else{
											$class="";
										}
										if(isset($_REQUEST['subject'])){
											if($_REQUEST['subject']!=""){
												$subject=" AND subject='".$_REQUEST['subject']."'";
											} else {
												$subject="";
											}
										}else{
											$subject="";
										}
										if(isset($_REQUEST['chapter'])){
											if($_REQUEST['chapter']!=""){
												$chapter=" AND find_in_set(".$_REQUEST['chapter'].",chapter) >0 ";
											} else {
												$chapter="";
											}
										}else{
											$chapter="";
										}
										if(isset($_REQUEST['exam'])){
											if($_REQUEST['exam']!=""){
												$exam=" AND exam LIKE '%".$_REQUEST['exam']."%'";
											} else {
												$exam="";
											}
										}else{
											$exam="";
										}
										if(isset($_REQUEST['examtype'])){
											if($_REQUEST['examtype']!=""){
												$examtype=" AND pexamtype='".$_REQUEST['examtype']."'";
											} else {
												$examtype="";
											}
										}else{
											$examtype="";
										}
										if(isset($_REQUEST['topic'])){
											if($_REQUEST['topic']!=""){
												$topic=" AND find_in_set(".$_REQUEST['topic'].",topic) >0 ";
											} else {
												$topic="";
											}
										}else{
											$topic="";
										}

										if(isset($_REQUEST['qset'])){
											if($_REQUEST['qset']!=""){
												if($_REQUEST['qset']!="All"){
													$qset=" AND ppaper='1' AND qset='".$_REQUEST['qset']."'";
												}else{
													$qset=" AND ppaper='1'";
												}
											} else {
												$qset="";
											}
										}else{
											$qset="";
										}

										if(isset($_REQUEST['complexity'])){
											if($_REQUEST['complexity']!=""){
												$complexity=" AND complexity ='".$_REQUEST['complexity']."'";
											} else {
												$complexity="";
											}
										}else{
											$complexity="";
										}
										if(isset($_REQUEST['usageset'])){
											if($_REQUEST['usageset']!=""){
												$usageset=" AND usageset ='".$_REQUEST['usageset']."'";
											} else {
												$usageset="";
											}
										}else{
											$usageset="";
										}
										if(isset($_REQUEST['questiontype'])){
											if($_REQUEST['questiontype']!=""){
												$questiontype=" AND inputquestion  In (".$_REQUEST['questiontype'].")";
											} else {
												$questiontype="";
											}
										}else{
											$questiontype="";
										}
										
										if(isset($_REQUEST['question_id'])){
											if($_REQUEST['question_id']!=""){
												$question_id=" AND id ='".$_REQUEST['question_id']."'";
											} else {
												$question_id="";
											}
										}else{
											$question_id="";
										}
										
										if($_REQUEST['fdate']!="" && $_REQUEST['tdate']!=""){

											
											$date=" and timestamp between ".strtotime($_REQUEST['fdate']. ' 00:00:01')." and ".strtotime($_REQUEST['tdate']. ' 23:59:59')."";

										} else if($_REQUEST['fdate']!="" && $_REQUEST['tdate']==""){

											$date=" and timestamp between ".strtotime($_REQUEST['fdate']. ' 00:00:01')." and ".strtotime($_REQUEST['fdate']. ' 23:59:59')."";


										} else if($_REQUEST['fdate']=="" && $_REQUEST['tdate']!=""){

											$date=" and timestamp between ".strtotime($_REQUEST['tdate']. ' 00:00:01')." and ".strtotime($_REQUEST['tdate']. ' 23:59:59')."";

										} else {
											$date="";
										}
										if(isset($_REQUEST['status'])){
											if($_REQUEST['status']!=""){
												$status=" AND vstatus1='".$_REQUEST['status']."'";
											} else {
												$status="";
											}
										}else{
											$status="";
										}
										
										
											$groupid="";
											$cond = $date.$class.$subject.$chapter.$topic.$exam.$examtype.$qset.$status.$complexity.$usageset.$questiontype.$question_id;
											$condition=" estatus='1'  and usageset!=0";
											

											if(strlen($condition) > 0){
												$condition ="where ".$condition.$cond;
											}
											$pagination = $session->showPagination(SECURE_PATH."fitmentreport/process1.php?tableDisplay=1&getreport=1&questions='+$('#questions').val()+'&class=".$_REQUEST['class']."&exam=".$_REQUEST['exam']."&examtype=".$_REQUEST['examtype']."&subject=".$_REQUEST['subject']."&chapter=".$_REQUEST['chapter']."&topic=".$_REQUEST['topic']."&fdate=".$_REQUEST['fdate']."&tdate=".$_REQUEST['tdate']."&status=".$_REQUEST['status']."&complexity=".$_REQUEST['complexity']."&usageset=".$_REQUEST['usageset']."&questiontype=".$_REQUEST['questiontype']."&question_theory=".$_REQUEST['question_theory']."&question_id=".$_REQUEST['question_id']."&page=".$_REQUEST['page']."&",$tableName,$start,$limit,$page,$condition);
											$q = "SELECT * FROM $tableName ".$condition." ";
											$result_sel = $database->query($q);
											$numres = mysqli_num_rows($result_sel);
											$query = "SELECT * FROM $tableName ".$condition."  order by timestamp DESC LIMIT $start,$limit ";
											$data_sel = $database->query($query);
										
										if(($start+$limit) > $numres){
											$onpage = $numres;
											}
											else{
											$onpage = $start+$limit;
											}
										if($numres > 0){
											?>
								
								<?php
								if($_REQUEST['usageset']!='' && $_REQUEST['complexity']!=''){
								?>
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered">
												  <thead>
													<tr>
													  <th colspan='2'></th>
													  <?php
														$sql=$database->query("select * from questiontype where estatus='1'");
														while($row=mysqli_fetch_array($sql)){
														?>
															 <th scope="col"><?php echo $row['questiontype']; ?></th>
														<?php } ?>
													  
													</tr>
												  </thead>
												 
												  <tbody>
													<tr>
													  <td><?php echo $database->get_name('question_useset','id',$_REQUEST['usageset'],'usageset'); ?></td>
													  <td><?php echo $database->get_name('complexity','id',$_REQUEST['complexity'],'complexity'); ?></td>
													   <?php
													$sql=$database->query("select * from questiontype where estatus='1'");	
												  while($row=mysqli_fetch_array($sql)){
													 
													  $sql1=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and usageset='".$_REQUEST['usageset']."' and complexity='".$_REQUEST['complexity']."' and   find_in_set(".$row['id'].",inputquestion)>0 ".$topic." ");
													  $row1=mysqli_fetch_array($sql1);
													?>
													  <td><?php if($row1['cnt']!=''){ echo $row1['cnt']; }else{ echo '0'; } ?></td>
													 <?php } ?> 
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								<?php
								}else if($_REQUEST['usageset']!='' && $_REQUEST['questiontype']!=''){
									?>
										<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered">
												  <thead>
													<tr>
													  <th colspan='2'></th>
													  <?php
														$sql=$database->query("select * from complexity where estatus='1'");
														while($row=mysqli_fetch_array($sql)){
														?>
															 <th scope="col"><?php echo $row['complexity']; ?></th>
														<?php } ?>
													  
													</tr>
												  </thead>
												 
												  <tbody>
													<tr>
													  <td><?php echo $database->get_name('question_useset','id',$_REQUEST['usageset'],'usageset'); ?></td>
													  <td><?php echo $database->get_name('questiontype','id',$_REQUEST['questiontype'],'questiontype'); ?></td>
													   <?php
													$sql=$database->query("select * from complexity where estatus='1'");	
												  while($row=mysqli_fetch_array($sql)){
													 
													  $sql1=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$_REQUEST['chapter'].",chapter)>0 and usageset='".$_REQUEST['usageset']."' and complexity='".$row['id']."' and   find_in_set(".$_REQUEST['questiontype'].",inputquestion)>0 ".$topic." ");
													  $row1=mysqli_fetch_array($sql1);
													?>
													  <td><?php if($row1['cnt']!=''){ echo $row1['cnt']; }else{ echo '0'; } ?></td>
													 <?php } ?> 
													</tr>
												</tbody>
											</table>
										</div>
									</div>
								<?php
								}else{
								}
								?>
								<br />
								<input type="hidden" id="questions" value="<?php  if(isset($_REQUEST['questions'])){ if($_REQUEST['questions']!=''){ echo $_REQUEST['questions']; }} ?>"/>
								<div class="row" >	
									<div class="col-md-10">
										<label  for="">
											
											<a class="radius-20 btn btn-success px-4 text-white my-2" onclick="setStateGet('viewDetails5','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getusageData=1&questions='+$('#questions').val()+'&class='+$('#class').val()+'&fdate='+$('#fdate').val()+'&tdate='+$('#tdate').val()+'&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&status='+$('#status').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&complexity='+$('#complexity').val()+'&questiontype='+$('#questiontype').val()+'&question_id='+$('#question_id').val()+'&usageset='+$('#usageset').val()+'')">
											 Change Usage Set</a>
											
										</label>
									</div>
								</div>
								<div class="row" >	
									<div class="col-md-5 custom-control custom-checkbox ml-4 pt-4">
										<input type="checkbox" onclick="checkAll()" class="custom-control-input" id="check" name="checkall" value="0">
										<label class="custom-control-label" for="check">Check All</label>
									</div>
								</div>
								<br />
								<div class="card border-0">
									<div class="card-body table-responsive table_data p-0">
										<table class="table table-bordered dashboard-table mb-0" >
											<thead class="thead-light">
												<tr>
													<th scope="col">Check</th>
													<th scope="col">Q.Id</th>
													<th scope="col">Actions</th>
													<th scope="col">Status</th>
													<th scope="col">Date</th>
													<th scope="col">Question Title</th>
													<th scope="col">Class</th>
													<th scope="col">Exam</th>
													<th scope="col">Usage Set</th>
													<th scope="col">Complexity</th>
													<th scope="col">Subject - Chapter</th>
													
													
												</tr>
											</thead>
										<tbody>
											<?php
											if(isset($_GET['page'])){
											
											if($_GET['page']==1)
											$i=1;
											else
											$i=(($_GET['page']-1)*50)+1;
											}else $i=1;

											$k = 1;
											//$sel=$database->query("select * from createquestion ".$condition." ");
											//echo "select * from createquestion ".$condition." ";
											while($value = mysqli_fetch_array($data_sel)){
												
												$value['exam']=rtrim($value['exam'],',');
												$value['class']=rtrim($value['class'],',');
												$value['chapter']=rtrim($value['chapter'],',');
												
												$mn=1;
												$exam='';
												$zSqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$value['exam'].")"); 
												while($rowexam=mysqli_fetch_array($zSqlexam)){
													$exam .= $rowexam['exam'].",";
													$mn++;
												}

												if($value['class']=='1'){
													$class='XI';
												}else if($value['class']=='2'){
													$class='XII';
												}else  if($value['class']=='1,2'){
													$class='XI,XII';
												}
												$dtime=$value['timestamp'];
												//$date=date("d-m-Y  H:i:s ", $value["time"]);
												$date1 = new DateTime("@$dtime");
												$dt=$date1->format('d/m/Y  H:i:s');
												$dt1=date('d/m/Y H:i:s',$value['timestamp']);
												$chapter ='';
												$m=1;
												$zSql = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and id IN(".$value['chapter'].")"); 
												while($row=mysqli_fetch_array($zSql)){
													$chapter .= $row['chapter'].",";
													$m++;
												}
												
												 if($value['vstatus1']==1){
													$status='<i class="fas fa-check-circle text-success pr-1"></i>Verified';
												  }
												  elseif($value['vstatus1']==2) {
													$status='<i class="fa fa-times-circle text-danger pr-1"></i>Rejected';

												  }
												  else{
													$status='<i class="fas fa-pause-circle text-warning pr-1"></i>Pending';
												  }
											
											$_POST['questions']=array_unique(explode(",",$_REQUEST['questions']));
											//print_r($_REQUEST['questions']);
										?>
											<tr>
												<td class="text-left">&nbsp;&nbsp;<div class="custom-control custom-checkbox custom-control-inline">
													<input type="checkbox" id="check<?php echo $i; ?>" name="customcheckboxInline1" class="checkBox custom-control-input" onclick="selectCheckBox()" value="<?php echo $value['id'];?>" <?php if(isset($_REQUEST['questions'])) {    if(in_array($value['id'],$_POST['questions'])) { echo 'checked'; } } ?> >
													<label class="custom-control-label" for="check<?php echo $i; ?>"></label></div>
												</td>
												<td class="text-left" ><?php echo $value['id']; ?></td>
												<td class="text-left web-enable-buttons" nowrap>
													<a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>fitmentreport/process2.php','viewDetails=1&class1=<?php echo $_REQUEST['class1']; ?>&exam=<?php echo $_REQUEST['exam']; ?>&subject1=<?php echo $_REQUEST['subject1']; ?>&chapter1=<?php echo $_REQUEST['chapter1']; ?>&topic1=<?php echo $_REQUEST['topic1']; ?>&question_id=<?php echo $_REQUEST['question_id']; ?>&date=<?php echo $_REQUEST['date']; ?>&userlevel=<?php echo $session->userlevel;?>&explanation=<?php echo $_REQUEST['explanation'];?>&start=<?php echo $start; ?>&limit=<?php echo $limit; ?>&id=<?php echo $value['id'];?>')"><i class="fa fa-eye"  style="color:green;"></i></a>&nbsp;&nbsp;&nbsp;

													
												</td>
												
												<td class="text-left"><?php echo $status; ?></td>
												<td class="text-left" ><?php echo $dt1; ?></td>
												
												<?php
												if($value['qtype']=='7'){
													$question=urldecode($value['question']);
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}else if($value['qtype']=='5'){
													$question=$value['question'];
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo urldecode($question); ?></div></td>
												<?php
												}else if($value['qtype']=='8'){
													$question=$value['question'];
												?>
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo urldecode($question); ?></div></td>
												<?php
												}else if($value['qtype']=='3'){
												
													$list1='';
													$list2='';
													$obj=json_decode($value['question'],true);
													foreach($obj as $rowpost2)
													{
														if(strlen($rowpost2)>0)
														{
															$rowpost = explode("_",$rowpost2);
															$list1.=$rowpost[0]."_";
															$list2.=$rowpost[1]."_";
														}
													}
													$qlist1 = explode("_",$list1);
													$qlist2 = explode("_",$list2);
												?>
													<td class="text-left" width="33%">
														<div   style="overflow:hidden;max-width:350px;" id="tableborder-none">
														<div class="d-flex justify-content-between">
														<div>
														<h5>List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														</ul>
														</div>
														<div>
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist1)
															{
															
																if(strlen($qqlist1['qlist2'])>0)
																{
																	echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
																}
															}
															?>
														</ol>
														</div>
														</div>
														</div>
													</td>
												<?php
												}else if($value['qtype']=='9'){
													$list1='';
													$list2='';
													$obj=json_decode($value['question'],true);
													foreach($obj as $rowpost2)
													{
														if(strlen($rowpost2)>0)
														{
															$rowpost = explode("_",$rowpost2);
															$list1.=$rowpost[0]."_";
															$list2.=$rowpost[1]."_";
														}
													}
													$qlist1 = explode("_",$list1);
													$qlist2 = explode("_",$list2);
													
													?>	
													<td class="text-left" width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none">
														<div class="d-flex justify-content-between">
														<div>
														<h5>List1</h5>
														<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist)
															{
															
																if(strlen($qqlist['qlist1'])>0)
																{
																	echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
																}
															}
															?>
														
														
														</ul>
														</div>
														<div>
														<h5>List2</h5>
														<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
														<?php
															foreach($obj as $qqlist1)
															{
															
																if(strlen($qqlist1['qlist2'])>0)
																{
																	echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
																}
															}
															?>
														</ol>
														</div>
														</div>
														</div>
													</td>
													<?php
												}else{
													$question=urldecode($value['question']);
												?>
													<td class="text-left"  width="33%"><div   style="overflow:hidden;max-width:350px;" id="tableborder-none"><?php echo $question; ?></div></td>
												<?php
												}
												
												?>
												<td class="text-left"><?php echo $class; ?></td>
												<td class="text-left"><?php echo rtrim($exam,",");; ?></td>
												<td class="text-left"><?php echo $database->get_name('question_useset','id',$value['usageset'],'usageset'); ?></td>
												<td class="text-left"><?php echo $database->get_name('complexity','id',$value['complexity'],'complexity'); ?></td>
												<td class="text-left"><?php echo $database->get_name('subject','id',$value['subject'],'subject'); ?> - <?php echo rtrim($chapter,','); ?></td>
												
											</tr>
											<?php 
												$i++;
												}
												?>
										</tbody>
									</table>
								
								</div>
								
							</div>
							<div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
									<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
									<?php echo $pagination ;?>
											
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
	</section>
  </div>
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}                             

 if(isset($_REQUEST['getusageData'])){
	
	$post = $session->cleanInput($_REQUEST);
	if($post['questions']==''){ 

	?>
		<script type="text/javascript">
			alert("Please Select Questions");
			jQuery.noConflict();
			$('#exampleModal5').modal('hide');
			setStateGet('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getreport=1&usageset=<?php echo $_REQUEST['usageset']; ?>&questions=<?php echo $_REQUEST['questions']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam'])){ echo '&exam='.$_REQUEST['exam'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['complexity'])){ echo '&complexity='.$_REQUEST['complexity'];}?><?php if(isset($_REQUEST['questiontype'])){ echo '&questiontype='.$_REQUEST['questiontype'];}?><?php if(isset($_REQUEST['fdate'])){ echo '&fdate='.$_REQUEST['fdate'];}?><?php if(isset($_REQUEST['tdate'])){ echo '&tdate='.$_REQUEST['tdate'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];} ?>');

			
		</script>
	<?php 
	}else{ 
			
		?>
				<script type="text/javascript">
				jQuery.noConflict();
					$('#exampleModal5').modal('show');
				</script>
				<?php

		$q=$post['questions'];
		$q1=explode(",",$post['questions']);
		$q2=array_unique($q1);
		$count1=count($q2)-1;
		
		?>
			<div class="row">
				<div class="col-lg-6" >
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Usage Set</label>
						<div class="col-sm-6">
						<select class="form-control" name="usageset1" value=""   id="usageset1" >
							<option value=''>-- Select --</option>
								<?php
								$uageset = $database->query("SELECT * FROM question_useset WHERE estatus='1' "); 
								
								while($datauageset = mysqli_fetch_array($uageset))
								{
									?>
								<option value="<?php echo $datauageset['id'];?>" ><?php echo $datauageset['usageset'];?></option>
								<?php
								}
								?>
								
							</select>
							<span class="error" style="color:red;"><?php if(isset($_SESSION['error']['usageset1'])){ echo $_SESSION['error']['usageset1'];}?></span>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process1.php','validateFormuse=1&questions=<?php echo $_REQUEST['questions']; ?>&usageset=<?php  echo $_REQUEST['usageset']; ?>&usageset1='+$('#usageset1').val()+'<?php if(isset($_REQUEST['class1'])){ echo '&class='.$_REQUEST['class'];}?><?php if(isset($_REQUEST['subject'])){ echo '&subject='.$_REQUEST['subject'];}?><?php if(isset($_REQUEST['chapter'])){ echo '&chapter='.$_REQUEST['chapter'];}?><?php if(isset($_REQUEST['topic'])){ echo '&topic='.$_REQUEST['topic'];}?><?php if(isset($_REQUEST['exam'])){ echo '&exam='.$_REQUEST['exam'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['complexity'])){ echo '&complexity='.$_REQUEST['complexity'];}?><?php if(isset($_REQUEST['questiontype'])){ echo '&questiontype='.$_REQUEST['questiontype'];}?><?php if(isset($_REQUEST['fdate'])){ echo '&fdate='.$_REQUEST['fdate'];}?><?php if(isset($_REQUEST['tdate'])){ echo '&tdate='.$_REQUEST['tdate'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];} ?>')">Submit</a>
				</div>
			</div>
			<?php
			unset($_SESSION['error']);
				
			
		
		}
	}

	if(isset($_REQUEST['validateFormuse'])){
		$_SESSION['error'] = array();
		$post = $session->cleanInput($_REQUEST);

		if(!$post['usageset1'] || strlen(trim($post['usageset1'])) == 0){
		  $_SESSION['error']['usageset1'] = "*Usage Set cannot be empty";
		}
		if(count($_SESSION['error']) > 0){
			
			?>
			<script type="text/javascript">
			
			
			jQuery.noConflict();
			$('#exampleModal5').modal('show');
			setState('viewDetails5','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getusageData=1&usageset=<?php echo $_REQUEST['usageset']; ?>&questions=<?php echo $_REQUEST['questions']; ?>&usageset1=<?php echo $_REQUEST['usageset1']; ?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam'])){ echo '&exam='.$_REQUEST['exam'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['complexity'])){ echo '&complexity='.$_REQUEST['complexity'];}?><?php if(isset($_REQUEST['questiontype'])){ echo '&questiontype='.$_REQUEST['questiontype'];}?><?php if(isset($_REQUEST['fdate'])){ echo '&fdate='.$_REQUEST['fdate'];}?><?php if(isset($_REQUEST['tdate'])){ echo '&tdate='.$_REQUEST['tdate'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];} ?>');
			</script>
		<?php
		}else{

		$q=$post['questions'];
		$q1=explode(",",$post['questions']);
		$q2=array_unique($q1);
		$count1=count($q2)-1;
		
		
		?>
		
		<?php
		for($i=0;$i<count($q2)-1;$i++){
			$sql=$database->query("select * from createquestion where estatus='1' and id='".$q1[$i]."'"); 
			$row=mysqli_fetch_array($sql);
			$result=$database->query("update createquestion set usageset='".$_REQUEST['usageset1']."',eusername='".$_SESSION['username']."',etimestamp='".time()."' where id='".$q1[$i]."'"); 
			$result=$database->query("insert chapter_log set question_id='".$q1[$i]."',oldchapter='".$row['usageset']."',newchapter='".$_REQUEST['usageset1']."',type='usageset',estatus='1',username='".$_SESSION['username']."',timestamp='".time()."'");
			$database->query("insert question_log set question_id='".$q1[$i]."',username='".$_SESSION['username']."',message='2',estatus='1',timestamp='".time()."'");
		}
		
		?>
			<!-- <div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>Question Usageset Updated successfully!</div> -->
			
				<script type="text/javascript">
				jQuery.noConflict();
				$('#exampleModal5').modal('hide');
				alert("Usage Set Updated Successfully!");
				location.reload();
				setStateGet('adminTable','<?php echo SECURE_PATH;?>fitmentreport/process1.php','getreport=1&usageset=<?php echo $_REQUEST['usageset']; ?>&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_REQUEST['class1'])){ echo '&class1='.$_REQUEST['class1'];}?><?php if(isset($_REQUEST['subject1'])){ echo '&subject1='.$_REQUEST['subject1'];}?><?php if(isset($_REQUEST['chapter1'])){ echo '&chapter1='.$_REQUEST['chapter1'];}?><?php if(isset($_REQUEST['topic1'])){ echo '&topic1='.$_REQUEST['topic1'];}?><?php if(isset($_REQUEST['exam'])){ echo '&exam='.$_REQUEST['exam'];}?><?php if(isset($_REQUEST['status'])){ echo '&status='.$_REQUEST['status'];}?><?php if(isset($_REQUEST['complexity'])){ echo '&complexity='.$_REQUEST['complexity'];}?><?php if(isset($_REQUEST['questiontype'])){ echo '&questiontype='.$_REQUEST['questiontype'];}?><?php if(isset($_REQUEST['fdate'])){ echo '&fdate='.$_REQUEST['fdate'];}?><?php if(isset($_REQUEST['tdate'])){ echo '&tdate='.$_REQUEST['tdate'];}?><?php if(isset($_REQUEST['question_id'])){ echo '&question_id='.$_REQUEST['question_id'];} ?>');
			</script>
		<?php
		
		
	}
		

	}
	?>

 <script>

    $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
	</script>
	<script>

    function checkAll()
    {

        if($('#check').val()==0)
        {

            $('#check').val(1);
$('input[type=checkbox]').prop('checked', true);

        }
        else
        {

            $('#check').val(0);
$('input[type=checkbox]').prop('checked', false);

        }


selectCheckBox();
    }   
function selectCheckBox()
{

var questions1='';
    $('.checkBox').each(function(){
    if($(this).is(':checked')) {
	 
if($('#que').val().length>0){

   questions1+=$('#que').val()+$(this).val()+",";
	$('#que').val('');

} else {

questions1+=$(this).val()+",";

}






   
 } else{

	 questions1+=$('#que1').val();
   $('#check').prop('checked', false);
     $('#check').val(0);
     // checkAll();
 }
  });

$('#questions').val(questions1);


}
</script>
<script type="text/javascript"> 
    
	function onlyNumbers(event) {
		var charCode = (event.which) ? event.which : event.keyCode

		if (charCode > 31 && (charCode < 48 || charCode > 57))
			return false;

		return true;
	}
</script>

 <?php echo $session->commonFooterAdminJS(); ?>