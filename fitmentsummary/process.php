<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>

<?php
	//Display bulkreport
if(isset($_REQUEST['tableDisplay'])){
	$sql=$database->query("select * from users where username='".$session->username."'");
	$rowsel=mysqli_fetch_array($sql);
	$chapter=rtrim($rowsel['chapter'],",");
	//echo "select * from chapter where estatus='1' and id in (".$chapter.")  order by id asc limit 0,1";
	if($session->userlevel!=9){
		$selchapter=$database->query("select * from chapter where estatus='1' and id in (".$chapter.")  order by id asc limit 0,1");
	}else{
		$selchapter=$database->query("select * from chapter where estatus='1'   order by id asc limit 0,1");
	}
	$rowchapter=mysqli_fetch_array($selchapter);
	
	if(isset($_POST['comp_tquestion'])){
		if($_POST['comp_tquestion']!=''){
			$_POST['comp_tquestion']=$_POST['comp_tquestion'];
			
		}else{
			$_POST['comp_tquestion']="1";
		}
	}else{
		$_POST['comp_tquestion']="1";
	
	}
	if(isset($_POST['chapter_topic'])){
		if($_POST['chapter_topic']!=''){
			$_POST['chapter_topic']=$_POST['chapter_topic'];
			
		}else{
			$_POST['chapter_topic']="1";
		}
	}else{
		$_POST['chapter_topic']="1";
	
	}
	if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=''){
				$subject= " AND subject IN (".$_REQUEST['subject'].")";
			}else{
				$subject='';
			}
		}else{
			$subject='';
		}
	if(isset($_REQUEST['chapter'])){
			if($_REQUEST['chapter']!=''){
				$chapter= " AND chapter in (".$_REQUEST['chapter'].")";
				$chapter1= " AND id in (".$_REQUEST['chapter'].")";
			}else{
				$chapter='';
				$chapter1='';
			}
		}else{
			$chapter='';
			$chapter1='';
		}
	$cond=$subject.$chapter1;
	$cond1=$subject.$chapter;
	?>
	
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">FITMENT REPORT 	</h6>
						</div>
						<div class="card-body">
							<div class="row">
								
								<div class="col-lg-5">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Chapter / Topic</label>
										<div class="col-sm-8">
											
											<select id="chapter_topic" class="form-control" name="chapter_topic" onchange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentsummary/process.php','tableDisplay=1&chapter_topic='+$('#chapter_topic').val()+'&comp_tquestion='+$('#comp_tquestion').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
												<option value="">Select</option>
												<option value="1" <?php if(isset($_POST['chapter_topic'])){ if($_POST['chapter_topic'] == '1'){ echo ' selected="selected"';}}else { echo ' selected="selected"'; }?>>Chapter</option>
												<option value="2" <?php if(isset($_POST['chapter_topic'])){ if($_POST['chapter_topic'] == '2'){ echo ' selected="selected"';};}?>>Topic</option>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['chapter_topic'])){ echo $_SESSION['error']['chapter_topic'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-5">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Complexity / Question Type</label>
										<div class="col-sm-8">
											
											<select id="comp_tquestion" class="form-control" name="comp_tquestion" onchange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentsummary/process.php','tableDisplay=1&comp_tquestion='+$('#comp_tquestion').val()+'&chapter_topic='+$('#chapter_topic').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')">
												<option value="">Select</option>
												<option value="1" <?php if(isset($_POST['comp_tquestion'])){ if($_POST['comp_tquestion'] == '1'){ echo ' selected="selected"';}}else { echo ' selected="selected"'; }?>>Complexity</option>
												<option value="2" <?php if(isset($_POST['comp_tquestion'])){ if($_POST['comp_tquestion'] == '2'){ echo ' selected="selected"';};}?>>Type Of Question</option>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['comp_tquestion'])){ echo $_SESSION['error']['comp_tquestion'];}?></span>
										</div>
									</div>
								</div>
								<?php 
								$data_sel1 =  $database->query("select * from users where username='".$session->username."'");
								$rowll1 = mysqli_fetch_array($data_sel1); 
								
								?>
								<div class="col-lg-5">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Subject</label>
										<div class="col-sm-8">
											
											<select class="form-control" name="subject" value=""   id="subject" onChange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentsummary/process.php','tableDisplay=1&comp_tquestion='+$('#comp_tquestion').val()+'&chapter_topic='+$('#chapter_topic').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'');">
												<option value=''>-- Select --</option>
												<?php
												if($session->userlevel=='6'){
													$row = $database->query("select * from subject where estatus='1'  and id in (".rtrim($rowll1['subject'],",").") ");
												}else{
													$row = $database->query("select * from subject where estatus='1' ");
												}
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
												<?php
												}
												?>
											</select>
										</div>
									</div>
								</div>
							
								
								<div class="col-lg-5">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Chapter</label>
										<div class="col-sm-8">
											
											<select class="form-control selectpicker1" name="chapter" value=""  multiple data-live-search="true"  id="chapter"  onChange="setState('adminTable','<?php echo SECURE_PATH;?>fitmentsummary/process.php','tableDisplay=1&comp_tquestion='+$('#comp_tquestion').val()+'&chapter_topic='+$('#chapter_topic').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'')" >
														<option value=''>-- Select --</option>
														<?php
														if($session->userlevel=='6'){
															$row = $database->query("select * from chapter where estatus='1' ".$subject."  and id  in (".rtrim($rowll1['chapter'],",").")  ");
														}else{
															$row = $database->query("select * from chapter where estatus='1' ".$subject." ");
														}
														
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], explode(",",$_REQUEST['chapter']))) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
														<?php
														}
														?>
													</select>
										</div>
									</div>
								</div>
									
										
									
							</div>
							<?php if($_POST['chapter_topic']=='1'){ ?>
								<table id="example" class="table table-responsive table-bordered nowrap" style="width:100%">
									<thead>
										<tr>
											<th>Chapter</th>
											<th>View</th>
											<th>Total</th>
											<th>Verified</th>
											<th>Pending</th>
											<th>Verifier Usename </th>
											<th>Reviewed</th>
											<th>Review Pending</th>
											<th>Fitment Status</th>
											
										</tr>
									</thead>
									<tbody>
										<?php
										$i=1;	
										if($session->userlevel==6){
											$sell=$database->query("select * from chapter where estatus='1' and id in (".rtrim($rowsel['chapter'],",").") ".$cond." order by id ASC");
										}else{
											$sell=$database->query("select * from chapter where estatus='1' ".$cond." order by id ASC");
										}
										
										while($rowl=mysqli_fetch_array($sell)){
											/*if($_POST['comp_tquestion']=='1'){
												$con=" AND complexity!=0";
											}else if($_POST['comp_tquestion']=='2'){
												$con=" AND inputquestion!=''";
											}else{
												$con="";
											}*/
											$selcht=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$rowl['id'].",chapter)>0   ");
											$rowcht=mysqli_fetch_array($selcht);
											
											$rowchtvv=0;
											$rowchtpp=0;

											$selchtv=$database->query("select count(id) as cnt,vstatus1 from createquestion where estatus='1'  and find_in_set(".$rowl['id'].",chapter)>0   GROUP BY vstatus1 ");
											while($rowchtv=mysqli_fetch_array($selchtv)){
											 if($rowchtv['vstatus1']=='1'){
												   $rowchtvv=$rowchtvv+$rowchtv['cnt'];
											   }
												if($rowchtv['vstatus1']=='0'){
												   $rowchtpp=$rowchtpp+$rowchtv['cnt'];
											   }
											}
											
											$rowchtvvr=0;
											$rowchtppr=0;
											$selchtv1=$database->query("select count(id) as cnt,vstatus1 from createquestion where estatus='1'  and vstatus1=1   and find_in_set(".$rowl['id'].",chapter)>0  GROUP BY review_status ");
											while($rowchtv1=mysqli_fetch_array($selchtv1)){
											 if($rowchtv1['review_status']=='1'){
												   $rowchtvvr=$rowchtvvr+$rowchtv1['cnt'];
											   }
												if($rowchtv['review_status']=='0'){
												   $rowchtppr=$rowchtppr+$rowchtv1['cnt'];
											   }
											}

											
											
											$selchtf=$database->query("select id from fitment_data  where estatus='1' and type='".$_POST['chapter_topic']."' and comp_tquestion='".$_POST['comp_tquestion']."' and chapter='".$rowl['id']."' ");
											$rowchtprf=mysqli_num_rows($selchtf);
											if($rowchtprf> 0){
												$status="Submitted";
											}else{
												$status="Pending";
											}
											$user='';
											$selchtvuser=$database->query("select id,vusername1 from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['id'].",chapter)>0      group by vusername1 ");
											while($rowchtvuser=mysqli_fetch_array($selchtvuser)){
												if($rowchtvuser['vusername1']!=''){
													$user.=$rowchtvuser['vusername1'].",";
												}
											}
											

										?>
											<tr>
												<td><?php echo $rowl['chapter']; ?></td>
												<?php 
												if($_POST['comp_tquestion']=='1'){ ?>
													<td><a data-toggle="modal" data-target="#exampleModal" class="btn "  onClick="setStateEnc2('viewDetails','<?php echo SECURE_PATH;?>fitmentsummary/process1.php','viewDetails=1&chapter=<?php echo $rowl['id'];?>&chapter_topic=<?php echo $_POST['chapter_topic'];?>&comp_tquestion=<?php echo $_POST['comp_tquestion'];?>')"><i class="fa fa-eye" style="color:blue;" ></i></td>
												<?php } else if($_POST['comp_tquestion']=='2'){ ?>
														<td><a data-toggle="modal" data-target="#exampleModal" class="btn "  onClick="setStateEnc2('viewDetails','<?php echo SECURE_PATH;?>fitmentsummary/process_cqtype.php','viewDetails=1&chapter=<?php echo $rowl['id'];?>&chapter_topic=<?php echo $_POST['chapter_topic'];?>&comp_tquestion=<?php echo $_POST['comp_tquestion'];?>')"><i class="fa fa-eye" style="color:blue;" ></i></td>
												<?php }
												?>
												<td><?php echo $rowcht['cnt']; ?> </td>
												<td><?php  if($rowchtvv!=''){ echo $rowchtvv; } else{ echo '0';  }  ?> </td>
												<td><?php  if($rowchtpp!=''){ echo $rowchtpp; } else{ echo '0'; }  ?> </td>
												<td><?php echo rtrim($user,","); ?></td>
												<td><?php  if($rowchtvvr!=''){ echo $rowchtvvr; } else{ echo '0'; }  ?> </td>
												<td><?php  if($rowchtppr!=''){ echo $rowchtppr; } else{ echo '0'; }  ?> </td>
												<td><?php echo $status; ?> </td>
											 
											</tr>
										<?php }  ?>
										
									</tbody>
								</table>
							<?php 
							}else if($_POST['chapter_topic']=='2'){
										
								?>
								<table id="example1" class="table table-responsive table-bordered nowrap" style="width:100%">
									<thead>
										<tr>

											<th>Chapter</th>
											<th>Topic</th>
											<th>View</th>
											<th>Total</th>
											<th>Verified</th>
											<th>Pending</th>
											<th>Verifier Usename </th>
											<th>Reviewed</th>
											<th>Review Pending</th>
											<th>Fitment Status</th>
											
										</tr>
									</thead>
									<tbody>
										<?php
										if($session->userlevel==6){
											$sell=$database->query("select * from topic where estatus='1' and chapter in (".rtrim($rowsel['chapter'],",").") ".$cond1." order by id ASC");
										}else{
											$sell=$database->query("select * from topic where estatus='1'  ".$cond1." order by id ASC");
										}
										while($rowl=mysqli_fetch_array($sell)){
											/*if($_POST['comp_tquestion']=='1'){
												$con=" AND complexity!=0";
											}else if($_POST['comp_tquestion']=='2'){
												$con=" AND inputquestion!=''";
											}else{
												$con="";
											}*/
											$selcht=$database->query("select count(id) as cnt from createquestion where estatus='1' and find_in_set(".$rowl['chapter'].",chapter)>0 and find_in_set(".$rowl['id'].",topic)>0  ");
											$rowcht=mysqli_fetch_array($selcht);
											
											$rowchtvv=0;
											$rowchtpp=0;
											$selchtv=$database->query("select count(id) as cnt,vstatus1 from createquestion where estatus='1'  and find_in_set(".$rowl['chapter'].",chapter)>0  and find_in_set(".$rowl['id'].",topic)>0   GROUP BY vstatus1 ");
											while($rowchtv=mysqli_fetch_array($selchtv)){
											 if($rowchtv['vstatus1']=='1'){
												   $rowchtvv=$rowchtvv+$rowchtv['cnt'];
											   }
												if($rowchtv['vstatus1']=='0'){
												   $rowchtpp=$rowchtpp+$rowchtv['cnt'];
											   }
											}
											$rowchtvvr=0;
											$rowchtppr=0;
											$selchtv1=$database->query("select count(id) as cnt,vstatus1 from createquestion where estatus='1'  and vstatus1=1   and find_in_set(".$rowl['chapter'].",chapter)>0 and find_in_set(".$rowl['id'].",topic)>0   GROUP BY review_status ");
											while($rowchtv1=mysqli_fetch_array($selchtv1)){
											 if($rowchtv1['review_status']=='1'){
												   $rowchtvvr=$rowchtvvr+$rowchtv1['cnt'];
											   }
												if($rowchtv['review_status']=='0'){
												   $rowchtppr=$rowchtppr+$rowchtv1['cnt'];
											   }
											}

											
											$selchtf=$database->query("select id from fitment_data  where estatus='1' and type='".$_POST['chapter_topic']."' and comp_tquestion='".$_POST['comp_tquestion']."' and chapter='".$rowl['chapter']."'  and topic='".$rowl['id']."'  ");
											$rowchtprf=mysqli_num_rows($selchtf);
											if($rowchtprf> 0){
												$status="Submitted";
											}else{
												$status="Pending";
											}
											$user='';
											$selchtvuser=$database->query("select id,vusername1 from createquestion where estatus='1' and vstatus1='1' and find_in_set(".$rowl['chapter'].",chapter)>0 and find_in_set(".$rowl['id'].",topic)>0     group by vusername1 ");
											while($rowchtvuser=mysqli_fetch_array($selchtvuser)){
												if($rowchtvuser['vusername1']!=''){
													$user.=$rowchtvuser['vusername1'].",";
												}
											}
											
										?>
											<tr>
												
												<td><?php echo $database->get_name('chapter','id',$rowl['chapter'],'chapter'); ?></td>
												<td><?php echo $rowl['topic']; ?></td>
												
												<?php 
												if($_POST['comp_tquestion']=='1'){ ?>
													<td><a data-toggle="modal" data-target="#exampleModal1" class="btn "  onClick="setStateEnc2('viewDetails1','<?php echo SECURE_PATH;?>fitmentsummary/process_topic.php','viewDetails1=1&chapter=<?php echo $rowl['chapter'];?>&topic=<?php echo $rowl['id'];?>&chapter_topic=<?php echo $_POST['chapter_topic'];?>&comp_tquestion=<?php echo $_POST['comp_tquestion'];?>')"><i class="fa fa-eye" style="color:blue;" ></i></td>
												<?php } else if($_POST['comp_tquestion']=='2'){ ?>
														<td><a data-toggle="modal" data-target="#exampleModal" class="btn "  onClick="setStateEnc2('viewDetails','<?php echo SECURE_PATH;?>fitmentsummary/process_cqtype1.php','viewDetails=1&chapter=<?php echo $rowl['chapter'];?>&topic=<?php echo $rowl['id'];?>&chapter_topic=<?php echo $_POST['chapter_topic'];?>&comp_tquestion=<?php echo $_POST['comp_tquestion'];?>')"><i class="fa fa-eye" style="color:blue;" ></i></td>
												<?php }
												?>
												<td><?php echo $rowcht['cnt']; ?> </td>
												<td><?php  if($rowchtvv!=''){ echo $rowchtvv; } else{ echo '0';  }  ?> </td>
												<td><?php  if($rowchtpp!=''){ echo $rowchtpp; } else{ echo '0'; }  ?> </td>
												<td><?php echo rtrim($user,","); ?></td>
												<td><?php  if($rowchtvvr!=''){ echo $rowchtvvr; } else{ echo '0';  }  ?> </td>
												<td><?php  if($rowchtppr!=''){ echo $rowchtppr; } else{ echo '0'; }  ?> </td>
												<td><?php echo $status; ?> </td>
												
											 
											</tr>
										<?php }  ?>
										
									</tbody>
								</table>
							<?php } ?>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<script>
	$('#example').DataTable( {
        responsive: true,
		"pageLength": 50,
    "order": [[ 1, "desc" ]]
    } );
	$('#example1').DataTable( {
        responsive: true,
		"pageLength": 50,
    "order": [[ 1, "desc" ]]
    } );
		
		</script>
	<?php
	}
	
	
	?>
	<?php
	if(isset($_REQUEST['getchapter'])){
		$sql=$database->query("select * from users where username='".$session->username."'");
		$rowl=mysqli_fetch_array($sql);
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=''){
				$subject= " AND subject='".$_REQUEST['subject']."'";
			}else{
				$subject='';
			}
		}else{
			$subject='';
		}
		?>
			<select class="form-control selectpicker2 chapter" name="chapter" value=""  multiple data-live-search="true"  id="chapter"  onChange="etState('adminTable','<?php echo SECURE_PATH;?>fitmentsummary/process.php','tableDisplay=1&comp_tquestion='+$('#comp_tquestion').val()+'&chapter_topic='+$('#chapter_topic').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'');" >
				<option value=''>-- Select --</option>
				<?php
				if($session->userlevel=='6'){
					$row = $database->query("select * from chapter where estatus='1'  and subject in (".rtrim($row['subject'],",").") and id in (".rtrim($rowl['chapter'],",").") ".$subject."");
				}else{
					echo "select * from chapter where estatus='1'".$subject."";
					$row = $database->query("select * from chapter where estatus='1'".$subject."  ");
				}
				
				while($data = mysqli_fetch_array($row))
				{
					?>
				<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
				<?php
				}
				?>
			</select>
		<?php

	}
	?>
		<script>
	$(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	</script>