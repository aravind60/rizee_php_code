<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Swamy">
    <meta name="theme-color" content="#0c8bf1" />
    <meta name="keywords" content="">
    <link href="vendor/images/baseline_local_activity_black_36dp.png" sizes="16X16" rel="shortcut icon"
        type="image/png" />

    <title>STMS Dashboard</title>

    <!-- Bootstrap core CSS-->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="vendor/app/css/style.css" rel="stylesheet">

    <!-- Scrollbar Custom CSS -->
    <link rel="stylesheet" href="vendor/jquery-mousewheel-scrollbar/css/jquery.mCustomScrollbar.min.css">


</head>

<body id="page-top">

    <div class="preloader">
        <div class="loader"></div>
    </div>

    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="dashboard.html">
                <h2 class="m-0"><i class="baseline-local_activity icon-image-preview icon-white icon-36"></i> STMS</h2>
            </a>
        </div>
        <ul class="side-menu accordion" id="accordionSidebar">
            <li class="nav-item">
                <a class="nav-link" href="dashboard.html"><i class="material-icons icon">widgets</i> Dashboard</a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#page-projects" data-toggle="collapse" aria-expanded="false"
                    aria-controls="collapseTwo">
                    <i class="material-icons icon">styles</i> Projects
                </a>
                <ul class="collapse sub-menu" id="page-projects" aria-labelledby="headingTwo"
                    data-parent="#accordionSidebar">
                    <li class="subnav-item">
                        <a class="subnav-link" href="projects.html">View Project</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="add-project.html">Add Project</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item active">
                <a class="nav-link collapse" href="#page-tickets" data-toggle="collapse" aria-expanded="true"
                    aria-controls="collapseUtilities">
                    <i class="material-icons icon">event_note</i> Tickets
                </a>
                <ul class="collapse sub-menu show" id="page-tickets" aria-labelledby="headingUtilities"
                    data-parent="#accordionSidebar">
                    <li class="subnav-item">
                        <a class="subnav-link" href="add-ticket.html">Add Tickets</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="#">Assigned To Me</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="#">Raised By Me</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="#">Unsolved Tickets</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="#">Solved Tickets</a>
                    </li>
                    <li class="subnav-item active">
                        <a class="subnav-link" href="view-tickets.html">View All Tickets</a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="users.html"><i class="material-icons icon">people</i> Users</a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#page-reports" data-toggle="collapse" aria-expanded="false"
                    aria-controls="collapsePages">
                    <i class="material-icons icon">assignment</i> Reports
                </a>
                <ul class="collapse sub-menu" id="page-reports" aria-labelledby="headingPages"
                    data-parent="#accordionSidebar">
                    <li class="subnav-item">
                        <a class="subnav-link" href="project-reports.html">Project Level Reports</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" href="user-reports.html">User Level Reports</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>

    <div class="menu-overlay"></div>

    <main class="main-wrapper">
        <header class="header-area">
            <nav class="navbar navbar-expand navbar-light bg-admin">
                <div class="container">
                    <button class="btn btn-light btn-circle text-theme order-1 order-sm-0" id="sidebarCollapse">
                        <i class="material-icons text-theme md-18">more_vert</i>
                    </button>
                    <!-- Navbar Search -->
                    <form class="form-inline mr-auto mr-0 ml-md-3">
                        <div class="has-search">
                            <span class="material-icons md-24 form-control-feedback">search</span>
                            <input type="text" class="form-control" placeholder="Search...">
                        </div>
                    </form>

                    <!-- Navbar -->
                    <ul class="navbar-nav ml-auto ml-md-0">
                        <li class="nav-item">
                            <a class="nav-link invite text-theme" href="add-ticket.html">
                                <span class="pt-2 d-none d-lg-inline-block"><i class="material-icons md-14">add</i> Add
                                    Ticket</span>
                                <span
                                    class="mt-1 d-xl-none d-lg-none text-theme btn btn-light btn-circle material-icons md-18">add</span>
                            </a>
                        </li>
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">notifications</i>
                                <span class="badge badge-danger">9+</span>
                            </a>
                            <div class="dropdown-menu notification dropdown-menu-right"
                                aria-labelledby="alertsDropdown">
                                <ul>
                                    <li>
                                        <div class="drop-title">Your Notifications</div>
                                    </li>
                                    <li>
                                        <div class="notification-center">
                                            <a href="#">
                                                <div class="btn btn-danger btn-circle shadow"><i
                                                        class="material-icons">notifications_none</i></div>
                                                <div class="notification-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new
                                                        admin!</span> <span class="time">9:30 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-success btn-circle"><i
                                                        class="material-icons">notifications_none</i></div>
                                                <div class="notification-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that
                                                        you have event</span> <span class="time">9:10 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-info btn-circle"><i
                                                        class="material-icons">notifications_none</i></div>
                                                <div class="notification-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this
                                                        template as you want</span> <span class="time">9:08 AM</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="btn btn-primary btn-circle"><i
                                                        class="material-icons">notifications_none</i></div>
                                                <div class="notification-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my
                                                        admin!</span>
                                                    <span class="time">9:02 AM</span>
                                                </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="pt-2 nav-link text-center" href="javascript:void(0);"> All
                                            notifications</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle pt-1" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="vendor/images/user-profile.png" width="35" alt="profile-user"
                                    class="rounded-circle">
                                <div class="d-none d-xl-inline-block">Rajan</div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right logout" aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="#"><i class="material-icons">settings</i> Settings</a>
                                <a class="dropdown-item" href="#"><i class="material-icons">style</i> Activity Log</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal"><i
                                        class="material-icons">exit_to_app</i>
                                    Logout</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="content-wrapper">
            <!-- Breadcrumbs-->
            <section class="breadcrumbs-area2 my-3">
                <div class="container">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title">
                            <h1 class="text-uppercase">Full Ticket <small>Lets get a quick Overview</small></h1>
                        </div>
                    </div>
                    <hr>
                </div>
            </section>
            <!-- End Breadcrumbs-->

            <!-- Content Area-->
            <section class="content-area">
                <div class="container">
                    <div class="row full-view-tickets">
                        <div class="col-md-12">
                            <div class="card border-0 shadow mb-4">
                                <div class="card-header py-3">
                                    <h6 class="m-0 font-weight-bold text-primary">Ticket Details</h6>
                                </div>
                                <div class="card-body">
                                    <div class="card ticket-view-list my-3">
                                        <div class="card-header">
                                            <h6 class="font-weight-bold card-title mb-0 text-uppercase">Lorem
                                                ipsum dolor sit amet.</h6>
                                        </div>
                                        <div class="card-body border-bottom">
                                            <div class="row">
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Assigned To</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0">Laxmi Narasimha Swamy</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Task Type</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0">Task Type-1.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Project</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0">Lorem ipsum dolor sit amet.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Reffered To</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0">Rajeev</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Due Date</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0">29-07-2019</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-6 col-md-12 col-sm-12">
                                                    <div class="row">
                                                        <div class="col-lg-4 col-md-4 col-sm-12">
                                                            <p class="mb-0 font-weight-bold">Priority</p>
                                                        </div>
                                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                                            <p class="mb-0 badge badge-danger text-white">High Priority
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-12 col-md-12 col-sm-12 my-2">
                                                    <h6 class="font-weight-bold">Description</h6>
                                                    <div class="description-text">
                                                        <p>Lorem ipsum is placeholder text commonly
                                                            used in the graphic, print, and publishing industries
                                                            for previewing layouts and visual mockups.</p>
                                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                            sed do eiusmod tempor incididunt ut labore et dolore
                                                            magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                            exercitation ullamco laboris nisi ut aliquip ex ea
                                                            commodo consequat. Duis aute irure dolor in
                                                            reprehenderit in voluptate velit esse cillum dolore eu
                                                            fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                                                            non proident, sunt in culpa qui officia deserunt mollit
                                                            anim id est laborum.</p>
                                                    </div>
                                                </div>
                                                <div class="single-element col-lg-12 col-md-12 col-sm-12 my-2">
                                                    <h6 class="font-weight-bold">Upload images</h6>
                                                    <div class="row align-items-center">
                                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                                            <div class="img-block mb-2">
                                                                <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22348%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20348%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c41bd221f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A17pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c41bd221f%22%3E%3Crect%20width%3D%22348%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22116.71875%22%20y%3D%22120.3%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                                    class="img-fluid rounded shadow" alt="img">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                                            <div class="img-block mb-2">
                                                                <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22348%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20348%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c41bd221f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A17pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c41bd221f%22%3E%3Crect%20width%3D%22348%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22116.71875%22%20y%3D%22120.3%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                                    class="img-fluid rounded shadow" alt="img">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                                            <div class="img-block mb-2">
                                                                <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22348%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20348%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c41bd221f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A17pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c41bd221f%22%3E%3Crect%20width%3D%22348%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22116.71875%22%20y%3D%22120.3%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                                    class="img-fluid rounded shadow" alt="img">
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-6 col-sm-12">
                                                            <div class="img-block mb-2">
                                                                <img src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22348%22%20height%3D%22225%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20348%20225%22%20preserveAspectRatio%3D%22none%22%3E%3Cdefs%3E%3Cstyle%20type%3D%22text%2Fcss%22%3E%23holder_16c41bd221f%20text%20%7B%20fill%3A%23eceeef%3Bfont-weight%3Abold%3Bfont-family%3AArial%2C%20Helvetica%2C%20Open%20Sans%2C%20sans-serif%2C%20monospace%3Bfont-size%3A17pt%20%7D%20%3C%2Fstyle%3E%3C%2Fdefs%3E%3Cg%20id%3D%22holder_16c41bd221f%22%3E%3Crect%20width%3D%22348%22%20height%3D%22225%22%20fill%3D%22%2355595c%22%3E%3C%2Frect%3E%3Cg%3E%3Ctext%20x%3D%22116.71875%22%20y%3D%22120.3%22%3EThumbnail%3C%2Ftext%3E%3C%2Fg%3E%3C%2Fg%3E%3C%2Fsvg%3E"
                                                                    class="img-fluid rounded shadow" alt="img">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="conversation">
                                            <div class="conversation-heading py-3 px-4 border-bottom">
                                                <h6 class="mb-0 font-weight-bold text-uppercase">User Conversations</h6>
                                            </div>
                                            <div class="conversation-body py-2 px-4">
                                                <!-- empty block -->
                                                <div class="empty d-flex flex-column align-items-center">
                                                    <div class="pt-5 mt-2 text-center">
                                                        <i class="material-icons md-64 text-muted-30"> question_answer </i>
                                                        <h5 class="text-muted-30">No Conversation</h5>
                                                    </div>
                                                </div>
                                                <ul class="list-unstyled">
                                                    <li class="sent">
                                                        <div class="msge-box">
                                                            <div class="personInfo mb-3">
                                                                <h6 class="userName text-primary font-weight-bold mb-0">
                                                                    Rajeev</h6>
                                                                <small class="designation text-muted">CTO
                                                                    Entrolabs</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                                magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                                exercitation ullamco laboris nisi ut aliquip ex ea
                                                                commodo consequat.</p>
                                                            <p class="mt-2 time">04 Jul 2019, 9:29 am </p>
                                                        </div>
                                                    </li>
                                                    <li class="replies">
                                                        <div class="msge-box">
                                                            <div class="personInfo mb-3">
                                                                <h6 class="userName text-primary font-weight-bold mb-0">
                                                                    Sridevi</h6>
                                                                <small class="designation text-muted">Sr. Php
                                                                    Developer</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                                magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                                exercitation ullamco laboris nisi ut aliquip ex ea
                                                                commodo consequat.</p>
                                                            <p class="mt-2 time">04 Jul 2019, 9:29 am</p>
                                                        </div>
                                                    </li>
                                                    <li class="sent">
                                                        <div class="msge-box">
                                                            <div class="personInfo mb-3">
                                                                <h6 class="userName text-primary font-weight-bold mb-0">
                                                                    Sireesha</h6>
                                                                <small class="designation text-muted">Sr. Php
                                                                    Developer</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                                magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                                exercitation ullamco laboris nisi ut aliquip ex ea
                                                                commodo consequat.</p>
                                                            <p class="mt-2 time">04 Jul 2019, 9:29 am </p>
                                                        </div>
                                                    </li>
                                                    <li class="replies">
                                                        <div class="msge-box">
                                                            <div class="personInfo mb-3">
                                                                <h6 class="userName text-primary font-weight-bold mb-0">
                                                                    Swamy</h6>
                                                                <small class="designation text-muted">Sr. Web &amp; UI
                                                                    Developer</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                                magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                                exercitation ullamco laboris nisi ut aliquip ex ea
                                                                commodo consequat.</p>
                                                            <p class="mt-2 time">04 Jul 2019, 9:29 am</p>
                                                        </div>
                                                    </li>
                                                    <li class="sent">
                                                        <div class="msge-box">
                                                            <div class="personInfo mb-3">
                                                                <h6 class="userName text-primary font-weight-bold mb-0">
                                                                    Sridevi</h6>
                                                                <small class="designation text-muted">Sr. Php
                                                                    Developer</small>
                                                            </div>
                                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                                                                sed do eiusmod tempor incididunt ut labore et dolore
                                                                magna aliqua. Ut enim ad minim veniam, quis nostrud
                                                                exercitation ullamco laboris nisi ut aliquip ex ea
                                                                commodo consequat.</p>
                                                            <p class="mt-2 time">04 Jul 2019, 9:29 am </p>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="conversation-footer pt-3 px-4 border-top">
                                            <form>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-12 form-group">
                                                        <select name="Table_status"
                                                            class="custom-select custom-select-sm form-control form-control-sm">
                                                            <option value="">Status1</option>
                                                            <option value="">Status2</option>
                                                            <option value="">Status3</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-8 col-sm-12 form-group">
                                                        <div class="input-group">
                                                            <input type="text" class="form-control"
                                                                placeholder="Type here..." required="">
                                                            <div class="input-group-append">
                                                                <button type="submit" class="btn btn-primary">
                                                                    <i class="fa fa-paper-plane" aria-hidden="true"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- End Cards-->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="material-icons">navigation</i>
            </a>
        </div>
        <footer class="footer border-top py-3 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copy-rights">
                            <span class="text-dark">&copy; 2019 <a href="#" target="_blank">Admin</a> All rights
                                reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </main>

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="vendor/app/js/admin.js"></script>

    <!-- jQuery Custom Scroller CDN -->
    <script src="vendor/jquery-mousewheel-scrollbar/js/jquery.mCustomScrollbar.min.js"></script>
</body>

</html>