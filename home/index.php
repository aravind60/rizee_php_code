<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	header('Location: '.SECURE_PATH.'admin/');
    }
    else
    {
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="<?php echo SECURE_PATH;?>vendor/images/favicon.ico">

        
        <?php echo $session->gdc_title();?>
        <?php echo $session->commonJS();?>
        <?php echo $session->commonAdminCSS();?>
    </head>

    <body id="page-top" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()">

    <div class="preloader">
        <div class="loader"></div>
    </div>

    <nav id="sidebar">
        <div class="sidebar-header shadow-sm">
            <a href="<?php echo SECURE_PATH;?>home/">
                <!-- <img src="<?php echo SECURE_PATH;?>vendor/images/Logo Exp_DS-02.png" class="img-fluid" alt="Image" /> -->
                <svg xmlns="http://www.w3.org/2000/svg" width="159.501" height="37.139"  viewBox="0 0 159.501 37.139"><g transform="translate(-384.741 -406.553)"><g transform="translate(445.845 406.553)"><path fill="#2a346c" d="M841.627,431.867l-4.882-7.052h-5.388v7.052H825.5V406.553h10.958a14.07,14.07,0,0,1,5.84,1.121,8.683,8.683,0,0,1,3.815,3.182,8.772,8.772,0,0,1,1.338,4.882,8.247,8.247,0,0,1-5.208,7.992l5.678,8.137Zm-1.483-19.4a6.155,6.155,0,0,0-4.014-1.139h-4.774v8.824h4.774a6.085,6.085,0,0,0,4.014-1.157,4.563,4.563,0,0,0,0-6.528Z" transform="translate(-825.498 -406.553)"></path><path fill="#2a346c" d="M1010.373,411.941a3.063,3.063,0,0,1,0-4.484,3.58,3.58,0,0,1,2.531-.9,3.668,3.668,0,0,1,2.531.868,2.783,2.783,0,0,1,.976,2.17,3.071,3.071,0,0,1-.976,2.332,3.537,3.537,0,0,1-2.531.922A3.577,3.577,0,0,1,1010.373,411.941Zm-.289,3.048h5.641v16.878h-5.641Z" transform="translate(-983.902 -406.553)"></path><path fill="#2a346c" d="M1100.574,463.929v4.34h-17.358v-3.4l9.909-11.717h-9.656v-4.34h16.78v3.4l-9.909,11.717Z" transform="translate(-1047.488 -442.954)"></path><path fill="#2a346c" d="M1242.651,458.334h-14.719a4.58,4.58,0,0,0,1.881,2.857,6.225,6.225,0,0,0,3.689,1.049,7.459,7.459,0,0,0,2.694-.452,6.529,6.529,0,0,0,2.188-1.428l3,3.255q-2.749,3.146-8.028,3.146a12.694,12.694,0,0,1-5.822-1.284,9.406,9.406,0,0,1-3.906-3.562,9.809,9.809,0,0,1-1.374-5.171,9.943,9.943,0,0,1,1.356-5.153,9.5,9.5,0,0,1,3.725-3.58,11.533,11.533,0,0,1,10.469-.054,8.983,8.983,0,0,1,3.634,3.526,10.518,10.518,0,0,1,1.32,5.334Q1242.76,456.924,1242.651,458.334Zm-13.2-6.256a4.618,4.618,0,0,0-1.591,2.965h9.583a4.667,4.667,0,0,0-1.591-2.947,4.758,4.758,0,0,0-3.182-1.1A4.858,4.858,0,0,0,1229.451,452.078Z" transform="translate(-1167.251 -441.157)"></path><path fill="#2a346c" d="M1407.25,458.334h-14.719a4.58,4.58,0,0,0,1.881,2.857,6.225,6.225,0,0,0,3.689,1.049,7.459,7.459,0,0,0,2.694-.452,6.53,6.53,0,0,0,2.188-1.428l3,3.255q-2.749,3.146-8.028,3.146a12.693,12.693,0,0,1-5.822-1.284,9.4,9.4,0,0,1-3.906-3.562,9.809,9.809,0,0,1-1.374-5.171,9.943,9.943,0,0,1,1.356-5.153,9.5,9.5,0,0,1,3.725-3.58,11.533,11.533,0,0,1,10.469-.054,8.983,8.983,0,0,1,3.634,3.526,10.518,10.518,0,0,1,1.32,5.334Q1407.359,456.924,1407.25,458.334Zm-13.2-6.256a4.618,4.618,0,0,0-1.591,2.965h9.583a4.667,4.667,0,0,0-1.591-2.947,4.759,4.759,0,0,0-3.182-1.1A4.858,4.858,0,0,0,1394.051,452.078Z" transform="translate(-1309.031 -441.157)"></path></g><g transform="translate(445.797 439.343)"><path fill="#2a346c" d="M826.554,644.174h-1.4v-.664h3.588v.664h-1.4v3.564h-.785Z" transform="translate(-825.153 -643.45)"></path><path fill="#2a346c" d="M874.312,643.51v4.228h-.785v-1.812H871.34v1.812h-.785V643.51h.785v1.746h2.187V643.51Z" transform="translate(-864.261 -643.45)"></path><path fill="#2a346c" d="M924.272,647.08v.658H921.1V643.51h3.087v.659h-2.3v1.1h2.042v.646h-2.042v1.166Z" transform="translate(-907.798 -643.45)"></path><path fill="#2a346c" d="M995.518,643.691a1.394,1.394,0,0,1,.625.52,1.587,1.587,0,0,1,0,1.6,1.386,1.386,0,0,1-.625.522,2.338,2.338,0,0,1-.957.181h-.955v1.22h-.785V643.51h1.74A2.343,2.343,0,0,1,995.518,643.691Zm-.214,1.945a.861.861,0,0,0,0-1.245,1.206,1.206,0,0,0-.779-.218h-.918v1.679h.918A1.208,1.208,0,0,0,995.3,645.636Z" transform="translate(-969.577 -643.45)"></path><path fill="#2a346c" d="M1042.442,647.08v.658h-3.172V643.51h3.087v.659h-2.3v1.1h2.042v.646h-2.042v1.166Z" transform="translate(-1009.586 -643.45)"></path><path fill="#2a346c" d="M1086.481,647.738l-.864-1.238q-.055.006-.163.006h-.954v1.232h-.785V643.51h1.74a2.345,2.345,0,0,1,.957.181,1.393,1.393,0,0,1,.625.52,1.452,1.452,0,0,1,.218.8,1.434,1.434,0,0,1-.233.822,1.386,1.386,0,0,1-.667.513l.973,1.389Zm-.284-3.346a1.206,1.206,0,0,0-.779-.218h-.918v1.686h.918a1.2,1.2,0,0,0,.779-.221.767.767,0,0,0,.266-.625A.758.758,0,0,0,1086.2,644.392Z" transform="translate(-1047.869 -643.45)"></path><path fill="#2a346c" d="M1131.6,644.169v1.286h2.042v.665H1131.6v1.619h-.785V643.51h3.087v.659Z" transform="translate(-1088.442 -643.45)"></path><path fill="#2a346c" d="M1176.994,647.08v.658h-3.172V643.51h3.087v.659h-2.3v1.1h2.042v.646h-2.042v1.166Z" transform="translate(-1125.485 -643.45)"></path><path fill="#2a346c" d="M1217.059,647.142a2.071,2.071,0,0,1-.809-.776,2.266,2.266,0,0,1,0-2.235,2.092,2.092,0,0,1,.812-.776,2.381,2.381,0,0,1,1.157-.281,2.418,2.418,0,0,1,.949.181,1.94,1.94,0,0,1,.725.526l-.507.477a1.481,1.481,0,0,0-1.13-.5,1.584,1.584,0,0,0-.779.19,1.372,1.372,0,0,0-.538.528,1.62,1.62,0,0,0,0,1.535,1.371,1.371,0,0,0,.538.528,1.584,1.584,0,0,0,.779.19,1.472,1.472,0,0,0,1.13-.5l.507.483a1.927,1.927,0,0,1-.728.525,2.432,2.432,0,0,1-.951.181A2.382,2.382,0,0,1,1217.059,647.142Z" transform="translate(-1161.778 -643.074)"></path><path fill="#2a346c" d="M1262.2,644.174h-1.4v-.664h3.588v.664h-1.4v3.564h-.785Z" transform="translate(-1200.4 -643.45)"></path><path fill="#2a346c" d="M1334.483,645.2h.743v1.685a2.362,2.362,0,0,1-.761.4,2.927,2.927,0,0,1-.894.139,2.411,2.411,0,0,1-1.166-.281,2.085,2.085,0,0,1-.815-.776,2.257,2.257,0,0,1,0-2.235,2.08,2.08,0,0,1,.818-.776,2.441,2.441,0,0,1,1.175-.281,2.568,2.568,0,0,1,.967.175,1.9,1.9,0,0,1,.731.513l-.5.483a1.589,1.589,0,0,0-1.166-.483,1.647,1.647,0,0,0-.794.187,1.362,1.362,0,0,0-.543.525,1.511,1.511,0,0,0-.2.773,1.487,1.487,0,0,0,.2.761,1.4,1.4,0,0,0,.543.532,1.594,1.594,0,0,0,.788.193,1.7,1.7,0,0,0,.87-.218Z" transform="translate(-1261.126 -643.074)"></path><path fill="#2a346c" d="M1382.59,647.306a1.915,1.915,0,0,1-.489-1.411V643.51h.785v2.356q0,1.245,1.075,1.245t1.069-1.245V643.51h.773V645.9a1.923,1.923,0,0,1-.486,1.411,2.134,2.134,0,0,1-2.727,0Z" transform="translate(-1304.889 -643.45)"></path><path fill="#2a346c" d="M1431.991,643.51h.785v4.228h-.785Z" transform="translate(-1347.863 -643.45)"></path><path fill="#2a346c" d="M1461.141,643.51h1.848a2.652,2.652,0,0,1,1.2.263,1.946,1.946,0,0,1,.816.743,2.265,2.265,0,0,1,0,2.217,1.945,1.945,0,0,1-.816.743,2.652,2.652,0,0,1-1.2.263h-1.848Zm1.812,3.564a1.8,1.8,0,0,0,.818-.178,1.276,1.276,0,0,0,.544-.508,1.63,1.63,0,0,0,0-1.528,1.277,1.277,0,0,0-.544-.508,1.794,1.794,0,0,0-.818-.178h-1.027v2.9Z" transform="translate(-1372.971 -643.45)"></path><path fill="#2a346c" d="M1515.553,647.08v.658h-3.172V643.51h3.087v.659h-2.3v1.1h2.042v.646h-2.042v1.166Z" transform="translate(-1417.109 -643.45)"></path></g><g transform="translate(384.741 406.948)"><path fill="#2a346c" d="M384.741,446.083,400.6,418.829h-6.84l5.6-9.431h12.954l20.105,36.684H421.843L408.578,423.6l-12.851,22.486Z" transform="translate(-384.741 -409.399)"></path><path fill="#f9c52d" d="M601.982,409.4h21.872l-10.936,19.118Z" transform="translate(-571.865 -409.399)"></path></g></g></svg>
            </a>
        </div>
		
        <ul class="side-menu accordion" id="accordionSidebar">
            
            <?php
            if($session->userlevel==7)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>datadashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>createquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Create Question</a>
                </li>
                <?php if($session->username!='dataentry_test'){ ?>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <?php } $customcheck=$database->query("select * from users where username='".$session->username."'");
                     $customcheck1=mysqli_fetch_array($customcheck);
                     if($customcheck1['customcontent']){
                ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent_v1/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
            <?php }
            if($customcheck1['ppaper']=='1'){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
					</li>
				<?php 
					}
                if($session->username!='dataentry_test'){ 
				?>
        
				<li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Question</a>
                </li>
              
                
            <?php
                }
            }else if($session->userlevel==8)
            {
            ?>
				
				<!-- <li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
				</li>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>question_analysis/','getLayout=true')"><i class="material-icons icon">styles</i>Question Analysis</a>
                </li>
                <?php $customcheck=$database->query("select * from users where username='".$session->username."'");
                     $customcheck1=mysqli_fetch_array($customcheck);
                     if($customcheck1['customcontent']){
                ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li> -->
            <?php 
            }
             
            if($customcheck1['ppaper']=='1'){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
					</li>
				<?php 
					}
				?>
                <?php  if($customcheck1['update_chapter']){ ?>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>que_chaptertopic/','getLayout=true')"><i class="material-icons icon">styles</i>Chapter and Topic Changes</a>
                </li>
				<?php } ?>
                
               
            <?php
            }else if($session->userlevel==3)
            {
				$customcheck=$database->query("select * from users where username='".$session->username."'");
                 $customcheck1=mysqli_fetch_array($customcheck);
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>dashboard_v1/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
				</li>
				<?php
				 if($customcheck1['questionsearch']){
				?>
					 <li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
					</li>
				<?php } ?>
				<?php
				if($customcheck1['customcontent']){
					?>
					<li class="nav-item ">
						<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent_v1/','getLayout=true&type=customcontent')"><i class="material-icons icon">styles</i>Custom Content</a>
					</li>
				<?php 
				}
				if($customcheck1['ppaper']=='1'){
					?>
						<li class="nav-item ">
							<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
						</li>
				<?php 
				}
				if($customcheck1['update_chapter']){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>que_chaptertopic_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Chapter and Topic Changes</a>
					</li>
                <?php 
                } 
               //if($session->username!='bhavya1' || $session->username!='aishwarya' || $session->username!='sireeshalec'){
                if($customcheck1['fitment_report']){
                ?>
                    <li class="nav-item ">
                        <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>fitmentreport/','getLayout=true')"><i class="material-icons icon">styles</i>Fitment Report</a>
                    </li>
                <?php } if($customcheck1['exam_paper_generation']==1){?>
                  <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>exampaper_generate/','getLayout=true')"><i class="material-icons icon">people</i>Exam Paper Generation</a>
                </li>
                  <?php }
                if($customcheck1['institute_epaper']){
                 ?>
                    <li class="nav-item ">
                        <a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_epaper/','getLayout=true')"><i class="material-icons icon">styles</i>Institute Paper Analysis</a>
                    </li>
                
                <?php
                }
                if($customcheck1['question_forum']){ ?>
                     <li class="nav-item ">
					    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionforum/','getLayout=true')"><i class="material-icons icon">styles</i>Question Forum</a>
                    </li>
                    
              <?php   }
              if($customcheck1['ppaper_analysis']){
                ?>
                 <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previous_paper_analysis/','getLayout=true')"><i class="material-icons icon">people</i>Previous Paper analysis</a>
                </li>
            <?php 
            }
            if($customcheck1['attributeoverview']){  ?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewdashboard/','getLayout=true')"><i class="material-icons icon">people</i> Attribute Overview</a>
                </li>
            <?php }  ?>
            
               
            <?php 
            }else if($session->userlevel==9)
            {

            $modulecheck = $database->query("select * from admin_modules where username='".$session->username."'");
                     $modulecheck1=mysqli_fetch_array($modulecheck);
                     if($modulecheck1['dashboard']==1){
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>admindashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <?php } if($modulecheck1['globaldata']==1){ ?>
				 <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-cogs icon" aria-hidden="true"></i> Global Data
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>subjects/','getLayout=true')">Add Subject</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>chapters/','getLayout=true')">Add Chapters</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>topics/','getLayout=true')">Add Topics</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>complexity/','getLayout=true')">Add Complexity</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questiontype/','getLayout=true')">Add Question Type</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>usageset/','getLayout=true')">Question Usage Set</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>ppaper_years/','getLayout=true')">Previous Paper Years</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>faqs/','getLayout=true')">Add FAQs</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>user_access_modules/','getLayout=true')">User Access Modules</a>
                        </li> 
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_plans/','getLayout=true')">Institute Plans</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>video_category/','getLayout=true')">Video Category</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_notifications/','getLayout=true')">Welcome Notifications</a>
                        </li>
                    </ul>
                </li>
                 <?php 
                 } 
				if($modulecheck1['webinar']==1){
                 ?>
                   
				 <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-ticketsweb" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="material-icons icon">people</i> Webinars
                    </a>
                    <ul class="collapse sub-menu" id="page-ticketsweb" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
						<?php if($session->username=='sireeshatest1' ){  ?>
                        <li class="subnav-item">
                            
							 <a class="subnav-link"" style="cursor:pointer" oonclick="setState('main-content','<?php echo SECURE_PATH;?>webinar/','getLayout=true')">Webinars</a>
                        </li>
						<?php } ?>
						<li class="subnav-item">
                            
							 <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>webinar_report/','getLayout=true')">Webinar Report</a>
                        </li>
                       
                       
                    </ul>
                </li>
				<?php } if($modulecheck1['videos']==1){ ?>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-file-video-o icon" aria-hidden="true"></i> Videos
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets1" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link"" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>createvideos/','getLayout=true')">Upload Videos</a>
                        </li>
						<li class="subnav-item">
                            
							 <a class="subnav-link"" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>videoprice/','getLayout=true')"> Video Prices</a>
                        </li>
                       
                       
                    </ul>
                </li>
                <?php } if($modulecheck1['emails']==1){ ?>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets5" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-envelope-square icon" aria-hidden="true"></i> Emails
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets5" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link"" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>email_template/','getLayout=true')">Email Template</a>
                        </li>
						<li class="subnav-item">
                            
							 <a class="subnav-link"" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>send_email_template/','getLayout=true')"> Send Email Template</a>
                        </li>
                       
                       
                    </ul>
                </li>
                <?php
                
				}if($modulecheck1['usercreation']==1){
            ?>

                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>users/','getLayout=true')"><i class="material-icons icon">people</i>User Creation</a>
                </li>
                 <?php }  if($modulecheck1['registredusers']==1){?>
                 <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>registredusers1/','getLayout=true')"><i class="material-icons icon">people</i>Registered User</a>
                </li>
                
				  <?php }  if($modulecheck1['exam_paper_generation']==1){?>
                  <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>exampaper_generate/','getLayout=true')"><i class="material-icons icon">people</i>Exam Paper Generation</a>
                </li>
                  <?php } 
                  if($modulecheck1['mocktest_report']==1){
                    ?>
                    <li class="nav-item">
                        <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>mocktest_report/','getLayout=true')"><i class="material-icons icon">people</i>Mock Test Report</a>
                    </li>
				  <?php } if($modulecheck1['institute_mocktest']==1){ ?>
                    <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_mocktest/','getLayout=true')"><i class="material-icons icon">people</i>Institute Mocktest</a>
                </li>
                    <?php
                     }
                  if($modulecheck1['institute_paper_analysis']==1){?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_epaper/','getLayout=true')"><i class="material-icons icon">styles</i>Institute Paper Analysis</a>
                </li>
                
                <?php }  if($modulecheck1['previous_paper_analysis']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previous_paper_analysis/','getLayout=true')"><i class="material-icons icon">people</i>Previous Paper analysis</a>
                </li>
                <?php }  if($modulecheck1['notify_users']==1){?>
               
			    <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/','getLayout=true')"><i class="material-icons icon">people</i>Institute Schedule</a>
                </li>
				
			   
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>notifyusers/','getLayout=true')"><i class="material-icons icon">people</i>Notify Users</a>
                </li>
				 <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>notifyusers_new/','getLayout=true')"><i class="material-icons icon">people</i>Notications New</a>
                </li>
                <?php } if($modulecheck1['notification_report']==1){ ?>
                <li class="nav-item ">
                        <a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>notification_report/','getLayout=true')"><i class="material-icons icon">widgets</i>  Notification Report</a>
                    </li>
				<?php  } if($modulecheck1['referral_code']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>referralcodes/','getLayout=true')"><i class="material-icons icon">people</i>Referral code</a>
                </li>
				<?php }  if($modulecheck1['referral_code_report']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>referall_report/','getLayout=true')"><i class="material-icons icon">people</i>Referral Code Report</a>
                </li>
				<?php }  if($modulecheck1['app_cards']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>home_cards/','getLayout=true')"><i class="material-icons icon">people</i>App Cards</a>
                </li>
				<?php }  if($modulecheck1['app_banners']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>app_banners/','getLayout=true')"><i class="material-icons icon">people</i>App Banners</a>
                </li>
                <?php } if($modulecheck1['student_analytics']==1){?>
                <li class="nav-item ">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_analytics_test/','getLayout=true')"><i class="material-icons icon">widgets</i>Testing - Student Usage Analytics</a>
                </li>
                 <?php  
                 }
 				if($modulecheck1['student_analytics']==1){?>
                <li class="nav-item ">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_analytics/','getLayout=true')"><i class="material-icons icon">widgets</i>  Student Usage Analytics</a>
                </li>
                 <?php  
                 }
                 if($modulecheck1['student_active_log']==1){
                     ?>
                     <li class="nav-item ">
                        <a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_anaytics_test/','getLayout=true')"><i class="material-icons icon">widgets</i>  Student Activity Data</a>
                    </li>
                <?php } if($session->username=='sireeshatest1'){ ?>
                    <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_version_latest/','getLayout=true')"><i class="material-icons icon">people</i>Student Android Report</a>
                </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>notification_report1/','getLayout=true')"><i class="material-icons icon">widgets</i>  Notification Report Test</a>
                    </li>
                     <?php
                }
                  if($modulecheck1['users']==1){ ?>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="material-icons icon">people</i> Users
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets1" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>dataentryusers/','getLayout=true')"><i class="far fa-user icon"></i>Data Entry
                            Users</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>lecturerusers/','getLayout=true')"><i class="far fa-user icon"></i>Lecturer
                            Users</a>
                    </li>
                </ul>
            </li>
             <?php } 
            if($modulecheck1['questionsearch']==1){
        ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionreport/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <?php }  if($modulecheck1['chapter_topic_changes']==1){?>
                 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>que_chaptertopic_admin/','getLayout=true')"><i class="material-icons icon">styles</i>Chapter and Topic Changes</a>
                </li>
				 <?php }  if($modulecheck1['fitment_report']==1){?>
                 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>fitmentsummary/','getLayout=true')"><i class="material-icons icon">styles</i>Fitment Report</a>
                </li>
                 <?php }  if($modulecheck1['question_forum']==1){?>
                    <li class="nav-item ">
                   <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionforum/','getLayout=true')"><i class="material-icons icon">styles</i>Question Forum</a>
               </li>
                   
             
                 <?php  } 
                 ?>
                 
                 <?php if($modulecheck1['customcontent']==1){?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
                 <?php  } if($modulecheck1['customcontent_overview']==1){?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>custom_overview/','getLayout=true&type=custom_overview')"><i class="material-icons icon">styles</i>Custom Content Overview</a>
                </li> 
				 <?php  } if($modulecheck1['usageset_report']==1){?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>usagesetreport/','getLayout=true')"><i class="material-icons icon">styles</i>Usageset Report</a>
                </li>
				<?php  } if($modulecheck1['reviewer_dashboard']==1){?>
                <li class="nav-item">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewerdashboard1/','getLayout=true')"><i class="material-icons icon">widgets</i> Reviewer Dashboard</a>
                </li>
				<?php  } if($modulecheck1['assessment_report']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>assessment_report/','getLayout=true')"><i class="material-icons icon">people</i>Assessment Report</a>
                </li>
				<?php  } if($modulecheck1['student_reported_issues']==1){?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_issues/','getLayout=true')"><i class="material-icons icon">styles</i>Student Reported Issues</a>
                </li>
				<?php  } if($modulecheck1['student_feedback']==1){?>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>feedback/','getLayout=true')"><i class="material-icons icon">people</i> Student Feedback</a>
                </li>
                <?php
				 } if($modulecheck1['rejectqns']==1){ ?>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Questions</a>
                </li>
                <?php } if($modulecheck1['deletedqns']==1){?>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>deleteqreport/','getLayout=true')"><i class="material-icons icon">styles</i>Deleted Questions</a>
                </li>
                 <?php } if($modulecheck1['ppaperqns']==1){?>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
                </li>
                <?php } if($modulecheck1['linkage_chapter_report']==1){?>
                <li class="nav-item ">
                            <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>Linkage_Report/','getLayout=true')"><i class="material-icons icon">styles</i>Linkage Chapter Report</a>
                        </li>
				 <?php } if($modulecheck1['chapter_report']==1){?>
                <li class="nav-item ">
                            <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>chapterreport/','getLayout=true')"><i class="material-icons icon">styles</i>Chapter wise Report</a>
                        </li>
						 <?php }?>
                <!-- <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-projects" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseTwo">
                        <i class="material-icons icon">styles</i> Questions
                    </a>
                    <ul class="collapse sub-menu" id="page-projects" aria-labelledby="headingTwo"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>viewquestion/','getLayout=true')">View Qestions</a>
                        </li>
                        <li class="subnav-item">
							<a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>createquestion/','getLayout=true')">Create Question</a>
						</li>
                           
                    </ul>
                </li> -->
               
            <?php
            }else if($session->userlevel==6)
            {
            ?>
				
				<li class="nav-item">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewdashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>review_qsearch/','getLayout=true')"><i class="material-icons icon">widgets</i> Question Search</a>
                </li>
                <li class="nav-item">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewerdashboard1/','getLayout=true')"><i class="material-icons icon">widgets</i> Reviewer Dashboard</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>fitmentsummary/','getLayout=true')"><i class="material-icons icon">styles</i>Fitment Report</a>
                </li>
                <li class="nav-item ">
						<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent_lec/','getLayout=true&type=customcontent')"><i class="material-icons icon">styles</i>Custom Content</a>
					</li>
                    <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent_v1/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Search</a>
                </li>
            <?php
            }else if($session->userlevel==1)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>superdashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-cogs icon" aria-hidden="true"></i> Global Data
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>subjects/','getLayout=true')">Add Subject</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>chapters/','getLayout=true')">Add Chapters</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>topics/','getLayout=true')">Add Topics</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>complexity/','getLayout=true')">Add Complexity</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questiontype/','getLayout=true')">Add Question Type</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>usageset/','getLayout=true')">Question Usage Set</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>ppaper_years/','getLayout=true')">Previous Paper Years</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>institute/','getLayout=true')">Add Institute</a>
                        </li>
                    </ul>
                </li>
               
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>users/','getLayout=true')"><i class="material-icons icon">people</i>User Creation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="material-icons icon">people</i> Users
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets1" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>instituteusers/','getLayout=true')">Create Institute Admin</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>registredusers1/','getLayout=true')">Registered Users</a>
                    </li>
  <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>assessment_report/','getLayout=true')"><i class="material-icons icon">people</i>Assessment Report</a>
                </li>

                </ul>
            </li>
            <li class="nav-item ">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_analytics/','getLayout=true')"><i class="material-icons icon">widgets</i>  Student Usage Analytics</a>
                </li>
            <li class="nav-item ">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewerdashboard1/','getLayout=true')"><i class="material-icons icon">widgets</i> Reviewer Dashboard</a>
                </li>
               
                <!-- <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionreport/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li> -->
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionreport/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Questions</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>deleteqreport/','getLayout=true')"><i class="material-icons icon">styles</i>Deleted Questions</a>
                </li>
				<li class="nav-item ">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>adminaccess/','getLayout=true')"><i class="material-icons icon">styles</i>Admin Modules</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>excelreports/','getLayout=true')"><i class="material-icons icon">styles</i>Excels</a>
                </li>
            <?php
            }else if($session->userlevel==2)
            {
				$customcheck=$database->query("select * from users where username='".$session->username."'");
                $customcheck1=mysqli_fetch_array($customcheck);
            ?>
				<?php if($customcheck1['lecturerdashboard']=='1'){ ?>
					<li class="nav-item active">
						<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
					</li>
				<?php }  ?>
				<?php if($customcheck1['questionsearch']=='1'){ ?>
					 <li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
					</li>
				<?php } ?>
				<?php if($customcheck1['questionanalysis']=='1'){ ?>
					<li class="nav-item ">
						<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewdashboard/','getLayout=true')"><i class="material-icons icon">styles</i>Question Analysis</a>
					</li>
                <?php
					}
                     if($customcheck1['customcontent']){
                ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
			 <?php 
				}	
             
				if($customcheck1['ppaper']=='1'){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions_v1/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
					</li>
				<?php 
				}
				?>
                <?php  if($customcheck1['update_chapter']){ ?>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>que_chaptertopic/','getLayout=true')"><i class="material-icons icon">styles</i>Chapter and Topic Changes</a>
                </li>
				<?php } ?>
                
            <?php
            }if($session->userlevel==4)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>student_analytics/','getLayout=true')"><i class="material-icons icon">widgets</i>  Student Usage Analytics</a>
                </li>
                
            <?php
            }
            ?>
            

           
            
           

        </ul>
		<div class="nav-item login-button">
            <a href="<?php echo SECURE_PATH;?>indexout.php" ><i class="fas fa-sign-out-alt"></i>
            <span class="pl-2">Logout</span></a>
        </div>
    </nav>
	
    <div class="menu-overlay"></div>

    <main class="main-wrapper">
        <header class="header-area">
            <nav class="navbar navbar-expand navbar-light bg-admin">
                <div class="container">
                    <button class="btn btn-light btn-circle text-theme order-1 order-sm-0" id="sidebarCollapse">
                        <i class="material-icons text-theme md-18">more_vert</i>
                    </button>
                    <!-- Navbar -->
                    <ul class="navbar-nav ml-auto">

                        
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle pt-1" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo SECURE_PATH;?>vendor/images/user-profile.png" width="35" alt="profile-user"
                                    class="rounded-circle">
                                    <div class="d-none d-xl-inline-block  text-white"><?php echo ucfirst($database->get_name('users','username',$session->username,'name'))?></div>
                            </a>
                            
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        
        <div id="main-content">
            
            <!-- End Breadcrumbs-->

            <!-- Content Area-->
			<?php
			if($session->userlevel==7){
			?>
				<script>
					setState('main-content','<?php echo SECURE_PATH;?>datadashboard/','getLayout=true');
				</script>
			<?php
            }else if($session->userlevel==6){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>review_qsearch/','getLayout=true');
                    </script>
                <?php
            }
            else if($session->userlevel==8){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true');
                    </script>
                <?php
                }else if($session->userlevel==9){
                    if($session->username=='nandana' ||  $session->username=='anushav' || $session->username=='preneeth' || $session->username=='sireesha'){
						?>
						<script>
							setState('main-content','<?php echo SECURE_PATH;?>student_analytics/','getLayout=true');
						</script>
					<?php
					}else{
			?>
					<script>
						setState('main-content','<?php echo SECURE_PATH;?>admindashboard/','getLayout=true');
					</script>
				<?php
				}
			}else if($session->userlevel==3){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>dashboard_v1/','getLayout=true');
                    </script>
                <?php
            }else if($session->userlevel==1){
        ?>
            <script>
                setState('main-content','<?php echo SECURE_PATH;?>superdashboard/','getLayout=true');
            </script>
        <?php
        }else if($session->userlevel==2){
            ?>
                <script>
                    setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true');
                </script>
            <?php
            }else if($session->userlevel==4){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>student_analytics/','getLayout=true');
                    </script>
                <?php
                }
        ?>
			
                <!-- End Content-->

                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="material-icons">navigation</i>
                </a>
            </div>
        </div>
        <footer class="footer border-top py-3 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copy-rights">
                            <span class="text-dark">&copy; 2019 <a href="#" target="_blank">Admin</a> All rights
                                reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </main>
</html>
<script type="text/javascript">
set_interval();
function set_interval(){
timer=setInterval("auto_logout()",1800000);
 
}

function reset_interval(){

  clearInterval(timer);
  
timer=setInterval("auto_logout()",1800000);
 
}

function auto_logout(){
	<?php
		if($_SESSION['username']!='rajeev' && $_SESSION['username']!='admin2'){
	
	?>
		location.replace("<?php echo SECURE_PATH;?>indexout.php");
	<?php
	}
	?>

}
</script>
<?php
    $session->commonFooterAdminJS();
    }
?>