<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	header('Location: '.SECURE_PATH.'admin/');
    }
    else
    {
    ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="shortcut icon" href="../img/favicon.ico">
        <?php echo $session->gdc_title();?>
        <?php echo $session->commonJS();?>
        <?php echo $session->commonAdminCSS();?>
    </head>

    <body id="page-top" onmousemove="reset_interval()" onclick="reset_interval()" onkeypress="reset_interval()" onscroll="reset_interval()">

    <div class="preloader">
        <div class="loader"></div>
    </div>

    <nav id="sidebar">
        <div class="sidebar-header">
            <a href="<?php echo SECURE_PATH;?>home/">
                <img src="<?php echo SECURE_PATH;?>vendor/images/neetjeelogo.png" class="img-fluid" alt="Image" />
            </a>
        </div>
		
        <ul class="side-menu accordion" id="accordionSidebar">
            
            <?php
            if($session->userlevel==7)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>datadashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>createquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Create Question</a>
                </li>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <?php $customcheck=$database->query("select * from users where username='".$session->username."'");
                     $customcheck1=mysqli_fetch_array($customcheck);
                     if($customcheck1['customcontent']){
                ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
            <?php }
            if($customcheck1['ppaper']=='1'){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
					</li>
				<?php 
					}
				?>
        
				<li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Question</a>
                </li>
              
                
            <?php
            }else if($session->userlevel==8)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
				</li>
				 <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>question_analysis/','getLayout=true')"><i class="material-icons icon">styles</i>Question Analysis</a>
                </li>
                <?php $customcheck=$database->query("select * from users where username='".$session->username."'");
                     $customcheck1=mysqli_fetch_array($customcheck);
                     if($customcheck1['customcontent']){
                ?>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
            <?php 
            } 
            if($customcheck1['ppaper']=='1'){
				?>
					<li class="nav-item ">
						<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
					</li>
				<?php 
					}
				?>
            
                <!-- <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
                </li> -->
               
            <?php
            }else if($session->userlevel==9)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>admindashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                
				 <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-cogs icon" aria-hidden="true"></i> Global Data
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>subjects/','getLayout=true')">Add Subject</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>chapters/','getLayout=true')">Add Chapters</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>topics/','getLayout=true')">Add Topics</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>complexity/','getLayout=true')">Add Complexity</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questiontype/','getLayout=true')">Add Question Type</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>usageset/','getLayout=true')">Question Usage Set</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>ppaper_years/','getLayout=true')">Previous Paper Years</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>users/','getLayout=true')"><i class="material-icons icon">people</i>User Creation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="material-icons icon">people</i> Users
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets1" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>dataentryusers/','getLayout=true')"><i class="far fa-user icon"></i>Data Entry
                            Users</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>lecturerusers/','getLayout=true')"><i class="far fa-user icon"></i>Lecturer
                            Users</a>
                    </li>
                </ul>
            </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionreport/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Questions</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>deleteqreport/','getLayout=true')"><i class="material-icons icon">styles</i>Deleted Questions</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>previousquestions/','getLayout=true')"><i class="material-icons icon">styles</i>Previous Paper Questions</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-projects" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseTwo">
                        <i class="material-icons icon">styles</i> Questions
                    </a>
                    <ul class="collapse sub-menu" id="page-projects" aria-labelledby="headingTwo"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>viewquestion/','getLayout=true')">View Qestions</a>
                        </li>
                        <li class="subnav-item">
							<a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>createquestion/','getLayout=true')">Create Question</a>
						</li>
                           
                    </ul>
                </li> -->
               
            <?php
            }else if($session->userlevel==6)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>reviewdashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                
				
            <?php
            }else if($session->userlevel==1)
            {
            ?>
				
				<li class="nav-item active">
					<a class="nav-link" href="#" onclick="setState('main-content','<?php echo SECURE_PATH;?>superdashboard/','getLayout=true')"><i class="material-icons icon">widgets</i> Dashboard</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="fa fa-cogs icon" aria-hidden="true"></i> Global Data
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                        <li class="subnav-item">
                            
							 <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>subjects/','getLayout=true')">Add Subject</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>chapters/','getLayout=true')">Add Chapters</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>topics/','getLayout=true')">Add Topics</a>
                        </li>
                        <li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>complexity/','getLayout=true')">Add Complexity</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questiontype/','getLayout=true')">Add Question Type</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>usageset/','getLayout=true')">Question Usage Set</a>
                        </li>
						<li class="subnav-item">
                            <a class="subnav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>ppaper_years/','getLayout=true')">Previous Paper Years</a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>users/','getLayout=true')"><i class="material-icons icon">people</i>User Creation</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link collapsed" href="#page-tickets1" data-toggle="collapse" aria-expanded="true"
                        aria-controls="collapseUtilities">
                        <i class="material-icons icon">people</i> Users
                    </a>
                    <ul class="collapse sub-menu" id="page-tickets1" aria-labelledby="headingUtilities"
                        data-parent="#accordionSidebar">
                
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>dataentryusers/','getLayout=true')"><i class="far fa-user icon"></i>Data Entry
                            Users</a>
                    </li>
                    <li class="subnav-item">
                        <a class="subnav-link" onclick="setState('main-content','<?php echo SECURE_PATH;?>lecturerusers/','getLayout=true')"><i class="far fa-user icon"></i>Lecturer
                            Users</a>
                    </li>
                </ul>
            </li>
                <!-- <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionreport/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li> -->
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>questionsearch/','getLayout=true')"><i class="material-icons icon">styles</i>Question Search</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link " style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>customcontent/','getLayout=true&type=questionsearch')"><i class="material-icons icon">styles</i>Custom Content</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>rejectedquestion/','getLayout=true')"><i class="material-icons icon">styles</i>Rejected Questions</a>
                </li>
                <li class="nav-item ">
					<a class="nav-link" style="cursor:pointer" onclick="setState('main-content','<?php echo SECURE_PATH;?>deleteqreport/','getLayout=true')"><i class="material-icons icon">styles</i>Deleted Questions</a>
                </li>
				
            <?php
            }
            ?>
            
           

        </ul>
		<div class="nav-item login-button">
            <a href="<?php echo SECURE_PATH;?>indexout.php" ><i class="fas fa-sign-out-alt"></i>
            <span class="pl-2">Logout</span></a>
        </div>
    </nav>
	
    <div class="menu-overlay"></div>

    <main class="main-wrapper">
        <header class="header-area">
            <nav class="navbar navbar-expand navbar-light bg-admin">
                <div class="container">
                    <button class="btn btn-light btn-circle text-theme order-1 order-sm-0" id="sidebarCollapse">
                        <i class="material-icons text-theme md-18">more_vert</i>
                    </button>
                    <!-- Navbar -->
                    <ul class="navbar-nav ml-auto">

                        
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle pt-1" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo SECURE_PATH;?>vendor/images/user-profile.png" width="35" alt="profile-user"
                                    class="rounded-circle">
                                    <div class="d-none d-xl-inline-block  text-white"><?php echo ucfirst($database->get_name('users','username',$session->username,'name'))?></div>
                            </a>
                            
                        </li>
                    </ul>
                </div>

            </nav>
        </header>
        
        <div id="main-content">
            
            <!-- End Breadcrumbs-->

            <!-- Content Area-->
			<?php
			if($session->userlevel==7){
			?>
				<script>
					setState('main-content','<?php echo SECURE_PATH;?>datadashboard/','getLayout=true');
				</script>
			<?php
            }else if($session->userlevel==6){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>reviewdashboard/','getLayout=true');
                    </script>
                <?php
            }
            else if($session->userlevel==8){
                ?>
                    <script>
                        setState('main-content','<?php echo SECURE_PATH;?>dashboard/','getLayout=true');
                    </script>
                <?php
                }else if($session->userlevel==9){
			?>
                <script>
					setState('main-content','<?php echo SECURE_PATH;?>admindashboard/','getLayout=true');
				</script>
			<?php
			}else if($session->userlevel==1){
        ?>
            <script>
                setState('main-content','<?php echo SECURE_PATH;?>superdashboard/','getLayout=true');
            </script>
        <?php
        }
			?>
                <!-- End Content-->

                <!-- Scroll to Top Button-->
                <a class="scroll-to-top rounded" href="#page-top">
                    <i class="material-icons">navigation</i>
                </a>
            </div>
        </div>
        <footer class="footer border-top py-3 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copy-rights">
                            <span class="text-dark">&copy; 2019 <a href="#" target="_blank">Admin</a> All rights
                                reserved.</span>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </main>
</html>
<script type="text/javascript">
set_interval();
function set_interval(){
timer=setInterval("auto_logout()",900000);
 
}

function reset_interval(){

  clearInterval(timer);
  
timer=setInterval("auto_logout()",900000);
 
}

function auto_logout(){
	<?php
		if($_SESSION['username']!='rajeev' && $_SESSION['username']!='admin2'){
	
	?>
		location.replace("<?php echo SECURE_PATH;?>indexout.php");
	<?php
	}
	?>

}
</script>
<?php
    $session->commonFooterAdminJS();
    }
?>