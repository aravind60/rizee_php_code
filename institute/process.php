<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
//$con=mysqli_connect('localhost','root','','neetjee');
$con=mysqli_connect('localhost','rspace','Rsp@2019','neetjee');
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM institute WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	$sel1=mysqli_query($con,"select * from institutions where institution_id='".$_POST['editform']."'");
	$row1=mysqli_fetch_array($sel1);
	$_POST['class_ids']=explode(",",$row1['class_ids']);
	$_POST['exam_ids']=explode(",",$row1['exam_ids']);
	$_POST['subject_ids']=explode(",",$row1['subject_ids']);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<!-- <script>
		
		editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
	                 editor.insertInto(document.getElementById('editorContainer'));
				 width:300;
				 height:500;
	</script> -->
	
  <div class="col-lg-12 col-md-12">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Institute Name<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="institute_name" value="<?php if(isset($_POST['institute_name'])) { echo $_POST['institute_name']; }else{} ?>"  autocomplete="off" id="institute_name" >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['institute_name'])){ echo $_SESSION['error']['institute_name'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Class<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="class_ids" class="selectpicker1" id="class_ids" multiple data-actions-box="true" data-live-search="true" >
						
						<?php
						$row1 = $database->query("SELECT * FROM class WHERE estatus='1' "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['class_ids'])) { if(in_array($data1['id'], $_POST['class_ids'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['class'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['class_ids'])){ echo $_SESSION['error']['class_ids'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="exam_ids" class="selectpicker2" id="exam_ids" multiple data-actions-box="true" data-live-search="true"  onchange="setStateGet('subjectdiv','<?php echo SECURE_PATH;?>institute/process.php','getsubject=1&exam='+$('#exam_ids').val()+'')">
						
						<?php
						$row1 = $database->query("SELECT * FROM exam WHERE estatus='1' "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['exam_ids'])) { if(in_array($data1['id'], $_POST['exam_ids'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['exam'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_ids'])){ echo $_SESSION['error']['exam_ids'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6" id="subjectdiv">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Subjects<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="subject_ids" class="selectpicker3" id="subject_ids" multiple data-actions-box="true" data-live-search="true" >
						
						<?php
						$row1 = $database->query("SELECT * FROM subject WHERE estatus='1' "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['subject_ids'])) { if(in_array($data1['id'], $_POST['subject_ids'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['subject'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['subject_ids'])){ echo $_SESSION['error']['subject_ids'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Institute Address<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<textarea type="text" class="form-control" name="institute_address" value=""  autocomplete="off" id="institute_address" rows='4'><?php if(isset($_POST['institute_address'])) { echo $_POST['institute_address']; }else{} ?></textarea>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['institute_address'])){ echo $_SESSION['error']['institute_address'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>institute/process.php','validateForm=1&institute_name='+$('#institute_name').val()+'&institute_address='+$('#institute_address').val()+'&class_ids='+$('#class_ids').val()+'&exam_ids='+$('#exam_ids').val()+'&subject_ids='+$('#subject_ids').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
			</div>
		</div>
	</div>
	
	
	
</div>

<?php
unset($_SESSION['error']);
}
?>

<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	$field = 'institute_name';
	if(!$post['institute_name'] || strlen(trim($post['institute_name'])) == 0){
	  $_SESSION['error'][$field] = "* Institute  cannot be empty";
	}
	$field = 'class_ids';
	if(!$post['class_ids'] || strlen(trim($post['class_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Class  cannot be empty";
	}
	 $field = 'exam_ids';
	if(!$post['exam_ids'] || strlen(trim($post['exam_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Exam cannot be empty";
	}
	 $field = 'subject_ids';
	if(!$post['subject_ids'] || strlen(trim($post['subject_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Subject cannot be empty";
	}
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>institute/process.php','addForm=1&institute_name=<?php echo $post['institute_name'];?>&institute_address=<?php echo $post['institute_address'];?>&class_ids=<?php echo $post['class_ids'];?>&exam_ids=<?php echo $post['exam_ids'];?>&subject_ids=<?php echo $post['subject_ids'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$post['from_page'];}?>')
    </script>
  <?php
	}
	else{
		
		if($id=='NULL')
		{
			$result=$database->query('insert institute set institute_name="'.$post['institute_name'].'",institute_address="'.$post['institute_address'].'",estatus="1",timestamp="'.time().'"');

			$institute_id = mysqli_insert_id($database->connection);

			
			//mysqli_query($con,"INSERT INTO institutions VALUES(".$institute_id.", '1,2', '1,2','1,2,3,4,5',1,".$institute_id.",'".time()."')");
			mysqli_query($con,"INSERT INTO institutions VALUES(".$institute_id.", '".$post['class_ids']."', '".$post['exam_ids']."','".$post['subject_ids']."',1,".$institute_id.",'".time()."')");
			mysqli_query($con,"INSERT  organisation_structure set  group_name='".$post['institute_name']."',parent_id='0',institution_id='".$institute_id."',username='".$session->username."',estatus='1',timestamp='".time()."'");
			mysqli_query($con,"INSERT  organisation_profile set  organisation_name='".$post['institute_name']."',complete_address='".$post['institute_address']."',region_id='0',institution_id='".$institute_id."',username='".$session->username."',estatus='1',timestamp='".time()."'");
			
			

		if($institute_id!="" && $institute_id!=0 && $institute_id!=NULL){
			
			
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Updation  Successfully!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>institute/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}else{
					?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Creation Failed!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>institute/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}
			
		}else{
			$result=$database->query('update institute set institute_name="'.$post['institute_name'].'",institute_address="'.$post['institute_address'].'" where id="'.$id.'"');
			//mysqli_query($con,"INSERT INTO institutions VALUES(".$institute_id.", '1,2', '1,2','1,2,3,4,5',1,".$institute_id.",'".time()."')");
			mysqli_query($con,'update institutions set institution_id="'.$id.'",class_ids="'.$post['class_ids'].'",exam_ids="'.$post['exam_ids'].'",subject_ids="'.$post['subject_ids'].'" where institution_id="'.$id.'"');
			mysqli_query($con,"update  organisation_profile set  organisation_name='".$post['institute_name']."',complete_address='".$post['institute_address']."'  where institution_id='".$id."'");
			if($result){
				?>
					<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Updated  Successfully!</div>
					
						<script type="text/javascript">
					
						animateForm('<?php echo SECURE_PATH;?>institute/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
			<?php
			}else{
					?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Institute Creation Failed!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>institute/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			}
		}
	}
		

?>

 
	<?php
	

}
?>
<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'institute';
  $condition = "estatus='1'";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."institute/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition."  order by id asc";
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
		  <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50
    
    
  });

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Institute Data</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Institute Name</th>
											<th class="text-left" nowrap>Institute Data</th>
											<th class="text-left" nowrap>Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$k=1;
									  while($value = mysqli_fetch_array($data_sel))
									  {
										 
									  ?>
														<tr>
														<td class="text-left"><?php echo $k;?></td>
										<td class="text-left" nowrap><?php echo $value['institute_name'];?></td>
										<td class="text-left" nowrap><?php echo $value['institute_address'];?></td>
										<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>institute/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
										<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>institute/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a></td>
									  </tr>
														<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php
	}
	else{
	?>
		
  <?php
	}

}
if(isset($_REQUEST['getsubject'])){
	if(isset($_REQUEST['exam'])){
		if($_REQUEST['exam']!=''){
			$examid='';
			$sql=$database->query("select * from exam where estatus='1' and id in (".$_REQUEST['exam'].")");
			while($row=mysqli_fetch_array($sql)){
				$examid.=$row['subjects'].",";
			}
			$exam=explode(",",$examid);
			$exam1=array_unique($exam);
			$subject=implode(",",$exam1);
			$subjectids=" AND id IN (".rtrim($subject,",").")";
		}else{
			$subjectids=" AND id IN (1,2,3,4,5)";
		}
	}else{
		$subjectids=" AND id IN (1,2,3,4,5)";
	}
	?>
		<div class="form-group row">
			<label class="col-sm-4 col-form-label">Subjects<span style="color:red;">*</span></label>
			<div class="col-sm-8">
				<select name="subject_ids" class="selectpicker3" id="subject_ids" multiple data-actions-box="true" data-live-search="true" >
					
					<?php
						echo "SELECT * FROM subject WHERE estatus='1' ".$subjectids."";
					$row1 = $database->query("SELECT * FROM subject WHERE estatus='1' ".$subjectids.""); 
					while($data1 = mysqli_fetch_array($row1))
					{
					  ?>
					<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['subject_ids'])) { if(in_array($data1['id'], $_POST['subject_ids'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['subject'];?></option>
					<?php
					}
					?>
				</select>
				<span class="error text-danger"><?php if(isset($_SESSION['error']['subject_ids'])){ echo $_SESSION['error']['subject_ids'];}?></span>
			</div>
		</div>
	<?php
}
?>
	<?php
if(isset($_GET['rowDelete'])){
	$database->query("update institute set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Subject deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>institute/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>

  <script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>



