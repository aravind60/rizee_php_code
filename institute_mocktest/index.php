<?php
    error_reporting(0);
    include('../include/session.php');
	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	header('Location: '.SECURE_PATH);
    }
    else
    {
        //unset($_SESSION['image']);  
    ?>
    
   
		<style>
         .wrs_editor .wrs_formulaDisplay {
             height: 250px !important;
         }
         </style>
       
				
			<div class="content-wrapper">
				<!-- Breadcrumbs-->
				<section class="breadcrumbs-area2 my-3">
					<div class="container">
						<div class="d-flex justify-content-between align-items-center">
							<div class="title">
								<h1 class="text-uppercase">Add Institute Mocktest <small>Lets you a create Institute Mocktest</small></h1>
							</div>
						</div>
						<hr>
					</div>
				</section>
                <!-- End Breadcrumbs-->
				<!-- Content Area-->
				<section class="content-area">
					<div class="container">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
								<div class="card border-0 shadow mb-4">
									<div
										class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
										<h6 class="m-0 font-weight-bold text-primary">Create Institute Mocktest</h6>
									</div>
									<div class="card-body">
									  
											<div class="row">
												<div class="col-lg-12 col-md-12">
													<div class="form-group row">
														<div class="col-sm-8">
															<a href="#" class="btn btn-md btn-theme-2"onClick="$('#adminForm').slideToggle()">Create Institute Mocktest</a>
														</div>
													</div>
													<div class="form-group row" id="adminForm" style="display:none;">
														<div class="col-sm-8">
															<script type="text/javascript">
																setStateGet('adminForm','<?php echo SECURE_PATH;?>institute_mocktest/process.php','addForm=1');
															</script>
														</div>
													</div>
												</div>
											</div>
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				 <section>
					<div id="adminTable">
						<script type="text/javascript">
							setStateGet('adminTable','<?php echo SECURE_PATH;?>institute_mocktest/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					</div>
				</section>
			</div>
		
	<?php
		
		}
	?>
