<?php
ini_set('display_errors','0');
include('../include/session.php');
?>
<style>
	  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
  </style>
<?php
	if(isset($_REQUEST['tabledata'])){
		

		 $tableName = 'institute_notifications_days';
  $condition = "estatus='1' and is_admin='1'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  $q = "SELECT count(*) as count FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query1($q);
  $numdata = mysqli_fetch_array($result_sel);
  $numres=$numdata['count'];
    $query = "SELECT * FROM $tableName ".$condition."  ORDER BY timestamp DESC";
  //echo $query;
  $data_sel = $database->query1($query);
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						
						<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														
														<th class="text-left" nowrap>Date & Time</th>
														<th class="text-left" nowrap>Class</th>
														<th class="text-left" nowrap>Exam</th>
														<th class="text-left" nowrap>Title</th>
														<th class="text-left" nowrap>Description</th>
														<th class="text-left" nowrap>Day</th>
														<th class="text-left" nowrap>Options</th>
													</tr>
												</thead>
												<tbody>
													<?php
														
													$i=1;
													while($value=mysqli_fetch_array($data_sel)){
														$time=$value['timestamp'];
														$class='';
														$classsel=$database->query("select * from class where estatus='1' and id in (".$value['class_ids'].")");
														while($rowclass=mysqli_fetch_array($classsel)){
															$class.=$rowclass['class'].",";
														}

														$exam='';
														$examsel=$database->query("select * from exam where estatus='1' and id in (".$value['exam_ids'].")");
														while($rowclass=mysqli_fetch_array($examsel)){
															$exam.=$rowclass['exam'].",";
														}

														?>
														<tr>

															<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y  H:i:s',$value['timestamp']);?></td>
															<td class="text-left"><?php echo rtrim($class,","); ?></td>
															<td class="text-left"><?php echo rtrim($exam,","); ?></td>
															<td class="text-left"><?php echo $value['title']; ?></td>
															<td class="text-left"><?php echo $value['description']; ?></td>
															<td>Day- <?php echo $value['day']; ?></td>
															<td>
																<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal1"onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>institute_notifications/prnt.php','viewdetails=1&id=<?php echo $value['id'];?>')"><i class="fa fa-eye" ></i></a>&nbsp;
																<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>institute_notifications/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a>
															</td>	
													  </tr>
													<?php
													$i++;
													}
													
													?>
												
												</tbody>
											</table>
									
										</div>
					</div>
				</div>
			</div>
		</div>

  </section>
 	<script type="text/javascript">
  $('#dataTable').DataTable({
	"pageLength": 50,
	"order": [[ 0, "desc" ]],
   
    
  });
   
</script>
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}
	}
	?>
