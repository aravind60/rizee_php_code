<?php
include('../include/db.php');
include('../include/session.php');
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
?>
<style>
.subjectanalysis {
	padding: .75rem 1.25rem;
	margin-bottom: 0;
	background-color: rgba(0,0,0,.03);
	border-bottom: 1px solid rgba(0,0,0,.125);
}
.subject-wise-analysis.nav-pills .nav-link.active {
	background-color:#000;
	color:#fff;
}
.subject-analysis-content label, h6 {
	color:#000;
}
.subject-analysis-content .table td p {
	color:#000000c7;
	font-weight:bold;
}
.subject-analysis-content .table th, .subject-analysis-content .table td {
	font-size:12px !important;
	padding:0.7rem 0.1rem !important;
}

.multiselect-item.multiselect-filter .input-group-btn{
	display:none;
}
.multiselect-native-select .multiselect.dropdown-toggle {
	border: 1px solid #ced4da;
}
.form-check .form-check-label
{
	margin-left:0;
}
</style>
<script>
	('.datepicker').selectpicker3();
</script>
<section class="content-area">
	<div class="container">
		<div class="row Data-Tables">
			<div class="col-xl-12 col-lg-12"> 
				<div class="card border-0 shadow mb-4">
					<div class="card-header py-3">
						<ul class="nav nav-pills" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#home" onclick="setStateGet('adminForm','<?php echo SECURE_PATH;?>institute_schedule/index.php','addForm=1')">Institute Schedule</a>
							</li>
							<li class="nav-item">
								<a class="nav-link " data-toggle="pill" href="#menu1" onclick="setState('ggg','<?php echo SECURE_PATH;?>institute_schedule/institute_schedule.php','tabledata=1')">Institute Schedule List</a>
							</li>

						</ul>
					</div>
					<div class="card-body">
						<div class="tab-content">
							<div id="home" class="container tab-pane active">
								<br>
								<div class="col-lg-12 col-md-12">
									<?php
									if(isset($_SESSION['SuccessMsg']))
									{
										?>
										<div class="alert alert-success">
											<strong>Success!</strong> <?php echo $_SESSION['SuccessMsg']; ?>
										</div>
										<?php
										unset($_SESSION['SuccessMsg']);
									}
									?>
									<?php
									if(isset($_SESSION['ErrorMsg']))
									{
										?>
										<div class="alert alert-danger">
											<strong>Success!</strong> <?php echo $_SESSION['ErrorMsg']; ?>
										</div>
										<?php
										unset($_SESSION['ErrorMsg']);
									}
									?>
									<?php
									$id=0;
									$txtDate="";
									$txtStartTime="";
									$txtEndTime="";
									$Exam_Id=[];
									$Subject_Id=0;
									$Chapter_Id=0;
									$Topic_Id=0;
									$txtRecordVideo="";
									$txtLive="";
									$txtConferenceDetails="";
									if(isset($_POST['editform']))
									{
										$id=$_POST['id'];
										$sql_tmp_schd=mysqli_query($con,"select * from institute_schedule where id=$id");
										while ($schdrow=mysqli_fetch_array($sql_tmp_schd)) 
										{
											$txtDate=$schdrow['date'];
											$txtStartTime=$schdrow['starttime'];
											$txtEndTime=$schdrow['endtime'];
											$Exam_Id=explode(",",$schdrow['exam_id']);
											$Subject_Id=$schdrow['subject'];
											$Chapter_Id=$schdrow['chapter'];
											$Topic_Id=$schdrow['topic'];
											$txtRecordVideo=$schdrow['recorded_video'];
											$txtLive=$schdrow['live_video'];
											$txtConferenceDetails=$schdrow['conference_details'];
										}
									}
								
									?>
									<div class="col-lg-12 col-md-12">
										<?php
										if($id>0)
										{
											?>
											<button class="btn btn-primary" id="btnAddNewSchedule" type="button">Add New Schedule</button>
											<?php
										}
										?>
										<div class="row">
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Date</label>
													<input type="date" name="txtDate" id="txtDate" class="form-control" value="<?php echo $txtDate; ?>">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Start Time</label>
													<input type="time" name="txtStartTime" id="txtStartTime" class="form-control" value="<?php echo $txtStartTime; ?>">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>End Time</label>
													<input type="time" name="txtEndTime" id="txtEndTime" class="form-control" value="<?php echo $txtEndTime; ?>">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Exam</label>
													<select class="form-control select2" name="Exam_Id[]" id="Exam_Id" multiple>
														<option value="3" <?php if(in_array(3,$Exam_Id)) { echo "selected"; } ?>>AP-EAMCEPT-MPC</option>
														<option value="7" <?php if(in_array(7,$Exam_Id)) { echo "selected"; } ?>>TS-EAMCEPT-MPC</option>
														<option value="6" <?php if(in_array(6,$Exam_Id)) { echo "selected"; } ?>>AP-EAMCEPT-BiPC</option>
														<option value="8" <?php if(in_array(8,$Exam_Id)) { echo "selected"; } ?>>TS-EAMCEPT-BiPC</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Subject</label>
													<select class="form-control select2" name="Subject_Id" id="Subject_Id">
														<option value="0">--Select--</option>
														<?php
														$sql_tmp_sub=mysqli_query($con,"select * from subject where estatus=1");
														while ($subrow=mysqli_fetch_array($sql_tmp_sub)) 
														{
															?>
															<option value="<?php echo $subrow['id']; ?>"><?php echo $subrow['subject']; ?></option>
															<?php
														}
														?>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Chapter</label>
													<select class="form-control select2" name="Chapter_Id" id="Chapter_Id">
														<option value="0">--Select--</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Topic</label>
													<select class="form-control select2" name="Topic_Id" id="Topic_Id">
														<option value="0">--Select--</option>
													</select>
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Recorded Video</label>
													<input type="text" name="txtRecordVideo" id="txtRecordVideo" class="form-control" value="<?php echo $txtRecordVideo; ?>">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Live</label>
													<input type="text" name="txtLive" id="txtLive" class="form-control" value="<?php echo $txtLive; ?>">
												</div>
											</div>
											<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
												<div class="form-group">
													<label>Conference Details</label>
													<input type="text" name="txtConferenceDetails" id="txtConferenceDetails"  class="form-control" value="<?php echo $txtConferenceDetails; ?>">
												</div>
											</div>
										</div>
										<button class="btn btn-primary" id="btnSave" type="button" name="btnSave"><?php echo ($id)?'Update':'Save'?></button>
										<?php
										if($id>0)
										{
											?>
											<div class="row mt-4">
												<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
													<h5>Note Details</h5>
												</div>
											</div>
											<?php 
											if(isset($_SESSION['error']['image']))
											{
												?>
												<span class="error text-danger"  ><?php echo $_SESSION['error']['image']; ?></span>
												<?php
												unset($_SESSION['error']['image']);
											}
											?>
											<div class="DivNoteDetails">
												<div class="row MainNoteRow">
													<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
														<div class="form-group">
															<label>Note File</label>
															<div id="file-uploader2" style="display:inline">
																<noscript>
																	<p>Please enable JavaScript to use file uploader.</p>
																	<!-- or put a simple form for upload here -->
																</noscript>
															</div>
															<input type="hidden" name="file" id="file" />
														</div>
													</div>
													<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
														<div class="form-group">
															<label>Description</label>
															<textarea class="form-control" id="NoteDescription"></textarea>
														</div>
													</div>
													<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
														<label style="width: 100%;">&nbsp;</label>
														<button class="btn btn-danger" id="btnSaveNoteRow" type="button">Add Note</button>
													</div>
												</div>
											</div>
											<div class="row mt-4">
												<div class="col-lg-12">
													<div class="table-responsive">
														<table class="table table-bordered table-striped">
															<thead>
																<tr>
																	<th scope="col">File</th>
																	<th scope="col">Note</th>
																	<th scope="col"></th>
																</tr>
															</thead>
															<tbody>
																<?php
																$sql_tmp_schd_file=mysqli_query($con,"select * from institute_schedule_files where schd_id=$id");
																while ($schfiledrow=mysqli_fetch_array($sql_tmp_schd_file)) 
																{
																	?>
																	<tr>
																		<td>
																			<a target="_blank" href="<?php echo SECURE_PATH;?>scheduler_exam_files/<?php echo $schfiledrow['files']; ?>"><?php echo $schfiledrow['files']; ?></a>
																		</td>
																		<td><?php echo $schfiledrow['description']; ?></td>
																		<td>
																			<button class="btn btn-danger btn-sm btnDeleteNote" data-id="<?php echo $schfiledrow['id'];?>" type="button"><i class="fa fa-trash"></i></button>
																		</td>
																	</tr>
																	<?php
																}
																?>
															</tbody>
														</table>
													</div>
												</div>
											</div>
											<?php
										}
										?>
									</div>
								</div>
							</div>
							<div id="menu1" class="container tab-pane active">
								<br>
								<div class="col-lg-12 col-md-12">
									<div class="card border-0" id="ggg">

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	$(".select2").select2();
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#Subject_Id').on('change', function() {
			var Subject_Id = this.value;
			$.ajax({
				url: "<?php echo SECURE_PATH;?>institute_schedule/fetch_data.php",
				type: "GET",
				data: {
					Subject_Id: Subject_Id,
					Fetch:"Chapter"
				},
				cache: false,
				success: function(res){
					if(res!="-100")
					{
						$("#Chapter_Id").html(res);
						<?php if($id>0) { ?>
							$('#Chapter_Id').val("<?php echo $Chapter_Id; ?>");
							$('#Chapter_Id').change();
						<?php } else { ?> 
							$("#Chapter_Id").val("0");
							$("#Chapter_Id").change();
						<?php } ?>
					}
				}
			});
		});
		$('#Chapter_Id').on('change', function() {
			var Chapter_Id = this.value;
			var Subject_Id=$("#Subject_Id").val();
			$.ajax({
				url: "<?php echo SECURE_PATH;?>institute_schedule/fetch_data.php",
				type: "GET",
				data: {
					Subject_Id: Subject_Id,
					Chapter_Id: Chapter_Id,
					Fetch:"Topic"
				},
				cache: false,
				success: function(res){
					if(res!="-100")
					{
						$("#Topic_Id").html(res);
						<?php if($id>0) { ?>
							$('#Topic_Id').val("<?php echo $Topic_Id; ?>");
						<?php } ?>
					}
				}
			});
		});
		<?php
		if($id>0)
		{
			?>
			$('#Subject_Id').val("<?php echo $Subject_Id; ?>");
			$('#Subject_Id').change();
			<?php
		}
		?>

		$('#btnSave').on('click', function() {
			var NewExam_Id=$("#Exam_Id").val().join(",");
			setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','saveform=1&id=<?php echo $id; ?>&txtDate='+$('#txtDate').val()+'&txtStartTime='+$('#txtStartTime').val()+'&txtEndTime='+$('#txtEndTime').val()+'&Exam_Id='+NewExam_Id+'&Subject_Id='+$('#Subject_Id').val()+'&Chapter_Id='+$('#Chapter_Id').val()+'&Topic_Id='+$('#Topic_Id').val()+'&txtRecordVideo='+$('#txtRecordVideo').val()+'&txtLive='+$('#txtLive').val()+'&txtConferenceDetails='+$('#txtConferenceDetails').val());
		});
		$('#btnSaveNoteRow').on('click', function() {
			setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','savenoteform=1&id=<?php echo $id; ?>&NoteFile='+$('#file').val()+'&NoteDescription='+$('#NoteDescription').val());
		});
		$('#btnAddNewSchedule').on('click', function() {
			setStateGet('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','addForm=1');
		});
		$(".btnDeleteNote").click(function() {
			var id = $(this).data("id");
			var data = 'id=' + id;
			var thistr=$(this).parents("tr");
			if (confirm("Are you sure delete this note detail ?")) {
				$.ajax({
					type : "POST",
					url : "<?php echo SECURE_PATH;?>institute_schedule/delete_schedule_note.php",
					data : data,
					success : function(res) {
						if(res=="100")
						{
							thistr.animate("fast").animate({ opacity : "hide" }, "slow");
						}
					}
				});
			}
			return false;
		});
	});
	var uploader = new qq.FileUploader({
		element: document.getElementById('file-uploader2'),
		action: '<?php echo SECURE_PATH;?>frame/js/upload/institute_upload.php?upload=file&upload_type=single',
		debug: true,
		multiple:false
	});
</script>
<?php
if(isset($_POST['saveform']))
{
	$txtDate=Date("Y-m-d",strtotime($_POST['txtDate']));
	$txtStartTime=$_POST['txtStartTime'];
	$txtEndTime=$_POST['txtEndTime'];
	$Exam_Id=$_POST['Exam_Id'];
	$Subject_Id=$_POST['Subject_Id'];
	$Chapter_Id=$_POST['Chapter_Id'];
	$Topic_Id=$_POST['Topic_Id'];
	$txtRecordVideo=$_POST['txtRecordVideo'];
	$txtLive=$_POST['txtLive'];
	$txtConferenceDetails=$_POST['txtConferenceDetails'];
	$timestamp=time();
	$id = (int)$_POST['id'];
	
	if($id>0)
	{
		$sql = "UPDATE `institute_schedule` SET `date`='$txtDate', `starttime`='$txtStartTime', `endtime`='$txtEndTime', `exam_id`='$Exam_Id', `subject`=$Subject_Id, `chapter`=$Chapter_Id, `topic`=$Topic_Id, `recorded_video`='$txtRecordVideo', `live_video`='$txtLive', `conference_details`='$txtConferenceDetails' WHERE id=$id";
		$st_reg=mysqli_query($con,$sql);
			
		if($st_reg)
		{
			$_SESSION['SuccessMsg']="Institute schedule data save successfilly...";
			?>
			<script type="text/javascript">
				setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $id; ?>');
			</script>
			<?php
		}
		else
		{
			$_SESSION['ErrorMsg']="Unable to save Institute schedule data...";
			?>
			<script type="text/javascript">
			alert("Unable to Data Save");
				setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $id; ?>');
			</script>
			<?php
		}
	}
	else
	{
		$st_reg=mysqli_query($con,"INSERT INTO `institute_schedule`(`institute_id`, `date`, `starttime`, `endtime`, `exam_id`, `subject`, `chapter`, `topic`, `recorded_video`, `live_video`, `conference_details`, `estatus`, `timestamp`) VALUES (2552,'$txtDate','$txtStartTime','$txtEndTime','$Exam_Id',$Subject_Id,$Chapter_Id,$Topic_Id,'$txtRecordVideo','$txtLive','$txtConferenceDetails',1,'$timestamp')");
		if($st_reg)
		{
			$reg_id=mysqli_insert_id($con);
			$_SESSION['SuccessMsg']="Institute schedule data save successfilly...";
			?>
			<script type="text/javascript">
				setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $reg_id; ?>');
			</script>
			<?php
		}
		else
		{
			$_SESSION['ErrorMsg']="Unable to save Institute schedule data...";
			?>
			<script type="text/javascript">
				setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $id; ?>');
			</script>
			<?php
		}
	}
}
if(isset($_POST['savenoteform']))
{
	$reg_id=$_POST['id'];
	$NoteFile=$_POST['NoteFile'];
	$NoteDescription=$_POST['NoteDescription'];
	$timestamp=time();

	$st_reg_file=mysqli_query($con,"INSERT INTO `institute_schedule_files`(`schd_id`, `files`, `description`, `estatus`, `timestamp`) VALUES ($reg_id,'$NoteFile','$NoteDescription',1,'$timestamp')");
	if($st_reg_file)
	{
		$_SESSION['SuccessMsg']="Institute schedule note save successfilly...";
		?>
		<script type="text/javascript">
			setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $reg_id; ?>');
		</script>
		<?php
	}
	else
	{
		$_SESSION['ErrorMsg']="Unable to save Institute schedule note...";
		?>
		<script type="text/javascript">
			setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $reg_id; ?>');
		</script>
		<?php
	}
}
?>