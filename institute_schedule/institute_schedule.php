<?php
include('../include/db.php');
include('../include/session.php');
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
?>
<?php
if(isset($_SESSION['SuccessMsg']))
{
	?>
	<div class="alert alert-success">
		<strong>Success!</strong> <?php echo $_SESSION['SuccessMsg']; ?>
	</div>
	<?php
	unset($_SESSION['SuccessMsg']);
}
?>
<?php
if(isset($_SESSION['ErrorMsg']))
{
	?>
	<div class="alert alert-danger">
		<strong>Success!</strong> <?php echo $_SESSION['ErrorMsg']; ?>
	</div>
	<?php
	unset($_SESSION['ErrorMsg']);
}
?>
<div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-12">
			<div class="table-responsive">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th scope="col">Institute</th>
							<th scope="col">Date</th>
							<th scope="col">Start Time</th>
							<th scope="col">End Time</th>
							<th scope="col">Exam</th>
							<th scope="col">Subject</th>
							<th scope="col">Chapter</th>
							<th scope="col">Topic</th>
							<th scope="col"></th>
							<th scope="col">ID</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql_tmp_schd=mysqli_query($con,"select ischd.id,ischd.institute_id,ischd.date,ischd.starttime,ischd.endtime,ischd.exam_id,sub.subject,chap.chapter,tp.topic from institute_schedule as ischd inner join subject as sub on sub.id=ischd.subject inner join chapter as chap on chap.id=ischd.chapter inner join topic as tp on tp.id=ischd.topic where ischd.estatus=1 order by ischd.id");
						while ($schdrow=mysqli_fetch_array($sql_tmp_schd)) 
						{
							?>
							<tr>
								<td><?php echo $schdrow['institute_id']; ?></td>
								<td><?php echo Date('m-d-Y',strtotime($schdrow['date'])); ?></td>
								<td><?php echo $schdrow['starttime']; ?></td>
								<td><?php echo $schdrow['endtime']; ?></td>
								<td><?php echo $schdrow['exam_id']; ?></td>
								<td><?php echo $schdrow['subject']; ?></td>
								<td><?php echo $schdrow['chapter']; ?></td>
								<td><?php echo $schdrow['topic']; ?></td>
								<td>
								<a href="javascript:void(0)" onClick="setState('main-content','<?php echo SECURE_PATH;?>institute_schedule/index.php','editform=1&id=<?php echo $schdrow['id']; ?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>
									<button class="btn btn-danger btn-sm btnDelete" data-id="<?php echo $schdrow['id'];?>" type="button"><i class="fa fa-trash"></i></button>
								</td>
								<td><?php echo $schdrow['id']; ?></td>
							</tr>
							<?php
						}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript">
	$(".select2").select2();
</script>
<script type="text/javascript">
	$(".btnDelete").click(function() {
		var id = $(this).data("id");
		var data = 'id=' + id;
		var thistr=$(this).parents("tr");
		if (confirm("Are you sure delete this institute schedule ?")) {
			$.ajax({
				type : "POST",
				url : "<?php echo SECURE_PATH;?>institute_schedule/delete_schedule.php",
				data : data,
				success : function(res) {
					if(res=="100")
					{
						thistr.animate("fast").animate({ opacity : "hide" }, "slow");
					}
				}
			});
		}
		return false;
	});
</script>