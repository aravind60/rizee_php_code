<style type="text/css">
.btn-group-sm>.btn, .btn-sm {
    padding: .2rem .2rem;}
    i.fa.fa-key.black {
    padding: 0.3rem 0.1rem;
}
    </style>
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

?>
<script>
$(document).ready(function(){
  //alert("sdfgh"+userlevelval);
  $('#userlevel').change(function(){
    var userlevelval = $("#userlevel option:selected").val();
    
    if(userlevelval == 3 || userlevelval == 2 || userlevelval == 1){
    $('.department').show();
    $('.designation').show();
    
  }
  else{
    $('.department').hide();
    $('.designation').hide();
  }
  });
  
});
</script>

<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM institute_admins WHERE username = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 ?>
<style>
  .spinner-border.w_1 {
    width: 1rem;height: 1rem;
  }
</style>
<script>
getFields5();
getFields6();
getFields1();
$('#subject').selectpicker2();
$('#chapter').selectpicker1();
</script>
<script>
chapterfunction();
//radiofunction('<?php echo $_POST['ppaper']; ?>');
function chapterfunction(){
	var chapter=$('#chapter').val();
	//alert(chapter);
	if(chapter.length >0){
		$('#chapview').show();
	}else{
		$('#chapview').hide();
	}
}
</script>
<script type="text/javascript">
	 $('#institute_name').autocomplete({
    source:"<?php echo SECURE_PATH;?>instituteusers/autoComplete.php",
    minLength: 1
  })
	 $("#institute_name").result(function(event, data) {
    $("#institute_address").val(data[1]);
  });
</script>
<?php
		if(isset($_POST['editform'])){
			if($_POST['editform']!=''){
				$style="style='display:none;'";
				$disa="disabled";
				$read="readonly";
			}else{
			}
		}else{
			$style="";
			$read="";
		}
			?>
 <div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">Institute Name<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				  <!-- <input type="text" name="institute_name" class="form-control institute_name" placeholder="Enter Institute Name" id="institute_name" value="<?php if(isset($_POST['institute_name'])){ echo $_POST['institute_name'];}?>" autocomplete="off"> -->
				  <select name="institution_id" class="form-control" id="institution_id" >
					<option value='' >-Select-</option>
						<?php
					  $sql=$database->query("select * from institute where estatus='1' ");
				  		while($row=mysqli_fetch_array($sql)){
						?>
							<option value="<?php echo $row['id'];?>" <?php if(isset($_POST['institution_id'])) { if($_POST['institution_id']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($row['institute_name']);?></option>
						<?php
						}
						?>

				  </select>
				  <span class="text-danger"><?php if(isset($_SESSION['error']['institution_id'])){ echo $_SESSION['error']['institution_id'];}?></span>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">Full Name<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				  <input type="text" name="name" class="form-control" placeholder="Enter Full Name" id="name" value="<?php if(isset($_POST['name'])){ echo $_POST['name'];}?>" autocomplete="off">
				  <span class="text-danger"><?php if(isset($_SESSION['error']['name'])){ echo $_SESSION['error']['name'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
	
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Username<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="username" class="form-control" placeholder="Enter Username" id="username" value="<?php if(isset($_POST['username'])){ echo $_POST['username'];}?>" autocomplete="off"  <?php echo $read; ?>>
					<span class="text-danger"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
				</div>
			</div>
		</div>
		
				<div class="col-lg-6" <?php echo $style; ?> >
				
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Password<span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<input type="password" name="password" class="form-control" placeholder="Enter Password" id="password" value="<?php if(isset($_POST['password'])){ echo $_POST['password'];}else{ }?>" autocomplete="off" <?php echo $disa; ?>>
							<span class="text-danger"><?php if(isset($_SESSION['error']['password'])){ echo $_SESSION['error']['password'];}?></span>
						</div>
					</div>
				
				</div>
			
		
		<div class="col-lg-6">
			<div class="form-group row">
          <label class="col-sm-4 col-form-label">Email<span style="color:red;">*</span></label>
          <div class="col-sm-8">
              <input type="text" name="email" class="form-control" placeholder="Enter Email" id="email" value="<?php if(isset($_POST['email'])){ echo $_POST['email'];}?>" autocomplete="off">
              <span class="text-danger"><?php if(isset($_SESSION['error']['email'])){ echo $_SESSION['error']['email'];}?></span>
          </div>
        </div>

		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Mobile No.<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="mobile" class="form-control" placeholder="10-digit Mobile number" id="mobile" onkeypress="return isNumber(event,$(this),10)"  maxlength="10" value="<?php if(isset($_POST['mobile'])){echo $_POST['mobile'];}?>" autocomplete="off">
					<span class="text-danger"><?php if(isset($_SESSION['error']['mobile'])){ echo $_SESSION['error']['mobile'];}?></span>
				</div>
			</div>
		</div>
		
		
		
	
    <div class="form-group row pl-3">
		
        <div class="col-lg-6">
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminForm','<?php echo SECURE_PATH;?>instituteusers/process.php','validateForm=1&name='+$('#name').val()+'&username='+$('#username').val()+'&password='+$('#password').val()+'&email='+$('#email').val()+'&mobile='+$('#mobile').val()+'&institution_id='+$('#institution_id').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$_POST['page'];}?>')">Submit</a>
        </div>
       

       
    </div>
</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}


//Process and Validate POST data
if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	$mobile = $post['mobile'];

  $name = $post['name'];
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  $field = 'name';
	if(!$post['name'] || strlen(trim($post['name'])) == 0){
	  $_SESSION['error'][$field] = "* Fullname name cannot be empty";
	}

	 $field = 'institution_id';
	if(!$post['institution_id'] || strlen(trim($post['institution_id'])) == 0){
	  $_SESSION['error'][$field] = "* Institute  cannot be empty";
	}
	
	$sqlli=$database->query("select * from institute_admins where valid='1' and username='".$post['username']."'");
	$rowc=mysqli_num_rows($sqlli);
	
	
  $field = 'username';
	if(!$post['username'] || strlen(trim($post['username'])) == 0){
	  $_SESSION['error'][$field] = "* Username name cannot be empty";
	   
  }else if(!isset($post['editform'])){
	 if($rowc>0){
		 $_SESSION['error'][$field] =  "* Username already Exists";
	 }
  }
	 
   
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  /* mobile number check */
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }


	$field = 'email';
	$email = $post['email'];
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}

  $field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	else if(strlen(trim($post['password'])) < 4){
      $_SESSION['error'][$field] = "* Length of Password Very low. Use atleast 4 Characters";

  }

	
	/*if($post['student_app']!=''){
		$student_app=$post['student_app'];
	}else{
		$student_app=0;
	}
	if($post['auth_rules']!=''){
		$auth_rules=$post['auth_rules'];
	}else{
		$auth_rules=0;
	}*/
  // $field = 'department';
	// if(!$post['department'] || strlen(trim($post['department'])) == 0){
	//   $_SESSION['error'][$field] = "* Department cannot be empty";
	// }
  
  if(strlen(trim($name)) == 0){
	  $name = $post['username'];
  }

  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>instituteusers/process.php','addForm=1&name=<?php echo $post['name'];?>&username=<?php echo $post['username'];?>&password=<?php echo $post['password'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&institution_id=<?php echo $post['institution_id'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$post['from_page'];}?>')
    </script>
  <?php
	}
	else{
		if($id=='NULL')
		{
			
			$database->query("INSERT institute_admins SET username='".$post['username']."',password='".md5($post['password'])."',userid='1',email='".$email."',mobile='".$mobile."',valid='1',userlevel='9',name='".$post['name']."',institution_id='".$post['institution_id']."',hash='1',hash_generated='1',student_app='0',auth_rules='0',timestamp='".time()."'");
	 
	 		$lid = mysqli_insert_id($database->connection);
			if($lid!="" && $lid!=0 && $lid!=NULL){

				$database->query("INSERT institute_users SET username='".$post['username']."',password='".$post['password']."',userid='1',email='".$email."',mobile='".$mobile."',valid='1',userlevel='9',name='".$post['name']."',institution_id='".$post['institution_id']."',hash='1',hash_generated='1',timestamp='".time()."'");
			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> User information saved successfully!
					</div>
				</div>
			</div>
			 <?php
			}else{
				 ?>
				  <div class="col-lg-12 col-md-12">
					<div class="form-group">
						<div class="alert alert-success">
						<i class="fa fa-thumbs-up fa-2x"></i> User information Insertion Failed!
						</div>
					</div>
				</div>
				 <?php
			}
		
		}else
		{
			
			  mysqli_query($database->connection,"update institute_admins SET username='".$post['username']."',email='".$email."',mobile='".$mobile."',valid='1',name='".$post['name']."',institution_id='".$post['institution_id']."',userlevel='9',etimestamp='".time()."' where username='".$id."'");
			  ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> User information Updated successfully!
					</div>
				</div>
			</div>
			  <?php
		}
  ?>
 
  
 <script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>instituteusers/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
  	
 
	<?php
  }
}
//Delete users
if(isset($_GET['rowDelete'])){
	mysqli_query($database->connection,"DELETE FROM institute_admins  where username='".$_GET['rowDelete']."'");
	mysqli_query($database->connection,"DELETE FROM institute_users  where username='".$_GET['rowDelete']."'");
	mysqli_query($database->connection,"insert deleted_users set username='".$_GET['rowDelete']."',uid='".$_GET['uid']."',timestamp='".time()."',estatus='1'");
?>
<div class="alert alert-success">Username deleted successfully!</div>
<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>instituteusers/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
<?php
}

if(isset($_REQUEST['validateForm1'])){
	if($_REQUEST['resetid']!=''){
		$result=$database->query('update institute_admins set password="'.md5($_REQUEST['password']).'" where username="'.$_REQUEST['resetid'].'"');
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Password Updated Successfully!</div>
			
			<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>instituteusers/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
	<?php
	}
}
//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'institute_admins';
  $condition = "valid=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  // $pagination = $session->showPagination(SECURE_PATH."instituteusers/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Institute Admin Details</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered"  width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Username (Full Name)</th>
											  <th class="text-left" nowrap>Full Name</th>
											  <th class="text-left" nowrap>Institute Name</th>
											  <th class="text-left" nowrap>Email</th>
											  <th class="text-left" nowrap>Mobile</th>
											  <th class="text-left" nowrap>Actions</th>
											</tr>
									</thead>
									<tbody>
									<?php
									
                  
									while($value = mysqli_fetch_array($data_sel))
									{
										  if($value['userlevel']=='7'){
											  $userlevel="Data Entry";
										  }else if($value['userlevel']=='8'){
											  $userlevel="Lecture";
										  }else if($value['userlevel']=='9'){
											  $userlevel="Admin";
										  }else{
										  }
										
									?>
									<tr>
										<td class="text-left"><?php echo $value['username'];?></td>
										<td class="text-left"><?php echo ucwords($value['name']);?></td>
										<td class="text-left"><?php echo $database->get_name('institute','id',$value['institution_id'],'institute_name'); ?> </td>
										<td class="text-left"><?php echo $value['email'];?></td>
										<td class="text-left"><?php echo '+91'.ucwords($value['mobile']);?></td>
										<td>
													<div id="dummy"></div>
										  <a href="#" class="btn btn-sm btn-primary" onClick="setState('adminForm','<?php echo SECURE_PATH;?>instituteusers/process.php','addForm=2&editform=<?php echo $value['username'];?>&page=<?php echo $_GET['page']; ?>')"><i
												  class="material-icons md-16 pt-1">edit</i></a>
										  <a href="#" class="btn btn-sm btn-danger" onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>instituteusers/process.php','rowDelete=<?php echo $value['username'];?>&uid=<?php echo $value['id']; ?>')"><i
												  class="material-icons md-16 pt-1">delete</i></a>
										
										</td>
									  </tr>
									<?php
									$i++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						<!-- <div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
						<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
						<?php echo $pagination ;?></div> -->
					</div>
				</div>
			</div>
		</div>

  </section>
 
	<?php
	}
	else{
	?>
		
  <?php
	}
	?>
<script type="text/javascript">
  $('#dataTable').DataTable({
    "pageLength": 50,
   
    
  });
   
</script>
<?php }
?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
		
			<label class="col-lg-4 col-form-label">Chapter<span style="color:red;">*</span></label>
			<div class="col-lg-6 ml-5">
				<select name="chapter" class="selectpicker1" id="chapter" multiple data-live-search="true"  onchange="chapterfunction();setStateGet('chapview','<?php echo SECURE_PATH;?>instituteusers/process.php','getchaptercview=1&chapter='+$('#chapter').val()+'')">
					<?php
					//echo "SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').") ";
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from institute_users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					  ?>
					<option value="<?php echo $data1['id'];?>"  <?php if(isset($_REQUEST['chapter'])) {if(in_array($data1['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?> ><?php echo $data1['chapter'];?></option>
					<?php
					 $i++;
					}
					?>
				</select>
				
				<span class="text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
			</div>
		   
		
    <?php
}?>
<?php
if(isset($_REQUEST['getchaptercview']))
{	
	
	
	?>
		
					<?php
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1'    AND id IN(".rtrim($_REQUEST['chapter'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from institute_users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					 
						  $count=$count+$rowdcount;
					 $i++;
					}
					
					?>
					<a href='#'  style="cursor:pointer;color: #007bff;" title="Print"  data-toggle="modal" data-target="#messageDetails1" onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>instituteusers/process1.php','viewDetails1=1&chapter=<?php echo $_REQUEST['chapter']; ?>');"><?php echo $count; ?> Users</a>
		
    <?php
}?>
		<?php
	if(isset($_REQUEST['getexamset']))
	{	
		$_REQUEST['qset']=explode(",",$_REQUEST['qset']);
	?>
		<label for="inputTopi" class="label">Set ID</label>
		<select class="form-control selectpicker4" multiple name="qset" value=""  id="qset"  >
			<option value=''>-- Select --</option>
			<?php
			
			$sel=$database->query("select * from previous_questions where estatus='1' and year in (".$_POST['year'].")  ORDER by id DESC");
			while($row=mysqli_fetch_array($sel)){
				$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
				while($row1=mysqli_fetch_array($sel1)){
				?>
					
					<option value="<?php echo $row1['id'];?>"  <?php if(isset($_REQUEST['qset'])) {if(in_array($row1['id'], $_REQUEST['qset'])) { echo 'selected="selected"'; } } ?> ><?php echo $row1['qset'];?></option>
			<?php
				}
			}
			?>
	</select>
    <?php
}?>

<script>
function selectfunction(){
	
	var userlevel=$('#userlevel').val();
	var ppaper=$('#ppaper').val();
	if(userlevel=='6' || userlevel=='7' || userlevel=='8'){
		$('.subjectdiv').show();
		if(userlevel=='7'){
			$('.contentd').show();
			$('.contentd1').show();
			$('.previous1').show();
			$('.update_chapter1').hide();
			$('.question_wchapter1').hide();
			$('.previous2').hide();
		}else if(userlevel=='8'){
		$('.contentd').show();
		$('.contentd1').hide();
		$('.previous1').show();
		$('.update_chapter1').show();
		$('.question_wchapter1').show();
			if(ppaper=='1'){
				$('.previous2').show();
			}else{
				$('.previous2').hide();
			}
		
		}else{
			$('.contentd').hide();
			$('.previous1').hide();
			$('.previous2').hide();
			$('.contentd1').hide();
			$('.update_chapter1').hide();
			$('.question_wchapter1').hide();
		}

	}else{
		$('.subjectdiv').hide();
        $('.contentd').hide();
		$('.previous1').hide();
		$('.previous2').hide();
		$('.question_wchapter').hide();
		

	}
}
</script>

	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
<script type="text/javascript">
	function getFields5()
    {
    	
    var ass='';
    $('.classn').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
      ass+=$(this).val()+",";
	 
    }else{
      $('#'+$(this).attr('id')).val(0);
      ass+=$(this).val()+",";
	 
    }
//alert(ass);

  });
        
        

    
}
	function getFields6()
    {
    	var userlevel=$('#userlevel').val();
    $('.ppaper').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 if(userlevel=='8'){
		$('.previous2').show();
	 }else{
		$('.previous2').hide();
	 }

    }else{
      $('#'+$(this).attr('id')).val(0);
     if(userlevel=='8'){
		$('.previous2').hide();
	  }else{
		$('.previous2').hide();
	 }
    }
//alert(ass);

  });
        
        

    
}
	
	function getFields8()
    {
		$('.student_app').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields9()
    {
		$('.auth_rules').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
    </script>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>