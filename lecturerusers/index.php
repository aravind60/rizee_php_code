<?php
    error_reporting(0);
    include('../include/session.php');

	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
        //unset($_SESSION['image']);  
    ?>
    
   
		
		
		
		
		<div class="content-wrapper">
<!--             <section class="breadcrumbs-area2 my-3">
				<div class="container">
					<div class="d-flex justify-content-between align-items-center">
						<div class="title">
							<h1 class="text-uppercase">Users</h1>
						</div>
					</div>
					<hr>
				</div>
			</section>  -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered" role="document">
					<div class="modal-content">
						<div class="modal-header">

							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body reject-modal text-left" id="rowReject" >
							
						</div>
						
					</div>
				</div>
			</div>
				<div class="modal fade " id="messageDetails1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Users assigned by Chapters</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails1">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
							<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
						</div>
						</div>
					</div>
				</div>
			<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Timeline Overview</h5>
						<button type="button"  onClick="$('#viewDetails').html('')" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="viewDetails">
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary"  onClick="$('#viewDetails').html('')" data-dismiss="modal">Close</button>
						<!-- <button type="button" class="btn btn-primary">Save changes</button> -->
					</div>
					</div>
				</div>
			</div>
			
          <section class="content-area" id="start_div" style="display: none;">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="card border-0 shadow mb-4">
                                        <div
                                            class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                                            <h6 class="m-0 font-weight-bold text-primary">Add User</h6>
                                        </div>
                                        <div class="card-body">
                                            <form>
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12">
                                                        <div class="form-group row">
                                                            <div class="col-sm-8">
                                                              
                                                            </div>
                                                        </div>
                                                        <div class="form-group row" id="adminForm">
                                                            <div class="col-sm-8">
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

       
			<section>
                <div class="container">
                    
                            <div class="form-group m-0" id="adminForm2">
								<div class="col-sm-8">
									<script type="text/javascript">
										setStateGet('adminForm2','<?php echo SECURE_PATH;?>lecturerusers/process.php','addForm=1');
									</script>
								</div>
							</div>
                      
                </div>
            </section>
			 <section id="datafill" class="container">

				<div id="adminTable">
					<script type="text/javascript">
					setStateGet('adminTable','<?php echo SECURE_PATH;?>lecturerusers/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
					</script>
				</div>
			</section>
		</div>
		<a class="scroll-to-top rounded" href="#page-top">
                <i class="material-icons">navigation</i>
            </a>
		
	<?php
		
		}
	?>
	
