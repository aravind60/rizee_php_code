<?php
include('../include/session.php');
error_reporting(0);
?>

<script src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>
<?php

if(isset($_REQUEST['viewDetails']))
{
	$sello=$database->query("select * from users");
	$rowese=mysqli_fetch_array($sello);
	if($_REQUEST['details']=='next'){
		$id=$_REQUEST['id']+1;
	}else if($_REQUEST['details']=='previous'){
		$id=$_REQUEST['id']-1;
	}else{
		$id=$_REQUEST['id'];
	}
	
	$sell=$database->query("select * from createquestion where estatus='1' and id='".$id."'");
	$rowcount=mysqli_num_rows($sell);
	if($rowcount>0){
		$row=mysqli_fetch_array($sell);
		//$selldata=$database->query("select * from questionset where estatus='1' and question_id='".$_REQUEST['id']."'");
		//$rowdata=mysqli_fetch_array($selldata);
		if($row['list1type']=='roman'){
			$list1type="upper-roman";
		}else if($row['list1type']=='alphabets'){
			$list1type="upper-alpha";
		}else if($row['list1type']=='numbers'){
			$list1type="decimal";
		}else{
			$list1type="upper-alpha";
		}
		if($row['list2type']=='roman'){
			$list2type="upper-roman";
		}else if($row['list2type']=='alphabets'){
			$list2type="upper-alpha";
		}else if($row['list2type']=='numbers'){
			$list2type="decimal";
		}else{
			$list2type="upper-alpha";
		}
		?>
			<div class="container">
				<div class="row">
					<?php
					$row['exam']=rtrim($row['exam'],',');
					$row['class']=rtrim($row['class'],',');
					$row['chapter']=rtrim($row['chapter'],',');
					$row['topic']=rtrim($row['topic'],',');
					
					if($row['class']=='1'){
						$class='XI';
					}else if($row['class']=='2'){
						$class='XII';
					}else  if($row['class']=='1,2'){
						$class='XI,XII';
					}
					if($row['exam']=='1'){
						$exam="NEET";
					}else if($row['exam']=='2'){
						$exam="JEE";
					}else if($row['exam']=='1,2'){
						$exam="NEET,JEE";
					}
					$chapter ='';
					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
					while($row2=mysqli_fetch_array($sql)){
						$chapter .= $row2['chapter'].",";
					}

						$topic ='';
						$k=1;
						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
						while($row1=mysqli_fetch_array($zSql1)){
							$topic .= $row1['topic'].",";
							$k++;
						}
						?>
					<div class="col-md-2">
						<h6>Class</h6>
						<p><?php echo $class; ?></p>
					</div>
					
					<div class="col-md-2">
						<h6>Exam</h6>
						<p><?php echo $exam; ?></p>
					</div>
					<div class="col-md-2">
						<h6>Subject</h6>
						<p><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></p>
					</div>
					<div class="col-md-3">
						<h6>Chapter</h6>
						<p><?php echo rtrim($chapter,","); ?></p>
					</div>
					
				</div>
				<div class="row ">
					<div class="col-md-12 pt-2 px-3">
							<h6>Question ID : <?php echo $row['id']; ?></h6>
						
						</div>
					<div class="col-md-12 pt-2 px-3">
						<h6>Question Created By : <?php echo $row['username']; ?></h6>
					
					</div>
					<?php
					if($row['qtype']=='5'){
						$type="Comprehension";
					?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Explanation</h6>
							<div><?php echo urldecode($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo urldecode($row['question']); ?></div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A)</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option1']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option2']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option3']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option4']); ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else if($row['qtype']=='8'){
						$type="Integer";
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo urldecode($row['question']); ?></div>
						</div>
						
						
						<div class="col-md-12 mt-2 questionView">
							<h6>answer</h6>
							<div><?php echo rtrim($row['answer'],","); ?></div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
					<?php
					}else if($row['qtype']=='3'){
						$type="Matching";
					
						$obj=json_decode($row['question'],true);
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<?php echo urldecode($row['mat_question']); ?>
							<br />
							<div class="questionlist-types d-flex">
								<div>
								<h5>List1</h5>
									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
										<?php
										foreach($obj as $qqlist)
										{
											if(strlen($qqlist['qlist1'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
											}
										}
										?>
									
									</ul>
								</div>
								<div class="pl-5">
									<h5>List2</h5>
									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
										<?php
										foreach($obj as $qqlist1)
										{
											if(strlen($qqlist1['qlist2'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist1['qlist2']).'</li>';
											}
										}
										?>
									</ol>
								</div>
							</div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option4']; ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
						
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else if($row['qtype']=='9'){
						$type="Matrix";
						$list1='';
						$list2='';
						$obj=json_decode($row['question'],true);
						
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<?php echo urldecode($row['mat_question']); ?>
							<br />
								<div class="questionlist-types d-flex">
									<div>
									<h5>List1</h5>
										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
											<?php
										foreach($obj as $qqlist)
										{
											if(strlen($qqlist['qlist1'])>0)
											{
												echo '<li style="width:150px;">'.urldecode($qqlist['qlist1']).'</li>';
											}
										}
										?>
										
										</ul>
									</div>
									<div class="pl-5">
										<h5>List2</h5>
										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
											<?php
											foreach($obj as $qqlist1)
											{
												if(strlen($qqlist1['qlist2'])>0)
												{
													echo '<li style="width:150px;">'.urldecode($qqlist1['qlist2']).'</li>';
												}
											}
											?>
										</ol>
									</div>
								</div>
							
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $row['option4']; ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
						<?php if($row['expla_vlink']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation Video Link</h6>
								<div><?php echo $row['expla_vlink']; ?></div>
									

							</div>
						<?php } ?>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}else{
						$type="General";
					?>
						<div class="col-md-3">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							<div><?php echo urldecode($row['question']); ?></div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option1']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option2']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option3']); ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo urldecode($row['option4']); ?>
								</div>
							</div>
						</div>
						<?php if($row['explanation']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation</h6>
								<div><?php echo urldecode($row['explanation']); ?></div>
									

							</div>
						<?php } ?>
						<?php if($row['expla_vlink']!=''){ ?>
							<div class="col-md-12 mt-2">
								<h6>Explanation Video Link</h6>
								<div><?php echo $row['expla_vlink']; ?></div>
									

							</div>
						<?php } ?>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($row['answer'],","); ?></p>
								

						</div>
					<?php
					}
					?>
					<?php if($row['complexity']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Complexity</h6>
							<p><?php echo $database->get_name('complexity','id',$row['complexity'],'complexity'); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['timeduration']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Answering Time Duration</h6>
							<p><?php echo $row['timeduration']; ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['inputquestion']!=''){
					$zSql2 = $database->query("SELECT * FROM questiontype WHERE estatus='1' and id IN(".$row['inputquestion'].")"); 
					while($row2=mysqli_fetch_array($zSql2)){
						$questiontype .= $row2['questiontype'].",";
						$k++;
					}
					?>
						<div class="col-md-12 mt-2">
							<h6>Type of Question</h6>
							<p><?php echo rtrim($questiontype,","); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['usageset']!=''){ ?>
						<div class="col-md-12 mt-2">
							<h6>Usage Set</h6>
							<p><?php echo $database->get_name('question_useset','id',$row['usageset'],'usageset'); ?></p>
								

						</div>
					<?php } ?>
					<?php if($row['question_theory']!='' && $row['question_theory']!='0'){ ?>
						<div class="col-md-12 mt-2">
							<h6>Question Theory</h6>
							<p><?php echo $database->get_name('question_theory','id',$row['question_theory'],'question_theory'); ?></p>
								

						</div>
					<?php } ?>
					
				</div>
			</div>
			
			 <div class="modal-footer">
			<!--  <a  class="btn btn-primary text-white" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process3.php','viewDetails=1&id=<?php echo $row['id'];?>&details=next')">Next</a>
			 <a   class="btn btn-primary text-white" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process3.php','viewDetails=1&id=<?php echo $row['id'];?>&details=previous')">Previous</a> -->
			
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> 
			
			
		  </div>                                               
		
	<?php 
		}else{
			echo "No Results Found";

		}
	}

 ?>