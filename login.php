<?php
include('include/session.php');
$_SESSION['pagevalue'] = 10;
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="images/favicons/favicon-32x32.ico" sizes="32x32" type="image/x-icon">
    <link rel="icon" href="images/favicons/favicon-18x18.ico" sizes="16x16" type="image/x-icon">

    <?php echo $session->devasthanam_title();?>

    <!-- Bootstrap core CSS -->
    <?php echo $session->commonUserCSS();?>
</head>

<body>

    <?php
echo  $session->villageHeader();
?>

    <!-- Page Content -->
    <main class="my-wrapper" id="loginForm">
        <section class="login_area" id="textDiv">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5" style="margin:0px auto;">
                        <form onSubmit="return false;">
                            <div class="cardify login-form">
                                <div class="login--header">
                                    <h3>Sign in to Continue</h3>
                                    <p style="display:none;">You can sign in with your username</p>
                                </div>
                                <!-- end .login_header -->

                                <div class="login--form">
                                    <div class="form-group">
                                        <label for="user_name">Username</label>
                                        <input id="user" type="text" autocomplete="off" class="form-control" tabindex="1" placeholder="Enter your Username...">
                                    </div>

                                    <div class="form-group">
                                        <label for="pass">Password</label>
                                        <input id="pass" type="password" autocomplete="off" class="form-control" tabindex="2" placeholder="Enter your Password...">
                                    </div>
<div class="row form-group">
                                         <div class="col-sm-5">
                                            <label class="control-label" id="captchaOperation"><img id="captcha" src="captcha.php"></label>&nbsp;
                                             <a onClick="refreshCaptcha();"><i class="fa fa-refresh" aria-hidden="true"></i></a>
                                                 </div>
                                          <div class="col-sm-7">
                                           <input type="text" class="form-control" id="vercode" tabindex="3" placeholder="Enter Captcha Code" required>
                                                  </div>
                                               </div>


                                    <div class="form-group" >
                                     
                                    <button class="btn btn-danger btn-block" tabindex="4"  onClick="setState('loginForm','<?php echo SECURE_PATH;?>check_login.php','Login=1&user='+$('#user').val()+'&pass='+$('#pass').val()+'&vercode='+$('#vercode').val()+'')" type="submit">Login Now</button>
                                    
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </main>

 
 
    <?php echo $session->commonUserJS(); ?>

    <script type="text/javascript">
       function refreshCaptcha(){
 
                    $('#captchaOperation img').attr('src','captcha.php');
            
            
		 
       }
    </script>
</body>

</html>