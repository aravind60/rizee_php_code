<?php
ini_set('display_errors','On');


// If necessary, modify the path in the require statement below to refer to the 
// location of your Composer autoload.php file.
require 'vendor/autoload.php';

use Aws\Ses\SesClient;
use Aws\Exception\AwsException;

// Create an SesClient. Change the value of the region parameter if you're 
// using an AWS Region other than US West (Oregon). Change the value of the
// profile parameter if you want to use a profile in your credentials file
// other than the default.
$SesClient = new SesClient([
    'profile' => 'default',
    'version' => '2010-12-01',
    'region'  => 'ap-south-1'
]);

// Replace sender@example.com with your "From" address.
// This address must be verified with Amazon SES.
$sender_email = 'info@mylearningplus.in';

// Replace these sample addresses with the addresses of your recipients. If
// your account is still in the sandbox, these addresses must be verified.
$recipient_emails = [$_REQUEST['email']];

// Specify a configuration set. If you do not want to use a configuration
// set, comment the following variable, and the
// 'ConfigurationSetName' => $configuration_set argument below.
$configuration_set = 'ConfigSet';

$subject = 'RIZEE - OTP Verification';
$plaintext_body = 'Hi, Verify your account details by entering the OTP: '.$_REQUEST['otp'].' - RIZEE' ;
$html_body =  '
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><META http-equiv="Content-Type" content="text/html; charset=utf-8"></head><body>



  
  
  
  


<div marginwidth="0" marginheight="0">

  

  <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
    <tr>
      <td align="center" valign="middle">
        
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
          <tr>
            <td align="center" valign="middle">
              <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:5px 5px 0 0">
                    <table width="615" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="10" align="left" valign="top" style="line-height:10px;font-size:10px"> </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top">
                          <table width="600" border="0" align="left" cellpadding="0" cellspacing="0">
                            <tr>
                              <td align="center" valign="top">
                                <a href="#0.1_">

                                  
                                    
                                  

                                </a>
                              </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                      <tr>
                        <td height="10" align="left" valign="top" style="line-height:10px;font-size:10px"> </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        

        
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
          <tr>
            <td align="center" valign="middle">
              <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" valign="top" bgcolor="#FFFFFF">
                    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="25" align="center" valign="top" style="font-size:25px;line-height:25px"> 
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="middle" style="font-family:&#39;Open Sans&#39;,sans-serif,Verdana;font-size:18px;font-weight:600;color:#000;text-transform:capitalize">
                          Verification Code </td>
                      </tr>
                      <tr>
                        <td height="15" align="center" valign="top" style="line-height:15px;font-size:15px"> 
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:30px;font-weight:400;line-height:28px;color:#000">
                          '.$_REQUEST['otp'].'</td>
                      </tr>
                      <tr>
                        <td height="10" align="center" valign="top" style="font-size:15px;line-height:15px"> 
                        </td>
                      </tr>
                      
                      <tr>
                        <td height="15" align="center" valign="top" style="font-size:15px;line-height:15px"> 
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:14px;font-weight:normal;line-height:28px;color:#000">
                          Here is your OTP verification code.
                        </td>
                      </tr>
                      <tr>
                        <td height="15" align="center" valign="top" style="font-size:15px;line-height:15px"> 
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:14px;font-weight:normal;line-height:28px;color:#000">
                          Please use this password to verify your account. This OTP is valid for 24 Hrs only.
                        </td>
                      </tr>
                      <tr>
                        <td height="45" align="center" valign="top" style="font-size:45px;line-height:45px"> 
                        </td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:14px;font-weight:normal;line-height:28px;color:#000">
                          If you didn&#39;t request this password, Please <span style="color:#0094bc">change your
                            password</span><br>
                          to protect your account
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        

        
        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
          <tr>
            <td align="center" valign="middle">
              <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                <tr>
                  <td align="center" valign="top" bgcolor="#FFFFFF">
                    <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td height="15" align="center" valign="top" style="font-size:15px;line-height:15px"> 
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        


        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
          <tr>
            <td align="center" valign="middle">
              
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                  <td align="center" valign="middle">
                    <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" valign="top" bgcolor="#FFFFFF">
                          <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="20" align="center" valign="top" style="font-size:20px;line-height:20px">
                                 </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              

              
              <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                  <td align="center" valign="middle">
                    <table width="750" border="0" align="center" cellpadding="0" cellspacing="0">
                      <tr>
                        <td align="center" valign="top" style="border-top:1px solid #eff1f4"></td>
                      </tr>
                      <tr>
                        <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:0 0 5px 5px">
                          <table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                              <td height="20" align="center" valign="top"> </td>
                            </tr>
                            <tr>
                              <td align="center" valign="top" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:14px;font-weight:normal;line-height:28px;color:#000">
                                MyLearning Plus Syncs accross all your devices
                              </td>
                            </tr>
                            <tr>
                              <td height="15" align="center" valign="top"> </td>
                            </tr>
                            <tr>
                              <td align="center">
                                
      <a href="#0.1_" style="color:#333333;text-decoration:none">
                                        <img alt="googleplay-store" style="color:#333333;width:100%;line-height:0;border-radius:5px;line-height:0">
                                      </a>

                              </td>
                            </tr>
                            <tr>
                              <td height="15" align="center" valign="top"> </td>
                            </tr>
                            <tr>
                              <td align="center" valign="middle" style="font-family:&#39;Open Sans&#39;,sans-serif;font-size:14px;font-weight:normal;line-height:28px">
                                Sent By
                                <a href="http://rizee.in" style="margin-left:10px;margin-right:10px;color:#1a8cec;text-decoration:none">MyLearning
                                  Plus</a>
                                <a href="http://rizee.in/support" style="margin-left:10px;margin-right:10px;color:#1a8cec;text-decoration:none">Support
                                  Center</a>
                                <a href="http://rizee.in/privacy" style="margin-left:10px;margin-right:10px;color:#1a8cec;text-decoration:none">Privacy
                                  Policy</a>
                              </td>
                            </tr>
                            <tr>
                              <td height="20" align="left" valign="top"> </td>
                            </tr>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              
            </td>
          </tr>
        </table>

        

</td></tr></table></div>

</body></html>';
$char_set = 'UTF-8';

try {
    $result = $SesClient->sendEmail([
        'Destination' => [
            'ToAddresses' => $recipient_emails,
        ],
        'ReplyToAddresses' => [$sender_email],
        'Source' => $sender_email,
        'Message' => [
          'Body' => [
              'Html' => [
                  'Charset' => $char_set,
                  'Data' => $html_body,
              ],
              'Text' => [
                  'Charset' => $char_set,
                  'Data' => $plaintext_body,
              ],
          ],
          'Subject' => [
              'Charset' => $char_set,
              'Data' => $subject,
          ],
        ],
        // If you aren't using a configuration set, comment or delete the
        // following line
      //  'ConfigurationSetName' => $configuration_set,
    ]);
    $messageId = $result['MessageId'];
    echo("Email sent! Message ID: $messageId"."\n");
} catch (AwsException $e) {
    // output error message if fails
    echo $e->getMessage();
    echo("The email was not sent. Error message: ".$e->getAwsErrorMessage()."\n");
    echo "\n";
}