(function($) {

    "use strict";



    /*-------------------------------*/
    /* Menu hide/show on scroll
    /*-------------------------------*/

    var ost = 0;
    $(window).scroll(function() {

        var cOst = $(this).scrollTop();

        if (cOst > 150) {
            $('nav').addClass('theme-bg-gradient');
            $('nav').addClass('navbar-fixed');
            $('.nav-item').addClass('color-black-scroll');

        } else {
            $('nav').removeClass('theme-bg-gradient');
            $('nav').addClass('navbar-fixed');
            $('.nav-item ').removeClass('color-black-scroll');
        }

        if (cOst == 0) {
            $('.navbar').addClass('top-nav-collapse');
            $('.navbar').removeClass('navbar-fixed');

        } else if (cOst > ost) {
            $('.navbar').addClass('top-nav-collapse').removeClass('default');
            $('.navbar').removeClass('navbar-fixed');
        } else {
            $('.navbar').addClass('default').removeClass('top-nav-collapse');
            
        }
        ost = cOst;
    });



}(jQuery));