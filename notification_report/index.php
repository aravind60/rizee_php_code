<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    	//header('Location: '.SECURE_PATH);
    }
    else
    {
    ?>
		<style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}
			@media (max-width: 767px){
				.web-enable-buttons {
				    display: none !important;
				}
				.mobile-enable-buttons {
				    display: table-cell !important;
				}

				.footer-pagination {
				    display: block !important;
				}
				
				.pagination .disabled {
				    padding: 5px 0px;
				   margin-left: 0;
				}
				.pagination .current {
				    font-size: 11px;
				}
				.pagination a {
					padding: 2px;
				}
			}

		</style>
       <div class="toast text-center mx-auto" id="enableuser1"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"  style="position: absolute;
    top: 45%;left:0;right:0" >

		  <!-- Then put toasts within -->
		  <div  role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
			  <strong class="mr-auto">Question</strong>
			 
			  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="toast-body p-5">
			User Enabled Successfully
			</div>
		  </div>
		</div>

		 <div class="toast text-center mx-auto" id="disableuser1"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"  style="position: absolute;
    top: 45%;left:0;right:0" >

		  <!-- Then put toasts within -->
		  <div  role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
			  <strong class="mr-auto">Question</strong>
			 
			  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="toast-body p-5">
			User Disabled Successfully
			</div>
		  </div>
		</div>
			
				
				<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Sent To People</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						</div>
					</div>
				</div>
				<div class="modal fade " id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Received People</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails1">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						</div>
					</div>
				</div>
                   
                <div class="modal fade " id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Seen People</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails2">
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>
						</div>
					</div>
				</div>
                    <section>
                        <div id="adminTable">
                            <script type="text/javascript">
                                setStateGet('adminTable','<?php echo SECURE_PATH;?>notification_report/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
                            </script>
                        </div>
                    </section>
                 </div>
 <?php
    
    }
?>