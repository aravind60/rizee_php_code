<?php
ini_set('display_errors','0');
include('../include/session.php');
?>
<style>
	  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
  </style>
<?php
	if(isset($_REQUEST['tabledata'])){


		 $tableName = 'institute_notifications';
	//$date7days = strtotime(date('d-m-Y',strtotime("-30 days")));
  $condition = "estatus='1' and is_admin='1' ";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  $q = "SELECT count(id) as count FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = $database->query1($q);
  $numdata = mysqli_fetch_array($result_sel);
  $numres=$numdata['count'];
    $query = "SELECT id,class_ids,exam_ids,student_ids,title,description,timestamp FROM $tableName ".$condition."  ORDER BY timestamp DESC";
  
  $data_sel = $database->query1($query);
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						
						<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														
														<th class="text-left" nowrap>Date & Time</th>
														<th class="text-left" nowrap>Class</th>
														<th class="text-left" nowrap>Exam</th>
														<th class="text-left" nowrap>Title</th>
														<th class="text-left" nowrap>Description</th>
														<th class="text-left" nowrap>Users</th>
														<th class="text-left" nowrap>Options</th>
													</tr>
												</thead>
												<tbody>
												<?php
												$i=1;
												
													while($value=mysqli_fetch_array($data_sel)){
														$time=$value['timestamp'];
														$class='';
														$class1='';
														if(strlen($value['class_ids'])>0){
															$classsel=$database->query("select id,class from class where estatus='1' and id in (".$value['class_ids'].")");
																if(mysqli_num_rows($classsel)>0){
																	while($rowclass=mysqli_fetch_array($classsel)){
																		$class.=$rowclass['class'].",";
																	}
																	$class1=rtrim($class,",");
																}else{
																	$class1='';
																}
															
														}else{
															$class='';
															$class1='';
														}

														$exam='';
														$exam1='';
														if(strlen($value['exam_ids'])>0){
															$examsel=$database->query("select id,exam from exam where estatus='1' and id in (".$value['exam_ids'].")");
															while($rowclass=mysqli_fetch_array($examsel)){
																$exam.=$rowclass['exam'].",";
															}
															$exam1=rtrim($exam,",");
														}else{
															$exam1='';

														}
														

														
														?>
														<tr>

															<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y  H:i:s',$value['timestamp']);?></td>
															<td class="text-left"><?php echo $class1; ?></td>
															<td class="text-left"><?php echo $exam1; ?></td>
															<td class="text-left" width="30%"><?php echo $value['title']; ?></td>
															<td class="text-left" width="30%"><?php echo $value['description']; ?></td>
															<td><a data-toggle="modal" data-target="#exampleModal" class="" style="color:green;" onClick="setState('viewDetails','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','viewdetails=<?php echo $value['id'];?>')"><u>Users</u></a></td>
															<td>
																<a style="cursor:pointer;" data-toggle="modal" data-target="#exampleModal1"onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>notifyusers/prnt.php','viewdetails=1&id=<?php echo $value['id'];?>')"><i class="fa fa-eye" ></i></a>
															</td>	
													  </tr>
													<?php
													$i++;
													}
												
													
													?>
												
												</tbody>
											</table>
									
										</div>
					</div>
				</div>
			</div>
		</div>

  </section>
 	<script type="text/javascript">
  $('#dataTable').DataTable({
	"pageLength": 50,
	"order": [[ 0, "desc" ]],
   
    
  });
   
</script>
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}
	}
	?>