
<?php
ini_set('display_errors','0');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');

ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);

if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
$imagepath="https://admin.rizee.in/files/";
?>

<script>
	function appactionsdiv(){
		var app_actions=$('#app_actions').val();
		
		if(app_actions=='1'){
			
			$('.appactiondatadiv').show();
			$('.appactiondatadiv1').hide();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Learn More');
		}else if(app_actions=='2'){
			$('.appactiondatadiv').show();
			$('.appactiondatadiv1').hide();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Explore More');
		}else if(app_actions=='3'){
			$('.appactiondatadiv').hide();
			$('.appactiondatadiv1').show();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Explore More');
		}else{
			$('.appactiondatadiv').hide();
			$('.appactiondatadiv1').hide();
			$('.appactiondatadiv4').show();
			$('#button_name').val('Explore More');
		}

	}
		</script>
	<?php
//$con = mysqli_connect("localhost","root","","neetjee");
 /*
$con = mysqli_connect("localhost","rspace","Rsp@2019","rizee_development");
function query($sql){
global $con;
 return mysqli_query($con,$sql);
}*/
	if(isset($_REQUEST['addForm'])){
		
		
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}

			  .multiselect-item.multiselect-filter .input-group-btn{
	display:none;
  }
  .multiselect-native-select .multiselect.dropdown-toggle {
  	border: 1px solid #ced4da;
  }
		</style>
		<style>
		
		</style>
	
	<script>
		('#class_ids').selectpicker3();
		$('#exam_ids').selectpicker1();
		$('#username').selectpicker2();
		//$('#chapter').selectpicker();
		/*$('#username').multiselect({
			enableFiltering: true,
			includeSelectAllOption: true,
			maxHeight: 300,
			 buttonWidth: "100%",
			dropDown: true
		});*/
	 </script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
								
							<div class="card-header py-3">
								<ul class="nav nav-pills" role="tablist">
									<li class="nav-item">
									  <a class="nav-link active" data-toggle="pill" href="#home" onclick="setStateGet('adminForm','<?php echo SECURE_PATH;?>notifyusers/process.php','addForm=1')">Send New Notification - Now</a>
									</li>
									<li class="nav-item">
									  <a class="nav-link " data-toggle="pill" href="#menu1" onclick="setState('ggg','<?php echo SECURE_PATH;?>notifyusers/ajax.php','tabledata=1')">Sent Notification</a>
									</li>
									
								</ul>
							</div>
							<div class="card-body">
								<?php
									echo("Log Testing");
								?>
							
							
							<div class="tab-content">
								<div id="home" class="container tab-pane active"><br>
									
										<div class="col-lg-12 col-md-12">
												<?php
												$_REQUEST['class_ids']=explode(",",$_REQUEST['class_ids']);
												?>
												<div class="row">
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">User Selection Type</label>
															<div class="col-sm-8">
																
																<div class="form-check form-check-inline">
																		  <input class="form-check-input" type="radio" name="user_selection_type" id="user_selection_type1" value="1" <?php if(isset($_POST['user_selection_type'])) { if($_POST['user_selection_type']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>  onclick="user_selection_type('1')">
																		  <label class="form-check-label" for="user_selection_type1">Excel Upload</label>
																		</div>
																		<div class="form-check form-check-inline">
																		  <input class="form-check-input" type="radio" name="user_selection_type" id="user_selection_type2" value="2" <?php if(isset($_POST['user_selection_type'])) { if($_POST['user_selection_type']=='2') { echo 'checked'; }else { echo 'checked';}  }else { echo 'checked';} ?> onclick="user_selection_type('2')">
																		  <label class="form-check-label" for="user_selection_type2">Manual</label>
																		</div>
																		<input type="hidden" name="user_selection_type" id="user_selection_type" value="<?php if(isset($_POST['user_selection_type'])) { if($_POST['user_selection_type']!=''){ echo $_POST['user_selection_type']; }else { echo '2';} }else{ echo '2'; }?>"  >
																	<span class="error text-danger"><?php if(isset($_SESSION['error']['user_selection_type'])){ echo $_SESSION['error']['user_selection_type'];}?></span>
																</div>
																
															</div>
														</div>
													
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Class</label>
															<div class="col-sm-8">
																<select class="form-control selectpicker3" multiple data-actions-box="true"  data-live-search="true" name="class_ids" value=""  id="class_ids" onChange="setState('users','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getusers=1&class='+$('#class_ids').val()+'&exam='+$('#exam_ids').val()+'&userlevel='+$('#userlevel').val()+'');">
																	<?php
																	$selexam =$database->query("select * from class where estatus='1' ");
																	
																	while($data = mysqli_fetch_array($selexam))
																	{
																		?>
																	<option value="<?php echo $data['id'];?>"   <?php if(isset($_REQUEST['class_ids'])) { if(in_array($data['id'], $_REQUEST['class_ids'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['class'];?></option>
																	
																		<?php
																	}
																	?>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['class_ids'])){ echo $_SESSION['error']['class_ids'];}?></span>
															</div>
														</div>
													</div>
													
													<?php
													$_REQUEST['exam_ids']=explode(",",$_REQUEST['exam_ids']);
													?>
																
													<div class="col-lg-6" >
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Exam</label>
															<div class="col-sm-8">
																<select class="form-control selectpicker1"  multiple name="exam_ids" value="" data-actions-box="true"  data-live-search="true"  id="exam_ids" onChange="setState('users','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getusers=1&class='+$('#class_ids').val()+'&exam='+$('#exam_ids').val()+'&userlevel='+$('#userlevel').val()+'');setState('aaa','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubject=1&class='+$('#class_ids').val()+'&exam='+$('#exam_ids').val()+'');">
																	<?php
																	$selexam =$database->query("select * from exam where estatus='1' and id IN (1,2,5) ");
																	
																	while($data = mysqli_fetch_array($selexam))
																	{
																		?>
																		<option value="<?php echo $data['id'];?>"   <?php if(isset($_REQUEST['exam_ids'])) { if(in_array($data['id'], $_REQUEST['exam_ids'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['exam'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_ids'])){ echo $_SESSION['error']['exam_ids'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6" >
														<div class="form-group row" >
															<label class="col-sm-4 col-form-label"> Actions Type</label>
															<div class="col-sm-8">
															<select class="form-control" name="app_actions" value=""   id="app_actions"  onchange="appactionsdiv();">
																	<option value=''>-- Select --</option>
																	<option value="1" <?php if(isset($_REQUEST['app_actions'])) { if($_REQUEST['app_actions']==1) { echo 'selected="selected"'; }  } ?>>Learn</option>
																	<option value="2" <?php if(isset($_REQUEST['app_actions'])) { if($_REQUEST['app_actions']==2) { echo 'selected="selected"'; }  } ?>>Practice</option>
																	<option value="3" <?php if(isset($_REQUEST['app_actions'])) { if($_REQUEST['app_actions']==3) { echo 'selected="selected"'; }  } ?>>Action</option>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['app_actions'])){ echo $_SESSION['error']['app_actions'];}?></span>
															</div>
														</div>
													</div>
													<?php
													if(isset($_POST['app_actions'])){
														if($_POST['app_actions']!=''){
															if($_POST['app_actions']=='1' || $_POST['app_actions']=='2'){
																$styled='';
																$styled1='style="display:none;"';
																$styled3='';
															}else if($_POST['app_actions']=='3'){
																
																$styled='style="display:none;"';
																$styled1='';
																$styled3='';
															}else{
																$styled='style="display:none;"';
																$styled1='style="display:none;"';
																$styled3='';
															}
														}else{
															$styled='style="display:none;"';
															$styled1='style="display:none;"';
															$styled3='style="display:none;"';
														}
													}else{
														$styled='style="display:none;"';
														$styled1='style="display:none;"';
														$styled3='style="display:none;"';
													}
													?>
													<div class="col-lg-6 appactiondatadiv"  <?php echo $styled; ?>>
														<div class="form-group row" id="aaa">
															<label class="col-sm-4 col-form-label">Subject</label>
															<div class="col-sm-8">
															<select class="form-control" name="subject" value=""   id="subject" onChange="setState('bbb','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubchapter=1&class='+$('#class_ids').val()+'&subject='+$('#subject').val()+'');">
																<option value=''>-- Select --</option>
																<?php
																$row = $database->query("select * from subject where estatus='1'");
																while($data = mysqli_fetch_array($row))
																{
																	?>
																<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
																<?php
																}
																?>
															</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6 appactiondatadiv" <?php echo $styled; ?>>
														<div class="form-group row" id="bbb">
															<label class="col-sm-4 col-form-label">Chapter</label>
															<div class="col-sm-8">
															<select class="form-control" name="chapter" value=""   id="chapter" onChange="setState('ccc','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getsubtopic=1&class='+$('#class_ids').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'');">
																	<option value=''>-- Select --</option>
																	<?php
																	$row = $database->query("select * from chapter where estatus='1'");
																	
																	while($data = mysqli_fetch_array($row))
																	{
																		?>
																	<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if($_REQUEST['chapter']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6 appactiondatadiv" <?php echo $styled; ?>>
														<div class="form-group row" id="ccc">
															<label class="col-sm-4 col-form-label">Topic</label>
															<div class="col-sm-8">
															<select class="form-control" name="topic" value=""   id="topic" >
																	<option value=''>-- Select --</option>
																	<?php
																	$row = $database->query("select * from topic where estatus='1'");
																	
																	while($data = mysqli_fetch_array($row))
																	{
																		?>
																	<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['topic'])) { if($_REQUEST['topic']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['topic'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
															</div>
														</div>
													</div>

													
													
													<div class="col-lg-6 appactiondatadiv1" <?php echo $styled1; ?>>
														<div class="form-group row" id="ccc">
															<label class="col-sm-4 col-form-label">App Action Data</label>
															<div class="col-sm-8">
															<select id="app_action_id" class="form-control" name="app_action_id">
																<option value="">-Select-</option>
																<?php
																$sql=$database->query1("select * from app_modules where estatus='1'");
																while($row=mysqli_fetch_array($sql)){
																?>
																	
																	<option value="<?php echo $row['id'];?>" <?php if(isset($_REQUEST['app_action_id'])) { if($_REQUEST['app_action_id']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['title'];?></option>
																<?php
																}
																?>
															</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['app_action_id'])){ echo $_SESSION['error']['app_action_id'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6 appactiondatadiv4" <?php echo $styled3; ?>>
														<div class="form-group row" >
															<label class="col-sm-4 col-form-label">Button Name</label>
															<div class="col-sm-8">
															<input type="text" id="button_name" class="form-control" value="<?php if(isset($_REQUEST['button_name'])){ if($_REQUEST['button_name']!=''){ echo $_REQUEST['button_name']; }} ?>">
																<span class="error text-danger"><?php if(isset($_SESSION['error']['button_name'])){ echo $_SESSION['error']['button_name'];}?></span>
															</div>
														</div>
													</div>
													
												
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Uselevel</label>
															<div class="col-sm-8">
																<select class="form-control" name="userlevel" value=""  id="userlevel"   onChange="setState('users','<?php echo SECURE_PATH;?>notifyusers/ajax1.php','getusers=1&class='+$('#class_ids').val()+'&exam='+$('#exam_ids').val()+'&userlevel='+$('#userlevel').val()+'')">
																	<option value=''>-- Select --</option>
																	
																	<option value="2" <?php if(isset($_REQUEST['userlevel'])) { if($_REQUEST['userlevel']==2) { echo 'selected="selected"'; }  } ?>>Registered Users</option>
																	<option value="1" <?php if(isset($_REQUEST['userlevel'])) { if($_REQUEST['userlevel']==1) { echo 'selected="selected"'; }  } ?>>Institute Users</option>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['userlevel'])){ echo $_SESSION['error']['userlevel'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6" id="usersdiv" >
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Users</label>
															<div class="col-sm-8" id="users">
																<select class="form-control selectpicker2" name="username" value=""   multiple   data-actions-box="true" data-live-search="true"   id="username" >
																	<?php
																$row1 = $database->query1("SELECT mobile FROM student_users WHERE estatus='1' and valid='1' and class_id in (".$_POST['class_ids'].") and exam_id in (".$_POST['exam_ids'].") and mobile in (".rtrim($_POST['username'],",").") "); 
																	while($data1 = mysqli_fetch_array($row1))
																	{
																	?>
																	<option value="<?php echo $data1['mobile'];?>" <?php if(isset($_POST['username'])) { if(in_array($data1['mobile'], explode(",",$_POST['username']))) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['mobile'];?></option>
																	<?php
																	}
																	?>
																</select>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6" id="exceldiv" style="display:none;">
														<div class="form-group row" >
														
														<label class="col-sm-4 col-form-label">Excel Upload<span style="color:red;">*</span></label>
															<div class="col-sm-8">
																<a href="<?php echo SECURE_PATH;?>notifyusers/sample_excel_format.xlsx" style="text-decoration:none;color:#ff3300;"  title="Export To Excel" >Download Sample Excel Format</a> 
																

																
																<div id="file-uploader2" style="display:inline">		
																	<noscript>			
																		<p>Please enable JavaScript to use file uploader.</p>
																		<!-- or put a simple form for upload here -->
																	</noscript>  
														  
																</div>
																<script> 
																 
																	function createUploader(){   
																		   
																		var uploader = new qq.FileUploader({
																			element: document.getElementById('file-uploader2'),
																			action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=file&width=580&height=480',
																			debug: true,
																			multiple:false
																		});           
																	}
																	
																	createUploader();
																</script>
																<input type="hidden" name="file" id="file" value="<?php if(isset($_POST['file'])){ echo $_POST['file'];}?>" />
												   
																<br  />
																<?php
																  if(isset($_POST['file'])){
																	if($_POST['file']!=''){
																		?>
																	   <a href="<?php echo SECURE_PATH."files/".$_POST['file'];?>" target="_blank" >View Document</a>
																		<?php
																	}
																  }
																?>
																<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['file'])){ echo $_SESSION['error']['file'];}?></span>
															</div>
														</div>
													</div>
													<div class="col-lg-6" >
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Title</label>
															<div class="col-sm-8">
															<textarea id="title" id="title" class="form-control" ><?php if(isset($_REQUEST['title'])){ if($_REQUEST['title']!=''){ echo $_REQUEST['title']; }} ?></textarea>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['title'])){ echo $_SESSION['error']['title'];}?></span>
															</div>
														</div>
													</div>

													<div class="col-lg-6" >
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Short Description</label>
															<div class="col-sm-8">
																<textarea id="short_description" id="short_description" class="form-control" rows="5"><?php if(isset($_REQUEST['short_description'])){ if($_REQUEST['short_description']!=''){ echo $_REQUEST['short_description']; }} ?></textarea>
																<span class="error text-danger"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['short_description'];}?></span>
															</div>
														</div>
													</div>

												</div>


												<div class="row">
													<div class="col-lg-12" >
														<div class="form-group row">
															<label class="col-sm-2 col-form-label">Long Description</label>
															<div class="col-sm-10">

																<div class="wrs_container">
																	<div class="wrs_row">
																		<div class="wrs_col wrs_s12">
																			<div id="editorContainer">
																				<div id="toolbarLocation"></div>
																				<textarea id="description" class="customcontent wrs_div_box" contenteditable="true" tabindex="0" spellcheck="false" role="textbox" aria-label="Rich Text Editor, example" title="Rich Text Editor, example"><?php if(isset($_POST['description'])) { echo urldecode($_POST['description']); }else{  } ?></textarea>

																			</div>
																			<a class="btn btn-link"  data-toggle="modal" data-target="#previewModal" style="pointer:cursor;" onClick="mobilePreview()">Mobile Preview</a>
																		</div>

																	</div>
																</div>


															</div>
														</div>
													</div>

												</div>
												<div class="row">
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Web View Image</label>
															<div class="col-sm-8">

																<div id="file-uploader1" style="display:inline">
																	<noscript>
																		<p>Please enable JavaScript to use file uploader.</p>
																		<!-- or put a simple form for upload here -->
																	</noscript>

																</div>
																<script>

																	function createUploader(){

																		var uploader = new qq.FileUploader({
																			element: document.getElementById('file-uploader1'),
																			action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=webview_image&upload_type=single&filetype=file',
																			debug: true,
																			multiple:false
																		});
																	}

																	createUploader();



																	// in your app create uploader as soon as the DOM is ready
																	// don't wait for the window to load

																</script>
																<input type="hidden" name="webview_image" id="webview_image" value="<?php if(isset($_POST['webview_image'])) { echo $_POST['webview_image']; } ?>"/>
																
																<?php
																if(isset($_POST['webview_image']))
																{
																	if($_POST['webview_image']!=''){
																		?>
																		<img src="<?php echo SECURE_PATH."files/".$_POST['webview_image'];?>" style="width:60%;height:auto;" />
																		<?php
																	}

																}
																?>

																<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['webview_image'])){ echo $_SESSION['error']['webview_image'];}?></span>
															</div>
														</div>

													</div>
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Mobile View Image</label>
															<div class="col-sm-8">

																<div id="file-uploader4" style="display:inline">
																	<noscript>
																		<p>Please enable JavaScript to use file uploader.</p>
																		<!-- or put a simple form for upload here -->
																	</noscript>

																</div>
																<script>

																	function createUploader(){

																		var uploader = new qq.FileUploader({
																			element: document.getElementById('file-uploader4'),
																			action: '<?php echo SECURE_PATH;?>frame/js/upload/php2.php?upload=mobileview_image&upload_type=single&filetype=file',
																			debug: true,
																			multiple:false
																		});
																	}

																	createUploader();



																	// in your app create uploader as soon as the DOM is ready
																	// don't wait for the window to load

																</script>
																<input type="hidden" name="mobileview_image" id="mobileview_image" value="<?php if(isset($_POST['mobileview_image'])) { echo $_POST['mobileview_image']; } ?>"/>
																
																<?php
																if(isset($_POST['mobileview_image']))
																{
																	if($_POST['mobileview_image']!=''){
																		?>
																		<img src="<?php echo SECURE_PATH."files/".$_POST['mobileview_image'];?>" style="width:60%;height:auto;" />
																		<?php
																	}

																}
																?>

																<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['mobileview_image'])){ echo $_SESSION['error']['mobileview_image'];}?></span>
															</div>
														</div>

													</div>
												</div>

												<div class="row">
													<div class="col-lg-6">
														<div class="form-group row">
															<label class="col-sm-4 col-form-label">Short Image</label>
															<div class="col-sm-8">

																<div id="file-uploader" style="display:inline">
																	<noscript>
																		<p>Please enable JavaScript to use file uploader.</p>
																		<!-- or put a simple form for upload here -->
																	</noscript>

																</div>
																<script>

																	function createUploader(){

																		var uploader = new qq.FileUploader({
																			element: document.getElementById('file-uploader'),
																			action: '<?php echo SECURE_PATH;?>frame/js/upload/php.php?upload=image&upload_type=single&filetype=file',
																			debug: true,
																			multiple:false
																		});
																	}

																	createUploader();



																	// in your app create uploader as soon as the DOM is ready
																	// don't wait for the window to load

																</script>
																<input type="hidden" name="image" id="image" value="<?php if(isset($_POST['image'])) { echo $_POST['image']; } ?>"/>
																
																<?php
																if(isset($_POST['image']))
																{
																	if($_POST['image']!=''){
																		?>
																		<img src="<?php echo SECURE_PATH."files/".$_POST['image'];?>" style="width:60%;height:auto;" />
																		<?php
																	}

																}
																?>

																<span class="error" style="color:red;" ><?php if(isset($_SESSION['error']['image'])){ echo $_SESSION['error']['image'];}?></span>
															</div>
														</div>

													</div>
													<div class="col-md-6 contentd" <?php echo $stylez;?> >
														<div class="form-group">
														 <div class="custom-control custom-checkbox">
														  <input type="checkbox" class="tip_of_the_day custom-control-input" id="tip_of_the_day"  onchange="tipofthedayfunction();"  value="<?php if(isset($_POST['tip_of_the_day'])){ if($_POST['tip_of_the_day']!=''){ echo $_POST['tip_of_the_day'];}else{ echo '0'; }}else{ echo '0'; } ?>" <?php if(isset($_POST['tip_of_the_day'])) { if($_POST['tip_of_the_day']==1) { echo 'checked'; }}else{ echo ''; } ?> >
														  <label class="custom-control-label" for="tip_of_the_day" >Tip of the day</label>
														</div>
														</div>
													</div>	
												</div>



													<script type="text/javascript" >
														createEditorInstance("en", {});
													</script>
														<script>
															function rand(){

																var conttype=$('#conttype').val();
																var data2;
																data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';



																$.ajax({
																	type: 'POST',
																	//url: 'http://localhost:81/neetjee/customcontent/img.php',
																	url: '<?php echo SECURE_PATH;?>notifyusers/img.php',
																	//url: 'http://neetjeeguru.com/customcontent/img.php',
																	data: data2,
																	contentType: 'application/json; charset=utf-8',
																	dataType: 'json',

																	success: function (result,xhr) {
																		console.log("reply");
																		var shortdesc=$('#short_description').val();
																		setState('adminForm','<?php echo SECURE_PATH;?>notifyusers/process.php','validateFormc=1&user_selection_type='+$('#user_selection_type').val()+'&webview_image='+$('#webview_image').val()+'&mobileview_image='+$('#mobileview_image').val()+'&class_ids='+$('#class_ids').val()+'&exam_ids='+$('#exam_ids').val()+'&title='+escape($('#title').val())+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&topic='+$('#topic').val()+'&app_actions='+$('#app_actions').val()+'&app_action_id='+$('#app_action_id').val()+'&username='+$('#username').val()+'&short_description='+escape($('#short_description').val())+'&userlevel='+$('#userlevel').val()+'&button_name='+$('#button_name').val()+'&tip_of_the_day='+$('#tip_of_the_day').val()+'&file='+$('#file').val()+'&image='+$('#image').val())
																	},
																	error: function(e){

																		console.log("ERROR: ", e);
																	}
																});
															}


															function mobilePreview(){

																var conttype=$('#conttype').val();
																var data2;
																data2 = '{ "description" : "' + encodeURIComponent(tinymce.get('description').getContent())+ '"}';


																$.ajax({
																	type: 'POST',
																	url: '<?php echo SECURE_PATH;?>notifyusers/img.php',
																	data: data2,
																	contentType: 'application/json; charset=utf-8',
																	dataType: 'json',

																	success: function (result,xhr) {


																		console.log('description',tinymce.get('description').getContent());

																		var html = data2.description;
																		var iframe = $('#displayframe');

																		iframe[0].srcdoc =    (tinymce.get('description').getContent());

																		console.log("iframe",iframe);

																	},
																	error: function(e){

																		console.log("ERROR: ", e);
																	}
																});

															}


														</script>




                                                <div class="form-group row pl-3">
											
													<div class="col-lg-12">
														<a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="rand();">Send Notification</a>
													</div>
										   		</div>
											</div>
										
									
								</div>
								<div id="menu1" class="container tab-pane fade"><br>
										<div class="card border-0" id="ggg">
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
		unset($_SESSION['error']);

        unset($_SESSION['description']);
	}
	if(isset($_POST['validateFormc'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';

  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  
	$field = 'exam_ids';
	if(!$post['exam_ids'] || strlen(trim($post['exam_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Exam name cannot be empty";
	}
	$field = 'class_ids';
	if(!$post['class_ids'] || strlen(trim($post['class_ids'])) == 0){
	  $_SESSION['error'][$field] = "* Class name cannot be empty";
	}
	$field = 'title';
	if(!$post['title'] || strlen(trim($post['title'])) == 0){
	  $_SESSION['error'][$field] = "* Title cannot be empty";
	}
	$field = 'userlevel';
	if(!$post['userlevel']){
	  $_SESSION['error'][$field] = "* userlevel cannot be empty";
	}
	if($post['user_selection_type']=='2'){
		$field = 'username';
		if(!$post['username'] || strlen(trim($post['username'])) == 0){
		  $_SESSION['error'][$field] = "* Please Select Users";
		}
	}else{
		$field = 'file';
		if(!$post['file'] || strlen(trim($post['file'])) == 0){
		  $_SESSION['error'][$field] = "* Please upload excel file";
		}
	}
	$field = 'short_description';
	if(!$post['short_description'] || strlen(trim($post['short_description'])) == 0){
	  $_SESSION['error'][$field] = "* Short Description cannot be empty";
	}
	if($post['app_actions']!=''){
		$field = 'button_name';
		if(!$post['button_name'] || strlen(trim($post['button_name'])) == 0){
		$_SESSION['error'][$field] = "* button name cannot be empty";
		}
	}
	if($post['app_actions']=='1' || $post['app_actions']=='2'){
		$field = 'subject';
		if(!$post['subject'] || strlen(trim($post['subject'])) == 0){
		$_SESSION['error'][$field] = "* Subject cannot be empty";
		}
		$field = 'chapter';
		if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){
		$_SESSION['error'][$field] = "* Chapter cannot be empty";
		}
		
	}
	
	if(!isset($_SESSION['description']))
        $_SESSION['description'] = "";

	
		
  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateFormc'] == 2){
		
		$desc=str_replace("'","%27",$post['short_description']);
		$title=str_replace("'","%27",$post['title']);
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
	  
      setState('adminForm','<?php echo SECURE_PATH;?>notifyusers/process.php','addForm=1&class_ids=<?php echo $post['class_ids'];?>&webview_image=<?php echo $post['webview_image'];?>&mobileview_image=<?php echo $post['mobileview_image'];?>&exam_ids=<?php echo $post['exam_ids'];?>&email=<?php echo $post['email'];?>&title=<?php echo $title;?>&short_description=<?php echo $desc;?>&file=<?php echo $post['file'];?>&userlevel=<?php echo $post['userlevel'];?>&username=<?php echo $post['username'];?>&subject=<?php echo $post['subject'];?>&chapter=<?php echo $post['chapter'];?>&topic=<?php echo $post['topic'];?>&app_actions=<?php echo $post['app_actions'];?>&button_name=<?php echo $post['button_name'];?>&app_action_id=<?php echo $post['app_action_id'];?>&tip_of_the_day=<?php echo $post['tip_of_the_day'];?>&class_ids=<?php echo $post['user_selection_type'];?>&file=<?php echo $post['file'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		$description=str_replace("'","&#39;",$post['short_description']);
		$ldesc=str_replace("'","&#39;",$_SESSION['description']);
		$headerdata='<!DOCTYPE html><html lang="en"><head><style>
		.floatingButton { color: #fff: ;background-color: #232F73;border-color: #12183A;font-size:16px;}
		.floatingButton:hover,.floatingButton.active,.floatingButton:active {color: #fff;background-color: #232F73;border-color: #12183A;}
		@media (max-width:991.98px) { .webimage { display: none;} } @media (min-width:992px) { .mobileimage { display: none;} }@media screen and (min-width: 992px) {.floatingButton { color: #fff;background-color: #232F73;border-color: #12183A;width: 100%;max-width: 250px;font-weight: bold;display: block;position: fixed;bottom: 4px;left: 0;right: -220px;margin: 0 auto;padding: 16px 12px;border-radius: 30.5px;text-align: center;}}
		@media screen and (max-width: 991.98px) {.floatingButton {color:#fff;background-color: #232F73;border-color: #12183A;width: 100%;max-width: 250px;font-weight: bold;display: block;position: fixed;bottom: 4px;left: 0;right: 0;margin: 0 auto;padding: 16px 12px;border-radius: 30.5px;text-align: center;}}</style></head>';
		$header=urlencode($headerdata);
		$var1=str_replace("%3C!DOCTYPE%20html%3E%0A%3Chtml%3E%0A%3Chead%3E%0A%3C%2Fhead%3E",$header,$ldesc);
		if($post['webview_image']!='' || $post['mobileview_image']!=''){
			$image1= $imagepath.$post['webview_image'];
			$image2= $imagepath.$post['mobileview_image'];
			
			$var2='<div class="webimage" ><img src="'.$image1.'"  width="100%" height="auto" /><br /><br /></div><div class="mobileimage" ><img src="'.$image2.'" width="100%" height="auto" /><br /><br /></div>';
			$var3=urlencode($var2);
			$var4=str_replace('+','%20',$var3);
		}else{
			$var4='';
		}
		$var6=explode($header."%0A",$var1);
		
		$var6count=count($var6);
		if($var6count>1){
			$data1=$var6[1];
			
		}else{
			$data1=$var6[0];
			
				
		}
		$dataval=$header;
		$long_desc=$dataval.$var4.$data1;
		//echo $long_desc;
		if($post['app_actions']!=''){
			
			if($post['topic']!=''){
				$topic=$post['topic'];
			}else{
				$topic=0;
			}	
			if($post['app_actions']=='1'){
				if($post['subject']!='' && $post['chapter']!=''){
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;"><a href="https://rizee.in/student/learn/:'.$post['subject'].'/:'.$post['chapter'].'/:'.$topic.'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
					$app_action_id=0;
				}else{
					$long_desc1=filterqa($long_desc);
					$app_action_id=0;
				}
			}else if($post['app_actions']=='2'){
				if($post['subject']!='' && $post['chapter']!=''){
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;"><a href="https://rizee.in/student/practice/:'.$post['subject'].'/:'.$post['chapter'].'/:'.$topic.'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
					$app_action_id=0;
				}else{
					$long_desc1=filterqa($long_desc);
					$app_action_id=0;
				}
			}else if($post['app_actions']=='3'){
				if($post['app_action_id']==15){
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;"><a href="https://rizee.in/student/LiveMockTest" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
				
				}else{
					$sell=$database->query1("select * from app_modules where estatus='1' and id='".$post['app_action_id']."'");
					$row=mysqli_fetch_array($sell);
					$long_desc2=explode("%3C%2Fbody%3E",$long_desc);
					$link='<div style="text-align: center;padding-top:60px;"><a href="https://rizee.in/student/action/'.$row['module_name'].'" class="floatingButton">'.$post['button_name'].'</a></div>';
					$link1=urlencode($link);
					$link2=str_replace('+','%20',$link1);
					$long_desc11=$long_desc2[0].$link2.$long_desc2[1];
					$long_desc1=filterqa($long_desc11);
				}
				$app_action_id=$post['app_action_id'];
			}else{
				$long_desc1=filterqa($long_desc);
				$app_action_id=0;
			}
			
		}else{
			$long_desc1=filterqa($long_desc);
			$app_action_id=0;
		}
		if($post['tip_of_the_day']!=''){
			$tip_of_the_day=$post['tip_of_the_day'];
		}else{
			$tip_of_the_day=0;
		}
		
		$title=str_replace("'","&#39;",$post['title']);
		$file = "";
		 if(strlen($post['image']) > 0)
		   $file = $imagepath.$post['image'];
		$userdata='';
		 if($post['user_selection_type']=='1'){
			 if($post['file']!=''){
					$sheet_no=1;
					$filename=$post['file'];
					$filename = '../files/'.$filename;
					$inputFileName = $filename;
					$inputFileType = PHPExcel_IOFactory::identify($inputFileName);
					$objReader = PHPExcel_IOFactory::createReader($inputFileType);
					$objPHPExcel = $objReader->load($inputFileName);
					$objPHPExcel->setActiveSheetIndex($sheet_no-1);

					if(isset($objPHPExcel)){
						$from=2;
						$highestColumm = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
						$highestRow = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
						$to=$highestRow;
							
						
						for($i=$from;$i<=$to; $i++){
							
							
							$user=$objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $i)->getValue();
							$userdata.=$user.",";
							$tc='';
							$tp='';
						}
					}
				}else{
					$userdata=$post['username'];
				}
			}else{
				$userdata=$post['username'];
			}
			$sqld=$database->query1("select id,timestamp from institute_notifications where estatus='1' and is_admin='1' order by timestamp desc limit 0,1");
			$rowd=mysqli_fetch_array($sqld);
			$ctime=time();
			$diff=round($ctime-$rowd['timestamp']);
			
			if($diff>20){	
		
				$result=$database->query1("INSERT INTO  institute_notifications  set title='".$title."',description='".$description."', long_description = '".trim($long_desc1)."', image = '".$file."',exam_ids='".$post['exam_ids']."',class_ids='".$post['class_ids']."',subject='".$post['subject']."',chapter='".$post['chapter']."',topic='".$post['topic']."',app_actions='".$post['app_actions']."',app_action_id='".$app_action_id."',student_ids='".rtrim($userdata,",")."',tip_of_the_day='".$tip_of_the_day."',userlevel='".$post['userlevel']."',section_ids=0,institution_id='0',is_admin='1',estatus='1',timestamp='".time()."'" );
			}
			$sid= mysqli_insert_id($database->connection1);
			
			if($result){
				$users=explode(",",$userdata);

				$newArr = array_chunk($users,100);
				
				$countarr= count($newArr);
				$i=1;
				foreach($newArr as $data){
					$data1=implode(",",$data);
						$database->query1("INSERT INTO  send_notification_ids  set notification_id='".$sid."',usernames='".rtrim($data1,",")."',estatus='1',timestamp='".time()."'" );
					$i++;
							
				}
				//echo SECURE_PATH."send_notification.php?send_notification=true&notification_id=".$sid."&userlevel=".$post['userlevel']."&exam_ids=".$post['exam_ids']."&class_ids=".$post['class_ids']."";
				//@file(SECURE_PATH."script_notification.php?send_notification=true&notification_id=".$sid."");

			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i>Notification Sent Successfully
					</div>
				</div>
			</div>
			<script type="text/javascript">
				alert("Notification Sent Successfully");
			 setState('adminForm','<?php echo SECURE_PATH;?>notifyusers/process.php','addForm=1');
		  </script>
	 <?php
		}else{
			?>
			<div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> Notification Failed
					</div>
				</div>
			</div>
			<script type="text/javascript">
				alert("Notification Failed");
			  setState('adminForm','<?php echo SECURE_PATH;?>notifyusers/process.php','addForm=1');
		  </script>
	 	<?php

		}
    
		
}
}


function filterqa($str){
  $str = urldecode($str);
  //$res = str_replace("&nbsp;","",$str);

  return $res= getAllMath($str);

}


function getAllMath($text){



  $result = "";
  $part1 = explode("<math",$text);

  if(count($part1) > 0){

      $result.= $part1[0];//strip_tags($part1[0],'<p><img><b>');

      foreach($part1 as $first){
          $part2 = explode("</math>",$first);

          if(count($part2) > 1){
              $mml = "<math ".$part2[0]."</math>";
             
              $fields = array(
                  'mml' => $mml,
                  'lang'=>urlencode('en')
          );
          
          
          $url="http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimagenew.php";
          
         $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $url);
    curl_setopt($ch,CURLOPT_POSTFIELDS, $fields);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
         $retval = curl_exec($ch);
        curl_close($ch);
  
        $svg = json_decode($retval,true);


        $alt = "MathML Question";
        if(isset($svg['result']['alt']))
            $alt = $svg['result']['alt'];



       // $img= '<img src="'.($svg['result']['content']).'"  class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; " width="'.$svg['result']['width'].'" height="'.$svg['result']['height'].'"></img>';
       $img= "<img src='".($svg['result']['content'])."'  class='Wirisformula' role='math' alt='.$alt.' style='vertical-align: -5px; ' width='".$svg['result']['width']."' height='".$svg['result']['height']."'></img>";
        $result.= $img.$part2[1];//strip_tags($part2[1],'<p><img>');
          
         
          }
      }
  }

  return $result;
}
	?>
<?php
if(isset($_POST['mobilePreview'])){
    echo urldecode($_SESSION['description']);
}
?>
	<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	<script>
	function tipofthedayfunction()
    {
		$('.tip_of_the_day').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
	</script>
<script>
	 $(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	$(".selectpicker3").selectpicker('refresh');

	function user_selection_type(value){
		$('#user_selection_type').val(value);
		if(value=='2'){
			$('#usersdiv').show();
			$('#exceldiv').hide();
		}else{
			$('#usersdiv').hide();
			$('#exceldiv').show();
		}
	}
	</script>
	
