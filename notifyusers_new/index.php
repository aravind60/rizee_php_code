<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
    ?><script src="../edut/js/wirisdisplay.js"></script>

			<style>
.wrs_tickContainer{
display:none;
}
			/*.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}*/

			.list-Unstyles{
				position:absolute;
				z-index:30 !important;
				cursor:pointer;
			}



#viewport {
		/* initial width only   */
			margin: auto ;
			 background-color:#CCCCCC; 
			
			margin-top: 8px;
			
			/* trying different values to fix iOS issues with iFrame busting loose and wrecking the joint.   */
			overflow:hidden; 
			-webkit-overflow-scrolling: touch;

			-webkit-transition: all 0.4s ease-in-out 0s;
			-moz-transition: all 0.4s ease-in-out 0s;
			-o-transition: all 0.4s ease-in-out 0s;
			transition: all 0.4s ease-in-out 0s;
			}

		iframe {
			background-color: white;
                        border:4px solid #666;
			position:absolute;
			margin:auto;
			border:none;
			
			}

		#mobilePreview  {

		height: 620px;
		}
		.custom-drop-down .dropdown-menu.show {
					min-width: 130% !important;
					width: 100%;
					overflow-x: auto !important;
				}
		@media (max-width: 767px){
			.custom-drop-down .dropdown-menu.show {
					min-width: 100% !important;
					width: 100%;
					overflow-x: auto !important;
				}
			.bootstrap-select .dropdown-menu.show {
					min-width: 100% !important;
					width: 100%;
					overflow-x: auto !important;
				}

				
				.bootstrap-select .inner.show {
					width:100% !important;
				}
				.web-enable-buttons {
				    display: none !important;
				}
				.mobile-enable-buttons {
				    display: table-cell !important;
				}

				.footer-pagination {
				    display: block !important;
				}
				
				.pagination .disabled {
				    padding: 5px 0px;
				   margin-left: 0;
				}
				.pagination .current {
				    /*padding: 2px 5px;*/
				    font-size: 11px;
				}
				.pagination a {
					padding: 2px;
				}
			}
		</style>
		 <script type="text/javascript">
		if (window.location.search !== '') {
		    var urlParams = window.location.search;
		    if (urlParams[0] == '?') {
		        urlParams = urlParams.substr(1, urlParams.length);
		        urlParams = urlParams.split('&');
		        for (i = 0; i < urlParams.length; i = i + 1) {
		            var paramVariableName = urlParams[i].split('=')[0];
		            if (paramVariableName === 'language') {
		                _wrs_int_langCode = urlParams[i].split('=')[1];
		                break;
		            }
		        }
		    }
		}
    </script>
	 
	

        <script>
			if(typeof urlParams !== 'undefined') {
				var selectLang = document.getElementById('lang_select');
				selectLang.value = urlParams[1];
			}
			
			function getContent(){
			
			
			
			
			    var inst, contents = new Object();
				
for (inst in tinyMCE.editors) {
    if (tinyMCE.editors[inst].getContent)
        contents[inst] = tinyMCE.editors[inst].getContent();
		}
			
			
				   console.log('editor 1', contents[0]);
			   console.log('editor 2', contents[1]);
			   console.log('editor 3', contents[2]);
			
		
			
			
			}
			
			
			
		</script>

		
		<style>
         
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}

		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		</style>
		
		
		<div class="content-wrapper">


            <div class="modal fade " id="previewModal" tabindex="-1" role="dialog" aria-labelledby="previewModal" aria-hidden="true">
                <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="previewModal">Mobile Preview</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="mobilePreview">

                            <div id="viewport" class="iphone">
                                <iframe id="displayframe" name="displayframe" height="480" width="320" src=""></iframe>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
			<div class="modal fade bd-example-modal-lg" id="exampleModal1"  tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg">
						 <div class="modal-content">
							  <div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Notify Users View</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								  <span aria-hidden="true">&times;</span>
								</button>
							  </div>
							  <div class="modal-body"  id="viewDetails1">
								
							  </div>
							  <div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
								
							  </div>
							</div>
					</div>
				</div>


            <div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-md" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Users
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal fade"  id="reviewquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" >
				  <div class="modal-dialog modal-xl" role="document">

					<div class="modal-content">
						<div class="modal-header">
								
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" id="viewDetails2">
								
							</div>
					</div>
				  </div>
				</div>
				
				 <div id="extraLargeModal" class="modal fade" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-xl">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Reviewer Dashboard Lecturer Wise Report</h5>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body" id="viewdata">
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								
							</div>
						</div>
					</div>
				</div>
            
           
			<section class="admindashboard">
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="adminForm">
							<div class="col-sm-8">
								<script type="text/javascript">
									setStateGet('adminForm','<?php echo SECURE_PATH;?>notifyusers_new/process.php','addForm=1');
								</script>
							</div>
						</div>
                   
                </div>
            </section>
            
			<section >
                
                   
                        <div class="form-group" id="chaptertopics">
                           
                        </div>
                    
               
            </section>

           
        </div>	
<?php
    
    }
?>