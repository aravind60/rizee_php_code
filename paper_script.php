<?php

ini_set('display_errors','0');

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "JEE Advance".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th rowspan="2">Sr.No.</th>
							<th rowspan="2">Class</th>
							<th rowspan="2">Subject</th>
							<th rowspan="2">Chapter Name</th>
							<th rowspan="2">Topic Name</th>
							<?php
							$year = array('1' =>'2018','2' => '2019','3' => '2020');
							foreach($year as $key => $stat){
								?>
								
								<?php
								$sql1=query("select * from previous_questions where estatus='1' and exam='2' and pexamtype='2' and year='".$stat."' ");
								$row=mysqli_fetch_array($sql1);
								$k=0;
								$sql11=query("select * from previous_sets where estatus='1'  and pid='".$row['id']."' ");
								while($row1=mysqli_fetch_array($sql11)){
									?>
									<!-- <th><?php echo $row1['qset']; ?></th>-->
									
									<?php
								$k++;
								}
								?>
								<th   colspan="<?php echo $k; ?>"><?php echo $stat; ?></th>
								<?php
								
							}
							?>
							
						</tr>
						<tr>
							<?php
							$year = array('1' =>'2018','2' => '2019','3' => '2020');
							foreach($year as $key => $stat){
								$sql1=query("select * from previous_questions where estatus='1' and exam='2' and pexamtype='2' and year='".$stat."' ");
								$row=mysqli_fetch_array($sql1);
								$sql11=query("select * from previous_sets where estatus='1'  and pid='".$row['id']."' ");
								$rowc=mysqli_num_rows($sql11);
								if($rowc>0){
									while($row1=mysqli_fetch_array($sql11)){
										?>
										<th><?php echo $row1['qset']; ?></th>
										<?php
									
									}
								}else{ ?>
									<th></th>
								<?php
								}
							}
							?>
						</tr>
                        <?php
                            $k=1;
							$sql=query("select * from topic where estatus='1'   and subject in (2,3,4) order by subject asc");
							while($row=mysqli_fetch_array($sql)){
								$sql1=query("select * from chapter where estatus='1' and id='".$row['chapter']."'");
								$row1=mysqli_fetch_array($sql1);
								$sqls=query("select * from subject where estatus='1' and id='".$row['subject']."'");
								$rows=mysqli_fetch_array($sqls);

								$sqlsc=query("select * from class where estatus='1' and id='".$row['class']."'");
								$rowsc=mysqli_fetch_array($sqlsc);
								
								echo "<tr>";
								?>
									
										
								<td><?php echo $k;?></td>
								<td ><?php echo $rowsc['class'];?></td>
								<td ><?php echo $rows['subject'];?></td>
								<td><?php echo $row1['chapter']; ?></td>
								<td><?php echo $row['topic'];?></td>
								<?php
								//$sell=query("select * from previous_questions where estatus='1' and exam=1 and year='2018'  order by year asc");
								//while($rowl=mysqli_fetch_array($sell)){
								
									//$sellset1=query("select * from previous_sets where estatus='1' and pid='".$rowl['id']."' and display_status='1' ");
									//while($rowlset1=mysqli_fetch_array($sellset1)){
										$year = array('1' =>'2018','2' => '2019','3' => '2020');
										foreach($year as $key => $stat){
											$sql1=query("select * from previous_questions where estatus='1' and exam='2' and pexamtype='2' and year='".$stat."' ");
											$rowk=mysqli_fetch_array($sql1);
											$sql11=query("select * from previous_sets where estatus='1'  and pid='".$rowk['id']."' ");
											$rowc=mysqli_num_rows($sql11);
											if($rowc>0){
												while($row11=mysqli_fetch_array($sql11)){
													$sqll=query("select count(id) as count from createquestion where estatus='1' and ppaper='1'    and find_in_set(2,exam)>0  and pexamtype='2' and year='".$stat."' and qset='".$row11['id']."' and find_in_set(".$row['id'].",topic)>0 and find_in_set(".$row['chapter'].",chapter)>0 order by id ASC ");
													$rowlcountt=mysqli_fetch_array($sqll);
										?>
													<td><?php if($rowlcountt['count']!=''){ echo $rowlcountt['count']; }else {echo '';} ?></td>
											
													<?php
												}
											}else{ ?>
												<td>0</td>
											<?php
											}
										}
									//}
									
									
								//}
								echo '</tr>';
								$k++;
							}
							?>
                               
                               
                              
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>