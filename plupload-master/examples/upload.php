<?php
include('../../include/session.php');
echo $session->username;
ini_set('memory_limit', '-1');
ini_set('display_errors', 'ON');

ini_set('post_max_size', '100000M');
ini_set('upload_max_filesize', '100000M');
ini_set('max_input_time ', 9000);
ini_set('max_execution_time', 9000);
/**
 * upload.php
 *
 * Copyright 2013, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

#!! IMPORTANT: 
#!! this file is just an example, it doesn't incorporate any security checks and 
#!! is not recommended to be used in production environment as it is. Be sure to 
#!! revise it and customize to your needs.


// Make sure file is not cached (as it happens for example on iOS devices)
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/* 
// Support CORS
header("Access-Control-Allow-Origin: *");
// other CORS headers if any...
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	exit; // finish preflight CORS requests here
}
*/

// 5 minutes execution time
@set_time_limit(20 * 60);

// Uncomment this one to fake upload time
// usleep(5000);

// Settings
//$targetDir = ini_get("upload_tmp_dir") . DIRECTORY_SEPARATOR . "plupload";
//$targetDir = 'uploads';

$source = $_FILES["file-upload"]["tmp_name"];
$destination = $_FILES["file-upload"]["name"];
// uploaded folder is moved to the destination
move_uploaded_file($source, $destination);

 // the response function
   function verbose($ok=1,$info=""){
      // failure to upload throws 400 error
      if ($ok==0) { http_response_code(400); }
      die(json_encode(["ok"=>$ok, "info"=>$info]));
   }
   
   // invalid upload
   if (empty($_FILES) || $_FILES['file']['error']) {
      verbose(0, "Failed to move uploaded file.");
   }
   // upload destination
   //$filePath = __DIR__ . DIRECTORY_SEPARATOR . "uploads";
   $filePath='../../files/';
   if (!file_exists($filePath)) {
      if (!mkdir($filePath, 0777, true)) {
         verbose(0, "Failed to create $filePath");
      }
   }
   $fileName = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];
   $pathinfo=pathinfo($fileName);

$file = md5($pathinfo['filename']);
 $ext = $pathinfo['extension'];
 $filepathdata=$file . '.' . $ext;
   $filePath = $filePath . DIRECTORY_SEPARATOR . $filepathdata;
   // dealing with the chunks
   $chunk = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
   $chunks = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;
   $out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
   if ($out) {
      $in = @fopen($_FILES['file']['tmp_name'], "rb");
      if ($in) {
         while ($buff = fread($in, 4096)) { fwrite($out, $buff); }
      } else {
         verbose(0, "Failed to open input stream");
      }
      @fclose($in);
      @fclose($out);
      @unlink($_FILES['file']['tmp_name']);
   } else {
      verbose(0, "Failed to open output stream");
   }
   // check if file was uploaded
   if (!$chunks || $chunk == $chunks - 1) {
      rename("{$filePath}.part", $filePath);
   }
   verbose(1, "Upload OK");
   ?>
