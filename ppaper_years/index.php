<?php
    error_reporting(0);
    include('../include/session.php');
	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
        //unset($_SESSION['image']);  
    ?>
    
   
		<style>
         .wrs_editor .wrs_formulaDisplay {
             height: 250px !important;
         }
         </style>
		
				
			<div class="content-wrapper">
				<!-- Breadcrumbs-->
				<section class="breadcrumbs-area2 my-3">
					<div class="container">
						<div class="d-flex justify-content-between align-items-center">
							<div class="title">
								<h1 class="text-uppercase">Add Previous Paper Years<small>Lets you a Create Previous Paper Years</small></h1>
							</div>
						</div>
						<hr>
					</div>
				</section>
                <!-- End Breadcrumbs-->
				<!-- Content Area-->
				<section class="content-area">
					<div class="container">
						<div class="row">
							<div class="col-xl-12 col-lg-12">
								<div class="card border-0 shadow mb-4">
									<div
										class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
										<h6 class="m-0 font-weight-bold text-primary">Create Previous Paper Years</h6>
									</div>
									<div class="card-body">
									  
											<div class="row">
												<div class="col-lg-12 col-md-12">
													<div class="form-group row">
														<div class="col-sm-8">
															<a href="#" class="btn btn-md btn-theme-2"onClick="$('#adminForm').slideToggle()">Create Previous Paper Years</a>
														</div>
													</div>
													<div class="form-group row" id="adminForm" style="display:none;">
														<div class="col-sm-8">
															<script type="text/javascript">
																setStateGet('adminForm','<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=1');
															</script>
														</div>
													</div>
												</div>
											</div>
										  
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				 <section>
					<div id="adminTable">
						<script type="text/javascript">
							setStateGet('adminTable','<?php echo SECURE_PATH;?>ppaper_years/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					</div>
				</section>
			</div>
		
	<?php
		
		}
	?>
	
	<script type="text/javascript">

	function addList(){
		cnt = $('#session_list').val();
		
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list').val(new_cnt);
	
		html = '<div class="list" id="list'+new_cnt+'"></div>';	
   
		$('#dates_list').append(html);
		//$("#exam_date"+new_cnt).datetimepicker();
		$(".datepicker1").datetimepicker();
		setStateGet('list'+new_cnt,'<?php echo SECURE_PATH;?>ppaper_years/process.php','add_listing='+new_cnt);
	}
	function removeList(id){

		cnt = $('#session_list').val();
		$('#list'+id).remove();
		new_cnt = parseInt(cnt)-1;
		$('#session_list').val(new_cnt);
	}
	function calconditions(){
		var retval = '';
		i=0;
        $('#dates_list .list').each(function(){
			retval+= $(this).find('.qset').val()+'_'+$(this).find('.username').val()+'_'+$(this).find('.exam_date').val()+'_'+$(this).find('.comid').val()+'^';
		});
		//alert(retval);
		return retval;
		
	}
	</script>
