<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}

//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
		?>
	 <script>
		$('html, body').animate({
		   scrollTop: $("#myDiv").offset().top
	   }, 2000);
	  </script>
	 <?php
    $data_sel = $database->query("SELECT * FROM previous_questions WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	<script>
		$(function () {
			$('.datepicker1').datetimepicker();
			
		});

	</script>
	<!-- <script>
		
		editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
	                 editor.insertInto(document.getElementById('editorContainer'));
				 width:300;
				 height:500;
	</script> -->
	
  <div class="col-lg-12 col-md-12" id="myDiv">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select class="form-control" name="exam" value=""   id="exam" onchange="examfunction();" >
						<option value=''>-- Select --</option>
						<?php
						$row = $database->query("select * from exam where estatus='1'");
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['exam'])) { if($_POST['exam']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['exam'];?></option>
						<?php
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam'])){ echo $_SESSION['error']['exam'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<?php
		if(isset($_POST['pexamtype'])) { 
			if($_POST['pexamtype']!='' && $_POST['pexamtype']!='0'){
				$style='';
				
			}else{
				$style='style="display:none;"';
			}
		}
	?>
	<div class="row pexamtype1" <?php echo $style; ?>>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Exam Type<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select class="form-control" name="pexamtype" value=""   id="pexamtype">
						<option value=''>-- Select --</option>
						<option value='1' <?php if(isset($_POST['pexamtype'])) { if($_POST['pexamtype']=='1') { echo 'selected="selected"'; }  } ?> >Main</option>
						<option value='2' <?php if(isset($_POST['pexamtype'])) { if($_POST['pexamtype']=='2') { echo 'selected="selected"'; }  } ?>>Advance</option>
					</select>
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['pexamtype'])){ echo $_SESSION['error']['pexamtype'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row" >
				<label class="col-sm-4 col-form-label">Year<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="year" value="<?php if(isset($_POST['year'])) { echo $_POST['year']; }else{} ?>"   id="year" autocomplete="off" >
					<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['year'])){ echo $_SESSION['error']['year'];}?></span>
				</div>
			</div>
		</div>
	</div>
	<?php
	$count=1;
	?>
	<div class="row">
		<div class="col-md-2">
			<label class="col-form-label">Question Set<span style="color:red;">*</span></label> 
		</div>
		<div id="dates_list" class="col-md-10">
			<?php
			if(isset($_POST['editform']))
			{
				$dataec = $database->query("select * from  previous_sets where estatus='1' and pid='".$_POST['id']."'");
				$rowe = mysqli_num_rows($dataec);
				if($rowe > 0)
				{
				while($rowec = mysqli_fetch_array($dataec))
				{
				?>
					<div class="list" id="list<?php echo $count;?>">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Set <?php echo $count; ?><span style="color:red;">*</span></label>
									
									<input name="qset"  id="qset" value="<?php if(isset($rowec['qset'])) { echo $rowec['qset']; }?>" class="form-control qset " autocomplete="off"  >
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qset'.$count])){ echo $_SESSION['error']['qset'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1"> Exam Date <span style="color:red;">*</span></label>
									
									<input name="exam_date"  id="exam_date" value="<?php if(isset($rowec['exam_date'])) { echo date("m/d/Y h:i A",$rowec['exam_date']); }?>" class="form-control exam_date datepicker1 "  autocomplete="off">
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam_date'.$count])){ echo $_SESSION['error']['exam_date'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Username <span style="color:red;">*</span></label>
									
									<select class="form-control username" name="username"  value=""   id="username" >
										<option value=''>-Select-</option>
											<?php
											
											$zSql1 = $database->query("SELECT * FROM users WHERE valid='1' and userlevel='8' "); 
											
											if(mysqli_num_rows($zSql1)>0)
											{ 
												while($data = mysqli_fetch_array($zSql1))
												{ 
													?> 
													<option value="<?php echo $data['username'];?>" <?php if(isset($rowec['username'])) { if($rowec['username']==$data['username']) { echo 'selected="selected"'; }  } ?>><?php echo $data['username'];?></option>
												<?php
												}
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['username'.$count])){ echo $_SESSION['error']['username'.$count];}?></span>
								</div>
							</div>
							<input type="hidden" name="comid" id="comid" class="comid" value="<?php if(isset($rowec['id'])) { echo $rowec['id']; }?>" >
							
							<div class="col-md-2 form-group" style="padding-top: 2rem;">
								<?php
								if($count == 1){
								?>
									<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
								<?php
								}
								else{
								?>
									<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
									<a class="btn btn-danger text-white" onclick="removeList('<?php echo $count; ?>')"><i class="fa fa-minus "></i></a>
								<?php
								}
								?>
								
							</div>
						</div>
					</div>

					
				
					<?php
					$count++;
					}
					?>
					<input type="hidden" id="session_list"  value="<?php echo  $count-1;?>"/>
			<?php
				}else{
				
				$posdata1 = explode('^',rtrim($_POST['data4']));
				foreach($posdata1 as $rowpost2)
				{
					if(strlen($rowpost2)>0)
					{
						$rowpost = explode('_',$rowpost2);
						/*echo '<pre>';
						print_r($rowpost);
						echo '</pre>';*/
						?>
									<div class="list" id="list<?php echo $count;?>">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Set <?php echo $count; ?><span style="color:red;">*</span></label>
									
									<input name="qset"  id="qset" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qset " autocomplete="off" >
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qset'.$count])){ echo $_SESSION['error']['qset'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Exam Date <span style="color:red;">*</span></label>
									
									<input name="exam_date"  id="exam_date" value="<?php if(isset($rowpost[2])) { echo $rowpost[2]; }?>" class="form-control exam_date datepicker1 "  autocomplete="off">
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam_date'.$count])){ echo $_SESSION['error']['exam_date'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Username <span style="color:red;">*</span></label>
									
									<select class="form-control username" name="username"  value=""   id="username" >
										<option value=''>-Select-</option>
											<?php
											
											$zSql1 = $database->query("SELECT * FROM users WHERE valid='1' and userlevel='8' "); 
											
											if(mysqli_num_rows($zSql1)>0)
											{ 
												while($data = mysqli_fetch_array($zSql1))
												{ 
													?> 
													<option value="<?php echo $data['username'];?>" <?php if(isset($rowpost[1])) { if($rowpost[1]==$data['username']) { echo 'selected="selected"'; }  } ?>><?php echo $data['username'];?></option>
												<?php
												}
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['username'.$count])){ echo $_SESSION['error']['username'.$count];}?></span>
								</div>
							</div>
							<input type="hidden" name="comid" id="comid" class="comid" value="<?php if(isset($rowpost[2])) { echo $rowpost[2]; }?>" >
							
							<div class="col-md-2 form-group" style="padding-top: 2rem;">
								<?php
								if($count == 1){
								?>
									<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
								<?php
								}
								else{
								?>
									<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
									<a class="btn btn-danger text-white" onclick="removeList('<?php echo $count; ?>')"><i class="fa fa-minus "></i></a>
								<?php
								}
								?>
								
							</div>
						</div>
					</div>

					
				
					<?php
					$count++;
					}
					
				}
				?>
					<input type="hidden" id="session_list"  value="<?php echo  $count-1;?>"/>
				
				<?php
				}
			}else if(isset($_REQUEST['data4']))
			{
				$posdata1 = explode('^',rtrim($_POST['data4']));
				foreach($posdata1 as $rowpost2)
				{
					if(strlen($rowpost2)>0)
					{
						$rowpost = explode('_',$rowpost2);
						/*echo '<pre>';
						print_r($rowpost);
						echo '</pre>';*/
						?>
							<div class="list" id="list<?php echo $count;?>">
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="exampleInputEmail1">Set <?php echo $count; ?><span style="color:red;">*</span></label>
									
									<input name="qset"  id="qset" value="<?php if(isset($rowpost[0])) { echo $rowpost[0]; }?>" class="form-control qset " autocomplete="off" >
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qset'.$count])){ echo $_SESSION['error']['qset'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Exam Date <span style="color:red;">*</span></label>
									
									<input name="exam_date"  id="exam_date" value="<?php if(isset($rowpost[2])) { echo $rowpost[2]; }?>" class="form-control exam_date datepicker1 "  autocomplete="off">
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam_date'.$count])){ echo $_SESSION['error']['exam_date'.$count];}?></span>
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Username <span style="color:red;">*</span></label>
									
									<select class="form-control username" name="username"  value=""   id="username" >
										<option value=''>-Select-</option>
											<?php
											
											$zSql1 = $database->query("SELECT * FROM users WHERE valid='1' and userlevel='8' "); 
											
											if(mysqli_num_rows($zSql1)>0)
											{ 
												while($data = mysqli_fetch_array($zSql1))
												{ 
													?> 
													<option value="<?php echo $data['username'];?>" <?php if(isset($rowpost[1])) { if($rowpost[1]==$data['username']) { echo 'selected="selected"'; }  } ?>><?php echo $data['username'];?></option>
												<?php
												}
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['username'.$count])){ echo $_SESSION['error']['username'.$count];}?></span>
								</div>
							</div>
							<input type="hidden" name="comid" id="comid" class="comid" value="<?php if(isset($rowpost[2])) { echo $rowpost[2]; }?>" >
							
							<div class="col-md-2 form-group" style="padding-top: 2rem;">
								<?php
								if($count == 1){
								?>
									<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
								<?php
								}
								else{
								?>
									<a class="btn btn-danger text-white" onclick="removeList('<?php echo $count; ?>')"><i class="fa fa-minus "></i></a>
								<?php
								}
								?>
								
							</div>
						</div>
					</div>

					
				
					<?php
					$count++;
					}
					
				}
				?>
					<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
			<?php
			}else{
			?>
				<div class="list" id="list">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="exampleInputEmail1">Set <?php echo $count; ?><span style="color:red;">*</span></label>
								
								<input name="qset"  id="qset" value="<?php if(isset($_POST['qset'])) { echo $_POST['qset']; }?>" class="form-control qset "  autocomplete="off">
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qset'])){ echo $_SESSION['error']['qset'];}?></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="exampleInputEmail1">Exam Date <span style="color:red;">*</span></label>
								
								<input name="exam_date"  id="exam_date" value="<?php if(isset($_POST['exam_date'])) { echo $_POST['exam_date']; }?>" class="form-control exam_date datepicker1 "  autocomplete="off">
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam_date'])){ echo $_SESSION['error']['exam_date'];}?></span>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="exampleInputEmail1">Username <span style="color:red;">*</span></label>
								
								<select class="form-control username" name="username"  value=""   id="username" >
									<option value=''>-Select-</option>
										<?php
										
										$zSql1 = $database->query("SELECT * FROM users WHERE valid='1' and userlevel='8' "); 
										
										if(mysqli_num_rows($zSql1)>0)
										{ 
											while($data = mysqli_fetch_array($zSql1))
											{ 
												?> 
												<option value="<?php echo $data['username'];?>" <?php if(isset($_POST['username'])) { if($_POST['username']==$data['username']) { echo 'selected="selected"'; }  } ?>><?php echo $data['username'];?></option>
											<?php
											}
											
										}
										?>
									</select>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
							</div>
						</div>

						<input type="hidden" name="comid" id="comid" class="comid" value="" >
						
						<div class="col-md-2 form-group" style="padding-top: 2rem;">
							<?php
							if($count == 1){
							?>
								<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
							<?php
							}
							else{
							?>
								<a class="btn btn-danger text-white" onclick="removeList('<?php echo $count; ?>')"><i class="fa fa-minus "></i></a>
							<?php
							}
							?>
							
						</div>
					</div>
				</div>
			
					<input type="hidden" id="session_list"  value="<?php echo  $count;?>"/>
				<?php } ?>
			</div>

	
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
			<label></label>
				<div class="col-sm-8 text-enter">
					<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>ppaper_years/process.php','validateForm=1&exam='+$('#exam').val()+'&pexamtype='+$('#pexamtype').val()+'&year='+$('#year').val()+'&data4='+calconditions()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
					</div>
			</div>
		</div>
	</div>
	
</div>

<?php
unset($_SESSION['error']);
}
?>

<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	if(!$post['exam'] || strlen(trim($post['exam'])) == 0){

		$_SESSION['error']['exam'] = "*Please Select Exam ";

	}
	if($post['examtype']=='2'){
		if(!$post['pexamtype'] || strlen(trim($post['pexamtype'])) == 0){

			$_SESSION['error']['pexamtype'] = "*Please Select Exam Type";

		}
	}
	if(!$post['year'] || strlen(trim($post['year'])) == 0){

		$_SESSION['error']['year'] = "*Please Enter Year";

	}
	$abcd = 1;
	$list = explode("^",rtrim($post['data4'],'^'));
	
		if(count($list)>0)
		{
		foreach ($list as $k)
		{
			
			$r = explode('_',$k);
			
			if(strlen(trim($r['0'])) == 0 ){
			  $_SESSION['error']['qset'.$abcd] = "Please Enter Set";
			}
			if(strlen(trim($r['2'])) == 0  || $r['2'] == 'undefined'){
			  $_SESSION['error']['exam_date'.$abcd] = "Please Select Exam date";
			}
			/*if(strlen(trim($r['1'])) == 0 ){
			  $_SESSION['error']['username'.$abcd] = "Please Select Username";
			}*/
			$abcd++;
		}
		if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
					
			?>
				<script type="text/javascript">
				$('#adminForm').show();

				setState('adminForm','<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=1&exam=<?php echo $post['exam'];?>&pexamtype=<?php echo $post['pexamtype'];?>&year=<?php echo $post['year'];?>&data4=<?php echo $post['data4']; ?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['page'])){ echo '&page='.$post['page'];}?>');
				
				</script>

			<?php
			}
			else{
				if($post['exam']!=''){
						$exam=$post['exam'];
				}else{
					$exam=0;
				}
				if($post['pexamtype']!=''){
					$pexamtype=$post['pexamtype'];
				}else{
					$pexamtype=0;
				}
				
				if($id=='NULL')
				{
					
					$result=$database->query("insert previous_questions set exam='".$exam."',pexamtype='".$pexamtype."',year='".$post['year']."',estatus='1',cusername='".$session->username."',timestamp='".time()."'");
					$lid=mysqli_insert_id($database->connection);
					if($result){
						$list = explode("^",rtrim($post['data4'],'^'));
							if(count($list)>0)
							{
								foreach ($list as $k)
								{
									$r = explode('_',$k);
									$exam_date=strtotime($r[2]);
									$result1=$database->query("insert previous_sets set pid='".$lid."',qset='".$r[0]."',username='".$r[1]."',exam_date='".$exam_date."',estatus='1',cusername='".$session->username."',timestamp='".time()."'");

									
								}
							}

									
					?>
						<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>Previous Paper Created  Successfully!</div>
						
							<script type="text/javascript">
						
							animateForm('<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					<?php
					
				
				}
			}else{
				
				$result=$database->query("update previous_questions set exam='".$exam."',pexamtype='".$pexamtype."',year='".$post['year']."',eusername='".$session->username."',etimestamp='".time()."' where id='".$id."'");
				$database->query("update previous_sets set estatus='0',eusername='".$session->username."',etimestamp='".time()."'   where pid='".$id."'");
					if($result){
						$list = explode("^",rtrim($post['data4'],'^'));
							if(count($list)>0)
							{
								foreach ($list as $k)
								{
									$r = explode('_',$k);
									$exam_date=strtotime($r[2]);
									if($r[3]!='' && $r[3]!='undefined'){ 
										
				
										$result1=$database->query("update previous_sets set qset='".$r[0]."',username='".$r[1]."',exam_date='".$exam_date."',estatus='1',eusername='".$session->username."',etimestamp='".time()."' where id='".$r[3]."' and pid='".$id."'");
									}else{
										$result1=$database->query("insert previous_sets set pid='".$id."',qset='".$r[0]."',username='".$r[1]."',exam_date='".$exam_date."',estatus='1',cusername='".$session->username."',timestamp='".time()."'");
									}
								}
							}

									
					?>
						<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>Previous Paper Updated  Successfully!</div>
						
							<script type="text/javascript">
						
							animateForm('<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					<?php
					
				
				}
			}
		}
	

?>

 
	<?php
	
		}
}

if(isset($_GET['rowDelete'])){
	$database->query("update  previous_questions set estatus='0',dusername='".$session->username."',dtimestamp='".time()."'  WHERE id = '".$_GET['rowDelete']."'");
	$database->query("update  previous_sets set estatus='0',dusername='".$session->username."',dtimestamp='".time()."' WHERE pid = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Previous Paper Deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'previous_questions';
  $condition = "estatus='1'";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."ppaper_years/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." order by id asc ";
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
	  <script type="text/javascript">

$('#dataTable').DataTable({
"pageLength": 10
});
</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Previous Paper Questions</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Exam</th>
											<th class="text-left" nowrap>Exam Type</th>
											<th class="text-left" nowrap>Year</th>
											<th class="text-left" nowrap>Sets</th>
											<th class="text-left" nowrap>Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
									$k=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										if($value['pexamtype']=='1'){
											$pexamtype="Mains";
										}else if($value['pexamtype']=='2'){
											$pexamtype="Advance";
										}else{
											$pexamtype="";
										}
										$sql=$database->query("select * from previous_sets where estatus='1' and pid='".$value['id']."'");
										$rowc=mysqli_num_rows($sql);
                  					?>
										<tr>
										<td class="text-left"><?php echo $k;?></td>
										<td class="text-left" nowrap><?php echo $database->get_name('exam','id',$value['exam'],'exam'); ?></td>
										<td class="text-left" nowrap><?php echo $pexamtype; ?></td>
										<td class="text-left" nowrap><?php echo $value['year']; ?></td>
										<td class="text-left" nowrap>
											<?php
											if($rowc>0){
												if(isset($value['pexamtype'])){
													if($value['pexamtype']!="" && $value['pexamtype']!="0"){
														$pexamtype=" AND pexamtype ='".$value['pexamtype']."' ";
													} else {
														$pexamtype="";
													}
												}else{
													$pexamtype="";
												}
											?>
												<table class="table table-bordered" ><tr><td>S.No</td><td>Exam Date</td><td>Set</td><td>Question Count</td><td>Username</td><td>PDF Download</td><td> Enable/Disable</td></tr>
													<?php
													$j=1;
													while($row=mysqli_fetch_array($sql)){
														if($row['exam_date']!=''){
															$exam_date=date("d/m/Y h:i A",$row['exam_date']);
														}else{
															$exam_date="-";
														}
													//echo "select count(id) as cnt from createquestion where estatus='1'  and FIND_IN_SET(".$value['exam'].",exam) >0 and ppaper='1'".$pexamtype." and year ='".$value['year']."' and qset ='".$row['id']."' ";
														$sqlq=$database->query("select count(id) as cnt from createquestion where estatus='1'  and FIND_IN_SET(".$value['exam'].",exam) >0 and ppaper='1'".$pexamtype." and year ='".$value['year']."' and qset ='".$row['id']."' "); 
														$rowq=mysqli_fetch_array($sqlq);
														?>
															<tr>
																<td><?php echo $j; ?></td><td><?php echo $exam_date; ?></td><td><?php echo $row['qset']; ?></td><td><?php echo $rowq['cnt']; ?></td><td><?php echo $row['username']; ?></td>
																<td><a href="<?php echo SECURE_PATH;?>ppaper_years/report.php?exam=<?php echo $value['exam']; ?>&examtype=<?php echo $value['pexamtype']; ?>&year=<?php echo $value['year']; ?>&set=<?php echo $row['id']; ?>&username=<?php echo $row['username']; ?>" style="float:center" target="_blank"  title="PDF Export" ><i class="text-center fa fa-file-pdf-o" style="font-size: 15px;color:blue;"></i></a></td>
																<td><?php
																	if($row['display_status']=='0'){
																		?>
																			<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>ppaper_years/process.php','getset=1&exam=<?php echo $value['exam']; ?>&examtype=<?php echo $value['pexamtype']; ?>&year=<?php echo $value['year']; ?>&set=<?php echo $row['id']; ?>&status=enablestatus')">Enable</a>
																		<?php
																	}else{
																		?>
																			<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>ppaper_years/process.php','getset=1&exam=<?php echo $value['exam']; ?>&examtype=<?php echo $value['pexamtype']; ?>&year=<?php echo $value['year']; ?>&set=<?php echo $row['id'];  ?>&status=disablestatus')">Disable</a>
																		<?php
																	}
																	?></td>
															</tr>
														<?php
													$j++;	
													}
													?>
												</table>
											<?php } ?>
										</td>
										<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>ppaper_years/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
												<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>ppaper_years/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a></td>
                  					</tr>
									<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}

if(isset($_REQUEST['getset'])){
	if($_REQUEST['status']=='enablestatus'){
		$result=$database->query("update previous_sets set display_status='1',eusername='".$session->username."',etimestamp='".time()."' where estatus='1' and id='".$_REQUEST['set']."'");
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Previous Paper Enabled  Successfully!</div>
			
				<script type="text/javascript">
			
				//animateForm('<?php echo SECURE_PATH;?>ppaper_years/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				 setStateGet('adminTable','<?php echo SECURE_PATH;?>ppaper_years/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
		<?php
	}else if($_REQUEST['status']=='disablestatus'){
			$result=$database->query("update previous_sets set display_status='0',eusername='".$session->username."',etimestamp='".time()."' where estatus='1' and id='".$_REQUEST['set']."'");
			?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Previous Paper Disabled  Successfully!</div>
			
				<script type="text/javascript">
			
				//animateForm('<?php echo SECURE_PATH;?>ppaper_years/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				 setStateGet('adminTable','<?php echo SECURE_PATH;?>ppaper_years/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
		<?php
	}
}
?>
	

   
   <br  />

<script>
function examfunction(){
	var exam=$('#exam').val();
	if(exam=='2'){
		$('.pexamtype1').show();
	}else{
		$('.pexamtype1').hide();
	}
}
</script>
<?php
if(isset($_REQUEST['add_listing']))
{
	$count = $_REQUEST['add_listing'];
?>
<script>
		$(function () {
			$('.datepicker1').datetimepicker();
			
		});

	</script>
	<div class="row">
						<div class="col-md-4">
							<div class="form-group">
								<label for="exampleInputEmail1">Set <?php echo $count; ?><span style="color:red;">*</span></label>
								
								<input name="qset"  id="qset" value="<?php if(isset($_POST['qset'])) { echo $_POST['qset']; }?>" class="form-control qset " autocomplete="off" >
								<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['qset'])){ echo $_SESSION['error']['qset'];}?></span>
							</div>
						</div>
						<div class="col-md-3">
								<div class="form-group">
									<label for="exampleInputEmail1">Exam Date <span style="color:red;">*</span></label>
									
									<input name="exam_date"  id="exam_date" value="<?php if(isset($_POST['exam_date'])) { echo $_POST['exam_date']; }?>" class="form-control exam_date exam_date datepicker1 "  autocomplete="off">
									<span class="error " style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['exam_date'])){ echo $_SESSION['error']['exam_date'];}?></span>
								</div>
							</div>
						<div class="col-md-3">
							<div class="form-group">
								<label for="exampleInputEmail1">Username <span style="color:red;">*</span></label>
								
								<select class="form-control username" name="username"  value=""   id="username" >
									<option value=''>-Select-</option>
										<?php
										
										$zSql1 = $database->query("SELECT * FROM users WHERE valid='1' and userlevel='8' "); 
										
										if(mysqli_num_rows($zSql1)>0)
										{ 
											while($data = mysqli_fetch_array($zSql1))
											{ 
												?> 
												<option value="<?php echo $data['username'];?>" <?php if(isset($_POST['username'])) { if($_POST['username']==$data['username']) { echo 'selected="selected"'; }  } ?>><?php echo $data['username'];?></option>
											<?php
											}
											
										}
										?>
									</select>
									<span class="error" style="color:red;font-size:9px;"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
							</div>
						</div>
						<input type="hidden" name="comid" id="comid" class="comid" value="" >
						
						<div class="col-md-2 form-group" style="padding-top: 2rem;">
							<?php
							if($count == 1){
							?>
								<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
							<?php
							}
							else{
							?>
								<a class="btn btn-success text-white" onclick="addList()"><i class="fa fa-plus"></i></a>
								<a class="btn btn-danger text-white" onclick="removeList('<?php echo $count; ?>')"><i class="fa fa-minus "></i></a>
							<?php
							}
							?>
							
						</div>
					</div>
	
				
	
<!--Addrow end-->
<?php
}
?>





