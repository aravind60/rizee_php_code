<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Username</th>
							<th style="text-align:center;">Exam</th>
							<th style="text-align:center;">Total Questions</th>
							
						</tr>
                        <?php
							$k=1;
							$sql=query("select id,exam,username from createquestion where  ppaper='1' and exam in (1)  group by username order by exam asc");
							while($row=mysqli_fetch_array($sql)){
								if($row['exam']==1){
									$exam="NEET";
								}else if($row['exam']==2){
									$exam="JEE";
								}else if($row['exam']==4){
									$exam="BITSAT";
								}
								$sql1=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='1' and username='".$row['username']."' and exam in (1) order by exam asc");
								$row1=mysqli_fetch_array($sql1);
								$sql2=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='0'  and username='".$row['username']."' and exam in (1) and dusername='admin2' order by exam asc");
								$row2=mysqli_fetch_array($sql2);
								$tot=$row1['cnt']+$row2['cnt'];
								if($tot){
								?>
									<tr>
										<td><?php echo $k; ?></td>
										<td><?php echo $row['username']; ?></td>
										<td>NEET</td>
										<td><?php echo $tot; ?></td>
									</tr>
							<?php
							$k++;
								}
							

							}
							$sql=query("select id,exam,username from createquestion where  ppaper='1' and exam in (4)  group by username order by exam asc");
							while($row=mysqli_fetch_array($sql)){
								if($row['exam']==1){
									$exam="NEET";
								}else if($row['exam']==2){
									$exam="JEE";
								}else if($row['exam']==4){
									$exam="BITSAT";
								}
								$sql1=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='1' and username='".$row['username']."' and exam in (4) order by exam asc");
								$row1=mysqli_fetch_array($sql1);
								$sql2=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='0'  and username='".$row['username']."' and exam in (4) and dusername='admin2' order by exam asc");
								$row2=mysqli_fetch_array($sql2);
								$tot=$row1['cnt']+$row2['cnt'];
								if($tot){
								?>
									<tr>
										<td><?php echo $k; ?></td>
										<td><?php echo $row['username']; ?></td>
										<td>BITSAT</td>
										<td><?php echo $tot; ?></td>
									</tr>
							<?php
							$k++;
								}
							

							}
							$k1=1;
							$sql=query("select id,exam,username from createquestion where  ppaper='1' and exam in (2)  group by username order by exam asc");
							while($row=mysqli_fetch_array($sql)){
								$type = array('1' =>'JEE Mains','2' =>'JEE Advance');
								foreach($type as $key => $stat){
								$sql1=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='1' and username='".$row['username']."' and exam in (2) and pexamtype='".$key."' order by exam asc");
								$row1=mysqli_fetch_array($sql1);
								$sql2=query("select count(id) as cnt from createquestion where  ppaper='1' and estatus='0'  and username='".$row['username']."' and dusername='admin2' and exam in (2) and pexamtype='".$key."' order by exam asc");
								$row2=mysqli_fetch_array($sql2);
								$tot=$row1['cnt']+$row2['cnt'];
								if($tot){
								?>
									<tr>
										<td><?php echo $k; ?></td>
										<td><?php echo $row['username']; ?></td>
										<td><?php echo $stat; ?></td>
										<td><?php echo $tot; ?></td>
									</tr>
							<?php
							$k++;
								}
							}
							

							}
                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>