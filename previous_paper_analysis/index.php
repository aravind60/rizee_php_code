<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
    ?>
		
		<style>
         
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}

		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		</style>
		
		
		<div class="content-wrapper">
			<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails">
							...
						</div>
						
						</div>
					</div>
				</div>
				<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg " role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Question Report</h6>
							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body reject-modal text-left" id="rowReject" >
							
						</div>
						
					</div>
				</div>
			</div>
				<div class="modal fade " id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails1">
						</div>
						
						</div>
					</div>
				</div>
				<div class="modal fade " id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails2">
							
						</div>
						
						</div>
					</div>
				</div>
				<div class="modal fade " id="exampleModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
					<div class="modal-dialog modal-lg" role="document">
						<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="viewDetails3">
							
						</div>
						
						</div>
					</div>
				</div>
           
			<section>
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="adminForm">
							<div class="col-sm-8">
								<script type="text/javascript">
									setStateGet('adminForm','<?php echo SECURE_PATH;?>previous_paper_analysis/process.php','addForm=1');
								</script>
							</div>
						</div>
                   
                </div>
            </section>
			
            <section>
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="adminTable">
							<div class="col-sm-8">
								
							</div>
						</div>
                   
                </div>
            </section>
			

            <!-- End Content-->

            <!-- Scroll to Top Button-->
           <!--  <a class="scroll-to-top rounded" href="#page-top">
                <i class="material-icons">navigation</i>
            </a> -->
        </div>	
<?php
    
    }
?>