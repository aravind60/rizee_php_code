<?php
include('../include/session.php');
error_reporting(0);


function filterqa($str){
    $str = urldecode($str);
    $res = str_replace("&nbsp;","",$str);

    return $res= getAllMath($res);

}


function getAllMath($text){



    $result = "";
    $part1 = explode("<math",$text);

    if(count($part1) > 0){

        $result.= strip_tags($part1[0],'<p><img>');

        foreach($part1 as $first){
            $part2 = explode("</math>",$first);

            if(count($part2) > 1){
                $mml = "<math ".$part2[0]."</math>";


                $svg = json_decode(file_get_contents('http://imgsl.mylearningplus.in/neetjee/edut/tinymce4/plugins/tiny_mce_wiris/integration/showimage.php?mml='.urlencode($mml).'&lang=en'),true);


                $alt = "MathML Question";
                if(isset($svg['result']['alt']))
                    $alt = $svg['result']['alt'];
                $img= '<img src="data:image/svg+xml;base64,'.base64_encode($svg['result']['content']).'" data-mathml="'.str_replace('"','&quot;',$mml).'" class="Wirisformula" role="math" alt="'.$alt.'" style="vertical-align: -5px; height: '.$svg['result']['height'].'px; width: '.$svg['result']['width'].'px;"></img>';

                $result.= $img.strip_tags($part2[1],'<p><img>');

            }
        }
    }

    return $result;
}


$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;


$selu=$database->query("SELECT * from users where username='".$_GET['username']."'");
$rowu=mysqli_fetch_array($selu);

if(isset($_GET['limit'])){
	if($_GET['limit']!=""){
		$limit=$_GET['limit'];
	} else {
		$limit="";
	}
}else{
	$limit="25";
}
if(isset($_GET['start'])){
	if($_GET['start']!=""){
		$start=$_GET['start'];
	} else {
		$start="0";
	}
}else{
	$start="0";
}
	
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script async src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>


		<!-- 
<script async src="../edut/tinymce4/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>

Bootstrap CSS -->
		<link href="<?php echo SECURE_PATH;?>vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
		<style >
			svg{
 vertical-align: -5px;
 
}
            @media print {
                @page { margin: 0.5cm 0cm;
                 size: A4 }
                body { margin: 1.6cm; }
            }

            p:empty {
                display: none;
            }


			.d-flex {
				display: -webkit-box!important;
				display: -ms-flexbox!important;
				display: flex!important;
			}
			.pdf-content .q-no {
				font-size:20px;
				font-family: 'Source Sans Pro', sans-serif !important;
			}

			.pdf-content  .question-header {
				padding-left:5px;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                padding-bottom: 16px
			}
			
			.pdf-content  .question-header p ,
			.pdf-content  .question-header p span{
                margin-block-end: 0.1em;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size: 18px !important;
				font-weight: 600 !important;
			}

            .question-header img{
                max-width:75%;
            }

			.pdf-content p{
			margin-block-start: 0;
    /*margin-block-end: 0;*/
			}

			.question-list  {

			padding-left:5px;
			font-family: 'Source Sans Pro', sans-serif !important;
			font-size:14px !important;
				}
			.question-list .options {
				/* align-items: center; */
				justify-content: center;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                display:inline-block !important;
				width:45%;
				/* float:left; */
                margin-right:16px;
			}
			.pdf-content p, span,
			.pdf-content .question-list p {
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;

			}

			.question-list .options p{
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                margin-block-end: 0em;
			}
            .question-list .options table{
                min-width:240px;
            }

            .question-list .options p img{
                max-width:77%;
            }
			.MsoListParagraph span span[style] {
				display:none;
			}


			.option{
			font-weight:bold;
			padding-left: 8px;
			}

            .question{
                padding-bottom:16px;
            }

            .clearall{
                clear:both;
                padding:0;margin:0;
            }

           /* tbody,tr,.question{
                page-break-inside:avoid;
                page-break-after:auto;
                position:relative;
                display:block;
            }*/
			
		</style>
	</head>
	<body>
		<?php
		if(isset($_GET['exam'])){
			if($_GET['exam']!=""){
				$exam=" AND FIND_IN_SET(".$_GET['exam'].",exam) > 0 ";
			} else {
				$exam="";
			}
		}else{
			$exam="";
		}
		if(isset($_GET['examtype'])){
			if($_GET['examtype']!=""){
				$examtype=" AND pexamtype ='".$_GET['examtype']."' ";
			} else {
				$examtype="";
			}
		}else{
			$examtype="";
		}
		if(isset($_GET['year'])){
			if($_GET['year']!=""){
				$year=" AND year ='".$_GET['year']."' ";
			} else {
				$year="";
			}
		}else{
			$year="";
		}
		if(isset($_GET['set'])){
			if($_GET['set']!=""){
				$set=" AND qset ='".$_GET['set']."' ";
			} else {
				$set="";
			}
		}else{
			$set="";
		}
		if($_GET['userlevel']==3){
			
			$sellq=$database->query("select * from users where username='".$_GET['username']."'");
			$rowq=mysqli_fetch_array($sellq);
			$subjectcon=" AND subject in (".$rowq['subject'].")";
			//echo $subjectcon;
		}else{
			$subjectcon="";
		}

		$con=$exam.$examtype.$year.$set.$subjectcon;
		if($_GET['examtype']=='1'){
			$examt="Mains";
		}else{
			$examt="Advanced";
		}

		$sqlset=$database->query("select * from previous_sets where estatus='1' and id='".$_GET['set']."'");
		$rowset=mysqli_fetch_array($sqlset);
		?>
		<div class="text-center">
			<h4 class="text-center"> <?php echo $rowset['qset']; ?>  Paper Analysis</h4>
		</div>
		<div class="container">
		
			<div class="">			
			<div  border="0"><?php
					$ij=1;
					//echo "select * from createquestion where estatus='1' and ppaper='1'".$con."  order by subject desc";
					$sel=$database->query("select * from createquestion where estatus='1' and ppaper='1'".$con."  order by subject asc");
					while($row=mysqli_fetch_array($sel)){
						$sel_app=$database->query("select * from questiondata_app1 where estatus='1' and question_id='".$row['id']."'  ");
						$rowc=mysqli_num_rows($sel_app);
						if($rowc>0){
							
							$row_app=mysqli_fetch_array($sel_app);
							$row['question']=$row_app['question'];
							$row['option1']=$row_app['option1'];
							$row['option2']=$row_app['option2'];
							$row['option3']=$row_app['option3'];
							$row['option4']=$row_app['option4'];
							$row['explanation']=$row_app['explanation'];
						}else{
							
							$row['question']=filterqa($row['question']);
							$row['option1']=filterqa($row['option1']);
							$row['option2']=filterqa($row['option2']);
							$row['option3']=filterqa($row['option3']);
							$row['option4']=filterqa($row['option4']);
							$row['explanation']=filterqa($row['explanation']);

						}
					?>
					
						
						
								<?php
								$row['exam']=rtrim($row['exam'],',');
								$row['class']=rtrim($row['class'],',');
								$row['chapter']=rtrim($row['chapter'],',');
								$row['topic']=rtrim($row['topic'],',');
								
								if($row['class']=='1'){
									$class='XI';
								}else if($row['class']=='2'){
									$class='XII';
								}else  if($row['class']=='1,2'){
									$class='XI,XII';
								}
								
								$exam ='';
								$sqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$row['exam'].")"); 
								while($rowexam=mysqli_fetch_array($sqlexam)){
									$exam .= $rowexam['exam'].",";
								}

								$chapter ='';
								$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$row['chapter'].")"); 
								while($row2=mysqli_fetch_array($sql)){
									$chapter .= $row2['chapter'].",";
								}

									$topic ='';
									$k=1;
									$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
									while($row1=mysqli_fetch_array($zSql1)){
										$topic .= $row1['topic'].",";
										$k++;
									}
									?>
						
						<div class="row">
						
							
								<div class="col-form-label pt-1 q-no"><?php echo $ij ;?>.</div>
								<span style="padding-left:7px;padding-top:7px;"><h6><b>Question No:  <?php echo $row['id']; ?></b></h6></span>
							
						</div>		
							 
						<div class="row pt-3" >
							<div class="col-md-3 col-sm-3">
								<h6><b>Class</b></h6>
								<span><?php echo $class; ?></span>
							</div>
							
							<div class="col-md-3 col-sm-3">
								<h6><b>Exam</b></h6>
								<span><?php echo rtrim($exam,","); ?></span>
							</div>
							<div class="col-md-3 col-sm-3">
								<h6><b>Subject</b></h6>
								<span><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></span>
							</div>
							<?php
							if(strlen($chapter)>0){
							?>
								<div class="col-md-3 col-sm-3">
									<h6><b>Chapter</b></h6>
									<span><?php echo rtrim($chapter,","); ?></span>
								</div>
							<?php } ?>
						</div>
						<div class="row pt-3">
								<?php
								if($row['qtype']=='5'){
									$type="Comprehension";
								?>
									<div class="col-md-6">
										<h6 class=""><b>Question Type:</b> <?php echo $type; ?></h6>
									</div>
									<div class="col-md-12">
										<h6 class=""><b>Description</b></h6>
										<div><?php echo filterqa($database->get_name('compquestion','id',$row['compquestion'],'compquestion')); ?></div>
									</div>
									<div class="col-md-12  ">
										<h6 class="">Question</b></h6>
										<div  ><?php echo $row['question']; ?></div>
										<div class="row question-list">
											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(A) <span class="pl-2 pb-0"><?php echo $row['option1']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(B)<span class="pl-2 pb-0"><?php echo $row['option2']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(C) <span class="pl-2 pb-0"><?php echo $row['option3']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(D) <span class="pl-2 pb-0"><?php echo $row['option4']; ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-12 mt-2">
										<h6 class="question-header"><b>Answer</b></h6>
										<p><?php echo rtrim($row['answer'],","); ?></p>
											

									</div>
									
									<?php if($row['explanation']!=''){ ?>
									<div class="col-md-12 mt-2">
										<h6><b>Explanation</b></h6>
										<p><?php echo $row['explanation']; ?></p>
											

									</div>
									<?php } ?>
									
								<?php
								}else if($row['qtype']=='8'){
									$type="Integer";
									?>
									<div class="col-md-6">
										<h6 class="question-header"><b>Question Type:</b> <?php echo $type; ?></h6>
									</div>
									<div class="col-md-12 mt-2 questionView">
										<h6 class="question-header"><b>Question</b></h6>
										<div><?php echo $row['question']; ?></div>
									</div>
									
									
									<div class="col-md-12 mt-2 questionView">
										<h6 class="question-header"><b>answer</b></h6>
										<div><?php echo rtrim($row['answer'],","); ?></div>
									</div>
									<?php if($row['explanation']!=''){ ?>
									<div class="col-md-12 mt-2">
										<h6><b>Explanation</b></h6>
										<p><?php echo $row['explanation']; ?></p>
											

									</div>
									<?php } ?>
								<?php
								}else if($row['qtype']=='3'){
									$type="Matching";
								
									$obj=json_decode($row['question'],true);
									
									?>
									<div class="col-md-6">
										<h6 class="question-header"><b>Question Type: <?php echo $type; ?></b></h6>
									</div>
									
									<div class="col-md-12 mt-2 ">
										<h6 class="question-header"><b>Question</b></h6>
										<?php echo filterqa($row['mat_question']); ?>
										<br />
										<div class="questionlist-types d-flex">
											<div class="mr-5">
											<h5>List1</h5>
												<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist)
													{
													
														if(strlen($qqlist['qlist1'])>0)
														{
															echo '<li  class="font-weight-normal" >'.filterqa($qqlist['qlist1']).'</li>';
														}
													}
													?>
												
												</ul>
											</div>
											<div class="ml-5">
												<h5>List2</h5>
												<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist1)
													{
													
														if(strlen($qqlist1['qlist2'])>0)
														{
															echo '<li  class="font-weight-normal" >'.filterqa($qqlist1['qlist2']).'</li>';
														}
													}
													?>
												</ol>
											</div>
										</div>
										<div class="col-md-12 mt-2 ">
										
										<div class="row question-list">
											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(A) <span class="pl-2 pb-0"><?php echo $row['option1']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(B)<span class="pl-2 pb-0"><?php echo $row['option2']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(C) <span class="pl-2 pb-0"><?php echo $row['option3']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(D) <span class="pl-2 pb-0"><?php echo $row['option4']; ?></span>
											</div>
											<?php if($row['option5']!=''){ ?>
											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(E) <span class="pl-2 pb-0"><?php echo $row['option5']; ?></span>
											</div>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-12 mt-2">
										<h6><b>Answer</b></h6>
										<p><?php echo rtrim($row['answer'],","); ?></p>
											

									</div>
									<?php if($row['explanation']!=''){ ?>
									<div class="col-md-12 mt-2">
										<h6><b>Explanation</b></h6>
										<p><?php echo $row['explanation']; ?></p>
											

									</div>
									<?php } ?>
									
									
								<?php
								}else if($row['qtype']=='9'){
									$type="Matrix";
									$obj=json_decode($row['question'],true);
									?>
									<div class="col-md-6">
										<h6><b>Question Type:</b> <?php echo $type; ?></h6>
									</div>
									
									<div class="col-md-12 mt-2 ">
										<h6 class="question-header"><b>Question</b></h6>
										<?php echo filterqa($row['mat_question']); ?>
										<br />
										<div class="questionlist-types d-flex">
											<div class="mr-5">
											<h5>List1</h5>
												<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist)
													{
													
														if(strlen($qqlist['qlist1'])>0)
														{
															echo '<li  class="font-weight-normal" >'.filterqa($qqlist['qlist1']).'</li>';
														}
													}
													?>
												
												</ul>
											</div>
											<div class="ml-5">
												<h5>List2</h5>
												<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist1)
													{
													
														if(strlen($qqlist1['qlist2'])>0)
														{
															echo '<li  class="font-weight-normal" >'.filterqa($qqlist1['qlist2']).'</li>';
														}
													}
													?>
												</ol>
											</div>
										</div>
										<div class="col-md-12 mt-2 ">
										
										<div class="row question-list">
											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(A) <span class="pl-2 pb-0"><?php echo $row['option1']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(B)<span class="pl-2 pb-0"><?php echo $row['option2']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(C) <span class="pl-2 pb-0"><?php echo $row['option3']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(D) <span class="pl-2 pb-0"><?php echo $row['option4']; ?></span>
											</div>
											<?php if($row['option5']!=''){ ?>
											<div class="col-lg-3 col-md-3 col-sm-6 d-flex">
												(E) <span class="pl-2 pb-0"><?php echo $row['option5']; ?></span>
											</div>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-12 mt-2">
										<h6><b>Answer</b></h6>
										<p><?php echo rtrim($row['answer'],","); ?></p>
											

									</div>
									
									<?php if($row['explanation']!=''){ ?>
									<div class="col-md-12 mt-2">
										<h6><b>Explanation</b></h6>
										<p><?php echo $row['explanation']; ?></p>
											

									</div>
									<?php } ?>
									
								<?php
								}else{
									$type="General";
								?>
									<div class="col-md-3">
										<h6><b>Question Type:</b> <?php echo $type; ?></h6>
									</div>
									
									
									<div class="col-md-12 mt-2 ">
										<h6 class="question-header"><b>Question</b></h6>
										<div class="" ><?php echo $row['question']; ?></div>
										<div class="row question-list">
											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(A) <span class="pl-2 pb-0"><?php echo $row['option1']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(B)<span class="pl-2 pb-0"><?php echo $row['option2']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(C) <span class="pl-2 pb-0"><?php echo $row['option3']; ?></span>
											</div>

											<div class="col-lg-3 col-md-3 col-sm-6 py-2 d-flex">
												(D) <span class="pl-2 pb-0"><?php echo $row['option4']; ?></span>
											</div>
										</div>
									</div>
									<div class="col-md-12 mt-2">
										<h6><b>Answer</b></h6>
										<p><?php echo rtrim($row['answer'],","); ?></p>
											

									</div>
									<?php if($row['explanation']!=''){ ?>
									<div class="col-md-12 mt-2">
										<h6><b>Explanation</b></h6>
										<p><?php echo $row['explanation']; ?></p>
											

									</div>
									<?php } ?>
									
									
								<?php
								}
								?>
								<div class="col-md-12">
									<table class="table table-bordered">
										<tr>
											<th nowrap>Chapter Code</th>
											<td><?php if($row['chapter']!=''){ echo $row['chapter']; }else{}?></td>
										</tr>
										<tr>
											<th nowrap>Topic Code</th>
											<td><?php if($row['topic']!=''){ echo $row['topic']; }else{}?></td>
										</tr>
										<?php
										if(strlen($row['topic'])!=0){
											$j=1;
											$topic ='';
											$k=1;
											$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$row['topic'].")"); 
											while($row1=mysqli_fetch_array($zSql1)){
												$topic .= $row1['topic'].",";
												$k++;
											}
										?>
											<tr>
												<th nowrap>Topic Name</th>
												<td><?php echo rtrim($topic,","); ?></td>
											</tr>
										<?php } ?>
										<tr>
											<th>Question Type</th>
											<td>
												<?php
												$mn=1;
												$qtype='';
												$sqll=$database->query("select * from questiontype where estatus='1'  and id IN (".$row['inputquestion'].")");
												while($rowl=mysqli_fetch_array($sqll)){
												$qtype.=$rowl['questiontype'].",";
												$mn++;
												}
												echo rtrim($qtype,",");
												?>
											</td>
										</tr>
										<tr>
											<th nowrap>Complexity</th>
											<td>
												<?php 
												$sql_com=$database->query("select * from complexity where estatus='1' and id='".$row['complexity']."'");
												$row_com=mysqli_fetch_array($sql_com);
												echo $row_com['complexity']; 
												?>	
											</td>
										</tr>
										<tr>
											<th nowrap>Timeduration (In Sec)</th>
											<td><?php echo $row['timeduration']; ?></td>
										</tr>
										<tr>
											<th nowrap>Question Theory</th>
											<td>
												<?php 
												$sql_qth=$database->query("select * from question_theory where estatus='1' and id='".$row['question_theory']."'");
												$row_th=mysqli_fetch_array($sql_qth);
												echo $row_th['question_theory']; 
												?>	
											</td>
										</tr>
										<tr>
											<th nowrap>Usage Set</th>
											<td>
												<?php 
												$sql_usageset=$database->query("select * from question_useset where estatus='1' and id='".$row['usageset']."'");
												$row_us=mysqli_fetch_array($sql_usageset);
												echo $row_us['usageset']; 
												?>	
											</td>
										</tr>
										
										<?php
										if($row['vstatus1']=='1'){
											$vstatus="Verified";
										}else if($row['vstatus1']=='0'){
											$vstatus="Pending";	
										}else if($row['vstatus1']=='2'){
											$vstatus="Rejected";	
										}
										?>
										<tr>
											<th nowrap>Verification status</th>
											<td><?php echo $vstatus; ?></td>
										</tr>
										<tr>
											<th nowrap>Verified Name</th>
											<td><?php echo $row['vusername1']; ?></td>
										</tr>
										<tr>
											<th nowrap>Verified Date&Time</th>
											<td><?php echo date("d/m/Y H:i:s",$row['vtimestamp1']); ?></td>
										</tr>
										<tr>
											<th nowrap>Comments</th>
											<td></td>
										</tr>
										
									
									</table>
								</div>
							</div>
								
								
							
					
							
				
				</div>
				<div style="page-break-after: always;" ></div>
					
					<?php  $ij++; } ?>
		</div>
		</div>


	</body>
</html>