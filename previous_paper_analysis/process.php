<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>

<style>
.card-header h6{
font-family: 'Varela Round', sans-serif !important;
}
.card{
	font-family: 'Varela Round', sans-serif !important;
}
</style>
	<?php
	$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
	//$con = mysqli_connect("localhost","root","","neetjee");
		function query($sql){
		global $con;
		 return mysqli_query($con,$sql);
		}
	if(isset($_REQUEST['addForm'])){
		
		$sql=$database->query("select * from users where username='".$_SESSION['username']."'");
		$rowsel=mysqli_fetch_array($sql);
	
	?>
		
	<script>
	$('#exampaper').selectpicker1();
	</script>
	
	
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Previous  Paper Analysis</h6>
							</div>
							<div class="card-body">
								
								<div class="form-row">
									
									<div class="col-md-3 mb-3">
									  <label for="validationCustom02">Exam</label>
										<select id="exam" name="exam" class="form-control"  onclick="examtypediv();" onchange="setState('examyeardiv','<?php echo SECURE_PATH;?>previous_paper_analysis/ajax.php','examyeardata=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'');setState('examnamediv','<?php echo SECURE_PATH;?>previous_paper_analysis/ajax.php','examnamedata=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'');" >
											<option value=''>-Select-</option>
											<?php
											if($session->userlevel==3){
												if($rowsel['subject']=='4'){
													$sql=$database->query("select * from exam where estatus='1' and id!=1 order by id asc");
												}else{
													$sql=$database->query("select * from exam where estatus='1'  order by id asc");
												}
											}else{
												$sql=$database->query("select * from exam where estatus='1' order by id asc");
											}
											//$sql=$database->query("select * from exam where estatus='1'");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['id']; ?>' <?php if(isset($_REQUEST['exam'])) { if($_REQUEST['exam']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['exam']; ?></option>
											<?php
												
											}
											?>
										</select>
									  
									</div>
									<?php
									if(isset($_REQUEST['examtype'])){
										if($_REQUEST['examtype']!=''){
											if($_REQUEST['examtype']=='2'){
												$style_examtype='';
											}else{
												$style_examtype='style="display:none;"';
											}
											
										}else{
											$style_examtype='style="display:none;"';
										}
									}else{
										$style_examtype='style="display:none;"';
									}
									?>
									<div class="col-md-3 mb-3 " <?php echo $style_examtype; ?>  id="examtypedatadiv" onchange="setState('examyeardiv','<?php echo SECURE_PATH;?>previous_paper_analysis/ajax.php','examyeardata=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'');setState('examnamediv','<?php echo SECURE_PATH;?>previous_paper_analysis/ajax.php','examnamedata=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'');" >
									  <label for="validationCustom02">Exam Type</label>
									  <select id="examtype" name="examtype" class="form-control" >
											<option value=''>-Select-</option>
											<option value='1' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']==1) { echo 'selected="selected"'; }  } ?>>Mains</option>
											<option value='2' <?php if(isset($_REQUEST['examtype'])) { if($_REQUEST['examtype']==2) { echo 'selected="selected"'; }  } ?>>Advance</option>
										</select>
									  
									</div>
									<div class="col-md-3 mb-3" id="examyeardiv">
									  <label for="validationCustom02">Year</label>
										<select id="year" name="year" class="form-control"   onchange="setState('examyeardiv','<?php echo SECURE_PATH;?>previous_paper_analysis/ajax.php','examyeardata=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'');" >
											<option value=''>-Select-</option>
											<?php
											$sql=$database->query("select * from previous_questions where estatus='1' group by year");
											while($row=mysqli_fetch_array($sql)){
											?>
												<option value='<?php echo $row['year']; ?>' <?php if(isset($_REQUEST['year'])) { if($_REQUEST['year']==$row['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row['year']; ?></option>
											<?php
												
											}
											?>
										</select>
									  
									</div>
									<div class="col-md-3 mb-3"  id="examnamediv" >
									  <label for="validationCustom02"><span style="color:red;">*</span>Exam Name</label>
									 <select id="exampaper" name="exampaper" class="form-control selectpicker1"  >
									 	<option value=''>-Select-</option>
											<?php
											$sql=$database->query("select * from previous_questions where estatus='1' ");
											while($row=mysqli_fetch_array($sql)){
												
												$sql2=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
												while($row2=mysqli_fetch_array($sql2)){


												?>
													<option value='<?php echo $row2['id']; ?>' <?php if(isset($_REQUEST['exampaper'])) { if($_REQUEST['exampaper']==$row2['id']) { echo 'selected="selected"'; }  } ?>><?php echo $row2['qset']; ?></option>
												<?php
												}
												
												
											}
											?>
										</select>
										<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['exampaper'])){ echo $_SESSION['error']['exampaper'];}?></span>
									</div>
								 </div>
								 <div class="text-right col-xl-12 col-lg-12 col-md-12 col-sm-12">
									<a type="button" class="btn-blue my-2 px-5 btn btn-primary text-white" onclick="setState('adminTable','<?php echo SECURE_PATH;?>previous_paper_analysis/process.php','tableDisplay=1&exam='+$('#exam').val()+'&examtype='+$('#examtype').val()+'&year='+$('#year').val()+'&exampaper='+$('#exampaper').val()+'');">Submit</a>
								</div> 
								 
									
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
		<?php
			unset($_SESSION['error']);
	}
		?>
	<?php
		if(isset($_REQUEST['tableDisplay'])){
			$field = 'exampaper';
			if(!$_REQUEST['exampaper'] || strlen(trim($_REQUEST['exampaper'])) == 0){
			  $_SESSION['error'][$field] = "* Please select Exampaper";
			}
			
			if(count($_SESSION['error']) > 0){
			?>
				<script type="text/javascript">
				$('#adminForm').slideDown();

				setState('adminForm','<?php echo SECURE_PATH;?>previous_paper_analysis/process.php','addForm=1&exampaper=<?php echo $_REQUEST['exampaper'];?>&exam=<?php echo $_REQUEST['exam'];?>&year=<?php echo $_REQUEST['year']; ?>&examtype=<?php echo $_REQUEST['examtype']; ?>')
				</script>

			<?php
			}else{
				
						if(isset($_REQUEST['year'])){
							if($_REQUEST['year']!=''){
								$year=" AND year='".$_REQUEST['year']."'";
								
							}else{
								$year="";
								
							}
						}else{
							$year="";
							
						}
						if(isset($_REQUEST['exam'])){
							if($_REQUEST['exam']!=''){
								$exam=" AND find_in_set(".$_REQUEST['exam'].",exam)>0";
								
							}else{
								$exam="";
								
							}
						}else{
							$exam="";
							
						}
						if(isset($_REQUEST['examtype'])){
							if($_REQUEST['examtype']!=''){
								$examtype=" AND pexamtype='".$_REQUEST['examtype']."'";
								
							}else{
								$examtype="";
								
							}
						}else{
								$examtype="";
								
								
							}

						if(isset($_REQUEST['exampaper'])){
							if($_REQUEST['exampaper']!=''){
								$exampaper=" AND qset IN (".$_REQUEST['exampaper'].")";
								
							}else{
								$exampaper="";
								
							}
						}else{
								$exampaper="";
								
						}
						$sqle=$database->query("select pid from previous_sets where estatus='1' and id='".$_REQUEST['exampaper']."'");
						$rowe=mysqli_fetch_array($sqle);
						$sqle1=$database->query("select * from previous_questions where estatus='1' and id='".$rowe['pid']."'");
						$rowe1=mysqli_fetch_array($sqle1);
						if($rowe1['exam']=='1'){
							$sub="1,2,3,5";
						}else if($rowe1['exam']=='2'){
							$sub="2,3,4";
						}else{
							$sub="1,2,3,4,5";
						}
						
						?>
						<script type="text/javascript">
						$('#example').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});

						$('#example1').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});

						$('#example2').DataTable({
							"paging":   false,
							"searching": false,
							"bInfo": false
							
						});
						
						</script>
						<div class="container">
							<div class="text-right pt-2 pb-2" >
							<a href="<?php echo SECURE_PATH;?>previous_paper_analysis/report.php?exam=<?php echo $_REQUEST['exam']; ?>&examtype=<?php echo $_REQUEST['examtype']; ?>&year=<?php echo $_REQUEST['year']; ?>&set=<?php echo $_REQUEST['exampaper']; ?>&userlevel=<?php echo $session->userlevel; ?>&username=<?php echo $session->username; ?>" style="float:center" target="_blank"  title="PDF Export" ><i class="text-center fa fa-file-pdf-o" style="font-size: 25px;color:red;"></i></a>
							</div>
						</div>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Chapter Wise Weightage</h6>
											</div>
											<div class="card-body">
												
												<div class="accordion" id="accordionExample">
													<?php
													$i=1;
														$sql=$database->query("select * from subject where estatus='1'  and id in (".$sub.")");
														while($row=mysqli_fetch_array($sql)){
															if($i==1){
																$classd="show";
																$icon="fa-minus";
															}else{
																$classd="";
																$icon="fa-plus";
															}
															if($row['id']=='1'){
																$image=SECURE_PATH."images/botany.png";
															}else if($row['id']=='2'){
																$image=SECURE_PATH."images/physics.png";
															}else if($row['id']=='3'){
																$image=SECURE_PATH."images/chemistry.png";
															}else if($row['id']=='4'){
																$image=SECURE_PATH."images/maths.png";
															}else if($row['id']=='5'){
																$image=SECURE_PATH."images/zoology.png";
															}
													?>
													<script>
													$('#dataTable<?php echo $row['id']; ?>').DataTable({
														"paging":   false,
														"searching": false,
														"bInfo": false
														
													});
													</script>
													
													<div class="card">
														<div class="card-header "  id="heading<?php echo $i; ?>" onclick="hideshow();">
														<h2 class="mb-0">
															<button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?php echo $i; ?>" aria-expanded="true" aria-controls="collapseOne">
															<?php echo $row['subject']; ?>
															</button><i 
														</h2>
														</div>

														<div id="collapse<?php echo $i; ?>" class="collapse <?php echo $classd; ?>" aria-labelledby="heading<?php echo $i; ?>" data-parent="#accordionExample">
														<div class="card-body">
														<table id="dataTable<?php echo $row['id']; ?>" class="table table-striped table-bordered" style="width:100%">
																	<thead>
																		<tr>
																			
																			<th>Class</th>
																			<th>Chapter</th>
																			<th>Qstn</th>
																			<th>Percentage</th>
																		</tr>
																		</tr>
																	</thead>
																	<tbody>
																		
																		
																	
																		
																		<?php
																		$i=1;
																		$sqlcom=$database->query("select * from chapter where  estatus='1' and subject='".$row['id']."'");
																		while($rowcom=mysqli_fetch_array($sqlcom)){
																			
																			$sqlqs=$database->query("select count(id) as cnt from createquestion where estatus='1'  and ppaper='1'  and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype."");
																			$rowqs=mysqli_fetch_array($sqlqs);

																			if($rowcom['class']=='1'){
																				$class="XI";
																			}else{
																				$class="XII";
																			}
																			$sqlq1_tot=$database->query("select count(id) as cnt from createquestion where estatus='1'  and ppaper='1' and  find_in_set(".$rowcom['id'].",chapter)>0 and subject='".$row['id']."' ".$inst_con.$exam.$exampaper.$examtype."");
																			$rowq1_tot=mysqli_fetch_array($sqlq1_tot);
																			$per=round(($rowq1_tot['cnt']/$rowqs['cnt'])*100);
																			
																			if(is_nan($per)){
																				$per1=0;
																			}else{
																				$per1=$per;
																			}
																			if($rowq1_tot['cnt']>0){
																		?>
																		<tr>
																			<td><?php echo $class; ?></td>
																			<td><?php echo $rowcom['chapter']; ?></td>
																			<td><?php echo $rowq1_tot['cnt']; ?></td>
																			<?php
																			
																		
																			//echo $rowq1_tot['cnt'];
																			
																			
																			
																			?>
																				<td><?php echo $per1; ?> %</td>
																				
																		</tr>
																		
																		<?php
																			$i++;
																		}
																		}
																		?>
																	
																	
																	</tbody>	
																	
																	
																</table>
														</div>
														</div>
													</div>
													<?php $i++; } ?>  
													
												</div>
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Complexity</h6>
											</div>
											<div class="card-body">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Complexity</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("select * from complexity where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															
															
															$sqlee=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1' and complexity='".$rowcom['id']."'  ".$year.$exam.$exampaper.$examtype." ");
															$rowcount=mysqli_fetch_array($sqlee);
															
															
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['complexity']; ?></td>
															<td><?php echo $rowcount['cnt']; ?></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																

																$sqlq1=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1'  and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype."");
																$rowqcom1=mysqli_fetch_array($sqlq1);
																
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1'  and complexity='".$rowcom['id']."' and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype." ");
																$rowcountqtype=mysqli_fetch_array($sqlee1);
																$per=round(($rowcountqtype['cnt']/$rowqcom1['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}
																?>
																	<td><?php echo $rowcountqtype['cnt']; ?> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																	<?php

																
															}
															?>
														</tr>
														
														<?php
															$i++;
														}
														?>
													
													
												</table>
												
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Question Type</h6>
											</div>
											<div class="card-body">
											<table id="example" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Question Type</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("select * from questiontype where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															
															$sqleeqt=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1'  and find_in_set(".$rowcom['id'].",inputquestion)>0 and subject!=''  ".$year.$exam.$exampaper.$examtype."");
															$rowcountqt=mysqli_fetch_array($sqleeqt);
															if($rowcountqt['cnt']>0){
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['questiontype']; ?></td>
															<td><?php echo $rowcountqt['cnt']; ?></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																$sqlq11=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1'  and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype."");
																$rowqcom11=mysqli_fetch_array($sqlq11);
																
																
																$sqlee1=$database->query("select count(id) as cnt from createquestion where estatus='1'  and ppaper='1' and find_in_set(".$rowcom['id'].",inputquestion)>0 and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype." ");
																$rowcountqthoey=mysqli_fetch_array($sqlee1);
																$per=round(($rowcountqthoey['cnt']/$rowqcom11['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}
																
																	?>
																	<td><?php echo $rowcountqthoey['cnt']; ?> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																	<?php
																
															}
															?>
														</tr>
														
														<?php
															$i++;
														}
													}
														?>
													
													
												</table>
												
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>

						

						<section class="content-area">
							<div class="container">
								<div class="row Data-Tables">
									<div class="col-xl-12 col-lg-12"> 
										<div class="card border-0 shadow mb-4">
											<div class="card-header py-3">
												<h6 class="m-0 font-weight-bold text-primary">Question Theory</h6>
											</div>
											<div class="card-body">
												
												<table id="example1" class="table table-striped table-bordered" style="width:100%">
													<thead>
														<tr>
															<th>Question Theory</th>
															<th>Qstn</th>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																echo '<th>'.$row['subject'].'</th>';
															}
															?>
														</tr>
													</thead>
													<tbody>
														
														<?php
															$i=1;
														$sqlcom=$database->query("SELECT * FROM question_theory where  estatus='1'");
														while($rowcom=mysqli_fetch_array($sqlcom)){
															
															
															$sqlee=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1' and   question_theory='".$rowcom['id']."' and subject!='' ".$year.$exam.$exampaper.$examtype." ");
															$rowcount=mysqli_fetch_array($sqlee);
														?>
														<tr class="<?php echo $style; ?>">
															<td><?php echo $rowcom['question_theory']; ?></td>
															<td><?php echo $rowcount['cnt']; ?></td>
															<?php
															$sql=$database->query("select * from subject where estatus='1' and id in (".$sub.")");
															while($row=mysqli_fetch_array($sql)){
																
																$sqleeqt13=$database->query("select count(id) as cnt from createquestion where estatus='1' and ppaper='1'  and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype."");
																$rowcountqt13=mysqli_fetch_array($sqleeqt13);
																
																$sqlee_qst=$database->query("select count(id) as cnt from createquestion where estatus='1'  and ppaper='1'  and question_theory='".$rowcom['id']."' and subject='".$row['id']."' ".$year.$exam.$exampaper.$examtype." ");
																$rowcount_qst=mysqli_fetch_array($sqlee_qst);
																$per=round(($rowcount_qst['cnt']/$rowcountqt13['cnt'])*100);
																if(is_nan($per)){
																	$per1=0;
																}else{
																	$per1=$per;
																}	

																?>
																	<td><?php echo $rowcount_qst['cnt']; ?> <small style="color:blue;">(<?php echo $per1; ?> %)</small></td>
																	<?php
															}
															?>
														</tr>
														
														<?php
															$i++;
														}
														?>
													
													
												</table>
												
											</div>
											
											
										</div>
									</div>
								</div>

							</div>
						</section>
						
						
				<?php	
				
			}
	}		
	?>
	<script>
	function examtypediv(){
		var exam=$('#exam').val();
		if(exam=='2'){
			$('#examtypedatadiv').show();
		}else{
			$('#examtypedatadiv').hide();
		}

	}
	
	</script>
