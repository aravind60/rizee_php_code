<?php
    error_reporting(0);
    include('../include/session.php');
	?>
	
	<?php
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	header('Location: '.SECURE_PATH);
    }
    else
    {
        //unset($_SESSION['image']);  
    ?>
    
   
		<style>
         /* .wrs_editor .wrs_formulaDisplay {
             height: 250px !important;
         } */
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}
		</style>
		<style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				/*margin:auto;*/
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}
			@media (max-width:767px){
			.web-enable-buttons {
				    display: none !important;
				}
				.mobile-enable-buttons {
				    display: table-cell !important;
				}
			}
		</style>
		
		
		<div class="content-wrapper">
                <!-- End Breadcrumbs-->
				<!-- Content Area-->
					<div class="toast text-center mx-auto" id="verifytoast"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"  style="position: absolute;
						top: 45%;left:0;right:0" >

					  <!-- Then put toasts within -->
						<div  role="alert" aria-live="assertive" aria-atomic="true">
							<div class="toast-header">
								<strong class="mr-auto">Question</strong>
						 
								  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								  </button>
							</div>
							<div class="toast-body p-5">
							 Question Verified Successfully
							</div>
					  </div>
					</div>
					<div class="toast text-center mx-auto" id="savedtoast"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"   style="position: absolute;
						top: 45%;left:0;right:0">

					  <!-- Then put toasts within -->
					  <div  role="alert" aria-live="assertive" aria-atomic="true">
						<div class="toast-header">
						  <strong class="mr-auto">Question</strong>
						 
						  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						<div class="toast-body p-5">
						 Question Updated Successfully
						</div>
					  </div>
					</div>
					<div class="toast text-center mx-auto" id="failedtoast"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"   style="position: absolute;
						top: 45%;left:0;right:0">

					  <!-- Then put toasts within -->
					  <div  role="alert" aria-live="assertive" aria-atomic="true">
						<div class="toast-header">
						  <strong class="mr-auto">Question</strong>
						 
						  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						  </button>
						</div>
						<div class="toast-body p-5">
						 Question Updation Failed..!
						</div>
					  </div>
					</div>
					<div class="toast text-center mx-auto" id="rowdeletetoast"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"   style="position: absolute;
    top: 45%;left:0;right:0">

		  <!-- Then put toasts within -->
		  <div  role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
			  <strong class="mr-auto">Question</strong>
			 
			  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="toast-body p-5">
			 Question Deleted Successfully
			</div>
		  </div>
		</div>

				<section class="description-table">
                <div class="container" >
                    <div class="card border-0 shadow-sm mb-3">
                        <div class="form-group mb-0" id="adminForm">
								<div class="col-sm-8">
									<script type="text/javascript">
										setStateGet('adminForm','<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','addForm=1');
									</script>
								</div>
							</div>
                    </div>
                </div>
				<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg " role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Question Report</h6>
							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body reject-modal text-left" id="rowReject" >
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="modal fade" id="examplerowReject" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg " role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Rejected Question Report</h6>
							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body text-left" id="rowReject1" >
							
						</div>
						
					</div>
				</div>
			</div>
			<div class="modal fade " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Question Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="viewDetails">
					</div>
					 
					</div>
				</div>
			</div>
			<div class="modal fade " id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Custom Content Details</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="viewDetails1">
					</div>
					 
					</div>
				</div>
			</div>
			<div class="modal fade " id="exampleModal4" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-lg" role="document">
					<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Reason for Question Deletion</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body" id="viewDetails4">
					</div>
					 
					</div>
				</div>
			</div>
			<div class="modal fade" id="examplerowReject" tabindex="-1" role="dialog"
				aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg " role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h6 class="modal-title" id="exampleModalLabel">Rejected Question Report</h6>
							<button type="button" class="close text-danger" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body text-left" id="rowReject1" >
							
						</div>
						
					</div>
				</div>
			</div>
            </section>
				 <section id="datafill">
					<div id="adminTable">
						<script type="text/javascript">
						setStateGet('adminTable','<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
						</script>
					</div>
				</section>
				
				

				

			</div>
		
	<?php
		
		}
	?>
	<script type="text/javascript">
	function addList(){
		cnt = $('#session_list').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list').val(new_cnt);
	
		html = '<div class="list" id="list'+new_cnt+'"></div>';	
   
		$('#dates_list').append(html);
   
		setStateGet('list'+new_cnt,'<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','add_listing='+new_cnt);
	}
	function removeList(id){

		$('#list'+id).remove();
	}

	function calconditions(){
		var newarray = [];
		i=1;
        $('#dates_list .list').each(function(){
			var retval = {};
			console.log(tinymce.get('question'+i).getContent());
			
			retval.question= encodeURIComponent(tinymce.get('question'+i).getContent());
			retval.option1= encodeURIComponent(tinymce.get('option1'+i).getContent());
			retval.option2= encodeURIComponent(tinymce.get('option2'+i).getContent());
			retval.option3= encodeURIComponent(tinymce.get('option3'+i).getContent());
			retval.option4= encodeURIComponent(tinymce.get('option4'+i).getContent());
			retval.explanation= encodeURIComponent(tinymce.get('explanation'+i).getContent());
			retval.comid= $(this).find('.comid').val();
			retval.answer= $(this).find('.answer').val();
			newarray.push(retval);
			console.log('new',newarray);
			i++;
		});
		console.log('new',newarray);

		 $.ajax({
			type: 'POST',
			url: '<?php echo SECURE_PATH;?>que_chaptertopic_admin/img2.php',
			data:JSON.stringify(newarray),
			contentType: 'application/json; charset=utf-8',
					
			dataType: 'json',
			success: function (data) {
					console.log("data4");
					
			},
				error: function(e){	

			console.log("ERROR: ", e);
			}
		});
		
	}
	function addList1(){
		cnt = $('#session_list1').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list1').val(new_cnt);
	
		html = '<div class="list1" id="list1'+new_cnt+'"></div>';	
   
		$('#dates_list1').append(html);
   
		setStateGet('list1'+new_cnt,'<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','add_listing1='+new_cnt);
	}
	function removeList1(id){

		$('#list1'+id).remove();
	}
	/*function calconditions1(){
		var retval = '';
		i=0;
        $('#dates_list1 .list1').each(function(){
			retval+= $(this).find('.qlist1').val()+'_'+$(this).find('.qlist2').val()+'^';
		});
		return retval;
		
	}*/
	function calconditions1(){
		
		var newarray = [];
		i=1;
        $('#dates_list1 .list1').each(function(){
			var retval = {};
			//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
			retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
			retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

			newarray.push(retval);
			console.log("kkk"+retval);
			i++;

		});

		console.log('new',newarray);

		 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>que_chaptertopic_admin/img1.php',
					data:JSON.stringify(newarray),
					contentType: 'application/json; charset=utf-8',
							
					dataType: 'json',
					success: function (data) {
							console.log("data4");
							
					},
						error: function(e){	

					console.log("ERROR: ", e);
					}
				});
		//return newarray;
		
	}
	function addList2(){
		cnt = $('#session_list2').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#session_list2').val(new_cnt);
	
		html = '<div class="list2" id="list2'+new_cnt+'"></div>';	
   
		$('#dates_list2').append(html);
   
		setStateGet('list2'+new_cnt,'<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','add_listing2='+new_cnt);
	}
	function removeList2(id){

		$('#list2'+id).remove();www
	}
	function calconditions2(){
		
		var newarray = [];
		i=1;
         $('#dates_list2 .list2').each(function(){
			var retval = {};
			//retval+= encodeURIComponent(tinymce.get('qlist1'+i).getContent())+'_'+encodeURIComponent(tinymce.get('qlist2'+i).getContent())+'^';
			retval.qlist1= encodeURIComponent(tinymce.get('qlist1'+i).getContent());
			retval.qlist2= encodeURIComponent(tinymce.get('qlist2'+i).getContent());

			newarray.push(retval);
			console.log("kkk"+retval);
			i++;

		});

		console.log('new',newarray);

		 $.ajax({
					type: 'POST',
					url: '<?php echo SECURE_PATH;?>que_chaptertopic_admin/img1.php',
					data:JSON.stringify(newarray),
					contentType: 'application/json; charset=utf-8',
							
					dataType: 'json',
					success: function (data) {
							console.log("data4");
							
					},
						error: function(e){	

					console.log("ERROR: ", e);
					}
				});
	}

	function issuedata(){
		var retval = '';
		i=0;
        $('#issuelist .ilist').each(function(){
			retval+= $(this).find('.issue_section').val()+'_'+$(this).find('.issue_type').val()+'_'+$(this).find('.rissue').val()+'_'+$(this).find('.vremarks').val()+'^';
		});
		return retval;
		
	}

	function addissueList(){
		cnt = $('#issue_llist').val();
		new_cnt = parseInt(cnt)+1;
	
		$('#issue_llist').val(new_cnt);
	
		html = '<div class="ilist" id="ilist'+new_cnt+'"></div>';	
   
		$('#issuelist').append(html);
   
		setStateGet('ilist'+new_cnt,'<?php echo SECURE_PATH;?>que_chaptertopic_admin/process.php','add_issuelist='+new_cnt);
	}
	function removeissueList(id){

		$('#ilist'+id).remove();
	}

	function NumAndTwoDecimals(e) {
		//alert(e);
		if (e.which != 46 && e.which != 45 && e.which != 46 &&
			  !(e.which >= 48 && e.which <= 57)) {
			return false;
		  }
		}
	</script>