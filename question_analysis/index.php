<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
    ?>
		
		<style>
         
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}

		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		.modal-backdrop.show {
			/* opacity: 99999999999999999999999; */
		}
		</style>
		
		<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Question Review
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>
				
				
		<div class="content-wrapper">
			
			<div class="container-fluid p-0">
				<div class="modal fade"  id="reviewquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" >
				  <div class="modal-dialog modal-xl" role="document">

					<div class="modal-content">
						<div class="modal-header">
								
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" id="viewDetails2">
								
							</div>
					</div>
				  </div>
				</div>
				<div class="form-group" id="adminForm">
					<div class="col-sm-8">
						<script type="text/javascript">
							setStateGet('adminForm','<?php echo SECURE_PATH;?>question_analysis/process.php','addForm=1');
						</script>
					</div>
				</div>
				
			   
			</div>
		</div>
		<script src="<?php echo SECURE_PATH;?>question_analysis/Chosen/chosen.jquery.js" type="text/javascript"></script>
    
    <script type="text/JavaScript">
        $(".chosen-select").chosen();

        $(document).on("click",".questionWiseCls li",function(e){
            $(this).closest("ul").find("li").removeClass("active");
            $(this).addClass("active");

            $(".selectedNameCls").html($(this).html());
        });    
    </script>	
<?php
    
    }
?>