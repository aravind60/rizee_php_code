<?php

ini_set('display_errors','0');

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions_attributeoverview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter Name</th>
							<th style="text-align:center;">Chapter Id</th>
							<th style="text-align:center;">Topic Name</th>
							<th style="text-align:center;">Topic Id</th>
							<th style="text-align:center;">Verified Questions</th>
							<th style="text-align:center;">Complexity</th>
							<th style="text-align:center;">Time Duration(Seconds)</th>
							<th style="text-align:center;">Type of Question</th>
							<th style="text-align:center;">Usage Set</th>
							<th style="text-align:center;">Question Theory</th>
							<th style="text-align:center;">Custom Content</th>
							<th style="text-align:center;">Lecturers</th>
							
						</tr>
                        <?php
                            $k=1;
                           
                               
                               
								   	$topic_sel = query("SELECT * FROM topic WHERE estatus=1  ORDER BY subject,chapter ASC");
									$topicount=mysqli_num_rows($topic_sel);
									if($topicount>0){
									 while($topic = mysqli_fetch_array($topic_sel)){
										 	$class_sel=query("SELECT id,class FROM class WHERE estatus=1 and id ='".$topic['class']."'");
											$class_row = mysqli_fetch_array($class_sel);
													
											$sub= query("SELECT * FROM subject WHERE estatus=1 AND id ='".$topic['subject']."'");
											$rowsub = mysqli_fetch_array($sub);

										 	$chapter_sel = query("SELECT id,class,subject,chapter FROM chapter WHERE estatus=1  and id='".$topic['chapter']."' ORDER BY subject ASC");
											$chapter = mysqli_fetch_array($chapter_sel);
										
											$sqlltv=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0 order by subject ASC ");
											$rowlcounttv=mysqli_fetch_array($sqlltv);
											
											$sqlltcom=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and (complexity!='' and complexity!='0') order by subject ASC ");
											$rowlcounttcom=mysqli_fetch_array($sqlltcom);

											$sqllttime=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and timeduration!='' order by subject ASC ");
											$rowlcountttime=mysqli_fetch_array($sqllttime);

											$sqllqtype=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and (inputquestion!='' and inputquestion!='0') order by subject ASC ");
											$rowlqtype=mysqli_fetch_array($sqllqtype);

											$sqlluseset=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and (usageset!='' and usageset!='0') order by subject ASC ");
											$rowluset=mysqli_fetch_array($sqlluseset);

											$sqllqtheory=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and (question_theory!='' and question_theory!='0') order by subject ASC ");
											$rowlqtheory=mysqli_fetch_array($sqllqtheory);

											$sqllconttype=query("select count(id) as count from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  and (conttype!='' and conttype!='0') order by subject ASC ");
											$rowlconttype=mysqli_fetch_array($sqllconttype);

											
											$sqllusers=query("select id,vusername from createquestion where estatus='1' and vstatus='1' and   FIND_IN_SET(".$chapter['class'].",class)  > 0 and  FIND_IN_SET(".$chapter['subject'].",subject)  > 0  and FIND_IN_SET(".$chapter['id'].",chapter)  > 0 and FIND_IN_SET(".$topic['id'].",topic)  > 0  ");
											$rowlusercount=mysqli_num_rows($sqllusers);
											$users='';
											if($rowlusercount>0){
												while($rowlusers=mysqli_fetch_array($sqllusers)){
												$users.=$rowlusers['vusername'].",";
												}
											}
											
											$userdata=array_unique(explode(",",$users));
											$userdata1=implode(",",$userdata);

											if($rowlcounttv['count']>0){
											echo "<tr>";
												?>	
												
												<td><?php echo $k;?></td>
												<td ><?php echo $class_row['class'];?></td>
												<td><?php echo $rowsub['subject']; ?></td>
												<td><?php echo $chapter['chapter'];?></td>
												<td><?php echo $chapter['id'];?></td>
												<td><?php echo $topic['topic']; ?></td>
												<td><?php echo $topic['id']; ?></td>
												<td><?php echo $rowlcounttv['count']; ?></td>
												<td><?php echo $rowlcounttcom['count']; ?></td>
												<td><?php echo $rowlcountttime['count']; ?></td>
												<td><?php echo $rowlqtype['count']; ?></td>
												<td><?php echo $rowluset['count']; ?></td>
												<td><?php echo $rowlqtheory['count']; ?></td>
												<td><?php echo $rowlconttype['count']; ?></td>
												<td><?php echo rtrim($userdata1,","); ?></td>
											<?php
											
											echo "</tr>";
											$k++;
											}
										 
										}
									}
										
										
											
										          
                                    
									
                                
                           

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>