<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
//$con = mysqli_connect("localhost","root","","neetjee");
function query($sql){
global $con;
 return mysqli_query($con,$sql);
}
?>
<script>
 $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
</script>
<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
		?>
	 <script>
		$('html, body').animate({
		   scrollTop: $("#myDiv").offset().top
	   }, 2000);
	  </script>
	 <?php
    $data_sel = query("SELECT * FROM referral_codes WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
	$_POST['discount']=$_POST['value'];
	$_POST['fromdate']=date('d-m-Y', $_POST['fromdate']);
	$_POST['expiry_date']=date('d-m-Y', $_POST['expiry_date']);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>

  <div class="col-lg-12 col-md-12" id="myDiv">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Referral Code<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="referral_code" id="referral_code" value="<?php if(isset($_POST['referral_code'])) { echo $_POST['referral_code']; }else{} ?>" autocomplete="off" >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['referral_code'])){ echo $_SESSION['error']['referral_code'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Description<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<textarea name="description" id="description" class="form-control" rows="2" ><?php if(isset($_POST['description'])) { echo $_POST['description']; }else{} ?></textarea>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['description'])){ echo $_SESSION['error']['description'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label"> Discount Type<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="type" class="form-control" id="type">
						<option  value="">-Select-</option>
						<option value="0" <?php if(isset($_POST['type'])){ if($_POST['type'] == 0){ echo ' selected="selected"';};}?>>Percentage</option>
						<option value="1" <?php if(isset($_POST['type'])){ if($_POST['type'] == 1){ echo ' selected="selected"';};}?>>Cutoff</option>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['type'])){ echo $_SESSION['error']['type'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Discount<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="discount" id="discount" value="<?php if(isset($_POST['discount'])) { echo $_POST['discount']; }else{} ?>" autocomplete="off" >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['discount'])){ echo $_SESSION['error']['discount'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">From Date<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="fromdate" id="fromdate" class="datepicker form-control" value="<?php if(isset($_POST['fromdate'])) { echo $_POST['fromdate']; }else{} ?>" onblur="search_report();" placeholder="Select Date" >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['fromdate'])){ echo $_SESSION['error']['fromdate'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">To Date<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="expiry_date" id="expiry_date" class="datepicker form-control" value="<?php if(isset($_POST['expiry_date'])) { echo $_POST['expiry_date']; }else{} ?>" onblur="search_report();" placeholder="Select Date" >
					<span class="error text-danger"><?php if(isset($_SESSION['error']['expiry_date'])){ echo $_SESSION['error']['expiry_date'];}?></span>
				</div>
			</div>
		</div>
	
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">User Type<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="user_type" class="form-control" id="user_type"  onchange="setState('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1&referral_code='+$('#referral_code').val()+'&description='+$('#description').val()+'&type='+$('#type').val()+'&discount='+$('#discount').val()+'&fromdate='+$('#fromdate').val()+'&expiry_date='+$('#expiry_date').val()+'&user_type='+$('#user_type').val()+'')">
						<option  value="">-Select-</option>
						<option value="6" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 6){ echo ' selected="selected"';};}?>>Reviewer</option>
						<option value="3" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 3){ echo ' selected="selected"';};}?>>Lecturer</option>
						<option value="2" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 2){ echo ' selected="selected"';};}?>>Student</option>
						<option value="1" <?php if(isset($_POST['user_type'])){ if($_POST['user_type'] == 1){ echo ' selected="selected"';};}?>>Admin</option>
					</select>
					<span class="error text-danger"><?php if(isset($_SESSION['error']['user_type'])){ echo $_SESSION['error']['user_type'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Plan<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<select name="plan_id" id="plan_id" class="form-control selectpicker1" value=""   multiple   data-actions-box="true" data-live-search="true" >
						<option value=''>-Select-</option>
						<?php
						$row = query("select * from student_plans where estatus='1'   ");
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['plan_id'])) { if(in_array($data['id'], explode(",",$_POST['plan_id']))) { echo 'selected="selected"'; } } ?>><?php echo ucwords($data['plan_name']);?></option>
						<?php
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['plan_id'])){ echo $_SESSION['error']['plan_id'];}?></span>
				</div>
			</div>
		</div>
		<?php
		if($_POST['user_type']=='1'){
			$user_type="Lecturer";
		}else if($_POST['user_type']=='2'){
			$user_type="Student";
		}else if($_POST['user_type']=='3'){
			$user_type="Admin";
		}else if($_POST['user_type']=='6'){
			$user_type="Reviewer";
		}else{
			$user_type="Username";
		}
			?>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label"><?php echo $user_type; ?></label>
				<div class="col-sm-8">
					<select class="form-control" name="lec_user" id="lec_user">
						<option value=''>-Select-</option>
						<?php
						if($_POST['user_type']=='1'){
							$sel1=$database->query("select * from users where valid='1' and userlevel='9'");
							while($rowl=mysqli_fetch_array($sel1)){
							?>
								<option value="<?php echo $rowl['username'];?>"   <?php if(isset($_POST['lec_user'])){ if($_POST['lec_user'] == $rowl['username']){ echo ' selected="selected"';};}?> ><?php echo $rowl['username'];?></option>
							 <?php
							}
						}else if($_POST['user_type']=='3'){
							$sel1=$database->query("select * from users where valid='1' and userlevel='3'");
							while($rowl=mysqli_fetch_array($sel1)){
							?>
								<option value="<?php echo $rowl['username'];?>"   <?php if(isset($_POST['lec_user'])){ if($_POST['lec_user'] == $rowl['username']){ echo ' selected="selected"';};}?> ><?php echo $rowl['username'];?></option>
							 <?php
							}
						}else if($_POST['user_type']=='6'){
							$sel1=$database->query("select * from users where valid='1' and userlevel='6'");
							while($rowl=mysqli_fetch_array($sel1)){
							?>
								<option value="<?php echo $rowl['username'];?>"   <?php if(isset($_POST['lec_user'])){ if($_POST['lec_user'] == $rowl['username']){ echo ' selected="selected"';};}?> ><?php echo $rowl['username'];?></option>
							 <?php
							}
						}else if($_POST['user_type']=='2'){
							$sel1=query("select * from student_users where estatus='1'");
							while($rowl=mysqli_fetch_array($sel1)){
							?>
								<option value="<?php echo $rowl['mobile'];?>"   <?php if(isset($_POST['lec_user'])){ if($_POST['lec_user'] == $rowl['mobile']){ echo ' selected="selected"';};}?> ><?php echo $rowl['mobile'];?></option>
							 <?php
							}
						}
						?>
					</select>
					<span class="error"><?php if(isset($_SESSION['error']['lec_user'])){ echo $_SESSION['error']['lec_user'];}?></span>
				</div>
			</div>
		</div>
		
	</div>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="form-group row">
			<label></label>
				<div class="col-sm-8">
					<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','validateForm=1&referral_code='+$('#referral_code').val()+'&description='+$('#description').val()+'&type='+$('#type').val()+'&discount='+$('#discount').val()+'&fromdate='+$('#fromdate').val()+'&plan_id='+$('#plan_id').val()+'&expiry_date='+$('#expiry_date').val()+'&lec_user='+$('#lec_user').val()+'&user_type='+$('#user_type').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
					</div>
			</div>
		</div>
	</div>
	
</div>

<?php
unset($_SESSION['error']);
}
?>
<?php
if(isset($_GET['rowDelete'])){
	//$database->query("DELETE FROM chapter WHERE id = '".$_GET['rowDelete']."'");
	$database->query("update  referral_codes set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Chapter deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>
<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	 $field = 'referral_code';
	if(!$post['referral_code'] || strlen(trim($post['referral_code'])) == 0){
	  $_SESSION['error'][$field] = "* Referral Code cannot be empty";
	}
	 $field = 'description';
	if(!$post['description'] || strlen(trim($post['description'])) == 0){
	  $_SESSION['error'][$field] = "* Description cannot be empty";
	}
	/*  $field = 'type';
	if(!$post['type']){
	  $_SESSION['error'][$field] = "* Type cannot be empty";
	} */
	 $field = 'user_type';
	if(!$post['user_type']){
	  $_SESSION['error'][$field] = "* User Type cannot be empty";
	}
	 $field = 'discount';
	if(!$post['discount'] || strlen(trim($post['discount'])) == 0){
	  $_SESSION['error'][$field] = "* Discount cannot be empty";
	}
	 $field = 'discount';
	if(!$post['discount'] || strlen(trim($post['discount'])) == 0){
	  $_SESSION['error'][$field] = "* Discount cannot be empty";
	}
	 $field = 'fromdate';
	if(!$post['fromdate'] || strlen(trim($post['fromdate'])) == 0){
	  $_SESSION['error'][$field] = "* Fromdate cannot be empty";
	}
	 $field = 'plan_id';
	if(!$post['plan_id'] || strlen(trim($post['plan_id'])) == 0){
	  $_SESSION['error'][$field] = "* Plan Id cannot be empty";
	}
	$field = 'expiry_date';
	if(!$post['expiry_date'] || strlen(trim($post['expiry_date'])) == 0){
	  $_SESSION['error'][$field] = "* To Date cannot be empty";
	}if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1&referral_code=<?php echo $post['referral_code'];?>&description=<?php echo $post['description'];?>&type=<?php echo $post['type'];?>&user_type=<?php echo $post['user_type'];?>&fromdate=<?php echo $post['fromdate'];?>&expiry_date=<?php echo $post['expiry_date'];?>&lec_user=<?php echo $post['lec_user'];?>&plan_id=<?php echo $post['plan_id'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		if($id=='NULL')
		{
			//echo "insert referral_codes set referral_code='".$post['referral_code']."',description='".$post['description']."',type='".$post['type']."',value='".$post['discount']."',fromdate='".strtotime($post['fromdate'])."',expiry_date='".strtotime($post['expiry_date'])."',lec_user='".$post['lec_user']."',estatus='1',username='".$session->username."',timestamp='".time()."'";
			$result=query("insert referral_codes set referral_code='".$post['referral_code']."',description='".$post['description']."',type='".$post['type']."',value='".$post['discount']."',fromdate='".strtotime($post['fromdate'])."',expiry_date='".strtotime($post['expiry_date'])."',lec_user='".$post['lec_user']."',user_type='".$post['user_type']."',plan_id='".$post['plan_id']."',status='1',estatus='1',username='".$session->username."',timestamp='".time()."'");
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i>Referral Code Data Created Successfully!</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
			
		}else{
			$result=query("update referral_codes set referral_code='".$post['referral_code']."',description='".$post['description']."',type='".$post['type']."',value='".$post['discount']."',fromdate='".strtotime($post['fromdate'])."',expiry_date='".strtotime($post['expiry_date'])."',lec_user='".$post['lec_user']."',plan_id='".$post['plan_id']."',user_type='".$post['user_type']."' where id='".$id."'");
			//echo "insert chapter set class='".$post['class']."',subject='".$post['subject']."',chapter='".$post['chapter']."'";
			
			?>
				<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Referral Code Data Updated Successfully</div>
				
					<script type="text/javascript">
				
					animateForm('<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
				</script>
			<?php
		}
	}
	

?>

 
	<?php
	

}
?>
<?php


//Display bulkreport
if(isset($_GET['tableDisplay'])){
	?>
	
	<?php
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'referral_codes';
  $condition = "estatus=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."referralcodes/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition." ";
  
  $data_sel = query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
 <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 25,
    "order": [[ 0, 'asc' ]]
    
  });

</script>
	

  	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Refferal Codes</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered dashboard-table" width="100%" cellspacing="0"  id="dataTable">
									<thead>
										<tr>
											<th class="text-left">Sr.No.</th>
											<th class="text-left">Referral Code</th>
											<th class="text-left">Description</th>
											<th class="text-left">Type</th>
											<th class="text-left">Discout</th>
											<th class="text-left">From date</th>
											<th class="text-left">To Date</th>
											<th class="text-left">Student Plans</th>
											<th class="text-left">User Type</th>
											<th class="text-left">Username</th>
											<th class="text-left">Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
									
									$k=1;
									while($value = mysqli_fetch_array($data_sel))
									{
										$time=$value['fromdate'];
										$time1=$value['expiry_date'];
										if($value['type']=='0'){
											$type="Percentage";
										}else{
											$type="Cutoff";
										}
										if($value['user_type']=='1'){
											$utype="Admin";
										}else if($value['user_type']=='2'){
											$utype="Student";
										}else if($value['user_type']=='3'){
											$utype="Lecturer";
										}else if($value['user_type']=='6'){
											$utype="Reviewer";
										}else{
											$utype="Admin";
										}
										$plan='';
										$query=query("select * from student_plans where id in (".$value['plan_id'].")");
										while($row=mysqli_fetch_array($query)){
											$plan.=$row['plan_name'].",";
										}

                  					?>
										<tr>
											<td><?php echo $k; ?></td>
											<td><?php echo $value['referral_code']; ?></td>
											<td><?php echo $value['description']; ?></td>
											<td><?php echo $type; ?></td>
											<td><?php echo $value['value']; ?></td>
											<td class="text-left" data-order="<?php echo $time; ?>"><?php echo date('d/m/Y',$value['fromdate']);?></td>
											<td class="text-left" data-order="<?php echo $time1; ?>"><?php echo date('d/m/Y',$value['expiry_date']);?></td>
											<td><?php echo rtrim($plan,","); ?></td>
											<td><?php echo $utype; ?></td>
											<td><?php echo $value['lec_user']; ?></td>
											<?php
											if($value['status']=='0'){
												?>
													<td nowrap>
													
													<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','validateForm1=1&editform=<?php echo $value['id']; ?>&status=enablestatus')">Enable</a></td>
												<?php
											}else{
												?>
													
													
													<td nowrap>
													<a  style="cursor:pointer;" class="btn btn-info text-white" onClick="setState('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=2&editform=<?php echo $value['id']; ?>')">Edit</a>
													
													<a  style="cursor:pointer;" class="btn btn-primary text-white" onClick="setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','validateForm1=1&editform=<?php echo $value['id']; ?>&status=disablestatus')">Disable</a></td>
												<?php
											}
											?>
										</tr>
									<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
  </section>
  
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
?>
	

   
   <br  />
   		<?php
if(isset($_REQUEST['validateForm1'])){
			if($_REQUEST['status']=='enablestatus'){
				$result=query("update  referral_codes  set status='1' where  id='".$_REQUEST['editform']."'");
			
				if($result){
					
					?>
					

					<script type="text/javascript">
					
						alert("User Enabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>referralcodes/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
						
						
					</script>
					<?php
				}

		}else if($_REQUEST['status']=='disablestatus'){
			
			$result=query("update  referral_codes  set status='0' where  id='".$_REQUEST['editform']."'");
			if($result){
				
				?>

				<script type="text/javascript">
				
					alert("User Disabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>referralcodes/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>referralcodes/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
					
				</script>
				
				<?php
			}
		}
		

	}

?>

<script>
	$(".selectpicker1").selectpicker('refresh');
	</script>


