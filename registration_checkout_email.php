<?php
require("sendgrid/sendgrid-php.php");


$conn=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
function query($sql)
{
    global $conn;
	return mysqli_query($conn,$sql);
}

$sql=query("select id,mobile,email,name from student_users where mobile='".$_REQUEST['mobile']."'");
$row=mysqli_fetch_array($sql);

$email = new \SendGrid\Mail\Mail(); 
$email->setFrom("info@rizee.in", "Rizee");
$email->setSubject("We Missed You- Check Out Our New Features");
$email->addTo($row['email'], $row['name']);

$message='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
      <meta name="description" content="Welcome Email Template">
      <meta name="author" content="Swamy">
      <meta name="generator" content="Entrolabs It Solution Pvt Ltd">
      <title>Welcome-email-template</title>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
      
   </head>
   <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
      <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
      <tr>
         <td align="center" valign="middle">
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
               <tr>
                  <td align="center" valign="middle">
                     <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                        <tr>
                           <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:5px 5px 0 0;">
                              <table width="615" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                 <tr>
                                    <td height="10" align="left" valign="top" style="line-height:10px; font-size:10px;">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td align="center" valign="top">
                                       <table width="600" border="0" align="left" cellpadding="0" cellspacing="0" class="two-left-inner">
                                          <tr>
                                             <td align="left" valign="top"> <a href="http://www.rizee.in" target="_blank"> <img src="http://rizee.in/images/email_banner_new4.jpg"   alt="Rizee - The Perfect Guide" width="700" height="700"></a></td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td height="10" align="left" valign="top" style="line-height:10px; font-size:10px;">&nbsp;</td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
               <tr>
                  <td align="center" valign="middle">
                     <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                        <tr>
                           <td align="center" valign="top" bgcolor="#FFFFFF">
                              <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                 <tr>
                                    <td height="25" align="center" valign="top" style="font-size:25px; line-height:25px;">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td align="left" valign="middle" style="font-family: sans-serif, Verdana; font-size:20px; font-weight:600; color:#000; text-transform:capitalize;"> Hello Students,</td>
                                 </tr>
                                 <tr>
                                    <td height="15" align="center" valign="top" style="line-height:15px; font-size:15px;">&nbsp;</td>
                                 </tr>
                                 <tr>
                                    <td align="center" valign="top">
                                       <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                         
                                          
                                          <tr>
                                             <td align="left" valign="top" style="font-family:Arial; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> <span style="font-size:16px;font-family:Arial;color: rgb(11,83,148);background-color:transparent;font-weight:700;font-variant-numeric:normal;font-variant-east-asian:normal;vertical-align:baseline;white-space:pre-wrap;">It looks like you are missing out on some exciting things happening on the Rizee Platform.</span> The Year 2020 might have been a bad year for all of us, but 2021 is all about hope, change & more importantly success for students in their exam preparation. </td>
                                          </tr>
                                          <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                               <tr>
                                             <td align="left" valign="top" style="font-size:16px;font-family:Arial;color:rgb(53,28,117);background-color:transparent;font-weight:700;font-variant-numeric:normal;font-variant-east-asian:normal;vertical-align:baseline;white-space:pre-wrap;">What you&apos;re missing out?</td>
                                          </tr>
                                <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                               <tr>
                                             <td align="left" valign="top" style="font-size:16px;font-family:Arial;color:rgb(53,28,117);background-color:transparent;font-weight:700;font-variant-numeric:normal;font-variant-east-asian:normal;vertical-align:baseline;white-space:pre-wrap;">One-Stop Destination for High-Quality Mock Tests-</td>
                                          </tr>
                               <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                               <tr>
                                             <td align="left" valign="top" style="font-family:Arial; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> JEE Mains & NEET 2021 Free Mock Tests- Yes, you heard it right. Simply login into your account & take a Free Mock Test in the Latest NTA Pattern. Check your exam readiness & improve performance with our detailed Exam Performance & Analysis. </td>
                                          </tr>
                                <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                               <tr>
                                             <td align="left" valign="top" style="font-size:16px;font-family:Arial;color:rgb(53,28,117);background-color:transparent;font-weight:700;font-variant-numeric:normal;font-variant-east-asian:normal;vertical-align:baseline;white-space:pre-wrap;">Prime Question Videos & Important Topic Videos-</td>
                                          </tr>
                               <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                               <tr>
                                             <td align="left" valign="top" style="font-family:Arial; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> Hurray! The Videos are here. Explore our all-new videos section with Prime Questions Videos based on important topics with detailed explanations & tricks from experienced faculty in NEET & IIT JEE.  </td>
                                          </tr>
                                <tr>
                                             <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;color: rgb(11,83,148);background-color:transparent;;"> &nbsp;</td>
                                          </tr>
                                <tr>
                                             <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> Visit <a href=" www.rizee.in/student " target="_blank" style="color: #1a8cec;text-decoration: none;"><u> www.rizee.in/student </u></a> to login & attempt the Free Mock Tests for NEET & JEE Mains 2021 or simply visit the mock tests section on your App.  
                                </td>
                                
                                          </tr>
                                          
                               
                
                                         
                                       </table>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td height="45" align="center" valign="top" style="font-size:45px; line-height:45px;">&nbsp;</td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
               <tr>
                  <td align="center" valign="middle">
                     <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                        <tr>
                           <td align="center" valign="top" bgcolor="#FFFFFF">
                              <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                 <tr>
                                    <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;">&nbsp;</td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
               <tr>
                  <td align="center" valign="middle">
                     <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                        <tr>
                           <td align="center" valign="middle">
                              <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                                 <tr>
                                    <td align="center" valign="top" bgcolor="#FFFFFF">
                                       <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                          <tr>
                                             <td height="20" align="center" valign="top" style="font-size:20px; line-height:20px;"> &nbsp;</td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                     <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                        <tr>
                           <td align="center" valign="middle">
                              <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                                 <tr>
                                    <td align="center" valign="top" style="border-top:1px solid #eff1f4;"></td>
                                 </tr>
                                 <tr>
                                    <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:0 0 5px 5px;">
                                       <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                          <tr>
                                             <td height="20" align="center" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td align="center" valign="top" style="font-family: sans-serif; font-size:16px; font-weight:normal; line-height:28px; color:#000;"> If you Like Rizee, rate us on Google Play store!</td>
                                          </tr>
                                          <tr>
                                             <td height="15" align="center" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td align="center">
                                                <table class="display-width" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto !important;" cellspacing="0" cellpadding="0" border="0" align="center">
                                                   <tr>
                                                      <td style="color:#333333; line-height:0;" width="120" align="center"> <a href="https://play.google.com/store/apps/details?id=in.rizee.mylearningplus" style="color:#333333; text-decoration:none;" data-color="App-Content"> <img src="http://rizee.in/gpstore.png" alt="googleplay-store" style="color:#333333; width:100%; line-height:0; border-radius:5px; line-height:0;"> </a></td>
                                                   </tr>
                                                </table>
                                             </td>
                                          </tr>
                                          <tr>
                                             <td height="15" align="center" valign="top">&nbsp;</td>
                                          </tr>
                                          <tr>
                                             <td align="center" valign="middle" class="mast-social-footer" style="font-family: sans-serif; font-size:16px; font-weight:normal; line-height:28px;"> Sent By <a href="http://www.rizee.in" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Rizee </a> <a href="https://www.rizee.in/support" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Support Center</a> <a href="https://www.rizee.in/privacy" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Privacy Policy</a></td>
                                          </tr>
                                          <tr>
                                             <td height="20" align="left" valign="top">&nbsp;</td>
                                          </tr>
                                       </table>
                                    </td>
                                 </tr>
                              </table>
                           </td>
                        </tr>
                     </table>
                  </td>
               </tr>
            </table>
   </body>
</html>

';
    $email->addContent("text/html",$message);

    $sendgrid = new \SendGrid('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');
    try {
        $response = $sendgrid->send($email);
        print $response->statusCode() . "\n";
        print_r($response->headers());
        print $response->body() . "\n";
    } catch (Exception $e) {
        echo 'Caught exception: '. $e->getMessage() ."\n";
    }

?>