<?php
ini_set('display_errors','On');
include('../include/session.php');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Hemanth">
    <title>Rizee - The Perfect Guide </title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    <link href="web/css/style1.css" rel="stylesheet">
	<meta name="keywords" content="">
	<link rel="shortcut icon" href="<?php echo SECURE_PATH;?>vendor/images/favicon.ico">
	<?php echo $session->gdc_title();?>
	 <?php echo $session->commonJS();?> 
</head>

<body>
		<header class="py-2">
			<div class="container">
				<a href="index.html"><img src="web/images/logo.png" width="130" alt="Image" /></a>
			</div>
		</header>
		<section class="main-wrapper">
			<div class="container">
				
				<div  id="adminForm" >
					
					<script type="text/javascript">
						setStateGet('adminForm','<?php echo SECURE_PATH;?>registrations/process.php','addForm=1');
					</script>
					
				</div>
				
			</div>
		</section>
		<section class="service-one" id="services">
			<div class="container">
				<div class="block-title text-center">
					<h2 class="block-title__title">Rizee above <br>you(rself)!</h2><!-- /.block-title__title -->
				</div><!-- /.block-title -->
				<div>
					<div>
						<ul class="nav nav-tabs" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab">For students</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab">For educational
									institutions</a>
							</li>

						</ul><!-- Tab panes -->
					</div>
					<div class="tab-content">
						<div class="tab-pane active" id="tabs-1" role="tabpanel">
							<div class="row">
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-bullseye"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">The Prep-Buddy You Need</a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">The learning material we provide, will give you
											crisp, concise notes and important information like formulas, equations,etc,
											- that you need to remember before an exam
										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-globe"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">Reflect &amp; Reassess </a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">Rizee will help you figure out where, why and how
											you�re going wrong in your tests, with our analytic software - so you can
											save time in trying to figure out the errors all by yourself
										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-paper-plane"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">Custom Practice Tests
											</a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">With our wide variety of filters you can choose a
											test that will help you practice better. It could be easy, moderate or hard,
											a previous test paper or a paper with all the things you want to practice
										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
							</div><!-- /.row -->
							<div class="text-center">
								<h5> <b>15 Day Free Trial!</b>
								</h5>
							</div>
						</div>
						<div class="tab-pane" id="tabs-2" role="tabpanel">
							<div class="row">
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-bullseye"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">Exclusive platform
											</a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">Empower your students by providing them the
											assessment and knowledge platform that�ll allow them to achieve higher
											grades

										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-globe"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">Easy Tracking </a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">Making it customisable, our platform will make it
											uber easy to monitor progress and data of students across all branches of
											your organization

										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
								<div class="col-lg-4">
									<div class="service-one__single">
										<div class="service-one__icon">
											<i class="fas fa-paper-plane"></i>
										</div><!-- /.service-one__icon -->
										<h3 class="service-one__title"><a href="#">Individual attention
											</a></h3>
										<!-- /.service-one__title -->
										<p class="service-one__text">The sub-forums we provide within the platform will
											allow teachers to discuss amongst each other, while focusing on each student
											and their progress
										</p><!-- /.service-one__text -->
									</div><!-- /.service-one__single -->
								</div><!-- /.col-lg-4 -->
							</div><!-- /.row -->
							<div class="text-center">
								<h5> <b>Reach out to us at: <a href="mailto:info@rizee.in">info@rizee.in</a> to have a
										Rizee platform for your institution</b>
								</h5>
							</div>
						</div>

					</div>
				</div>

			</div><!-- /.container -->
		</section>
		<!-- <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
			integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
			crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
			integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
			crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
			integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
			crossorigin="anonymous"></script> -->
			<?php echo $session->commonFooterAdminJS();?> 
		</body>

	</html>