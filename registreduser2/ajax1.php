<?php
ini_set("display_errors","ON");
include('../include/session.php');
 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'student_users';
 
// Table's primary key
$primaryKey = 'id';
 
// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
//$con = mysqli_connect("localhost","root","","qsbank");
$con = mysqli_connect("localhost","rspace","Rsp@2019","qsbank");
function query($sql){
global $con;
 return mysqli_query($con,$sql);
}


//$con1 = mysqli_connect("localhost","root","","neetjee");
$con1 = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
function query1($sql){
global $con1;
 return mysqli_query($con1,$sql);
}
if(isset($_REQUEST['type'])){
	if($_REQUEST['type']!=''){
		$type=" userlevel='".$_REQUEST['type']."'";
	}else{
		$type=" userlevel='2'";
	}
}else{
	$type=" userlevel='2'";
}
$todate=strtotime(date('m/d/Y'));

$todate1 = strtotime(date('m/d/Y'). '23:59:59');
$datey = strtotime('-1 days',$todate);
$datey1 = strtotime(date('d-m-Y',$datey).'23:59:59');
if(isset($_REQUEST['date'])){
	if($_REQUEST['date']!=''){
		if($_REQUEST['date']=='today'){
			$date=" AND timestamp between  ".$todate." and ".$todate1."";
		}else if($_REQUEST['date']=='yesterday'){
			$date="  AND timestamp between  ".$datey." and ".$datey1."";
		}else{
			$date="";
		}
	}else{
		$date="";
	}
}else{
	$date="";
}

if(isset($_REQUEST['status'])){
	if($_REQUEST['status']!=''){
			if($_REQUEST['status']=='1'){
				$status=" AND  estatus=1 ";
			
			}else{
				$status=" AND  estatus=0";
			}

	}else{
		$status="  AND  estatus=1 ";
	}
}else{
	$status="  AND  estatus=1 ";
}
 $columns = array(
    array( 'db' => 'mobile', 'dt' => 0),
    array( 'db' => 'name',  'dt' => 1),
    array( 'db' => 'email',   'dt' => 2 ),
    array( 'db' => 'mobile',     'dt' => 3 ),
	array( 'db' => 'class_id','dt' => 4,
        'formatter' => function( $d, $row ) {
			 $sql=query("select * from class where estatus='1' and id='".$d."'");
			$row=mysqli_fetch_array($sql);
			return $row['class'];
        }
    ),
	 array( 'db' => 'exam_id','dt' => 5,
        'formatter' => function( $d, $row ) {
			 $sql=query("select * from exam where estatus='1' and id='".$d."'");
			$row=mysqli_fetch_array($sql);
			return $row['exam'];
        }
    ),
	 array( 'db' => 'timestamp','dt' => 6,
        'formatter' => function( $d, $row ) {
            return date( 'd/m/Y', $d);
        }
    ),
	array( 'db' => 'mobile','dt' => 7,
		'formatter' => function( $d, $row ) {
			$sqll2=query1("SELECT mobile,device_id,ip_address FROM verify_students where  mobile='".$d."'");
			$rowll2=mysqli_fetch_array($sqll2);
			if($rowll2['device_id']!=''){
				$device_id=$rowll2['device_id'];
			}else{
				$device_id=$rowll2['ip_address'];
			}
			return $device_id;
        }
	),
	array( 'db' => 'mobile','dt' => 8,
		'formatter' => function( $d, $row ) {
			$sqll2=query1("SELECT device_id,ip_address FROM student_users where  mobile='".$d."'");
			$rowll2=mysqli_fetch_array($sqll2);
			if($rowll2['device_id']!=''){
				$device_id=$rowll2['device_id'];
			}else{
				$device_id=$rowll2['ip_address'];
			}
			return $device_id;
			
        }
	),
	array( 'db' => 'current_plan_id',     'dt' => 9,
		'formatter' => function( $d, $row ) {
			$sqll1=query1("SELECT * FROM `student_plans` where estatus='1' and id='".$d."'");
			$rowll1=mysqli_fetch_array($sqll1);
			
			return $rowll1['valid_days'];
			
        } ),
	 array( 'db' => 'expiry_date','dt' => 10,
		'formatter' => function( $d, $row ) {
			if($d!=''){
				return date("d/m/Y",$d);
			}else{
				return '';
			}
        }),
		
	array( 'db' => 'expiry_date',     'dt' => 11,
		'formatter' => function( $d, $row ) {
			if($d!=''){
				$now = time();
				$datediff = $d - $now;

				$diff=round($datediff / (60 * 60 * 24));
				if($diff>0){
					$diff1=$diff;
				}else{
					$diff1=0;
				};
				return $diff1;
			}else{
				return '0';
			}
	}),
	array( 'db' => 'target_year',     'dt' => 12),
	array( 'db' => 'valid',     'dt' => 13,
		'formatter' => function( $d, $row ) {
			if($d=='1'){
				return "Verified";
			}else if($d=='0'){
				return "Not Verified";
			}else{
				return '';
			}
		}
	),
    
	array( 'db' => 'estatus',     'dt' => 14,
		'formatter' => function( $d, $row ) {
			if($d=='0'){
				return "Inactive";
			}else if($d=='1'){
				return "Active";
			}else{
				return '';
			}
		}
	),
	array( 'db' => 'mobile',     'dt' => 15,
		'formatter' => function( $d, $row ) {
			$sqll2=query1("SELECT * FROM student_users where  mobile='".$d."'");
			$rowll2=mysqli_fetch_array($sqll2);
			if($rowll2['estatus']=='0'){
				return "<a  style='cursor:pointer;' class='btn btn-primary text-white' onClick=setStateGet('adminTable','".SECURE_PATH."registreduser2/process.php','validateForm=1&mobile=".$d."&status=enablestatus')>Enable User</a>";
			}else if($rowll2['estatus']=='1'){
				return "<a  style='cursor:pointer;' class='btn btn-primary text-white' onClick=setStateGet('adminTable','".SECURE_PATH."registreduser2/process.php','validateForm=1&mobile=".$d."&status=disablestatus')>Disable User</a>&nbsp;&nbsp;&nbsp;
				<a  style='cursor:pointer;' class='btn btn-primary text-white' data-toggle='modal' data-target='#myModal1'  onClick=setStateGet('viewdetails1','".SECURE_PATH."registreduser2/process.php','viewdetails1=1&id=".$rowll2['id']."&plan_id=".$rowll2['current_plan_id']."&expiry_date=".$rowll2['expiry_date']."')>Update Plan</a>&nbsp;&nbsp;&nbsp;&nbsp;
				<a  style='cursor:pointer;' class='btn btn-primary mt-2 text-white' onClick=setState('adminForm','".SECURE_PATH."registreduser2/process.php','addForm=2&editform=".$rowll2['id']."')>Edit</a>
				
				";
			}else{
				return '';
			}
		}
	),
	
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'rspace',
    'pass' => 'Rsp@2019',
    'db'   => 'neetjee',
    'host' => 'localhost'
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
 
require( 'ssp.class.php' );
 $whereall=$type.$date.$status;
 $output =  SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $whereall );

//print_r($output);

$convert = json_encode($output);


if(json_last_error_msg() == "No error"){
    echo $convert;

}
else{
        $temp = $output['data'];

        foreach($temp as $k =>$tmp){

        	 $output['data'][$k] = mb_convert_encoding($tmp, 'UTF-8', 'UTF-8');

		}

		echo json_encode($output);
}
?>
