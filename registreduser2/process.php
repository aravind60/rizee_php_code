<?php
include('../include/session.php');
include('../ssp.class.php');
ini_set('memory_limit',-1);
ini_set('max_execution_time',1);
ini_set('display_errors',0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
//$con = mysqli_connect("localhost","root","","neetjee");
$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
function query($sql){
global $con;
 return mysqli_query($con,$sql);
}
?>

<?php
if(isset($_REQUEST['addForm'])){

 if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = query("SELECT * FROM student_users WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
	 $_POST = array_merge($_POST,$data);
	 $data_sel1 = query("SELECT * FROM students WHERE contact_no = '".$data['mobile']."'");
	 $data1 = mysqli_fetch_array($data_sel1);
	 $_POST['branch_id']=$data1['branch_id'];
	 $_POST['section_id']=$data1['section_id'];
	 $_POST['package_id']=$data1['package_id'];
	  $_POST['category_id']=$data1['category_id'];
   
	
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 ?>
<style>
  .spinner-border.w_1 {
    width: 1rem;height: 1rem;
  }
</style>
<script>

</script>
<script>
chapterfunction();

function chapterfunction(){
	var chapter=$('#chapter').val();
	//alert(chapter);
	if(chapter.length >0){
		$('#chapview').show();
	}else{
		$('#chapview').hide();
	}
}
</script>
<?php
		if(isset($_POST['editform'])){
			if($_POST['editform']!=''){
				$style="style='display:none;'";
				$disa="disabled";
				$read="readonly";
			}else{
			}
		}else{
			$style="";
			$read="";
		}
			?>
 <div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">Full Name<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				  <input type="text" name="name" class="form-control" placeholder="Enter Full Name" id="name" value="<?php if(isset($_POST['name'])){ echo $_POST['name'];}?>" autocomplete="off">
				  <span class="text-danger"><?php if(isset($_SESSION['error']['name'])){ echo $_SESSION['error']['name'];}?></span>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">User Name</label>
				<div class="col-sm-8">
				  <input type="text" name="username" class="form-control" placeholder="Enter User Name" id="username" value="<?php if(isset($_POST['username'])){ echo $_POST['username'];}?>" autocomplete="off">
				  <span class="text-danger"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6" <?php echo $style; ?> >
		
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Password<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="password" name="password" class="form-control" placeholder="Enter Password" id="password" value="<?php if(isset($_POST['password'])){ echo $_POST['password'];}else{ }?>" autocomplete="off" >
					<span class="text-danger"><?php if(isset($_SESSION['error']['password'])){ echo $_SESSION['error']['password'];}?></span>
				</div>
			</div>
		
		</div>
			
		
		<div class="col-lg-6">
			<div class="form-group row">
          <label class="col-sm-4 col-form-label">Email<span style="color:red;">*</span></label>
          <div class="col-sm-8">
              <input type="text" name="email" class="form-control" placeholder="Enter Email" id="email" value="<?php if(isset($_POST['email'])){ echo $_POST['email'];}?>" autocomplete="off">
              <span class="text-danger"><?php if(isset($_SESSION['error']['email'])){ echo $_SESSION['error']['email'];}?></span>
          </div>
        </div>

		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Mobile No.<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="mobile" class="form-control" placeholder="10-digit Mobile number" id="mobile" onkeypress="return isNumber(event,$(this),10)"  maxlength="10" value="<?php if(isset($_POST['mobile'])){echo $_POST['mobile'];}?>" autocomplete="off" <?php echo $read; ?> >
					<span class="text-danger"><?php if(isset($_SESSION['error']['mobile'])){ echo $_SESSION['error']['mobile'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Class<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="class_id" id="class_id" class="form-control" >
					<option value=''>-Select-</option>
					<option value='1' <?php if(isset($_POST['class_id'])){ if($_POST['class_id'] == 1){ echo ' selected="selected"';};}?> >XI</option>
					<option value='2' <?php if(isset($_POST['class_id'])){ if($_POST['class_id'] == 2){ echo ' selected="selected"';};}?>>XII</option>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['class_id'])){ echo $_SESSION['error']['class_id'];}?></span>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Institute<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="institution_id" id="institution_id" class="form-control" onChange="setState('aaa','<?php echo SECURE_PATH;?>registreduser2/process.php','getbranch=1&institution_id='+$('#institution_id').val()+'');setState('ddd','<?php echo SECURE_PATH;?>registreduser2/process.php','getpackage=1&institution_id='+$('#institution_id').val()+'')" >
					<option value=''>-Select-</option>
					<?php
					$row = $database->query("select * from institute where estatus='1'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['institution_id'])) { if($_POST['institution_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['institute_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['institution_id'])){ echo $_SESSION['error']['institution_id'];}?></span>
				</div>
			</div>
		</div>
		
		<div class="col-lg-6" id="aaa">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Branch<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="institution_id" id="branch_id" class="form-control"  onChange="setState('bbb','<?php echo SECURE_PATH;?>registreduser2/process.php','getsection=1&institution_id='+$('#institution_id').val()+'&branch_id='+$('#branch_id').val()+'')">
					<option value=''>-Select-</option>
					<?php
					
					$row = query("select * from branches where estatus='1' and institution_id='".$_POST['institution_id']."'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['branch_id'])) { if($_POST['branch_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['branch_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['branch_id'])){ echo $_SESSION['error']['branch_id'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6" id="bbb">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Sections<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="section_id" id="section_id" class="form-control" onChange="setState('ddd','<?php echo SECURE_PATH;?>registreduser2/process.php','getpackage=1&institution_id='+$('#institution_id').val()+'')">
					<option value=''>-Select-</option>
					<?php
					$row = query("select * from sections where estatus='1' and institution_id='".$_POST['institution_id']."'  and branch_id='".$_POST['branch_id']."'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['section_id'])) { if($_POST['section_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['section_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['section_id'])){ echo $_SESSION['error']['section_id'];}?></span>
				</div>
			</div>
		</div>

		<div class="col-lg-6" id="ddd">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Packages<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="package_id" id="package_id" class="form-control" onChange="setState('fff','<?php echo SECURE_PATH;?>registreduser2/process.php','getcategory=1&institution_id='+$('#institution_id').val()+'&package_id='+$('#package_id').val()+'');setState('ggg','<?php echo SECURE_PATH;?>registreduser2/process.php','getexamid=1&institution_id='+$('#institution_id').val()+'&package_id='+$('#package_id').val()+'')">
					<option value=''>-Select-</option>
					<?php
			$row = query("select * from packages where estatus='1' and institution_id='".$_POST['institution_id']."'  ");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['package_id'])) { if($_POST['package_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['package_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['package_id'])){ echo $_SESSION['error']['package_id'];}?></span>
				</div>
			</div>
		</div>
	
		
		<div class="col-lg-6" id="ggg">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
					<div class="col-sm-8">
					<select name="exam" id="exam_id" class="form-control" >
						<option value=''>-Select-</option>
						<?php
			
						$sel = query("select id,exams_covered from packages where estatus='1' and institution_id='".$_POST['institution_id']."'  and id='".$_POST['package_id']."' and exams_covered='".$_POST['exam_id']."' ");
						$row=mysqli_fetch_array($sel);
						$sell1=$database->query("select * from exam where estatus='1' and id ='".$row['exams_covered']."'");
						while($data = mysqli_fetch_array($sell1))
						{
						?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['exam_id'])) { if($_POST['exam_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['exam']);?></option>
						<?php
						}
						?>
						
					</select>
					<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['exam_id'])){ echo $_SESSION['error']['exam_id'];}?></span>
					</div>
				</div>
			</div>
			<div class="col-lg-6"  id="fff">
				<div class="form-group row">
					<label class="col-sm-4 col-form-label">Categories<span style="color:red;">*</span></label>
					<div class="col-sm-8">
					<select name="category_id" id="category_id" class="form-control" >
						<option value=''>-Select-</option>
						<?php
						$row = query("select * from categories where estatus='1' and institution_id='".$_POST['institution_id']."'  and package_id='".$_POST['package_id']."' ");
						while($data = mysqli_fetch_array($row))
						{
							?>
						<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['category_id'])) { if($_POST['category_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['category_name']);?></option>
						<?php
						}
						?>
					</select>
					<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['category_id'])){ echo $_SESSION['error']['category_id'];}?></span>
					</div>
				</div>
			</div>
		
		
		
		
		
	
    </div>

    <div class="form-group row pl-3">
		
        <div class="col-lg-6">
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','validateFormc=1&name='+$('#name').val()+'&username='+$('#username').val()+'&password='+$('#password').val()+'&email='+$('#email').val()+'&mobile='+$('#mobile').val()+'&class_id='+$('#class_id').val()+'&exam_id='+$('#exam_id').val()+'&branch_id='+$('#branch_id').val()+'&section_id='+$('#section_id').val()+'&package_id='+$('#package_id').val()+'&institution_id='+$('#institution_id').val()+'&category_id='+$('#category_id').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?>')">Submit</a>
        </div>
       

       
    </div>
</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}
if(isset($_GET['rowDelete'])){
	//query("update student_users set valid='0',estatus='0' where id='".$_GET['rowDelete']."' and mobile='".$_GET['mobile']."'");
	//query("update students set estatus='0' where contact_no='".$_GET['mobile']."'");
	query("delete from  student_users  where id='".$_GET['rowDelete']."' and mobile='".$_GET['mobile']."'");
	query("delete  from  students  where contact_no='".$_GET['mobile']."'");
?>
<div class="alert alert-success">Username deleted successfully!</div>
<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
<?php
}
if(isset($_POST['validateFormc'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	$mobile = $post['mobile'];
	$email = $post['email'];
  $name = $post['name'];
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  $field = 'name';
	if(!$post['name'] || strlen(trim($post['name'])) == 0){
	  $_SESSION['error'][$field] = "* Fullname name cannot be empty";
	}
	$field = 'class_id';
	if(!$post['class_id'] || strlen(trim($post['class_id'])) == 0){
	  $_SESSION['error'][$field] = "* Class name cannot be empty";
	}
	$field = 'exam_id';
	if(!$post['exam_id'] || strlen(trim($post['exam_id'])) == 0){
	  $_SESSION['error'][$field] = "* Exam name cannot be empty";
	}
	$field = 'institution_id';
	if(!$post['institution_id'] || strlen(trim($post['institution_id'])) == 0){
	  $_SESSION['error'][$field] = "* Institute name cannot be empty";
	}
	$field = 'branch_id';
	if(!$post['branch_id'] || strlen(trim($post['branch_id'])) == 0){
	  $_SESSION['error'][$field] = "* Branch cannot be empty";
	}
	$field = 'section_id';
	if(!$post['section_id'] || strlen(trim($post['section_id'])) == 0){
	  $_SESSION['error'][$field] = "* Section cannot be empty";
	}
	$field = 'package_id';
	if(!$post['package_id'] || strlen(trim($post['package_id'])) == 0){
	  $_SESSION['error'][$field] = "* Package cannot be empty";
	}
	/*$field = 'category_id';
	if(!$post['category_id'] || strlen(trim($post['category_id'])) == 0){
	  $_SESSION['error'][$field] = "* Category cannot be empty";
	}*/
	
  
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  /* mobile number check */
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }else if(!isset($post['editform'])){
	$sello=query("select * from student_users where mobile='".$_POST['mobile']."'");
	$rowcountc=mysqli_num_rows($sello);
	if($rowcountc>0){
			$_SESSION['error'][$field] = "* Mobile Number Duplicate";
	}
}


	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}else if(!isset($post['editform'])){
		$sello1=query("select * from student_users where email='".$email."'");
		  $rowcountc1=mysqli_num_rows($sello1);
		if($rowcountc1 > 0){
				$_SESSION['error'][$field] = "* Email Id Duplicate";
		}
	}else if(strlen($email) > 0){
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$_SESSION['error'][$field] = "* Invalid Email ID";
		}
	}
	
  $field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	
 

  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateFormc'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1&name=<?php echo $post['name'];?>&username=<?php echo $post['username'];?>&password=<?php echo $post['password'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&class_id=<?php echo $post['class_id'];?>&exam_id=<?php echo $post['exam_id'];?>&institution_id=<?php echo $post['institution_id'];?>&branch_id=<?php echo $post['branch_id'];?>&section_id=<?php echo $post['section_id'];?>&package_id=<?php echo $post['package_id'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		
			$subpass=$_REQUEST['password'];
			$options = [
			'cost' => 12,
		  
			];
			$pass=password_hash($subpass, PASSWORD_BCRYPT, $options);
				  
			$pass1= substr($pass,3);
			$password='$2b'.$pass1;
			
			//$sql=query("select * from student_users where estatus='1' and mobile='".$_REQUEST['mobile']."'");
			//$rowld=mysqli_fetch_array($sql);
			
			 $result=query("INSERT INTO student_users VALUES('','".$_REQUEST['name']."','".$_REQUEST['username']."','".$_REQUEST['email']."','".$_REQUEST['mobile']."','".$password."','1','".$_REQUEST['class_id']."','".$_REQUEST['exam_id']."','2020','1','1','1','".$_REQUEST['institution_id']."','','".$_REQUEST['branch_id']."','','','1','".time()."','0','','','','','','','','','')  ON DUPLICATE KEY UPDATE name='".$_REQUEST['name']."',username='".$_REQUEST['username']."',email='".$_REQUEST['email']."',mobile='".$_REQUEST['mobile']."',class_id='".$_REQUEST['class_id']."',exam_id='".$_REQUEST['exam_id']."',institution_id='".$_REQUEST['institution_id']."',branch_id='".$_REQUEST['branch_id']."'" );
			 query("insert register_userlog set mobile='".$_REQUEST['mobile']."',eusername='".$session->username."',message=1,timestamp='".time()."'");
			 
			$sid= mysqli_insert_id($con);
			if($result){
				
				 query("INSERT INTO students VALUES('','".$_REQUEST['name']."','".$_REQUEST['mobile']."','".$_REQUEST['email']."','".$_REQUEST['branch_id']."','".$_REQUEST['exam_id']."','".$_REQUEST['class_id']."','".$_REQUEST['section_id']."','".$_REQUEST['package_id']."','".$_REQUEST['category_id']."','".$_REQUEST['institution_id']."','1','".$session->username."','".time()."')  ON DUPLICATE KEY UPDATE student_name='".$_REQUEST['name']."',contact_no='".$_REQUEST['mobile']."',email='".$_REQUEST['email']."',exam_id='".$_REQUEST['exam_id']."',class_id='".$_REQUEST['class_id']."',institution_id='".$_REQUEST['institution_id']."',branch_id='".$_REQUEST['branch_id']."',section_id='".$_REQUEST['section_id']."',package_id='".$_REQUEST['package_id']."',category_id='".$_REQUEST['category_id']."'" );
				

			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> User information saved successfully!
					</div>
				</div>
			</div>
			<script type="text/javascript">
			  animateForm('<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
		  </script>
	 <?php
		}else{
			?>
			<div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> User information Failed!
					</div>
				</div>
			</div>
			<script type="text/javascript">
			  animateForm('<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
		  </script>
	 	<?php

		}
    
		
}
}
 ?>
 <script>
function search_report() {
		
			var type = $('#type').val();
			
			setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&type='+type+'');
		}
	</script>
<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
   $tableName = 'student_users';
  $condition = "";
  if(isset($_GET['keyword'])){
  }
  if(isset($_REQUEST['type'])){
		if($_REQUEST['type']!=''){
			$type=" userlevel='".$_REQUEST['type']."'";
		}else{
			$type="";
		}
	}else{
		$type="userlevel='2'";
	}
	$todate=strtotime(date('m/d/Y'));

  $todate1 = strtotime(date('m/d/Y'). '23:59:59');
  $datey = strtotime('-1 days',$todate);
  $datey1 = strtotime(date('d-m-Y',$datey).'23:59:59');
	if(isset($_REQUEST['date'])){
		if($_REQUEST['date']!=''){
			if($_REQUEST['date']=='today'){
				$date=" AND timestamp between  ".$todate." and ".$todate1."";
			}else if($_REQUEST['date']=='yesterday'){
				$date="  AND timestamp between  ".$datey." and ".$datey1."";
			}else{
				$date="";
			}
		}
	}else{
		$date="";
	}

	if(isset($_REQUEST['status'])){
		if($_REQUEST['status']!=''){
				if($_REQUEST['status']=='1'){
					$status=" AND  estatus=1 ";
				
				}else{
					$status=" AND  estatus=0";
				}

		}
	}else{
		$status=" AND  estatus=1 ";
	}
	$condition = $type.$date.$status;
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  $q = "SELECT * FROM $tableName ".$condition." ORDER BY timestamp DESC";
  $result_sel = query($q);
  $numres = mysqli_num_rows($result_sel);
  
    $query = "SELECT * FROM $tableName ".$condition."   ORDER BY timestamp DESC";
 
  
  $data_sel = query($query);
  $todaysel=query("SELECT count(id) as cnt FROM $tableName where   timestamp between ".$todate." and ".$todate1." and userlevel='2' ORDER BY timestamp DESC");
	 $rowl=mysqli_fetch_array($todaysel);

	$yessel=query("SELECT count(id) as cnt FROM $tableName where   timestamp between ".$datey." and ".$datey1."  and userlevel='2' ORDER BY timestamp DESC");
	 $rowly=mysqli_fetch_array($yessel);

	 $yessel1=query("SELECT count(id) as cnt FROM $tableName  where userlevel='2'   ORDER BY timestamp DESC");
	 $rowly1=mysqli_fetch_array($yessel1);

	 
	 $activesel=query("SELECT count(id) as cnt FROM $tableName  where estatus=0 and userlevel='2'  ORDER BY timestamp DESC");
	 $rowlsel=mysqli_fetch_array($activesel);
	 $dactivesel=query("SELECT count(id) as cnt FROM $tableName  where estatus=1 and userlevel='2'  ORDER BY timestamp DESC");
	 $drowlsel=mysqli_fetch_array($dactivesel);
	 $rsel=query("SELECT count(id) as cnt FROM $tableName  where userlevel='2'   ORDER BY timestamp DESC");
	 $rowsel=mysqli_fetch_array($rsel);
	 $rsel1=query("SELECT count(id) as cnt FROM $tableName  where userlevel='1'   ORDER BY timestamp DESC");
	 $rowsel1=mysqli_fetch_array($rsel1);
 
	  
	?>
	 <script type="text/javascript">


  $('#dataTable').DataTable({
	
    "pageLength": 50,
	 "processing": true,
     "serverSide": true,
    "ajax": {
		"url":"<?php echo SECURE_PATH;?>registreduser2/ajax1.php",
		"method": 'POST',
		"data" : {

            "type" : "<?php echo $_REQUEST['type']; ?>",
            "status": "<?php echo $_REQUEST['status']; ?>",
			"date": "<?php echo $_REQUEST['date']; ?>"
           
        }
	},
	"order": [[ 6, "desc" ]],
	"columnDefs": [{
                    targets: "_all"
                   
                 }]
 });

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Registered user Details</h6>
						</div>

						<div class="card-body">
							<table class="table table-bordered">
								<thead>
									<tr class="row mx-0">
										
										<th class="bg-white text-center col">
											<i class="far fa-user fa-4x text-light mb-2"></i>
											<h5>Users</h5>
										</th> 
										
										<th class="bg-white col">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&date=today&type=2')" ><h6>Today</h6>
											<h3 class="text-danger pt-4"><?php echo $rowl['cnt'];?></h3></a>
										   
										</th>
									   
										<th class="bg-light col">
											 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&date=yesterday&type=2')" ><h6>Yesterday</h6>
											<h3 class="text-danger pt-4"><?php echo $rowly['cnt'];?></h3></a>
											
										</th>
										
										<th class="bg-white col">
											 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&date=overall&type=2')" ><h6>Overall</h6>
											<h3 class="text-danger pt-4"><?php echo $rowly1['cnt'];?></h3></a>
										   
										</th>
										<th class="bg-white col">
										   <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&status=1&type=2')" > <h6>Active</h6>
											<h3 class="text-danger pt-4"><?php echo $drowlsel['cnt'];?></h3></a>
										   
										</th>
									   
										<th class="bg-light col">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&status=0&type=2')" ><h6>Inactive</h6>
											<h3 class="text-danger pt-4"><?php echo $rowlsel['cnt'];?></h3></a>
											
										</th>
										<th class="bg-white col">
										   <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&type=2')" > <h6>Registred Users</h6>
											<h3 class="text-danger pt-4"><?php echo $rowsel['cnt'];?></h3></a>
										   
										</th>
									   
										<th class="bg-light col">
											<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&type=1')" ><h6>Institute Users</h6>
											<h3 class="text-danger pt-4"><?php echo $rowsel1['cnt'];?></h3></a>
											
										</th>
									</tr>
								</thead>
							</table>
							<div class="row">
								<div class="col-md-6">
									<label class="m-0 font-weight-bold ">OTP Remainders</label>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th></th>
												<th>Total Remainders</th>
												<th>Completed Registrations</th>
											</tr>

										</thead>
										<tbody>
											<?php
											 $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
											$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
											$dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
											$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
											$date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
											$today=" and timestamp between ".$dateto." and ".$dateto1."";
											$yesterday=" and timestamp between ".$dateyes." and ".$datetoyes1."";
											$overall=" and timestamp between ".$date364days." and ".$dateto1."";
											$daysrange = array($today =>'Today',$yesterday => 'Yesterday',$overall => 'Overall');
											foreach($daysrange as $key => $days){
												$sqltotal=query("select count(id) as cnt from unregistredusers  where estatus='1' ".$key."");
												$rowtotal=mysqli_fetch_array($sqltotal);
												$count=0;
												$sqltotal1=query("select * from unregistredusers  where estatus='1' ".$key."");
												while($rowtotal1=mysqli_fetch_array($sqltotal1)){
													$sql1=query("select count(id) as cnt from student_users where estatus='1' and mobile='".$rowtotal1['mobile']."' and mobile_verified='1' and current_plan_id!='0' ");
													$rowl=mysqli_fetch_array($sql1);
													if($rowl['cnt']>0){
														$count++;
													}

												}
												if($rowtotal['cnt']!=''){
												$total=$rowtotal['cnt'];
											}else{
												$total=0;
											}
												
											?>
												<tr>
													 <th>
														<h6><?php echo $days; ?></h6>
													</th>
													
													<td><?php echo $total; ?></td>
													<td><?php echo $count; ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								<div class="col-md-6">
									<label class="m-0 font-weight-bold " >Package Remainders</label>
									<table class="table table-bordered">
										<thead>
											<tr>
												<th></th>
												<th>Total Remainders</th>
												<th>Updated Package</th>
											</tr>

										</thead>
										<tbody>
											<?php
											 $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
											$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
											$dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
											$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
											$date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
											$today=" and timestamp between ".$dateto." and ".$dateto1."";
											$yesterday=" and timestamp between ".$dateyes." and ".$datetoyes1."";
											$overall=" and timestamp between ".$date364days." and ".$dateto1."";
											$daysrange1 = array($today =>'Today',$yesterday => 'Yesterday',$overall => 'Overall');
											foreach($daysrange1 as $key => $days){
												$sqltotal=query("select count(id) as cnt from user_remainders  where remainder_type='1' ".$key."");
												$rowtotal=mysqli_fetch_array($sqltotal);
												$count=0;
												$sqltotal1=query("select * from user_remainders  where remainder_type='1' ".$key."");
												while($rowtotal1=mysqli_fetch_array($sqltotal1)){
													$sql1=query("select count(id) as cnt from student_transactions where estatus='1' and mobile='".$rowtotal1['mobile']."'  and plan_id!=0  ".$key."");
													$rowl=mysqli_fetch_array($sql1);
													if($rowl['cnt']>0){
														$count++;
													}

												}
											if($rowtotal['cnt']!=''){
												$total=$rowtotal['cnt'];
											}else{
												$total=0;
											}

												
											?>
												<tr>
													 <th>
														<h6><?php echo $days; ?></h6>
													</th>
													
													<td><?php echo $total; ?></td>
													<td><?php echo $count; ?></td>
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<label class="m-0 font-weight-bold">Registration Overview</label>
									<table class="table table-bordered">
										<thead>
										<tr align="center">
												<th></th>
												<th>Completed Registrations</th>
												<th>Incomplete Registrations</th>
												<th>Package Not Opted</th>
												<th>Class/Exam Not Opted</th>
												<th>Incomplete OTP Verification</th>
											</tr>

										</thead>
										<tbody>
											<?php
											 $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
											$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
											$dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
											$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
											$date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
											$today=" and timestamp between ".$dateto." and ".$dateto1."";
											$yesterday=" and timestamp between ".$dateyes." and ".$datetoyes1."";
											$overall=" and timestamp between ".$date364days." and ".$dateto1."";
											$daysrange = array($today =>'Today',$yesterday => 'Yesterday',$overall => 'Overall');
											foreach($daysrange as $key => $days){
												$sqlt=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2'  ".$key."");
												$rowt=mysqli_fetch_array($sqlt);

												$sqltotal=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2' and valid='1'  ".$key."");
												$rowtotal=mysqli_fetch_array($sqltotal);
												$sqltotal_in=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2' and valid='0'  ".$key."");
												$rowtotal_in=mysqli_fetch_array($sqltotal_in);
												$sqltotal_plan=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2' and mobile_verified='1' and class_id!='0' and exam_id!='0' and current_plan_id='0'  ".$key."");
												$rowtotal_plan=mysqli_fetch_array($sqltotal_plan);

												$sqltotal_class=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2' and mobile_verified='1' and (class_id='0' OR exam_id='0')   ".$key."");
												$rowtotal_class=mysqli_fetch_array($sqltotal_class);

												$sqltotal_otp=query("select count(id) as cnt from student_users  where estatus='1' and userlevel='2' and mobile_verified='0'   ".$key."");
												$rowtotal_otp=mysqli_fetch_array($sqltotal_otp);
												
												if($rowt['cnt']!=''){
													$tot=$rowt['cnt'];
												}else{
													$tot=0;
												}

												if($rowtotal['cnt']!=''){
													$total=$rowtotal['cnt'];
												}else{
													$total=0;
												}
												if($rowtotal_in['cnt']!=''){
													$total_in=$rowtotal_in['cnt'];
												}else{
													$total_in=0;
												}
												if($rowtotal_plan['cnt']!=''){
													$total_plan=$rowtotal_plan['cnt'];
												}else{
													$total_plan=0;
												}

												if($rowtotal_class['cnt']!=''){
													$total_class=$rowtotal_class['cnt'];
												}else{
													$total_class=0;
												}

												if($rowtotal_otp['cnt']!=''){
													$total_otp=$rowtotal_otp['cnt'];
												}else{
													$total_otp=0;
												}

												$totap=round(($total/$rowt['cnt'])*100);
												$totain=round(($total_in/$rowt['cnt'])*100);
												$totaplan=round(($total_plan/$rowt['cnt'])*100);
												$totclass=round(($total_class/$rowt['cnt'])*100);
												$tototp=round(($total_otp/$rowt['cnt'])*100);
											?>
												<tr align="center">
													 <th>
														<h6><?php echo $days; ?></h6>
													</th>
													<td ><?php echo $total; ?><small class="text-primary pl-2">(<?php echo $totap; ?>%)</small></td>
													<td><?php echo $total_in; ?><small class="text-primary pl-2">(<?php echo $totain; ?>%)</small></td>
													<td><?php echo $total_plan; ?><small class="text-primary pl-2">(<?php echo $totaplan; ?>%)</small></td>
													<td><?php echo $total_class; ?><small class="text-primary pl-2">(<?php echo $totclass; ?>%)</small></td>
													<td><?php echo $total_otp; ?><small class="text-primary pl-2">(<?php echo $tototp; ?>%)</small></td>
													
												</tr>
											<?php } ?>
										</tbody>
									</table>
								</div>
								
							</div>
							<br/>
							<?php  if($numres > 0){ ?>
								<div class="table-responsive">
									<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" >
										<thead>
											<tr>
												<!-- <th class="text-left"  rowspan='2' nowrap>Sr.No</th> -->
												<th class="text-left"  rowspan='2' nowrap>Username</th>
												  <th class="text-left"  rowspan='2' nowrap>Full Name</th>
												  <th class="text-left" rowspan='2' nowrap>Email</th>
												  <th class="text-left" rowspan='2' nowrap>Mobile</th>
												  <th class="text-left" rowspan='2' nowrap>Class</th>
												  <th class="text-left" rowspan='2' nowrap>Exam</th>
												   <th class="text-left" rowspan='2' nowrap>Created On</th>
												   <th class="text-left" nowrap colspan='2'>IP / Device ID</th>
												   <th class="text-left" rowspan='2' nowrap>Plan Days</th>
												   <th class="text-left" rowspan='2' nowrap>Plan Expiry Date</th>
												   <th class="text-left" rowspan='2' nowrap>Remaining Days</th>
												   <th class="text-left" rowspan='2' nowrap>Target Year</th>
												    <th class="text-left" rowspan='2' nowrap>Verification Status</th>
													<th class="text-left" rowspan='2' nowrap>Status</th>
												   <th class="text-left" rowspan='2' nowrap>Actions</th>
											</tr>
											<tr>
												<th class="text-left" nowrap>Registred Id</th>
												   <th class="text-left" nowrap>Current Id</th>
											</tr>
										</thead>
										<tbody>
										
										</tbody>
									</table>
								</div>
							<?php
							}
							
							?>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php

}


if(isset($_REQUEST['Resendotp'])){
	
	?>
	<form>
		<div class="form-group">
		<label for="recipient-name" class="col-form-label">Email</label>
		<input type="email" id="email" class="form-control" value="<?php echo $_REQUEST['email']; ?>"  >
	  </div>
	  <div class="form-group">
		<label for="recipient-name" class="col-form-label">Mobile</label>
		<input type="mobile" id="mobile" class="form-control"  value="<?php echo $_REQUEST['mobile']; ?>">
	  </div>
	  
	 <div class="center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary" data-dismiss="modal" onClick="setState('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','validateForm1=1&email='+$('#email').val()+'&mobile='+$('#mobile').val()+'&oldmobile=<?php echo $_REQUEST['mobile']; ?>&oldemail=<?php echo $_REQUEST['email']; ?>&resetid=<?php echo $_REQUEST['rid']; ?>')">Send OTP</a>
		</div>
    </form>	
<?php
}

	 if(isset($_REQUEST['validateForm1'])){
		 	$query=query("select * from student_users where  id='".$_REQUEST['resetid']."'");
			$rowl=mysqli_fetch_array($query);
			$otp=rand(1,999999);
			$message="Hi Your OTP: ".$otp." To Verify your account use the otp and complete the process";
			$url="http://bsms.entrolabs.com/spanelv2/api.php?username=neetjeeotp&password=Neet@1234&to=".$_REQUEST['mobile']."&from=NEJEGU&message=".urlencode($message)."";
			$res=@file($url);
			$url1="http://rizee.in:3002/sendmail/".$_REQUEST['email']."/".$otp."";
			$res1=@file($url1);
			//query("insert  verify_students  set name='".$rowl['name']."',email_otp='".$otp."',mobile_otp='".$otp."',email='".$_REQUEST['email']."',mobile='".$_REQUEST['mobile']."',timestamp='".time()."'" );
			query("INSERT INTO verify_students VALUES('','".$rowl['name']."','".$otp."','".$otp."','".$_REQUEST['email']."','".$_REQUEST['mobile']."','','','','".time()."')  ON DUPLICATE KEY UPDATE email_otp='".$otp."',mobile_otp='".$otp."',mobile='".$_REQUEST['mobile']."',email='".$_REQUEST['email']."'" );
			$result=query("update  student_users  set email='".$_REQUEST['email']."',mobile='".$_REQUEST['mobile']."' where  id='".$_REQUEST['resetid']."'");
			
			if($result){
				query("update  students  set email='".$_REQUEST['email']."',mobile='".$_REQUEST['mobile']."' where  contact_no='".$_REQUEST['oldmobile']."'");
				?>
				

				<script type="text/javascript">
				
					alert("OTP Sent Successfully");
					setStateGet('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1');
					setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
					
					
				</script>
				<?php
			}

	
		

	}

if(isset($_REQUEST['validateForm'])){
	
			if($_REQUEST['status']=='enablestatus'){
				$result=query("update  student_users  set estatus='1',valid='1' where  mobile='".$_REQUEST['mobile']."'");
				query("insert register_userlog set mobile='".$_REQUEST['mobile']."',eusername='".$session->username."',message=2,timestamp='".time()."'");
				if($result){
					query("update  students  set estatus='1' where  contact_no='".$_REQUEST['mobile']."'");
					?>
					

					<script type="text/javascript">
					
						alert("User Enabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
						
						
					</script>
					<?php
				}

		}else if($_REQUEST['status']=='disablestatus'){
			
			$result=query("update  student_users  set estatus='0',valid='0' where  mobile='".$_REQUEST['mobile']."'");
			query("insert register_userlog set mobile='".$_REQUEST['mobile']."',eusername='".$session->username."',message=3,timestamp='".time()."'");
			if($result){
				query("update  students  set estatus='0' where  contact_no='".$_REQUEST['mobile']."'");
				?>

				<script type="text/javascript">
				
					alert("User Disabled Successfully");
						setStateGet('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1');
						setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
					
				</script>
				
				<?php
			}
		}
		

	}

	
	if(isset($_REQUEST['validateForm3'])){
		
		$_SESSION['error'] = array();
		$post = $session->cleanInput($_REQUEST);
		$field = 'plan_id';
		if(!$post['plan_id'] || strlen(trim($post['plan_id'])) == 0){
		  $_SESSION['error'][$field] = "* Plan Id cannot be empty";
		}

		$field = 'expiry_date';
		if(!$post['expiry_date'] || strlen(trim($post['expiry_date'])) == 0){
		  $_SESSION['error'][$field] = "* Expiry Date cannot be empty";
		}

		$field = 'comments';
		if(!$post['comments'] || strlen(trim($post['comments'])) == 0){
		  $_SESSION['error'][$field] = "* Comments cannot be empty";
		}
		$field = 'amount';
		if(!$post['amount'] || strlen(trim($post['amount'])) == 0){
		  $_SESSION['error'][$field] = "* Amount cannot be empty";
		}
		if(count($_SESSION['error']) > 0){
	?>
    <script type="text/javascript">
		$('#myModal1').modal('show');
      setState('viewdetails1','<?php echo SECURE_PATH;?>registreduser2/process.php','viewdetails1=1&plan_id=<?php echo $post['plan_id'];?>&mobile=<?php echo $post['mobile'];?>&expiry_date=<?php echo strtotime($post['expiry_date']);?>&referral_code=<?php echo $post['referral_code'];?>&amount=<?php echo $post['amount'];?>&transaction_id=<?php echo $post['transaction_id'];?>&comments=<?php echo $post['comments'];?>');
    </script>
  <?php
	}
	else{
		$sell1=query("select * from student_plans where estatus='1' and id='".$_REQUEST['plan_id']."'");
		$rowll1=mysqli_fetch_array($sell1);

		$sell2=query("select * from student_users where  mobile='".$_REQUEST['mobile']."'");
		$rowll2=mysqli_fetch_array($sell2);
		query("insert  `students_plans_log` set mobile='".$rowll2['mobile']."',prev_planid='".$rowll2['current_plan_id']."',prev_expiry_date='".$rowll2['expiry_date']."',current_planid='".$_REQUEST['plan_id']."',current_expiry_date='".strtotime($_REQUEST['expiry_date'])."',estatus=1,username='".$session->username."',timestamp='".time()."'");
		
		$result=query("insert  student_transactions  set mobile='".$_REQUEST['mobile']."',plan_id='".$_REQUEST['plan_id']."',referral_id='0',actual_amount='".$rowll1['amount']."',paid_amount='".$rowll1['amount']."',timestamp='".time()."'");
		$result=query("update  student_users  set current_plan_id='".$_REQUEST['plan_id']."',expiry_date='".strtotime($_REQUEST['expiry_date'])."' where mobile='".$_REQUEST['mobile']."'");



		
		if($result){
			$date=date("dmy",time());
			$order_id=$date.rand(4);
			
			$result=query("insert  payments  set name='".$rowll2['name']."',email='".$rowll2['email']."',mobile='".$_REQUEST['mobile']."',plan_id='".$_REQUEST['plan_id']."',referral_id='".$_REQUEST['referral_code']."',amount='".$_REQUEST['amount']."',order_id='".$order_id."',transaction_id='".$_REQUEST['transaction_id']."',bank_info='".$_REQUEST['comments']."',status='2',source='2',payment_type='direct',estatus='1',timestamp='".time()."'");
			?>
			

			<script type="text/javascript">
			
				alert("Plan Updated Successfully");
				setStateGet('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','addForm=1');
				setStateGet('adminTable','<?php echo SECURE_PATH;?>registreduser2/process.php','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
				
				
			</script>
			<?php
		}

		
	}
	}
	if(isset($_REQUEST['getbranch'])){
		if(isset($_REQUEST['institution_id'])){
			$_REQUEST['institution_id']=$_REQUEST['institution_id'];
		}else{
			$_REQUEST['institution_id']="";
		}

		?>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Branch<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="institution_id" id="branch_id" class="form-control"  onChange="setState('bbb','<?php echo SECURE_PATH;?>registreduser2/process.php','getsection=1&institution_id=<?php echo $_REQUEST['institution_id']; ?>&branch_id='+$('#branch_id').val()+'')">
					<option value=''>-Select-</option>
					<?php
					$row = query("select * from branches where estatus='1' and institution_id='".$_REQUEST['institution_id']."'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['branch_id'])) { if($_REQUEST['branch_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['branch_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['branch_id'])){ echo $_SESSION['error']['branch_id'];}?></span>
				</div>
			</div>
			<?php
	}
		if(isset($_REQUEST['getsection'])){
		if(isset($_REQUEST['institution_id'])){
			$_REQUEST['institution_id']=$_REQUEST['institution_id'];
		}else{
			$_REQUEST['institution_id']="";
		}
		if(isset($_REQUEST['branch_id'])){
			$_REQUEST['branch_id']=$_REQUEST['branch_id'];
		}else{
			$_REQUEST['branch_id']="";
		}


		?>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Section<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="section_id" id="section_id" class="form-control" onChange="setState('ddd','<?php echo SECURE_PATH;?>registreduser2/process.php','getpackage=1&institution_id='+$('#institution_id').val()+'')">
					<option value=''>-Select-</option>
					<?php
					$row = query("select * from sections  where estatus='1' and institution_id='".$_REQUEST['institution_id']."' and branch_id='".$_REQUEST['branch_id']."'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['section_id'])) { if($_REQUEST['section_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['section_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['section_id'])){ echo $_SESSION['error']['section_id'];}?></span>
				</div>
			</div>
			<?php
	}
	if(isset($_REQUEST['getpackage'])){
		if(isset($_REQUEST['institution_id'])){
			$_REQUEST['institution_id']=$_REQUEST['institution_id'];
		}else{
			$_REQUEST['institution_id']="";
		}
		

		?>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Packages<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="package_id" id="package_id" class="form-control" onChange="setState('fff','<?php echo SECURE_PATH;?>registreduser2/process.php','getcategory=1&institution_id=<?php echo $_REQUEST['institution_id']; ?>&package_id='+$('#package_id').val()+'');setState('ggg','<?php echo SECURE_PATH;?>registreduser2/process.php','getexamid=1&institution_id=<?php echo $_REQUEST['institution_id']; ?>&package_id='+$('#package_id').val()+'')" >
					<option value=''>-Select-</option>
					<?php
					
					$row = query("select * from packages where estatus='1' and institution_id='".$_REQUEST['institution_id']."'");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['package_id'])) { if($_REQUEST['package_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['package_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['package_id'])){ echo $_SESSION['error']['package_id'];}?></span>
				</div>
			</div>
			<?php
	}
	if(isset($_REQUEST['getexamid'])){
		if(isset($_REQUEST['institution_id'])){
			$_REQUEST['institution_id']=$_REQUEST['institution_id'];
		}else{
			$_REQUEST['institution_id']="";
		}
		

		?>

		<div class="form-group row">
					<label class="col-sm-4 col-form-label">Exam<span style="color:red;">*</span></label>
					<div class="col-sm-8">
					<select name="exam" id="exam_id" class="form-control" >
						<option value=''>-Select-</option>
						<?php
			
						$sel = query("select id,exams_covered from packages where estatus='1' and institution_id='".$_REQUEST['institution_id']."'  and id='".$_REQUEST['package_id']."'  ");
						$row=mysqli_fetch_array($sel);
						$sell1=$database->query("select * from exam where estatus='1' and id ='".$row['exams_covered']."'");
						while($data = mysqli_fetch_array($sell1))
						{
						?>
							<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['exam_id'])) { if($_REQUEST['exam_id']==$data['id']) { echo 'selected="selected"'; }  }else{ echo 'selected="selected"';} ?>><?php echo ucwords($data['exam']);?></option>
						<?php
						}
						?>
						
					</select>
					<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['exam_id'])){ echo $_SESSION['error']['exam_id'];}?></span>
					</div>
				</div>
	<?php
	}
					
	if(isset($_REQUEST['getcategory'])){
		if(isset($_REQUEST['institution_id'])){
			$_REQUEST['institution_id']=$_REQUEST['institution_id'];
		}else{
			$_REQUEST['institution_id']="";
		}
		if(isset($_REQUEST['package_id'])){
			$_REQUEST['package_id']=$_REQUEST['package_id'];
		}else{
			$_REQUEST['package_id']="";
		}
		

		?>
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Categories<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				<select name="category_id" id="category_id" class="form-control" >
					<option value=''>-Select-</option>
					<?php
			
					$row = query("select * from categories where estatus='1' and institution_id='".$_REQUEST['institution_id']."'  and package_id='".$_REQUEST['package_id']."' ");
					while($data = mysqli_fetch_array($row))
					{
						?>
					<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['category_id'])) { if($_REQUEST['category_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['category_name']);?></option>
					<?php
					}
					?>
				</select>
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['category_id'])){ echo $_SESSION['error']['category_id'];}?></span>
				</div>
			</div>
			<?php
	}
	if(isset($_REQUEST['viewdetails1'])){
		
		if(isset($_REQUEST['expiry_date'])){
			if($_REQUEST['expiry_date']!='' && $_REQUEST['plan_id']!=''){
				$_REQUEST['expiry_date']=date('m/d/Y',$_REQUEST['expiry_date']);
			}else{
				$_REQUEST['expiry_date']="";
			}
		}else{
			$_REQUEST['expiry_date']="";
		}
		if(isset($_REQUEST['plan_id'])){
			if($_REQUEST['plan_id']!=''){
				$_REQUEST['plan_id']=$_REQUEST['plan_id'];
			}else{
				$_REQUEST['plan_id']="";
			}
		}else{
			$_REQUEST['plan_id']="";
		}
		$sql1=query("select * from student_users where id='".$_REQUEST['id']."'");
		$rowl1=mysqli_fetch_array($sql1);
		?>
	<form>
		<div class="form-group">
		<label for="recipient-name" class="col-form-label">Plan </label>
		<select name="plan_id" id="plan_id" class="form-control" onchange="planexpiryfunction();">
			<option value=''>-Select-</option>
		<?php
		$sql=query("SELECT * FROM `student_plans` where estatus='1'");
		while($rowl=mysqli_fetch_array($sql)){
			?>
				<option value="<?php echo $rowl['id'];?>" <?php if(isset($_REQUEST['plan_id'])) { if($_REQUEST['plan_id']==$rowl['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($rowl['plan_name']);?></option>
			<?php
		}
		?>
			</select>
			<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['plan_id'])){ echo $_SESSION['error']['plan_id'];}?></span>
	  </div>
	  <div class="form-group" >
		<label for="recipient-name" class="col-form-label">Expiry Date</label>
		<input type="text" id="expiry_date" class="form-control"  readonly  value="<?php echo $_REQUEST['expiry_date']; ?>">
	<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['expiry_date'])){ echo $_SESSION['error']['expiry_date'];}?></span>
		
	  </div>
	  <input type="hidden" id="expiry_date1" class="form-control datepicker"  readonly  value="<?php echo $rowl1['expiry_date']; ?>">
	  <div class="form-group">
		<label for="recipient-name" class="col-form-label">Referral Code </label>
		<select name="referral_code" id="referral_code" class="form-control" onchange="planexpiryfunction();">
			<option value=''>-Select-</option>
		<?php
		$sql=query("SELECT * FROM referral_codes where estatus='1'");
		while($rowl=mysqli_fetch_array($sql)){
			?>
				<option value="<?php echo $rowl['id'];?>" <?php if(isset($_REQUEST['referral_code'])) { if($_REQUEST['referral_code']==$rowl['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($rowl['referral_code']);?></option>
			<?php
		}
		?>
			</select>
			<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['referral_code'])){ echo $_SESSION['error']['referral_code'];}?></span>
	  </div>
	  <div class="form-group" >
		<label for="recipient-name" class="col-form-label">Amount</label>
		<input type="text" id="amount" class="form-control" value="<?php echo $_REQUEST['amount']; ?>">
	<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['amount'])){ echo $_SESSION['error']['amount'];}?></span>
		
	  </div>
	  <div class="form-group" >
		<label for="recipient-name" class="col-form-label">Transaction Id</label>
		<input type="text" id="transaction_id" class="form-control"    value="<?php echo $_REQUEST['transaction_id']; ?>">
	<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['transaction_id'])){ echo $_SESSION['error']['transaction_id'];}?></span>
		
	  </div>
	  <div class="form-group" >
		<label for="recipient-name" class="col-form-label">Comments</label>
		<textarea id="comments" class="form-control" rows='2'><?php echo $_REQUEST['comments']; ?></textarea>
	<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['comments'])){ echo $_SESSION['error']['comments'];}?></span>
		
	  </div>
	 <div class="center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary" data-dismiss="modal" onClick="setState('adminForm','<?php echo SECURE_PATH;?>registreduser2/process.php','validateForm3=1&plan_id='+$('#plan_id').val()+'&expiry_date='+$('#expiry_date').val()+'&referral_code='+$('#referral_code').val()+'&amount='+$('#amount').val()+'&transaction_id='+$('#transaction_id').val()+'&comments='+$('#comments').val()+'&mobile=<?php echo $rowl1['mobile']; ?>')">Update</a>
		</div>
    </form>	
<?php
		  unset($_SESSION['error']);
	}
	?>
	
	<script>
	function planexpiryfunction(){
		var value1=$('#plan_id').val();
		if(value1=='5'){
			var plandays="15";
		}else if(value1=='1'){
			var plandays="31";
		}else if(value1=='2'){
			var plandays="365";
		}
		var plan_days=$('#plan_days').val();
		var value2=$('#expiry_date1').val();
		 var start =  moment();
		var end=moment().add(plandays,'days');
		var enddate=end.format('MM/DD/YYYY');
		if(value2==''){
			$('#expiry_date').val(enddate);
		}else{
			var enddate1=moment().format("X"); 
			if(value2>enddate1){
				var diffDate = (value2 - enddate1) / ( 60 * 60 * 24);
				var days = Math.round(diffDate);
				var tot=parseInt(days)+parseInt(plandays);
				var pdays=moment().add(tot,'days');	
				var enddate3=pdays.format('MM/DD/YYYY');
				$('#expiry_date').val(enddate3);
			}else{
				$('#expiry_date').val(enddate);
			}
			
		}
		
	}
	</script>
