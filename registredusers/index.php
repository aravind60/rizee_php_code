<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    	//header('Location: '.SECURE_PATH);
    }
    else
    {
    ?>
         <div class="content-wrapper">
				<div class="modal" id="myModal">
						<div class="modal-dialog">
						  <div class="modal-content">
						  
							<!-- Modal Header -->
							<div class="modal-header">
							  <h6 class="modal-title">Reset Password</h6>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							
							<!-- Modal body -->
							<div class="modal-body" id="viewdetails">
							 
							</div>
							
						  </div>
						</div>
					  </div>
					  <div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog">
						  <div class="modal-content">
						  
							<!-- Modal Header -->
							<div class="modal-header">
							  <h6 class="modal-title">Update Plan Days</h6>
							  <button type="button" class="close" data-dismiss="modal">&times;</button>
							</div>
							
							<!-- Modal body -->
							<div class="modal-body" id="viewdetails1">
							 
							</div>
							
						  </div>
						</div>
					  </div> 	
					  
                   
                    <section class="content-area">
                        <div class="container">
                            <div class="row">
                                <div class="col-xl-12 col-lg-12">
                                    <div class="card border-0 shadow mb-4">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    
                    <section>
                        <div id="adminTable">
                            <script type="text/javascript">
                                setStateGet('adminTable','<?php echo SECURE_PATH;?>registredusers/process.php','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
                            </script>
                        </div>
                    </section>
                 </div>

				 <div class="toast text-center mx-auto" id="enableuser1"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"  class="d-flex justify-content-center align-items-center;"   style="position: absolute;
						top: 30%;left:0;right:0" >

		  <!-- Then put toasts within -->
		  <div  role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
			  <strong class="mr-auto">Question</strong>
			 
			  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="toast-body p-5">
			User Enabled Successfully
			</div>
		  </div>
		</div>

		 <div class="toast text-center mx-auto" id="disableuser1"  aria-live="polite" aria-atomic="true" class="d-flex justify-content-center align-items-center;"  class="d-flex justify-content-center align-items-center;"   style="position: absolute;
						top: 30%;left:0;right:0" >

		  <!-- Then put toasts within -->
		  <div  role="alert" aria-live="assertive" aria-atomic="true">
			<div class="toast-header">
			  <strong class="mr-auto">Question</strong>
			 
			  <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
				<span aria-hidden="true">&times;</span>
			  </button>
			</div>
			<div class="toast-body p-5">
			User Disabled Successfully
			</div>
		  </div>
		</div> 
	</div>   
 <?php
    
    }
?>