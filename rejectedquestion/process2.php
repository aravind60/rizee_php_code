<?php
include('../include/session.php');
error_reporting(0);
?>


<?php

if(isset($_REQUEST['viewDetails1']))
{
	if($_REQUEST['list1type']=='roman'){
			$list1type="upper-roman";
		}else if($_REQUEST['list1type']=='alphabets'){
			$list1type="upper-alpha";
		}else if($_REQUEST['list1type']=='numbers'){
			$list1type="decimal";
		}else{
			$list1type="upper-alpha";
		}
		if($_REQUEST['list2type']=='roman'){
			$list2type="upper-roman";
		}else if($_REQUEST['list2type']=='alphabets'){
			$list2type="upper-alpha";
		}else if($_REQUEST['list2type']=='numbers'){
			$list2type="decimal";
		}else{
			$list2type="upper-alpha";
		}
	?>
	<div class="container">
				<div class="row">
					<?php
					$_REQUEST['exam']=rtrim($_REQUEST['exam'],',');
					$_REQUEST['class']=rtrim($_REQUEST['class'],',');
					$_REQUEST['chapter']=rtrim($_REQUEST['chapter'],',');
					$_REQUEST['topic']=rtrim($_REQUEST['topic'],',');
					
					if($_REQUEST['class']=='1'){
						$class='XI';
					}else if($_REQUEST['class']=='2'){
						$class='XII';
					}else  if($_REQUEST['class']=='1,2'){
						$class='XI,XII';
					}
					if($_REQUEST['exam']=='1'){
						$exam="NEET";
					}else if($_REQUEST['exam']=='2'){
						$exam="JEE";
					}else if($_REQUEST['exam']=='1,2'){
						$exam="NEET,JEE";
					}
					$chapter ='';
					$sql = $database->query("SELECT * FROM chapter WHERE estatus='1' and id IN(".$_REQUEST['chapter'].")"); 
					while($row2=mysqli_fetch_array($sql)){
						$chapter .= $row2['chapter'].",";
					}

						$topic ='';
						$k=1;
						$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$_REQUEST['topic'].")"); 
						while($row1=mysqli_fetch_array($zSql1)){
							$topic .= $row1['topic'].",";
							$k++;
						}
						?>
					<div class="col-md-2">
						<h6>Class</h6>
						<p><?php echo $class; ?></p>
					</div>
					
					<div class="col-md-2">
						<h6>Exam</h6>
						<p><?php echo $exam; ?></p>
					</div>
					<div class="col-md-2">
						<h6>Subject</h6>
						<p><?php echo $database->get_name('subject','id',$_REQUEST['subject'],'subject'); ?></p>
					</div>
					<div class="col-md-6">
						<h6>Chapter</h6>
						<p><?php echo rtrim($chapter,","); ?></p>
					</div>
					<?php
					 if($_REQUEST['qtype']=='3'){
						$type="Matching";
					
						$list1='';
						$list2='';
						$obj = explode("^",rtrim($_REQUEST['data5'],'^'));
						foreach($obj as $rowpost2)
						{
							if(strlen($rowpost2)>0)
							{
								$rowpost = explode("_",$rowpost2);
								$list1.=$rowpost[0]."_";
								$list2.=$rowpost[1]."_";
							}
						}
						$qlist1 = explode("_",$list1);
						$qlist2 = explode("_",$list2);
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							
							<div class="questionlist-types d-flex">
								<div>
								<h5>List1</h5>
									<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
										<?php
										foreach($qlist1 as $qqlist)
										{
										
											if(strlen($qqlist)>0)
											{
												echo '<li style="width:150px;">'.$qqlist.'</li>';
											}
										}
										?>
									
									</ul>
								</div>
								<div class="pl-5">
									<h5>List2</h5>
									<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
										<?php
										foreach($qlist2 as $qqlist1)
										{
											if(strlen($qqlist1)>0)
											{
												echo '<li style="width:150px;">'.$qqlist1.'</li>';
											}
										}
										?>
									</ol>
								</div>
							</div>
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option4']; ?>
								</div>
							</div>
						</div>
						<div class="col-md-12 mt-2">
							<h6>Explanation</h6>
							<p><?php echo urldecode($_SESSION['explanation']); ?></p>
								

						</div>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($_REQUEST['answer'],","); ?></p>
								

						</div>
					<?php
					}else if($_REQUEST['qtype']=='9'){
						$type="Matrix";
						$list1='';
						$list2='';
						$obj = explode("^",rtrim($_REQUEST['data6'],'^'));
						foreach($obj as $rowpost2)
						{
							if(strlen($rowpost2)>0)
							{
								$rowpost = explode("_",$rowpost2);
								$list1.=$rowpost[0]."_";
								$list2.=$rowpost[1]."_";
							}
						}
						$qlist1 = explode("_",$list1);
						$qlist2 = explode("_",$list2);
						?>
						<div class="col-md-6">
							<h6>Question Type</h6>
							<p><?php echo $type; ?></p>
						</div>
						
						<div class="col-md-12 mt-2 questionView">
							<h6>Question</h6>
							
								<div class="questionlist-types d-flex">
									<div>
									<h5>List1</h5>
										<ul style="list-style-type: <?php echo $list1type; ?>;padding: 0px 13px;">
											<?php
											foreach($qlist1 as $qqlist)
											{
											
												if(strlen($qqlist)>0)
												{
													echo '<li style="width:150px;">'.$qqlist.'</li>';
												}
											}
											?>
										
										</ul>
									</div>
									<div class="pl-5">
										<h5>List2</h5>
										<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
											<?php
											foreach($qlist2 as $qqlist1)
											{
												if(strlen($qqlist1)>0)
												{
													echo '<li style="width:150px;">'.$qqlist1.'</li>';
												}
											}
											?>
										</ol>
									</div>
								</div>
							
							<div class="row">

								<div class="col-md-1 py-2 ">
									(A) 						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option1']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(B)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option2']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(C)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option3']; ?>
								</div>

								<div class="col-md-1 py-2 ">
									(D)						</div>

								<div class="col-md-5 py-2 ">
									<?php echo $_REQUEST['option4']; ?>
								</div>
							</div>
						</div>
						<div class="col-md-12 mt-2">
							<h6>Explanation</h6>
							<p><?php echo urldecode($_SESSION['explanation']); ?></p>
								

						</div>
						<div class="col-md-12 mt-2">
							<h6>Answer</h6>
							<p><?php echo rtrim($_REQUEST['answer'],","); ?></p>
								

						</div>
					<?php
					}
						?>
						
					
				</div>
			</div>
			
			

	
<?php }

 ?>