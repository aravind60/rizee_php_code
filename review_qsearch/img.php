<?php
ini_set('display_errors','On');
include('../include/session.php');
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

if(isset($_REQUEST)){
    
   $data = json_decode(file_get_contents('php://input'), true);

  $_SESSION['question'] = $data['question'];
  $_SESSION['option1'] = $data['option1'];
  $_SESSION['option2'] = $data['option2'];
  $_SESSION['option3'] = $data['option3'];
  $_SESSION['option4'] = $data['option4'];
  $_SESSION['explanation'] = $data['explanation'];
  $_SESSION['compquestion'] = $data['compquestion'];
   $_SESSION['mat_question'] = $data['mat_question'];
   $_SESSION['mat_que_paragraph'] = $data['mat_que_paragraph'];
$_SESSION['mat_que_footer'] = $data['mat_que_footer'];
echo json_encode($data);
}




