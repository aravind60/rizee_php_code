<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['addForm'])){
		
		$data_sel =  $database->query("select * from users where username='".$session->username."'");
		$row = mysqli_fetch_array($data_sel);
		$sqll=$database->query("select count(id)  from createquestion where estatus='1' and class in (".$row['class'].") find_in_set(".$row['class'].",class)>0 and subject='".$row['subject']."' and chapter IN (".$row['chapter'].") and vstatus1='1'");
		$row1 = mysqli_fetch_array($sqll);
		if($_REQUEST['class']!=''){
			$_REQUEST['class']=$_REQUEST['class'];
		}else{
			$_REQUEST['class']="1";
		}

	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<div class="subject-analysis-content white_block m_top10">
			<div class="background_yash">
				<div class="row">
					<div class="col-sm-3">
						<label class="text-uppercase font-weight-bold f_12">Class</label>
						
							<select   class="form-control chosen-select " id="class"   >
								<?php 
								$seclass=$database->query("select * from class where estatus='1' and id  in (".rtrim($row['class'],",").")");
								while($rowclass=mysqli_fetch_array($seclass)){ ?>
								
								<option value="<?php  echo $rowclass['id'];  ?>" <?php if(isset($_REQUEST['class'])){ if($_REQUEST['class']==$rowclass['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowclass['class'];?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-3">
						<label class="text-uppercase font-weight-bold f_12">Question Type</label>
						
						<select   class="form-control chosen-select " id="totalquestions"   >
							<option value='verified' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='verified'){ echo 'selected';}else{ echo 'selected';}} ?> >Verified Question</option>
							<option value='notverified' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='notverified'){ echo 'selected';}else{}} ?> >Not Verified</option>
							<option value='All' <?php if(isset($_REQUEST['totalquestions'])){ if($_REQUEST['totalquestions']=='All'){ echo 'selected';}else{}} ?> >All Questions</option>
					</select>
					</div>
					<div class="col-sm-3">
						<label class="text-uppercase font-weight-bold f_12">Chapter</label>
						
						<select id="chapter" class="form-control chosen-select" >
							<option value=''>All</option>
							<?php
					
							$sqoll=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$row['subject']."' and id in (".$row['chapter'].") ");
							while($rowoll=mysqli_fetch_array($sqoll)){ ?>
								<option value="<?php  echo $rowoll['id'];  ?>" <?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']==$rowoll['id']){ echo 'selected';}else{}} ?>  ><?php echo $rowoll['chapter'];?></option>
							<?php } ?>
						</select>
					</div>
					<div class="col-sm-3" id="attributesOverveiw">
						<a class="btn btn-outline-success btn_bg_white text-uppercase m_top30" onclick="setState('adminForm','<?php echo SECURE_PATH;?>reviewdashboard/process.php','addForm=1&class='+$('#class').val()+'&totalquestions='+$('#totalquestions').val()+'&chapter='+$('#chapter').val());" >SUBMIT</a>
					</div>
				</div>
			</div>
			<?php
			if($_REQUEST['class']!=''){
				$class=" AND class LIKE '%".$_REQUEST['class']."%'";
			}else{
				$class=" AND class IN (".$row['class'].")";
			}
			if($_REQUEST['chapter']!=''){
				$chapter=" AND chapter in (".$_REQUEST['chapter'].")";
			}else{
				$chapter=" AND chapter IN (".$row['chapter'].")";
			}
			if($_REQUEST['topic']!=''){
				$topic=" AND topic in (".$_REQUEST['topic'].")";
			}else{
				$topic="";
			}
			$data_sel =  $database->query("select * from users where username='".$session->username."'");
			$row = mysqli_fetch_array($data_sel);
			//$class2=" and class='".$row['subject']."'";
			$subject=" and subject='".$row['subject']."'";
			$chapter2=" and chapter IN (".$row['chapter'].")";
			$review_status=" and review_status='1'";
			$_REQUEST['subject']=$row['subject'];
			if($_REQUEST['totalquestions']=='All'){
				$con=$class.$subject.$chapter.$topic;

			}else if($_REQUEST['totalquestions']=='notverified'){
				$vstatus=" and vstatus1='0'";
				$con=$class.$subject.$chapter.$topic.$vstatus;

			}else if($_REQUEST['totalquestions']=='verified'){
				$vstatus=" and vstatus1='1'";
				$con=$class.$subject.$chapter.$topic.$vstatus;
				
			}else{
				$vstatus=" and vstatus1='1'";
				$con=$class.$subject.$chapter.$topic.$vstatus;

			}
			
			
			/*$totchaptersquev=$database->query("select count(id) from createquestion where estatus='1'".$con."");
			$rowchapterquev=mysqli_fetch_array($totchaptersquev);

			$totchaptersquevr=$database->query("select count(id) from createquestion where estatus='1' and review_status='0'".$con."");
			$rowchapterquevr=mysqli_fetch_array($totchaptersquevr);

			$totchaptersquevr1=$database->query("select count(id) from createquestion where estatus='1' and review_status='1'".$con."");
			$rowchapterquevr1=mysqli_fetch_array($totchaptersquevr1);

			$totchaptersquea=$database->query("select count(*) from createquestion where estatus='1'".$con."");
			$rowchapterquea=mysqli_fetch_array($totchaptersquea);*/
			if(isset($_POST['chapoverview'])){
				$ctitle=$database->get_name('chapter','id',$_POST['chapter'],'chapter');
			}else{
				$ctitle="All Chapters";
			}
			$tot=0;
			$totchap=$database->query("select count(id) from chapter where estatus='1' and id IN (".$row['chapter'].")".$class.$subject."");
			$rowchap=mysqli_fetch_array($totchap);

			$totchap2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."'".$subject."");
			while($rowchap1=mysqli_fetch_array($totchap2)){
				
				$totchap=$database->query("select count(id) from review_chapters where estatus='1' and class='".$_REQUEST['class']."'  and subject='".$row['subject']."' and chapter='".$rowchap1['id']."' group by chapter");
				
				$rowchap1=mysqli_num_rows($totchap);
				$tot=$tot+$rowchap1;
			}
			$ptot=$rowchap[0]-$tot;

			$tott=0;
			$totchapt=$database->query("select count(id) from topic where estatus='1' and class='".$_REQUEST['class']."' and chapter='".$_REQUEST['chapter']."'".$subject."");
			$rowchapt=mysqli_fetch_array($totchapt);
			
			while($rowchapt1=mysqli_fetch_array($rowchapt)){
				$totchap=$database->query("select count(id) from review_chapters where estatus='1' and class='".$_REQUEST['class']."'  and subject='".$row['subject']."' and chapter='".$rowchapt1['chapter']."' and topic='".$rowchapt1['id']."'");
				$rowchap1=mysqli_fetch_array($totchap);
				$tott=$tott+$rowchap1[0];
			}
			$ptott=$rowchapt[0]-$tott;
			
			?>
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<h5 class="card-header text-center text-uppercase font-weight-600 subjectanalysis"><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?> Subject Analysis</h5>
						<div class="card-body p-2">
							<h6 class="card-title text-uppercase font-weight-600">OverView</h6>    
							<div class="row">
								<div class="col-sm-3">
									<div class="br_yash">
										<h6 class="font-weight-600 text-uppercase font_14">Total</h6>
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $rowchapt[0];  }else{ echo $rowchap[0]; }  }else{ echo $rowchap[0]; }?></h4>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_yash">
										<h6 class="font-weight-600 text-uppercase font_14">Chapters/Topics</h6>
												
												<h4 class="">
													<?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $rowchapt[0];  }else{ echo $rowchap[0]; }  }else{ echo $rowchap[0]; }?>
												
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_yellow">
										<div class="row">
											<div class="col-sm-8">
												<h6 class="font-weight-600 text-uppercase font_14">Review Pending</h6>
											</div>
											<div class="col-sm-4 float-right">
												<img class="float-right icon_view" src="<?php echo SECURE_PATH;?>vendor/images/in progress.png">
											</div>
											<div class="col-sm-12">
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $ptott;  }else{ echo $ptot; }  }else{ echo $ptot; }?></h4>
												
											</div>
										 </div>
									</div>
								</div>
								<div class="col-sm-3">
									<div class="br_green">
										<div class="row">
											<div class="col-sm-8">
												<h6 class="font-weight-600 text-uppercase font_14">Completed</h6>
											</div>
											<div class="col-sm-4 float-right">
												<img class="float-right icon_view" src="<?php echo SECURE_PATH;?>vendor/images/completed.png">
											</div>
											<div class="col-sm-12">
												<h4 class="m_top10"><?php if(isset($_REQUEST['chapter'])){ if($_REQUEST['chapter']!=''){ echo $tott;  }else{ echo $tot; }  }else{ echo $tot; }?></h4>
											</div>
										 </div>
									</div>
								</div>
							</div>
						</div>
					  </div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<div class="card-header p-2" id="questionattribute">
							<div class="row">
								<div class="col-sm-12">
									
										
										<h5 class="mb-0">
											<a href='' class="d-flex justify-content-between align-items-center"
											data-toggle="collapse" data-target="#attributediv" aria-expanded="true"
											aria-controls="attributediv" onclick="setStateGet('questionattribute','<?php echo SECURE_PATH;?>reviewdashboard/process5.php','questionattribute=1&class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>')" >
												<h5 class="card-title mb-0"><b>Attribute Overview</b></h5>
												<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
											</a>
			
			
										</h5>
									
																					
								</div>
							</div>
						</div>
					</div>	
				</div>		 
			</div>
			<div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<div class="card-header p-2" id="questionCategory">
							<div class="row">
								<div class="col-sm-12">
									
								
										<h5 class="mb-0">
											<a href='' class="d-flex justify-content-between align-items-center"
											data-toggle="collapse" data-target="#customdiv" aria-expanded="true"
											aria-controls="customdiv" onclick="setStateGet('questionCategory','<?php echo SECURE_PATH;?>reviewdashboard/process7.php','questionCategory=1&class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>')" >
												<h5 class="card-title mb-0"><b>Question Category Vs Question types</b></h5>
												<h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
											</a>
										</h5>
									
																					
								</div>
							</div>
						</div>
					</div>	
				</div>		 
			</div>	
			<!-- <div class="row">
				<div class="col-sm-12">
					<div class="card m_top10">
						<div class="card-header p-2">
							<div class="row">
								<div class="col-sm-3">
									<h6 class="text-uppercase font-weight-600 mt-1 selectedNameCls">Question Usage Set Wise Details</h6>
								</div>
								<div class="col-sm-9">
									
									
									<ul class="subject-wise-analysis list-inline float-right switch-custom questionWiseCls mb-0 attribute nav nav-pills attribute" id="pills-tab" role="tablist">
										<li>
											<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
											   role="tab" aria-controls="pills-home" aria-selected="true"><b>Question Usage Set Wise</b></a>
										</li>
										<li>
											<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
											   role="tab" aria-controls="pills-profile" aria-selected="false"><b>Question Type Wise</b></a>
										</li>
										<li>
											<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
											   role="tab" aria-controls="pills-contact" aria-selected="false"><b>Chapter Wise</b></a>
										</li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="card-body p-2">
							 <div class="tab-content" id="pills-tabContent">
							<?php
								
			
			
								if($_REQUEST['exam']=='1'){
									$cols="4";
								}else if($_REQUEST['exam']=='2'){
									$cols="3";
								}else if($_REQUEST['exam']=='1,2'){
									$cols="7";
								}else{
									$cols="7";
								}
								?>
							<div class="table-responsive">
										 <div class="tab-content" id="pills-tabContent">
											<?php
												
			
			
												if($_REQUEST['exam']=='1'){
													$cols="4";
												}else if($_REQUEST['exam']=='2'){
													$cols="3";
												}else if($_REQUEST['exam']=='1,2'){
													$cols="7";
												}else{
													$cols="7";
												}
												?>
												<div class="tab-pane fade show active" id="pills-home" role="tabpanel"
													 aria-labelledby="pills-home-tab">
													<table class="table table-bordered table_custom">
														<thead>
															<tr>
																<th rowspan="2"></th>
																<th rowspan="2">View</th>
																<th rowspan="2">Review</th>
																<th rowspan="2">OverAll</th>
																<th colspan="2">Total</th>
																<?php
																$data1 = $database->query("select * from complexity where estatus=1 ");
																while($data1row=mysqli_fetch_array($data1)){
				  
																	?>
																	<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
																	</th>
																<?php } ?>
															</tr>
															<tr>
																<?php
																$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata=mysqli_fetch_array($seldata)){
																?>
																	<th>
																		<?php echo $rowdata['question_theory']; ?>
																	</th>
																<?php
																	 }
																$sell2=$database->query("select * from complexity where estatus='1'");
																while($rowll2=mysqli_fetch_array($sell2)){
																	$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata=mysqli_fetch_array($seldata)){
																?>
																	<th >
																		<?php echo $rowdata['question_theory']; ?>
																	</th>
																<?php
																	 }
																}
																?>
															</tr>
															
														</thead>
														<tbody>
															<?php
															$l=1;
														
															$sell2=$database->query("select * from question_useset where estatus='1'");
															while($rowll2=mysqli_fetch_array($sell2)){
																$j=1;
																$count=0;
																$count1=0;
																$tcount=0;
																$sell12l=$database->query("select * from complexity where estatus='1'");
																while($rowll12=mysqli_fetch_array($sell12l)){
																	$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata11=mysqli_fetch_array($seldata)){
																		 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$rowdata11['id']."' and usageset='".$rowll2['id']."'".$con." ");
																		
																		
																		 $todayttocount=mysqli_fetch_array($todata);
																		
																		 if($rowdata11['id']=='1'){
																			$count=$count+$todayttocount[0];
																		 }else{
																			 $count1=$count1+$todayttocount[0];
																		 }
																		  $tcount=$tcount+$todayttocount[0];
																	}
																}
																
																?>
																<td><?php echo $rowll2['usageset']; ?></td>
																<td class="reviewdashboard-buttons">
																	<a style="cursor:pointer;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>reviewdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&complexity=&usageset=<?php echo $rowll2['id'];?>");' ><i class="fa fa-eye"  ></i></a>
																</td>
																 <td class="reviewdashboard-buttons">&nbsp;</td> 
																 <td>
																	<?php echo $tcount; ?>
																</td>
																		
																<?php
																$seldata=$database->query("select * from question_theory where estatus='1'");
																while($rowdata=mysqli_fetch_array($seldata)){
																	if($rowdata['id']==1){
																?>
																	<td>
																		<?php echo $count; ?>
																	</td>
																<?php
																	}else if($rowdata['id']==2){
																	?>
																	<td>
																		<?php echo $count1; ?>
																	</td>
																	<?php
																	}
																}
																
																$j=1;
																$s1=$database->query("select * from complexity where estatus='1'");
																while($rs1=mysqli_fetch_array($s1)){
																	$seldata1=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata1=mysqli_fetch_array($seldata1)){
																		 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity!='' and question_theory='".$rowdata1['id']."' and complexity='".$rs1['id']."' and usageset='".$rowll2['id']."'".$con." ");
																		
																		 $todayttocount=mysqli_fetch_array($todata);
																?>
																	<td>
																	  <?php  if($todayttocount[0]!=''){ echo $todayttocount[0]; } else{ echo '0'; } ?>
																	</td>
																<?php
																	 }
																}
															   ?>
																
															</tr>
															<?php
															$l++;
														}
														?>
														
														</tbody>
													</table>
												</div>
												<div class="tab-pane fade" id="pills-profile" role="tabpanel"
													 aria-labelledby="pills-profile-tab">
													<table class="table table-bordered table_custom">
						  										<thead>
						  											<tr>
						  												<th rowspan="2"></th>
						  												<th rowspan="2">View</th>
						  												<th rowspan="2">Review</th>
						  												<th rowspan="2">OverAll</th>
						  												<th colspan="2">Total</th>
						  												<?php
						  												$data1 = $database->query("select * from complexity where estatus=1 ");
						  												while($data1row=mysqli_fetch_array($data1)){
						  
						  													?>
						  													<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
						  													</th>
						  												<?php } ?>
						  											</tr>
						  											<tr>
						  												<?php
						  												$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th>
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												$sell2=$database->query("select * from complexity where estatus='1'");
						  												while($rowll2=mysqli_fetch_array($sell2)){
						  													$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th >
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												}
						  												?>
						  											</tr>
						  											
						  										</thead>
						  										<tbody>
						  											
						  												<?php
																	$l=1;
																
																	$sell2=$database->query("select * from questiontype where estatus='1'");
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$qcount=0;
																		$qcount1=0;
																		$qtcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$seldataq=$database->query("select * from question_theory where estatus='1'");
																			while($qrowdata=mysqli_fetch_array($seldataq)){
																			
																				 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."' and inputquestion='".$rowll2['id']."' and question_theory='".$qrowdata['id']."' ".$con."");
																				
																				$todayttocount=mysqli_fetch_array($todata);
																				if($qrowdata['id']=='1'){
																					$qcount=$qcount+$todayttocount[0];
																				 }else{
																					 $qcount1=$qcount1+$todayttocount[0];
																				 }
																				 $qtcount=$qtcount+$todayttocount[0];
																				
																			}
																		}
																		
																		?>
			
																		<tr>
																			
																			<td><?php echo $rowll2['questiontype']; ?></td>
																			<td class="reviewdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print"  target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>reviewdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&inputquestion=<?php echo $rowll2['id'];?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			
																			<td class="reviewdashboard-buttons">&nbsp;</td>
																			 <td>
																				<?php echo  $qtcount; ?>
																			</td>
																					
																			<?php
																			$seldata=$database->query("select * from question_theory where estatus='1'");
																			while($rowdata=mysqli_fetch_array($seldata)){
																				if($rowdata['id']==1){
																			?>
																				<td>
																					<?php echo $qcount; ?>
																				</td>
																			<?php
																				}else if($rowdata['id']==2){
																				?>
																				<td>
																					<?php echo $qcount1; ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			$sell12=$database->query("select * from complexity where estatus='1'");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$seldata=$database->query("select * from question_theory where estatus='1'");
																				while($rowdata=mysqli_fetch_array($seldata)){
																					  $todata1 = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and inputquestion='".$rowll2['id']."' and question_theory='".$rowdata['id']."'".$con."");
			
																					 $todayttocount1=mysqli_fetch_array($todata1);
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																		$l++;
																	}
																	?>
						  										</tbody>
						  									</table>
												</div>
												<div class="tab-pane fade" id="pills-contact" role="tabpanel"
													 aria-labelledby="pills-contact-tab">
													<table class="table table-bordered table_custom">
						  										<thead>
						  											<tr>
						  												<th rowspan="2"></th>
						  												<th rowspan="2">View</th>
						  												<th rowspan="2">Review</th>
						  												<th rowspan="2">OverAll</th>
						  												<th colspan="2">Total</th>
						  												<?php
						  												$data1 = $database->query("select * from complexity where estatus=1 ");
						  												while($data1row=mysqli_fetch_array($data1)){
						  
						  													?>
						  													<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
						  													</th>
						  												<?php } ?>
						  											</tr>
						  											<tr>
						  												<?php
						  												$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th>
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												$sell2=$database->query("select * from complexity where estatus='1'");
						  												while($rowll2=mysqli_fetch_array($sell2)){
						  													$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th >
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												}
						  												?>
						  											</tr>
						  											
						  										</thead>
						  										<tbody>
						  											<?php
																if(isset($_POST['chapter'])){
																	
																	if($_POST['chapter']!=''){
																		$l=1;
																		$sell2=$database->query("select * from topic where estatus='1' and chapter='".$_POST['chapter']."'");
																		while($rowll2=mysqli_fetch_array($sell2)){
																			$j=1;
																			$count=0;
																			$count1=0;
																			$tcount=0;
																			$sel2=$database->query("select * from complexity where estatus='1'");
																			while($rowl2=mysqli_fetch_array($sel2)){
																				$sel12=$database->query("SELECT * FROM `question_theory` ");
																				while($rowlll2=mysqli_fetch_array($sel12)){
																				
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."' and question_theory='".$rowlll2['id']."' and chapter in (".$_POST['chapter'].") and topic='".$rowll2['id']."'".$con." ");
																					
																					
																					 $todayttocount=mysqli_fetch_array($todata);
																					 if($rowlll2['id']=='1'){
																						$count=$count+$todayttocount[0];
																					 }else{
																						 $count1=$count1+$todayttocount[0];
																					 }
																					 $tcount=$tcount+$todayttocount[0];
																					
																				}
																			}
																			$selll=$database->query("select count(*) As count from review_chapters where estatus='1' and chapter = '".$_POST['chapter']."' and class='".$_REQUEST['class']."'  group by chapter");
																			$rowlll=mysqli_fetch_array($selll);
																			
																			?>
			
																			<tr>
																				<td> 
																					<?php echo $rowll2['topic']; ?>
																				</td>
																				<td class="reviewdashboard-buttons">
																					<a style="cursor:pointer;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $_POST['chapter'];?>&topic=<?php echo $rowll2['id'];?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																				</td>
																				
																				<?php
																			if($rowlll[0]>0){
																			?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process4.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $_POST['chapter'];?>&type=1')"><i class="fa fa-eye"  ></i></p>
			
																				</td>
																			<?php }else{ 
																				?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=2')"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;</p>
																				</td>
																			<?php } ?>
																				 <td>
																				 																					<p><?php echo  $tcount; ?></p>
																				 																				</td>
																				<?php if($_REQUEST['totalquestions']=='All'){
																				
																					 $todata = $database->query("select count(*) from createquestion where estatus=1  and chapter='".$_POST['chapter']."'  and topic='".$rowll2['id']."'".$con." ");
																					
																					 $todayttocount=mysqli_fetch_array($todata);
																					 $tcount= $todayttocount[0];
																				?>
																					 <td>
																						<?php echo  $tcount; ?>
																					</td>
																				<?php }else{ ?>
																					<td>
																					<?php echo  $tcount; ?>
																					</td>
																				<?php }?>
																						
																				<?php
																				$se2=$database->query("SELECT * FROM `question_theory` ");
																				while($ro2=mysqli_fetch_array($se2)){
																				
																					if($ro2['id']==1){
																				?>
																					<td>
																						<?php echo $count; ?>
																					</td>
																				<?php
																					}else if($ro2['id']==2){
																					?>
																					<td>
																						<?php echo $count1; ?>
																					</td>
																					<?php
																					}
																				}
																				
																				
																				$j=1;
																				
																				$sell12=$database->query("select * from complexity where estatus='1' ");
																				while($rowll12=mysqli_fetch_array($sell12)){
																					$se2=$database->query("SELECT * FROM `question_theory` ");
																					while($ro2=mysqli_fetch_array($se2)){
																						
																						$todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$ro2['id']."'  and topic in (".$rowll2['id'].")".$con." ");
																						
																						 $todayttocount1=mysqli_fetch_array($todata);
																						
																				?>
																					<td>
																					 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																					</td>
																				<?php
																					 }
																				}
																				?>
																				
																			</tr>
																			<?php
																			$l++;
																		}
																	}else{
																		$l=1;
																	$sele=$database->query("select * from users where username='".$_SESSION['username']."'");
																	$rowe=mysqli_fetch_array($sele);
																	$sell2=$database->query("select * from chapter where estatus='1' and id in (".$rowe['chapter'].") and subject='".$rowe['subject']."'".$class);
																	
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$count=0;
																		$count1=0;
																		$tcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$sel12=$database->query("SELECT * FROM `question_theory`");
																			while($rowlll2=mysqli_fetch_array($sel12)){
																				
																				  $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."'  and chapter in (".$rowll2['id'].") and question_theory='".$rowlll2['id']."'".$con." ");
																				
																				 $todayttocount=mysqli_fetch_array($todata);
																				if($rowlll2['id']=='1'){
																					$count=$count+$todayttocount[0];
																				 }else{
																					 $count1=$count1+$todayttocount[0];
																				 }
																				 $tcount=$tcount+$todayttocount[0];
																				
																			}
																		}
																		$selll=$database->query("select count(*) As count from review_chapters where estatus='1' and chapter = '".$rowll2['id']."' and class='".$_REQUEST['class']."' and subject='".$rowe['subject']."' group by chapter");
																		
																		$rowlll=mysqli_fetch_array($selll);
			
																		
																		?>
			
																		<tr>
																			<td>
																				<a   onClick="setState('chapoverview','<?php echo SECURE_PATH;?>reviewdashboard/process.php','attributesOverveiw=1&chapoverview=1&class=<?php echo $_REQUEST['class']; ?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>')" ><?php echo $rowll2['chapter']; ?></a>
																			</td>
																			<td class="reviewdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print" onClick='window.open("<?php echo SECURE_PATH;?>reviewdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			<?php
																			if($rowlll[0]>0){
																			?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process4.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-eye"  ></i></p>
			
																				</td>
																			<?php }else{ ?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;</p>
																				</td>
																			<?php } ?>
																			
																				<td>
																					<?php echo  $tcount; ?>
																				</td>
																			
																					
																			<?php
																			$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																				if($ro2['id']==1){
																			?>
																				<td>
																					<?php echo $count; ?>
																				</td>
																			<?php
																				}else if($ro2['id']==2){
																				?>
																				<td>
																					<?php echo $count1; ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			
																			$sell12=$database->query("select * from complexity where estatus='1' ");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and chapter in (".$rowll2['id'].")  and question_theory='".$ro2['id']."'".$con." ");
																					
																					 $todayttocount1=mysqli_fetch_array($todata);
																					
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																		$l++;
																	}
															
																	}
																	
																	?>
																
																<?php
																}else{
																?> 
															
																	<?php
																	
																	$l=1;
																	$sele=$database->query("select * from users where username='".$_SESSION['username']."'");
																	$rowe=mysqli_fetch_array($sele);
																	$sell2=$database->query("select * from chapter where estatus='1' and id in (".$rowe['chapter'].") and subject='".$rowe['subject']."'".$class);
																	
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$count=0;
																		$count1=0;
																		$tcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$sel12=$database->query("SELECT * FROM `question_theory`");
																			while($rowlll2=mysqli_fetch_array($sel12)){
																				
																				  $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."' and question_theory='".$rowlll2['id']."' and chapter in (".$rowll2['id'].")".$con." ");
																				
																				 $todayttocount=mysqli_fetch_array($todata);
																				if($rowlll2['id']=='1'){
																					$count=$count+$todayttocount[0];
																				 }else{
																					 $count1=$count1+$todayttocount[0];
																				 }
																				 $tcount=$tcount+$todayttocount[0];
																				
																			}
																		}
																		$selll=$database->query("select count(*) As count from review_chapters where estatus='1' and chapter = '".$rowll2['id']."' and class='".$_REQUEST['class']."' and subject='".$rowe['subject']."' group by chapter");
																		
																		$rowlll=mysqli_fetch_array($selll);
			
																		
																		?>
			
																		<tr>
																			<td>
																				<a   onClick="setState('adminForm','<?php echo SECURE_PATH;?>reviewdashboard/process.php','addForm=1&chapoverview=1&class=<?php echo $_REQUEST['class']; ?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>')" ><?php echo $rowll2['chapter']; ?></a>
																			</td>
																			<td class="reviewdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print" onClick='window.open("<?php echo SECURE_PATH;?>reviewdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $rowe['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			<?php
																			if($rowlll[0]>0){
																			?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process4.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-eye"  ></i></p>
			
																				</td>
																			<?php }else{ ?>
																				<td class="reviewdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>reviewdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;</p>
																				</td>
																			<?php } ?>
																			<td>
																					<?php echo  $tcount; ?>
																				</td>
																			
																					
																			<?php
																			$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																				if($ro2['id']==1){
																			?>
																				<td>
																					<?php echo $count; ?>
																				</td>
																			<?php
																				}else if($ro2['id']==2){
																				?>
																				<td>
																					<?php echo $count1; ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			
																			$sell12=$database->query("select * from complexity where estatus='1' ");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$ro2['id']."' and chapter in (".$rowll2['id'].")".$con." ");
																					
																					 $todayttocount1=mysqli_fetch_array($todata);
																					
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																		$l++;
																	}
															
																	?>
																<?php } ?>
						  										</tbody>
						  									</table>
												</div>
												
												
											<?php   ?>
										</div>
										
						  			</div>
								</div>
							</div>
						 
			</div> -->
		</div>
			
	<?php
	}
	
	?>
		