<?php
    error_reporting(0);
    include('../include/session.php');
    ini_set('display_errors','0');
    if(!$session->logged_in)
    {
		
    	?>
    	<script type="text/javascript">
			alert("User with the same username logged in to another browser");
			location.replace("<?php echo SECURE_PATH;?>admin/");
		</script>
		<?php
    }
    else
    {
    ?>
		
		<style>
         
		.note-popover.popover {
			max-width: none;
			display: none;
		}
		.note-editor {
			position: relative;
			padding: 1px;
		}
	
         </style>
		<style type="text/css">
		
		.vendor-list.table tbody td,,
		.vendor-list.table tbody th {
		/* word-break: keep-all; */
		width: 10% !important;
		}

		button:nth-of-type(1).active{
		color: #fff !important;
		background-color: #6c757d !important;
		border-color: #6c757d !important; 
		}
		.slabel{
		display:block !important
		}
		</style>
		
		
		<div class="content-wrapper">
			<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Question Review
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>
				<div class="modal fade"  id="reviewquestionmodal" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" >
				  <div class="modal-dialog modal-xl" role="document">

					<div class="modal-content">
						<div class="modal-header">
								
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body" id="viewDetails2">
								
							</div>
					</div>
				  </div>
				</div>
				
				 <div id="extraLargeModal" class="modal fade" tabindex="-1" role="dialog">
					<div class="modal-dialog modal-xl">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Reviewer Dashboard Lecturer Wise Report</h5>
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							</div>
							<div class="modal-body" id="viewdata">
								
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
								
							</div>
						</div>
					</div>
				</div>
            
           
			<section class="admindashboard">
                <div class="container-fluid p-0">
                    
					<div class="form-group mb-0" id="adminForm">
							<div class="col-sm-8">
								<script type="text/javascript">
									setStateGet('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1');
								</script>
							</div>
						</div>
                   
                </div>
            </section>
            
			<section >
                
                   
                        <div class="form-group" id="chaptertopics">
                           
                        </div>
                    
               
            </section>

            <!-- End Content-->

            <!-- Scroll to Top Button-->
           <!--  <a class="scroll-to-top rounded" href="#page-top">
                <i class="material-icons">navigation</i>
            </a> -->
        </div>	
<?php
    
    }
?>