
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['addForm'])){
		
		$data_sel =  $database->query("select * from users where username='".$session->username."'");
		$row = mysqli_fetch_array($data_sel);
		$sqll=$database->query("select count(id)  from createquestion where estatus='1' and class in (".$row['class'].") find_in_set(".$row['class'].",class)>0 and subject='".$row['subject']."' and chapter IN (".$row['chapter'].") and vstatus1='1'");
		$row1 = mysqli_fetch_array($sqll);
		if($_REQUEST['class']!=''){
			$_REQUEST['class']=$_REQUEST['class'];
		}else{
			$_REQUEST['class']="1";
		}
		if($session->userlevel=='6'){
			
			$rowee=$database->query("select * from subject where estatus='1' and id in (".$row['subject'].") order by id asc limit 0,1");
			$rowee1=mysqli_fetch_array($rowee);
			if($_REQUEST['subject']!=''){
				$_REQUEST['subject']=$_REQUEST['subject'];
			}else{
				$_REQUEST['subject']=$rowee1['id'];
			}
			$chaptercond1=" AND chapter in (".$row['chapter'].") ";
			$chaptercond2=" AND a.chapter in (".$row['chapter'].") ";
		}else{
			$chaptercond1="";
			$chaptercond2="";
		}
		if(isset($_REQUEST['subject'])){
			if($_REQUEST['subject']!=''){
				$subject= " AND subject='".$_REQUEST['subject']."'";
				$subject2= " AND id='".$_REQUEST['subject']."'";

			}else{
				$subject='';
				$subject2= "";
			}
		}else{
			$subject='';
			$subject2= "";
		}
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
		.daterangestyle1{
			background-color:#007bff9e;
		}
		.daterangestyle2{
			background-color:#ff8007a3;
		}
		.daterangestyle3{
			background-color:#6e8e8d;
		}
		</style>
	
	<script>
	$('#chapter').selectpicker1();
	 $('.chapter').selectpicker2();
	  $('#issues_list').selectpicker3();
	 </script>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<?php 
									if(isset($_REQUEST['type'])){ 
										if($_REQUEST['type']!='Custom Range'){
											$datarange4=$_REQUEST['type'];
										}else{
											$datarange4=$_REQUEST['reportrange'];
										}
									} 
									if(isset($_REQUEST['type1'])){ 
										if($_REQUEST['type1']!='Custom Range'){
											$datarange5=$_REQUEST['type1'];
										}else{
											$datarange5=$_REQUEST['reportrange1'];
										}
									} 
									?>
									<div class="row">
										<?php if($session->userlevel=='1' || $session->userlevel=='9'){ ?>
											<div class="col-lg-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Verified Date</label>
													<div class="form-control daterange p-2 col-sm-6" id="reportrange">
															<i class="far fa-calendar-alt"></i>&nbsp;
															<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type'])){ if($_REQUEST['type']!=''){echo $datarange4; } else{ echo 'Today'; } }else { echo 'Today';} ?><i class="fa fa-caret-down pl-1 pl-3"></i>
														</div>
												</div>
											</div>
										<?php }else{ ?>
											<div class="col-lg-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Verified Date</label>
													<div class="form-control daterange p-2 col-sm-6" id="reportrange">
															<i class="far fa-calendar-alt"></i>&nbsp;
															<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type'])){ if($_REQUEST['type']!=''){echo $datarange4; } else{ echo 'Yesterday'; } }else { echo 'Yesterday';} ?><i class="fa fa-caret-down pl-1 pl-3"></i>
														</div>
												</div>
											</div>
										<?php } ?>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Reviewed Date</label>
												<div class="form-control daterange p-2 col-sm-6" id="reportrange1">
														<i class="far fa-calendar-alt"></i>&nbsp;
														<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type1'])){ if($_REQUEST['type1']!=''){echo $datarange5; } else{ echo 'Days Wise'; } }else { echo 'Days Wise';} ?><i class="fa fa-caret-down pl-1 pl-3"></i>
													</div>
											</div>
										</div>
									</div>
									<?php //$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']); ?>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Subject</label>
												<div class="p-2 col-sm-6">
													
													<select class="form-control" name="subject" value=""   id="subject" onChange="search_report1();setState('aaa','<?php echo SECURE_PATH;?>reviewerdashboard1/ajax.php','getchapter=1&subject='+$('#subject').val()+'');">
														<option value=''>-- Select --</option>
														<?php
														if($session->userlevel=='6'){
															$row = $database->query("select * from subject where estatus='1'  and id in (".rtrim($row['subject'],",").") ");
														}else{
															$row = $database->query("select * from subject where estatus='1' ");
														}
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
										<?php 
										$data_sel1 =  $database->query("select * from users where username='".$session->username."'");
										$rowll1 = mysqli_fetch_array($data_sel1); 
										
										?>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-4 col-form-label">Chapter</label>
												<div class="p-2 col-sm-6" id="aaa">
													
													
													<select class="form-control selectpicker1" name="chapter" value=""  multiple data-live-search="true"  id="chapter"  onChange="search_report1()"  data-actions-box="true">
														<?php
														if($session->userlevel=='6'){
															$row = $database->query("select * from chapter where estatus='1'  and id  in (".rtrim($rowll1['chapter'],",").")  ");
														}else{
															$row = $database->query("select * from chapter where estatus='1' ".$subject." ");
														}
														
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if(in_array($data['id'], explode(",",$_REQUEST['chapter']))) { echo 'selected="selected"'; } } ?>  ><?php echo $data['chapter'];?></option>
														<?php
														}
														?>
													</select>
												</div>
											</div>
										</div>
									</div>
							</div>
							<div class="card-header py-3">
								<ul class="nav nav-pills" role="tablist">
									<li class="nav-item">
									  <a class="nav-link active" data-toggle="pill" href="#home" onclick="setStateGet('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&chapter='+$('#chapter').val()+'&subject='+$('#subject').val()+'&report=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&report1=1&reportrange=<?php echo $_REQUEST['reportrange']; ?>')">Lecturer</a>
									</li>
									<li class="nav-item">
									  <a class="nav-link " data-toggle="pill" href="#menu1" onclick="setState('ggg','<?php echo SECURE_PATH;?>reviewerdashboard1/ajax.php','lecturerdata=1&chapter='+$('#chapter').val()+'&subject='+$('#subject').val()+'&report=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&report1=1&reportrange=<?php echo $_REQUEST['reportrange']; ?>')">Chapter Wise</a>
									</li>
									
								</ul>
							</div>
							<div class="card-body">
								

							<div class="tab-content">
								<div id="home" class="container tab-pane active"><br>
									<?php
									$dateto=strtotime(date('d-m-Y'));
									$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
									//$dateyess1 = strtotime(date('d-m-Y',strtotime("-1 days"). ' 00:00:01'));
									//$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days"). ' 23:59:59'));
									$dateyess1 = strtotime('-1 days',$dateto);
									 $datetoyes1 = strtotime(date('d-m-Y',$dateyess1).'23:59:59');
									 $dateyess2 = strtotime(date('d-m-Y',strtotime("-2 days")));
									$dateyess3 = strtotime(date('d-m-Y',strtotime("-3 days")));
									$dateyess5 = strtotime(date('d-m-Y',strtotime("-5 days")));
									$dateyess7 = strtotime(date('d-m-Y',strtotime("-7 days")));
									$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
									$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
									$dateyess364= strtotime(date('d-m-Y',strtotime("-364 days")));
								if(isset($_REQUEST['report'])){
									if($_REQUEST['type']=="Today"){
										$vdate=" and vtimestamp1 between ".$dateto." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateto." and ".$dateto1." ";
									} else if($_REQUEST['type']=="Yesterday"){
										$vdate=" and vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
									}else if($_REQUEST['type']=="Last 2 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess2." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess2." and ".$dateto1." ";
									}
									else if($_REQUEST['type']=="Last 3 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
									}else if($_REQUEST['type']=="Last 5 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
									}
									else if($_REQUEST['type']=="Last 7 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
									}else if($_REQUEST['type']=="Last 15 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
									}else if($_REQUEST['type']=="Last 30 Days"){
										$vdate=" and vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
									}else if($_REQUEST['type']=="Overall"){
										if($_REQUEST['reportrange']!=''){
											$vdate=" and vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
											$vdate1=" and a.vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
										}
									}else if($_REQUEST['type']=="Custom Range"){
										if($_REQUEST['reportrange']!=''){
											$dat=explode("-",$_REQUEST['reportrange']);
											$fdat=strtotime($dat[0]);
											$tdat=strtotime($dat[1]);
											$vdate=" and vtimestamp1 between ".$fdat." and ".$tdat." ";
											$vdate1=" and a.vtimestamp1 between ".$fdat." and ".$tdat." ";
										}
										
									}else{
										if($session->userlevel=='1' || $session->userlevel=='9'){
											$fdate=date('m/d/Y', $dateto);
											$tdate=date('m/d/Y', $dateto);
											$_REQUEST['reportrange']=$fdate." - ".$tdate;
											$vdate=" and vtimestamp1 between ".$dateto." and ".$dateto1." ";
											$vdate1=" and a.vtimestamp1 between ".$dateto." and ".$dateto1." ";
										}else{
											$fdate=date('m/d/Y', $dateyess1);
											$tdate=date('m/d/Y', $dateyess1);
											$_REQUEST['reportrange']=$fdate." - ".$tdate;
											$vdate=" and vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
											$vdate1=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
										}
									}
								}else{
									if($session->userlevel=='1' || $session->userlevel=='9'){
										$fdate=date('m/d/Y', $dateto);
										$tdate=date('m/d/Y', $dateto);
										$_REQUEST['reportrange']=$fdate." - ".$tdate;
										$vdate=" and vtimestamp1 between ".$dateto." and ".$dateto1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateto." and ".$dateto1." ";
									}else{
										$fdate=date('m/d/Y', $dateyess1);
										$tdate=date('m/d/Y', $dateyess1);
										$_REQUEST['reportrange']=$fdate." - ".$tdate;
										$vdate=" and vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
										$vdate1=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
									}
								}
								if(isset($_REQUEST['report1'])){
									if($_REQUEST['type1']=="Today"){
										$rdate=" and rtimestamp between ".$dateto." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateto." and ".$dateto1." ";
									} else if($_REQUEST['type1']=="Yesterday"){
										$rdate=" and rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
									}else if($_REQUEST['type1']=="Last 2 Days"){
										$rdate=" and rtimestamp between ".$dateyess2." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess2." and ".$dateto1." ";
									}
									else if($_REQUEST['type1']=="Last 3 Days"){
										$rdate=" and rtimestamp between ".$dateyess3." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess3." and ".$dateto1." ";
									}else if($_REQUEST['type1']=="Last 5 Days"){
										$rdate=" and rtimestamp between ".$dateyess5." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess5." and ".$dateto1." ";
									}
									else if($_REQUEST['type1']=="Last 7 Days"){
										$rdate=" and rtimestamp between ".$dateyess7." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess7." and ".$dateto1." ";
									}else if($_REQUEST['type1']=="Last 15 Days"){
										$rdate=" and rtimestamp between ".$dateyess15." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess15." and ".$dateto1." ";
									}else if($_REQUEST['type1']=="Last 30 Days"){
										$rdate=" and rtimestamp between ".$dateyess30." and ".$dateto1." ";
										$rdate1=" and a.rtimestamp between ".$dateyess30." and ".$dateto1." ";
									}else if($_REQUEST['type']=="Overall"){
										if($_REQUEST['reportrange1']!=''){
											$rdate=" and rtimestamp between ".$dateyess364." and ".$dateto1." ";
											$rdate1=" and a.rtimestamp between ".$dateyess364." and ".$dateto1." ";
										}
									}else if($_REQUEST['type1']=="Custom Range"){
										
										if($_REQUEST['reportrange1']!=''){
											$dat=explode("-",$_REQUEST['reportrange1']);
											$fdat=strtotime($dat[0]);
											$tdat=strtotime($dat[1]);
											$rdate=" and rtimestamp between ".$fdat." and ".$tdat." ";
											$rdate1=" and a.rtimestamp between ".$fdat." and ".$tdat." ";
										}
										
									}else if($_REQUEST['type1']==""){
										$rdate="";
										$rdate1="";
										
									}else{
										$rdate="";
										$rdate1="";
									}
								}else{
									$rdate="";
									$rdate1="";
								}
								
								if(isset($_REQUEST['chapter'])){
										if($_REQUEST['chapter']!=""){
											$chapter=" AND id in (".$_REQUEST['chapter'].")";
											$chapter1=" AND a.id in (".$_REQUEST['chapter'].")";
											$chapter2=" AND chapter in (".$_REQUEST['chapter'].")";
											$chapter4=" AND a.chapter in (".$_REQUEST['chapter'].")";
										} else {
											$chapter="";
											$chapter1="";
											$chapter2="";
											$chapter4="";
										}
									}else{
										$chapter="";
										$chapter1="";
										$chapter2="";
											$chapter4="";
									}

									if(isset($_REQUEST['subject'])){
										if($_REQUEST['subject']!=""){
											$subject=" AND subject='".$_REQUEST['subject']."'";
											$subject1=" AND a.subject='".$_REQUEST['subject']."'";
										} else {
											$subject="";
											$subject1="";
										}
									}else{
										$subject="";
										$subject1="";
									}
								
									if($session->userlevel=='1' || $session->userlevel=='9') {
										if(isset($_REQUEST['type3'])){ 
											if($_REQUEST['type3']!='Custom Range'){
												$datarange3=$_REQUEST['type3'];
											}else{
												$datarange3=$_REQUEST['reportrange3'];
											}
										} 
										if(isset($_REQUEST['type4'])){ 
											if($_REQUEST['type4']!='Custom Range'){
												$datarange4=$_REQUEST['type4'];
											}else{
												$datarange4=$_REQUEST['reportrange4'];
											}
										} 
										if(isset($_REQUEST['report3'])){
											if($_REQUEST['type3']=="Today"){
												$vdate2=" and vtimestamp1 between ".$dateto." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateto." and ".$dateto1." ";
											} else if($_REQUEST['type3']=="Yesterday"){
												$vdate2=" and vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess1." and ".$datetoyes1." ";
											}else if($_REQUEST['type3']=="Last 2 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess2." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess2." and ".$dateto1." ";
											}
											else if($_REQUEST['type3']=="Last 3 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess3." and ".$dateto1." ";
											}else if($_REQUEST['type3']=="Last 5 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess5." and ".$dateto1." ";
											}
											else if($_REQUEST['type3']=="Last 7 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess7." and ".$dateto1." ";
											}else if($_REQUEST['type3']=="Last 15 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess15." and ".$dateto1." ";
											}else if($_REQUEST['type3']=="Last 30 Days"){
												$vdate2=" and vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess30." and ".$dateto1." ";
											}else if($_REQUEST['type3']=="Overall"){
												if($_REQUEST['reportrange3']!=''){
													$vdate2=" and vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
													$vdate3=" and a.vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
												}
											}else if($_REQUEST['type3']=="Custom Range"){
												if($_REQUEST['reportrange3']!=''){
													$dat=explode("-",$_REQUEST['reportrange3']);
													$fdat=strtotime($dat[0]);
													$tdat=strtotime($dat[1]);
													$vdate2=" and vtimestamp1 between ".$fdat." and ".$tdat." ";
													$vdate3=" and a.vtimestamp1 between ".$fdat." and ".$tdat." ";
												}
												
											}else{
												$fdate=date('m/d/Y', $dateyess364);
												$tdate=date('m/d/Y', $dateto1);
												$_REQUEST['reportrange3']=$fdate." - ".$tdate;
												$vdate2=" and vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
												$vdate3=" and a.vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
											}
										}else{
											$fdate=date('m/d/Y', $dateyess364);
											$tdate=date('m/d/Y', $dateto1);
											$_REQUEST['reportrange3']=$fdate." - ".$tdate;
											$vdate2=" and vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
											$vdate3=" and a.vtimestamp1 between ".$dateyess364." and ".$dateto1." ";
										}
										if(isset($_REQUEST['report4'])){
											if($_REQUEST['type4']=="Today"){
												$rdate3=" and rtimestamp between ".$dateto." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateto." and ".$dateto1." ";
											} else if($_REQUEST['type4']=="Yesterday"){
												$rdate3=" and rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess1." and ".$datetoyes1." ";
											}else if($_REQUEST['type4']=="Last 2 Days"){
												$rdate3=" and rtimestamp between ".$dateyess2." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess2." and ".$dateto1." ";
											}
											else if($_REQUEST['type4']=="Last 3 Days"){
												$rdate3=" and rtimestamp between ".$dateyess3." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess3." and ".$dateto1." ";
											}else if($_REQUEST['type4']=="Last 5 Days"){
												$rdate3=" and rtimestamp between ".$dateyess5." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess5." and ".$dateto1." ";
											}
											else if($_REQUEST['type4']=="Last 7 Days"){
												$rdate3=" and rtimestamp between ".$dateyess7." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess7." and ".$dateto1." ";
											}else if($_REQUEST['type4']=="Last 15 Days"){
												$rdate3=" and rtimestamp between ".$dateyess15." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess15." and ".$dateto1." ";
												
											}else if($_REQUEST['type4']=="Last 30 Days"){
												$rdate3=" and rtimestamp between ".$dateyess30." and ".$dateto1." ";
												$rdate4=" and a.rtimestamp between ".$dateyess30." and ".$dateto1." ";
											}else if($_REQUEST['type4']=="Overall"){
												if($_REQUEST['reportrange4']!=''){
													$rdate3=" and rtimestamp between ".$dateyess364." and ".$dateto1." ";
													$rdate4=" and a.rtimestamp between ".$dateyess364." and ".$dateto1." ";
												}
											}else if($_REQUEST['type4']=="Custom Range"){
												
												if($_REQUEST['reportrange4']!=''){
													$dat=explode("-",$_REQUEST['reportrange4']);
													$fdat=strtotime($dat[0]);
													$tdat=strtotime($dat[1]);
													$rdate3=" and rtimestamp between ".$fdat." and ".$tdat." ";
													$rdate4=" and a.rtimestamp between ".$fdat." and ".$tdat." ";
												}
												
											}else if($_REQUEST['type4']==""){
												$rdate3="";
												$rdate4="";
												
											}else{
												$rdate3="";
												$rdate4="";
											}
										}else{
											$rdate3="";
											$rdate4="";
										}
										if(isset($_REQUEST['typelec_chapter'])){
											if($_REQUEST['typelec_chapter']!=''){
												$_REQUEST['typelec_chapter']=$_REQUEST['typelec_chapter'];
											}else{
												$_REQUEST['typelec_chapter']='lecturer_cumulative';
											}
										}else{
											$_REQUEST['typelec_chapter']='lecturer_cumulative';
										}
									?>
										<script type="text/javascript">


									 $('#dataTable1').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									  
									</script>
										<div class="row">
											
											<div class="col-lg-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Verified Date2</label>
													<div class="form-control daterange p-2 col-sm-6" id="reportrange3">
															<i class="far fa-calendar-alt"></i>
															<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type3'])){ if($_REQUEST['type3']!=''){echo $datarange3; } else{ echo 'Overall'; } }else { echo 'Overall';} ?><i class="fa fa-caret-down pl-3"></i>
														</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group row">
													<label class="col-sm-4 col-form-label">Reviewed Date2</label>
													<div class="form-control daterange p-2 col-sm-6" id="reportrange4">
															<i class="far fa-calendar-alt"></i>
															<span class="datesuperadmin pl-2"><?php if(isset($_REQUEST['type4'])){ if($_REQUEST['type4']!=''){echo $datarange4; } else{ echo 'Days Wise'; } }else { echo 'Days Wise';} ?><i class="fa fa-caret-down pl-3"></i>
														</div>
												</div>
											</div>
										</div>
										<div class="row">
												<div class="col-lg-6">
													<div class="form-group row">
														<label class="col-sm-4 col-form-label">Type</label>
															
															<div class="pl-1 col-sm-6">
																<select class="form-control" name="typelec_chapter" value=""  id="typelec_chapter"  onChange="search_report1()" >
																	<option value=''>-- Select --</option>
																	<option value="lecturer_cumulative" <?php if(isset($_REQUEST['typelec_chapter'])){ if($_REQUEST['typelec_chapter'] =='lecturer_cumulative'){ echo ' selected="selected"';}}else{ echo ' selected="selected"';}?>  >Lecturer Cumulative</option>
																	<option value="chapter" <?php if(isset($_REQUEST['typelec_chapter'])){ if($_REQUEST['typelec_chapter'] =='chapter'){ echo ' selected="selected"';};}?>   >Chapter</option>
																</select>
															</div>
														
													</div>
												</div>
												
												<?php  
													if(isset($_REQUEST['issues_list'])){
														if($_REQUEST['issues_list']!=''){
															$_REQUEST['issues_list']=$_REQUEST['issues_list'];
															$val1=explode(",",$_REQUEST['issues_list']);

															$colspan=4+count($val1);
															
														}else{
															$colspan="4";
															$_REQUEST['issues_list']='';
														}
													}else{
														$colspan="12";
														$_REQUEST['issues_list']='complexity,usageset,inputquestion,questiontheory,class,exam,chapter,topic';
													}
													
													$issues = array('complexity' =>'Complexity','timeduration' =>'Time Duration','usageset'=> 'Usageset','inputquestion' => 'Question Type','questiontheory' => 'Question Theory','usageset' => 'Usageset','class' => 'Class','exam' => 'Exam','chapter' => 'Chapter','topic' => 'Topic'); 
												?>
												<div class="col-lg-6">
													<div class="form-group row">
														<label class="col-sm-4 col-form-label">Issues List</label>
														
															<div class="pl-1 col-sm-6">
																<select class="form-control selectpicker3" name="issues_list"  multiple data-live-search="true" value=""  id="issues_list"   onChange="search_report1()"  >
																	<?php
																	 foreach($issues as $key => $issuevalue){ ?>
																		<option value='<?php echo $key; ?>' <?php if(isset($_REQUEST['issues_list'])) { if(in_array($key, explode(",",$_REQUEST['issues_list']))) { echo 'selected="selected"'; } }else { } ?>><?php echo $issuevalue; ?></option>
																	<?php } ?>
																	
																	
																</select>
															</div>
														
													</div>
												</div>

											</div>
											
											<script>
													issuefunction();
													function issuefunction(){
												
														var vala = $('#issues_list').val()+ '';
														var data=vala.split(',');
														if($.inArray("complexity", data) !== -1 ) {
															$('.complexity').show();
														}else{
															$('.complexity').hide();
														}
														if($.inArray("timeduration", data) !== -1 ) {
															$('.timeduration').show();
														}else{
															$('.timeduration').hide();
														}
														if($.inArray("usageset", data) !== -1 ) {
															$('.usageset').show();
														}else{
															$('.usageset').hide();
														}
														if( $.inArray("inputquestion", data) !== -1 ) {
															$('.inputquestion').show();
														}else{
															$('.inputquestion').hide();
														}
														if( $.inArray("questiontheory", data) !== -1 ) {
															$('.questiontheory').show();
														}else{
															$('.questiontheory').hide();
														}
														if( $.inArray("class", data) !== -1 ) {
															$('.class').show();
														}else{
															$('.class').hide();
														}
														if( $.inArray("exam", data) !== -1 ) {
															$('.exam').show();
														}else{
															$('.exam').hide();
														}
														if( $.inArray("chapter", data) !== -1 ) {
															$('.chapter').show();
														}else{
															$('.chapter').hide();
														}
														if( $.inArray("topic", data) !== -1 ) {
															$('.topic').show();
														}else{
															$('.topic').hide();
														}

														
														
													};

												</script>
												
											<!-- <div class="card border-0 pt-3">
												<div class="card-body table-responsive table_data p-0">
													<table class=" table table-bordered  dashboard-table mb-0" >
														<thead class="">
															
															<tr>
																<th scope="col"></th>
																<th scope="col" colspan="3">Total</th>
																<th scope="col" colspan="3">Botony</th>
																<th scope="col" colspan="3">Physics</th>
																<th scope="col" colspan="3">Chemistry</th>
																<th scope="col" colspan="3">Mathematics</th>
																<th scope="col" colspan="3">Zoology</th>
																
															</tr>
															<tr>
																<th scope="col" ></th>
																<th scope="col">Verified</th>
																<th scope="col">Reviewed</th>
																<th scope="col">Errors</th>
																<?php
																$sel=$database->query("select * from subject where estatus='1'");
																while($row=mysqli_fetch_array($sel)){
																	?>
																		<th scope="col">Verified</th>
																		<th scope="col">Reviewed</th>
																		<th scope="col">Errors</th>
																<?php
																}
																?>
																
																
																
																
															</tr>
														</thead>
														<tbody>
															<?php
																 $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
																$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
																$dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
																$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
																$date3days = strtotime(date('d-m-Y',strtotime("-3 days")));
																$date7days = strtotime(date('d-m-Y',strtotime("-7 days")));
																$date15days = strtotime(date('d-m-Y',strtotime("-15 days")));
																$date30days = strtotime(date('d-m-Y',strtotime("-30 days")));
																$date45days = strtotime(date('d-m-Y',strtotime("-45 days")));
																$date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
																$today=" and vtimestamp1 between ".$dateto." and ".$dateto1."";
																$yesterday=" and vtimestamp1 between ".$dateyes." and ".$datetoyes1."";
																$overall=" and vtimestamp1 between ".$date364days." and ".$dateto1."";
																 $daysrange = array($today =>'Today',$yesterday => 'Yesterday',$overall => 'Overall');
																  foreach($daysrange as $key => $days){
																		
																		$sqllt=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  ");
																		$rowlt=mysqli_fetch_array($sqllt);

																		$sqll1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' ".$key." ");
																		$rowl1=mysqli_fetch_array($sqll1);

																		
																		$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1'  ");
																		$rowlvt1=mysqli_fetch_array($sqllvt1);

																		$sqllv1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' ".$key." ");
																		$rowlv1=mysqli_fetch_array($sqllv1);

																		
																  		if($days=='Today'){
																			$style="style='background-color:#8972bb9e;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.vtimestamp1 between ".$dateto." and ".$dateto1." ");
																		}else if($days=='Yesterday'){
																			$style="style='background-color:#ffa0009e;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.vtimestamp1 between ".$dateyes." and ".$datetoyes1."");
																		}else if($days=='Overall'){
																			$style="style='background-color:#007bff75;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.vtimestamp1 between ".$date364days." and ".$dateto1."");
																		}
																		
																		$rowv1=mysqli_fetch_array($sqlv1);
																		$s=$rowv1['cnt'];
																			
																		
																		//$sqlv11=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  ");

																		//$ss=$rowv11['cnt'];
																			

																		$percentpt = $rowl1[0]/$rowlt[0];
																		if(is_nan($percentpt))
																			$percentpt=0;
																		else
																			$percentpt=$percentpt;
																		$percent_t= number_format( $percentpt * 100) . '%';


																		$percentptvt = $rowlv1[0]/$rowlt[0];
																		if(is_nan($percentptvt))
																			$percentptvt=0;
																		else
																			$percentptvt=$percentptvt;
																		$percent_ttv= number_format( $percentptvt * 100) . '%';
																		$percent_s = $s/$rowlv1[0];
																		if(is_nan($percent_s))
																			$percent_s=0;
																		else
																			$percent_s=$percent_s;
																		$percent_s2= number_format( $percent_s * 100) . '%';
																	?>
																	 <tr>
																		<th rowspan="2" <?php echo $style; ?> >
																			<h6 class="text-white" ><?php echo $days; ?></h6>
																		</th>
																		<td <?php echo $style; ?> class="text-white"><?php if($rowl1['cnt']!=''){ echo $rowl1['cnt']; } else{ echo '0'; } ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php if($rowlv1['cnt']!=''){ echo $rowlv1['cnt']; } else{ echo '0'; } ?></td>
																		<td <?php echo $style; ?> class="text-white" ><?php echo $s; ?></td>
																		<?php
																		$sel=$database->query("select * from subject where estatus='1'");
																		while($row=mysqli_fetch_array($sel)){
																			$sqllta=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and subject='".$row['id']."'  ");
																			$rowlta=mysqli_fetch_array($sqllta);

																			$sqlla1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' ".$key." ");
																			$rowla1=mysqli_fetch_array($sqlla1);

																		
																			$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1'  ");
																			$rowlvt1=mysqli_fetch_array($sqllvt1);

																			$sqllv1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1' ".$key." ");
																			$rowlv1=mysqli_fetch_array($sqllv1);

																		
																			if($days=='Today'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.vtimestamp1 between ".$dateto." and ".$dateto1." ");
																			}else if($days=='Yesterday'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."' and a.vtimestamp1 between ".$dateyes." and ".$datetoyes1."");
																			}else if($days=='Overall'){
																				$sqlv1=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."'and a.vtimestamp1 between ".$date364days." and ".$dateto1."");
																			}
																		
																				$rowv1=mysqli_fetch_array($sqlv1);
																				$s1=$rowv1['cnt'];
																				
																				//$sqlv11=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1'   and b.version='2'  ");

																				//$rowv11=mysqli_fetch_array($sqlv11);
																					//$ss1=$rowv11['cnt'];
																					

																			/*	$percentpt = $rowl1['cnt']/$rowlt['cnt'];
																				if(is_nan($percentpt))
																					$percentpt=0;
																				else
																					$percentpt=$percentpt;
																				$percent_t= number_format( $percentpt * 100) . '%';


																				$percentptv = $rowlv1['cnt']/$rowlt['cnt'];
																				if(is_nan($percentptv))
																					$percentptv=0;
																				else
																					$percentptv=$percentptv;
																				$percent_tva= number_format( $percentptv * 100) . '%';
																				$percentpss = $s/$ss;
																				if(is_nan($percentpss))
																					$percentpss=0;
																				else
																					$percentpss=$percentpss;
																				$percent_ss= number_format( $percentpss * 100) . '%';*/
																			?>
																				<td <?php echo $style; ?> class="text-white"><?php echo $rowla1['cnt']; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $rowlv1['cnt']; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $s1; ?></td>
																		<?php
																		}
																		?>
																	</tr>
																	<tr>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_t; ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_ttv; ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_s2; ?></td>
																		<?php
																		$sel=$database->query("select * from subject where estatus='1'");
																		while($row=mysqli_fetch_array($sel)){
																			
																			$sqllta=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and subject='".$row['id']."'  ");
																			$rowlta=mysqli_fetch_array($sqllta);

																			$sqlla1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' ".$key." ");
																			$rowla1=mysqli_fetch_array($sqlla1);

																		
																			$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1'  ");
																			$rowlvt1=mysqli_fetch_array($sqllvt1);

																			$sqllva1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1' ".$key." ");
																			$rowlva1=mysqli_fetch_array($sqllva1);

																		
																			if($days=='Today'){
																				
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.vtimestamp1 between ".$dateto." and ".$dateto1." ");
																			}else if($days=='Yesterday'){
																				$sqlv1=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.vtimestamp1 between ".$datetoyes." and ".$datetoyes1."");
																			}else if($days=='Overall'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."' and a.vtimestamp1 between ".$date364days." and ".$dateto1."");
																			}
																		
																				$rowv1=mysqli_fetch_array($sqlv1);
																				$s11=$rowv1['cnt'];
																					
																				
																				$sqlv11=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and  b.changes_count!=0 and  b.message='1'   and b.version='2'  ");

																				$rowv11=mysqli_fetch_array($sqlv11);
																					$ss11=$rowv11['changes'];
																					
																				

																				$percentpta = $rowla1[0]/$rowlta[0];
																				if(is_nan($percentpta))
																					$percentpta=0;
																				else
																					$percentpta=$percentpta;
																				$percent_ta= number_format( $percentpta * 100) . '%';


																				$percentptva = $rowlva1[0]/$rowlta[0];
																				if(is_nan($percentptva))
																					$percentptva=0;
																				else
																					$percentptva=$percentptva;
																				$percent_tva= number_format( $percentptva * 100) . '%';
																				$percentpssa = $s11/$rowlva1[0];
																				if(is_nan($percentpssa))
																					$percentpssa=0;
																				else
																					$percentpssa=$percentpssa;
																				$percent_ssa= number_format( $percentpssa * 100) . '%';
																			?>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_ta; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_tva; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_ssa; ?></td>
																		<?php
																		}
																		?>
																	</tr>
																	
																<?php } ?>
														</tbody>
														
													</table>
														

												</div>
											</div> -->
											<div class="card border-0 pt-3">
												<div class="card-body table-responsive table_data p-0">
													<table class=" table table-bordered  dashboard-table mb-0" >
														<thead class="">
															
															<tr>
																<th scope="col"></th>
																<th scope="col" colspan="3">Total</th>
																<th scope="col" colspan="3">Botony</th>
																<th scope="col" colspan="3">Physics</th>
																<th scope="col" colspan="3">Chemistry</th>
																<th scope="col" colspan="3">Mathematics</th>
																<th scope="col" colspan="3">Zoology</th>
																
															</tr>
															<tr>
																<th scope="col" ></th>
																<th scope="col">Verified</th>
																<th scope="col">Reviewed</th>
																<th scope="col">Errors</th>
																<?php
																$sel=$database->query("select * from subject where estatus='1'");
																while($row=mysqli_fetch_array($sel)){
																	?>
																		<th scope="col">Verified</th>
																		<th scope="col">Reviewed</th>
																		<th scope="col">Errors</th>
																<?php
																}
																?>
																
																
																
																
															</tr>
														</thead>
														<tbody>
															<?php
																 $dateto = strtotime(date('d-m-Y'). ' 00:00:01');
																$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
																$dateyes =strtotime(date('d-m-Y',strtotime("-1 days")));
																$datetoyes1 = strtotime(date('d-m-Y',strtotime("-1 days")). ' 23:59:59');
																$date3days = strtotime(date('d-m-Y',strtotime("-3 days")));
																$date7days = strtotime(date('d-m-Y',strtotime("-7 days")));
																$date15days = strtotime(date('d-m-Y',strtotime("-15 days")));
																$date30days = strtotime(date('d-m-Y',strtotime("-30 days")));
																$date45days = strtotime(date('d-m-Y',strtotime("-45 days")));
																$date364days = strtotime(date('d-m-Y',strtotime("-364 days")));
																$today=" and vtimestamp1 between ".$dateto." and ".$dateto1."";
																$yesterday=" and vtimestamp1 between ".$dateyes." and ".$datetoyes1."";
																$overall=" and vtimestamp1 between ".$date364days." and ".$dateto1."";
																 $daysrange = array($today =>'Today',$yesterday => 'Yesterday',$overall => 'Overall');
																  foreach($daysrange as $key => $days){
																	  if($days=='Today'){
																			$key1=" and rtimestamp between ".$dateto." and ".$dateto1."";
																	  }else if($days=='Yesterday'){
																			$key1=" and rtimestamp between ".$dateyes." and ".$datetoyes1."";
																		}else if($days=='Overall'){
																		   $key1=" and rtimestamp between ".$date364days." and ".$dateto1."";
																	   }
																		$sqllt=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  ");
																		$rowlt=mysqli_fetch_array($sqllt);

																		$sqll1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' ".$key." ");
																		$rowl1=mysqli_fetch_array($sqll1);

																		
																		$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1'  ".$key1." ");
																		$rowlvt1=mysqli_fetch_array($sqllvt1);

																		$sqllv1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' ".$key1." ");
																		$rowlv1=mysqli_fetch_array($sqllv1);

																		
																  		if($days=='Today'){
																			$style="style='background-color:#8972bb9e;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.rtimestamp between ".$dateto." and ".$dateto1." ");
																		}else if($days=='Yesterday'){
																			$style="style='background-color:#ffa0009e;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.rtimestamp between ".$dateyes." and ".$datetoyes1."");
																		}else if($days=='Overall'){
																			$style="style='background-color:#007bff75;'";
																			$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.rtimestamp between ".$date364days." and ".$dateto1."");
																		}
																		
																		$rowv1=mysqli_fetch_array($sqlv1);
																		$s=$rowv1['cnt'];
																			
																		
																		//$sqlv11=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  ");

																		//$ss=$rowv11['cnt'];
																			

																		$percentpt = $rowl1[0]/$rowlt[0];
																		if(is_nan($percentpt))
																			$percentpt=0;
																		else
																			$percentpt=$percentpt;
																		$percent_t= number_format( $percentpt * 100) . '%';


																		$percentptvt = $rowlv1[0]/$rowlt[0];
																		if(is_nan($percentptvt))
																			$percentptvt=0;
																		else
																			$percentptvt=$percentptvt;
																		$percent_ttv= number_format( $percentptvt * 100) . '%';
																		$percent_s = $s/$rowlv1[0];
																		if(is_nan($percent_s))
																			$percent_s=0;
																		else
																			$percent_s=$percent_s;
																		$percent_s2= number_format( $percent_s * 100) . '%';
																	?>
																	 <tr>
																		<th rowspan="2" <?php echo $style; ?> >
																			<h6 class="text-white" ><?php echo $days; ?></h6>
																		</th>
																		<td <?php echo $style; ?> class="text-white"><?php if($rowl1['cnt']!=''){ echo $rowl1['cnt']; } else{ echo '0'; } ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php if($rowlv1['cnt']!=''){ echo $rowlv1['cnt']; } else{ echo '0'; } ?></td>
																		<td <?php echo $style; ?> class="text-white" ><?php echo $s; ?></td>
																		<?php
																		$sel=$database->query("select * from subject where estatus='1'");
																		while($row=mysqli_fetch_array($sel)){
																			$sqllta=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and subject='".$row['id']."'  ");
																			$rowlta=mysqli_fetch_array($sqllta);

																			$sqlla1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' ".$key." ");
																			$rowla1=mysqli_fetch_array($sqlla1);

																		
																			$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1'  ");
																			$rowlvt1=mysqli_fetch_array($sqllvt1);

																			$sqllv1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1' ".$key1." ");
																			$rowlv1=mysqli_fetch_array($sqllv1);

																		
																			if($days=='Today'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.rtimestamp between ".$dateto." and ".$dateto1." ");
																			}else if($days=='Yesterday'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."' and a.rtimestamp between ".$dateyes." and ".$datetoyes1."");
																			}else if($days=='Overall'){
																				$sqlv1=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."'and a.rtimestamp between ".$date364days." and ".$dateto1."");
																			}
																		
																				$rowv1=mysqli_fetch_array($sqlv1);
																				$s1=$rowv1['cnt'];
																				
																				//$sqlv11=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1'   and b.version='2'  ");

																				//$rowv11=mysqli_fetch_array($sqlv11);
																					//$ss1=$rowv11['cnt'];
																					

																			/*	$percentpt = $rowl1['cnt']/$rowlt['cnt'];
																				if(is_nan($percentpt))
																					$percentpt=0;
																				else
																					$percentpt=$percentpt;
																				$percent_t= number_format( $percentpt * 100) . '%';


																				$percentptv = $rowlv1['cnt']/$rowlt['cnt'];
																				if(is_nan($percentptv))
																					$percentptv=0;
																				else
																					$percentptv=$percentptv;
																				$percent_tva= number_format( $percentptv * 100) . '%';
																				$percentpss = $s/$ss;
																				if(is_nan($percentpss))
																					$percentpss=0;
																				else
																					$percentpss=$percentpss;
																				$percent_ss= number_format( $percentpss * 100) . '%';*/
																			?>
																				<td <?php echo $style; ?> class="text-white"><?php echo $rowla1['cnt']; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $rowlv1['cnt']; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $s1; ?></td>
																		<?php
																		}
																		?>
																	</tr>
																	<tr>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_t; ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_ttv; ?></td>
																		<td <?php echo $style; ?> class="text-white"><?php echo $percent_s2; ?></td>
																		<?php
																		$sel=$database->query("select * from subject where estatus='1'");
																		while($row=mysqli_fetch_array($sel)){
																			
																			$sqllta=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and subject='".$row['id']."'  ");
																			$rowlta=mysqli_fetch_array($sqllta);

																			$sqlla1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' ".$key." ");
																			$rowla1=mysqli_fetch_array($sqlla1);

																		
																			$sqllvt1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1'  ");
																			$rowlvt1=mysqli_fetch_array($sqllvt1);

																			$sqllva1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and subject='".$row['id']."' and review_status='1' ".$key1." ");
																			$rowlva1=mysqli_fetch_array($sqllva1);

																		
																			if($days=='Today'){
																				
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.rtimestamp between ".$dateto." and ".$dateto1." ");
																			}else if($days=='Yesterday'){
																				$sqlv1=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.changes_count!=0 and b.message='1' and b.version='2' and a.subject='".$row['id']."' and a.rtimestamp between ".$datetoyes." and ".$datetoyes1."");
																			}else if($days=='Overall'){
																				$sqlv1=$database->query("select  count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and b.changes_count!=0 and b.message='1' and b.version='2'  and a.subject='".$row['id']."' and a.rtimestamp between ".$date364days." and ".$dateto1."");
																			}
																		
																				$rowv1=mysqli_fetch_array($sqlv1);
																				$s11=$rowv1['cnt'];
																					
																				
																				$sqlv11=$database->query("select count(b.changes_count) as cnt from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id and  b.changes_count!=0 and  b.message='1'   and b.version='2'  ");

																				$rowv11=mysqli_fetch_array($sqlv11);
																					$ss11=$rowv11['changes'];
																					
																				

																				$percentpta = $rowla1[0]/$rowlta[0];
																				if(is_nan($percentpta))
																					$percentpta=0;
																				else
																					$percentpta=$percentpta;
																				$percent_ta= number_format( $percentpta * 100) . '%';


																				$percentptva = $rowlva1[0]/$rowlta[0];
																				if(is_nan($percentptva))
																					$percentptva=0;
																				else
																					$percentptva=$percentptva;
																				$percent_tva= number_format( $percentptva * 100) . '%';
																				$percentpssa = $s11/$rowlva1[0];
																				if(is_nan($percentpssa))
																					$percentpssa=0;
																				else
																					$percentpssa=$percentpssa;
																				$percent_ssa= number_format( $percentpssa * 100) . '%';
																			?>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_ta; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_tva; ?></td>
																				<td <?php echo $style; ?> class="text-white"><?php echo $percent_ssa; ?></td>
																		<?php
																		}
																		?>
																	</tr>
																	
																<?php } ?>
														</tbody>
														
													</table>
														

												</div>
											</div>
											<div class="d-flex justify-content-between pt-2">
													<a class=""   title="Print"  data-toggle="modal" data-target="#extraLargeModal" onClick="setStateGet('viewdata','<?php echo SECURE_PATH;?>reviewerdashboard1/tableview.php','viewDetails=1&subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&typelec_chapter=<?php echo $_REQUEST['typelec_chapter']; ?>&issues_list=<?php echo $_REQUEST['issues_list']; ?>')" style="cursor:pointer;padding-left:850px;padding-top:5px;color:blue;"><u>Full View</u></a>
													
													<a href="<?php echo SECURE_PATH;?>reviewerdashboard1/excel_download.php?subject=<?php echo $_REQUEST['subject']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&typelec_chapter=<?php echo $_REQUEST['typelec_chapter']; ?>&issues_list=<?php echo $_REQUEST['issues_list']; ?>" style="float:right;color:red" target="_blank" class="" title="Excel Export" ><i class="fa fa-file-excel-o" style="font-size: 25px;"></i></a>
												</div>
											<div class="card border-0 pt-3">
												<div class="card-body table-responsive table_data p-0">
													<table class=" table table-bordered  dashboard-table mb-0" id="dataTable1">
														<thead class="">
															<tr>
																<th scope="col" rowspan='2' class="daterangestyle3 text-white">Lecturer</th>
																<th scope="col" rowspan='2' class="daterangestyle3 text-white" >Subject</th>
																<th scope="col" rowspan='2' class="daterangestyle3 text-white">Chapter</th>
																 <th scope="col" rowspan='2' class="daterangestyle3 text-white">Reviewer Name</th>
																
																<th scope="col" rowspan='1' colspan="<?php echo $colspan; ?>" class="sorting daterangestyle1  text-center text-white">Daterange-1 <?php if(isset($_REQUEST['reportrange'])){  if($_REQUEST['reportrange']){ echo  " (".$_REQUEST['reportrange'].")"; } }else {echo  $_REQUEST['reportrange']; } ?></th>
																<th scope="col" rowspan='1'  colspan="<?php echo $colspan; ?>" class="sorting daterangestyle2 text-center text-white">Daterange-2 <?php if(isset($_REQUEST['reportrange3'])){  if($_REQUEST['reportrange3']){ echo  " (".$_REQUEST['reportrange3'].")"; } }else {echo  $_REQUEST['reportrange3']; } ?> </th>
																
															</tr>
															<tr>
																<th scope="col" class="sorting daterangestyle1 text-white">Total Verified</th>
																<th scope="col" class="sorting daterangestyle1 text-white">Total Reviewed</th>
																<th scope="col" class="sorting daterangestyle1 text-white">Total Issue Questions</th>
																<th scope="col" class="sorting daterangestyle1 text-white">Error (%)</th>
																<th scope="col" class="complexity sorting daterangestyle1 text-white">Complexity</th>
																<th scope="col" class="timeduration daterangestyle1 text-white">Timeduration</th>
																<th scope="col" class="usageset daterangestyle1 text-white">Usageset</th>
																<th scope="col" class="inputquestion sorting daterangestyle1 text-white">Question Type</th>
																<th scope="col" class="questiontheory sorting daterangestyle1 text-white">Question Theory</th>
																<th scope="col" class="class sorting daterangestyle1 text-white">Class</th>
																<th scope="col" class="exam sorting daterangestyle1 text-white">Exam</th>
																<th scope="col" class="chapter sorting daterangestyle1 text-white">Chapter</th>
																<th scope="col" class="topic sorting daterangestyle1 text-white">Topic</th>
																<th scope="col" class="sorting daterangestyle2 text-white">Total Verified</th>
																<th scope="col" class="sorting daterangestyle2 text-white">Total Reviewed</th>
																<th scope="col" class="sorting daterangestyle2 text-white">Total Issue Questions</th>
																<th scope="col" class="sorting daterangestyle2 text-white">Error (%)</th>
																<th scope="col" class="complexity sorting daterangestyle2 text-white">Complexity</th>
																<th scope="col" class="timeduration daterangestyle2 text-white">Timeduration</th>
																<th scope="col" class="usageset sorting daterangestyle2 text-white">Usageset</th>
																<th scope="col" class="inputquestion sorting daterangestyle2 text-white">Question Type</th>
																<th scope="col" class="questiontheory sorting daterangestyle2 text-white">Question Theory</th>
																<th scope="col" class="class sorting daterangestyle2 text-white">Class</th>
																<th scope="col" class="exam sorting daterangestyle2 text-white">Exam</th>
																<th scope="col" class="chapter sorting daterangestyle2 text-white">Chapter</th>
																<th scope="col" class="topic sorting daterangestyle2 text-white">Topic</th>
																
															</tr>
														</thead>
														<tbody>
															<?php
															if($_REQUEST['typelec_chapter']=='lecturer_cumulative'){
																	$sql=$database->query("select username,subject,chapter from users where valid='1' and userlevel='3'  ");
																	while($row=mysqli_fetch_array($sql)){
																		if(isset($_REQUEST['subject'])){
																			if($_REQUEST['subject']!=""){
																				$subject=" AND subject='".$_REQUEST['subject']."'";
																				$subject1=" AND a.subject='".$_REQUEST['subject']."'";
																			} else {
																				$subject="";
																				$subject1="";
																			}
																		}else{
																			$subject="";
																			$subject1="";
																		}

																		if(isset($_REQUEST['chapter'])){
																			if($_REQUEST['chapter']!=""){
																				$chapter=" AND chapter IN (".$_REQUEST['chapter'].")";
																				$chapter1=" AND a.chapter IN (".$_REQUEST['chapter'].")";
																				$chapter2=" AND id IN (".$_REQUEST['chapter'].")";
																			} else {
																				$chapter="";
																				$chapter1="";
																				$chapter2="";
																			}
																		}else{
																			$chapter="";
																			$chapter1="";
																			$chapter2="";
																		}
															
																
																		$cond=$vdate.$rdate.$subject.$chapter;
																		$array=array();
															
																		$sqlsub=$database->query("select subject from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."'   ".$vdate2.$rdate3.$subject."  group by subject");
																		while($rowsub=mysqli_fetch_array($sqlsub)){
																	
																			$row['subject']=$rowsub['subject'];
																			$sqlverify=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."'  ".$vdate.$rdate.$subject);
																			$rowverify=mysqli_fetch_array($sqlverify);
																			
																			
																			$sqlreview=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and vusername1='".$row['username']."'   ".$vdate.$rdate.$subject);
																			$rowreview=mysqli_fetch_array($sqlreview);

																			
																			$sqlverify1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."'  ".$vdate2.$rdate3.$subject);
																			$rowverify1=mysqli_fetch_array($sqlverify1);
																			
																			
																			$sqlreview1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and vusername1='".$row['username']."'  ".$vdate2.$rdate3.$subject);
																			$rowreview1=mysqli_fetch_array($sqlreview1);
																
																				$s=0;
																				$timing=0;
																				$qtype=0;
																				$comple=0;
																				$qtheory=0;
																				$usage=0;
																				$class=0;
																				$exam=0;
																				$chapter12=0;
																				$topic=0;
																				//echo $vdate1.$rdate1.$subject1;
																				$sqlv1=$database->query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'     and a.vusername1='".$row['username']."' ".$vdate1.$rdate1.$subject1." and b.changes_count!=0 ");
																				while($rowv1=mysqli_fetch_array($sqlv1)){
																			
																			
																			
																					$changes=$rowv1['changes'];
																					if($rowv1['changes_count']!=0){
																						//$s++;
																					}
																					
																					$changes=$rowv1['changes'];
																					$data=explode("^",$changes);
																					$a=1;
																					// echo $_REQUEST['issues_list'];
																					$lissues=explode(",",$_REQUEST['issues_list']);
																					//print_r($lissues);
																					if($rowv1['changes']!='No Changes'){
																					if(count($data)>0){
																							$com=0;
																							$time=0;
																							$use=0;
																							$qt=0;
																							$qt1=0;
																							$c1=0;
																							$e1=0;
																							$cha1=0;
																							$top=0;
																							foreach($data as $dataa){
																								$data1=explode("_",$dataa);
																								if($data1[0]!=''){
																									$new='';
																									$old='';
																									
																									if($data1[0]=='complexity'){
																										if (in_array("complexity", explode(",",$_REQUEST['issues_list']))){
																											$comple++;
																											
																											$com=1;
																										}
																									}
																									if($data1[0]=='timeduration'){
																										if (in_array("timeduration", explode(",",$_REQUEST['issues_list']))){
																											$timing++;
																											$time=1;
																										}
																										
																									}
																									if($data1[0]=='usageset'){
																										if (in_array("usageset", explode(",",$_REQUEST['issues_list']))){
																											$usage++;
																											$use=1;
																										}
																									}
																									if($data1[0]=='questiontheory'){
																										if (in_array("questiontheory", explode(",",$_REQUEST['issues_list']))){
																											$qtheory++;
																											$qt=1;
																										}
																									}
																									if($data1[0]=='inputquestion'){
																										if (in_array("inputquestion", explode(",",$_REQUEST['issues_list']))){	
																											$qtype++;
																											$qt1=1;

																										}
																									}
																									if($data1[0]=='class'){
																										if (in_array("class", explode(",",$_REQUEST['issues_list']))){		
																											$class++;
																											$c1=1;

																										}
																									}
																									if($data1[0]=='exam'){
																										if (in_array("exam", explode(",",$_REQUEST['issues_list']))){	
																											$exam++;
																											$e1=1;
																										}
																									}
																									if($data1[0]=='chapter'){
																										if (in_array("chapter", explode(",",$_REQUEST['issues_list']))){
																											$chapter12++;
																											$cha1=1;
																										}
																									}
																									if($data1[0]=='topic'){
																										if (in_array("topic", explode(",",$_REQUEST['issues_list']))){
																										
																											$topic++;
																											$top=1;
																										}
																									}
																									
																									?>
																										
																									<?php
																									
																								
																								$a++;
																								}
																							}
																							
																							if($com!=0 || $time!=0 || $use!=0 || $qt!=0 || $qt1!=0 || $c1!=0 || $e1!=0 || $cha1!=0 || $top!=0){
																									$s++;
																								}
																						}
																						
																						
																					}
																					
																				}
																				
																			$s1=0;
																			$timing1=0;
																			$qtype1=0;
																			$comple1=0;
																			$qtheory1=0;
																			$usage1=0;
																			$class1=0;
																			$exam1=0;
																			$chapter121=0;
																			$topic1=0;
																			$sqlv2=$database->query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'    and a.vusername1='".$row['username']."' ".$vdate3.$rdate4.$subject1." and b.changes_count!=0 ");
																			while($rowv2=mysqli_fetch_array($sqlv2)){
																				$changes=$rowv2['changes'];
																				if($rowv2['changes_count']!=0){
																					//$s1++;
																				}
																				
																				$changes=$rowv2['changes'];
																				$data=explode("^",$changes);
																				$a=1;
																				
																				if($rowv2['changes']!='No Changes'){
																				$com1=0;
																				$time1=0;
																				$use1=0;
																				$qt11=0;
																				$qt12=0;
																				$c2=0;
																				$e2=0;
																				$cha2=0;
																				$top2=0;
																				if(count($data)>0){
																						foreach($data as $dataa){
																							$data1=explode("_",$dataa);
																							if($data1[0]!=''){
																								$new='';
																								$old='';
																								
																								if($data1[0]=='complexity'){
																									if (in_array("complexity", explode(",",$_REQUEST['issues_list']))){
																										$comple1++;
																										$com1=1;
																									}
																								}
																								if($data1[0]=='timeduration'){
																									if (in_array("timeduration", explode(",",$_REQUEST['issues_list']))){
																										$timing1++;
																										$time1=1;
																									}
																									
																								}
																								if($data1[0]=='usageset'){
																									if (in_array("usageset", explode(",",$_REQUEST['issues_list']))){
																										$usage1++;
																										$use1=1;
																									}
																								}
																								if($data1[0]=='questiontheory'){
																									if (in_array("questiontheory", explode(",",$_REQUEST['issues_list']))){
																										$qtheory1++;
																										$qt11=1;
																									}
																								}
																								if($data1[0]=='inputquestion'){
																									if (in_array("inputquestion", explode(",",$_REQUEST['issues_list']))){	
																										$qtype1++;
																										$qt12=1;
																									}
																								}
																								if($data1[0]=='class'){
																									if (in_array("class", explode(",",$_REQUEST['issues_list']))){		
																										$class1++;
																										$c2=1;
																									}
																								}
																								if($data1[0]=='exam'){
																									if (in_array("exam", explode(",",$_REQUEST['issues_list']))){
																										$exam1++;
																										$e2=1;
																									}
																								}
																								if($data1[0]=='chapter'){
																									if (in_array("chapter", explode(",",$_REQUEST['issues_list']))){
																										$chapter121++;
																										$cha2=1;
																									}
																								}
																								if($data1[0]=='topic'){
																									if (in_array("topic", explode(",",$_REQUEST['issues_list']))){
																										$topic1++;
																										$top2=1;
																									}
																								}
																								
																								?>
																									
																								<?php
																								
																							
																							$a++;
																							}
																						}
																						if($com1!=0 || $time1!=0 || $use1!=0 || $qt11!=0 || $qt12!=0 || $c2!=0 || $e2!=0 || $cha2!=0 || $top2!=0){
																							$s1++;
																						}
																					}
																				}
																			}
																			$chp='';
																			$sqlchp=$database->query("select chapter from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."'   ".$vdate.$rdate.$subject."  group by chapter");
																			while($rowchp=mysqli_fetch_array($sqlchp)){
																				
																				$selchp1=$database->query("select id,subject,chapter from chapter where estatus='1' and id in (".$rowchp['chapter'].")");
																				while($rowlchp1=mysqli_fetch_array($selchp1)){
																					$chp.=$rowlchp1['id'].",";
																				}
																			}
																			$chap=explode(",",$chp);
																			$chap1=array_unique($chap);
																			$chap2=implode(",",$chap1);
																			
																			
																			$subj='';
																			$sqlsub=$database->query("select subject from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."'   ".$vdate.$rdate.$subject."  group by subject");
																			while($rowsub=mysqli_fetch_array($sqlsub)){
																				$sub_row=$database->query("select id,subject from subject where estatus='1' and id ='".$rowsub['subject']."' ");
																				$rowsubject=mysqli_fetch_array($sub_row);
																				$subj.=$rowsubject['subject'].",";
																			}
																			 $percentp = $s/$rowreview['cnt'];
																			if(is_nan($percentp))
																				$percentp=0;
																			else
																				$percentp=$percentp;
																			$percent_vp= number_format( $percentp * 100) . '%';

																			 $percentp1 = $s1/$rowreview1['cnt'];
																			if(is_nan($percentp1))
																				$percentp1=0;
																			else
																				$percentp1=$percentp1;
																			$percent_vp1= number_format( $percentp1 * 100) . '%';

																			$rusername1='';
																			$sqlrevi1=$database->query("select rusername from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."' and review_status='1'  and subject='".$row['subject']."' ".$vdate.$rdate.$subject." group by rusername");
																			while($rowrevi=mysqli_fetch_array($sqlrevi1)){
																				
																				$rusername1.=$rowrevi['rusername'].",";

																				
																			}
																			$ruser=explode(",",$rusername1);
																			
																			//$rusernames="gghdfhgasfdh,dgsfdhfashdgsh,dgsafdgfsahgd";
																			//$rusernames='';
																			if(strlen($cond)>0){
																				
																				//	if($rowverify['cnt']>0 || $rowverify1['cnt']>0 ){
																					if($rowverify['cnt']>0 || $rowverify1['cnt']>0 || $rowreview['cnt']>0 || $rowreview1['cnt']>0){	
																				?>
																					<tr>
																							<td class="fixed-side"><?php echo $row['username']; ?></td>
																							<td class="fixed-side"><?php echo rtrim($subj,","); ?></td>
																							<td class="text-left">
																							<?php
																							$k=1;
																				//echo "select * from chapter where estatus='1' and id in (".rtrim($chap2,",").")";
																							$selchp11=$database->query("select * from chapter where estatus='1' and id in (".rtrim($chap2,",").")");
																							while($rowlchp11=mysqli_fetch_array($selchp11)){
																								echo $k.".".$rowlchp11['chapter']." <br />";
																								$k++;
																							}
																							?></td>
																							<td class="text-left"><?php //echo wordwrap($rusernames,15,"<br>\n",TRUE); 
																								$j=1;
																								foreach($ruser as $ruserdata){
																									if($ruserdata!=''){
																										echo $j.".".$ruserdata." <br />";
																										$j++;
																									}
																								}
																							?></td>
																							<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify['cnt']; ?></a></td>
																							<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&vstatus=1&review_status=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview['cnt']; ?></a></td>
																							<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s; ?></a></td>
																							<td><?php echo $percent_vp; ?></td>
																							<td class="complexity"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple; ?></a></td>
																							<td class="timeduration"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing; ?></a></td>
																							<td class="usageset"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage; ?></a></td>
																							<td class="inputquestion"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype; ?></a></td>
																							<td class="questiontheory"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory; ?></a></td>
																							<td class="class"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class; ?></a></td>
																							<td class="exam" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam; ?></a></td>
																							<td class="chapter"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter12; ?></a></td>
																							<td class="topic"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic; ?></a></td>
																							<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify1['cnt']; ?></a></td>
																							<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&vstatus=1&review_status=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview1['cnt']; ?></a></td>
																							<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s1; ?></a></td>
																							<td><?php echo $percent_vp1; ?></td>
																							<td class="complexity"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple1; ?></a></td>
																							<td class="timeduration" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing1; ?></a></td>

																							<td class="usageset" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage1; ?></a></td>
																							<td class="inputquestion" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype1; ?></a></td>
																							<td class="questiontheory" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory1; ?></a></td>
																							<td class="class" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class1; ?></a></td>

																							<td class="exam" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam1; ?></a></td>

																							<td class="chapter" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter121; ?></a></td>

																							<td class="topic" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['subject'];?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic1; ?></a></td>
																							
																					</tr>
																					<?php
																					}
																				
																			}
																		}
																	}
															}else{
																$sql=$database->query("select * from users where valid='1' and userlevel='3'  ");
																while($row=mysqli_fetch_array($sql)){
													
												
													
															if(isset($_REQUEST['subject'])){
																if($_REQUEST['subject']!=""){
																	$subject=" AND subject='".$_REQUEST['subject']."'";
																	$subject1=" AND a.subject='".$_REQUEST['subject']."'";
																} else {
																	$subject="";
																	$subject1="";
																}
															}else{
																$subject="";
																$subject1="";
															}

															if(isset($_REQUEST['chapter'])){
																if($_REQUEST['chapter']!=""){
																	$chapter=" AND chapter IN (".$_REQUEST['chapter'].")";
																	$chapter1=" AND a.chapter IN (".$_REQUEST['chapter'].")";
																	$chapter2=" AND id IN (".$_REQUEST['chapter'].")";
																} else {
																	$chapter="";
																	$chapter1="";
																	$chapter2="";
																}
															}else{
																$chapter="";
																$chapter1="";
																$chapter2="";
															}
															
																
															$cond=$vdate.$rdate.$subject.$chapter;
															$array=array();
															
															$sqllo=$database->query("select chapter from createquestion where estatus='1' and vstatus1='1' and vusername1='".$row['username']."' ".$vdate.$rdate.$subject.$chapter." group by chapter ");
															while($rowllo=mysqli_fetch_array($sqllo)){
																$jj=1;
																if($rowllo['chapter']!='' && $rowllo['chapter']!='0'){
																	$chap_row=$database->query("select * from chapter where estatus='1' and id in (".rtrim($rowllo['chapter'],",").") ".$chapter2."");
																	while($chapsubject=mysqli_fetch_array($chap_row)){
																		$array[]=$chapsubject['id'];
																		$jj++;
																	}
																}
																
															}
															$chapterdata=array_unique($array);
															foreach($chapterdata as $chapterdata1){
																if($chapterdata1!=''){
																	
																	$sqlverify=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and find_in_set(".$chapterdata1.",chapter) >0  and vusername1='".$row['username']."' ".$vdate.$rdate.$subject);
																	$rowverify=mysqli_fetch_array($sqlverify);
																	
																	
																	$sqlreview=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and vusername1='".$row['username']."'  and find_in_set(".$chapterdata1.",chapter) >0  ".$vdate.$rdate.$subject);
																	$rowreview=mysqli_fetch_array($sqlreview);

																	$sqlverify1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and find_in_set(".$chapterdata1.",chapter) >0  and vusername1='".$row['username']."' ".$vdate2.$rdate3.$subject);
																	$rowverify1=mysqli_fetch_array($sqlverify1);
																	
																	
																	$sqlreview1=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and vusername1='".$row['username']."'  and find_in_set(".$chapterdata1.",chapter) >0  ".$vdate2.$rdate3.$subject);
																	$rowreview1=mysqli_fetch_array($sqlreview1);
																
																	$s=0;
																	$timing=0;
																	$qtype=0;
																	$comple=0;
																	$qtheory=0;
																	$usage=0;
																	$class=0;
																	$exam=0;
																	$chapter12=0;
																	$topic=0;
																	$sqlv1=$database->query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'   and find_in_set(".$chapterdata1.",a.chapter) >0  and a.vusername1='".$row['username']."' ".$vdate1.$rdate1.$subject1);
																	while($rowv1=mysqli_fetch_array($sqlv1)){
																
																
																
																		$changes=$rowv1['changes'];
																		if($rowv1['changes_count']!=0){
																			//$s++;
																		}
																		
																		$changes=$rowv1['changes'];
																		$data=explode("^",$changes);
																		$a=1;
																		// echo $_REQUEST['issues_list'];
																		$lissues=explode(",",$_REQUEST['issues_list']);
																		//print_r($lissues);
																		if($rowv1['changes']!='No Changes'){
																		if(count($data)>0){
																				$com=0;
																				$time=0;
																				$use=0;
																				$qt=0;
																				$qt1=0;
																				$c1=0;
																				$e1=0;
																				$cha1=0;
																				$top=0;
																				foreach($data as $dataa){
																					$data1=explode("_",$dataa);
																					if($data1[0]!=''){
																						$new='';
																						$old='';
																						
																						if($data1[0]=='complexity'){
																							if (in_array("complexity", explode(",",$_REQUEST['issues_list']))){
																								$comple++;
																								
																								$com=1;
																							}
																						}
																						if($data1[0]=='timeduration'){
																							if (in_array("timeduration", explode(",",$_REQUEST['issues_list']))){
																								$timing++;
																								$time=1;
																							}
																							
																						}
																						if($data1[0]=='usageset'){
																							if (in_array("usageset", explode(",",$_REQUEST['issues_list']))){
																								$usage++;
																								$use=1;
																							}
																						}
																						if($data1[0]=='questiontheory'){
																							if (in_array("questiontheory", explode(",",$_REQUEST['issues_list']))){
																								$qtheory++;
																								$qt=1;
																							}
																						}
																						if($data1[0]=='inputquestion'){
																							if (in_array("inputquestion", explode(",",$_REQUEST['issues_list']))){	
																								$qtype++;
																								$qt1=1;

																							}
																						}
																						if($data1[0]=='class'){
																							if (in_array("class", explode(",",$_REQUEST['issues_list']))){		
																								$class++;
																								$c1=1;

																							}
																						}
																						if($data1[0]=='exam'){
																							if (in_array("exam", explode(",",$_REQUEST['issues_list']))){	
																								$exam++;
																								$e1=1;
																							}
																						}
																						if($data1[0]=='chapter'){
																							if (in_array("chapter", explode(",",$_REQUEST['issues_list']))){
																								$chapter12++;
																								$cha1=1;
																							}
																						}
																						if($data1[0]=='topic'){
																							if (in_array("topic", explode(",",$_REQUEST['issues_list']))){
																							
																								$topic++;
																								$top=1;
																							}
																						}
																						
																						?>
																							
																						<?php
																						
																					
																					$a++;
																					}
																				}
																				
																				if($com!=0 || $time!=0 || $use!=0 || $qt!=0 || $qt1!=0 || $c1!=0 || $e1!=0 || $cha1!=0 || $top!=0){
																						$s++;
																					}
																			}
																			
																			
																		}
																	}

																	$s1=0;
																	$timing1=0;
																	$qtype1=0;
																	$comple1=0;
																	$qtheory1=0;
																	$usage1=0;
																	$class1=0;
																	$exam1=0;
																	$chapter121=0;
																	$topic1=0;
																	$sqlv2=$database->query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'   and find_in_set(".$chapterdata1.",a.chapter) >0  and a.vusername1='".$row['username']."' ".$vdate3.$rdate4.$subject1);
																	while($rowv2=mysqli_fetch_array($sqlv2)){
																
																
																
																		$changes=$rowv2['changes'];
																		if($rowv2['changes_count']!=0){
																			//$s++;
																		}
																		
																		$changes=$rowv2['changes'];
																		$data=explode("^",$changes);
																		$a=1;
																		
																		if($rowv2['changes']!='No Changes'){
																		$com1=0;
																		$time1=0;
																		$use1=0;
																		$qt11=0;
																		$qt12=0;
																		$c2=0;
																		$e2=0;
																		$cha2=0;
																		$top2=0;
																		if(count($data)>0){
																				foreach($data as $dataa){
																					$data1=explode("_",$dataa);
																					if($data1[0]!=''){
																						$new='';
																						$old='';
																						
																						if($data1[0]=='complexity'){
																							if (in_array("complexity", explode(",",$_REQUEST['issues_list']))){
																								$comple1++;
																								$com1=1;
																							}
																						}
																						if($data1[0]=='timeduration'){
																							if (in_array("timeduration", explode(",",$_REQUEST['issues_list']))){
																								$timing1++;
																								$time1=1;
																							}
																							
																						}
																						if($data1[0]=='usageset'){
																							if (in_array("usageset", explode(",",$_REQUEST['issues_list']))){
																								$usage1++;
																								$use1=1;
																							}
																						}
																						if($data1[0]=='questiontheory'){
																							if (in_array("questiontheory", explode(",",$_REQUEST['issues_list']))){
																								$qtheory1++;
																								$qt11=1;
																							}
																						}
																						if($data1[0]=='inputquestion'){
																							if (in_array("inputquestion", explode(",",$_REQUEST['issues_list']))){	
																								$qtype1++;
																								$qt12=1;
																							}
																						}
																						if($data1[0]=='class'){
																							if (in_array("class", explode(",",$_REQUEST['issues_list']))){		
																								$class1++;
																								$c2=1;
																							}
																						}
																						if($data1[0]=='exam'){
																							if (in_array("exam", explode(",",$_REQUEST['issues_list']))){
																								$exam1++;
																								$e2=1;
																							}
																						}
																						if($data1[0]=='chapter'){
																							if (in_array("chapter", explode(",",$_REQUEST['issues_list']))){
																								$chapter121++;
																								$cha2=1;
																							}
																						}
																						if($data1[0]=='topic'){
																							if (in_array("topic", explode(",",$_REQUEST['issues_list']))){
																								$topic1++;
																								$top2=1;
																							}
																						}
																						
																						?>
																							
																						<?php
																						
																					
																					$a++;
																					}
																				}
																				if($com1!=0 || $time1!=0 || $use1!=0 || $qt11!=0 || $qt12!=0 || $c2!=0 || $e2!=0 || $cha2!=0 || $top2!=0){
																					$s1++;
																				}
																			}
																		}
																	}
																	

																	$chap_row=$database->query("select * from chapter where estatus='1' and id='".$chapterdata1."'");
																	$rowchapter=mysqli_fetch_array($chap_row);

																	$sub_row=$database->query("select * from subject where estatus='1' and id ='".$rowchapter['subject']."' ");
																	$rowsubject=mysqli_fetch_array($sub_row);

																	 $percentp = $s/$rowreview['cnt'];
																	if(is_nan($percentp))
																		$percentp=0;
																	else
																		$percentp=$percentp;
																	$percent_vp= number_format( $percentp * 100) . '%';

																	 $percentp1 = $s1/$rowreview1['cnt'];
																	if(is_nan($percentp1))
																		$percentp1=0;
																	else
																		$percentp1=$percentp1;
																	$percent_vp1= number_format( $percentp1 * 100) . '%';

																	$rusername1='';
																	$sqlrevi1=$database->query("select rusername from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."' and find_in_set(".$chapterdata1.",chapter) >0 and review_status='1'   ".$vdate.$rdate.$subject." group by rusername ");
																	while($rowrevi=mysqli_fetch_array($sqlrevi1)){
																		
																		$rusername1.=$rowrevi['rusername'].",";

																		
																	}
																	
																	$rusernames=rtrim($rusername1,",");
																	?>
																		<tr>
																			<td class="fixed-side"><?php echo $database->get_name('users','username',$row['username'],'name'); ?></td>
																			<td class="fixed-side"><?php echo $rowsubject['subject']; ?></td>
																			<td class=""><?php echo $rowchapter['chapter']; ?></td>
																			<td class=""><?php echo wordwrap($rusernames,50,"<br>\n",TRUE);  ?></td>
																			<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify['cnt']; ?></a></td>
																			<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&review_status=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview['cnt']; ?></a></td>
																			<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s; ?></a></td>
																			<td><?php echo $percent_vp; ?></td>
																			<td class="complexity"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple; ?></a></td>
																			<td class="timeduration"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing; ?></a></td>
																			<td class="usageset"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage; ?></a></td>
																			<td class="inputquestion"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype; ?></a></td>
																			<td class="questiontheory"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory; ?></a></td>
																			<td class="class"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class; ?></a></td>
																			<td class="exam" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam; ?></a></td>
																			<td class="chapter"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter12; ?></a></td>
																			<td class="topic"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic; ?></a></td>
																			<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify1['cnt']; ?></a></td>
																			<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $row['subject'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&review_status=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview1['cnt']; ?></a></td>
																			<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s1; ?></a></td>
																			<td><?php echo $percent_vp1; ?></td>
																			<td class="complexity"><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple1; ?></a></td>
																			<td class="timeduration" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing1; ?></a></td>

																			<td class="usageset" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage1; ?></a></td>
																			<td class="inputquestion" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype1; ?></a></td>
																			<td class="questiontheory" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory1; ?></a></td>
																			<td class="class" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class1; ?></a></td>

																			<td class="exam" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam1; ?></a></td>

																			<td class="chapter" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter121; ?></a></td>

																			<td class="topic" ><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $row['id'];?>&chapter=<?php echo $chapterdata1; ?>&vstatus=1&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic1; ?></a></td>
																			
																			
																			
																	</tr>
																	<?php

																	}
																}
															}
															}
															?>
															
															
														</tbody>
													</table>
														

												</div>
											</div>
										<?php
									} else{
									$_REQUEST['chapter3']=$_REQUEST['chapter'];
									?>
									
									
									<br />
									<script type="text/javascript">


									 $('#dataTable1').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									  
									</script>
									<div class="card border-0">
										<div class="card-body  table_data p-0">

											
												<table class=" table table-bordered  table-responsive dashboard-table mb-0" id="dataTable1">
													<thead class="thead-light">
														<tr>
															<th scope="col" class="fixed-scoll" >Lecturer</th>
															<th scope="col" class="fixed-scoll">Subject</th>
															<th scope="col" class="fixed-scoll">Chapter</th>
															<th scope="col" class="fixed-scoll">Reviewer Name</th>
															<th scope="col">Total Verified</th>
															<th scope="col">Total Reviewed</th>
															<th scope="col">Total Issue Questions</th>
															<th scope="col">Error (%)</th>
															<th scope="col">Timing Issue</th>
															<th scope="col">QuestionType Issue</th>
															<th scope="col">Complexity Issue</th>
															<th scope="col">Question TheoryIssue</th>
															<th scope="col">Usageset Issue</th>
															<th scope="col">Class Issue</th>
															<th scope="col">Exam Issue</th>
															<th scope="col">Chapter Issue</th>
															<th scope="col">Topic Issue</th>
														</tr>
													</thead>
													<tbody>
														<?php
														if($session->userlevel=='6'){
															
															$user=array();
															$userdata='';
															$sqluser=$database->query("select * from users where valid='1'  and userlevel='6' and username='".$session->username."'");
															$rowuser=mysqli_fetch_array($sqluser);
															$sello=$database->query("select * from chapter where estatus='1' and id in (".rtrim($rowuser['chapter'],",").")");
															while($rowllo=mysqli_fetch_array($sello)){
																$sqll1=$database->query("select * from users where valid='1' and userlevel='3' and find_in_set(".$rowllo['id'].",chapter)>0  ");
																while($rowll1=mysqli_fetch_array($sqll1)){
																	$user[]=$rowll1['username'];
																}
															}
															$user1=array_unique($user);
															foreach($user1 as $userd){
																$userdata.="'".$userd."'";
																$userdata.=",";
															}
															$sql=$database->query("select * from users where valid='1' and userlevel='3'  and username in (".rtrim($userdata,",").")  group by username ");
														}else{
															if($_REQUEST['chapter']!=''){
																$user=array();
																$userdata='';
																$sello=$database->query("select * from chapter where estatus='1' and id in (".rtrim($_REQUEST['chapter'],",").")");
																while($rowllo=mysqli_fetch_array($sello)){
																	$sqll1=$database->query("select * from users where valid='1' and userlevel='3' and find_in_set(".$rowllo['id'].",chapter)>0  ");
																	while($rowll1=mysqli_fetch_array($sqll1)){
																		$user[]=$rowll1['username'];
																	}
																}
																$user1=array_unique($user);
																foreach($user1 as $userd){
																	$userdata.="'".$userd."'";
																	$userdata.=",";
																}
																$sql=$database->query("select * from users where valid='1' and userlevel='3'  and username in (".rtrim($userdata,",").")  group by username ");
															}else{
																$sql=$database->query("select * from users where valid='1' and userlevel='3'  ");
															}
														}
														while($row=mysqli_fetch_array($sql)){
															
														
															
															if(isset($_REQUEST['subject'])){
																if($_REQUEST['subject']!=""){
																	$subject=" AND subject='".$_REQUEST['subject']."'";
																	$subject1=" AND a.subject='".$_REQUEST['subject']."'";
																} else {
																	$subject="";
																	$subject1="";
																}
															}else{
																$subject="";
																$subject1="";
															}

															if(isset($_REQUEST['chapter'])){
																if($_REQUEST['chapter']!=""){
																	$chapter=" AND chapter IN (".$_REQUEST['chapter'].")";
																	$chapter1=" AND a.chapter IN (".$_REQUEST['chapter'].")";
																	$chapter2=" AND id IN (".$_REQUEST['chapter'].")";
																} else {
																	$chapter="";
																	$chapter1="";
																	$chapter2="";
																}
															}else{
																$chapter="";
																$chapter1="";
																$chapter2="";
															}
															
																
															$cond=$vdate.$rdate.$subject.$chapter;
															$array=array();
															
															$sqllo=$database->query("select chapter from createquestion where estatus='1' and vstatus1='1' and vusername1='".$row['username']."' ".$vdate.$rdate.$subject.$chapter." group by chapter ");
															while($rowllo=mysqli_fetch_array($sqllo)){
																$jj=1;
																if($rowllo['chapter']!='' && $rowllo['chapter']!='0'){
																	$chap_row=$database->query("select * from chapter where estatus='1' and id in (".rtrim($rowllo['chapter'],",").") ".$chapter2."");
																	while($chapsubject=mysqli_fetch_array($chap_row)){
																		$array[]=$chapsubject['id'];
																		$jj++;
																	}
																}
																
															}
															$chapterdata=array_unique($array);
															foreach($chapterdata as $chapterdata1){
																if($chapterdata1!=''){
																	
																	$sqlverify=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1'  and find_in_set(".$chapterdata1.",chapter) >0  and vusername1='".$row['username']."' ".$vdate.$rdate.$subject);
																	$rowverify=mysqli_fetch_array($sqlverify);
																	
																	
																	$sqlreview=$database->query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and review_status='1' and vusername1='".$row['username']."'  and find_in_set(".$chapterdata1.",chapter) >0  ".$vdate.$rdate.$subject);
																	$rowreview=mysqli_fetch_array($sqlreview);
																
																	$s=0;
																	$timing=0;
																	$qtype=0;
																	$comple=0;
																	$qtheory=0;
																	$usage=0;
																	$class=0;
																	$exam=0;
																	$chapter12=0;
																	$topic=0;
																	$sqlv1=$database->query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'   and find_in_set(".$chapterdata1.",a.chapter) >0  and a.vusername1='".$row['username']."' ".$vdate1.$rdate1.$subject1);
																	while($rowv1=mysqli_fetch_array($sqlv1)){
																
																
																
																		$changes=$rowv1['changes'];
																		if($rowv1['changes_count']!=0){
																			$s++;
																		}
																		
																		$changes=$rowv1['changes'];
																		$data=explode("^",$changes);
																		$a=1;
																		
																		if($rowv1['changes']!='No Changes'){
																		if(count($data)>0){
																				foreach($data as $dataa){
																					$data1=explode("_",$dataa);
																					if($data1[0]!=''){
																						$new='';
																						$old='';
																						
																						if($data1[0]=='complexity'){
																							
																							$comple++;
																						}
																						if($data1[0]=='timeduration'){
																							
																							$timing++;
																							
																						}
																						if($data1[0]=='usageset'){
																							
																							$usage++;
																						}
																						if($data1[0]=='questiontheory'){
																							
																							$qtheory++;
																						}
																						if($data1[0]=='inputquestion'){
																								
																								$qtype++;
																						}
																						if($data1[0]=='class'){
																								
																								$class++;
																						}
																						if($data1[0]=='exam'){
																							
																							$exam++;
																						}
																						if($data1[0]=='chapter'){
																							
																							$chapter12++;
																						}
																						if($data1[0]=='topic'){
																							
																							$topic++;
																						}
																						
																						?>
																							
																						<?php
																						
																					
																					$a++;
																					}
																				}
																			}
																		}
																	}
																	

																	$chap_row=$database->query("select * from chapter where estatus='1' and id='".$chapterdata1."'");
																	$rowchapter=mysqli_fetch_array($chap_row);

																	$sub_row=$database->query("select * from subject where estatus='1' and id ='".$rowchapter['subject']."' ");
																	$rowsubject=mysqli_fetch_array($sub_row);

																	 $percentp = $s/$rowreview['cnt'];
																	if(is_nan($percentp))
																		$percentp=0;
																	else
																		$percentp=$percentp;
																	$percent_vp= number_format( $percentp * 100) . '%';

																	$rusername1='';
																	$sqlrevi1=$database->query("select rusername from createquestion where estatus='1' and vstatus1='1'   and vusername1='".$row['username']."' and review_status='1'  and subject='".$row['subject']."' ".$vdate.$rdate.$subject." group by rusername");
																	while($rowrevi=mysqli_fetch_array($sqlrevi1)){
																		
																		$rusername1.=$rowrevi['rusername'].",";

																		
																	}
																	
																	$rusernames=rtrim($rusername1,",");
																	if(strlen($cond)>0){
																		if(strlen($vdate)>0 && strlen($rdate)>0){
																			if($rowverify['cnt']>0){
																			?>
																			<tr>
																					<td class="fixed-scoll"><?php echo $database->get_name('users','username',$row['username'],'name'); ?></td>
																					<td class="fixed-scoll"><?php echo $rowsubject['subject']; ?></td>
																					<td class="fixed-scoll"><?php echo $rowchapter['chapter']; ?></td>
																					<td class=""><?php echo wordwrap($rusernames,15,"<br>\n",TRUE);  ?></td>
																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify['cnt']; ?></a></td>

																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&review_status=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview['cnt']; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s; ?></a></td>

																					<td><?php echo $percent_vp; ?></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter12; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic; ?></a></td>
																			
																			</tr>
																			<?php
																			}
																		}else{
																			if($rowverify['cnt']>0){
																			
																		?>
																			<tr>
																					<td class="fixed-scoll"><?php echo $database->get_name('users','username',$row['username'],'name'); ?></td>
																					<td class="fixed-scoll"><?php echo $rowsubject['subject']; ?></td>
																					<td class="fixed-scoll"><?php echo $rowchapter['chapter']; ?></td>
																					<td class=""><?php echo wordwrap($rusernames,15,"<br>\n",TRUE);  ?></td>
																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify['cnt']; ?></a></td>

																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&review_status=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview['cnt']; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s; ?></a></td>

																					<td><?php echo $percent_vp; ?></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter12; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic; ?></a></td>
																			</tr>
																			<?php
																			}
																		}
																	}else{
																			
																			if($rowverify['cnt']>0){
																			
																		?>
																		<tr>
																			<td class="fixed-scoll"><?php echo $database->get_name('users','username',$row['username'],'name'); ?></td>
																					<td class="fixed-scoll"><?php echo $rowsubject['subject']; ?></td>
																					<td class="fixed-scoll"><?php echo $rowchapter['chapter']; ?></td>
																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowverify['cnt']; ?></a></td>

																					<td><a  style="cursor:pointer;color: #007bff;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>reviewerdashboard1/process1.php?getreport=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&review_status=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>")' ><?php echo $rowreview['cnt']; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=')" ><?php echo $s; ?></a></td>

																					<td><?php echo $percent_vp; ?></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=timeduration')" ><?php echo $timing; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=inputquestion')" ><?php echo $qtype; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=complexity')" ><?php echo $comple; ?></a></td>

																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=questiontheory')" ><?php echo $qtheory; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=usageset')" ><?php echo $usage; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=class')" ><?php echo $class; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=exam')" ><?php echo $exam; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=chapter')" ><?php echo $chapter12; ?></a></td>
																					<td><a style="cursor:pointer;" class="text-info" data-toggle="modal" data-target="#reviewquestionmodal" onClick="setStateGet('viewDetails2','<?php echo SECURE_PATH;?>reviewerdashboard1/process2.php','viewDetails2=1&subject=<?php echo $rowsubject['id'];?>&chapter=<?php echo $chapterdata1;?>&vstatus=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&lecturer=<?php echo $row['username']; ?>&change=topic')" ><?php echo $topic; ?></a></td>
																		</tr>
																		<?php
																		}
																	}

																}
															}
														}
														?>
														
													</tbody>
												</table>
												
											</div>
										</div>
									<?php } ?>	
								</div>
								<div id="menu1" class="container tab-pane fade"><br>
										<div class="card border-0" id="ggg">
										
									</div>
								</div>
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	
	?>
	<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	<script>
	function search_report1() {
	var subject = $('#subject').val();
	var chapter = $('#chapter').val();
	var issues_list = $('#issues_list').val();
	var typelec_chapter = $('#typelec_chapter').val();
	setStateGet('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&chapter='+$('#chapter').val()+'&subject='+$('#subject').val()+'&issues_list='+$('#issues_list').val()+'&typelec_chapter='+$('#typelec_chapter').val()+'&report=1&type=<?php echo $_REQUEST['type']; ?>&type1=<?php echo $_REQUEST['type1']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&report1=1&reportrange=<?php echo $_REQUEST['reportrange']; ?>&report3=1&type3=<?php echo $_REQUEST['type3']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&report4=1&type4=<?php echo $_REQUEST['type4']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>');
	//setTimeout(function(){  issuefunction(); }, 1000);
}
</script>
<?php 
if($session->userlevel=='1' || $session->userlevel=='9'){ ?>
		<script type="text/javascript">
	$(function() {
		<?php
		if(isset($_REQUEST['type'])){

		if($_REQUEST['type'] == 'Overall'){
		?>
		var start = moment().subtract('364','days');
		<?php
		}
		if($_REQUEST['type'] == 'Today'){
		?>
		var start = moment();
		<?php
		}
		if($_REQUEST['type'] == 'Yesterday'){
		?>
		var start = moment().subtract('1','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 3 Days'){
		?>
		var start = moment().subtract('2','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 5 Days'){
		?>
		var start = moment().subtract('4','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 7 Days'){
		?>
		var start = moment().subtract('6','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 15 Days'){
		?>
		var start = moment().subtract('14','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 30 Days'){
		?>
		var start = moment().subtract('29','days');
		<?php
		}
	   
		if($_REQUEST['type'] == 'Custom Range'){
			$date3=strtotime(date('m/d/Y'));
			$cudate=explode("-",$_REQUEST['reportrange']);
			$date4=strtotime($cudate[0]);
			$datef=$date3-$date4;
			$date6=round($datef / (60 * 60 * 24));
			
		?>
			var start = moment().subtract(<?php echo $date6; ?>,'days');
		
		<?php
		}
		}
		else{
		?>
		var start = moment();
		<?php
		}
		?>
		var end = moment();
		
		function cb(start, end,para) {
			console.log("startdate",start, "enddate",end,"para",para);
			if(para!=undefined){
				var a = $('#reportrange span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
				//alert(a);
				setState('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&report=1&type='+para+'&reportrange='+$('#reportrange span').text()+'&report1=1&type1=<?php echo $_REQUEST['type1']; ?>&chapter=<?php echo $_REQUEST['chapter3']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>');
			}
			if(para=='Custom Range'){
				 $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			}else{
				 $('#reportrange span').html(para);
			}
		}
		
		$('#reportrange').daterangepicker({
			
			startDate: start,
			endDate: end,
			ranges: {

				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment()],
				'Last 3 Days': [moment().subtract(3, 'days'), moment()],
				'Last 5 Days': [moment().subtract(4, 'days'), moment()],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 15 Days': [moment().subtract(14, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'Overall': [moment().subtract(364,'days'),moment()]
			}
		}, cb);

		//cb(start, end);
		
	});
	 
	</script>
<?php
}else{
	?>
			<script type="text/javascript">
	$(function() {
		<?php
		if(isset($_REQUEST['type'])){

		if($_REQUEST['type'] == 'Overall'){
		?>
		var start = moment().subtract('364','days');
		<?php
		}
		if($_REQUEST['type'] == 'Today'){
		?>
		var start = moment();
		<?php
		}
		if($_REQUEST['type'] == 'Yesterday'){
		?>
		var start = moment().subtract('1','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 2 Days'){
		?>
		var start = moment().subtract('2','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 3 Days'){
		?>
		var start = moment().subtract('2','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 5 Days'){
		?>
		var start = moment().subtract('4','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 7 Days'){
		?>
		var start = moment().subtract('6','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 15 Days'){
		?>
		var start = moment().subtract('14','days');
		<?php
		}
		if($_REQUEST['type'] == 'Last 30 Days'){
		?>
		var start = moment().subtract('29','days');
		<?php
		}
	   
		if($_REQUEST['type'] == 'Custom Range'){
			$date3=strtotime(date('m/d/Y'));
			$cudate=explode("-",$_REQUEST['reportrange']);
			$date4=strtotime($cudate[0]);
			$datef=$date3-$date4;
			$date6=round($datef / (60 * 60 * 24));
			
		?>
			var start = moment().subtract(<?php echo $date6; ?>,'days');
		
		<?php
		}
		}
		else{
		?>
		var start = moment().subtract('1','days');


		<?php
		}
		?>
		var end = moment();
		
		function cb(start, end,para) {
			console.log("startdate",start, "enddate",end,"para",para);
			if(para!=undefined){
				var a = $('#reportrange span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
				//alert(a);
				setState('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&report=1&type='+para+'&reportrange='+$('#reportrange span').text()+'&report1=1&type1=<?php echo $_REQUEST['type1']; ?>&chapter=<?php echo $_REQUEST['chapter3']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>');
			}
			if(para=='Custom Range'){
				 $('#reportrange span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			}else{
				 $('#reportrange span').html(para);
			}
		}
		
		$('#reportrange').daterangepicker({
			
			startDate: start,
			endDate: end,
			ranges: {

				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment()],
				'Last 3 Days': [moment().subtract(3, 'days'), moment()],
				'Last 5 Days': [moment().subtract(4, 'days'), moment()],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 15 Days': [moment().subtract(14, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'Overall': [moment().subtract(364,'days'),moment()]
			}
		}, cb);

		//cb(start, end);
		
	});
	 
	</script>
	<?php
}
?>
<script type="text/javascript">
$(function() {
    <?php
    if(isset($_REQUEST['type1'])){

    if($_REQUEST['type1'] == 'Overall'){
    ?>
    var start = moment().subtract('364','days');
    <?php
    }
    if($_REQUEST['type1'] == 'Today'){
    ?>
    var start = moment();
    <?php
    }
	if($_REQUEST['type1'] == 'Yesterday'){
    ?>
    var start = moment().subtract('1','days');
    <?php
    }
	if($_REQUEST['type1'] == 'Last 3 Days'){
    ?>
    var start = moment().subtract('2','days');
    <?php
    }
	if($_REQUEST['type1'] == 'Last 5 Days'){
    ?>
    var start = moment().subtract('4','days');
    <?php
    }
    if($_REQUEST['type1'] == 'Last 7 Days'){
    ?>
    var start = moment().subtract('6','days');
    <?php
    }
    if($_REQUEST['type1'] == 'Last 15 Days'){
    ?>
    var start = moment().subtract('14','days');
    <?php
    }
    if($_REQUEST['type1'] == 'Last 30 Days'){
    ?>
    var start = moment().subtract('29','days');
    <?php
    }
   
	if($_REQUEST['type1'] == 'Custom Range'){
		$date3=strtotime(date('m/d/Y'));
		$cudate=explode("-",$_REQUEST['reportrange1']);
		$date4=strtotime($cudate[0]);
		$datef=$date3-$date4;
		$date6=round($datef / (60 * 60 * 24));
		
    ?>
		var start = moment().subtract(<?php echo $date6; ?>,'days');
	
    <?php
    }
    }
    else{
    ?>
    //var start = moment().subtract('364','days');

	var start = moment();

    <?php
    }
    ?>
    var end = moment();
	
    function cb(start, end,para) {
		console.log("startdate",start, "enddate",end,"para",para);
		if(para!=undefined){
			var a = $('#reportrange1 span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			//alert(a);
			setState('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&report1=1&type1='+para+'&reportrange1='+$('#reportrange1 span').text()+'&report=1&type=<?php echo $_REQUEST['type']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&type3=<?php echo $_REQUEST['type3']; ?>&type4=<?php echo $_REQUEST['type4']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>');
		}
		if(para=='Custom Range'){
			 $('#reportrange1 span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
		}else{
			 $('#reportrange1 span').html(para);
		}
    }
	
    $('#reportrange1').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);

	//cb(start, end);
	
});



$(function() {
    <?php
    if(isset($_REQUEST['type3'])){

    if($_REQUEST['type3'] == 'Overall'){
    ?>
    var start = moment().subtract('364','days');
    <?php
    }
    if($_REQUEST['type3'] == 'Today'){
    ?>
    var start = moment();
    <?php
    }
	if($_REQUEST['type3'] == 'Yesterday'){
    ?>
    var start = moment().subtract('1','days');
    <?php
    }
	if($_REQUEST['type3'] == 'Last 2 Days'){
    ?>
    var start = moment().subtract('2','days');
    <?php
    }
	if($_REQUEST['type3'] == 'Last 3 Days'){
    ?>
    var start = moment().subtract('2','days');
    <?php
    }
	if($_REQUEST['type3'] == 'Last 5 Days'){
    ?>
    var start = moment().subtract('4','days');
    <?php
    }
    if($_REQUEST['type3'] == 'Last 7 Days'){
    ?>
    var start = moment().subtract('6','days');
    <?php
    }
    if($_REQUEST['type3'] == 'Last 15 Days'){
    ?>
    var start = moment().subtract('14','days');
    <?php
    }
    if($_REQUEST['type3'] == 'Last 30 Days'){
    ?>
    var start = moment().subtract('29','days');
    <?php
    }
   
	if($_REQUEST['type3'] == 'Custom Range'){
		$date3=strtotime(date('m/d/Y'));
		$cudate=explode("-",$_REQUEST['reportrange3']);
		$date4=strtotime($cudate[0]);
		$datef=$date3-$date4;
		$date6=round($datef / (60 * 60 * 24));
		
    ?>
		var start = moment().subtract(<?php echo $date6; ?>,'days');
	
    <?php
    }
    }
    else{
     ?>
    var start = moment().subtract('364','days');
    <?php
    
    }
    ?>
    var end = moment();
	
    function cb(start, end,para) {
		console.log("startdate",start, "enddate",end,"para",para);
		if(para!=undefined){
			var a = $('#reportrange3 span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			//alert(a);
			setState('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&report3=1&type3='+para+'&reportrange3='+$('#reportrange3 span').text()+'&report4=1&type4=<?php echo $_REQUEST['type4']; ?>&reportrange4=<?php echo $_REQUEST['reportrange4']; ?>&report=1&type=<?php echo $_REQUEST['type']; ?>&report1=1&type1=<?php echo $_REQUEST['type1']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>');
		}
		if(para=='Custom Range'){
			 $('#reportrange3 span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
		}else{
			 $('#reportrange3 span').html(para);
		}
    }
	
    $('#reportrange3').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
			 'Last 2 Days': [moment().subtract(2, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);

	//cb(start, end);
	
});

$(function() {
    <?php
    if(isset($_REQUEST['type4'])){

    if($_REQUEST['type4'] == 'Overall'){
    ?>
    var start = moment().subtract('364','days');
    <?php
    }
    if($_REQUEST['type4'] == 'Today'){
    ?>
    var start = moment();
    <?php
    }
	if($_REQUEST['type4'] == 'Yesterday'){
    ?>
    var start = moment().subtract('1','days');
    <?php
    }
	if($_REQUEST['type4'] == 'Last 3 Days'){
    ?>
    var start = moment().subtract('2','days');
    <?php
    }
	if($_REQUEST['type4'] == 'Last 5 Days'){
    ?>
    var start = moment().subtract('4','days');
    <?php
    }
    if($_REQUEST['type4'] == 'Last 7 Days'){
    ?>
    var start = moment().subtract('6','days');
    <?php
    }
    if($_REQUEST['type4'] == 'Last 15 Days'){
    ?>
    var start = moment().subtract('14','days');
    <?php
    }
    if($_REQUEST['type4'] == 'Last 30 Days'){
    ?>
    var start = moment().subtract('29','days');
    <?php
    }
   
	if($_REQUEST['type4'] == 'Custom Range'){
		$date3=strtotime(date('m/d/Y'));
		$cudate=explode("-",$_REQUEST['reportrange4']);
		$date4=strtotime($cudate[0]);
		$datef=$date3-$date4;
		$date6=round($datef / (60 * 60 * 24));
		
    ?>
		var start = moment().subtract(<?php echo $date6; ?>,'days');
	
    <?php
    }
    }
    else{
    ?>
    //var start = moment().subtract('364','days');

	var start = moment();

    <?php
    }
    ?>
    var end = moment();
	
    function cb(start, end,para) {
		console.log("startdate",start, "enddate",end,"para",para);
		if(para!=undefined){
			var a = $('#reportrange4 span').text(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
			//alert(a);
			setState('adminForm','<?php echo SECURE_PATH;?>reviewerdashboard1/process.php','addForm=1&report3=1&type3='+para+'&reportrange3='+$('#reportrange3 span').text()+'&report4=1&type4='+para+'&reportrange4='+$('#reportrange4 span').text()+'&report3=1&type3=<?php echo $_REQUEST['type3']; ?>&reportrange3=<?php echo $_REQUEST['reportrange3']; ?>&report=1&type=<?php echo $_REQUEST['type']; ?>&report1=1&type1=<?php echo $_REQUEST['type1']; ?>&chapter=<?php echo $_REQUEST['chapter']; ?>&subject=<?php echo $_REQUEST['subject']; ?>&reportrange=<?php echo $_REQUEST['reportrange']; ?>&reportrange1=<?php echo $_REQUEST['reportrange1']; ?>');
		}
		if(para=='Custom Range'){
			 $('#reportrange4 span').html(start.format('MM/DD/YYYY') + ' - ' + end.format('MM/DD/YYYY'));
		}else{
			 $('#reportrange4 span').html(para);
		}
    }
	
    $('#reportrange4').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);

	//cb(start, end);
	
});

//var start = moment().subtract('364','days');;
var start = moment();
 var end = moment();
  $('#reportrange').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);
  $('#reportrange1').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);
	
	 $('#reportrange3').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);

	 $('#reportrange4').daterangepicker({
		
        startDate: start,
        endDate: end,
        ranges: {

            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment()],
            'Last 3 Days': [moment().subtract(3, 'days'), moment()],
			'Last 5 Days': [moment().subtract(4, 'days'), moment()],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 15 Days': [moment().subtract(14, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
			'Overall': [moment().subtract(364,'days'),moment()]
        }
    }, cb);
</script>

	<script>
	$(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	$(".selectpicker3").selectpicker('refresh');
	</script>

	<!--<script>
    jQuery(document).ready(function () {
      jQuery(".main-table").clone(true).appendTo('#table-scroll').addClass('clone');
    });
  </script>-->