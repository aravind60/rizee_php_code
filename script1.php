<?php
include('include/session.php');
ini_set('display_errors','0');

$fileName = "users.xls";

// headers for download
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");


?>
<!doctype html>
<html lang="en">
	<head>
		
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
			
			<tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Username</th>
							<th style="text-align:center;">Password</th>
							<th style="text-align:center;">Usertype</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							
						</tr>
						
						<?php
						$k=1;
						
						$sel=$database->query("select * from users where password1!=''");
						while($row=mysqli_fetch_array($sel)){
						if($row['userlevel']=='8'){
							$userlevel="Lecture";
						}else if($row['userlevel']=='7'){
							$userlevel="Dataentry";
						}else{
							$userlevel="admin";
						}
						$class ='';
						$m=1;
						$zSql = $database->query("SELECT * FROM class WHERE estatus='1' and id IN(".rtrim($row['class'],',').")"); 
						while($row1=mysqli_fetch_array($zSql)){
							$class .= $row1['class'].",";
							$m++;
						}
						$chapter ='';
						$lm=1;
						$zSql1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and id IN(".rtrim($row['chapter'],',').")"); 
						while($rowlm=mysqli_fetch_array($zSql1)){
							$chapter .= $rowlm['chapter'].",";
							$lm++;
						}
					
						
						
							?>	
							
								<td><?php echo $k;?></td>
								<td><?php echo $row['username'];?></td>
								<td><?php echo $row['password1'];?></td>
								<td><?php echo $userlevel;?></td>
								<td><?php echo rtrim($class,',');?></td>
								<td><?php echo $database->get_name('subject','id',$row['subject'],'subject'); ?></td>
								<td><?php echo rtrim($chapter,",");?></td>
							
							<?php
							echo "</tr>";
							$k++;
						}

						?>
						
					</table>
				</td>
			</tr>
		</table>
	
	</body>
</html>