<?php
ini_set("display_errors","ON");
require("../sendgrid/sendgrid-php.php");
ini_set('memory_limit', '-1');
ini_set('max_input_time ', 10000);
ini_set('max_execution_time', 10000);


$conn=mysqli_connect('localhost','rspace','Rsp@2019','neetjee') ;
function query($sql)
{
    global $conn;
	return mysqli_query($conn,$sql);
}

//$sql=query("select id,mobile,email,name from student_users where estatus='1' and userlevel='2' and mobile='7288877881'");
$sql=query("select id,mobile,email,name from student_users where estatus='1' and userlevel='2' and valid='1' and exam_id='2' order by id asc limit 10000,2000");
while($row=mysqli_fetch_array($sql)){

$email = new \SendGrid\Mail\Mail(); 
$email->setFrom("info@rizee.in", "Rizee");
$email->setSubject("Rizee - Video Solutions of JEE Mains Feb 7th All India Live Mock Test");
$email->addTo($row['email'], $row['name']);

$message='<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
       <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;" />
       <meta name="description" content="Welcome Email Template">
       <meta name="author" content="Swamy">
       <meta name="generator" content="Entrolabs It Solution Pvt Ltd">
       <title>Welcome-email-template</title>
       <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
       <style type="text/css">body{width:100%;background-color:#eff1f4;margin:0;padding:0;-webkit-font-smoothing:antialiased;mso-margin-top-alt:0px;mso-margin-bottom-alt:0px;mso-padding-alt:0px 0px 0px 0px}.mast-social-footer ul{margin:2px 0 0 -20px;width:auto}.mast-social-footer ul li{list-style:outside none none;margin:0 0 0 10px;text-indent:0;width:auto}.mast-social-footer ul li, .mast-social-footer ul a{float:left}.mast-social-footer ul a span{font-size:18px}.fb:hover{color:#4060a5}.link:hover{color:#0094bc}.skype:hover{color:#00c6ff}.gp:hover{color:#e64522}.tw:hover{color:#00abe3}p,h1,h2,h3,h4{margin-top:0;margin-bottom:0;padding-top:0;padding-bottom:0}span.preheader{display:none;font-size:1px}html{width:100%}table{font-size:12px;border:0}.menu-space{padding-right:25px}a,a:hover{text-decoration:none;color:#FFF}@media only screen and (max-width:767px){body{width:auto !important}table[class=main]{width:440px !important}table[class=two-left]{width:420px !important;margin:0px auto}table[class=full]{width:100% !important;margin:0px auto}table[class=menu-space]{padding-right:0px}table[class=menu]{width:438px !important;margin:0px auto;border-bottom:#e1e0e2 solid 1px}table[class=two-left-inner]{width:400px !important;margin:0 auto 2px}table[class=two-left-menu]{text-align:center}}@media only screen and (max-width:479px){body{width:auto !important}table[class=main]{width:310px !important}table[class=two-left]{width:300px !important;margin:0px auto}table[class=full]{width:100% !important;margin:0px auto}table[class=menu-space]{padding-right:0px}table[class=menu]{width:308px !important;margin:0px auto;border-bottom:#e1e0e2 solid 1px}table[class=two-left-inner]{width:280px !important;margin:0 auto 2px}table[class=two-left-menu]{width:310px !important;margin:0px auto}}.table-heading th{color:#011636;font-weight:700}</style>
    </head>
    <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
       <table width="100%" cellspacing="0" cellpadding="0" border="0" align="center">
       <tr>
          <td align="center" valign="middle">
             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                   <td align="center" valign="middle">
                      <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                         <tr>
                            <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:5px 5px 0 0;">
                               <table width="615" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                  <tr>
                                     <td height="10" align="left" valign="top" style="line-height:10px; font-size:10px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                     <td align="center" valign="top">
                                        <table width="600" border="0" align="left" cellpadding="0" cellspacing="0" class="two-left-inner">
                                           <tr>
                                              <td align="left" valign="top"> <a href="https://youtu.be/5AspDGk129Y" target="_blank"> <img src="https://admin.rizee.in/vendor/images/notification_image.jpg"   alt="Rizee - The Perfect Guide" width="700" height="600"></a></td>
                                           </tr>
                                        </table>
                                     </td>
                                  </tr>
                                  <tr>
                                     <td height="10" align="left" valign="top" style="line-height:10px; font-size:10px;">&nbsp;</td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                   <td align="center" valign="middle">
                      <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                         <tr>
                            <td align="center" valign="top" bgcolor="#FFFFFF">
                               <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                  <tr>
                                     <td height="25" align="center" valign="top" style="font-size:25px; line-height:25px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                     <td align="left" valign="middle" style="font-family: sans-serif, Verdana; font-size:18px; font-weight:600; color:#000; text-transform:capitalize;"> Hello <span style="font-weight:700;">'.ucwords($row['name']).',</span></td>
                                  </tr>
                                  <tr>
                                     <td height="15" align="center" valign="top" style="line-height:15px; font-size:15px;">&nbsp;</td>
                                  </tr>
                                  <tr>
                                     <td align="center" valign="top">
                                        <table width="700" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                           <tr>
                                              <td align="left" valign="top" style="font-family: sans-serif; font-size:16px; font-weight:normal; line-height:28px; color:#000;"> Tune in for Complete Video Solutions of All India Live Mock Test for JEE Mains from the Expert IIT JEE Faculty. Get insights on how to tackle different types of questions & time management for your Final Examination. Don&#10076;t Miss Your Opportunity- a lot of important Questions & Topics for your Upcoming JEE Mains 2021 have been comprehensively covered in the session.</td>
                                           </tr>
                                           <tr>
                                              <td  align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr>
                                          
                                           <tr>
                                              <td  align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr
                              
                               
                                 <tr>
                                              <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr>
                               
                                  <tr>
                                              <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr>
									 <tr>
                                              <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> <b>Click on below links to watch it on Youtube : </b><br />
										</td>
									 </tr>
                                   <tr>
                                              <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr>
                                
										 <tr>
                                              <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> AILMT- Chemistry Video Solutions: <a href="https://youtu.be/5AspDGk129Y" style="color:#039be5;">https://youtu.be/5AspDGk129Y</a><br /><br />
										</td>
									 </tr>
									  <tr>
                                              <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> AILMT- Mathematics Video Solutions: <a href="https://youtu.be/QZfJ4OdsCiM" style="color:#039be5;">https://youtu.be/QZfJ4OdsCiM</a><br /><br />
										</td>
									 </tr>
									  <tr>
                                              <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;"> AILMT- Physics Video Solutions: <a href="https://youtu.be/rsH2hbjDBD4" style="color:#039be5;">https://youtu.be/rsH2hbjDBD4</a><br /><br />
										</td>
									 </tr>
                                   <tr>
                                              <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;"> &nbsp;</td>
                                           </tr>
                                          
                                          
                                           
										 <tr>
                                              <td align="left" valign="top" style="font-family:  sans-serif; font-size:16px; font-weight:normal; line-height:23px; color:#000;">Visit <a href="www.rizee.in" style="color:#039be5;">www.rizee.in</a>  or download our app from the Play Store to get more details.
										</td>
									 </tr>
                                          
                                        </table>
                                     </td>
                                  </tr>
                                  <tr>
                                     <td height="45" align="center" valign="top" style="font-size:45px; line-height:45px;">&nbsp;</td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                   <td align="center" valign="middle">
                      <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                         <tr>
                            <td align="center" valign="top" bgcolor="#FFFFFF">
                               <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                  <tr>
                                     <td height="15" align="center" valign="top" style="font-size:15px; line-height:15px;">&nbsp;</td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
             <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                <tr>
                   <td align="center" valign="middle">
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                         <tr>
                            <td align="center" valign="middle">
                               <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                                  <tr>
                                     <td align="center" valign="top" bgcolor="#FFFFFF">
                                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                           <tr>
                                              <td height="20" align="center" valign="top" style="font-size:20px; line-height:20px;"> &nbsp;</td>
                                           </tr>
                                        </table>
                                     </td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                      <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#eff1f4">
                         <tr>
                            <td align="center" valign="middle">
                               <table width="750" border="0" align="center" cellpadding="0" cellspacing="0" class="main">
                                  <tr>
                                     <td align="center" valign="top" style="border-top:1px solid #eff1f4;"></td>
                                  </tr>
                                  <tr>
                                     <td align="center" valign="top" bgcolor="#FFFFFF" style="border-radius:0 0 5px 5px;">
                                        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" class="two-left-inner">
                                           <tr>
                                              <td height="20" align="center" valign="top">&nbsp;</td>
                                           </tr>
                                           <tr>
                                              <td align="center" valign="top" style="font-family: sans-serif; font-size:14px; font-weight:normal; line-height:28px; color:#000;"> If you Like Rizee, rate us on Google Play store!</td>
                                           </tr>
                                           <tr>
                                              <td height="15" align="center" valign="top">&nbsp;</td>
                                           </tr>
                                           <tr>
                                              <td align="center">
                                                 <table class="display-width" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; width:auto !important;" cellspacing="0" cellpadding="0" border="0" align="center">
                                                    <tr>
                                                       <td style="color:#333333; line-height:0;" width="120" align="center"> <a href="https://play.google.com/store/apps/details?id=in.rizee.mylearningplus" style="color:#333333; text-decoration:none;" data-color="App-Content"> <img src="http://rizee.in/gpstore.png" alt="googleplay-store" style="color:#333333; width:100%; line-height:0; border-radius:5px; line-height:0;"> </a></td>
                                                    </tr>
                                                 </table>
                                              </td>
                                           </tr>
                                           <tr>
                                              <td height="15" align="center" valign="top">&nbsp;</td>
                                           </tr>
                                           <tr>
                                              <td align="center" valign="middle" class="mast-social-footer" style="font-family: sans-serif; font-size:14px; font-weight:normal; line-height:28px;"> Sent By <a href="http://www.rizee.in" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Rizee </a> <a href="https://www.rizee.in/support" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Support Center</a> <a href="https://www.rizee.in/privacy" target="_blank" style="margin-left: 10px;margin-right: 10px;color: #1a8cec;text-decoration: none;">Privacy Policy</a></td>
                                           </tr>
                                           <tr>
                                              <td height="20" align="left" valign="top">&nbsp;</td>
                                           </tr>
                                        </table>
                                     </td>
                                  </tr>
                               </table>
                            </td>
                         </tr>
                      </table>
                   </td>
                </tr>
             </table>
    </body>
 </html>';
    $email->addContent("text/html",$message);

    $sendgrid = new \SendGrid('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');
    try {
      
        $response = $sendgrid->send($email);
        if($response->statusCode()=='202'){
           echo "insert into mail_log set username='".$row['mobile']."',subject='Video Solutions of JEE Mains Feb 7th All India Live Mock Test',timestamp='".time()."'";
           query("insert into mail_log set username='".$row['mobile']."',subject='Video Solutions of JEE Mains Feb 7th All India Live Mock Test',timestamp='".time()."'");
        }
        print $response->statusCode() . "\n";
        print_r($response->headers());
        print $response->body() . "\n";
    } catch (Exception $e) {
        echo 'Caught exception: '. $e->getMessage() ."\n";
    }
}
?>