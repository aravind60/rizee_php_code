<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
/*define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");*/
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Questions Overview".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

// headers for download
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");



?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
			
			<tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
							<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Subject Id</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Chapter Id</th>
							<th style="text-align:center;">Topic</th>
							<th style="text-align:center;">Topic Id</th>
							<th style="text-align:center;">Questions</th>
							<th style="text-align:center;">Verified Questions</th>
						</tr>
						<?php
						$k=1;
						
						$chap_sel=query("select * from chapter where estatus='1'  group by  id order by class,subject ASC ");
					
						while($chap_row=mysqli_fetch_array($chap_sel)){
							$quec_sel=query("select count(*) As count from createquestion where estatus='1' and class in (".$chap_row['class'].") and subject in (".$chap_row['subject'].") and chapter in (".$chap_row['id'].") ");
							$quec_row=mysqli_fetch_array($quec_sel);

							$topic_count=0;
							$topi_sel=query("select * from topic where estatus='1' and class in (".$chap_row['class'].") and subject in (".$chap_row['subject'].") and chapter in (".$chap_row['id'].") ");
							
							$class=query("select * from class where estatus='1'  and id='".$chap_row['class']."'");
								$class_row=mysqli_fetch_array($class);
								$subject=query("select * from subject where estatus='1'  and id='".$chap_row['subject']."'");
								$sub_row=mysqli_fetch_array($subject);
							while($topi_row=mysqli_fetch_array($topi_sel)){
								$que_sel=query("select count(*) As count from createquestion where estatus='1' and class in (".$chap_row['class'].") and subject in (".$chap_row['subject'].") and chapter in (".$chap_row['id'].")  and topic in (".$topi_row['id'].") ");
								$que_row=mysqli_fetch_array($que_sel);


								$que_selv=query("select count(*) As count from createquestion where estatus='1' and class in (".$chap_row['class'].") and subject in (".$chap_row['subject'].") and chapter in (".$chap_row['id'].")  and topic in (".$topi_row['id'].") and vstatus='1' ");
								$que_rowv=mysqli_fetch_array($que_selv);
								
								
								$topic_count=$topic_count+$que_row['count'];
							echo "<tr>";
							?>	
							
								<td><?php echo $k;?></td>
								<td ><?php echo $class_row['class'];?></td>
								<td><?php echo $sub_row['subject']; ?></td>
								<td ><?php echo $sub_row['id'];?></td>
								<td ><?php echo $chap_row['chapter'];?></td>
								<td><?php echo $chap_row['id']; ?></td>
								<td ><?php echo $topi_row['topic']; ?></td>
								<td ><?php echo $topi_row['id'];?></td>
								<td><?php echo $que_row['count']; ?></td>
								
								<td><?php echo $que_rowv['count']; ?></td>
							<?php
							echo "</tr>";
							$k++;
									
							}
							$quec=$quec_row['count']-$topic_count;
							?>
							<tr >
								<td></td>
								<td><?php echo $class_row['class'];?></td>
								<td><?php echo $sub_row['subject']; ?></td>
								<td><?php echo $sub_row['id']; ?></td>
								<td><?php echo $chap_row['chapter'];?></td>
								<td><?php echo $chap_row['id'];?></td>
								<td>Topic Not Assigned</td>
								<td>Not Assigned</td>
								<td><?php echo $quec; ?></td>
								<td>&nbsp;</td>
							</tr>
						<?php	
						}

						?>
						
					</table>
				</td>
			</tr>
		</table>
		<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>
?>