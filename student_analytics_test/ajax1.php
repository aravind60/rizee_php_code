<?php
include('../include/session.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simple to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
 
// DB table to use
$table = 'student_users';
 
// Table's primary key
$primaryKey = 'id';
 

if(isset($_REQUEST['userlevel'])){
		if($_REQUEST['userlevel']=='1'){
			$userlevel=" AND userlevel='1'";
		}else{
			$userlevel=" AND userlevel='2'";
		}
	}else{
		$userlevel='';
	}
	if(isset($_REQUEST['typed'])){
		if($_REQUEST['typed']=='paid'){
			$type=" AND current_plan_id!='5'";
		}else if($_REQUEST['typed']=='trail'){
			$type=" AND current_plan_id='5' and expiry_date>=".time()."";
		}else if($_REQUEST['typed']=='trailexp'){
			$type=" AND current_plan_id='5' and expiry_date<".time()."";
		}else{
			$type="";
		}
	}else{
		$type='';
	}
	/*if(isset($_REQUEST['date'])){
		if($_REQUEST['date']!=''){
			$date=" AND timestamp between ".strtotime($_REQUEST['date']. "00:00:00")." AND ".strtotime($_REQUEST['date']. "23:59:59")." ";
		}else{
			$date="";
		}
	}else{
		$date='';
	}*/

	$dateto=strtotime(date('d-m-Y'));
	$dateto1 = strtotime(date('d-m-Y'). ' 23:59:59');
	$dateyess1 = strtotime('-1 days',$dateto);
	 $datetoyes1 = strtotime(date('d-m-Y',$dateyess1).'23:59:59');
	 $dateyess2 = strtotime(date('d-m-Y',strtotime("-2 days")));
	$dateyess3 = strtotime(date('d-m-Y',strtotime("-3 days")));
	$dateyess5 = strtotime(date('d-m-Y',strtotime("-5 days")));
	$dateyess7 = strtotime(date('d-m-Y',strtotime("-7 days")));
	$dateyess15= strtotime(date('d-m-Y',strtotime("-15 days")));
	$dateyess30= strtotime(date('d-m-Y',strtotime("-30 days")));
	$dateyess364= strtotime(date('d-m-Y',strtotime("-364 days")));
if(isset($_REQUEST['type'])){
	if($_REQUEST['type']=="Today"){
		$date=" and timestamp between ".$dateto." and ".$dateto1." ";
	} else if($_REQUEST['type']=="Yesterday"){
		$date=" and timestamp between ".$dateyess1." and ".$datetoyes1." ";
	}else if($_REQUEST['type']=="Last 2 Days"){
		$date=" and timestamp between ".$dateyess2." and ".$dateto1." ";
		}
	else if($_REQUEST['type']=="Last 3 Days"){
		$date=" and timestamp between ".$dateyess3." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 5 Days"){
		$date=" and timestamp between ".$dateyess5." and ".$dateto1." ";
	}
	else if($_REQUEST['type']=="Last 7 Days"){
		$date=" and timestamp between ".$dateyess7." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 15 Days"){
		$date=" and timestamp between ".$dateyess15." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Last 30 Days"){
		$date=" and timestamp between ".$dateyess30." and ".$dateto1." ";
	}else if($_REQUEST['type']=="Overall"){
		if($_REQUEST['reportrange']!=''){
			$date=" and timestamp between ".$dateyess364." and ".$dateto1." ";
		}
	}else if($_REQUEST['type']=="Custom Range"){
		if($_REQUEST['reportrange']!=''){
			$dat=explode("-",$_REQUEST['reportrange']);
			$fdat=strtotime($dat[0]);
			$tdat=strtotime($dat[1]);
			$date=" and timestamp between ".$fdat." and ".$tdat." ";
		}
		
	}else{
		
		$date='';
	}
}else{
	
		
	$date='';	
}
	if(isset($_REQUEST['institution_id'])){
		if($_REQUEST['institution_id']!=''){
			$institution_id=" AND institution_id='".$_REQUEST['institution_id']."'";
		}else{
			$institution_id="";
		}
	}else{
		$institution_id='';
	}
	$condition="  estatus='1'and valid='1'".$userlevel.$date.$institution_id.$type."";
//$con = mysqli_connect("localhost","root","","qsbank");
$con = mysqli_connect("localhost","rspace","Rsp@2019","qsbank");
function query($sql){
global $con;
 return mysqli_query($con,$sql);
}


//$con1 = mysqli_connect("localhost","root","","neetjee");
$con1 = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
function query1($sql){
global $con1;
 return mysqli_query($con1,$sql);
}



 $columns = array(
    array( 'db' => 'mobile', 'dt' => 0),
    array( 'db' => 'name',  'dt' => 1),
    array( 'db' => 'email',   'dt' => 2 ),
    array( 'db' => 'mobile',     'dt' => 3 ),
	array( 'db' => 'class_id','dt' => 4,
        'formatter' => function( $d, $row ) {
			 $sql=query("select * from class where estatus='1' and id='".$d."'");
			$row=mysqli_fetch_array($sql);
			return $row['class'];
        }
    ),
	 array( 'db' => 'exam_id','dt' => 5,
        'formatter' => function( $d, $row ) {
			 $sql=query("select * from exam where estatus='1' and id='".$d."'");
			$row=mysqli_fetch_array($sql);
			return $row['exam'];
        }
    ),
	 array( 'db' => 'timestamp','dt' => 6,
        'formatter' => function( $d, $row ) {
            return date( 'd/m/Y', $d);
        }
    ),
	
	array( 'db' => 'current_plan_id',     'dt' => 7,
		'formatter' => function( $d, $row ) {
			$sqll1=query1("SELECT * FROM `student_plans` where estatus='1' and id='".$d."'");
			$rowll1=mysqli_fetch_array($sqll1);
			
			return $rowll1['valid_days'];
			
        } ),
	 array( 'db' => 'expiry_date','dt' => 8,
		'formatter' => function( $d, $row ) {
			if($d!=''){
				return date("d/m/Y",$d);
			}else{
				return '';
			}
        }),
	array( 'db' => 'target_year',     'dt' => 9),	
	 array( 'db' => 'mobile','dt' => 10,
		'formatter' => function( $d, $row ) {
			$sel_practice=query1("select id from student_questions where estatus='1' and session_id!='' and mobile='".$d."' group by question_id");
			$row_practicetot=mysqli_num_rows($sel_practice);
			if($row_practicetot!=''){ return number_format($row_practicetot); } else { return '0'; }
        }),

	array( 'db' => 'mobile','dt' => 11,
		'formatter' => function( $d, $row ) {
			$sel_chapter=query1("SELECT id FROM `student_exam_sessions`  where estatus='1' and type='custom' and sub_type='chapter'  and mobile='".$d."' and is_completed='1' ORDER BY `id` DESC");
			$row_chaptertot=mysqli_num_rows($sel_chapter);

			if($row_chaptertot!=''){ return $row_chaptertot; } else { return '0'; }
     }),

	 array( 'db' => 'mobile','dt' => 12,
		'formatter' => function( $d, $row ) {
			$sel_cumulative=query1("SELECT id FROM `student_exam_sessions`  where estatus='1' and type='custom' and sub_type='cumulative' and mobile='".$d."' and is_completed='1' ORDER BY `id` DESC");
			$row_cumulative=mysqli_num_rows($sel_cumulative);

			if($row_cumulative!=''){ return $row_cumulative; } else { return '0'; }
     }),
	array( 'db' => 'mobile','dt' => 13,
		'formatter' => function( $d, $row ) {
			$sel_error=query1("SELECT id FROM `student_exam_sessions`  where estatus='1' and type='error_exam'  and mobile='".$d."' and is_completed='1' ORDER BY `id` DESC");
			$row_error=mysqli_num_rows($sel_error);

		if($row_error!=''){ return $row_error; } else { return '0'; }
     }),
	array( 'db' => 'mobile','dt' => 14,
		'formatter' => function( $d, $row ) {
			$sel_prev=query1("SELECT id FROM `student_exam_sessions`  where estatus='1' and type='previous_exam'  and mobile='".$d."' and is_completed='1' ORDER BY `id` DESC");
			$row_prev=mysqli_num_rows($sel_prev);

		if($row_prev!=''){ return $row_prev; } else { return '0'; }
     }),

	array( 'db' => 'mobile','dt' => 15,
		'formatter' => function( $d, $row ) {
			$sel_rev=query1("SELECT sum(count) as cnt FROM `student_content_views` where mobile='".$d."' and content_type!=1");
											$row_rev=mysqli_fetch_array($sel_rev);

		if($row_rev['cnt']!=''){ return $row_rev['cnt']; } else { return '0'; }
     }),

	array( 'db' => 'mobile','dt' => 16,
		'formatter' => function( $d, $row ) {
			$sel_short=query1("SELECT sum(count) as cnt FROM `student_content_views` where mobile='".$d."' and content_type=1");
			$row_short=mysqli_fetch_array($sel_short);

		if($row_short['cnt']!=''){ return $row_short['cnt']; } else { return '0'; }
     }),
	
	
	
);
 
// SQL server connection information
$sql_details = array(
    'user' => 'rspace',
    'pass' => 'Rsp@2019',
    'db'   => 'neetjee',
    'host' => 'localhost'
);
 
 
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( 'ssp.class.php' );
  $whereall=$condition;

  $output =  SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $whereall );

//print_r($output);

$convert = json_encode($output);


if(json_last_error_msg() == "No error"){
    echo $convert;

}
else{
        $temp = $output['data'];

        foreach($temp as $k =>$tmp){

        	 $output['data'][$k] = mb_convert_encoding($tmp, 'UTF-8', 'UTF-8');

		}

		echo json_encode($output);
}

?>