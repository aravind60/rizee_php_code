<?php
include('../include/session.php');
error_reporting(E_ALL);
ini_set('display_errors', 1);
if(isset($_REQUEST['formdata'])){
?>
<div class="col-lg-12 col-md-12 pt-3 pb-3" >
									<div class="row" >
										
										<div class="col-lg-6">
											<div class="form-group row">
											<label class="col-sm-5 col-form-label">From Time</label>
												<div class="col-sm-7">
												
													
													<select id="fromtime" name="fromtime" class="form-control">
														<option value=''>-Select-</option>
															<?php
															for($hours=5; $hours<24; $hours++)
															{
																for($mins=0; $mins<60; $mins+=30)
																{ 
																	$ctime=time();
																	
																	$time = str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($mins,2,'0',STR_PAD_LEFT).'';
																	$ctime1=strtotime($time);
																	$timed1=round(($ctime-$ctime1)/60);
																	//echo '<option value= "'.$time.'"  >'.$time.'</option>';
																	if($timed1>30){
																	?>
																		<option value="<?php echo $time;?>" <?php if(isset($_REQUEST['fromtime'])){ if($time==$_REQUEST['fromtime']) { echo 'selected="selected"'; } }else{ echo 'selected="selected"';}?>><?php echo date('h:i A', strtotime($time));;?></option>
																	<?php
																	}
																}
															}
															
															?>
															</select>
											
															<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['fromtime'])){ echo $_SESSION['error']['fromtime'];}?></span>
													
												</div>
											</div>
										</div>
									
										<div class="col-lg-6">
											<div class="form-group row">
											<label class="col-sm-5 col-form-label">To Time</label>
												<div class="col-sm-7">
												
													<select id="totime" name="totime" class="form-control">
														<option value=''>-Select-</option>
															<?php
															$j=1;
															for($hours=5; $hours<24; $hours++)
															{
																for($mins=0; $mins<60; $mins+=30)
																{	
																	if($j>=2){

																	$ctime=time();
																		$ctimed=date("H:i",$ctime);
																		
																			$time = str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($mins,2,'0',STR_PAD_LEFT).'';

																			if($ctimed>$time){
																				?>
																			<option value="<?php echo $time;?>" <?php if($time==$_REQUEST['totime']) { echo 'selected="selected"'; }else{  }?>><?php echo date('h:i A', strtotime($time));?></option>
																		<?php
																			}
																		
																	}
																	$j++;
																}
															}
															?>
															</select>
											
															<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['totime'])){ echo $_SESSION['error']['totime'];}?></span>
												</div>
											</div>
										</div>
										
										
										
										
										<div class="col-lg-6" >
											<div class="form-group row">
											<label class="col-sm-5 col-form-label">Mobile</label>
												<div class="col-sm-7">
												<input type="text" name="mobile" id="mobile" value="<?php if(isset($_REQUEST['mobile'])){ echo $_REQUEST['mobile'];}else{}?>" class="form-control"   >
												
												</div>
											</div>
										</div>

										
									</div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group row">
												<a class="radius-20 btn btn-theme px-5" onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>student_anaytics_test/process.php','tableDisplay=1&fromtime='+$('#fromtime').val()+'&totime='+$('#totime').val()+'&mobile='+$('#mobile').val()+'')"> Search</a>
											</div>
										</div>
									</div>
								</div>


								<?php }
								?>