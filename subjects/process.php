<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}

//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM subject WHERE id = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 
 <?php
    }
 }
 ?>
	
	<script>
		$(function () {
			$('.datepicker1,.datepicker2,.datepicker3').datetimepicker({
			   format: 'DD-MM-YYYY'
			   
			});
		});

	</script>
	<!-- <script>
		
		editor = com.wiris.jsEditor.JsEditor.newInstance({'language': 'en'});
	                 editor.insertInto(document.getElementById('editorContainer'));
				 width:300;
				 height:500;
	</script> -->
	
  <div class="col-lg-12 col-md-12">
 
 	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Subject<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" class="form-control" name="subject" value="<?php if(isset($_POST['subject'])) { echo $_POST['subject']; }else{} ?>"  autocomplete="off" id="subject" >
					<span class="error"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<a class="radius-20 btn btn-theme px-5" onClick="setState('adminForm','<?php echo SECURE_PATH;?>subjects/process.php','validateForm=1&subject='+$('#subject').val()+'&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?><?php if(isset($_POST['editform'])) { echo '&editform='.$_POST['editform']; } ?>')"> Save</a>
			</div>
		</div>
	</div>
	
	
	
</div>

<?php
unset($_SESSION['error']);
}
?>

<?php
if(isset($_POST['validateForm'])){
	
	$_SESSION['error'] = array();
	
	$post = $session->cleanInput($_POST);

	 	$id = 'NULL';
		  	if(isset($post['editform'])){
	  $id = $post['editform'];

	}
	
	if($id=='NULL')
	{
		
		$result=$database->query('insert subject set subject="'.$post['subject'].'",timestamp="'.time().'"');
		
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Subject Class  Successfully!</div>
			
				<script type="text/javascript">
			
				animateForm('<?php echo SECURE_PATH;?>subjects/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
		<?php
		
	}else{
		$result=$database->query('update subject set subject="'.$post['subject'].'" where id="'.$id.'"');
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Subject Updated  Successfully!</div>
			
				<script type="text/javascript">
			
				animateForm('<?php echo SECURE_PATH;?>subjects/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
			</script>
	<?php
	}
	

?>

 
	<?php
	

}
?>
<?php

//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'subject';
  $condition = "estatus='1'";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."class/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ";
 $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  $query = "SELECT * FROM $tableName ".$condition."  order by id asc";
  
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	  
	?>
		  <script type="text/javascript">


  $('#dataTable').DataTable({
    "pageLength": 50
    
    
  });

</script>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">Subject</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered" id="dataTable" width="100%" cellspacing="0" id="dataTable">
									<thead>
										<tr>
											<th class="text-left" nowrap>Sr.No.</th>
											<th class="text-left" nowrap>Subject</th>
											<th class="text-left" nowrap>Options</th>
										</tr>
									</thead>
									<tbody>
									<?php
										$k=1;
									  while($value = mysqli_fetch_array($data_sel))
									  {
										 
									  ?>
														<tr>
														<td class="text-left"><?php echo $k;?></td>
										<td class="text-left" nowrap><?php echo $value['subject'];?></td>
										<td class="text-left" nowrap><a class=""  onClick="setState('adminForm','<?php echo SECURE_PATH;?>subjects/process.php','addForm=2&editform=<?php echo $value['id'];?>')"><span style="color:blue;"><i class="fa fa-pencil-square-o" style="color:blue;"></i></a>&nbsp;&nbsp;&nbsp;
										<a class=" " onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>subjects/process.php','rowDelete=<?php echo $value['id'];?>')"><i class="fa fa-trash" style="color:red;"></i></a></td>
									  </tr>
														<?php
									$k++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
  </section>
 
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
?>
	<?php
if(isset($_GET['rowDelete'])){
	$database->query("DELETE FROM subject WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> Subject deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>subjects/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>

   
   <br  />
   		<?php


?>




