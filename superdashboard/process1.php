<?php
include('../include/session.php');
error_reporting(0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>

 <?php echo $session->commonJS();?>
 <?php echo $session->commonAdminCSS();?>
   <style>
			.pagination {
				display:block;
				text-align:left;				
				font-size:12px;
				font-weight:normal;
				/*margin:auto;*/
				
			}

			.pagination a,.pagination a:link,visited{
			border: 1px solid transparent;
				-webkit-border-radius: 5px;
				-moz-border-radius: 5px;
			display: inline-block;
				padding: 5px 10px;
				margin: 0 3px;
				cursor: pointer;
				border-radius: 3px;
				*cursor: hand;
				color: #797979;
				text-decoration:none;
			}

			.pagination a:hover {
				font-size:12px;
			
			background-color: #eee;
			
				
			}



			.pagination .current {
				display: inline-block;
				padding: 5px 10px;
				margin-left:2px;
				text-decoration:none;
				background: none repeat scroll 0 0 #fff;
				border-radius: 50%;
				color: #797979;
			
				cursor:default;
			border: 1px solid #ddd;
			
				
			}


			.pagination .disabled {
				display: inline-block;
				padding: 5px 10px;
			 border: 1px solid transparent;
				border-radius: 3px;
			
			margin-left:3px;
			color: #c7c7c7;
				cursor:default;
			}

			.list-Unstyles{
				position:absolute;
				z-index:30 !important;
				cursor:pointer;
			}
		</style>
	<script>
		function search_report() {
		
			var class1 = $('#class').val();
			var subject = $('#subject').val();
			var chapter = $('#chapter').val();
			var topic = $('#topic').val();
			var date = $('#date').val();
			var status = $('#status').val();
			var explanation = $('#explanation').val();
			var complexity = $('#complexity').val();
			var usageset = $('#usageset').val();
			var question_id = $('#question_id').val();
			setStateGet('adminTable','<?php echo SECURE_PATH;?>superdashboard/process1.php','viewDetails2=1&class='+class1+'&subject='+subject+'&chapter='+chapter+'&topic='+topic+'&date='+date+'&status='+status+'&explanation='+explanation+'&complexity='+complexity+'&usageset='+usageset+'&question_id='+question_id);
		}
	</script>
	<div class="modal fade" id="reviewexampleModal" tabindex="-1"
				role="dialog" aria-labelledby="exampleModalLabel"
				aria-hidden="true">
				<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="exampleModalLabel">
								Question Details
							</h5>
							<button type="button" class="close" data-dismiss="modal"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div class="modal-body" id="reviewviewDetails">
							
						</div>
						
					</div>
				</div>
			</div>
	<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Question Review
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>	
			<div class="modal fade" id="exampleModal" tabindex="-1"
					role="dialog" aria-labelledby="exampleModalLabel"
					aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered modal-lg" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">
									Question Review
								</h5>
								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body question-modal" id="viewDetails">
								
							</div>
							
						</div>
					</div>
				</div>
	<div id="adminTable">
		<?php
		if(isset($_REQUEST['viewDetails2']))
		{
			
			
			
		?>
		
		<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
						<div class="card-header py-3">
							<h6 class="m-0 font-weight-bold text-primary">Question Details</h6>
						</div>
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Date</label>
										<div class="col-sm-8">
											
											<input type="text" name="date" id="date" class="datepicker form-control" value="<?php echo $_REQUEST['date'];?>" onblur="search_report();" placeholder="Select Date" >
											<span class="error"><?php if(isset($_SESSION['error']['class1'])){ echo $_SESSION['error']['class1'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row">
										<label class="col-sm-4 col-form-label">Class</label>
										<div class="col-sm-8">
											<select class="form-control" name="class" value=""  id="class" onchange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												$row = $database->query("select * from class where estatus='1' and  id='".$_REQUEST['class']."'");
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['class'])) { if($_REQUEST['class']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['class'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
										</div>
									</div>
								</div>
								<?php
								if($_REQUEST['subject']!=''){ ?>
									<div class="col-lg-4 col-md-4 col-sm-4">
										<div class="form-group row">
											<label class="col-sm-4 col-form-label">Subject</label>
											<div class="col-sm-8">
												<select class="form-control" name="subject" value=""  id="subject" onchange="search_report();">
													<option value=''>-- Select --</option>
													<?php
													$row = $database->query("select * from subject where estatus='1' and  id='".$_REQUEST['subject']."'");
													while($data = mysqli_fetch_array($row))
													{
														?>
													<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['subject'])) { if($_REQUEST['subject']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['subject'];?></option>
													<?php
													}
													?>
												</select>
												<span class="error"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
											</div>
										</div>
									</div>
								<?php } ?>
								
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Chapter</label>
										<div class="col-sm-8">
										<select class="form-control" name="chapter" value=""   id="chapter" onChange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												if($_REQUEST['chapter']!=''){
													$row = $database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."' and class='".$_REQUEST['class']."' and  id='".$_REQUEST['chapter']."'");
													
												}else{
													$row = $database->query("select * from chapter where estatus='1' and subject='".$_REQUEST['subject']."' and class='".$_REQUEST['class']."'");
												}
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['chapter'])) { if($_REQUEST['chapter']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['chapter'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Topic</label>
										<div class="col-sm-8">
										<select class="form-control" name="topic" value=""   id="topic" onChange="search_report();">
												<option value=''>-- Select --</option>
												<?php
												if($_REQUEST['topic']!=''){
													$row = $database->query("select * from topic where estatus='1' and subject='".$_REQUEST['subject']."' and class='".$_REQUEST['class']."' and  chapter='".$_REQUEST['chapter']."' and  id='".$_REQUEST['topic']."'");
													
												}else{
													$row = $database->query("select * from topic where estatus='1' and subject='".$_REQUEST['subject']."' and class='".$_REQUEST['class']."' and  chapter='".$_REQUEST['chapter']."'");
												}
												while($data = mysqli_fetch_array($row))
												{
													?>
												<option value="<?php echo $data['id'];?>" <?php if(isset($_REQUEST['topic'])) { if($_REQUEST['topic']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['topic'];?></option>
												<?php
												}
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['topic'])){ echo $_SESSION['error']['topic'];}?></span>
										</div>
									</div>
								</div>
								
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Status</label>
										<div class="col-sm-8">
										<select class="form-control" name="status" value=""   id="status" onChange="search_report();">
												<option value=''>-- Select --</option>
												<option value='0' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='0') { echo 'selected="selected"'; }  } ?>>Pending</option>
												<option value='1' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='1') { echo 'selected="selected"'; }  } ?>>Verified</option>
												<option value='2' <?php if(isset($_REQUEST['status'])) { if($_REQUEST['status']=='2') { echo 'selected="selected"'; }  } ?>>Rejected</option>
												?>
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['status'])){ echo $_SESSION['error']['status'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Explanation</label>
										<div class="col-sm-8">
										<select class="form-control" name="explanation" value=""   id="explanation" onChange="search_report();">
												<option value=''>-- Select --</option>
												<option value='1' <?php if(isset($_REQUEST['explanation'])) { if($_REQUEST['explanation']=='1') { echo 'selected="selected"'; }  } ?>>With Explanation</option>
												<option value='2' <?php if(isset($_REQUEST['explanation'])) { if($_REQUEST['explanation']=='2') { echo 'selected="selected"'; }  } ?>>With Out Explanation</option>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['explanation'])){ echo $_SESSION['error']['explanation'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Usage Set</label>
										<div class="col-sm-8">
										<select class="form-control" name="usageset" value=""   id="usageset" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$uageset = $database->query("SELECT * FROM question_useset WHERE estatus='1' "); 
												
												while($datauageset = mysqli_fetch_array($uageset))
												{
													?>
												<option value="<?php echo $datauageset['id'];?>" <?php if(isset($_REQUEST['usageset'])) { if($_REQUEST['usageset']==$datauageset['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datauageset['usageset'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Complexity</label>
										<div class="col-sm-8">
										<select class="form-control" name="complexity" value=""   id="complexity" onChange="search_report();">
											<option value=''>-- Select --</option>
												<?php
												$dcomplexity = $database->query("SELECT * FROM complexity WHERE estatus='1' "); 
												
												while($datacomplexity = mysqli_fetch_array($dcomplexity))
												{
													?>
												<option value="<?php echo $datacomplexity['id'];?>" <?php if(isset($_REQUEST['complexity'])) { if($_REQUEST['complexity']==$datacomplexity['id']) { echo 'selected="selected"'; }  } ?>><?php echo $datacomplexity['complexity'];?></option>
												<?php
												}
												?>
												
											</select>
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-4">
									<div class="form-group row" id="ccc1">
										<label class="col-sm-4 col-form-label">Question Id</label>
										<div class="col-sm-8">
										
											<input type="text" name="question_id" id="question_id" class="form-control" onChange="search_report();" onkeypress="return onlyNos(event,this);" value="<?php echo $_REQUEST['question_id'];?>" >
											<span class="error"><?php if(isset($_SESSION['error']['usageset'])){ echo $_SESSION['error']['usageset'];}?></span>
										</div>
									</div>
								</div>
								<?php
								$data_selu =  $database->query("select * from users where username='".$session->username."'");
								$rowu = mysqli_fetch_array($data_selu);
								$limit=25;
								if(isset($_GET['page']))
								{
									$start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
									$page=$_GET['page'];
								}
								else
								{
									$start = 0;      //if no page var is given, set start to 0
									$page=0;
								}
								
								
								//Search Form

								$tableName = 'createquestion';
								
								if(isset($_GET['keyword'])){
								}
								
								if(isset($_REQUEST['class'])){
									if($_REQUEST['class']!=""){
										$class=" AND FIND_IN_SET(".$_REQUEST['class'].",class) >0 ";
									} else {
										$class="";
									}
								}else{
									$class="";
								}
								
								
								if(isset($_REQUEST['explanation'])){
									if($_REQUEST['explanation']=="1"){
										$explanation=" AND (explanation!='%3Cp%3ENA%3C%2Fp%3E' OR explanation!='' )";
									} else if($_REQUEST['explanation']=='2'){
										$explanation=" AND (explanation='%3Cp%3ENA%3C%2Fp%3E'  OR explanation='')";
									}else {
										$explanation="";
									}
								}else{
									$explanation="";
								}
								if(isset($_REQUEST['complexity'])){
									if($_REQUEST['complexity']!=""){
										$complexity=" AND complexity ='".$_REQUEST['complexity']."' and question_theory!=''";
									} else {
										$complexity="";
									}
								}else{
									$complexity="";
								}
								if(isset($_REQUEST['usageset'])){
									if($_REQUEST['usageset']!=""){
										$usageset=" AND usageset ='".$_REQUEST['usageset']."'";
									} else {
										$usageset="";
									}
								}else{
									$usageset="";
								}
								if(isset($_REQUEST['inputquestion'])){
									if($_REQUEST['inputquestion']!=""){
										$inputquestion=" AND inputquestion ='".$_REQUEST['inputquestion']."'";
									} else {
										$inputquestion="";
									}
								}else{
									$inputquestion="";
								}
								
								if(isset($_REQUEST['question_id'])){
									if($_REQUEST['question_id']!=""){
										$question_id=" AND id='".$_REQUEST['question_id']."'";
									} else {
										$question_id="";
									}
								}else{
									$question_id="";
								}
								if(isset($_REQUEST['date'])){
									if($_REQUEST['date']!=""){
										$date=" and timestamp between ".strtotime($_REQUEST['date']. ' 00:00:01')." and ".strtotime($_REQUEST['date']. ' 23:59:59')."";
									} else {
										$date="";
									}
								}else{
									$date="";
								}
								if(isset($_REQUEST['status'])){
									if($_REQUEST['status']!=""){
										$status=" AND vstatus1='".$_REQUEST['status']."'";
									} else {
										$status="";
									}
								}else{
									$status="";
								}
								if(isset($_REQUEST['subject'])){
									if($_REQUEST['subject']!=""){
										$subject=" AND subject  ='".$_REQUEST['subject']."'";
									} else {
										$subject="";
									}
									}else{
										$subject="";
									}
									if($_REQUEST['chapter']!=''){
										$chapter=" AND chapter IN (".$_REQUEST['chapter'].")";
									}else{
										$chapter="";
									}
									
									if(isset($_REQUEST['topic'])){
										if($_REQUEST['topic']!=""){
											$topic=" AND topic IN (".$_REQUEST['topic'].")";
										} else {
											$topic="";
										}
									}else{
										$topic="";
									}

									
								
									
									$cond = $date.$class.$subject.$chapter.$status.$explanation.$complexity.$usageset.$question_id.$topic.$question_theory.$inputquestion;
									if($_REQUEST['totalquestions']=='All'){
										$condition="estatus='1'";
									}else{
										$condition="estatus='1' and vstatus1='1' and complexity!='0' ";
									}
									
									
									if(strlen($condition) > 0){
										$condition ="where ".$condition.$cond;
									}
									
									
									$pagination = $session->showPagination(SECURE_PATH."superdashboard/process1.php?viewDetails2=1&class1=".$_REQUEST['class1']."&subject=".$_REQUEST['subject']."&chapter=".$_REQUEST['chapter']."&topic=".$_REQUEST['topic']."&date=".$_REQUEST['date']."&explanation=".$_REQUEST['explanation']."&complexity=".$_REQUEST['complexity']."&usageset=".$_REQUEST['usageset']."&totalquestions=".$_REQUEST['totalquestions']."&question_id=".$_REQUEST['question_id']."&",$tableName,$start,$limit,$page,$condition);
									$q = "SELECT * FROM $tableName ".$condition." ";
									$result_sel = $database->query($q);
									$numres = mysqli_num_rows($result_sel);
									$query = "SELECT * FROM $tableName ".$condition."  order by timestamp DESC LIMIT ".$start.",".$limit." ";
									$data_sel = $database->query($query);
								
								if(($start+$limit) > $numres){
									$onpage = $numres;
									}
									else{
									$onpage = $start+$limit;
									}
								if($numres > 0){
									?>
								</div>
										<div class="text-right ml-auto"><p class"mb-3">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p></div>
								<table class="table table-bordered table-responsive dashboard-table mb-0" >
									<thead class="thead-light">
										<tr>
											<th scope="col">Sr.No.</th>
											<th scope="col">Question Id</th>
											<th scope="col">Date</th>
											<th scope="col">Question Title</th>
											<th scope="col">Class</th>
											<th scope="col">Exam</th>
											<th scope="col">Subject</th>
											<th scope="col">Chapter</th>
											<th scope="col">Status</th>
											<th scope="col">Actions</th>
										</tr>
									</thead>
									<?php
									if(isset($_GET['page'])){
											
									if($_GET['page']==1)
									$i=1;
									else
									$i=(($_GET['page']-1)*($limit))+1;
									}else $i=1;
									$k = 1;
									
									while($value = mysqli_fetch_array($data_sel)){

										//$date=date("d-m-Y  H:i:s ", $value["time"]);
											//$date1 = new DateTime("@$dtime");
											$dt1=date("d-m-Y  H:i:s ", $value["timestamp"]);
											$value['class']=rtrim($value['class'],',');
											$value['exam']=rtrim($value['exam'],",");
											  if($value['vstatus1']==1){
												$status='<i class="fas fa-check-circle text-success pr-1"></i>Verified';
											  }
											  elseif($value['vstatus1']==2) {
												$status='<i class="fa fa-times-circle text-danger pr-1"></i>Rejected';

											  }
											  else{
												$status='<i class="fas fa-pause-circle text-warning pr-1"></i>Pending';
											  }
											$mn=1;
											$exam='';
											$zSqlexam = $database->query("SELECT * FROM exam WHERE estatus='1' and id IN(".$value['exam'].")"); 
											while($rowexam=mysqli_fetch_array($zSqlexam)){
												$exam .= $rowexam['exam'].",";
												$mn++;
											}
											if($value['class']=='1'){
											$class='XI';
											}else if($value['class']=='2'){
												$class='XII';
											}else  if($value['class']=='1,2'){
												$class='XI,XII';
											}
											$chapter2 ='';
											$m=1;
											$zSqll3 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and id IN(".$value['chapter'].")"); 
											
												while($row3=mysqli_fetch_array($zSqll3)){
													$chapter2 .= $row3['chapter'].",";
													$m++;
												}
											$topic ='';
											$k=1;
											$zSql1 = $database->query("SELECT * FROM topic WHERE estatus='1' and id IN(".$value['topic'].")"); 
											while($row1=mysqli_fetch_array($zSql1)){
												$topic .= $row1['topic'].",";
												$k++;
											}
											if($value['list1type']=='roman'){
												$list1type="upper-roman";
											}else if($value['list1type']=='alphabets'){
												$list1type="upper-alpha";
											}else if($value['list1type']=='numbers'){
												$list1type="decimal";
											}else{
												$list1type="upper-alpha";
											}
											if($value['list2type']=='roman'){
												$list2type="upper-roman";
											}else if($value['list2type']=='alphabets'){
												$list2type="upper-alpha";
											}else if($value['list2type']=='numbers'){
												$list2type="decimal";
											}else{
												$list2type="upper-alpha";
											}
										if($value['vstatus1']==1){
											$status='<i class="fas fa-check-circle text-success pr-1"></i>Verified';
										}
										elseif($value['vstatus1']==2) {
											$status='<i class="fa fa-times-circle text-danger pr-1"></i>Rejected';

										}
										else{
											$status='<i class="fas fa-pause-circle text-warning pr-1"></i>Pending';
										}
										$question2=urldecode($value['question']);
										$question=preg_replace('/<table>.*<\/table>/', '', $question2);
												
										?>
										<tr>
											<td class="text-left"><?php echo $i; ?></td>
											<td class="text-left" ><?php echo $value['id']; ?></td>
											<td class="text-left" ><?php echo $dt1; ?></td>
											<?php
											if($value['qtype']=='7'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left" width="33%"><div    id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='5'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left" width="33%"><div    id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='8'){
												$question=urldecode($value['question']);
											?>
												<td class="text-left" width="33%"><div    id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}else if($value['qtype']=='3'){
											$list1='';
											$list2='';
											$obj=json_decode($value['question'],true);
											foreach($obj as $rowpost2)
											{
												if(strlen($rowpost2)>0)
												{
													$rowpost = explode("_",$rowpost2);
													$list1.=$rowpost[0]."_";
													$list2.=$rowpost[1]."_";
												}
											}
											$qlist1 = explode("_",$list1);
											$qlist2 = explode("_",$list2);
										?>
											<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none">
												<div class="d-flex justify-content-between">
												<div>
												<h5>List1</h5>
												<ul style="list-style-type: <?php echo $list1type; ?>;padding-left:14px">
												<?php
												foreach($obj as $qqlist)
												{
												
													if(strlen($qqlist['qlist1'])>0)
													{
														echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
													}
												}
												?>
												
												</ul>
												</div>
												<div>
												<h5>List2</h5>
												<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
												<?php
												foreach($obj as $qqlist1)
												{
												
													if(strlen($qqlist1['qlist2'])>0)
													{
														echo '<li>'.urldecode($qqlist1['qlist2']).'</li>';
													}
												}
												?>
												</ol>
												</div>
												</div>
												</div>
											</td>
											<?php
											}else if($value['qtype']=='9'){
												$obj=json_decode($value['question'],true);
												?>	
												<td class="text-left imagewrapper" width="33%"><div   id="tableborder-none">
													<div class="d-flex justify-content-between">
													<div>
													<h5>List1</h5>
													<ul style="list-style-type: upper-alpha;padding-left:14px">
													<?php
													foreach($obj as $qqlist)
													{
													
														if(strlen($qqlist['qlist1'])>0)
														{
															echo '<li>'.urldecode($qqlist['qlist1']).'</li>';
														}
													}
													?>
													
													</ul>
													</div>
													<div>
													<h5>List2</h5>
													<ol style="list-style-type: <?php echo $list2type; ?>;padding: 0px 13px;">
													<?php
													foreach($obj as $qqlist2)
													{
													
														if(strlen($qqlist2['qlist2'])>0)
														{
															echo '<li>'.urldecode($qqlist2['qlist2']).'</li>';
														}
													}
													?>
													</ol>
													</div>
													</div>
													</div>
												</td>
												<?php
											}else{
												$question=urldecode($value['question']);
											?>
												<td class="text-left imagewrapper" width="33%"><div  id="tableborder-none"><?php echo $question; ?></div></td>
											<?php
											}
											?>
											
											
											<td class="text-left"><?php echo $class; ?></td>
											<td class="text-left"><?php echo $exam; ?></td>
											<td class="text-left"><?php echo $database->get_name('subject','id',$value['subject'],'subject'); ?></td>
											<td class="text-left"><?php echo rtrim($chapter2,','); ?></td>
											<td class="text-left" ><?php echo $status; ?></td>
											<td class="text-left review-buttons" nowrap>
												<a class="text-success" style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#reviewexampleModal" onClick="setStateGet('reviewviewDetails','<?php echo SECURE_PATH;?>superdashboard/process3.php','viewDetails=1&id=<?php echo $value['id'];?>');"><i class="fa fa-eye"  ></i></a> &nbsp;
												<a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process2.php','viewDetails=1&class=<?php echo rtrim($value['id'],",");?>&subject=<?php echo $value['subject'];?>&chapter=<?php echo rtrim($value['chapter'],","); ?>&topic=<?php echo rtrim($value['topic'],","); ?>&questionid=<?php echo $value['id']; ?>&type=3')"><i class="fa fa-edit" aria-hidden="true"></i></a>
											</td>
										</tr>

										
										<?php 
											$i++;
											}
											?>
									</tbody>
							</table>
							<div class="my-3 text-right footer-pagination chapter-review ml-auto">
									
									<?php echo $pagination ;?>
											
								</div>
					</div>
				</div>
			</div>
	</section>                              
		
<?php 
	}
}

 ?>
 </div>
 <?php echo $session->commonFooterAdminJS(); ?>
 <script>

    $(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});
	</script>