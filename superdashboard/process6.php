<?php 
include('../include/session.php');
error_reporting(0);
if(isset($_REQUEST['questionattribute'])){

    ?>
   <div id="accordion">
   <div class="card-header p-2" id="questionattribute">
        <div class="row">
            <div class="col-sm-12">
                
            
                    <h5 class="mb-0">
                                    <a href='' class="d-flex justify-content-between align-items-center"
                        data-toggle="collapse" data-target="#attributediv" aria-expanded="true"
                        aria-controls="attributediv" >
                            <h5 class="card-title mb-0"><b>Attribute Overview</b></h5>
                            <h5 class="mb-0"><i class="far fa-caret-square-down"></i></h5>
                        </a>
                    </h5>
                
                                                                
            </div>
        </div>
    </div>

    <div id="attributediv" class="collapse show" aria-labelledby="questionattribute" data-parent="#accordion">
    <div class="">
    <?php
    
    if(isset($_REQUEST['class'])){
            if($_REQUEST['class']!=""){
                $class=" AND   FIND_IN_SET(".$_REQUEST['class'].",class) > 0 ";
            } else {
                $class="";
            }
    }else{
        $class="";
    }
    if(isset($_REQUEST['subject'])){
        if($_REQUEST['subject']!=""){
            $subject=" AND   FIND_IN_SET(".$_REQUEST['subject'].",subject) > 0 ";
        } else {
            $subject="";
        }
    }else{
        $subject="";
    }
    if(isset($_REQUEST['chapter'])){
        if($_REQUEST['chapter']!=""){
            $chapter=" AND   FIND_IN_SET(".$_REQUEST['chapter'].",chapter) > 0 ";
        } else {
            $chapter="";
        }
    }else{
        $chapter="";
    }
    if(isset($_REQUEST['exam'])){
        if($_REQUEST['exam']!=""){
               
            if($_REQUEST['exam']=="1"){
                $exam= " AND FIND_IN_SET(1,exam) > 0 ";
            }else if($_REQUEST['exam']=="2"){
                $exam= " AND FIND_IN_SET(2,exam) > 0 and pexamtype in (1)   ";
            }else if($_REQUEST['exam']=="3"){
                $exam= " AND FIND_IN_SET(2,exam) > 0 and pexamtype in (2) ";
            }
        }
    }else{
        $exam="";
    }
    if($_REQUEST['totalquestions']=='All'){
        $con=$class.$subject.$chapter.$exam;

    }else if($_REQUEST['totalquestions']=='notverified'){
        $vstatus=" and vstatus1='0'";
        $con=$class.$subject.$chapter.$exam.$vstatus;

    }else if($_REQUEST['totalquestions']=='verified'){
        $vstatus=" and vstatus1='1'";
        $con=$class.$subject.$chapter.$exam.$vstatus;
        
    }else{
        $vstatus=" and vstatus1='1'";
        $con=$class.$subject.$chapter.$exam.$vstatus;

    }
   
        ?>
     
							<div class="row">
								<div class="col-sm-3">
									<h6 class="text-uppercase font-weight-600 mt-1 selectedNameCls">Question Usage Set Wise Details</h6>
								</div>
								<div class="col-sm-9">
									
									
									<ul class="subject-wise-analysis list-inline float-right switch-custom questionWiseCls mb-0 attribute nav nav-pills attribute" id="pills-tab" role="tablist">
										<li>
											<a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home"
											   role="tab" aria-controls="pills-home" aria-selected="true"><b>Question Usage Set Wise</b></a>
										</li>
										<li>
											<a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile"
											   role="tab" aria-controls="pills-profile" aria-selected="false"><b>Question Type Wise</b></a>
										</li>
										<li>
											<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact"
											   role="tab" aria-controls="pills-contact" aria-selected="false"><b>Chapter Wise</b></a>
										</li>
										
									</ul>
								</div>
							</div>
						</div>
						<div class="card-body p-2">
							<div class="tab-content" id="pills-tabContent">
								<?php
								if($_REQUEST['exam']=='1'){
									$cols="4";
								}else if($_REQUEST['exam']=='2'){
									$cols="3";
								}else if($_REQUEST['exam']=='1,2'){
									$cols="7";
								}else{
									$cols="7";
								}
								?>
							<div class="table-responsive">
										 <div class="tab-content" id="pills-tabContent">
											<?php
												
			
			
												if($_REQUEST['exam']=='1'){
													$cols="4";
												}else if($_REQUEST['exam']=='2'){
													$cols="3";
												}else if($_REQUEST['exam']=='1,2'){
													$cols="7";
												}else{
													$cols="7";
												}
												?>
												<div class="tab-pane fade show active" id="pills-home" role="tabpanel"
													 aria-labelledby="pills-home-tab">
													<table class="table table-bordered table_custom">
														<thead>
															<tr>
																<th rowspan="2"></th>
																<th rowspan="2">View</th>
																<th rowspan="2">Review</th>
																<th rowspan="2">OverAll</th>
																<th colspan="2">Total</th>
																<?php
																$data1 = $database->query("select * from complexity where estatus=1 ");
																while($data1row=mysqli_fetch_array($data1)){
				  
																	?>
																	<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
																	</th>
																<?php } ?>
															</tr>
															<tr>
																<?php
																$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata=mysqli_fetch_array($seldata)){
																?>
																	<th>
																		<?php echo $rowdata['question_theory']; ?>
																	</th>
																<?php
																	 }
																$sell2=$database->query("select * from complexity where estatus='1'");
																while($rowll2=mysqli_fetch_array($sell2)){
																	$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata=mysqli_fetch_array($seldata)){
																?>
																	<th >
																		<?php echo $rowdata['question_theory']; ?>
																	</th>
																<?php
																	 }
																}
																?>
															</tr>
															
														</thead>
														<tbody>
															<?php
															$l=1;
															$overall=0;
															$totalcnt=0;
															
															$sell2=$database->query("select * from question_useset where estatus='1'");
															while($rowll2=mysqli_fetch_array($sell2)){
																$j=1;
																$count=0;
																$count1=0;
																$tcount=0;
																$sell12l=$database->query("select * from complexity where estatus='1'");
																while($rowll12=mysqli_fetch_array($sell12l)){
																	$seldata=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata11=mysqli_fetch_array($seldata)){
																		 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$rowdata11['id']."' and usageset='".$rowll2['id']."'".$con." ");
																		
																		
																		 $todayttocount=mysqli_fetch_array($todata);
																		
																		 if($rowdata11['id']=='1'){
																			$count=$count+$todayttocount[0];
																		 }else{
																			 $count1=$count1+$todayttocount[0];
																		 }
																		  $tcount=$tcount+$todayttocount[0];
																	}
																}
																
																?>
																<td><?php echo $rowll2['usageset']; ?></td>
																<td class="superdashboard-buttons">
																	<a style="cursor:pointer;"  title="Print" target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>superdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&complexity=&usageset=<?php echo $rowll2['id'];?>");' ><i class="fa fa-eye"  ></i></a>
																</td>
																 <td class="superdashboard-buttons">&nbsp;</td> 
																 <td>
																	<?php echo $tcount; ?>
																</td>
																		
																<?php
																$seldata=$database->query("select * from question_theory where estatus='1'");
																while($rowdata=mysqli_fetch_array($seldata)){
																	if($rowdata['id']==1){
																?>
																	<td>
																		<?php echo $count; ?>
																	</td>
																<?php
																	}else if($rowdata['id']==2){
																	?>
																	<td>
																		<?php echo $count1; ?>
																	</td>
																	<?php
																	}
																}
																
																$j=1;
																$s1=$database->query("select * from complexity where estatus='1'");
																while($rs1=mysqli_fetch_array($s1)){
																	$seldata1=$database->query("select * from question_theory where estatus='1'");
																	while($rowdata1=mysqli_fetch_array($seldata1)){
																		 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity!='' and question_theory='".$rowdata1['id']."' and complexity='".$rs1['id']."' and usageset='".$rowll2['id']."'".$con." ");
																		 $todayttocount=mysqli_fetch_array($todata);
																		
																		
																?>
																	<td>
																	  <?php  if($todayttocount[0]!=''){ echo $todayttocount[0]; } else{ echo '0'; } ?>
																	</td>
																<?php
																	 }
																	
																
																	
																}
															   ?>
																
															</tr>
															<?php
																$overall=$overall+$tcount;
																$totalcnt=$totalcnt+$tcount;
																//$easycnt=$easycnt+$todayttocount1;
															$l++;
														}
														?>
														<tr>
															<td colspan='3' align='right'>Total</td>
															<td><?php echo $overall; ?></td>
															<td colspan='2'><?php echo $overall; ?></td>
															<?php 
															
															
																
																$s1=$database->query("select * from complexity where estatus='1'");
																while($rs1=mysqli_fetch_array($s1)){
																	$easycnt=0;
															$moderatecnt=0;
															$difficultcnt=0;
															$highdifficultcnt=0;
																	$sell2=$database->query("select * from question_useset where estatus='1'");
																	while($rowll2=mysqli_fetch_array($sell2)){ 
																		$todata1= $database->query("select count(id) as cnt from createquestion where estatus=1 and complexity!='' and question_theory!='' and complexity='".$rs1['id']."' and usageset='".$rowll2['id']."'".$con." ");
																		$todayttocount1=mysqli_fetch_array($todata1);
																		$easycnt=$easycnt+$todayttocount1['cnt'];
																		
																	}
																	?>
																		<td colspan='2'><?php echo $easycnt; ?></td>
																		<?php
																}
																?>
																
																
															
																
														</tr>
														
														</tbody>

													</table>
												</div>

												<div class="tab-pane fade" id="pills-profile" role="tabpanel"
													 aria-labelledby="pills-profile-tab">
													<table class="table table-bordered table_custom">
						  										<thead>
						  											<tr>
						  												<th rowspan="2"></th>
						  												<th rowspan="2">View</th>
						  												<th rowspan="2">Review</th>
						  												<th rowspan="2">OverAll</th>
						  												<th colspan="2">Total</th>
						  												<?php
						  												$data1 = $database->query("select * from complexity where estatus=1 ");
						  												while($data1row=mysqli_fetch_array($data1)){
						  
						  													?>
						  													<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
						  													</th>
						  												<?php } ?>
						  											</tr>
						  											<tr>
						  												<?php
						  												$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th>
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												$sell2=$database->query("select * from complexity where estatus='1'");
						  												while($rowll2=mysqli_fetch_array($sell2)){
						  													$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th >
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												}
						  												?>
						  											</tr>
						  											
						  										</thead>
						  										<tbody>
						  											
						  												<?php
																	$l=1;
																	$overall1=0;
																	$totalcnt1=0;
																	$sell2=$database->query("select * from questiontype where estatus='1'");
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$qcount=0;
																		$qcount1=0;
																		$qtcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$seldataq=$database->query("select * from question_theory where estatus='1'");
																			while($qrowdata=mysqli_fetch_array($seldataq)){
																			
																				 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."'  and  FIND_IN_set(".$rowll2['id'].",inputquestion) > 0 and question_theory='".$qrowdata['id']."' ".$con."");
																				
																				$todayttocount=mysqli_fetch_array($todata);
																				if($qrowdata['id']=='1'){
																					$qcount=$qcount+$todayttocount[0];
																				 }else{
																					 $qcount1=$qcount1+$todayttocount[0];
																				 }
																				 $qtcount=$qtcount+$todayttocount[0];
																				
																			}
																		}
																		
																		?>
			
																		<tr>
																			
																			<td><?php echo $rowll2['questiontype']; ?></td>
																			<td class="superdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print"  target='_blank' onClick='window.open("<?php echo SECURE_PATH;?>superdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_REQUEST['chapter'];?>&inputquestion=<?php echo $rowll2['id'];?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			
																			<td class="superdashboard-buttons">&nbsp;</td>
																			 <td>
																				<?php  if($qtcount!=''){ echo $qtcount; } else{ echo '0'; } ?>
																			</td>
																					
																			<?php
																			$seldata=$database->query("select * from question_theory where estatus='1'");
																			while($rowdata=mysqli_fetch_array($seldata)){
																				if($rowdata['id']==1){
																			?>
																				<td>
																					 <?php  if($qcount!=''){ echo $qcount; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				}else if($rowdata['id']==2){
																				?>
																				<td>
																					 <?php  if($qcount1!=''){ echo $qcount1; } else{ echo '0'; } ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			$sell12=$database->query("select * from complexity where estatus='1'");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$seldata=$database->query("select * from question_theory where estatus='1'");
																				while($rowdata=mysqli_fetch_array($seldata)){
																					  $todata1 = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and  FIND_IN_set(".$rowll2['id'].",inputquestion) > 0  and question_theory='".$rowdata['id']."'".$con."");
			
																					 $todayttocount1=mysqli_fetch_array($todata1);
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																			$overall1=$overall1+$qtcount;
																			$totalcnt1=$totalcnt1+$qtcount;
																		$l++;
																	}
																	?>
																	<tr>
																		<td colspan='3' align='right'>Total</td>
																		<td><?php echo $overall1; ?></td>
																		<td colspan='2'><?php echo $overall1; ?></td>
																		<?php 
																		
																		
																			
																			$s1=$database->query("select * from complexity where estatus='1'");
																			while($rs1=mysqli_fetch_array($s1)){
																				$easycnt1=0;
																		
																				$sell2=$database->query("select * from questiontype where estatus='1'");
																				while($rowll2=mysqli_fetch_array($sell2)){ 
																					//echo "select count(id) as cnt from createquestion where estatus=1 and complexity!='' and inputquestion!='' and complexity='".$rs1['id']."' and usageset='".$rowll2['id']."'".$con." ";
																					$todata1= $database->query("select count(id) as cnt from createquestion where estatus=1 and complexity!='' and find_in_set(".$rowll2['id'].",inputquestion) and complexity='".$rs1['id']."' ".$con." ");
																					$todayttocount1=mysqli_fetch_array($todata1);
																					$easycnt1=$easycnt1+$todayttocount1['cnt'];
																					
																				}
																				?>
																					<td colspan='2'><?php echo $easycnt1; ?></td>
																					<?php
																			}
																			?>
																	</tr>
						  										</tbody>
						  									</table>
												</div>
												<div class="tab-pane fade" id="pills-contact" role="tabpanel"
													 aria-labelledby="pills-contact-tab">
													<table class="table table-bordered table_custom">
						  										<thead>
						  											<tr>
						  												<th rowspan="2"></th>
						  												<th rowspan="2">View</th>
						  												<th rowspan="2">Review</th>
						  												<th rowspan="2">OverAll</th>
						  												<th colspan="2">Total</th>
						  												<?php
						  												$data1 = $database->query("select * from complexity where estatus=1 ");
						  												while($data1row=mysqli_fetch_array($data1)){
						  
						  													?>
						  													<th colspan="2"><?php echo $data1row['complexity']; ?></h6>
						  													</th>
						  												<?php } ?>
						  											</tr>
						  											<tr>
						  												<?php
						  												$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th>
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												$sell2=$database->query("select * from complexity where estatus='1'");
						  												while($rowll2=mysqli_fetch_array($sell2)){
						  													$seldata=$database->query("select * from question_theory where estatus='1'");
						  													while($rowdata=mysqli_fetch_array($seldata)){
						  												?>
						  													<th >
						  														<?php echo $rowdata['question_theory']; ?>
						  													</th>
						  												<?php
						  													 }
						  												}
						  												?>
						  											</tr>
						  											
						  										</thead>
						  										<tbody>
						  											<?php
																if(isset($_POST['chapter'])){
																	
																	if($_POST['chapter']!=''){
																		$l=1;
																		$overall4=0;
																		$totalcnt4=0;
																		$sell2=$database->query("select * from topic where estatus='1' and chapter='".$_POST['chapter']."'");
																		while($rowll2=mysqli_fetch_array($sell2)){
																			$j=1;
																			$count=0;
																			$count1=0;
																			$tcount=0;
																			$sel2=$database->query("select * from complexity where estatus='1'");
																			while($rowl2=mysqli_fetch_array($sel2)){
																				$sel12=$database->query("SELECT * FROM `question_theory` ");
																				while($rowlll2=mysqli_fetch_array($sel12)){
																				
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."' and question_theory='".$rowlll2['id']."' and chapter in (".$_POST['chapter'].") and topic='".$rowll2['id']."'".$con." ");
																					
																					
																					 $todayttocount=mysqli_fetch_array($todata);
																					 if($rowlll2['id']=='1'){
																						$count=$count+$todayttocount[0];
																					 }else{
																						 $count1=$count1+$todayttocount[0];
																					 }
																					 $tcount=$tcount+$todayttocount[0];
																					
																				}
																			}
																			$selll=$database->query("select count(*) As count from review_chapters where estatus='1' and chapter = '".$_POST['chapter']."' and class='".$_REQUEST['class']."'  group by chapter");
																			$rowlll=mysqli_fetch_array($selll);
																			
																			?>
			
																			<tr>
																				<td> 
																					<?php echo $rowll2['topic']; ?>
																				</td>
																				<td class="superdashboard-buttons">
																					<a style="cursor:pointer;"  title="Print" target='_blank'  onClick='window.open("<?php echo SECURE_PATH;?>superdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_POST['chapter'];?>&topic=<?php echo $rowll2['id'];?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																				</td>
																				
																				<?php
																			if($rowlll[0]>0){
																			?>
																				<td class="superdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process4.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $_POST['chapter'];?>&type=1')"><i class="fa fa-eye"  ></i></p>
			
																				</td>
																			<?php }else{ 
																				?>
																				<td class="superdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $row['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=2')"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;</p>
																				</td>
																			<?php } ?>
																				<?php if($_REQUEST['totalquestions']=='All'){
																				
																					 $todata = $database->query("select count(*) from createquestion where estatus=1  and chapter='".$_POST['chapter']."'  and topic='".$rowll2['id']."'".$con." ");
																					
																					 $todayttocount=mysqli_fetch_array($todata);
																					 $tcount= $todayttocount[0];
																				?>
																					 <td>
																						<?php echo  $tcount; ?>
																					</td>
																				<?php }else{ ?>
																					<td>
																					<?php echo  $tcount; ?>
																					</td>
																				<?php }?>
																						
																				<?php
																				$se2=$database->query("SELECT * FROM `question_theory` ");
																				while($ro2=mysqli_fetch_array($se2)){
																				
																					if($ro2['id']==1){
																				?>
																					<td>
																						<?php echo $count; ?>
																					</td>
																				<?php
																					}else if($ro2['id']==2){
																					?>
																					<td>
																						<?php echo $count1; ?>
																					</td>
																					<?php
																					}
																				}
																				
																				
																				$j=1;
																				
																				$sell12=$database->query("select * from complexity where estatus='1' ");
																				while($rowll12=mysqli_fetch_array($sell12)){
																					$se2=$database->query("SELECT * FROM `question_theory` ");
																					while($ro2=mysqli_fetch_array($se2)){
																						
																						$todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$ro2['id']."'  and find_in_set(".$rowll2['id'].",topic)>0 ".$con." ");
																						
																						 $todayttocount1=mysqli_fetch_array($todata);
																						
																				?>
																					<td>
																					 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																					</td>
																				<?php
																					 }
																				}
																				?>
																				
																			</tr>
																			<?php
																			$l++;
																			$overall4=$overall4+$tcount;
																			$totalcnt4=$totalcnt4+$tcount;
																		}
																		?>
																			<tr>
																		<td colspan='3' align='right'>Total</td>
																		<td><?php echo $overall4; ?></td>
																		<td colspan='2'><?php echo $overall4; ?></td>
																		<?php 
																		
																		
																			
																			$s1=$database->query("select * from complexity where estatus='1'");
																			while($rs1=mysqli_fetch_array($s1)){
																				$easycnt2=0;
																		
																				$sell2=$database->query("select * from topic where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
																				while($rowll2=mysqli_fetch_array($sell2)){ 
																					
																					$todata1= $database->query("select count(id) as cnt from createquestion where estatus=1 and complexity!='' and topic!='' and complexity='".$rs1['id']."' and find_in_set(".$rowll2['id'].",topic>0  ".$con." ");
																					$todayttocount1=mysqli_fetch_array($todata1);
																					$easycnt2=$easycnt2+$todayttocount1['cnt'];
																					
																				}
																				?>
																					<td colspan='2'><?php echo $easycnt2; ?></td>
																					<?php
																			}
																			?>
																	</tr>
																		<?php

																	}else{
																		$l=1;
																		$overall3=0;
																	$totalcnt3=0;
																	$sell2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
																	
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$count=0;
																		$count1=0;
																		$tcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$sel12=$database->query("SELECT * FROM `question_theory`");
																			while($rowlll2=mysqli_fetch_array($sel12)){
																				
																				  $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."'  and chapter in (".$rowll2['id'].") and question_theory='".$rowlll2['id']."'".$con." ");
																				
																				 $todayttocount=mysqli_fetch_array($todata);
																				if($rowlll2['id']=='1'){
																					$count=$count+$todayttocount[0];
																				 }else{
																					 $count1=$count1+$todayttocount[0];
																				 }
																				 $tcount=$tcount+$todayttocount[0];
																				
																			}
																		}
																		$selll=$database->query("select count(*) As count from review_chapters where estatus='1' and chapter = '".$rowll2['id']."' and class='".$_REQUEST['class']."' and subject='".$rowe['subject']."' group by chapter");
																		
																		$rowlll=mysqli_fetch_array($selll);
			
																		
																		?>
			
																		<tr>
																			<td>
																				<a   onClick="setState('addForm','<?php echo SECURE_PATH;?>superdashboard/process.php','questionattribute=1&chapoverview=1&class=<?php echo $_REQUEST['class']; ?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>')" ><?php echo $rowll2['chapter']; ?></a>
																			</td>
																			<td class="superdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print" onClick='window.open("<?php echo SECURE_PATH;?>superdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			<?php
																			if($rowlll[0]>0){
																			?>
																				<td class="superdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process4.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-eye"  ></i></p>
			
																				</td>
																			<?php }else{ ?>
																				<td class="superdashboard-buttons">
																					<p><a class=""  style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-edit" aria-hidden="true"></i></a>&nbsp;&nbsp;&nbsp;</p>
																				</td>
																			<?php } ?>
																			
																				<td>
																					<?php echo  $tcount; ?>
																				</td>
																			
																					
																			<?php
																			$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																				if($ro2['id']==1){
																			?>
																				<td>
																					<?php echo $count; ?>
																				</td>
																			<?php
																				}else if($ro2['id']==2){
																				?>
																				<td>
																					<?php echo $count1; ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			
																			$sell12=$database->query("select * from complexity where estatus='1' ");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."'  and find_in_set(".$rowll2['id'].",chapter)>0  and question_theory='".$ro2['id']."'".$con." ");
																					
																					 $todayttocount1=mysqli_fetch_array($todata);
																					
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																		$l++;
																		$overall3=$overall3+$tcount;
																			$totalcnt3=$totalcnt3+$tcount;
																	} ?>

																		<tr>
																		<td colspan='3' align='right'>Total</td>
																		<td><?php echo $overall3; ?></td>
																		<td colspan='2'><?php echo $overall3; ?></td>
																		<?php 
																		
																		
																			
																			$s1=$database->query("select * from complexity where estatus='1'");
																			while($rs1=mysqli_fetch_array($s1)){
																				$easycnt2=0;
																		
																				$sell2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
																				while($rowll2=mysqli_fetch_array($sell2)){ 
																					
																					$todata1= $database->query("select count(id) as cnt from createquestion where estatus=1 and complexity!='' and chapter!='' and complexity='".$rs1['id']."' and find_in_set(".$rowll2['id'].",chapter) ".$con." ");
																					$todayttocount1=mysqli_fetch_array($todata1);
																					$easycnt2=$easycnt2+$todayttocount1['cnt'];
																					
																				}
																				?>
																					<td colspan='2'><?php echo $easycnt2; ?></td>
																					<?php
																			}
																			?>
																	</tr>
																	<?php
															
																	}
																	
																	?>
																
																<?php
																}else{
																?> 
															
																	<?php
																	
																	$l=1;
																	$overall2=0;
																	$totalcnt2=0;
																	$sell2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
																	
																	while($rowll2=mysqli_fetch_array($sell2)){
																		$j=1;
																		$count=0;
																		$count1=0;
																		$tcount=0;
																		$sel2=$database->query("select * from complexity where estatus='1'");
																		while($rowl2=mysqli_fetch_array($sel2)){
																			$sel12=$database->query("SELECT * FROM `question_theory`");
																			while($rowlll2=mysqli_fetch_array($sel12)){
																				
																				  $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowl2['id']."' and question_theory='".$rowlll2['id']."' and  find_in_set(".$rowll2['id'].",chapter)>0 ".$con." ");
																				
																				 $todayttocount=mysqli_fetch_array($todata);
																				if($rowlll2['id']=='1'){
																					$count=$count+$todayttocount[0];
																				 }else{
																					 $count1=$count1+$todayttocount[0];
																				 }
																				 $tcount=$tcount+$todayttocount[0];
																				
																			}
																		}
			
																		
																		?>
			
																		<tr>
																			<td>
																				<a   onClick="setState('questionattribute','<?php echo SECURE_PATH;?>superdashboard/process6.php','questionattribute=1&chapoverview=1&class=<?php echo $_REQUEST['class']; ?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>')" ><?php echo $rowll2['chapter']; ?></a>
																			</td>
																			<td class="superdashboard-buttons">
																				<a style="cursor:pointer;"  title="Print" onClick='window.open("<?php echo SECURE_PATH;?>superdashboard/process1.php?viewDetails2=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&totalquestions=<?php echo $_REQUEST['totalquestions']; ?>");' ><i class="fa fa-eye"  ></i></a>
																			</td>
																			<td class="review-buttons">
																				<a   style="cursor:pointer;" title="Print"  data-toggle="modal" data-target="#exampleModal" onClick="setStateGet('viewDetails','<?php echo SECURE_PATH;?>superdashboard/process2.php','viewDetails=1&class=<?php echo $_REQUEST['class'];?>&subject=<?php echo $_REQUEST['subject'];?>&chapter=<?php echo $rowll2['id']; ?>&type=1')"><i class="fa fa-edit" aria-hidden="true"></i></a>
																			</td>
																			<td>
																					<?php echo  $tcount; ?>
																				</td>
																			
																					
																			<?php
																			$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																				if($ro2['id']==1){
																			?>
																				<td>
																					<?php echo $count; ?>
																				</td>
																			<?php
																				}else if($ro2['id']==2){
																				?>
																				<td>
																					<?php echo $count1; ?>
																				</td>
																				<?php
																				}
																			}
																			
																			$j=1;
																			
																			$sell12=$database->query("select * from complexity where estatus='1' ");
																			while($rowll12=mysqli_fetch_array($sell12)){
																				$se2=$database->query("SELECT * FROM `question_theory`");
																				while($ro2=mysqli_fetch_array($se2)){
																					 $todata = $database->query("select count(*) from createquestion where estatus=1 and complexity='".$rowll12['id']."' and question_theory='".$ro2['id']."' and  find_in_set(".$rowll2['id'].",chapter)>0 ".$con." ");
																					
																					 $todayttocount1=mysqli_fetch_array($todata);
																					
																			?>
																				<td>
																				 <?php  if($todayttocount1[0]!=''){ echo $todayttocount1[0]; } else{ echo '0'; } ?>
																				</td>
																			<?php
																				 }
																			}
																		   ?>
																			
																		</tr>
																		<?php
																		$overall2=$overall2+$tcount;
																		$totalcnt2=$totalcnt2+$tcount;
																		$l++;
																	}
															
																	?>
																		<tr>
																		<td colspan='3' align='right'>Total</td>
																		<td><?php echo $overall2; ?></td>
																		<td colspan='2'><?php echo $overall2; ?></td>
																		<?php 
																		
																		
																			
																			$s1=$database->query("select * from complexity where estatus='1'");
																			while($rs1=mysqli_fetch_array($s1)){
																				$easycnt2=0;
																		
																				$sell2=$database->query("select * from chapter where estatus='1' and class='".$_REQUEST['class']."' and subject='".$_REQUEST['subject']."'");
																				while($rowll2=mysqli_fetch_array($sell2)){ 
																					
																					$todata1= $database->query("select count(id) as cnt from createquestion where estatus=1 and complexity!='' and chapter!='' and complexity='".$rs1['id']."' and find_in_set(".$rowll2['id'].",chapter) ".$con." ");
																					$todayttocount1=mysqli_fetch_array($todata1);
																					$easycnt2=$easycnt2+$todayttocount1['cnt'];
																					
																				}
																				?>
																					<td colspan='2'><?php echo $easycnt2; ?></td>
																					<?php
																			}
																			?>
																	</tr>
																<?php } ?>
						  										</tbody>
						  									</table>
												</div>
												
												
											<?php   ?>
										</div>
										
						  		
<?php
}
?>
<script type="text/JavaScript">

$(".chosen-select").chosen();

$(document).on("click",".questionWiseCls li",function(e){
	
	$(this).closest("ul").find("li").removeClass("active");
	$(this).addClass("active");

	$(".selectedNameCls").html($(this).html());
}); 

</script>