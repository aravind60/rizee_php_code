<?php
include('../include/session.php');
error_reporting(1);
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


		<!-- 
<script async src="../edut/tinymce4/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image"></script>

<script async src="https://www.wiris.net/demo/plugins/app/WIRISplugins.js?viewer=image"></script>

Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo SECURE_PATH;?>vendor/bootstrap/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i&display=swap" rel="stylesheet">
		<style >

          svg{
 vertical-align: -5px;
 
}

            @media print {
                @page { margin: 0.5cm 0cm;
                 size: A4 }
                body { margin: 1.6cm; }
            }

            p:empty {
                display: none;
            }


			.d-flex {
				display: -webkit-box!important;
				display: -ms-flexbox!important;
				display: flex!important;
			}
			.pdf-content .q-no {
				font-size:20px;
				font-family: 'Source Sans Pro', sans-serif !important;
			}

			.pdf-content  .question-header {
				padding-left:5px;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                padding-bottom: 16px
			}
			
			.pdf-content  .question-header p ,
			.pdf-content  .question-header p span{
                margin-block-end: 0.1em;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size: 18px !important;
				font-weight: 600 !important;
			}

            .question-header img{
                max-width:75%;
            }

			.pdf-content p{
			margin-block-start: 0;
    /*margin-block-end: 0;*/
			}

			.question-list  {

			padding-left:5px;
			font-family: 'Source Sans Pro', sans-serif !important;
			font-size:14px !important;
				}
			.question-list .options {
				/* align-items: center; */
				justify-content: center;
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                display:inline-block !important;
				width:45%;
				float:left;
                margin-right:16px;
			}
			.pdf-content p, span,
			.pdf-content .question-list p {
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;

			}

			.question-list .options p{
				font-family: 'Source Sans Pro', sans-serif !important;
				font-size:16px !important;
                margin-block-end: 0em;
			}
            .question-list .options table{
                min-width:240px;
            }

            .question-list .options p img{
                max-width:77%;
            }
			.MsoListParagraph span span[style] {
				display:none;
			}


			.option{
			font-weight:bold;
			padding-left: 8px;
			}

            .question{
                padding-bottom:16px;
            }

            .clearall{
                clear:both;
                padding:0;margin:0;
            }

            tbody,tr,.question{
                page-break-inside:avoid;
                page-break-after:auto;
                position:relative;
                display:block;
            }
		</style>
	</head>
	<body>
	
		<?php
		
		?>
	
	<body>
	<div class="pdf-content py-3">
		<div class="container">
			<h2 class="text-center"> Custom Conetnt</h2>
	
			<?php
			$k=1;
			$sel=$database->query("SELECT b.cid,b.description FROM `customcontent` as a inner join customcontent_app as b on a.estatus='1' and b.estatus='1' and a.id=b.cid  and b.cid='2617' ");
			?>
            <table class="table qtable">

            <?php
			while($value=mysqli_fetch_array($sel)){
			?>
			<tr>
               <td valign="top" style="width:50px;">
                   <div class="col-form-label pt-1 q-no"><?php echo $k ;?>.</div>

               </td>
                <td valign="top">
					
			
					<div class="question">
						<div class="questionid">Q.Id:	<?php echo $value['cid']; ?></div>
						<div class="question-header">	<?php echo $value['description']; ?></div>
							
								
					</div>
				
				<?php	
			//$k++;

			}
			
			?>
		</table>
	</div>
</div>


	</body>
</html>