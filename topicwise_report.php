<?php

ini_set('display_errors','1');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

ini_set('memory_limit', '-1');
ini_set('post_max_size', '100000M');
ini_set('upload_max_filesize', '100000M');
ini_set('max_input_time ', 100000);
ini_set('max_execution_time', 9000);
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = " Total Questions report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

//header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						
						 	<th style="text-align:center;">Sr.No.</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Topic</th>
							
							<?php
							/*$sql=query("select * from customcontent_types where estatus='1'");
							while($row=mysqli_fetch_array($sql)){
								?>
								<th style="text-align:center;"><?php echo $row['customcontent']; ?></th>
								<?php
							}*/
							?>
							 <th style="text-align:center;">Question Id</th>
							<th style="text-align:center;">Verification Status</th>
							<th style="text-align:center;">Verified Username</th>
							<th style="text-align:center;">Verified Date</th>
							<th style="text-align:center;">Review Status</th>
							<th style="text-align:center;">Reviewed Username</th>
							<th style="text-align:center;">Reviewed Date</th>
							<th style="text-align:center;">Edit Username</th>
							<th style="text-align:center;">Edit Date</th> 

							
						</tr>
                        <?php
                            /* $k=1;
                            $sel=query("SELECT *  FROM topic WHERE estatus=1  and subject='5' ORDER BY subject ASC ");
                            while($row = mysqli_fetch_array($sel)){
                                $selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$row['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
								$selchapter=query("SELECT *  FROM chapter WHERE estatus=1 and id='".$row['chapter']."' ORDER BY id ASC");
								$rowchapter = mysqli_fetch_array($selchapter);
								
									
									$selltot=query("select * from createquestion where  estatus='1' and subject='5' and find_in_set(".$row['id'].",topic)>0 and find_in_set(".$row['chapter'].",chapter)>0  ");
									while($rowl=mysqli_fetch_array($selltot)){
									if($rowl['vstatus']==1){
										$vstatus="Verified";
									}else if($rowl['vstatus']==0){
										$vstatus="Pending";
									}else if($rowl['vstatus']==2){
										$vstatus="Rejected";
									}
									if($rowl['review_status']==1){
										$rstatus="Verified";
									}else if($rowl['review_status']==0){
										$rstatus="Pending";
									}else if($rowl['review_status']==2){
										$rstatus="Rejected";
									}
									
									echo "<tr>";
										?>	
										
											<td><?php echo $k;?></td>
											<td ><?php echo $rowsubject['subject'];?></td>
											
											<td><?php echo $rowchapter['chapter']; ?></td>
											<td><?php echo $row['topic']; ?></td>
											<td ><?php echo $rowl['id'];?></td>
											<td ><?php echo $vstatus;?></td>
											<td ><?php echo $rowl['vusername1'];?></td>
											<td ><?php echo date('d/m/Y H:i:s',$rowl['vtimestamp1']);?></td>
											<td ><?php echo $rstatus;?></td>
											<td ><?php echo $rowl['rusername'];?></td>
											<td ><?php echo  date('d/m/Y H:i:s',$rowl['rtimestamp']);?></td>
											
										<?php
										/*$sqlcustom=query("select * from customcontent_types where estatus='1'");
										while($rowcustom=mysqli_fetch_array($sqlcustom)){
											$selltot1=query("select count(id) as cnt from customcontent where  estatus='1' and find_in_set(".$row['id'].",topic)>0 and find_in_set(".$row['chapter'].",chapter)>0 and subject='".$row['subject']."' and conttype='".$rowcustom['id']."' ");
											$rowltot1=mysqli_fetch_array($selltot1);
											?>
											<td ><?php echo $rowltot1['cnt'];?></td>
										<?php
										}
										echo "</tr>";
										$k++;
                                                            
									           
									}     
							}*/
							$k=1;
							$selltot=query("select * from createquestion where  estatus='1'  order by subject,id asc  ");
							while($rowl=mysqli_fetch_array($selltot)){
								$selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$rowl['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
								$selchapter=query("SELECT *  FROM chapter WHERE estatus=1 and id='".$rowl['chapter']."' ORDER BY id ASC");
								$rowchapter = mysqli_fetch_array($selchapter);
								$seltopic=query("SELECT *  FROM topic WHERE estatus=1 and id='".$rowl['topic']."' ORDER BY id ASC");
								$rowtopic = mysqli_fetch_array($seltopic);
								if($rowl['vstatus']==1){
									$vstatus="Verified";
								}else if($rowl['vstatus']==0){
									$vstatus="Pending";
								}else if($rowl['vstatus']==2){
									$vstatus="Rejected";
								}
								if($rowl['review_status']==1){
									$rstatus="Verified";
								}else if($rowl['review_status']==0){
									$rstatus="Pending";
								}else if($rowl['review_status']==2){
									$rstatus="Rejected";
								}
								if($rowl['rtimestamp']!=''){
									$rtimestamp=date('d/m/Y H:i:s',$rowl['rtimestamp']);
								}else{
									$rtimestamp='';
								}
								if($rowl['vtimestamp1']!=''){
									$vtimestamp1=date('d/m/Y H:i:s',$rowl['vtimestamp1']);
								}else{
									$vtimestamp1='';
								}
								if($rowl['etimestamp']!=''){
									$etimestamp=date('d/m/Y H:i:s',$rowl['etimestamp']);
								}else{
									$etimestamp='';
								}
								echo "<tr>";
										?>	
										
											<td><?php echo $k;?></td>
											<td ><?php echo $rowsubject['subject'];?></td>
											
											<td><?php echo $rowchapter['chapter']; ?></td>
											<td><?php echo $rowtopic['topic']; ?></td>
											<td ><?php echo $rowl['id'];?></td>
											<td ><?php echo $vstatus;?></td>
											<td ><?php echo $rowl['vusername1'];?></td>
											<td ><?php echo $vtimestamp1;?></td>
											<td ><?php echo $rstatus;?></td>
											<td ><?php echo $rowl['rusername'];?></td>
											<td ><?php echo  $rtimestamp;?></td>
											<td ><?php echo $rowl['eusername'];?></td>
											<td ><?php echo  $etimestamp;?></td>
											
										<?php
										
										echo "</tr>";
										
								$k++;
                                                            
									           
									}
							
						
						/*$k=1;
						$sql=query("SELECT * FROM `createquestion` where estatus='1' and timestamp between 1598898600 and 1603045799 order by subject,timestamp asc");
						while($row=mysqli_fetch_array($sql)){
							$selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$row['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
							echo "<tr>";
										?>	
										
					 					<td><?php echo $k;?></td>
					 					<td ><?php echo $row['id'];?></td>
						 					<td ><?php echo $rowsubject['subject'];?></td>
						 					<td ><?php echo date("d/m/Y",$row['timestamp']);?></td>
					 					<td ><?php echo $row['username'];?></td>
						 <?php
						 $k++;
						 }*/
                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>