<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

ini_set('memory_limit', '-1');
ini_set('post_max_size', '100000M');
ini_set('upload_max_filesize', '100000M');
ini_set('max_input_time ', 100000);
ini_set('max_execution_time', 9000);
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = " Users report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
//header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						
						 	<th style="text-align:center;">Username</th>
							 <th style="text-align:center;">Type</th>
							 <th style="text-align:center;">Date</th>
							<th style="text-align:center;">Verified Count</th>
							<th style="text-align:center;">Edit Count</th>
							<th style="text-align:center;">Delete Count</th>
							
						</tr>
                        <?php
							$sql=query("select * from users where userlevel in (3,6) and valid='1' order by userlevel asc");
							while($row=mysqli_fetch_array($sql)){
								$sel=query("SELECT DISTINCT(from_unixtime(timestamp,'%d-%m-%Y')) as date FROM question_log where username='".$row['username']."' and timestamp between 1604687400 and 1607452199 order by timestamp asc");
								while($rowsel=mysqli_fetch_array($sel)){
									$fromdate = strtotime($rowsel['date']);
									$todate = strtotime($rowsel['date'].' 23:59:59');
									$sel1=query("select id from question_log where estatus='1' and message='2' and username='".$row['username']."' and  timestamp between ".$fromdate." and ".$todate." group by question_id");
									$rowl1=mysqli_num_rows($sel1);

									$sel2=query("select id from question_log where estatus='1' and message='5' and username='".$row['username']."' and  timestamp between ".$fromdate." and ".$todate." group by question_id");
									$rowl2=mysqli_num_rows($sel2);

									$sel3=query("select id from question_log where estatus='1' and message='3' and username='".$row['username']."' and  timestamp between ".$fromdate." and ".$todate." group by question_id");
									$rowl3=mysqli_num_rows($sel3);
									if($row['userlevel']=='3'){
										$type="Lecturer";
									}else{
										$type="Reviewer";
									}
									if($rowl1!=0 || $rowl2!=0 || $rowl3!=0){
										?>
										<tr>
											<td><?php echo $row['username']; ?></td>
											<td><?php echo $type; ?></td>
											<td><?php echo $rowsel['date']; ?></td>
											<td><?php echo $rowl2; ?></td>
											<td><?php echo $rowl1; ?></td>
											<td><?php echo $rowl3; ?></td>
										</tr>
										<?php	
									}
								}
							}
						 ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>