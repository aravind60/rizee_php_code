<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");

ini_set('memory_limit', '-1');
ini_set('post_max_size', '100000M');
ini_set('upload_max_filesize', '100000M');
ini_set('max_input_time ', 100000);
ini_set('max_execution_time', 9000);
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = " Total Questions report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";
header("Content-Disposition: attachment; filename=\"$fileName\"");
header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						
						 	<th style="text-align:center;">Sr.No.</th>
							 <th style="text-align:center;">Class</th>
							<th style="text-align:center;">Subject</th>
							<th style="text-align:center;">Chapter</th>
							<th style="text-align:center;">Topic</th>
							<th style="text-align:center;">Question Id</th>
							<th style="text-align:center;">Exam Type</th>
							<th style="text-align:center;">Mains/Advance</th>
							<th style="text-align:center;">Question (Previous Question/Question Bank)</th>
							<th style="text-align:center;">Question Type</th>
							<th style="text-align:center;">Attribute Question Type</th>
							<th style="text-align:center;">Complexity</th>
							<th style="text-align:center;">Timeduration</th>
							<th style="text-align:center;">Usage Set</th>
							<th style="text-align:center;">Question Theory</th>
							<th style="text-align:center;">Explanation (Yes/No)</th>
							<th style="text-align:center;">Created Date</th>
							<th style="text-align:center;">Created By</th>
							<th style="text-align:center;">Verification Status</th>
							<th style="text-align:center;">Verified Username</th>
							<th style="text-align:center;">Verified Date</th>
							<th style="text-align:center;">Review Status</th>
							<th style="text-align:center;">Reviewed Username</th>
							<th style="text-align:center;">Reviewed Date</th>
							<th style="text-align:center;">Edit Username</th>
							<th style="text-align:center;">Edit Date</th> 

							
						</tr>
                        <?php
                           	$k=1;
							$selltot=query("select * from createquestion where  estatus='1'  order by subject,id asc  ");
							while($rowl=mysqli_fetch_array($selltot)){
								$selclass=query("SELECT *  FROM class WHERE estatus=1 and id='".$rowl['class']."' ORDER BY id ASC");
								$rowclass = mysqli_fetch_array($selclass);
								$selsubject=query("SELECT *  FROM subject WHERE estatus=1 and id='".$rowl['subject']."' ORDER BY id ASC");
								$rowsubject = mysqli_fetch_array($selsubject);
								$selchapter=query("SELECT *  FROM chapter WHERE estatus=1 and id='".$rowl['chapter']."' ORDER BY id ASC");
								$rowchapter = mysqli_fetch_array($selchapter);
								$seltopic=query("SELECT *  FROM topic WHERE estatus=1 and id='".$rowl['topic']."' ORDER BY id ASC");
								$rowtopic = mysqli_fetch_array($seltopic);
								if($rowl['vstatus1']==1){
									$vstatus="Verified";
									$vtimestamp1=date('d/m/Y H:i:s',$rowl['vtimestamp1']);
									$vusername=$rowl['vusername1'];
								}else if($rowl['vstatus1']==0){
									$vstatus="Pending";
									$vtimestamp1="";
									$vusername="";
								}else if($rowl['vstatus1']==2){
									$vstatus="Rejected";
									$vtimestamp1=date('d/m/Y H:i:s',$rowl['vtimestamp1']);
									$vusername=$rowl['vusername1'];
								}
								if($rowl['review_status']==1){
									$rstatus="Verified";
								}else if($rowl['review_status']==0){
									$rstatus="Pending";
								}else if($rowl['review_status']==2){
									$rstatus="Rejected";
								}
								if($rowl['rtimestamp']!=''){
									$rtimestamp=date('d/m/Y H:i:s',$rowl['rtimestamp']);
								}else{
									$rtimestamp='';
								}
								if($rowl['timestamp']!=''){
									$ctimestamp=date('d/m/Y H:i:s',$rowl['timestamp']);
								}else{
									$ctimestamp='';
								}
								
								if($rowl['etimestamp']!=''){
									$etimestamp=date('d/m/Y H:i:s',$rowl['etimestamp']);
								}else{
									$etimestamp='';
								}
								$sqltype=query("select * from questiontype where estatus='1' and id='".$rowl['qtype']."' ");
								$rowtype=mysqli_fetch_array($sqltype);

								$qtype='';
								$sqltype1=query("select * from questiontype where estatus='1' and id in (".$rowl['inputquestion'].") ");
								while($rowtype1=mysqli_fetch_array($sqltype1)){
									$qtype.=$rowtype1['questiontype'].",";
								}
								$exam='';
								$sqlexam=query("select * from exam where estatus='1' and id in (".$rowl['exam'].") ");
								while($rowexam=mysqli_fetch_array($sqlexam)){
									$exam.=$rowexam['exam'].",";
								}

								$sqlcom=query("select * from complexity where estatus='1' and id='".$rowl['complexity']."' ");
								$rowcom=mysqli_fetch_array($sqlcom);
								$sqlset=query("select * from question_useset where estatus='1' and id='".$rowl['usageset']."' ");
								$rowset=mysqli_fetch_array($sqlset);
								$sqlqt=query("select * from question_theory where estatus='1' and id='".$rowl['question_theory']."' ");
								$rowqt=mysqli_fetch_array($sqlqt);
								if(($rowl['explanation']=='%3Cp%3ENA%3C%2Fp%3E') || ($rowl['explanation'] == '%3Cp%3ENA%3C%2Fp%3E%0A%3Cp%3E%26nbsp%3B%3C%2Fp%3E') || ($rowl['explanation']=='') || (strlen($rowl['explanation']) < 30)){	 
									$explanation="No";
								}else{
									$explanation="Yes";
								}
								if($rowl['ppaper']=='0'){
									$questiondata="Question Bank";
								}else{
									$questiondata='Previous Paper';
								}
								
								if(strpos($rowl['exam'],'2')){
									if($rowl['pexamtype']=='1'){
										$examtype="Mains";
									}else if($rowl['pexamtype']=='2'){
										$examtype="Advanced";
									}else{
										$examtype='';
									}
								}else if($rowl['exam']=='2'){
									if($rowl['pexamtype']=='1'){
										$examtype="Mains";
									}else if($rowl['pexamtype']=='2'){
										$examtype="Advanced";
									}else{
										$examtype='';
									}
								}else{
									$examtype='';
								}
								echo "<tr>";
										?>	
										
											<td><?php echo $k;?></td>
											<td ><?php echo $rowclass['class'];?></td>
											<td ><?php echo $rowsubject['subject'];?></td>
											
											<td><?php echo $rowchapter['chapter']; ?></td>
											<td><?php echo $rowtopic['topic']; ?></td>
											<td ><?php echo $rowl['id'];?></td>
											
											<td ><?php echo rtrim($exam,",");?></td>
											<td ><?php echo $examtype;?></td>
											<td ><?php echo $questiondata;?></td>
											<td><?php echo $rowtype['questiontype']; ?></td>
											<td><?php echo rtrim($qtype,","); ?></td>
											<td><?php echo $rowcom['complexity']; ?></td>
											<td><?php echo $rowl['timeduration']; ?></td>
											<td><?php echo $rowset['usageset']; ?></td>
											<td><?php echo $rowqt['question_theory']; ?></td>
											<td><?php echo $explanation; ?></td>
											<td ><?php echo $ctimestamp;?></td>
											<td ><?php echo $rowl['username'];?></td>
											<td ><?php echo $vstatus;?></td>
											<td ><?php echo $vusername;?></td>
											<td ><?php echo $vtimestamp1;?></td>
											<td ><?php echo $rstatus;?></td>
											<td ><?php echo $rowl['rusername'];?></td>
											<td ><?php echo  $rtimestamp;?></td>
											<td ><?php echo $rowl['eusername'];?></td>
											<td ><?php echo  $etimestamp;?></td>
											
										<?php
										
										echo "</tr>";
										
								$k++;
                                                            
									           
									}
							
						
						                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>