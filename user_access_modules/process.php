
<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
?>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
	<?php
	//$con = mysqli_connect("localhost","root","","neetjee");
	$con = mysqli_connect("localhost","rspace","Rsp@2019","neetjee");
	function query($sql){
	global $con;
	 return mysqli_query($con,$sql);
	}
	if(isset($_REQUEST['addForm'])){
		
		if(isset($_POST['institution_id']) && isset($_POST['plan_id'])){
		?>
	 
		 <?php
		if(isset($_POST['plan_id'])){
			 if($_POST['plan_id']!='' && $_POST['plan_id']!='0'){
				$plan_id=" AND plan_id='".$_POST['plan_id']."'";

			 }else{
				 $plan_id="";
			 }
		 }else{
			 $plan_id="";
		 }
		 $data_sel =query("SELECT * FROM user_access_modules WHERE institution_id = '".$_POST['institution_id']."'  ".$plan_id."");
		if(mysqli_num_rows($data_sel) > 0){
		$data = mysqli_fetch_array($data_sel);
		$_POST = array_merge($_POST,$data);
		$data=json_decode($_POST['modules'],true);
		$_POST['ra_actions']=$data['ra_actions'];
		$_POST['ra_overall']=$data['ra_overall'];
		$_POST['ra_overall_details']=$data['ra_overall_details'];
		$_POST['ra_subject_strength']=$data['ra_subject_strength'];
		$_POST['ra_subject_strength_details']=$data['ra_subject_strength_details'];
		$_POST['ra_time_analysis']=$data['ra_time_analysis'];
		$_POST['ra_time_analysis_details']=$data['ra_time_analysis_details'];
		$_POST['ra_complexity']=$data['ra_complexity'];
		$_POST['ra_complexity_details']=$data['ra_complexity_details'];
		$_POST['ra_qtheory']=$data['ra_qtheory'];
		$_POST['ra_qtheory_details']=$data['ra_qtheory_details'];
		$_POST['ra_qtype']=$data['ra_qtype'];
		$_POST['ra_qtype_details']=$data['ra_qtype_details'];
		$_POST['custom_chapter']=$data['custom_chapter'];
		$_POST['custom_cumulative']=$data['custom_cumulative'];
		$_POST['custom_semi_grand']=$data['custom_semi_grand'];
		$_POST['custom_grand']=$data['custom_grand'];
		$_POST['custom_completed']=$data['custom_completed'];
		$_POST['custom_completed_analysis']=$data['custom_completed_analysis'];
		$_POST['error_chapter']=$data['error_chapter'];
		$_POST['error_cumulative']=$data['error_cumulative'];
		$_POST['error_completed']=$data['error_completed'];
		$_POST['error_completed_analysis']=$data['error_completed_analysis'];
		$_POST['previous_custom']=$data['previous_custom'];
		$_POST['previous_single']=$data['previous_single'];
		$_POST['previous_completed']=$data['previous_completed'];
		$_POST['previous_completed_analysis']=$data['previous_completed_analysis'];
		$_POST['schedule_exam_completed']=$data['schedule_exam_completed'];
		$_POST['schedule_completed_analysis']=$data['schedule_completed_analysis'];
		$_POST['schedule_exam']=$data['schedule_exam'];
		$_POST['custom_exam']=$data['custom_exam'];
		$_POST['previous_exam']=$data['previous_exam'];
		$_POST['practise_completed']=$data['practise_completed'];
		$_POST['practise_completed_analysis']=$data['practise_completed_analysis'];
		$_POST['lp_practice_exam']=$data['lp_practice_exam'];
		$_POST['lp_practice_filter']=$data['lp_practice_filter'];
		$_POST['lp_custom_content']=$data['lp_custom_content'];
		$_POST['lp_error_exam']=$data['lp_error_exam'];
		$_POST['lp_topics']=$data['lp_topics'];
		$_POST['lp_chapter_dashboard']=$data['lp_chapter_dashboard'];
		$_POST['lp_topic_dashboard']=$data['lp_topic_dashboard'];
		$_POST['lp_topics_practice_exam']=$data['lp_topics_practice_exam'];
		$_POST['lp_topic_custom_content']=$data['lp_topic_custom_content'];
		$_POST['lp_topic_error_exam']=$data['lp_topic_error_exam'];
		$_POST['ins_custom_exam']=$data['ins_custom_exam'];
		$_POST['ins_previous_exam']=$data['ins_previous_exam'];
		//$_POST['linkage_analysis_tab']=$data['linkage_analysis_tab'];
		$_POST['videos_tab']=$data['videos_tab'];
		$_POST['mock_test']=$data['mock_test'];
		$_POST['jee_mains_2021']=$data['jee_mains_2021'];
		$_POST['help_videos']=$data['help_videos'];
		$_POST['blog_tab']=$data['blog_tab'];
		if($data['ins_learn_tab']!=1){
			$_POST['learn_tab']=$data['learn_tab'];
		}else{
			$_POST['learn_tab']=2;
			
		}
		if($data['ins_exam_tab']!=1){
			$_POST['exam_tab']=$data['exam_tab'];
		}else{
			$_POST['exam_tab']=2;
			
		}
		if($data['ins_error_exam']!=1){
			$_POST['error_exam']=$data['error_exam'];
		}else{
			$_POST['error_exam']=2;
			
		}
		if($data['ins_ready_exam_tab']!=1){
			$_POST['ready_exam_tab']=$data['ready_exam_tab'];
		}else{
			$_POST['ready_exam_tab']=2;
			
		}
		if($data['ins_analysis_tab']!=1){
			$_POST['analysis_tab']=$data['analysis_tab'];
		}else{
			$_POST['analysis_tab']=2;
			
		}
		if($data['ins_bookmarks_tab']!=1){
			$_POST['bookmarks_tab']=$data['bookmarks_tab'];
		}else{
			$_POST['bookmarks_tab']=2;
			
		}
		if($data['ins_notes_tab']!=1){
			$_POST['notes_tab']=$data['notes_tab'];
		}else{
			$_POST['notes_tab']=2;
			
		}
		if($data['ins_profile_tab']!=1){
			$_POST['profile_tab']=$data['profile_tab'];
		}else{
			$_POST['profile_tab']=2;
			
		}
		if($data['ins_videos_tab']!=1){
			$_POST['videos_tab']=$data['videos_tab'];
		}else{
			$_POST['videos_tab']=2;
			
		}
		if($data['ins_mock_test']!=1){
			$_POST['mock_test']=$data['mock_test'];
		}else{
			$_POST['mock_test']=2;
			
		}
		if($data['ins_jee_mains_2021']!=1){
			$_POST['jee_mains_2021']=$data['jee_mains_2021'];
		}else{
			$_POST['jee_mains_2021']=2;
			
		}
		if($data['ins_blog_tab']!=1){
			$_POST['blog_tab']=$data['blog_tab'];
		}else{
			$_POST['blog_tab']=2;
			
		}
		if($data['ins_doubts_tab']!=1){
			$_POST['doubts_tab']=$data['doubts_tab'];
		}else{
			$_POST['doubts_tab']=2;
			
		}
		if($data['ins_feedback_tab']!=1){
			$_POST['feedback_tab']=$data['feedback_tab'];
		}else{
			$_POST['feedback_tab']=2;
			
		}
		if($data['ins_package_tab']!=1){
			$_POST['package_tab']=$data['package_tab'];
		}else{
			$_POST['package_tab']=2;
			
		}

		if($data['ins_home_tab']!=1){
			$_POST['home_tab']=$data['home_tab'];
		}else{
			$_POST['home_tab']=2;
			
		}
		if($data['ins_revision_material_tab']!=1){
			$_POST['revision_material_tab']=$data['revision_material_tab'];
		}else{
			$_POST['revision_material_tab']=2;
			
		}
		if($data['ins_previous_paper_analysis_tab']!=1){
			$_POST['previous_paper_analysis_tab']=$data['previous_paper_analysis_tab'];
		}else{
			$_POST['previous_paper_analysis_tab']=2;
			
		}
		
		if($data['ins_linkage_analysis_tab']!=1){
			$_POST['linkage_analysis_tab']=$data['linkage_analysis_tab'];
		}else{
			$_POST['linkage_analysis_tab']=2;
			
		}
		
		if($data['ins_help_videos']!=1){
			$_POST['help_videos']=$data['help_videos'];
		}else{
			$_POST['help_videos']=2;
			
		}
		/*if($data['ins_video_tab']!=1){
			$_POST['video_tab']=$data['video_tab'];
		}else{
			$_POST['video_tab']=2;
			
		}*/
		
		?>
	 <script type="text/javascript">
	 $('#adminForm').slideDown();
	 </script>
	 
 <?php
    }
 }
 
 ?>
	
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12"> 
					<div class="card border-0 shadow mb-4">
							
						<!-- <div class="card-header py-3">
							<ul class="nav nav-pills" role="tablist">
								<li class="nav-item">
								  <a class="nav-link active" data-toggle="pill" href="#home" onclick="setStateGet('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1')">Add User Access Modules</a>
								</li>
								<li class="nav-item">
								  <a class="nav-link " data-toggle="pill" href="#menu1" onclick="setState('ggg','<?php echo SECURE_PATH;?>user_access_modules/ajax.php','tabledata=1')">Report</a>
								</li>
								
							</ul>
						</div> -->
						<div class="card-body">
							
						<?php
						if(isset($_REQUEST['dataenable'])){
							if($_REQUEST['dataenable']=='0'){
								$dataactive="active";
								$dataactive1="";
							}else if($_REQUEST['dataenable']=='1'){
								$dataactive1="active";
								$dataactive="";
							}else{
								$dataactive1="";
								$dataactive="";
							}
						}else{
							$dataactive1="";
							$dataactive="";
						}
						?>
						<div class="tab-content">
							<div id="home" class="container tab-pane active"><br>
	
							 <div class="col-lg-12 col-md-12">
									<div class="row" >	
										<div class="col-md-9"></div>
										<div class="col-md-3 pr-5">
                                        
                                        <ul class="nav nav-pills mb-3 class-tabs" role="tablist">
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo $dataactive; ?>" id="pills-home-tab" data-toggle="pill"
                                                    href="#pills-home" role="tab" aria-controls="pills-home" onclick="useraccessdiv1(0);"
                                                    aria-selected="true">Enable</a>
                                            </li>
                                            <li class="nav-item">
                                                <a class="nav-link <?php echo $dataactive1; ?>" id="pills-profile-tab" data-toggle="pill"
                                                    href="#pills-profile" role="tab" aria-controls="pills-profile" onclick="useraccessdiv1(1);"
                                                    aria-selected="false">Disable</a>
                                            </li>
                                        </ul>
										<input type="hidden" id="dataenable" class="form-control" value="<?php if(isset($_REQUEST['dataenable'])){ if($_REQUEST['dataenable']!=''){ echo $_REQUEST['dataenable']; } }else{ echo '';} ?> ">
                                    </div>
									</div>
									<div class="row">
										<!-- <div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">User Type</label>
												<div class="col-sm-6">
													
													<select class="form-control" name="userlevel" value=""  id="userlevel" onchange="usertypediv();" >
														<option value=''>-- Select --</option>
														
														<option value="2" <?php if(isset($_REQUEST['userlevel'])) { if($_REQUEST['userlevel']==2) { echo 'selected="selected"'; }  } ?>>Registered Users</option>
														<option value="1" <?php if(isset($_REQUEST['userlevel'])) { if($_REQUEST['userlevel']==1) { echo 'selected="selected"'; }  } ?>>Institute Users</option>
													</select>
																
													
													<span class="error text-danger"><?php if(isset($_SESSION['error']['userlevel'])){ echo $_SESSION['error']['userlevel'];}?></span>
												</div>
											</div>
										</div> -->
										<div class="col-lg-6" id="institutediv" >
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Institute</label>
												<div class="col-sm-6">
													<select class="form-control "  name="institution_id" value=""  id="institution_id" onChange="setState('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1&institution_id='+$('#institution_id').val()+'&plan_id='+$('#plan_id').val()+'&dataenable='+$('#dataenable').val()+'');">
														<option value=''>-- Select --</option>
														<?php
														$row = $database->query("select * from institute where estatus='1'");
														while($data = mysqli_fetch_array($row))
														{
															?>
														<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['institution_id'])) { if($_POST['institution_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo $data['institute_name'];?></option>
														<?php
														}
														?>
													</select>
													
													<span class="error text-danger"><?php if(isset($_SESSION['error']['institution_id'])){ echo $_SESSION['error']['institution_id'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6" >
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Plans</label>
												<div class="col-sm-6">
												<select name="plan_id" id="plan_id" class="form-control" onChange="setState('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1&institution_id='+$('#institution_id').val()+'&plan_id='+$('#plan_id').val()+'&dataenable='+$('#dataenable').val()+'');" >
													<option value=''>-Select-</option>
													<?php
													$row = query("select * from institute_student_plans where estatus='1' and institute_id='".$_POST['institution_id']."'   ");
													while($data = mysqli_fetch_array($row))
													{
														?>
													<option value="<?php echo $data['id'];?>" <?php if(isset($_POST['plan_id'])) { if($_POST['plan_id']==$data['id']) { echo 'selected="selected"'; }  } ?>><?php echo ucwords($data['plan_name']);?></option>
													<?php
													}
													?>
												</select>
												<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['plan_id'])){ echo $_SESSION['error']['plan_id'];}?></span>
												</div>
											</div>
										</div>
																	
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Actions</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_actions" id="ra_actions1" value="0" <?php if(isset($_POST['ra_actions'])) { if($_POST['ra_actions']=='0') { echo 'checked'; }else { echo 'checked';}  }else { echo 'checked';} ?> onclick="ra_actions(0)">
														  <label class="form-check-label" for="ra_actions1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_actions" id="ra_actions2" value="1" <?php if(isset($_POST['ra_actions'])) { if($_POST['ra_actions']=='1') { echo 'checked'; }else { echo '';}  }else { echo '';} ?> onclick="ra_actions(1)">
														  <label class="form-check-label" for="ra_actions2">Disable</label>
														</div>
														<input type="hidden" name="ra_actions" id="ra_actions"  value="<?php if(isset($_POST['ra_actions'])) { if($_POST['ra_actions']!=''){ echo $_POST['ra_actions']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_actions'])){ echo $_SESSION['error']['ra_actions'];}?></span>
												</div>
											</div>
										</div>
								
										
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Over All</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_overall" id="ra_overall1" value="0" <?php if(isset($_POST['ra_overall'])) { if($_POST['ra_overall']=='0') { echo 'checked'; }else { echo 'checked';}  }else { echo 'checked';} ?>   onclick="ra_overall(0)">
														  <label class="form-check-label" for="ra_overall1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_overall" id="ra_overall2" value="1" <?php if(isset($_POST['ra_overall'])) { if($_POST['ra_overall']=='1') { echo 'checked'; }else { echo '';}  }else { echo '';} ?> onclick="ra_overall(1)">
														  <label class="form-check-label" for="ra_overall2">Disable</label>
														</div>
														<input type="hidden" name="ra_overall" id="ra_overall"   value="<?php if(isset($_POST['ra_overall'])) { if($_POST['ra_overall']!=''){ echo $_POST['ra_overall']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_overall'])){ echo $_SESSION['error']['ra_overall'];}?></span>
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Details</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_overall_details" id="ra_overall_details1" value="0" <?php if(isset($_POST['ra_overall_details'])) { if($_POST['ra_overall_details']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>  onclick="ra_overall_details('0')">
														  <label class="form-check-label" for="ra_overall_details1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_overall_details" id="ra_overall_details2" value="1" <?php if(isset($_POST['ra_overall_details'])) { if($_POST['ra_overall_details']=='1') { echo 'checked'; }else { echo '';}  }else { echo '';} ?> onclick="ra_overall_details('1')">
														  <label class="form-check-label" for="ra_overall_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_overall_details" id="ra_overall_details" value="<?php if(isset($_POST['ra_overall_details'])) { if($_POST['ra_overall_details']!=''){ echo $_POST['ra_overall_details']; }else { echo '0';} }else{ echo '0'; }?>"  >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_overall_details'])){ echo $_SESSION['error']['ra_overall_details'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Subject Strenth</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_subject_strength" id="ra_subject_strength1" value="0" <?php if(isset($_POST['ra_subject_strength'])) { if($_POST['ra_subject_strength']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> onclick="ra_subject_strength(0)">
														  <label class="form-check-label" for="ra_subject_strength1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_subject_strength" id="ra_subject_strength2" value="1" <?php if(isset($_POST['ra_subject_strength'])) { if($_POST['ra_subject_strength']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?> onclick="ra_subject_strength(1)">
														  <label class="form-check-label" for="ra_subject_strength2">Disable</label>
														</div>
														<input type="hidden" name="ra_subject_strength" id="ra_subject_strength"  value="<?php if(isset($_POST['ra_subject_strength'])) { if($_POST['ra_subject_strength']!=''){ echo $_POST['ra_subject_strength']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['result_analysis_actions'])){ echo $_SESSION['error']['result_analysis_actions'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Subject Strenth Details</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_subject_strength_details" id="ra_subject_strength_details1" value="0" <?php if(isset($_POST['ra_subject_strength_details'])) { if($_POST['ra_subject_strength_details']==0) { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> onclick="ra_subject_strength_details(0)">
														  <label class="form-check-label" for="ra_subject_strength_details1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_subject_strength_details" id="ra_subject_strength_details2" value="1" <?php if(isset($_POST['ra_subject_strength_details'])) { if($_POST['ra_subject_strength_details']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?> onclick="ra_subject_strength_details(1)">
														  <label class="form-check-label" for="ra_subject_strength_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_subject_strength_details" id="ra_subject_strength_details"  value="<?php if(isset($_POST['ra_subject_strength_details'])) { if($_POST['ra_subject_strength_details']!=''){ echo $_POST['ra_subject_strength_details']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_subject_strength_details'])){ echo $_SESSION['error']['ra_subject_strength_details'];}?></span>
												</div>
											</div>
										</div>
										
								
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Time Analysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_time_analysis" id="ra_time_analysis1" value="0"  onclick="ra_time_analysis(0)" <?php if(isset($_POST['ra_time_analysis'])) { if($_POST['ra_time_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> >
														  <label class="form-check-label" for="ra_time_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_time_analysis" id="ra_time_analysis2" value="1" onclick="ra_time_analysis(1)" <?php if(isset($_POST['ra_time_analysis'])) { if($_POST['ra_time_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_time_analysis2">Disable</label>
														</div>
														<input type="hidden" name="ra_time_analysis" id="ra_time_analysis"  value="<?php if(isset($_POST['ra_time_analysis'])) { if($_POST['ra_time_analysis']!=''){ echo $_POST['ra_time_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_time_analysis'])){ echo $_SESSION['error']['ra_time_analysis'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Time Analysis Details</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_time_analysis_details" id="ra_time_analysis_details1" value="0"  onclick="ra_time_analysis_details(0)" <?php if(isset($_POST['ra_time_analysis_details'])) { if($_POST['ra_time_analysis_details']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ra_time_analysis_details">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_time_analysis_details" id="ra_time_analysis_details2" value="1" onclick="ra_time_analysis_details(1)" <?php if(isset($_POST['ra_time_analysis_details'])) { if($_POST['ra_time_analysis_details']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_time_analysis_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_time_analysis_details" id="ra_time_analysis_details"   value="<?php if(isset($_POST['ra_time_analysis_details'])) { if($_POST['ra_time_analysis_details']!=''){ echo $_POST['ra_time_analysis_details']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_time_analysis_details'])){ echo $_SESSION['error']['ra_time_analysis_details'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Complexity</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_complexity" id="ra_complexity1" value="0"  onclick="ra_complexity(0)" <?php if(isset($_POST['ra_complexity'])) { if($_POST['ra_complexity']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ra_complexity1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_complexity" id="ra_complexity2" value="1" onclick="ra_complexity(1)" <?php if(isset($_POST['ra_complexity'])) { if($_POST['ra_complexity']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_complexity2">Disable</label>
														</div>
														<input type="hidden" name="ra_complexity" id="ra_complexity"  value="<?php if(isset($_POST['ra_complexity'])) { if($_POST['ra_complexity']!=''){ echo $_POST['ra_complexity']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_complexity'])){ echo $_SESSION['error']['ra_complexity'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Complexity</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_complexity_details" id="ra_complexity_details1" value="0"  onclick="ra_complexity_details(0)" <?php if(isset($_POST['ra_complexity_details'])) { if($_POST['ra_complexity_details']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ra_complexity_details1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_complexity_details" id="ra_complexity_details2" value="1" onclick="ra_complexity_details(1)" <?php if(isset($_POST['ra_complexity_details'])) { if($_POST['ra_complexity_details']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_complexity_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_complexity_details" id="ra_complexity_details"  value="<?php if(isset($_POST['ra_complexity_details'])) { if($_POST['ra_complexity_details']!=''){ echo $_POST['ra_complexity_details']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_complexity_details'])){ echo $_SESSION['error']['ra_complexity_details'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Question Theory</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtheory" id="ra_qtheory1" value="0" <?php if(isset($_POST['ra_qtheory'])) { if($_POST['ra_qtheory']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> onclick="ra_qtheory(0)">
														  <label class="form-check-label" for="ra_qtheory1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtheory" id="ra_qtheory2" value="1" onclick="ra_qtheory(1)" <?php if(isset($_POST['ra_qtheory'])) { if($_POST['ra_qtheory']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_qtheory2">Disable</label>
														</div>
														<input type="hidden" name="ra_qtheory" id="ra_qtheory"  value="<?php if(isset($_POST['ra_qtheory'])) { if($_POST['ra_qtheory']!=''){ echo $_POST['ra_qtheory']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_qtheory'])){ echo $_SESSION['error']['ra_qtheory'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Question Theory Details</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtheory_details" id="ra_qtheory_details1" value="0" <?php if(isset($_POST['ra_qtheory_details'])) { if($_POST['ra_qtheory_details']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> onclick="ra_qtheory_details(0)">
														  <label class="form-check-label" for="ra_qtheory_details1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtheory_details" id="ra_qtheory_details2" value="1" onclick="ra_qtheory_details(1)" <?php if(isset($_POST['ra_qtheory_details'])) { if($_POST['ra_qtheory_details']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_qtheory_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_qtheory_details" id="ra_qtheory_details"  value="<?php if(isset($_POST['ra_qtheory_details'])) { if($_POST['ra_qtheory_details']!=''){ echo $_POST['ra_qtheory_details']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_qtheory_details'])){ echo $_SESSION['error']['ra_qtheory_details'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Question Type</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtype" id="ra_qtype1" value="0"  onclick="ra_qtype(0)" <?php if(isset($_POST['ra_qtype'])) { if($_POST['ra_qtype']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ra_qtype1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtype" id="ra_qtype2" value="1" onclick="ra_qtype(1)" <?php if(isset($_POST['ra_qtype'])) { if($_POST['ra_qtype']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_qtype2">Disable</label>
														</div>
														<input type="hidden" name="ra_qtype" id="ra_qtype"  value="<?php if(isset($_POST['ra_qtype'])) { if($_POST['ra_qtype']!=''){ echo $_POST['ra_qtype']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_qtype'])){ echo $_SESSION['error']['ra_qtype'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Result Analysis Question Type Details</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtype_details" id="ra_qtype_details1" value="0"  onclick="ra_qtype_details(0)" <?php if(isset($_POST['ra_qtype_details'])) { if($_POST['ra_qtype_details']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ra_qtype_details1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ra_qtype_details" id="ra_qtype_details2" value="1" onclick="ra_qtype_details(1)" <?php if(isset($_POST['ra_qtype_details'])) { if($_POST['ra_qtype_details']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ra_qtype_details2">Disable</label>
														</div>
														<input type="hidden" name="ra_qtype_details" id="ra_qtype_details"  value="<?php if(isset($_POST['ra_qtype_details'])) { if($_POST['ra_qtype_details']!=''){ echo $_POST['ra_qtype_details']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ra_qtype_details'])){ echo $_SESSION['error']['ra_qtype_details'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Chapter</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_chapter" id="custom_chapter1" value="0"  onclick="custom_chapter(0)" <?php if(isset($_POST['custom_chapter'])) { if($_POST['custom_chapter']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_chapter1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_chapter" id="custom_chapter2" value="1" onclick="custom_chapter(1)" <?php if(isset($_POST['custom_chapter'])) { if($_POST['custom_chapter']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_chapter2">Disable</label>
														</div>
														<input type="hidden" name="custom_chapter" id="custom_chapter"  value="<?php if(isset($_POST['custom_chapter'])) { if($_POST['custom_chapter']!=''){ echo $_POST['custom_chapter']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_chapter'])){ echo $_SESSION['error']['custom_chapter'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Cumulative</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_cumulative" id="custom_cumulative1" value="0"  onclick="custom_cumulative(0)" <?php if(isset($_POST['custom_cumulative'])) { if($_POST['custom_cumulative']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_cumulative1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_cumulative" id="custom_cumulative2" value="1" onclick="custom_cumulative(1)" <?php if(isset($_POST['custom_cumulative'])) { if($_POST['custom_cumulative']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_cumulative2">Disable</label>
														</div>
														<input type="hidden" name="custom_cumulative" id="custom_cumulative"  value="<?php if(isset($_POST['custom_cumulative'])) { if($_POST['custom_cumulative']!=''){ echo $_POST['custom_cumulative']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_cumulative'])){ echo $_SESSION['error']['custom_cumulative'];}?></span>
												</div>
											</div>
										</div>
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Semi Grand</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_semi_grand" id="custom_semi_grand1" value="0"  onclick="custom_semi_grand(0)" <?php if(isset($_POST['custom_semi_grand'])) { if($_POST['custom_semi_grand']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_semi_grand1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_semi_grand" id="custom_semi_grand2" value="1" onclick="custom_semi_grand(1)" <?php if(isset($_POST['custom_semi_grand'])) { if($_POST['custom_semi_grand']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_semi_grand2">Disable</label>
														</div>
														<input type="hidden" name="custom_semi_grand" id="custom_semi_grand"  value="<?php if(isset($_POST['custom_semi_grand'])) { if($_POST['custom_semi_grand']!=''){ echo $_POST['custom_semi_grand']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_semi_grand'])){ echo $_SESSION['error']['custom_semi_grand'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Grand</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_grand" id="custom_grand1" value="0"  onclick="custom_grand('0')" <?php if(isset($_POST['custom_grand'])) { if($_POST['custom_grand']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_grand1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_grand" id="custom_grand2" value="1" onclick="custom_grand('1')" <?php if(isset($_POST['custom_grand'])) { if($_POST['custom_grand']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_grand2">Disable</label>
														</div>
														<input type="hidden" name="custom_grand" id="custom_grand"  value="<?php if(isset($_POST['custom_grand'])) { if($_POST['custom_grand']!=''){ echo $_POST['custom_grand']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_grand'])){ echo $_SESSION['error']['custom_grand'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Completed</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_completed" id="custom_completed1" value="0"  onclick="custom_completed(0)" <?php if(isset($_POST['custom_completed'])) { if($_POST['custom_completed']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_completed1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_completed" id="custom_completed2" value="1" onclick="custom_completed(1)" <?php if(isset($_POST['custom_completed'])) { if($_POST['custom_completed']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_completed2">Disable</label>
														</div>
														<input type="hidden" name="custom_completed" id="custom_completed"  value="<?php if(isset($_POST['custom_completed'])) { if($_POST['custom_completed']!=''){ echo $_POST['custom_completed']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_completed'])){ echo $_SESSION['error']['custom_completed'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Completed Analysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_completed_analysis" id="custom_completed_analysis1" value="0"  onclick="custom_completed_analysis(0)" <?php if(isset($_POST['custom_completed_analysis'])) { if($_POST['custom_completed_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_completed_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_completed_analysis" id="custom_completed_analysis2" value="1" onclick="custom_completed_analysis(1)" <?php if(isset($_POST['custom_completed_analysis'])) { if($_POST['custom_completed_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_completed_analysis2">Disable</label>
														</div>
														<input type="hidden" name="custom_completed_analysis" id="custom_completed_analysis"  value="<?php if(isset($_POST['custom_completed_analysis'])) { if($_POST['custom_completed_analysis']!=''){ echo $_POST['custom_completed_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_completed_analysis'])){ echo $_SESSION['error']['custom_completed_analysis'];}?></span>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Error Chapter</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_chapter" id="error_chapter1" value="0"  onclick="error_chapter(0)" <?php if(isset($_POST['error_chapter'])) { if($_POST['error_chapter']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="error_chapter1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_chapter" id="error_chapter2" value="1" onclick="error_chapter(1)" <?php if(isset($_POST['error_chapter'])) { if($_POST['error_chapter']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_chapter2">Disable</label>
														</div>
														<input type="hidden" name="error_chapter" id="error_chapter"  value="<?php if(isset($_POST['error_chapter'])) { if($_POST['error_chapter']!=''){ echo $_POST['error_chapter']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['error_chapter'])){ echo $_SESSION['error']['error_chapter'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Error Cumulative</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_cumulative" id="error_cumulative1" value="0"  onclick="error_cumulative(0)" <?php if(isset($_POST['error_cumulative'])) { if($_POST['error_cumulative']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="error_cumulative1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_cumulative" id="error_cumulative2" value="1" onclick="error_cumulative(1)" <?php if(isset($_POST['error_cumulative'])) { if($_POST['error_cumulative']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_cumulative2">Disable</label>
														</div>
														<input type="hidden" name="error_cumulative" id="error_cumulative"  value="<?php if(isset($_POST['error_cumulative'])) { if($_POST['error_cumulative']!=''){ echo $_POST['error_cumulative']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['error_cumulative'])){ echo $_SESSION['error']['error_cumulative'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Error Completed</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_completed" id="error_completed1" value="0"  onclick="error_completed(0)" <?php if(isset($_POST['error_completed'])) { if($_POST['error_completed']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="error_completed1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_completed" id="error_completed2" value="1" onclick="error_completed(1)" <?php if(isset($_POST['error_completed'])) { if($_POST['error_completed']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_completed2">Disable</label>
														</div>
														<input type="hidden" name="error_completed" id="error_completed"  value="<?php if(isset($_POST['error_completed'])) { if($_POST['error_completed']!=''){ echo $_POST['error_completed']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['error_completed'])){ echo $_SESSION['error']['error_completed'];}?></span>
												</div>
											</div>
										</div>
										
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Error Completed Analysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_completed_analysis" id="error_completed_analysis1" value="0"  onclick="error_completed_analysis(0)" <?php if(isset($_POST['error_completed_analysis'])) { if($_POST['error_completed_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="error_completed_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_completed_analysis" id="error_completed_analysis2" value="1" onclick="error_completed_analysis(1)" <?php if(isset($_POST['error_completed_analysis'])) { if($_POST['error_completed_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_completed_analysis2">Disable</label>
														</div>
														<input type="hidden" name="error_completed_analysis" id="error_completed_analysis"  value="<?php if(isset($_POST['error_completed_analysis'])) { if($_POST['error_completed_analysis']!=''){ echo $_POST['error_completed_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['error_completed_analysis'])){ echo $_SESSION['error']['error_completed_analysis'];}?></span>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Custom</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_custom" id="previous_custom1" value="0"  onclick="previous_custom(0)"  <?php if(isset($_POST['previous_custom'])) { if($_POST['previous_custom']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?> >
														  <label class="form-check-label" for="previous_custom1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_custom" id="previous_custom2" value="1" onclick="previous_custom(1)" <?php if(isset($_POST['previous_custom'])) { if($_POST['previous_custom']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_custom2">Disable</label>
														</div>
														<input type="hidden" name="previous_custom" id="previous_custom"  value="<?php if(isset($_POST['previous_custom'])) { if($_POST['previous_custom']!=''){ echo $_POST['previous_custom']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_custom'])){ echo $_SESSION['error']['previous_custom'];}?></span>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Single</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_single" id="previous_single1" value="0"  onclick="previous_single(0)" <?php if(isset($_POST['previous_single'])) { if($_POST['previous_single']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="previous_single1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_single" id="previous_single2" value="1" onclick="previous_single(1)" <?php if(isset($_POST['previous_single'])) { if($_POST['previous_single']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_single2">Disable</label>
														</div>
														<input type="hidden" name="previous_single" id="previous_single"   value="<?php if(isset($_POST['previous_single'])) { if($_POST['previous_single']!=''){ echo $_POST['previous_single']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_single'])){ echo $_SESSION['error']['previous_single'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Completed</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_completed" id="previous_completed1" value="0"  onclick="previous_completed(0)" <?php if(isset($_POST['previous_completed'])) { if($_POST['previous_completed']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="previous_completed1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_completed" id="previous_completed2" value="1" onclick="previous_completed(1)" <?php if(isset($_POST['previous_completed'])) { if($_POST['previous_completed']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_completed2">Disable</label>
														</div>
														<input type="hidden" name="previous_completed" id="previous_completed"  value="<?php if(isset($_POST['previous_completed'])) { if($_POST['previous_completed']!=''){ echo $_POST['previous_completed']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_completed'])){ echo $_SESSION['error']['previous_completed'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Completed Aanalysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_completed_analysis" id="previous_completed_analysis1" value="0" checked onclick="previous_completed_analysis(0)" <?php if(isset($_POST['previous_completed_analysis'])) { if($_POST['previous_completed_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="previous_completed_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_completed_analysis" id="previous_completed_analysis2" value="1" onclick="previous_completed_analysis(1)" <?php if(isset($_POST['previous_completed_analysis'])) { if($_POST['previous_completed_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_completed_analysis2">Disable</label>
														</div>
														<input type="hidden" name="previous_completed_analysis" id="previous_completed_analysis"   value="<?php if(isset($_POST['previous_completed_analysis'])) { if($_POST['previous_completed_analysis']!=''){ echo $_POST['previous_completed_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_completed_analysis'])){ echo $_SESSION['error']['previous_completed_analysis'];}?></span>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Schedule Exam Completed</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_exam_completed" id="schedule_exam_completed1" value="0" checked onclick="schedule_exam_completed(0)" <?php if(isset($_POST['schedule_exam_completed'])) { if($_POST['schedule_exam_completed']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="schedule_exam_completed1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_exam_completed" id="schedule_exam_completed2" value="1" onclick="schedule_exam_completed(1)" <?php if(isset($_POST['schedule_exam_completed'])) { if($_POST['schedule_exam_completed']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="schedule_exam_completed2">Disable</label>
														</div>
														<input type="hidden" name="schedule_exam_completed" id="schedule_exam_completed"  value="<?php if(isset($_POST['schedule_exam_completed'])) { if($_POST['schedule_exam_completed']!=''){ echo $_POST['schedule_exam_completed']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['schedule_exam_completed'])){ echo $_SESSION['error']['schedule_exam_completed'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Schedule Exam Completed Analysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_completed_analysis" id="schedule_completed_analysis1" value="0"  onclick="schedule_completed_analysis(0)" <?php if(isset($_POST['schedule_completed_analysis'])) { if($_POST['schedule_completed_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="schedule_completed_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_completed_analysis" id="schedule_completed_analysis2" value="1" onclick="schedule_completed_analysis(1)" <?php if(isset($_POST['schedule_completed_analysis'])) { if($_POST['schedule_completed_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="schedule_completed_analysis2">Disable</label>
														</div>
														<input type="hidden" name="schedule_completed_analysis" id="schedule_completed_analysis"  value="<?php if(isset($_POST['schedule_completed_analysis'])) { if($_POST['schedule_completed_analysis']!=''){ echo $_POST['schedule_completed_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['schedule_completed_analysis'])){ echo $_SESSION['error']['schedule_completed_analysis'];}?></span>
												</div>
											</div>
										</div>

										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Schedule Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_exam" id="schedule_exam1" value="0"  onclick="schedule_exam(0)" <?php if(isset($_POST['schedule_exam'])) { if($_POST['schedule_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="schedule_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="schedule_exam" id="schedule_exam2" value="1" onclick="schedule_exam(1)" <?php if(isset($_POST['schedule_exam'])) { if($_POST['schedule_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="schedule_exam2">Disable</label>
														</div>
														<input type="hidden" name="schedule_exam" id="schedule_exam"  value="<?php if(isset($_POST['schedule_exam'])) { if($_POST['schedule_exam']!=''){ echo $_POST['schedule_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['schedule_exam'])){ echo $_SESSION['error']['schedule_exam'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Custom Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_exam" id="custom_exam1" value="0"  onclick="custom_exam(0)" <?php if(isset($_POST['custom_exam'])) { if($_POST['custom_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="custom_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="custom_exam" id="custom_exam2" value="1" onclick="custom_exam(1)"  <?php if(isset($_POST['custom_exam'])) { if($_POST['custom_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="custom_exam2">Disable</label>
														</div>
														<input type="hidden" name="custom_exam" id="custom_exam"  value="<?php if(isset($_POST['custom_exam'])) { if($_POST['custom_exam']!=''){ echo $_POST['custom_exam']; }else { echo '0';} }else{ echo '0'; }?>"  >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['custom_exam'])){ echo $_SESSION['error']['custom_exam'];}?></span>
												</div>
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Previous Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_exam" id="previous_exam1" value="0"  onclick="previous_exam(0)" <?php if(isset($_POST['previous_exam'])) { if($_POST['previous_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="previous_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_exam" id="previous_exam2" value="1" onclick="previous_exam(1)" <?php if(isset($_POST['previous_exam'])) { if($_POST['previous_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_exam2">Disable</label>
														</div>
														<input type="hidden" name="previous_exam" id="previous_exam"  value="<?php if(isset($_POST['previous_exam'])) { if($_POST['previous_exam']!=''){ echo $_POST['previous_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_exam'])){ echo $_SESSION['error']['previous_exam'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Practice Completed </label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="practise_completed" id="practise_completed1" value="0"  onclick="practise_completed(0)" <?php if(isset($_POST['practise_completed'])) { if($_POST['practise_completed']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="practise_completed1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="practise_completed" id="practise_completed2" value="1" onclick="practise_completed(1)" <?php if(isset($_POST['practise_completed'])) { if($_POST['practise_completed']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="practise_completed2">Disable</label>
														</div>
														<input type="hidden" name="practise_completed" id="practise_completed"  value="<?php if(isset($_POST['practise_completed'])) { if($_POST['practise_completed']!=''){ echo $_POST['practise_completed']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['practise_completed'])){ echo $_SESSION['error']['practise_completed'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Practice Completed Analysis</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="practise_completed_analysis" id="practise_completed_analysis1" value="0"  onclick="practise_completed_analysis(0)" <?php if(isset($_POST['practise_completed_analysis'])) { if($_POST['practise_completed_analysis']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="practise_completed_analysis1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="practise_completed_analysis" id="practise_completed_analysis2" value="1" onclick="practise_completed_analysis(1)" <?php if(isset($_POST['practise_completed_analysis'])) { if($_POST['practise_completed_analysis']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="practise_completed_analysis2">Disable</label>
														</div>
														<input type="hidden" name="practise_completed_analysis" id="practise_completed_analysis"  value="<?php if(isset($_POST['practise_completed_analysis'])) { if($_POST['practise_completed_analysis']!=''){ echo $_POST['practise_completed_analysis']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['practise_completed_analysis'])){ echo $_SESSION['error']['practise_completed_analysis'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice  Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_practice_exam" id="lp_practice_exam1" value="0"  onclick="lp_practice_exam(0)" <?php if(isset($_POST['lp_practice_exam'])) { if($_POST['lp_practice_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_practice_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_practice_exam" id="lp_practice_exam2" value="1" onclick="lp_practice_exam(1)" <?php if(isset($_POST['lp_practice_exam'])) { if($_POST['lp_practice_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_practice_exam2">Disable</label>
														</div>
														<input type="hidden" name="lp_practice_exam" id="lp_practice_exam"  value="<?php if(isset($_POST['lp_practice_exam'])) { if($_POST['lp_practice_exam']!=''){ echo $_POST['lp_practice_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_practice_exam'])){ echo $_SESSION['error']['lp_practice_exam'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice  Filter</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_practice_filter" id="lp_practice_filter1" value="0"  onclick="lp_practice_filter(0)" <?php if(isset($_POST['lp_practice_filter'])) { if($_POST['lp_practice_filter']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_practice_filter1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_practice_filter" id="lp_practice_filter2" value="1" onclick="lp_practice_filter(1)" <?php if(isset($_POST['lp_practice_filter'])) { if($_POST['lp_practice_filter']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_practice_filter2">Disable</label>
														</div>
														<input type="hidden" name="lp_practice_filter" id="lp_practice_filter"  value="<?php if(isset($_POST['lp_practice_filter'])) { if($_POST['lp_practice_filter']!=''){ echo $_POST['lp_practice_filter']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_practice_filter'])){ echo $_SESSION['error']['lp_practice_filter'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice  Custom Content</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_custom_content" id="lp_custom_content1" value="0"  onclick="lp_custom_content(0)" <?php if(isset($_POST['lp_custom_content'])) { if($_POST['lp_custom_content']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_custom_content1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_custom_content" id="lp_custom_content2" value="1" onclick="lp_custom_content(1)" <?php if(isset($_POST['lp_custom_content'])) { if($_POST['lp_custom_content']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_custom_content2">Disable</label>
														</div>
														<input type="hidden" name="lp_custom_content" id="lp_custom_content"  value="<?php if(isset($_POST['lp_custom_content'])) { if($_POST['lp_custom_content']!=''){ echo $_POST['lp_custom_content']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_custom_content'])){ echo $_SESSION['error']['lp_custom_content'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Error Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_error_exam" id="lp_error_exam1" value="0"  onclick="lp_error_exam(0)" <?php if(isset($_POST['lp_error_exam'])) { if($_POST['lp_error_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_error_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_error_exam" id="lp_error_exam2" value="1" onclick="lp_error_exam(1)" <?php if(isset($_POST['lp_error_exam'])) { if($_POST['lp_error_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_error_exam2">Disable</label>
														</div>
														<input type="hidden" name="lp_error_exam" id="lp_error_exam"  value="<?php if(isset($_POST['lp_error_exam'])) { if($_POST['lp_error_exam']!=''){ echo $_POST['lp_error_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_error_exam'])){ echo $_SESSION['error']['lp_error_exam'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Topics</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topics" id="lp_topics1" value="0"  onclick="lp_topics(0)" <?php if(isset($_POST['lp_topics'])) { if($_POST['lp_topics']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_topics1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topics" id="lp_topics2" value="1" onclick="lp_topics(1)" <?php if(isset($_POST['lp_topics'])) { if($_POST['lp_topics']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_topics2">Disable</label>
														</div>
														<input type="hidden" name="lp_topics" id="lp_topics"  value="<?php if(isset($_POST['lp_topics'])) { if($_POST['lp_topics']!=''){ echo $_POST['lp_topics']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_topics'])){ echo $_SESSION['error']['lp_topics'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Chapter Dashboard</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_chapter_dashboard" id="lp_chapter_dashboard1" value="0"  onclick="lp_chapter_dashboard(0)" <?php if(isset($_POST['lp_chapter_dashboard'])) { if($_POST['lp_chapter_dashboard']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_chapter_dashboard1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_chapter_dashboard" id="lp_chapter_dashboard2" value="1" onclick="lp_chapter_dashboard(1)" <?php if(isset($_POST['lp_chapter_dashboard'])) { if($_POST['lp_chapter_dashboard']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_chapter_dashboard2">Disable</label>
														</div>
														<input type="hidden" name="lp_chapter_dashboard" id="lp_chapter_dashboard" value="<?php if(isset($_POST['lp_chapter_dashboard'])) { if($_POST['lp_chapter_dashboard']!=''){ echo $_POST['lp_chapter_dashboard']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_chapter_dashboard'])){ echo $_SESSION['error']['lp_chapter_dashboard'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Topic Dashboard</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_dashboard" id="lp_topic_dashboard1" value="0"  onclick="lp_topic_dashboard(0)" <?php if(isset($_POST['lp_topic_dashboard'])) { if($_POST['lp_topic_dashboard']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_topic_dashboard1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_dashboard" id="lp_topic_dashboard2" value="1" onclick="lp_topic_dashboard(1)" <?php if(isset($_POST['lp_topic_dashboard'])) { if($_POST['lp_topic_dashboard']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_topic_dashboard2">Disable</label>
														</div>
														<input type="hidden" name="lp_topic_dashboard" id="lp_topic_dashboard"  value="<?php if(isset($_POST['lp_topic_dashboard'])) { if($_POST['lp_topic_dashboard']!=''){ echo $_POST['lp_topic_dashboard']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_topic_dashboard'])){ echo $_SESSION['error']['lp_topic_dashboard'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Topic practice Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topics_practice_exam" id="lp_topics_practice_exam1" value="0"  onclick="lp_topics_practice_exam(0)" <?php if(isset($_POST['lp_topics_practice_exam'])) { if($_POST['lp_topics_practice_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_topics_practice_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topics_practice_exam" id="lp_topics_practice_exam2" value="1" onclick="lp_topics_practice_exam(1)" <?php if(isset($_POST['lp_topics_practice_exam'])) { if($_POST['lp_topics_practice_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_topics_practice_exam2">Disable</label>
														</div>
														<input type="hidden" name="lp_topics_practice_exam" id="lp_topics_practice_exam"  value="<?php if(isset($_POST['lp_topics_practice_exam'])) { if($_POST['lp_topics_practice_exam']!=''){ echo $_POST['lp_topics_practice_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_topics_practice_exam'])){ echo $_SESSION['error']['lp_topics_practice_exam'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Topic Custom Content</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_custom_content" id="lp_topic_custom_content1" value="0"  onclick="lp_topic_custom_content(0)"  <?php if(isset($_POST['lp_topic_custom_content'])) { if($_POST['lp_topic_custom_content']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_topic_custom_content1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_custom_content" id="lp_topic_custom_content2" value="1" onclick="lp_topic_custom_content(1)" <?php if(isset($_POST['lp_topic_custom_content'])) { if($_POST['lp_topic_custom_content']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_topic_custom_content2">Disable</label>
														</div>
														<input type="hidden" name="lp_topic_custom_content" id="lp_topic_custom_content" value="<?php if(isset($_POST['lp_topic_custom_content'])) { if($_POST['lp_topic_custom_content']!=''){ echo $_POST['lp_topic_custom_content']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_topic_custom_content'])){ echo $_SESSION['error']['lp_topic_custom_content'];}?></span>
												</div>
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Learn&Practice Topic Error Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_error_exam" id="lp_topic_error_exam1" value="0"  onclick="lp_topic_error_exam(0)" <?php if(isset($_POST['lp_topic_error_exam'])) { if($_POST['lp_topic_error_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="lp_topic_error_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="lp_topic_error_exam" id="lp_topic_error_exam2" value="1" onclick="lp_topic_error_exam(1)" <?php if(isset($_POST['lp_topic_error_exam'])) { if($_POST['lp_topic_error_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="lp_topic_error_exam2">Disable</label>
														</div>
														<input type="hidden" name="lp_topic_error_exam" id="lp_topic_error_exam"  value="<?php if(isset($_POST['lp_topic_error_exam'])) { if($_POST['lp_topic_error_exam']!=''){ echo $_POST['lp_topic_error_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['lp_topic_error_exam'])){ echo $_SESSION['error']['lp_topic_error_exam'];}?></span>
												</div>
											</div>
										</div>
										
										
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Institute Custom Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ins_custom_exam" id="ins_custom_exam1" value="0"  onclick="ins_custom_exam(0)" <?php if(isset($_POST['ins_custom_exam'])) { if($_POST['ins_custom_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ins_custom_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ins_custom_exam" id="ins_custom_exam2" value="1" onclick="ins_custom_exam(1)" <?php if(isset($_POST['ins_custom_exam'])) { if($_POST['ins_custom_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ins_custom_exam2">Disable</label>
														</div>
														
														<input type="hidden" name="ins_custom_exam" id="ins_custom_exam"  value="<?php if(isset($_POST['ins_custom_exam'])) { if($_POST['ins_custom_exam']!=''){ echo $_POST['ins_custom_exam']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ins_custom_exam'])){ echo $_SESSION['error']['ins_custom_exam'];}?></span>
												</div>
											</div>
										</div>
									
										<div class="col-lg-6">
											<div class="form-group row">
												<label class="col-sm-6 col-form-label">Institute Previous Exam</label>
												<div class="col-sm-6">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ins_previous_exam" id="ins_previous_exam1" value="0"  onclick="ins_previous_exams('0')" <?php if(isset($_POST['ins_previous_exam'])) { if($_POST['ins_previous_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ins_previous_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ins_previous_exam" id="ins_previous_exam2" value="1" onclick="ins_previous_exams('1')" <?php if(isset($_POST['ins_previous_exam'])) { if($_POST['ins_previous_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ins_previous_exam2">Disable</label>
														</div>
														
														<input type="hidden" name="ins_previous_exam" id="ins_previous_exam"  value="<?php if(isset($_POST['ins_previous_exam'])) { if($_POST['ins_previous_exam']!=''){ echo $_POST['ins_previous_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ins_previous_exam'])){ echo $_SESSION['error']['ins_previous_exam'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Learn Tab </label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="learn_tab" id="learn_tab1" value="0"  onclick="learn_tab('0')" <?php if(isset($_POST['learn_tab'])) { if($_POST['learn_tab']=="0") { echo 'checked'; }else { echo '';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="learn_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="learn_tab" id="learn_tab2" value="1" <?php if(isset($_POST['learn_tab'])){ if($_POST['learn_tab']=="1") { echo 'checked'; }else{}  }else{} ?> onclick="learn_tab('1')" >
														  <label class="form-check-label" for="learn_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="learn_tab" id="learn_tab3" value="2" onclick="learn_tab('2')" <?php if(isset($_POST['learn_tab'])){ if($_POST['learn_tab']!=0 && $_POST['learn_tab']!=1) { echo 'checked'; }else{}  }else{}  ?>   >
														  <label class="form-check-label" for="learn_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="learn_tab" id="learn_tab"  value="<?php if(isset($_POST['learn_tab'])) { if($_POST['learn_tab']!=''){ echo $_POST['learn_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['learn_tab'])){ echo $_SESSION['error']['learn_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Exam Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="exam_tab" id="exam_tab1" value="0"  onclick="exam_tab('0')" <?php if(isset($_POST['exam_tab'])) { if($_POST['exam_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="exam_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="exam_tab" id="exam_tab2" value="1" onclick="exam_tab('1')" <?php if(isset($_POST['exam_tab'])) { if($_POST['exam_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="exam_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="exam_tab" id="exam_tab3" value="2" onclick="exam_tab('2')" <?php if(isset($_POST['exam_tab'])) { if($_POST['exam_tab']!=0 && $_POST['exam_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="exam_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="exam_tab" id="exam_tab"  value="<?php if(isset($_POST['exam_tab'])) { if($_POST['exam_tab']!=''){ echo $_POST['exam_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['exam_tab'])){ echo $_SESSION['error']['exam_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Error Exam</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_exam" id="error_exam1" value="0"  onclick="error_exam('0')" <?php if(isset($_POST['error_exam'])) { if($_POST['error_exam']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="error_exam1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_exam" id="error_exam2" value="1" onclick="error_exam('1')" <?php if(isset($_POST['error_exam'])) { if($_POST['error_exam']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_exam2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="error_exam" id="error_exam3" value="2" onclick="error_exam('2')" <?php if(isset($_POST['error_exam'])) { if($_POST['error_exam']!=0 && $_POST['error_exam']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="error_exam3">Completely Hidden</label>
														</div>
														<input type="hidden" name="error_exam" id="error_exam"  value="<?php if(isset($_POST['error_exam'])) { if($_POST['error_exam']!=''){ echo $_POST['error_exam']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['error_exam'])){ echo $_SESSION['error']['error_exam'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Get Ready Exam Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ready_exam_tab" id="ready_exam_tab1" value="0"  onclick="ready_exam_tab('0');" <?php if(isset($_POST['ready_exam_tab'])) { if($_POST['ready_exam_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="ready_exam_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ready_exam_tab" id="ready_exam_tab2" value="1" onclick="ready_exam_tab('1');" <?php if(isset($_POST['ready_exam_tab'])) { if($_POST['ready_exam_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ready_exam_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="ready_exam_tab" id="ready_exam_tab3" value="2" onclick="ready_exam_tab('2')" <?php if(isset($_POST['ready_exam_tab'])) { if($_POST['ready_exam_tab']!=0 && $_POST['ready_exam_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="ready_exam_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="ready_exam_tab" id="ready_exam_tab"  value="<?php if(isset($_POST['ready_exam_tab'])) { if($_POST['ready_exam_tab']!=''){ echo $_POST['ready_exam_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['ready_exam_tab'])){ echo $_SESSION['error']['ready_exam_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Analysis Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="analysis_tab" id="analysis_tab1" value="0"  onclick="analysis_tab('0');" <?php if(isset($_POST['analysis_tab'])) { if($_POST['analysis_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="analysis_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="analysis_tab" id="analysis_tab2" value="1" onclick="analysis_tab('1');" <?php if(isset($_POST['analysis_tab'])) { if($_POST['analysis_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="analysis_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="analysis_tab" id="analysis_tab3" value="2" onclick="analysis_tab('2')" <?php if(isset($_POST['analysis_tab'])) { if($_POST['analysis_tab']!=0 && $_POST['analysis_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="analysis_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="analysis_tab" id="analysis_tab"  value="<?php if(isset($_POST['analysis_tab'])) { if($_POST['analysis_tab']!=''){ echo $_POST['analysis_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['analysis_tab'])){ echo $_SESSION['error']['analysis_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Bookmarks Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="bookmarks_tab" id="bookmarks_tab1" value="0"  onclick="bookmarks_tab('0');" <?php if(isset($_POST['bookmarks_tab'])) { if($_POST['bookmarks_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="bookmarks_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="bookmarks_tab" id="bookmarks_tab2" value="1" onclick="bookmarks_tab('1');" <?php if(isset($_POST['bookmarks_tab'])) { if($_POST['bookmarks_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="bookmarks_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="bookmarks_tab" id="bookmarks_tab3" value="2" onclick="bookmarks_tab('2');" <?php if(isset($_POST['bookmarks_tab'])) { if($_POST['bookmarks_tab']!=0 && $_POST['bookmarks_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="bookmarks_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="bookmarks_tab" id="bookmarks_tab"  value="<?php if(isset($_POST['bookmarks_tab'])) { if($_POST['bookmarks_tab']!=''){ echo $_POST['bookmarks_tab']; }else { echo '0';} }else{ echo '0'; }?>"  >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['bookmarks_tab'])){ echo $_SESSION['error']['bookmarks_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Notes Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="notes_tab" id="notes_tab1" value="0"  onclick="notes_tab('0')" <?php if(isset($_POST['notes_tab'])) { if($_POST['notes_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="notes_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="notes_tab" id="notes_tab2" value="1" onclick="notes_tab('1')" <?php if(isset($_POST['notes_tab'])) { if($_POST['notes_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="notes_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="notes_tab" id="notes_tab3" value="2" onclick="notes_tab('2')" <?php if(isset($_POST['notes_tab'])) { if($_POST['notes_tab']!=0 && $_POST['notes_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="notes_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="notes_tab" id="notes_tab"  value="<?php if(isset($_POST['notes_tab'])) { if($_POST['notes_tab']!=''){ echo $_POST['notes_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['notes_tab'])){ echo $_SESSION['error']['notes_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Profile Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="profile_tab" id="profile_tab1" value="0"  onclick="profile_tab('0')" <?php if(isset($_POST['profile_tab'])) { if($_POST['profile_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="profile_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="profile_tab" id="profile_tab2" value="1" onclick="profile_tab('1')" <?php if(isset($_POST['profile_tab'])) { if($_POST['profile_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="profile_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="profile_tab" id="profile_tab3" value="2" onclick="profile_tab('2')" <?php if(isset($_POST['profile_tab'])) { if($_POST['profile_tab']!=0 && $_POST['profile_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>> 
														  <label class="form-check-label" for="profile_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="profile_tab" id="profile_tab"  value="<?php if(isset($_POST['profile_tab'])) { if($_POST['profile_tab']!=''){ echo $_POST['profile_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['profile_tab'])){ echo $_SESSION['error']['profile_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Doubts Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="doubts_tab" id="doubts_tab1" value="0"  onclick="doubts_tab('0')" <?php if(isset($_POST['doubts_tab'])) { if($_POST['doubts_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="doubts_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="doubts_tab" id="doubts_tab2" value="1" onclick="doubts_tab('1')" <?php if(isset($_POST['doubts_tab'])) { if($_POST['doubts_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="doubts_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="doubts_tab" id="doubts_tab3" value="2" onclick="doubts_tab('2')" <?php if(isset($_POST['doubts_tab'])) { if($_POST['doubts_tab']!=0 && $_POST['doubts_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="doubts_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="doubts_tab" id="doubts_tab"  value="<?php if(isset($_POST['doubts_tab'])) { if($_POST['doubts_tab']!=''){ echo $_POST['doubts_tab']; }else { echo '0';} }else{ echo '0'; }?>"  >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['doubts_tab'])){ echo $_SESSION['error']['doubts_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Feedback Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="feedback_tab" id="feedback_tab1" value="0"  onclick="feedback_tab('0')" <?php if(isset($_POST['feedback_tab'])) { if($_POST['feedback_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="feedback_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="feedback_tab" id="feedback_tab2" value="1" onclick="feedback_tab('1')" <?php if(isset($_POST['feedback_tab'])) { if($_POST['feedback_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="feedback_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="feedback_tab" id="feedback_tab3" value="2" onclick="feedback_tab('2')" <?php if(isset($_POST['feedback_tab'])) { if($_POST['feedback_tab']!=0 && $_POST['feedback_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="feedback_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="feedback_tab" id="feedback_tab"  value="<?php if(isset($_POST['feedback_tab'])) { if($_POST['feedback_tab']!=''){ echo $_POST['feedback_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['feedback_tab'])){ echo $_SESSION['error']['feedback_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Package Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="package_tab" id="package_tab1" value="0"  onclick="package_tab('0')" <?php if(isset($_POST['package_tab'])) { if($_POST['package_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="package_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="package_tab" id="package_tab2" value="1" onclick="package_tab('1')"  <?php if(isset($_POST['package_tab'])) { if($_POST['package_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="package_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="package_tab" id="package_tab3" value="2" onclick="package_tab('2')"  <?php if(isset($_POST['package_tab'])) { if($_POST['package_tab']!=0 && $_POST['package_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="package_tab3"> Completely Hidden</label>
														</div>
														<input type="hidden" name="package_tab" id="package_tab"  value="<?php if(isset($_POST['package_tab'])) { if($_POST['package_tab']!=''){ echo $_POST['package_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['package_tab'])){ echo $_SESSION['error']['package_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Home Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="home_tab" id="home_tab1" value="0"  onclick="home_tab('0')" <?php if(isset($_POST['home_tab'])) { if($_POST['home_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="home_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="home_tab" id="home_tab2" value="1" onclick="home_tab('1')" <?php if(isset($_POST['home_tab'])) { if($_POST['home_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="home_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="home_tab" id="home_tab3" value="2" onclick="home_tab('2')" <?php if(isset($_POST['home_tab'])) { if($_POST['home_tab']!=0 && $_POST['home_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="home_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="home_tab" id="home_tab"  value="<?php if(isset($_POST['home_tab'])) { if($_POST['home_tab']!=''){ echo $_POST['home_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['home_tab'])){ echo $_SESSION['error']['home_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Revision Material Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="revision_material_tab" id="revision_material_tab1" value="0"  onclick="revision_material_tab('0')" <?php if(isset($_POST['revision_material_tab'])) { if($_POST['revision_material_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="revision_material_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="revision_material_tab" id="revision_material_tab2" value="1" onclick="revision_material_tab('1')" <?php if(isset($_POST['revision_material_tab'])) { if($_POST['revision_material_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="revision_material_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="revision_material_tab" id="revision_material_tab3" value="2" onclick="revision_material_tab('2')" <?php if(isset($_POST['revision_material_tab'])) { if($_POST['revision_material_tab']!=0 && $_POST['revision_material_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="revision_material_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="revision_material_tab" id="revision_material_tab"  value="<?php if(isset($_POST['revision_material_tab'])) { if($_POST['revision_material_tab']!=''){ echo $_POST['revision_material_tab']; }else { echo '0';} }else{ echo '0'; }?>" >
													<span class="error text-danger"><?php if(isset($_SESSION['error']['revision_material_tab'])){ echo $_SESSION['error']['revision_material_tab'];}?></span>
												</div>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Previous Paper Analysis Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_paper_analysis_tab" id="previous_paper_analysis_tab1" value="0"  onclick="previous_paper_analysis_tab('0')"  <?php if(isset($_POST['previous_paper_analysis_tab'])) { if($_POST['previous_paper_analysis_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="previous_paper_analysis_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_paper_analysis_tab" id="previous_paper_analysis_tab2" value="1" onclick="previous_paper_analysis_tab('1')" <?php if(isset($_POST['previous_paper_analysis_tab'])) { if($_POST['previous_paper_analysis_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_paper_analysis_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="previous_paper_analysis_tab" id="previous_paper_analysis_tab3" value="2" onclick="previous_paper_analysis_tab('2')" <?php if(isset($_POST['previous_paper_analysis_tab'])) { if($_POST['previous_paper_analysis_tab']!=0 && $_POST['previous_paper_analysis_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="previous_paper_analysis_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="previous_paper_analysis_tab" id="previous_paper_analysis_tab"  value="<?php if(isset($_POST['previous_paper_analysis_tab'])) { if($_POST['previous_paper_analysis_tab']!=''){ echo $_POST['previous_paper_analysis_tab']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['previous_paper_analysis_tab'])){ echo $_SESSION['error']['previous_paper_analysis_tab'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Linkage Type Analysis Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="linkage_analysis_tab" id="linkage_analysis_tab1" value="0"  onclick="linkage_analysis_tab('0')"  <?php if(isset($_POST['linkage_analysis_tab'])) { if($_POST['linkage_analysis_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="linkage_analysis_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="linkage_analysis_tab" id="linkage_analysis_tab2" value="1" onclick="linkage_analysis_tab('1')" <?php if(isset($_POST['linkage_analysis_tab'])) { if($_POST['linkage_analysis_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="linkage_analysis_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="linkage_analysis_tab" id="linkage_analysis_tab3" value="2" onclick="linkage_analysis_tab('2')" <?php if(isset($_POST['linkage_analysis_tab'])) { if($_POST['linkage_analysis_tab']!=0 && $_POST['linkage_analysis_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="linkage_analysis_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="linkage_analysis_tab" id="linkage_analysis_tab"  value="<?php if(isset($_POST['linkage_analysis_tab'])) { if($_POST['linkage_analysis_tab']!=''){ echo $_POST['linkage_analysis_tab']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['linkage_analysis_tab'])){ echo $_SESSION['error']['linkage_analysis_tab'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Videos</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="videos_tab" id="videos_tab1" value="0"  onclick="videos_tab('0')"  <?php if(isset($_POST['videos_tab'])) { if($_POST['videos_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="videos_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="videos_tab" id="videos_tab2" value="1" onclick="videos_tab('1')" <?php if(isset($_POST['videos_tab'])) { if($_POST['videos_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="videos_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="videos_tab" id="linkage_analysis_tab3" value="2" onclick="videos_tab('2')" <?php if(isset($_POST['videos_tab'])) { if($_POST['videos_tab']!=0 && $_POST['videos_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="videos_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="videos_tab" id="videos_tab"  value="<?php if(isset($_POST['videos_tab'])) { if($_POST['videos_tab']!=''){ echo $_POST['videos_tab']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['videos_tab'])){ echo $_SESSION['error']['videos_tab'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Mock Test</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="mock_test" id="mock_test1" value="0"  onclick="mock_test('0')"  <?php if(isset($_POST['mock_test'])) { if($_POST['mock_test']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="mock_test1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="mock_test" id="mock_test2" value="1" onclick="mock_test('1')" <?php if(isset($_POST['mock_test'])) { if($_POST['mock_test']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="mock_test2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="mock_test" id="mock_test3" value="2" onclick="mock_test('2')" <?php if(isset($_POST['mock_test'])) { if($_POST['mock_test']!=0 && $_POST['mock_test']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="mock_test3">Completely Hidden</label>
														</div>
														<input type="hidden" name="mock_test" id="mock_test"  value="<?php if(isset($_POST['mock_test'])) { if($_POST['mock_test']!=''){ echo $_POST['mock_test']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['mock_test'])){ echo $_SESSION['error']['mock_test'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">JEE Mains 2021</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="jee_mains_2021" id="jee_mains_2021_1" value="0"  onclick="jee_mains_2021('0')"  <?php if(isset($_POST['jee_mains_2021'])) { if($_POST['jee_mains_2021']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="jee_mains_2021_1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="jee_mains_2021" id="jee_mains_2021_2" value="1" onclick="jee_mains_2021('1')" <?php if(isset($_POST['jee_mains_2021'])) { if($_POST['jee_mains_2021']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="jee_mains_2021_2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="jee_mains_2021" id="jee_mains_2021_3" value="2" onclick="jee_mains_2021('2')" <?php if(isset($_POST['jee_mains_2021'])) { if($_POST['jee_mains_2021']!=0 && $_POST['jee_mains_2021']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="jee_mains_2021_3">Completely Hidden</label>
														</div>
														<input type="hidden" name="jee_mains_2021" id="jee_mains_2021"  value="<?php if(isset($_POST['jee_mains_2021'])) { if($_POST['jee_mains_2021']!=''){ echo $_POST['jee_mains_2021']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['jee_mains_2021'])){ echo $_SESSION['error']['jee_mains_2021'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Help Videos</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="help_videos" id="help_videos1" value="0"  onclick="help_videos('0')"  <?php if(isset($_POST['help_videos'])) { if($_POST['help_videos']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="help_videos1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="help_videos" id="help_videos2" value="1" onclick="help_videos('1')" <?php if(isset($_POST['help_videos'])) { if($_POST['help_videos']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="help_videos2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="help_videos" id="help_videos3" value="2" onclick="help_videos('2')" <?php if(isset($_POST['help_videos'])) { if($_POST['help_videos']!=0 && $_POST['help_videos']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="help_videos3">Completely Hidden</label>
														</div>
														<input type="hidden" name="help_videos" id="help_videos"  value="<?php if(isset($_POST['help_videos'])) { if($_POST['help_videos']!=''){ echo $_POST['help_videos']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['help_videos'])){ echo $_SESSION['error']['help_videos'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Blog Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="blog_tab" id="blog_tab1" value="0"  onclick="blog_tab('0')"  <?php if(isset($_POST['blog_tab'])) { if($_POST['blog_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="blog_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="blog_tab" id="blog_tab2" value="1" onclick="blog_tab('1')" <?php if(isset($_POST['blog_tab'])) { if($_POST['blog_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="blog_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="blog_tab" id="blog_tab3" value="2" onclick="blog_tab('2')" <?php if(isset($_POST['blog_tab'])) { if($_POST['blog_tab']!=0 && $_POST['blog_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="blog_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="blog_tab" id="blog_tab"  value="<?php if(isset($_POST['blog_tab'])) { if($_POST['blog_tab']!=''){ echo $_POST['blog_tab']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['blog_tab'])){ echo $_SESSION['error']['blog_tab'];}?></span>
												</div>
											</div>
										</div>	
									</div>
									<!-- <div class="row">
										<div class="col-lg-12">
											<div class="form-group row">
												<label class="col-sm-3 col-form-label">Videos Tab</label>
												<div class="col-sm-9">
													<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="video_tab" id="video_tab1" value="0"  onclick="video_tab('0')"  <?php if(isset($_POST['video_tab'])) { if($_POST['video_tab']=='0') { echo 'checked'; }else { echo 'checked';}  } else { echo 'checked';} ?>>
														  <label class="form-check-label" for="video_tab1">Enable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="video_tab" id="video_tab2" value="1" onclick="video_tab('1')" <?php if(isset($_POST['video_tab'])) { if($_POST['video_tab']=='1') { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="video_tab2">Disable</label>
														</div>
														<div class="form-check form-check-inline">
														  <input class="form-check-input" type="radio" name="video_tab" id="video_tab3" value="2" onclick="video_tab('2')" <?php if(isset($_POST['video_tab'])) { if($_POST['video_tab']!=0 && $_POST['video_tab']!=1) { echo 'checked'; }else { echo '';}  } else { echo '';} ?>>
														  <label class="form-check-label" for="video_tab3">Completely Hidden</label>
														</div>
														<input type="hidden" name="video_tab" id="video_tab"  value="<?php if(isset($_POST['video_tab'])) { if($_POST['video_tab']!=''){ echo $_POST['video_tab']; }else { echo '0';} }else{ echo '0'; }?>">
													<span class="error text-danger"><?php if(isset($_SESSION['error']['video_tab'])){ echo $_SESSION['error']['video_tab'];}?></span>
												</div>
											</div>
										</div>	
									</div> -->
									<div class="form-group row pl-3 text-center">
									
										<div class="col-lg-12">
											 <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','validateForm=1&userlevel='+$('#userlevel').val()+'&dataenable='+$('#dataenable').val()+'&institution_id='+$('#institution_id').val()+'&plan_id='+$('#plan_id').val()+'&ra_actions='+$('#ra_actions').val()+'&ra_overall='+$('#ra_overall').val()+'&ra_overall_details='+$('#ra_overall_details').val()+'&ra_subject_strength='+$('#ra_subject_strength').val()+'&ra_subject_strength_details='+$('#ra_subject_strength_details').val()+'&ra_time_analysis='+$('#ra_time_analysis').val()+'&ra_time_analysis_details='+$('#ra_time_analysis_details').val()+'&ra_complexity='+$('#ra_complexity').val()+'&ra_complexity_details='+$('#ra_complexity_details').val()+'&ra_qtheory='+$('#ra_qtheory').val()+'&ra_qtheory_details='+$('#ra_qtheory_details').val()+'&ra_qtype='+$('#ra_qtype').val()+'&ra_qtype_details='+$('#ra_qtype_details').val()+'&custom_chapter='+$('#custom_chapter').val()+'&custom_cumulative='+$('#custom_cumulative').val()+'&custom_semi_grand='+$('#custom_semi_grand').val()+'&custom_grand='+$('#custom_grand').val()+'&custom_completed='+$('#custom_completed').val()+'&custom_completed_analysis='+$('#custom_completed_analysis').val()+'&error_chapter='+$('#error_chapter').val()+'&error_cumulative='+$('#error_cumulative').val()+'&error_completed='+$('#error_completed').val()+'&error_completed_analysis='+$('#error_completed_analysis').val()+'&previous_custom='+$('#previous_custom').val()+'&previous_single='+$('#previous_single').val()+'&previous_completed='+$('#previous_completed').val()+'&previous_completed_analysis='+$('#previous_completed_analysis').val()+'&schedule_exam_completed='+$('#schedule_exam_completed').val()+'&schedule_completed_analysis='+$('#schedule_completed_analysis').val()+'&schedule_exam='+$('#schedule_exam').val()+'&custom_exam='+$('#custom_exam').val()+'&error_exam='+$('#error_exam').val()+'&previous_exam='+$('#previous_exam').val()+'&practise_completed='+$('#practise_completed').val()+'&practise_completed_analysis='+$('#practise_completed_analysis').val()+'&lp_practice_exam='+$('#lp_practice_exam').val()+'&lp_practice_filter='+$('#lp_practice_filter').val()+'&lp_custom_content='+$('#lp_custom_content').val()+'&lp_error_exam='+$('#lp_error_exam').val()+'&lp_topics='+$('#lp_topics').val()+'&lp_chapter_dashboard='+$('#lp_chapter_dashboard').val()+'&lp_topic_dashboard='+$('#lp_topic_dashboard').val()+'&lp_topics_practice_exam='+$('#lp_topics_practice_exam').val()+'&lp_topic_custom_content='+$('#lp_topic_custom_content').val()+'&lp_topic_error_exam='+$('#lp_topic_error_exam').val()+'&learn_tab='+$('#learn_tab').val()+'&exam_tab='+$('#exam_tab').val()+'&ready_exam_tab='+$('#ready_exam_tab').val()+'&analysis_tab='+$('#analysis_tab').val()+'&bookmarks_tab='+$('#bookmarks_tab').val()+'&notes_tab='+$('#notes_tab').val()+'&profile_tab='+$('#profile_tab').val()+'&ins_custom_exam='+$('#ins_custom_exam').val()+'&ins_previous_exam='+$('#ins_previous_exam').val()+'&home_tab='+$('#home_tab').val()+'&home_tab='+$('#home_tab').val()+'&revision_material_tab='+$('#revision_material_tab').val()+'&previous_paper_analysis_tab='+$('#previous_paper_analysis_tab').val()+'&doubts_tab='+$('#doubts_tab').val()+'&feedback_tab='+$('#feedback_tab').val()+'&package_tab='+$('#package_tab').val()+'&linkage_analysis_tab='+$('#linkage_analysis_tab').val()+'&videos_tab='+$('#videos_tab').val()+'&mock_test='+$('#mock_test').val()+'&jee_mains_2021='+$('#jee_mains_2021').val()+'&help_videos='+$('#help_videos').val()+'&blog_tab='+$('#blog_tab').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$_POST['from_page'];}?><?php if(isset($_POST['page'])){ echo '&page='.$_POST['page'];}?>')">Submit</a>
										</div>
									</div>
								</div>
										   
							</div>
							<div id="menu1" class="container tab-pane fade"><br>
									<div class="card border-0" id="ggg">
									
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>

			</div>
		</section>					
									
								
							
		
			
	<?php
		unset($_SESSION['error']);

        unset($_SESSION['description']);
	}
	if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';

  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  
	
	$field = 'institution_id';
	if(!$post['institution_id'] || strlen(trim($post['institution_id'])) == 0){
	  $_SESSION['error'][$field] = "* Institution Id cannot be empty";
	}
	
	
	
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1&userlevel=<?php echo $post['userlevel'];?>&dataenable=<?php echo $post['dataenable'];?>&institution_id=<?php echo $post['institution_id'];?>&plan_id=<?php echo $post['plan_id'];?>&ra_actions=<?php echo $post['ra_actions'];?>&ra_overall=<?php echo $post['ra_overall'];?>&ra_overall_details=<?php echo $post['ra_overall_details'];?>&ra_subject_strength=<?php echo $post['ra_subject_strength'];?>&ra_subject_strength_details=<?php echo $post['ra_subject_strength_details'];?>&ra_time_analysis=<?php echo $post['ra_time_analysis'];?>&ra_time_analysis_details=<?php echo $post['ra_time_analysis_details'];?>&ra_complexity=<?php echo $post['ra_complexity'];?>&ra_complexity_details=<?php echo $post['ra_complexity_details'];?>&ra_qtheory=<?php echo $post['ra_qtheory'];?>&ra_qtheory_details=<?php echo $post['ra_qtheory_details'];?>&ra_qtype=<?php echo $post['ra_qtype'];?>&ra_qtype_details=<?php echo $post['ra_qtype_details'];?>&custom_chapter=<?php echo $post['custom_chapter'];?>&custom_cumulative=<?php echo $post['custom_cumulative'];?>&custom_semi_grand=<?php echo $post['custom_semi_grand'];?>&custom_grand=<?php echo $post['custom_grand'];?>&custom_completed=<?php echo $post['custom_completed'];?>&custom_completed_analysis=<?php echo $post['custom_completed_analysis'];?>&error_chapter=<?php echo $post['error_chapter'];?>&error_cumulative=<?php echo $post['error_cumulative'];?>&error_completed=<?php echo $post['error_completed'];?>&error_completed_analysis=<?php echo $post['error_completed_analysis'];?>&previous_custom=<?php echo $post['previous_custom'];?>&previous_single=<?php echo $post['previous_single'];?>&previous_completed=<?php echo $post['previous_completed'];?>&previous_completed_analysis=<?php echo $post['previous_completed_analysis'];?>&schedule_exam_completed=<?php echo $post['schedule_exam_completed'];?>&schedule_exam=<?php echo $post['schedule_exam'];?>&schedule_completed_analysis=<?php echo $post['schedule_completed_analysis'];?>&custom_exam=<?php echo $post['custom_exam'];?>&error_exam=<?php echo $post['error_exam'];?>&previous_exam=<?php echo $post['previous_exam'];?>&practise_completed=<?php echo $post['practise_completed'];?>&practise_completed_analysis=<?php echo $post['practise_completed_analysis'];?>&lp_practice_exam=<?php echo $post['lp_practice_exam'];?>&lp_practice_filter=<?php echo $post['lp_practice_filter'];?>&lp_custom_content=<?php echo $post['lp_custom_content'];?>&lp_error_exam=<?php echo $post['lp_error_exam'];?>&lp_topics=<?php echo $post['lp_topics'];?>&lp_chapter_dashboard=<?php echo $post['lp_chapter_dashboard'];?>&lp_topic_dashboard=<?php echo $post['lp_topic_dashboard'];?>&lp_topics_practice_exam=<?php echo $post['lp_topics_practice_exam'];?>&lp_topic_custom_content=<?php echo $post['lp_topic_custom_content'];?>&lp_topic_error_exam=<?php echo $post['lp_topic_error_exam'];?>&learn_tab=<?php echo $post['learn_tab'];?>&exam_tab=<?php echo $post['exam_tab'];?>&ready_exam_tab=<?php echo $post['ready_exam_tab'];?>&analysis_tab=<?php echo $post['analysis_tab'];?>&bookmarks_tab=<?php echo $post['bookmarks_tab'];?>&notes_tab=<?php echo $post['notes_tab'];?>&profile_tab=<?php echo $post['profile_tab'];?>&home_tab=<?php echo $post['home_tab'];?>&ins_custom_exam=<?php echo $post['ins_custom_exam'];?>&ins_previous_exam=<?php echo $post['ins_previous_exam'];?>&revision_material_tab=<?php echo $post['revision_material_tab'];?>&previous_paper_analysis_tab=<?php echo $post['previous_paper_analysis_tab'];?>&doubts_tab=<?php echo $post['doubts_tab'];?>&feedback_tab=<?php echo $post['feedback_tab'];?>&package_tab=<?php echo $post['package_tab'];?>&videos_tab=<?php echo $post['videos_tab'];?>&mock_test=<?php echo $post['mock_test'];?>&linkage_analysis_tab=<?php echo $post['linkage_analysis_tab'];?>&blog_tab=<?php echo $post['blog_tab'];?>&jee_mains_2021=<?php echo $post['jee_mains_2021'];?>&help_videos=<?php echo $post['help_videos'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?>')
    </script>
  <?php
	}
	else{
		
		
			
			$jsonData['userlevel'] = 1;
			$jsonData['institution_id'] = $post['institution_id'];
			if($post['plan_id']!=''){ $plan_id=$post['plan_id']; }else{ $plan_id=0; }
			if($post['ra_actions']=='0'){ $jsonData['ra_actions'] = false; }else{  $jsonData['ra_actions'] = true; }
            if($post['ra_overall']=='0'){ $jsonData['ra_overall'] = false; }else{  $jsonData['ra_overall'] = true; }
			if($post['ra_overall_details']=='0'){ $jsonData['ra_overall_details'] = false; }else{  $jsonData['ra_overall_details'] = true; }
			if($post['ra_subject_strength']=='0'){ $jsonData['ra_subject_strength'] = false; }else{  $jsonData['ra_subject_strength'] = true; }
			if($post['ra_subject_strength_details']=='0'){ $jsonData['ra_subject_strength_details'] = false; }else{  $jsonData['ra_subject_strength_details'] = true; }
			if($post['ra_time_analysis']=='0'){ $jsonData['ra_time_analysis'] = false; }else{  $jsonData['ra_time_analysis'] = true; }
			if($post['ra_time_analysis_details']=='0'){ $jsonData['ra_time_analysis_details'] = false; }else{  $jsonData['ra_time_analysis_details'] = true; }
			if($post['ra_complexity']=='0'){ $jsonData['ra_complexity'] = false; }else{  $jsonData['ra_complexity'] = true; }
			if($post['ra_complexity_details']=='0'){ $jsonData['ra_complexity_details'] = false; }else{  $jsonData['ra_complexity_details'] = true; }
			if($post['ra_qtheory']=='0'){ $jsonData['ra_qtheory'] = false; }else{  $jsonData['ra_qtheory'] = true; }
			if($post['ra_qtheory_details']=='0'){ $jsonData['ra_qtheory_details'] = false; }else{  $jsonData['ra_qtheory_details'] = true; }
			if($post['ra_qtype']=='0'){ $jsonData['ra_qtype'] = false; }else{  $jsonData['ra_qtype'] = true; }
			if($post['ra_qtype_details']=='0'){ $jsonData['ra_qtype_details'] = false; }else{  $jsonData['ra_qtype_details'] = true; }
			if($post['custom_completed']=='0'){ $jsonData['custom_completed'] = false; }else{  $jsonData['custom_completed'] = true; }
			if($post['custom_completed_analysis']=='0'){ $jsonData['custom_completed_analysis'] = false; }else{  $jsonData['custom_completed_analysis'] = true; }
			if($post['error_chapter']=='0'){ $jsonData['error_chapter'] = false; }else{  $jsonData['error_chapter'] = true; }
			if($post['error_cumulative']=='0'){ $jsonData['error_cumulative'] = false; }else{  $jsonData['error_cumulative'] = true; }
			if($post['error_completed']=='0'){ $jsonData['error_completed'] = false; }else{  $jsonData['error_completed'] = true; }
			if($post['error_completed_analysis']=='0'){ $jsonData['error_completed_analysis'] = false; }else{  $jsonData['error_completed_analysis'] = true; }
			if($post['previous_custom']=='0'){ $jsonData['previous_custom'] = false; }else{  $jsonData['previous_custom'] = true; }
			if($post['previous_single']=='0'){ $jsonData['previous_single'] = false; }else{  $jsonData['previous_single'] = true; }
			if($post['previous_completed']=='0'){ $jsonData['previous_completed'] = false; }else{  $jsonData['previous_completed'] = true; }
			if($post['previous_completed_analysis']=='0'){ $jsonData['previous_completed_analysis'] = false; }else{  $jsonData['previous_completed_analysis'] = true; }
			if($post['schedule_exam_completed']=='0'){ $jsonData['schedule_exam_completed'] = false; }else{  $jsonData['schedule_exam_completed'] = true; }
			if($post['schedule_completed_analysis']=='0'){ $jsonData['schedule_completed_analysis'] = false; }else{  $jsonData['schedule_completed_analysis'] = true; }
			if($post['schedule_exam']=='0'){ $jsonData['schedule_exam'] = false; }else{  $jsonData['schedule_exam'] = true; }
			if($post['custom_exam']=='0'){ $jsonData['custom_exam'] = false; }else{  $jsonData['custom_exam'] = true; }
			if($post['error_exam']=='0'){ $jsonData['error_exam'] = false; }else{  $jsonData['error_exam'] = true; }
			if($post['previous_exam']=='0'){ $jsonData['previous_exam'] = false; }else{  $jsonData['previous_exam'] = true; }
			if($post['practise_completed']=='0'){ $jsonData['practise_completed'] = false; }else{  $jsonData['practise_completed'] = true; }
			if($post['practise_completed_analysis']=='0'){ $jsonData['practise_completed_analysis'] = false; }else{  $jsonData['practise_completed_analysis'] = true; }
			if($post['lp_practice_exam']=='0'){ $jsonData['lp_practice_exam'] = false; }else{  $jsonData['lp_practice_exam'] = true; }
			if($post['lp_practice_filter']=='0'){ $jsonData['lp_practice_filter'] = false; }else{  $jsonData['lp_practice_filter'] = true; }
			if($post['lp_custom_content']=='0'){ $jsonData['lp_custom_content'] = false; }else{  $jsonData['lp_custom_content'] = true; }
			if($post['lp_error_exam']=='0'){ $jsonData['lp_error_exam'] = false; }else{  $jsonData['lp_error_exam'] = true; }
			if($post['lp_topics']=='0'){ $jsonData['lp_topics'] = false; }else{  $jsonData['lp_topics'] = true; }
			if($post['lp_chapter_dashboard']=='0'){ $jsonData['lp_chapter_dashboard'] = false; }else{  $jsonData['lp_chapter_dashboard'] = true; }
			if($post['lp_topic_dashboard']=='0'){ $jsonData['lp_topic_dashboard'] = false; }else{  $jsonData['lp_topic_dashboard'] = true; }
			if($post['lp_topic_custom_content']=='0'){ $jsonData['lp_topic_custom_content'] = false; }else{  $jsonData['lp_topic_custom_content'] = true; }
			if($post['lp_topics_practice_exam']=='0'){ $jsonData['lp_topics_practice_exam'] = false; }else{  $jsonData['lp_topics_practice_exam'] = true; }
			if($post['lp_topic_error_exam']=='0'){ $jsonData['lp_topic_error_exam'] = false; }else{  $jsonData['lp_topic_error_exam'] = true; }
			if($post['learn_tab']=='0'){ $jsonData['learn_tab'] = false; }else{  $jsonData['learn_tab'] = true; }
			if($post['exam_tab']=='0'){ $jsonData['exam_tab'] = false; }else{  $jsonData['exam_tab'] = true; }
			if($post['ready_exam_tab']=='0'){ $jsonData['ready_exam_tab'] = false; }else{  $jsonData['ready_exam_tab'] = true; }
			if($post['analysis_tab']=='0'){ $jsonData['analysis_tab'] = false; }else{  $jsonData['analysis_tab'] = true; }
			if($post['bookmarks_tab']=='0'){ $jsonData['bookmarks_tab'] = false; }else{  $jsonData['bookmarks_tab'] = true; }
			if($post['notes_tab']=='0'){ $jsonData['notes_tab'] = false; }else{  $jsonData['notes_tab'] = true; }
			if($post['profile_tab']=='0'){ $jsonData['profile_tab'] = false; }else{  $jsonData['profile_tab'] = true; }
			if($post['revision_material_tab']=='0'){ $jsonData['revision_material_tab'] = false; }else{  $jsonData['revision_material_tab'] = true; }
			if($post['previous_paper_analysis_tab']=='0'){ $jsonData['previous_paper_analysis_tab'] = false; }else{  $jsonData['previous_paper_analysis_tab'] = true; }
			if($post['doubts_tab']=='0'){ $jsonData['doubts_tab'] = false; }else{  $jsonData['doubts_tab'] = true; }
			if($post['feedback_tab']=='0'){ $jsonData['feedback_tab'] = false; }else{  $jsonData['feedback_tab'] = true; }
			if($post['package_tab']=='0'){ $jsonData['package_tab'] = false; }else{  $jsonData['package_tab'] = true; }
			if($post['home_tab']=='0'){ $jsonData['home_tab'] = false; }else{  $jsonData['home_tab'] = true; }
			if($post['linkage_analysis_tab']=='0'){ $jsonData['linkage_analysis_tab'] = false; }else{  $jsonData['linkage_analysis_tab'] = true; }
			if($post['videos_tab']=='0'){ $jsonData['videos_tab'] = false; }else{  $jsonData['videos_tab'] = true; }
			if($post['mock_test']=='0'){ $jsonData['mock_test'] = false; }else{  $jsonData['mock_test'] = true; }
			if($post['jee_mains_2021']=='0'){ $jsonData['jee_mains_2021'] = false; }else{  $jsonData['jee_mains_2021'] = true; }
			if($post['help_videos']=='0'){ $jsonData['help_videos'] = false; }else{  $jsonData['help_videos'] = true; }
			if($post['blog_tab']=='0'){ $jsonData['blog_tab'] = false; }else{  $jsonData['blog_tab'] = true; }
			//if($post['video_tab']=='0'){ $jsonData['video_tab'] = false; }else{  $jsonData['video_tab'] = true; }

			if($post['learn_tab']=='0'){
				$jsonData['ins_learn_tab'] = false;
				
			}else if($post['learn_tab']=='1'){
				$jsonData['ins_learn_tab'] = false;
			}else if($post['learn_tab']=='2'){
				$jsonData['ins_learn_tab'] = true;
			}
			if($post['exam_tab']=='0'){
				$jsonData['ins_exam_tab'] = false;
			}else if($post['exam_tab']=='1'){
				$jsonData['ins_exam_tab'] = false;
			}else if($post['exam_tab']=='2'){
				$jsonData['ins_exam_tab'] = true;
			}

			if($post['ready_exam_tab']=='0'){
				$jsonData['ins_ready_exam_tab'] = false;
			}else if($post['ready_exam_tab']=='1'){
				$jsonData['ins_ready_exam_tab'] = false;
			}else if($post['ready_exam_tab']=='2'){
				$jsonData['ins_ready_exam_tab'] = true;
			}
			if($post['analysis_tab']=='0'){
				$jsonData['ins_analysis_tab'] =false;
			}else if($post['analysis_tab']=='1'){
				$jsonData['ins_analysis_tab'] = false;
			}else if($post['analysis_tab']=='2'){
				$jsonData['ins_analysis_tab'] = true;
			}
			if($post['bookmarks_tab']=='0'){
				$jsonData['ins_bookmarks_tab'] = false;
			}else if($post['bookmarks_tab']=='1'){
				$jsonData['ins_bookmarks_tab'] = false;
			}else if($post['bookmarks_tab']=='2'){
				$jsonData['ins_bookmarks_tab'] = true;
			}

			if($post['notes_tab']=='0'){
				$jsonData['ins_notes_tab'] = false;
			}else if($post['notes_tab']=='1'){
				$jsonData['ins_notes_tab'] = false;
			}else if($post['notes_tab']=='2'){
				$jsonData['ins_notes_tab'] = true;
			}
			if($post['profile_tab']=='0'){
				$jsonData['ins_profile_tab'] = false;
			}else if($post['profile_tab']=='1'){
				$jsonData['ins_profile_tab'] = false;
			}else if($post['profile_tab']=='2'){
				$jsonData['ins_profile_tab'] = true;
			}
			if($post['revision_material_tab']=='0'){
				$jsonData['ins_revision_material_tab'] = false;
			}else if($post['revision_material_tab']=='1'){
				$jsonData['ins_revision_material_tab'] = false;
			}else if($post['revision_material_tab']=='2'){
				$jsonData['ins_revision_material_tab'] = true;
			}
			if($post['linkage_analysis_tab']=='0'){
				$jsonData['ins_linkage_analysis_tab'] = false;
			}else if($post['linkage_analysis_tab']=='1'){
				$jsonData['ins_linkage_analysis_tab'] = false;
			}else if($post['linkage_analysis_tab']=='2'){
				$jsonData['ins_linkage_analysis_tab'] = true;
			}
			if($post['videos_tab']=='0'){
				$jsonData['ins_videos_tab'] = false;
			}else if($post['videos_tab']=='1'){
				$jsonData['ins_videos_tab'] = false;
			}else if($post['videos_tab']=='2'){
				$jsonData['ins_videos_tab'] = true;
			}
			if($post['mock_test']=='0'){
				$jsonData['ins_mock_test'] = false;
			}else if($post['mock_test']=='1'){
				$jsonData['ins_mock_test'] = false;
			}else if($post['mock_test']=='2'){
				$jsonData['ins_mock_test'] = true;
			}
			if($post['jee_mains_2021']=='0'){
				$jsonData['ins_jee_mains_2021'] = false;
			}else if($post['jee_mains_2021']=='1'){
				$jsonData['ins_jee_mains_2021'] = false;
			}else if($post['jee_mains_2021']=='2'){
				$jsonData['ins_jee_mains_2021'] = true;
			}
			if($post['help_videos']=='0'){
				$jsonData['ins_help_videos'] = false;
			}else if($post['help_videos']=='1'){
				$jsonData['ins_help_videos'] = false;
			}else if($post['help_videos']=='2'){
				$jsonData['ins_help_videos'] = true;
			}
			if($post['blog_tab']=='0'){
				$jsonData['ins_blog_tab'] = false;
			}else if($post['blog_tab']=='1'){
				$jsonData['ins_blog_tab'] = false;
			}else if($post['blog_tab']=='2'){
				$jsonData['ins_blog_tab'] = true;
			}

			/*if($post['video_tab']=='0'){
				$jsonData['ins_video_tab'] = false;
			}else if($post['video_tab']=='1'){
				$jsonData['ins_video_tab'] = false;
			}else if($post['video_tab']=='2'){
				$jsonData['ins_video_tab'] = true;
			}*/
			if($post['previous_paper_analysis_tab']=='0'){
				$jsonData['ins_previous_paper_analysis_tab'] = false;
			}else if($post['previous_paper_analysis_tab']=='1'){
				$jsonData['ins_previous_paper_analysis_tab'] = false;
			}else if($post['previous_paper_analysis_tab']=='2'){
				$jsonData['ins_previous_paper_analysis_tab'] = true;
			}
			if($post['doubts_tab']=='0'){
				$jsonData['ins_doubts_tab'] = false;
			}else if($post['doubts_tab']=='1'){
				$jsonData['ins_doubts_tab'] = false;
			}else if($post['doubts_tab']=='2'){
				$jsonData['ins_doubts_tab'] = true;
			}
			if($post['feedback_tab']=='0'){
				$jsonData['ins_feedback_tab'] = false;
			}else if($post['feedback_tab']=='1'){
				$jsonData['ins_feedback_tab'] = false;
			}else if($post['feedback_tab']=='2'){
				$jsonData['ins_feedback_tab'] = true;
			}
			if($post['package_tab']=='0'){
				$jsonData['ins_package_tab'] = false;
			}else if($post['package_tab']=='1'){
				$jsonData['ins_package_tab'] = false;
			}else if($post['package_tab']=='2'){
				$jsonData['ins_package_tab'] = true;
			}

			if($post['error_exam']=='0'){
				$jsonData['ins_error_exam'] = false;
			}else if($post['error_exam']=='1'){
				$jsonData['ins_error_exam'] = false;
			}else if($post['error_exam']=='2'){
				$jsonData['ins_error_exam'] = true;
			}

			if($post['home_tab']=='0'){
				$jsonData['ins_home_tab'] = false;
			}else if($post['home_tab']=='1'){
				$jsonData['ins_home_tab'] = false;
			}else if($post['home_tab']=='2'){
				$jsonData['ins_home_tab'] = true;
			}

			if($post['userlevel']=='1'){
				$jsonData['custom_chapter'] = true;
				$jsonData['custom_cumulative'] = true;
				$jsonData['custom_semi_grand'] = true;
				$jsonData['custom_grand'] = true;
			}else{
				if($post['custom_chapter']=='0'){ $jsonData['custom_chapter'] = false; }else{  $jsonData['custom_chapter'] = true; }
				if($post['custom_cumulative']=='0'){ $jsonData['custom_cumulative'] = false; }else{  $jsonData['custom_cumulative'] = true; }
				if($post['custom_semi_grand']=='0'){ $jsonData['custom_semi_grand'] = false; }else{  $jsonData['custom_semi_grand'] = true; }
				if($post['custom_grand']=='0'){ $jsonData['custom_grand'] = false; }else{  $jsonData['custom_grand'] = true; }
				
			}
			if($post['ins_custom_exam']=='0'){ $jsonData['ins_custom_exam'] = false; }else{  $jsonData['ins_custom_exam'] = true; }
			if($post['ins_previous_exam']=='0'){ $jsonData['ins_previous_exam'] = false; }else{  $jsonData['ins_previous_exam'] = true; }
			

		$data=json_encode($jsonData);
	
		//if($id=='NULL')
		//{
			if($post['institution_id']!='' && $post['plan_id']!='' &&  $post['plan_id']!='0'){
				$sql=query("select * from user_access_modules where estatus='1' and institution_id='".$post['institution_id']."' and plan_id='".$plan_id."'");
				$rowcount=mysqli_num_rows($sql);
				if($rowcount>0){
					echo "INSERT INTO user_access_modules VALUES('','1','".$post['institution_id']."','".$plan_id."','".$data."','1','".time()."')  ON DUPLICATE KEY UPDATE institution_id='".$post['institution_id']."',plan_id='".$plan_id."',modules='".$data."'";
					$result=query("INSERT INTO user_access_modules VALUES('','1','".$post['institution_id']."','".$plan_id."','".$data."','1','".time()."')  ON DUPLICATE KEY UPDATE institution_id='".$post['institution_id']."',plan_id='".$plan_id."',modules='".$data."'");
				
				}else{
					$result=query("INSERT  user_access_modules set userlevel='1',institution_id='".$post['institution_id']."',plan_id='".$plan_id."',modules='".$data."',estatus='1',timestamp='".time()."'");

				}
			}else{
				$result=query("INSERT INTO user_access_modules VALUES('','1','".$post['institution_id']."','".$plan_id."','".$data."','1','".time()."')  ON DUPLICATE KEY UPDATE institution_id='".$post['institution_id']."',plan_id='".$plan_id."',modules='".$data."'");
			}
 


				//$sid= mysqli_insert_id($con);
				if($result){
					
				

				 ?>
				  <div class="col-lg-12 col-md-12">
					<div class="form-group">
						<div class="alert alert-success">
						<i class="fa fa-thumbs-up fa-2x"></i>User Access Module Created Successfully
						</div>
					</div>
				</div>
				<script type="text/javascript">
					alert("User Access Module Created Successfully");
					setStateGet('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1');
				
			  </script>
		 <?php
			}else{
				?>
				<div class="col-lg-12 col-md-12">
					<div class="form-group">
						<div class="alert alert-success">
						<i class="fa fa-thumbs-up fa-2x"></i> User Access Module Created Failed
						</div>
					</div>
				</div>
				<script type="text/javascript">
					alert("User Access Module Created Failed");
				  setStateGet('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1');
			  </script>
			<?php

			}
		
		/*}else{
			$result=query("update  user_access_modules  set institution_id='".$post['institution_id']."',userlevel='".$post['userlevel']."',modules='".$data."' where id='".$id."'
				
				" );
 

if($result){
				
			

			 ?>
			  <div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i>User Access Modules Updated Successfully
					</div>
				</div>
			</div>
			<script type="text/javascript">
				alert("User Access Module Updated Successfully");
				setStateGet('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1');
			
		  </script>
	 <?php
		}else{
			?>
			<div class="col-lg-12 col-md-12">
				<div class="form-group">
					<div class="alert alert-success">
					<i class="fa fa-thumbs-up fa-2x"></i> User Access Modules Updation Failed.
					</div>
				</div>
			</div>
			<script type="text/javascript">
				alert("User Access Module Updation Failed");
			 setStateGet('adminForm','<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1');
		  </script>
	 	<?php

		}
    
	}*/
}
}
	?>

<?php
if(isset($_GET['rowDelete'])){
	$database->query("update  user_access_modules set estatus='0'  WHERE id = '".$_GET['rowDelete']."'");
	?>
    <div class="alert alert-success"> User Access Modules Deleted successfully!</div>

  <script type="text/javascript">

animateForm('<?php echo SECURE_PATH;?>user_access_modules/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
   <?php
}
?>

<?php
if(isset($_POST['mobilePreview'])){
    echo urldecode($_SESSION['description']);
}
?>
	<script>
	$(function () {
		$('.datepicker').datetimepicker({
			format: 'DD-MM-YYYY'
		});
	});

	
	</script>
	<script>
	 $(".selectpicker1").selectpicker('refresh');
	$(".selectpicker2").selectpicker('refresh');
	$(".selectpicker3").selectpicker('refresh');
	function ra_actions(value){
		$('#ra_actions').val(value);

	}
	function ra_overall(value){
		$('#ra_overall').val(value);

	}
	function ra_overall_details(value){
		$('#ra_overall_details').val(value);

	}
	function ra_subject_strength(value){
		$('#ra_subject_strength').val(value);

	}
	function ra_subject_strength_details(value){
		$('#ra_subject_strength_details').val(value);

	}
	function ra_time_analysis(value){
		$('#ra_time_analysis').val(value);

	}
	function ra_time_analysis_details(value){
		$('#ra_time_analysis_details').val(value);

	}
	function ra_complexity(value){
		$('#ra_complexity').val(value);

	}
	function ra_complexity_details(value){
		$('#ra_complexity_details').val(value);

	}
	function ra_complexity_details(value){
		$('#ra_complexity_details').val(value);

	}
	function ra_qtheory(value){
		$('#ra_qtheory').val(value);
	}
	function ra_qtheory_details(value){
		$('#ra_qtheory_details').val(value);
	}
	function ra_qtype(value){
		$('#ra_qtype').val(value);
	}
	function ra_qtype_details(value){
		$('#ra_qtype_details').val(value);
	}
	function custom_chapter(value){
		$('#custom_chapter').val(value);
	}
	function custom_cumulative(value){
		$('#custom_cumulative').val(value);
	}
	function custom_semi_grand(value){
		$('#custom_semi_grand').val(value);
	}
	function custom_grand(value){
		$('#custom_grand').val(value);
	}
	function custom_completed(value){
		$('#custom_completed').val(value);
	}
	function custom_completed_analysis(value){
		$('#custom_completed_analysis').val(value);
	}
	function error_chapter(value){
		$('#error_chapter').val(value);
	}
	function error_cumulative(value){
		$('#error_cumulative').val(value);
	}
	function error_completed(value){
		$('#error_completed').val(value);
	}
	function error_completed(value){
		$('#error_completed').val(value);
	}
	function error_completed_analysis(value){
		$('#error_completed_analysis').val(value);
	}
	function previous_custom(value){
		$('#previous_custom').val(value);
	}
	function previous_single(value){
		$('#previous_single').val(value);
	}
	function previous_single(value){
		$('#previous_single').val(value);
	}
	function previous_completed(value){
		$('#previous_completed').val(value);
	}
	function previous_completed_analysis(value){
		$('#previous_completed_analysis').val(value);
	}
	function schedule_exam_completed(value){
		$('#schedule_exam_completed').val(value);
	}
	function schedule_completed_analysis(value){
		$('#schedule_completed_analysis').val(value);
	}
	function schedule_exam(value){
		$('#schedule_exam').val(value);
	}
	function custom_exam(value){
		$('#custom_exam').val(value);
	}
	
	function error_exam(value){
		$('#error_exam').val(value);
	}
	function error_exam(value){
		$('#error_exam').val(value);
	}
	function previous_exam(value){
		$('#previous_exam').val(value);
	}
	function practise_completed(value){
		$('#practise_completed').val(value);
	}
	function practise_completed_analysis(value){
		$('#practise_completed_analysis').val(value);
	}
	function lp_practice_exam(value){
		$('#lp_practice_exam').val(value);
	}
	function lp_practice_filter(value){
		$('#lp_practice_filter').val(value);
	}
	function lp_custom_content(value){
		$('#lp_custom_content').val(value);
	}
	function lp_error_exam(value){
		$('#lp_error_exam').val(value);
	}
	function lp_topics(value){
		$('#lp_topics').val(value);
	}
	function lp_chapter_dashboard(value){
		$('#lp_chapter_dashboard').val(value);
	}
	function lp_topic_dashboard(value){
		$('#lp_topic_dashboard').val(value);
	}
	function lp_topics_practice_exam(value){
		$('#lp_topics_practice_exam').val(value);
	}
	function lp_topic_custom_content(value){
		$('#lp_topic_custom_content').val(value);
	}
	function lp_topic_error_exam(value){
		$('#lp_topic_error_exam').val(value);
	}
	function learn_tab(value){
		$('#learn_tab').val(value);
	}
	function exam_tab(value){
		$('#exam_tab').val(value);
	}
	function ready_exam_tab(value){
		$('#ready_exam_tab').val(value);
	}
	function analysis_tab(value){
		$('#analysis_tab').val(value);
	}
	function bookmarks_tab(value){
		$('#bookmarks_tab').val(value);
	}
	function notes_tab(value){
		$('#notes_tab').val(value);
	}
	function profile_tab(value){
		$('#profile_tab').val(value);
	}
	function ins_learn_tab(value){
		$('#ins_learn_tab').val(value);
	}
	function ins_exam_tab(value){
		$('#ins_exam_tab').val(value);
	}
	function ins_ready_exam_tab(value){
		$('#ins_ready_exam_tab').val(value);
	}
	function ins_bookmarks_tab(value){
		$('#ins_bookmarks_tab').val(value);
	}
	function ins_notes_tab(value){
		$('#ins_notes_tab').val(value);
	}
	function ins_notes_tab(value){
		$('#ins_notes_tab').val(value);
	}
	function ins_profile_tab(value){
		$('#ins_profile_tab').val(value);
	}
	function ins_custom_exam(value){
		$('#ins_custom_exam').val(value);
	}
	function ins_previous_exams(value){
		
		$('#ins_previous_exam').val(value);
	}
	function revision_material_tab(value){
		$('#revision_material_tab').val(value);
	}
	function previous_paper_analysis_tab(value){
		$('#previous_paper_analysis_tab').val(value);
	}
	function doubts_tab(value){
		$('#doubts_tab').val(value);
	}
	function feedback_tab(value){
		$('#feedback_tab').val(value);
	}
	function package_tab(value){
		$('#package_tab').val(value);
	}
	function home_tab(value){
		$('#home_tab').val(value);
	}
	function linkage_analysis_tab(value){
		$('#linkage_analysis_tab').val(value);
	}
	function mock_test(value){
		$('#mock_test').val(value);
	}
	function jee_mains_2021(value){
		$('#jee_mains_2021').val(value);
	}
	function help_videos(value){
		$('#help_videos').val(value);
	}
	function blog_tab(value){
		$('#blog_tab').val(value);
	}
	function videos_tab(value){
		$('#videos_tab').val(value);
	}
	/*function video_tab(value){
		$('#video_tab').val(value);
	}*/
	function usertypediv(){
		$val=$("#userlevel").val();
		if($val=='1'){
			$('#institutediv').show();
		}else{
			$('#institutediv').hide();
		}
	}
	</script>
	<script>

    function useraccessdiv1(value)
    {
		$('#dataenable').val(value);
		if(value==0){
			$("#ra_actions1"). prop("checked", true);
			$("#ra_overall1"). prop("checked", true);
			$("#ra_overall_details1"). prop("checked", true);
			$("#ra_subject_strength1"). prop("checked", true);
			$("#ra_subject_strength_details1"). prop("checked", true);
			$("#ra_time_analysis1"). prop("checked", true);
			$("#ra_time_analysis_details1"). prop("checked", true);
			$("#ra_complexity1"). prop("checked", true);
			$("#ra_complexity_details1"). prop("checked", true);
			$("#ra_qtheory1"). prop("checked", true);
			$("#ra_qtheory_details1"). prop("checked", true);
			$("#ra_qtype1"). prop("checked", true);
			$("#ra_qtype_details1"). prop("checked", true);
			$("#custom_chapter1"). prop("checked", true);
			$("#custom_cumulative1"). prop("checked", true);
			$("#custom_semi_grand1"). prop("checked", true);
			$("#custom_grand1"). prop("checked", true);
			$("#custom_completed1"). prop("checked", true);
			$("#custom_completed_analysis1"). prop("checked", true);

			$("#error_chapter1"). prop("checked", true);
			$("#error_cumulative1"). prop("checked", true);
			$("#error_completed1"). prop("checked", true);
			$("#error_completed_analysis1"). prop("checked", true);
			$("#previous_custom1"). prop("checked", true);
			$("#previous_single1"). prop("checked", true);
			$("#previous_completed1"). prop("checked", true);
			$("#previous_completed_analysis1"). prop("checked", true);

			$("#schedule_exam_completed1"). prop("checked", true);
			$("#schedule_completed_analysis1"). prop("checked", true);
			$("#schedule_exam1"). prop("checked", true);
			$("#custom_exam1"). prop("checked", true);

			$("#previous_exam1"). prop("checked", true);
			$("#practise_completed1"). prop("checked", true);
			$("#practise_completed_analysis1"). prop("checked", true);
			$("#lp_practice_exam1"). prop("checked", true);

			$("#lp_practice_filter1"). prop("checked", true);
			$("#lp_custom_content1"). prop("checked", true);
			$("#lp_error_exam1"). prop("checked", true);
			$("#lp_topics1"). prop("checked", true);

			$("#lp_chapter_dashboard1"). prop("checked", true);
			$("#lp_topic_dashboard1"). prop("checked", true);
			$("#lp_topics_practice_exam1"). prop("checked", true);
			$("#lp_topic_custom_content1"). prop("checked", true);

			$("#lp_topic_error_exam1"). prop("checked", true);
			$("#ins_custom_exam1"). prop("checked", true);
			$("#ins_previous_exam1"). prop("checked", true);
			$("#learn_tab1"). prop("checked", true);
			$("#exam_tab1"). prop("checked", true);
			$("#error_exam1"). prop("checked", true);
			$("#ready_exam_tab1"). prop("checked", true);
			$("#analysis_tab1"). prop("checked", true);
			$("#bookmarks_tab1"). prop("checked", true);
			$("#notes_tab1"). prop("checked", true);
			$("#profile_tab1"). prop("checked", true);
			$("#doubts_tab1"). prop("checked", true);

			$("#feedback_tab1"). prop("checked", true);
			$("#package_tab1"). prop("checked", true);
			$("#home_tab1"). prop("checked", true);
			$("#revision_material_tab1"). prop("checked", true);
			$("#previous_paper_analysis_tab1"). prop("checked", true);
			$("#linkage_analysis_tab1"). prop("checked", true);
			$("#videos_tab1"). prop("checked", true);
			$("#mock_test1"). prop("checked", true);
			$("#jee_mains_2021_1"). prop("checked", true);
			$("#help_videos_1"). prop("checked", true);
			$("#blog_tab1"). prop("checked", true);
			//$("#video_tab1"). prop("checked", true);
		}else{
			$("#ra_actions2"). prop("checked", true);
			$("#ra_overall2"). prop("checked", true);
			$("#ra_overall_details2"). prop("checked", true);
			$("#ra_subject_strength2"). prop("checked", true);
			$("#ra_subject_strength_details2"). prop("checked", true);
			$("#ra_time_analysis2"). prop("checked", true);
			$("#ra_time_analysis_details2"). prop("checked", true);
			$("#ra_complexity2"). prop("checked", true);
			$("#ra_complexity_details2"). prop("checked", true);
			$("#ra_qtheory2"). prop("checked", true);
			$("#ra_qtheory_details2"). prop("checked", true);
			$("#ra_qtype2"). prop("checked", true);
			$("#ra_qtype_details2"). prop("checked", true);
			$("#custom_chapter2"). prop("checked", true);
			$("#custom_cumulative2"). prop("checked", true);
			$("#custom_semi_grand2"). prop("checked", true);
			$("#custom_grand2"). prop("checked", true);
			$("#custom_completed2"). prop("checked", true);
			$("#custom_completed_analysis2"). prop("checked", true);
			$("#error_chapter2"). prop("checked", true);
			$("#error_cumulative2"). prop("checked", true);
			$("#error_completed2"). prop("checked", true);
			$("#error_completed_analysis2"). prop("checked", true);
			$("#previous_custom2"). prop("checked", true);
			$("#previous_single2"). prop("checked", true);
			$("#previous_completed2"). prop("checked", true);
			$("#previous_completed_analysis2"). prop("checked", true);
			$("#schedule_exam_completed2"). prop("checked", true);
			$("#schedule_completed_analysis2"). prop("checked", true);
			$("#schedule_exam2"). prop("checked", true);
			$("#custom_exam2"). prop("checked", true);

			$("#previous_exam2"). prop("checked", true);
			$("#practise_completed2"). prop("checked", true);
			$("#practise_completed_analysis2"). prop("checked", true);
			$("#lp_practice_exam2"). prop("checked", true);

			$("#lp_practice_filter2"). prop("checked", true);
			$("#lp_custom_content2"). prop("checked", true);
			$("#lp_error_exam2"). prop("checked", true);
			$("#lp_topics2"). prop("checked", true);

			$("#lp_chapter_dashboard2"). prop("checked", true);
			$("#lp_topic_dashboard2"). prop("checked", true);
			$("#lp_topics_practice_exam2"). prop("checked", true);
			$("#lp_topic_custom_content2"). prop("checked", true);

			$("#lp_topic_error_exam2"). prop("checked", true);
			$("#ins_custom_exam2"). prop("checked", true);
			$("#ins_previous_exam2"). prop("checked", true);
			$("#learn_tab2"). prop("checked", true);
			$("#exam_tab2"). prop("checked", true);
			$("#error_exam2"). prop("checked", true);
			$("#ready_exam_tab2"). prop("checked", true);
			$("#analysis_tab2"). prop("checked", true);
			$("#bookmarks_tab2"). prop("checked", true);
			$("#notes_tab2"). prop("checked", true);
			$("#profile_tab2"). prop("checked", true);
			$("#doubts_tab2"). prop("checked", true);

			$("#feedback_tab2"). prop("checked", true);
			$("#package_tab2"). prop("checked", true);
			$("#home_tab2"). prop("checked", true);
			$("#revision_material_tab2"). prop("checked", true);
			$("#previous_paper_analysis_tab2"). prop("checked", true);
			$("#linkage_analysis_tab2"). prop("checked", true);
			$("#videos_tab2"). prop("checked", true);
			$("#mock_test2"). prop("checked", true);
			$("#jee_mains_2021_2"). prop("checked", true);
			$("#help_videos_2"). prop("checked", true);
			$("#blog_tab2"). prop("checked", true);
			//$("#video_tab2"). prop("checked", true);
		}
		$('#ra_actions').val(value);
		$('#ra_overall').val(value);

		$('#ra_overall_details').val(value);

		$('#ra_subject_strength').val(value);

		$('#ra_subject_strength_details').val(value);

		$('#ra_time_analysis').val(value);
		$('#ra_time_analysis_details').val(value);
		$('#ra_complexity').val(value);
		$('#ra_complexity_details').val(value);
		$('#ra_complexity_details').val(value);
		$('#ra_qtheory').val(value);
		$('#ra_qtheory_details').val(value);
		$('#ra_qtype').val(value);
		$('#ra_qtype_details').val(value);
		$('#custom_chapter').val(value);
		$('#custom_cumulative').val(value);
		$('#custom_semi_grand').val(value);
		$('#custom_grand').val(value);
		$('#custom_completed').val(value);
		$('#custom_completed_analysis').val(value);
		$('#error_chapter').val(value);
		$('#error_cumulative').val(value);
		$('#error_completed').val(value);
		$('#error_completed').val(value);
		$('#error_completed_analysis').val(value);
		$('#previous_custom').val(value);
		$('#previous_single').val(value);
		$('#previous_single').val(value);
		$('#previous_completed').val(value);
		$('#previous_completed_analysis').val(value);
		$('#schedule_exam_completed').val(value);
		$('#schedule_completed_analysis').val(value);
		$('#schedule_exam').val(value);
		$('#custom_exam').val(value);
		$('#error_exam').val(value);
		$('#error_exam').val(value);
		$('#previous_exam').val(value);
		$('#practise_completed').val(value);
		$('#practise_completed_analysis').val(value);
		$('#lp_practice_exam').val(value);
		$('#lp_practice_filter').val(value);
		$('#lp_custom_content').val(value);
		$('#lp_error_exam').val(value);
		$('#lp_topics').val(value);
		$('#lp_chapter_dashboard').val(value);
		$('#lp_topic_dashboard').val(value);
		$('#lp_topics_practice_exam').val(value);
		$('#lp_topic_custom_content').val(value);
		$('#lp_topic_error_exam').val(value);
		$('#learn_tab').val(value);
		$('#exam_tab').val(value);
		$('#ready_exam_tab').val(value);
		$('#analysis_tab').val(value);
		$('#bookmarks_tab').val(value);
		$('#notes_tab').val(value);
		$('#profile_tab').val(value);
		$('#ins_learn_tab').val(value);
		$('#ins_exam_tab').val(value);
		$('#ins_ready_exam_tab').val(value);
		$('#ins_bookmarks_tab').val(value);
		$('#ins_notes_tab').val(value);
		$('#ins_notes_tab').val(value);
		$('#ins_profile_tab').val(value);
		$('#ins_custom_exam').val(value);
		$('#ins_previous_exam').val(value);
		$('#revision_material_tab').val(value);
		$('#previous_paper_analysis_tab').val(value);
		$('#doubts_tab').val(value);
		$('#feedback_tab').val(value);
		$('#package_tab').val(value);
		$('#home_tab').val(value);
	
		$('#linkage_analysis_tab').val(value);
		$('#videos_tab').val(value);
		$('#mock_test').val(value);
		$("#jee_mains_2021"). val(value);
		$("#help_videos"). val(value);
		$("#blog_tab"). val(value);
    }   


</script>
	
