<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		alert("User with the same username logged in to another browser");
		//setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
		location.replace("<?php echo SECURE_PATH;?>admin/");
	</script>
<?php
}
?>
<script>
$(document).ready(function(){
  //alert("sdfgh"+userlevelval);
  $('#userlevel').change(function(){
    var userlevelval = $("#userlevel option:selected").val();
    
    if(userlevelval == 3 || userlevelval == 2 || userlevelval == 1){
    $('.department').show();
    $('.designation').show();
    
  }
  else{
    $('.department').hide();
    $('.designation').hide();
  }
  });
  
});
</script>

<?php
//Metircs Forms, Tables and Functions
//Display cadre form
if(isset($_REQUEST['addForm'])){
	if($_REQUEST['addForm'] == 2 && isset($_POST['editform'])){
    $data_sel = $database->query("SELECT * FROM users WHERE username = '".$_POST['editform']."'");
    if(mysqli_num_rows($data_sel) > 0){
    $data = mysqli_fetch_array($data_sel);
    $_POST = array_merge($_POST,$data);
 ?>
 <script type="text/javascript">
 $('#adminForm').slideDown();
 </script>
 <?php
    }
 }
 ?>
<style>
  .spinner-border.w_1 {
    width: 1rem;height: 1rem;
  }
</style>
<script>
getFields5();
getFields6();
getFields1();
$('#subject').selectpicker2();
$('#chapter').selectpicker1();
</script>
<script>
chapterfunction();
//radiofunction('<?php echo $_POST['ppaper']; ?>');
function chapterfunction(){
	var chapter=$('#chapter').val();
	//alert(chapter);
	if(chapter.length >0){
		$('#chapview').show();
	}else{
		$('#chapview').hide();
	}
}
</script>
<?php
		if(isset($_POST['editform'])){
			if($_POST['editform']!=''){
				$style="style='display:none;'";
				$disa="disabled";
				$read="readonly";
			}else{
			}
		}else{
			$style="";
			$read="";
		}
			?>
 <div class="col-lg-12 col-md-12">
	<div class="row">
		<div class="col-lg-6">
			<div class="form-group row">
			  <label class="col-sm-4 col-form-label">Full Name<span style="color:red;">*</span></label>
				<div class="col-sm-8">
				  <input type="text" name="name" class="form-control" placeholder="Enter Full Name" id="name" value="<?php if(isset($_POST['name'])){ echo $_POST['name'];}?>" autocomplete="off">
				  <span class="text-danger"><?php if(isset($_SESSION['error']['name'])){ echo $_SESSION['error']['name'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
	
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Username<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="username" class="form-control" placeholder="Enter Username" id="username" value="<?php if(isset($_POST['username'])){ echo $_POST['username'];}?>" autocomplete="off"  <?php echo $read; ?>>
					<span class="text-danger"><?php if(isset($_SESSION['error']['username'])){ echo $_SESSION['error']['username'];}?></span>
				</div>
			</div>
		</div>
		
				<div class="col-lg-6" <?php echo $style; ?> >
				
					<div class="form-group row">
						<label class="col-sm-4 col-form-label">Password<span style="color:red;">*</span></label>
						<div class="col-sm-8">
							<input type="password" name="password" class="form-control" placeholder="Enter Password" id="password" value="<?php if(isset($_POST['password'])){ echo $_POST['password'];}else{ echo rand(1000,9999).$session->generateRandStr(4);}?>" autocomplete="off" <?php echo $disa; ?>>
							<span class="text-danger"><?php if(isset($_SESSION['error']['password'])){ echo $_SESSION['error']['password'];}?></span>
						</div>
					</div>
				
				</div>
			
		
		<div class="col-lg-6">
			<div class="form-group row">
          <label class="col-sm-4 col-form-label">Email<span style="color:red;">*</span></label>
          <div class="col-sm-8">
              <input type="text" name="email" class="form-control" placeholder="Enter Email" id="email" value="<?php if(isset($_POST['email'])){ echo $_POST['email'];}?>" autocomplete="off">
              <span class="text-danger"><?php if(isset($_SESSION['error']['email'])){ echo $_SESSION['error']['email'];}?></span>
          </div>
        </div>

		</div>
		<div class="col-lg-6">
			<div class="form-group row">
				<label class="col-sm-4 col-form-label">Mobile No.<span style="color:red;">*</span></label>
				<div class="col-sm-8">
					<input type="text" name="mobile" class="form-control" placeholder="10-digit Mobile number" id="mobile" onkeypress="return isNumber(event,$(this),10)"  maxlength="10" value="<?php if(isset($_POST['mobile'])){echo $_POST['mobile'];}?>" autocomplete="off">
					<span class="text-danger"><?php if(isset($_SESSION['error']['mobile'])){ echo $_SESSION['error']['mobile'];}?></span>
				</div>
			</div>
		</div>
		<div class="col-lg-6">
			<div class="form-group row">
            <label class="col-sm-4 col-form-label">User Type<span style="color:red;">*</span></label>
            <div class="col-sm-8">
                <select name="userlevel" class="custom-select" id="userlevel" onchange="selectfunction();" >
                    <option value="0">Select User Type</option>
                    <option value="7" <?php if(isset($_POST['userlevel'])){ if($_POST['userlevel'] == 7){ echo ' selected="selected"';};}?>>Data Entry Operator</option>
                    <option value="8" <?php if(isset($_POST['userlevel'])){ if($_POST['userlevel'] == 8){ echo ' selected="selected"';};}?>>Lecture</option>
                    <option value="6" <?php if(isset($_POST['userlevel'])){ if($_POST['userlevel'] == 6){ echo ' selected="selected"';};}?>>Reviewer</option>
					<option value="9" <?php if(isset($_POST['userlevel'])){ if($_POST['userlevel'] == 9){ echo ' selected="selected"';};}?>>Admin</option>
					<option value="1" <?php if(isset($_POST['userlevel'])){ if($_POST['userlevel'] == 1){ echo ' selected="selected"';};}?>>Super Admin</option>
				 </select>
				<span class="text-danger" id="userfunc"><?php if(isset($_SESSION['error']['userlevel'])){ echo $_SESSION['error']['userlevel'];}?></span>
            </div>
            
			</div>
		</div>
		<?php
			
		if(isset($_POST['userlevel'])){
			if($_POST['userlevel']=='6' || $_POST['userlevel']=='7' || $_POST['userlevel']=='8' ){
				$style="";
				
             }else{
				$style="style='display:none;'";
			}
				if($_POST['userlevel']=='7' ){

					$stylez="";
					$stylez1="";
					$stylez2="style='display:none;'";
				}else if($_POST['userlevel']=='8'){
					$stylez="";
					$stylez2="";
					$stylez1="style='display:none;'";
				}else{
					$stylez="style='display:none;'";
					$stylez1="style='display:none;'";
					$stylez2="style='display:none;'";
				}
			
		}else{
				$style="style='display:none;'";
				$stylez="style='display:none;'";
				$stylez1="style='display:none;'";
				$stylez2="style='display:none;'";
		}

		$class=$_POST['class'];
		$subject=$_POST['subject'];
		$_POST['class']=explode(",",$_POST['class']);
		?>
		<div class="col-md-6"  >
			<div class="form-group subjectdiv" <?php echo $style; ?> >
			<label class="col-sm-4 col-form-label" for="inputExam">Class<span class="text-danger">*</span></label>
			
			
			
				<div class="custom-control custom-checkbox custom-control-inline">
					<input type="checkbox" id="customcheckboxInline1" name="customcheckboxInline1" class="class3 custom-control-input" onChange="getFields1();setStateGet('aaa','<?php echo SECURE_PATH;?>users/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter=<?php echo $_POST['chapter']; ?>')"  value="<?php if(isset($_POST['class'])) {  if(in_array('1', $_POST['class'])) { echo '1'; }else{ echo '1'; } } else { echo '1'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('1', $_POST['class'])) { echo 'checked'; } } ?> >
					<label class="custom-control-label" for="customcheckboxInline1">XI</label>
				</div>
				<div class="custom-control custom-checkbox custom-control-inline">
					<input type="checkbox" id="customcheckboxInline2" name="customcheckboxInline2" class="class3 custom-control-input" onChange="getFields1();setStateGet('aaa','<?php echo SECURE_PATH;?>users/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter=<?php echo $_POST['chapter']; ?>')"  value="<?php if(isset($_POST['class'])) {  if(in_array('2', $_POST['class'])) { echo '2'; }else{ echo '2'; } } else { echo '2'; } ?>"  <?php if(isset($_POST['class'])) { if(in_array('2', $_POST['class'])) { echo 'checked'; }} ?> >
					<label class="custom-control-label" for="customcheckboxInline2">XII</label>
				</div>
				<input type="hidden" class="class3" id="class" value="<?php echo $class; ?>" >
				<span class="error" style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['class'])){ echo $_SESSION['error']['class'];}?></span>
			</div>
		</div>
		<div class="col-lg-6" >
			<div class="form-group row subjectdiv" <?php echo $style; ?> >
			<?php
				$_POST['subject']=explode(",",$_POST['subject']);
			?>
            <label class="col-sm-4 col-form-label">Subject<span style="color:red;">*</span></label>
            <div class="col-sm-8">
                <select name="subject" class="selectpicker2" id="subject" multiple data-live-search="true" onchange="setStateGet('aaa','<?php echo SECURE_PATH;?>users/process.php','getchapter=1&class='+$('#class').val()+'&subject='+$('#subject').val()+'&chapter=<?php echo $_POST['chapter']; ?>')">
                    <?php
                    $row = $database->query("select * from subject where estatus='1'");
                    while($data = mysqli_fetch_array($row))
                    {
                      ?>
                    <option value="<?php echo $data['id'];?>"   <?php if(isset($_POST['subject'])) { if(in_array($data['id'], $_POST['subject'])) { echo 'selected="selected"'; } } ?> ><?php echo $data['subject'];?></option>
                    <?php
                    }
                    ?>
                </select>
				
            </div>
			 <span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['subject'])){ echo $_SESSION['error']['subject'];}?></span>
        </div>
		</div>
		<?php 
			$chapter1=$_POST['chapter'];
			if(isset($chapter1)){
				if($chapter1!=''){
					$style14="";
				}else{
					$style14="display:none;";
				}
			}else{
				$style14="display:none;";
			}
			$i=1;
			$count=0;
			$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1'    AND id IN(".rtrim($chapter1,',').")  "); 
			while($data1 = mysqli_fetch_array($row1))
			{
				$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
				$rowdcount=mysqli_num_rows($userd);
			 
				  $count=$count+$rowdcount;
			 $i++;
			}
			
			?>
			
		
    <?php
			?>
		<div class="col-lg-8" >
		<div class="d-flex align-items-center">
			<div class="form-group row subjectdiv"  id="aaa" <?php echo $style; ?> >
				<?php
					$_POST['chapter']=explode(",",$_POST['chapter']);
				?>
				<label class="col-lg-4 col-form-label">Chapter<span style="color:red;">*</span></label>
				<div class="col-lg-5 ml-5">
					<select name="chapter" class="selectpicker1" id="chapter" multiple data-live-search="true" onchange="chapterfunction();setStateGet('chapview','<?php echo SECURE_PATH;?>users/process.php','getchaptercview=1&chapter='+$('#chapter').val()+'')">
						<?php
					   // $row1 = $database->query("select * from chapter where estatus='1'  ");
						$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' and class IN  (".rtrim($class,',').") and subject IN  (".rtrim($subject,',').") "); 
						while($data1 = mysqli_fetch_array($row1))
						{
						  ?>
						<option value="<?php echo $data1['id'];?>" <?php if(isset($_POST['chapter'])) { if(in_array($data1['id'], $_POST['chapter'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data1['chapter'];?></option>
						<?php
						}
						?>
					</select>
					
					
				</div>
				
				<span class="text-danger"  style="color:red;font-size:14px;"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
			</div>
			
			<div class="col-sm-3">
			<div id="chapview" style="<?php echo $style14; ?>margin-left:2rem" ><a href='#'  style="cursor:pointer;color: #007bff;" title="Print"  data-toggle="modal" data-target="#messageDetails1" onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>users/process1.php','viewDetails1=1&chapter=<?php echo $chapter1; ?>');"><?php echo $count; ?> Users</a></div>
			</div>
			</div>
		</div>
		</div>
		<?php $customcontent=$_POST['customcontent']; 
		 ?>
		 <div class="form-group row pl-3">
			<div class="col-md-6 contentd" <?php echo $stylez;?> >
				<div class="form-group">
				 <div class="custom-control custom-checkbox">
				  <input type="checkbox" class="classn custom-control-input" id="customcontent"  onchange="getFields5();"  value="<?php if(isset($_POST['customcontent'])){ if($_POST['customcontent']!=''){ echo $_POST['customcontent'];}else{ echo '1'; }}else{ echo '1'; } ?>" <?php if(isset($_POST['customcontent'])) { if($_POST['customcontent']==1) { echo 'checked'; }}else{ echo 'checked'; } ?> >
				  <label class="custom-control-label" for="customcontent" >Custom Content</label>
				</div>
				</div>
			</div>	
		</div>
		
		
		 <div class="form-group row pl-3">
				<div class="col-md-6 contentd1" <?php echo $stylez1;?> >
					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="attributes custom-control-input" id="attributes"  onchange="getFields7();"  value="<?php if(isset($_POST['attributes'])){ if($_POST['attributes']!=''){ echo $_POST['attributes'];}else{ echo '0'; }}else{ echo '0'; } ?>" <?php if(isset($_POST['attributes'])) { if($_POST['attributes']==1) { echo 'checked'; }}else{ echo ''; } ?> >
							<label class="custom-control-label" for="attributes" >Attribute Selection</label>
						</div>
					</div>
				</div>	
			</div>
			
			<?php
			if(isset($_POST['ppaper'])){
			 	if($_POST['ppaper']=='1'){
					$stylep="";
				}else{
					$stylep="style='display:none;'";
				}
			}else{
				$stylep="style='display:none;'";
			}
			 ?>
			<div class="form-group row pl-3">
				<div class="col-md-6 previous1" <?php echo $stylez;?> >
					<div class="form-group">
						<div class="custom-control custom-checkbox">
							<input type="checkbox" class="ppaper custom-control-input" id="ppaper"  onchange="getFields6();"  value="<?php if(isset($_POST['ppaper'])){ if($_POST['ppaper']!=''){ echo $_POST['ppaper'];}else{ echo '0'; }}else{ echo '0'; } ?>" <?php if(isset($_POST['ppaper'])) {	if($_POST['ppaper']==1) { echo 'checked'; }}else{ echo ''; } ?> >
						 <label class="custom-control-label" for="ppaper" >Previous Year Question Papers</label>
						</div>
					</div>
				</div>
				<?php 
				 $_POST['year1']=$_POST['year'];
				 $_POST['year']=explode(",",$_POST['year']);
				 $_POST['qset']=explode(",",$_POST['qset']);
				 ?>
			<div class="col-md-3 previous2 " <?php echo $stylep;?> >
				 <label for="inputTopi" class="label">Previous Question Year</label>
					<select class="form-control selectpicker3" multiple  name="year" value=""   id="year"  onChange="setState('examsetid','<?php echo SECURE_PATH;?>users/process.php','getexamset=1&year='+$('#year').val()+'&qset='+$('#qset').val()+'')" >
					<option value=''>-- Select --</option>
					<?php
					$row = $database->query("SELECT * FROM previous_questions  WHERE estatus='1'  group by year");
					while($data = mysqli_fetch_array($row))
					{
						?>
						<option value="<?php echo $data['year'];?>" <?php if(isset($_POST['year'])) { if(in_array($data['year'], $_POST['year'])) { echo 'selected="selected"'; } } ?>   ><?php echo $data['year'];?></option>
					<?php
					}
					?>
				</select>
			</div>
			<div class="col-md-3 previous2"  id="examsetid" <?php echo $stylep;?> >
				 <label for="inputTopi" class="label">Set ID</label>
				<select class="form-control selectpicker4"  multiple name="qset" value=""  id="qset"  >
					<option value=''>-- Select --</option>
					<?php
					
					$sel=$database->query("select * from previous_questions where estatus='1'   and year in (".rtrim($_POST['year1'],",").") ORDER by id DESC");
					while($row=mysqli_fetch_array($sel)){
						$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
						while($row1=mysqli_fetch_array($sel1)){
						?>
							
							<option value="<?php echo $row1['id'];?>" <?php if(isset($_POST['qset'])) { if(in_array($row1['id'], $_POST['qset'])) { echo 'selected="selected"'; } } ?>   ><?php echo $row1['qset'];?></option>
					<?php
						}
					}
					?>
			</select>
			</div>

			
			
        </div>
		
		<div class="form-group row pl-3">
			<div class="col-md-6 update_chapter1" <?php echo $stylez2;?> >
				<div class="form-group">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="update_chapter custom-control-input" id="update_chapter"  onchange="getFields8();"  value="<?php if(isset($_POST['update_chapter'])){ if($_POST['update_chapter']!=''){ echo $_POST['update_chapter'];}else{ echo '0'; }}else{ echo '0'; } ?>" <?php if(isset($_POST['update_chapter'])) {	if($_POST['update_chapter']==1) { echo 'checked'; }}else{ echo ''; } ?> >
					 <label class="custom-control-label" for="update_chapter" >Change Chapter and Topic</label>
					</div>
				</div>
			</div>
		</div>

		<div class="form-group row pl-3">
			<div class="col-md-6 question_wchapter1" <?php echo $stylez2;?> >
				<div class="form-group">
					<div class="custom-control custom-checkbox">
						<input type="checkbox" class="question_wchapter custom-control-input" id="question_wchapter"  onchange="getFields9();"  value="<?php if(isset($_POST['question_wchapter'])){ if($_POST['question_wchapter']!=''){ echo $_POST['question_wchapter'];}else{ echo '0'; }}else{ echo '0'; } ?>" <?php if(isset($_POST['question_wchapter'])) {	if($_POST['question_wchapter']==1) { echo 'checked'; }}else{ echo ''; } ?> >
					 <label class="custom-control-label" for="question_wchapter">Show Questions with out Chapter</label>
					</div>
				</div>
			</div>
		</div>
    </div>

    <div class="form-group row pl-3">
		
        <div class="col-lg-6">
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="setState('adminForm','<?php echo SECURE_PATH;?>users/process.php','validateForm=1&name='+$('#name').val()+'&username='+$('#username').val()+'&password='+$('#password').val()+'&email='+$('#email').val()+'&mobile='+$('#mobile').val()+'&userlevel='+$('#userlevel').val()+'&class='+$('#class').val()+'&customcontent='+$('#customcontent').val()+'&subject='+$('#subject').val()+'&chapter='+$('#chapter').val()+'&ppaper='+$('#ppaper').val()+'&update_chapter='+$('#update_chapter').val()+'&year='+$('#year').val()+'&qset='+$('#qset').val()+'&attributes='+$('#attributes').val()+'&question_wchapter='+$('#question_wchapter').val()+'<?php if(isset($_POST['editform'])){ echo '&editform='.$_POST['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$_POST['from_page'];}?><?php if(isset($_POST['page'])){ echo '&page='.$_POST['page'];}?>')">Submit</a>
        </div>
        <?php if($_REQUEST['from_page']=='dataentry_users'){
        ?>
        	<div class="col-lg-6">
         
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="      setState('main-content','<?php echo SECURE_PATH;?>dataentryusers/','getLayout=true')">Cancel</a>
       
        </div>
      <?php  } ?>

       <?php if($_REQUEST['from_page']=='lecturer_users'){
        ?>
        	<div class="col-lg-6">
         
            <a class="radius-20 btn btn-theme px-5" style="cursor:pointer" onClick="      setState('main-content','<?php echo SECURE_PATH;?>lecturerusers/','getLayout=true')">Cancel</a>
       
        </div>
      <?php  } ?>
    </div>
</div>
<script type="text/javascript"> 
    function isNumber(evt) {
      evt = (evt) ? evt : window.event;
      var charCode = (evt.which) ? evt.which : evt.keyCode;
      if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
      }
      return true;
    }
</script>
<?php
unset($_SESSION['error']);
}


//Process and Validate POST data
if(isset($_POST['validateForm'])){
	$_SESSION['error'] = array();
  $post = $session->cleanInput($_POST);
  $id = 'NULL';
	$mobile = $post['mobile'];
	$email = $post['email'];
  $name = $post['name'];
  if(isset($post['editform'])){
	  $id = $post['editform'];
  }

  $field = 'name';
	if(!$post['name'] || strlen(trim($post['name'])) == 0){
	  $_SESSION['error'][$field] = "* Fullname name cannot be empty";
	}
	 $field = 'userlevel';
	if(!$post['userlevel'] || strlen(trim($post['userlevel'])) == 0){
	  $_SESSION['error'][$field] = "* Usertype cannot be empty";
	}
	if($post['userlevel']!= 9 && $post['userlevel']!= 1){
		 $field = 'class';
		if(!$post['class'] || strlen(trim($post['class'])) == 0){
		  $_SESSION['error'][$field] = "* Class cannot be empty";
		}
		 $field = 'subject';
		if(!$post['subject'] || strlen(trim($post['subject'])) == 0){
		  $_SESSION['error'][$field] = "* subject cannot be empty";
		}
		 $field = 'chapter';
		if(!$post['chapter'] || strlen(trim($post['chapter'])) == 0){
		  $_SESSION['error'][$field] = "* chapter cannot be empty";
		}

	}

	//  $field = 'subject';
	// if(!$post['subject'] || strlen(trim($post['subject'])) == 0){
	//   $_SESSION['error'][$field] = "* Subject cannot be empty";
	// }
  $field = 'username';
	if(!$post['username'] || strlen(trim($post['username'])) == 0){
	  $_SESSION['error'][$field] = "* Username name cannot be empty";
	}else if(!ctype_alnum($post['username'])){
        $_SESSION['error'][$field] = "* Username not alphanumeric";
      }
      else if(!isset($post['editform'])){
      /* Check if username is reserved */
      if(strcasecmp($post['username'], GUEST_NAME) == 0){
        $_SESSION['error'][$field] =  "* Username reserved word";
      }
      /* Check if username is already in use */
      else if($database->usernameTaken($post['username'])){
        $_SESSION['error'][$field] =  "* Username already in use";
      }
      /* Check if username is banned */
      else if($database->usernameBanned($post['username'])){
        $_SESSION['error'][$field] =  "* Username banned";
      }
  }
  
  $field = 'mobile';
	if(!$mobile || strlen(trim($mobile)) == 0){
	  $_SESSION['error'][$field] = "* Mobile No. cannot be empty";
	}
  else if(strlen($mobile) < 10){
    $_SESSION['error'][$field] = "* Mobile number below 10 digits";
  }
  else if(strlen($mobile) > 10){
    $_SESSION['error'][$field] = "* Mobile number above 10 digits";
  }
  /* mobile number check */
  else if(!preg_match("~^([6-7-8-9]{1}[0-9]{9})+$~", $mobile)){
          $_SESSION['error'][$field] = "* Invalid Mobile number";
  }


	$field = 'email';
	if(!$email || strlen(trim($email)) == 0){
	 $_SESSION['error'][$field] = "* Email cannot be empty";
	}
	elseif(strlen($email) > 0){
         /* Check if valid email address */
         $regex = "~^[_+a-z0-9-]+(\.[_+a-z0-9-]+)*"
                 ."@[a-z0-9-]+(\.[a-z0-9-]{1,})*"
                 ."\.([a-z]{2,}){1}$~";

		        $email = stripslashes($email);
		 if(!preg_match($regex,$email)){
           $_SESSION['error'][$field] = "* Invalid Email ID";
         }

	}
  $field = 'password';
	if(!$post['password'] || strlen(trim($post['password'])) == 0){
	  $_SESSION['error'][$field] = "* Password cannot be empty";
	}
	else if(strlen(trim($post['password'])) < 4){
      $_SESSION['error'][$field] = "* Length of Password Very low. Use atleast 4 Characters";

  }
  if($post['customcontent']!=''){
		$customcontent=$post['customcontent'];
	}else{
		$customcontent=0;
	}
	if($post['ppaper']!=''){
		$ppaper=$post['ppaper'];
	}else{
		$ppaper=0;
	}
	if($post['year']!=''){
		$year=$post['year'];
	}else{
		$year='';
	}
	if($post['qset']!=''){
		$qset=$post['qset'];
	}else{
		$qset='';
	}
	if($post['attributes']!=''){
		$attributes=$post['attributes'];
	}else{
		$attributes=0;
	}
	if($post['update_chapter']!=''){
		$update_chapter=$post['update_chapter'];
	}else{
		$update_chapter=0;
	}
	if($post['question_wchapter']!=''){
		$question_wchapter=$post['question_wchapter'];
	}else{
		$question_wchapter=0;
	}
  // $field = 'department';
	// if(!$post['department'] || strlen(trim($post['department'])) == 0){
	//   $_SESSION['error'][$field] = "* Department cannot be empty";
	// }
  
  if(strlen(trim($name)) == 0){
	  $name = $post['username'];
  }

  //Check if any errors exist
	if(count($_SESSION['error']) > 0 || $post['validateForm'] == 2){
	?>
    <script type="text/javascript">
      $('#adminForm').slideDown();
      setState('adminForm','<?php echo SECURE_PATH;?>users/process.php','addForm=1&name=<?php echo $post['name'];?>&username=<?php echo $post['username'];?>&password=<?php echo $post['password'];?>&email=<?php echo $post['email'];?>&mobile=<?php echo $post['mobile'];?>&userlevel=<?php echo $post['userlevel'];?>&subject=<?php echo $post['subject'];?>&class=<?php echo $post['class'];?>&chapter=<?php echo $post['chapter'];?>&customcontent=<?php echo $post['customcontent'];?>&attributes=<?php echo $post['attributes'];?>&ppaper=<?php echo $post['ppaper'];?>&update_chapter=<?php echo $post['update_chapter'];?>&question_wchapter=<?php echo $post['question_wchapter'];?>&year=<?php echo $post['year'];?>&qset=<?php echo $post['qset'];?><?php if(isset($_POST['editform'])){ echo '&editform='.$post['editform'];}?><?php if(isset($_POST['from_page'])){ echo '&from_page='.$post['from_page'];}?>')
    </script>
  <?php
	}
	else{
		if($id=='NULL')
		{
		//	echo "INSERT users SET username='".$post['username']."',password='".md5($post['password'])."',userid='1',userlevel='".$post['userlevel']."',class='".$post['class']."',chapter='".$post['chapter']."',subject='".$post['subject']."',email='".$email."',mobile='".$mobile."',valid='1',name='".$post['name']."',customcontent=".$post['customcontent'].",hash='1',hash_generated='1',timestamp='".time()."'";
		
      mysqli_query($database->connection,"INSERT users SET username='".$post['username']."',password='".md5($post['password'])."',userid='1',userlevel='".$post['userlevel']."',class='".$post['class']."',chapter='".$post['chapter']."',subject='".$post['subject']."',email='".$email."',mobile='".$mobile."',valid='1',name='".$post['name']."',customcontent=".$customcontent.",attributes=".$attributes.",ppaper=".$ppaper.",update_chapter=".$update_chapter.",question_wchapter=".$question_wchapter.",year='".rtrim($post['year'],",")."',qset='".rtrim($post['qset'],",")."',hash='1',hash_generated='1',timestamp='".time()."'");
	 ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> User information saved successfully!
			</div>
		</div>
	</div>
	 <?php
    }
		else
		{
      mysqli_query($database->connection,"update users SET username='".$post['username']."',userlevel='".$post['userlevel']."',class='".$post['class']."',chapter='".$post['chapter']."',subject='".$post['subject']."',email='".$email."',mobile='".$mobile."',valid='1',name='".$post['name']."',customcontent=".$customcontent.",attributes=".$attributes.",ppaper=".$ppaper.",year='".rtrim($post['year'],",")."',qset='".rtrim($post['qset'],",")."',update_chapter=".$update_chapter.",question_wchapter=".$question_wchapter.",etimestamp='".time()."' where username='".$id."'");
	  ?>
	  <div class="col-lg-12 col-md-12">
		<div class="form-group">
			<div class="alert alert-success">
			<i class="fa fa-thumbs-up fa-2x"></i> User information Updated successfully!
			</div>
		</div>
	</div>
	  <?php
    }
  ?>
 
  <?php
  if(isset($_REQUEST['from_page']) && $_REQUEST['from_page']=='dataentry_users'){
  	?>
  	 <script type="text/javascript">
      setState('main-content','<?php echo SECURE_PATH;?>dataentryusers/','getLayout=true')
  </script>

  	<?php
  }
   else if(isset($_REQUEST['from_page']) && $_REQUEST['from_page']=='lecturer_users'){
  	?>
  	 <script type="text/javascript">
      setState('main-content','<?php echo SECURE_PATH;?>lecturerusers/','getLayout=true')
  </script>

  	<?php
  }
  else{
  	?>
 <script type="text/javascript">
      animateForm('<?php echo SECURE_PATH;?>users/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($post['page'])){echo $post['page'];}else{ echo '1';}?>');
  </script>
  	<?php
  }
  ?>
 
	<?php
  }
}
//Delete users
if(isset($_GET['rowDelete'])){
	mysqli_query($database->connection,"update users set valid='0',dtimestamp='".time()."' where username='".$_GET['rowDelete']."'");
?>
<div class="alert alert-success">Username deleted successfully!</div>
<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>dataentryusers/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
<?php
}
if(isset($_REQUEST['viewdetails'])){
	?>
	<form>
		<div class="form-group">
		<label for="recipient-name" class="col-form-label">Username</label>
		<input type="username1" id="username1" class="form-control" value="<?php echo $_REQUEST['viewdetails']; ?>" readonly >
	  </div>
	  <div class="form-group">
		<label for="recipient-name" class="col-form-label">Password</label>
		<input type="password" id="password" class="form-control" >
	  </div>
	  
	 <div class="center">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a href="#" class="btn btn-primary" data-dismiss="modal" onClick="setState('adminForm','<?php echo SECURE_PATH;?>users/process.php','validateForm1=1&password='+$('#password').val()+'&resetid=<?php echo $_REQUEST['viewdetails'];?>')">Update</a>
		</div>
    </form>	
<?php
}
if(isset($_REQUEST['validateForm1'])){
	if($_REQUEST['resetid']!=''){
		$result=$database->query('update users set password="'.md5($_REQUEST['password']).'" where username="'.$_REQUEST['resetid'].'"');
		?>
			<div class="alert alert-success"><i class="fa fa-thumbs-up fa-2x"></i> Password Updated Successfully!</div>
			
			<script type="text/javascript">
  animateForm('<?php echo SECURE_PATH;?>users/process.php','addForm=1','tableDisplay=1&page=<?php if(isset($_GET['page'])){echo $_GET['page'];}else{ echo '1';}?>');
</script>
	<?php
	}
}
//Display bulkreport
if(isset($_GET['tableDisplay'])){
	//Pagination code
  $limit=50;
  if(isset($_GET['page']))
  {
    $start = ($_GET['page'] - 1) * $limit;     //first item to display on this page
    $page=$_GET['page'];
  }
  else
  {
    $start = 0;      //if no page var is given, set start to 0
  $page=0;
  }
  //Search Form
?>
<?php
  $tableName = 'users';
  $condition = "valid=1";//"userlevel = '8' OR userlevel = '9' OR userlevel = '7' OR userlevel = '6'";
  if(isset($_GET['keyword'])){
  }
  if(strlen($condition) > 0){
    $condition = 'WHERE '.$condition;
  }
  //$query_string = $_SERVER['QUERY_STRING'];
  $pagination = $session->showPagination(SECURE_PATH."users/process.php?tableDisplay=1&",$tableName,$start,$limit,$page,$condition);
  $q = "SELECT * FROM $tableName ".$condition." ORDER BY userlevel DESC";
  $result_sel = $database->query($q);
  $numres = mysqli_num_rows($result_sel);
  if($session->userlevel==9){
    $query = "SELECT * FROM $tableName ".$condition." ORDER BY userlevel DESC LIMIT $start,$limit";
  }
  else{
    $query = "SELECT * FROM $tableName ".$condition." AND userlevel<9 ORDER BY userlevel DESC LIMIT $start,$limit";
  }
  
  //echo $query;
  $data_sel = $database->query($query);
  if(($start+$limit) > $numres){
	 $onpage = $numres;
	 }
	 else{
	  $onpage = $start+$limit;
	 }
  if($numres > 0){
	?>
	<section class="content-area">
		<div class="container">
			<div class="row Data-Tables">
				<div class="col-xl-12 col-lg-12">
					<div class="card border-0 shadow mb-4">
						<div
							class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
							<h6 class="m-0 font-weight-bold text-primary">User Details</h6>
						</div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-bordered"  width="100%" cellspacing="0">
									<thead>
										<tr>
											<th class="text-left" nowrap>Username (Full Name)</th>
											  <th class="text-left" nowrap>Full Name</th>
											  <th class="text-left" nowrap>User Type</th>
											  <th class="text-left" nowrap>Email</th>
												<th class="text-left" nowrap>Mobile</th>
											  <th class="text-left" nowrap>Actions</th>
											</tr>
									</thead>
									<tbody>
									<?php
									if(isset($_GET['page'])){
										if($_GET['page']==1)
										$i=1;
										else
										$i=(($_GET['page']-1)*50)+1;
									}else $i=1;
                  
									while($value = mysqli_fetch_array($data_sel))
									{
										  if($value['userlevel']=='7'){
											  $userlevel="Data Entry";
										  }else if($value['userlevel']=='8'){
											  $userlevel="Lecture";
										  }else if($value['userlevel']=='9'){
											  $userlevel="Admin";
										  }else{
										  }
									?>
									<tr>
										<td class="text-left"><?php echo $value['username'];?></td>
										<td class="text-left"><?php echo ucwords($value['name']);?></td>
										<td class="text-left"><?php echo ucwords($userlevel);?></td>
										<td class="text-left"><?php echo ucwords($value['email']);?></td>
										<td class="text-left"><?php echo '+91'.ucwords($value['mobile']);?></td>
										<td>
													<div id="dummy"></div>
										  <a href="#" class="btn btn-sm btn-primary" onClick="setState('adminForm','<?php echo SECURE_PATH;?>users/process.php','addForm=2&editform=<?php echo $value['username'];?>&page=<?php echo $_GET['page']; ?>')"><i
												  class="material-icons md-16 pt-1">edit</i></a><?php if($value['userlevel'] != 9) { ?>
										  <a href="#" class="btn btn-sm btn-danger" onClick="confirmDelete('adminForm','<?php echo SECURE_PATH;?>users/process.php','rowDelete=<?php echo $value['username'];?>')"><i
												  class="material-icons md-16 pt-1">delete</i></a>
										<a data-toggle="modal" data-target="#myModal" class="btn  text-uppercase  col-md-3 text-white" style="background-color: #555555;" onClick="setState('viewdetails','<?php echo SECURE_PATH;?>users/process.php','viewdetails=<?php echo $value['username'];?>')"><i class="fa fa-key black" aria-hidden="true"></i></a>
										  <?php
											}
										  ?>
										</td>
									  </tr>
									<?php
									$i++;
									}
									?>
									</tbody>
								</table>
							</div>
						</div>
						<div class="my-3 footer-pagination d-flex justify-content-between align-items-center">
						<p class="mb-0">Showing <?php echo ($start+1); ?> To <?php echo ($onpage); ?> results out of <?php echo $numres; ?></p>
						<?php echo $pagination ;?></div>
					</div>
				</div>
			</div>
		</div>

  </section>
 
	<?php
	}
	else{
	?>
		<div class="text-danger text-center">No Results Found</div>
  <?php
	}

}
?>
<?php
if(isset($_REQUEST['getchapter']))
{	
	$_REQUEST['chapter']=explode(",",$_REQUEST['chapter']);
	?>
		
			<label class="col-lg-4 col-form-label">Chapter<span style="color:red;">*</span></label>
			<div class="col-lg-6 ml-5">
				<select name="chapter" class="selectpicker1" id="chapter" multiple data-live-search="true"  onchange="chapterfunction();setStateGet('chapview','<?php echo SECURE_PATH;?>users/process.php','getchaptercview=1&chapter='+$('#chapter').val()+'')">
					<?php
					//echo "SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').") ";
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1' AND class IN(".rtrim($_REQUEST['class'],',').")   AND subject IN(".rtrim($_REQUEST['subject'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					  ?>
					<option value="<?php echo $data1['id'];?>"  <?php if(isset($_REQUEST['chapter'])) {if(in_array($data1['id'], $_REQUEST['chapter'])) { echo 'selected="selected"'; } } ?> ><?php echo $data1['chapter'];?></option>
					<?php
					 $i++;
					}
					?>
				</select>
				
				<span class="text-danger"><?php if(isset($_SESSION['error']['chapter'])){ echo $_SESSION['error']['chapter'];}?></span>
			</div>
		   
		
    <?php
}?>
<?php
if(isset($_REQUEST['getchaptercview']))
{	
	
	
	?>
		
					<?php
					$i=1;
					$count=0;
					$row1 = $database->query("SELECT * FROM `chapter` WHERE estatus='1'    AND id IN(".rtrim($_REQUEST['chapter'],',').")  "); 
					while($data1 = mysqli_fetch_array($row1))
					{
						$userd=$database->query("select * from users where valid='1' and FIND_IN_SET(".$data1['id'].",`chapter`)");
						$rowdcount=mysqli_num_rows($userd);
					 
						  $count=$count+$rowdcount;
					 $i++;
					}
					
					?>
					<a href='#'  style="cursor:pointer;color: #007bff;" title="Print"  data-toggle="modal" data-target="#messageDetails1" onClick="setStateGet('viewDetails1','<?php echo SECURE_PATH;?>users/process1.php','viewDetails1=1&chapter=<?php echo $_REQUEST['chapter']; ?>');"><?php echo $count; ?> Users</a>
		
    <?php
}?>
		<?php
	if(isset($_REQUEST['getexamset']))
	{	
		$_REQUEST['qset']=explode(",",$_REQUEST['qset']);
	?>
		<label for="inputTopi" class="label">Set ID</label>
		<select class="form-control selectpicker4" multiple name="qset" value=""  id="qset"  >
			<option value=''>-- Select --</option>
			<?php
			
			$sel=$database->query("select * from previous_questions where estatus='1' and year in (".$_POST['year'].")  ORDER by id DESC");
			while($row=mysqli_fetch_array($sel)){
				$sel1=$database->query("select * from previous_sets where estatus='1' and pid='".$row['id']."'");
				while($row1=mysqli_fetch_array($sel1)){
				?>
					
					<option value="<?php echo $row1['id'];?>"  <?php if(isset($_REQUEST['qset'])) {if(in_array($row1['id'], $_REQUEST['qset'])) { echo 'selected="selected"'; } } ?> ><?php echo $row1['qset'];?></option>
			<?php
				}
			}
			?>
	</select>
    <?php
}?>

<script>
function selectfunction(){
	
	var userlevel=$('#userlevel').val();
	var ppaper=$('#ppaper').val();
	if(userlevel=='6' || userlevel=='7' || userlevel=='8'){
		$('.subjectdiv').show();
		if(userlevel=='7'){
			$('.contentd').show();
			$('.contentd1').show();
			$('.previous1').show();
			$('.update_chapter1').hide();
			$('.question_wchapter1').hide();
			$('.previous2').hide();
		}else if(userlevel=='8'){
		$('.contentd').show();
		$('.contentd1').hide();
		$('.previous1').show();
		$('.update_chapter1').show();
		$('.question_wchapter1').show();
			if(ppaper=='1'){
				$('.previous2').show();
			}else{
				$('.previous2').hide();
			}
		
		}else{
			$('.contentd').hide();
			$('.previous1').hide();
			$('.previous2').hide();
			$('.contentd1').hide();
			$('.update_chapter1').hide();
			$('.question_wchapter1').hide();
		}

	}else{
		$('.subjectdiv').hide();
        $('.contentd').hide();
		$('.previous1').hide();
		$('.previous2').hide();
		$('.question_wchapter').hide();
		

	}
}
</script>

	<script type="text/javascript">


function getFields1()
{
    var ass='';
    $('.class3').each(function(element) {
		if($(this).is(':checked')) {
                    $('#'+$(this).attr('id')).val();
			ass+=$(this).val()+",";
		}
		$('#class').val(ass);
	});
        
        

    
}
</script>
<script type="text/javascript">
	function getFields5()
    {
    	
    var ass='';
    $('.classn').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
      ass+=$(this).val()+",";
	 
    }else{
      $('#'+$(this).attr('id')).val(0);
      ass+=$(this).val()+",";
	 
    }
//alert(ass);

  });
        
        

    
}
	function getFields6()
    {
    	var userlevel=$('#userlevel').val();
    $('.ppaper').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 if(userlevel=='8'){
		$('.previous2').show();
	 }else{
		$('.previous2').hide();
	 }

    }else{
      $('#'+$(this).attr('id')).val(0);
     if(userlevel=='8'){
		$('.previous2').hide();
	  }else{
		$('.previous2').hide();
	 }
    }
//alert(ass);

  });
        
        

    
}
	function getFields7()
    {
    	var userlevel=$('#userlevel').val();
    $('.attributes').each(function(element) {
    if($(this).is(':checked')) {
     $('#'+$(this).attr('id')).val(1);
	 

    }else{
      $('#'+$(this).attr('id')).val(0);
    
    }
//alert(ass);

  });
        
        

    
}

	function getFields8()
    {
		var userlevel=$('#userlevel').val();
		$('.update_chapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }

	function getFields9()
    {
		$('.question_wchapter').each(function(element) {
			if($(this).is(':checked')) {
				$('#'+$(this).attr('id')).val(1);
			}else{
				$('#'+$(this).attr('id')).val(0);
			
			}
		});
    }
    </script>
<script>
 $(".selectpicker1").selectpicker('refresh');
 $(".selectpicker2").selectpicker('refresh');
  $(".selectpicker3").selectpicker('refresh');
 $(".selectpicker4").selectpicker('refresh');
</script>