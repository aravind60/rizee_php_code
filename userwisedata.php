<?php

ini_set('display_errors','0');
define("DB_SERVER","localhost");
define("DB_USER","rspace");
define("DB_PWD","Rsp@2019");
define("DB_NAME","qsbank");
/*define("DB_SERVER","localhost");
define("DB_USER","root");
define("DB_PWD","");
define("DB_NAME","qsbank");*/
$conn=mysqli_connect(DB_SERVER,DB_USER,DB_PWD,DB_NAME) ;

if(mysqli_connect_errno()){
    echo "failed to connect to mysql".mysqli_connect_error();
}

function query($sql)
{
    global $conn;


    return mysqli_query($conn,$sql);
}
$date=gmstrftime('%Y-%m-%d',time()+19800);
$exdate=explode("-",$date);
$fy=$exdate[0];
$fy1=$exdate[0]+1;
$fileName = "Topic wise report".gmstrftime('%d%m%Y%H%M%S',time()+19800). ".xls";

//header("Content-Disposition: attachment; filename=\"$fileName\"");
//header("Content-Type: application/vnd.ms-excel");
?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
		<style>
			.styleb
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
			}

			.stylebu
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:14px;
				color:#000000;
				font-weight:bold;
				text-decoration:underline;
			}

			.stylebu1
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:20px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}

			.style
			{
				font-family:verdana;
				font-size:12px;
				color:#000000;
			}
			.style1
			{
				font-family:verdana;
				font-size:14px;
				color:#000000;
			}

			.hstyleb
			{
				font-family:verdana;
				font-size:30px;
				font-weight:bold;
				color:#000000;
				text-decoration:underline;
			}
			.styleb11
			{
				font-family:Optima,Segoe,Segoe UI,Candara,Calibri,Arial,sans-serif;
				font-size:12px;
				color:#000000;
				font-weight:bold;
				/*text-decoration:underline;*/
			}
		</style>
	</head>
	<body>  
		<table border="0" cellpadding="2" cellspacing="2" width="800px" align="center">
        <tr align="left">
				<td>
					<table border="1" cellpadding="2" cellspacing="2" width="100%" align="center" style="border-collapse:collapse;">
						<tr align="center" class="styleb">
						<th style="text-align:center;">Sr.No.</th>
						<th style="text-align:center;">Attribute Username</th>
						<th style="text-align:center;">Full Name</th>
						<th style="text-align:center;">Total Verified Questions</th>
						<th style="text-align:center;">Total Reviewed Questions</th>
						<th style="text-align:center;">Total Error Percentage</th>
							
						</tr>
                        <?php
                             $k=1;
                            $sel=query("SELECT *  FROM users WHERE valid=1 and userlevel='3'  ORDER BY subject ASC");
                            while($row = mysqli_fetch_array($sel)){
								$sql1=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and vusername1='".$row['username']."'");
								$row1=mysqli_fetch_array($sql1);
								
								$sql11=query("select count(id) as cnt from createquestion where estatus='1' and vstatus1='1' and vusername1='".$row['username']."' and review_status='1'");
								$row11=mysqli_fetch_array($sql11);
								$s=0;
								echo "select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'   and a.subject='".$row['subject']."'  and a.vusername1='".$row['username']."' ";
								$sqlv1=query("select  a.id,b.changes_count,b.changes from createquestion as a inner join reviewer_log as b on a.estatus='1' and a.vstatus1='1'  and a.review_status='1' and b.estatus='1' and a.id=b.question_id  and b.message='1' and b.version='2'   and a.subject='".$row['subject']."'  and a.vusername1='".$row['username']."' ");
								while($rowv1=mysqli_fetch_array($sqlv1)){
							
							
							
									$changes=$rowv1['changes'];
									if($rowv1['changes_count']!=0){
										//$s++;
									}
									
									$changes=$rowv1['changes'];
									$data=explode("^",$changes);
									$a=1;
									
									if($rowv1['changes']!='No Changes'){
									if(count($data)>0){
											$com=0;
											$time=0;
											$use=0;
											$qt=0;
											$qt1=0;
											$c1=0;
											$e1=0;
											$cha1=0;
											$top=0;
											$_REQUEST['issues_list']='complexity,usageset,inputquestion,questiontheory,class,exam,chapter,topic';
											foreach($data as $dataa){
												$data1=explode("_",$dataa);
												if($data1[0]!=''){
													$new='';
													$old='';
													
													if($data1[0]=='complexity'){
														if (in_array("complexity", explode(",",$_REQUEST['issues_list']))){
															$comple++;
															
															$com=1;
														}
													}
													if($data1[0]=='timeduration'){
														if (in_array("timeduration", explode(",",$_REQUEST['issues_list']))){
															$timing++;
															$time=1;
														}
														
													}
													if($data1[0]=='usageset'){
														if (in_array("usageset", explode(",",$_REQUEST['issues_list']))){
															$usage++;
															$use=1;
														}
													}
													if($data1[0]=='questiontheory'){
														if (in_array("questiontheory", explode(",",$_REQUEST['issues_list']))){
															$qtheory++;
															$qt=1;
														}
													}
													if($data1[0]=='inputquestion'){
														if (in_array("inputquestion", explode(",",$_REQUEST['issues_list']))){	
															$qtype++;
															$qt1=1;

														}
													}
													if($data1[0]=='class'){
														if (in_array("class", explode(",",$_REQUEST['issues_list']))){		
															$class++;
															$c1=1;

														}
													}
													if($data1[0]=='exam'){
														if (in_array("exam", explode(",",$_REQUEST['issues_list']))){	
															$exam++;
															$e1=1;
														}
													}
													if($data1[0]=='chapter'){
														if (in_array("chapter", explode(",",$_REQUEST['issues_list']))){
															$chapter12++;
															$cha1=1;
														}
													}
													if($data1[0]=='topic'){
														if (in_array("topic", explode(",",$_REQUEST['issues_list']))){
														
															$topic++;
															$top=1;
														}
													}
													
													?>
														
													<?php
													
												
												$a++;
												}
											}
											
											if($com!=0 || $time!=0 || $use!=0 || $qt!=0 || $qt1!=0 || $c1!=0 || $e1!=0 || $cha1!=0 || $top!=0){
													$s++;
												}
										}
										
										
									}
									
								}
								

								$percentp = $s/$row11['cnt'];
								if(is_nan($percentp))
																		$percentp=0;
																	else
																		$percentp=$percentp;
																	$percent_vp= number_format( $percentp * 100) . '%';
									echo "<tr>";
										?>	
										
											<td><?php echo $k;?></td>
											<td ><?php echo $row['username'];?></td>
											<td><?php echo $row['name']; ?></td>
											<td ><?php echo $row1['cnt'];?></td>
											<td ><?php echo $row11['cnt'];?></td>
											<td ><?php echo $percent_vp;?></td>
											
										<?php
											
										echo "</tr>";
										$k++;
                                                            
                                                
                                       
                            }

                        ?>
                        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
	</body>
</html>