<?php

use Vimeo\Vimeo;
use Vimeo\Exceptions\VimeoUploadException;

/**
 *   Copyright 2013 Vimeo
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

$config = require(__DIR__ . '/init.php');
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
if (empty($config['access_token'])) {
    throw new Exception(
        'You can not upload a file without an access token. You can find this token on your app page, or generate ' .
        'one using `auth.php`.'
    );
}

// Instantiate the library with your client id, secret and access token (pulled from dev site)
$lib = new Vimeo($config['client_id'], $config['client_secret'], $config['access_token']);

// Create a variable with a hard coded path to your file system
//$file_name ="../../files/video.mp4";
$file_name='/videos/'.$_REQUEST['videoid'];
  

try {
    $uri = $lib->request($file_name,array(),'DELETE');
    $jsonData['status'] = 1;
    
   
} catch (VimeoUploadException $e) {
    // We may have had an error. We can't resolve it here necessarily, so report it to the user.
    //echo 'Error uploading ' . $file_name . "\n";
    //echo 'Server reported: ' . $e->getMessage() . "\n";
    $jsonData['video'] = $_REQUEST['video'];
    $jsonData['status'] = 2;
    $jsonData['statusdata'] = 'Server reported: ' . $e->getMessage() . "\n";
} catch (VimeoRequestException $e) {
   // echo 'There was an error making the request.' . "\n";
    //echo 'Server reported: ' . $e->getMessage() . "\n";
    $jsonData['video'] = $_REQUEST['video'];
    $jsonData['status'] = 2;
    $jsonData['statusdata'] = 'Server reported: ' . $e->getMessage() . "\n";
}
echo json_encode($jsonData);
?>