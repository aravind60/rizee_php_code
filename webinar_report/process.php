<?php
ini_set('display_errors','On');
include('../include/session.php');
include('../Classes/Excel/PHPExcel.php');
error_reporting(0);
ini_set('memory_limit',-1);
ini_set('max_execution_time', 0);
if(!$session->logged_in){
?>
	<script type="text/javascript">
		setStateGet('main','<?php echo SECURE_PATH;?>login_process.php','loginForm=1');
	</script>
<?php
}
?>


	<?php
	if(isset($_REQUEST['tableDisplay'])){
		
		
	
	?>
		<style>
			.subjectanalysis {
				padding: .75rem 1.25rem;
				margin-bottom: 0;
				background-color: rgba(0,0,0,.03);
				border-bottom: 1px solid rgba(0,0,0,.125);
			}
			.subject-wise-analysis.nav-pills .nav-link.active {
				background-color:#000;
				color:#fff;
			}
			.subject-analysis-content label, h6 {
				color:#000;
			}
			.subject-analysis-content .table td p {
				color:#000000c7;
				font-weight:bold;
			}
			.subject-analysis-content .table th, .subject-analysis-content .table td {
				font-size:12px !important;
				padding:0.7rem 0.1rem !important;
			}
		</style>
		<style>
	
	 </style>
	<script>
	$('.chapter').selectpicker1();
	 </script>
	 	<?php
	$todate=strtotime(date('m/d/Y'));

  $todate1 = strtotime(date('m/d/Y'). '23:59:59');
  $datey = strtotime('-1 days',$todate);
  $datey1 = strtotime(date('d-m-Y',$datey).'23:59:59');
	 $todaysel=$database->query("SELECT count(id) as cnt FROM webinar_registration where  estatus='1' and timestamp between ".$todate." and ".$todate1."  ORDER BY timestamp DESC");
	 $rowl=mysqli_fetch_array($todaysel);

	$yessel=$database->query("SELECT count(id) as cnt FROM webinar_registration where   estatus='1' and timestamp between ".$datey." and ".$datey1."   ORDER BY timestamp DESC");
	 $rowly=mysqli_fetch_array($yessel);

	 $yessel1=$database->query("SELECT count(id) as cnt FROM webinar_registration  where estatus='1'   ORDER BY timestamp DESC");
	 $rowly1=mysqli_fetch_array($yessel1);
	 ?>
		<section class="content-area">
			<div class="container">
				<div class="row Data-Tables">
					<div class="col-xl-12 col-lg-12"> 
						<div class="card border-0 shadow mb-4">
							<div class="card-header py-3">
								<h6 class="m-0 font-weight-bold text-primary"> Webinar Registration Report</h6>
							</div>
							<div class="card-body">
									<table class="table table-bordered">
										<thead>
											<tr class="row mx-0">
												
												<th class="bg-white text-center col">
													<i class="far fa-user fa-4x text-light mb-2"></i>
													<h5>Users</h5>
												</th> 
												
												<th class="bg-white col">
													<a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>webinar_report/process.php','tableDisplay=1&type=Today')" ><h6>Today</h6>
													<h3 class="text-danger pt-4"><?php if($rowl['cnt']!=''){ echo $rowl['cnt']; }else{ echo '0'; }?></h3></a>
												   
												</th>
											   
												<th class="bg-light col">
													 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>webinar_report/process.php','tableDisplay=1&type=Yesterday')" ><h6>Yesterday</h6>
													<h3 class="text-danger pt-4"><?php if($rowly['cnt']!=''){ echo $rowly['cnt']; }else{ echo '0'; }?></h3></a>
													
												</th>
												
												<th class="bg-white col">
													 <a onClick="setStateGet('adminTable','<?php echo SECURE_PATH;?>webinar_report/process.php','tableDisplay=1&status=&type=overall')" ><h6>Overall</h6>
													<h3 class="text-danger pt-4"><?php if($rowly1['cnt']!=''){ echo $rowly1['cnt']; }else{ echo '0'; }?></h3></a>
												   
												</th>
												

											</tr>
										</thead>
									</table>
									<br />
									<script type="text/javascript">


									  $('#dataTable').DataTable({
										"pageLength": 50,
										"order": [[ 1, 'asc' ]]
										
									  });

									</script>
									<div class="card border-0">
										<div class="card-body table-responsive table_data p-0">
											<table class="table table-bordered dashboard-table mb-0" id="dataTable">
												<thead class="thead-light">
													<tr>
														<th scope="col" class="text-center" >Student Name</th>
														<th scope="col" class="text-center" >Registration Date</th>
														<th scope="col" class="text-center">Class</th>
														<th scope="col" class="text-center">Mobile</th>
														<th scope="col" class="text-center">Email</th>
													</tr>
													
												</thead>
											<tbody>
												<?php
												
												if(isset($_REQUEST['type'])){
													if($_REQUEST['type']=="Today"){
														$date=" and timestamp between ".$todate." and ".$todate1." ";
													} else if($_REQUEST['type']=="Yesterday"){
														$date=" and timestamp between ".$datey." and ".$datey1." ";
													}else if($_REQUEST['type']=="Last 2 Days"){
														$date=" and timestamp between ".$rowly1." and ".$todate1." ";
													}else{
														$date='';
													}
												}else{
													$date='';
												}
												//echo "select * from webinar_registration where estatus='1' ".$date."";
												$sel=$database->query("select * from webinar_registration where estatus='1' ".$date."");
												$k=1;
												while($row=mysqli_fetch_array($sel)){
													$sel1=$database->query("select * from class where  id='".$row['class']."'");
													$row1=mysqli_fetch_array($sel1);
													
												?>
													<tr>
														<td><?php echo $row['name'];?></td>
														<td><?php echo date("d/m/Y",$row['timestamp']); ?></td>
														<td><?php echo $row1['class']; ?></td>
														<td><?php echo $row['mobile']; ?></td>
														<td><?php echo $row['email']; ?></td>
													</tr>
												<?php $k++; } ?>
											</tbody>
										</table>
									
									
									</div>	
								</div>
								
								
							</div>
						</div>
					</div>
				</div>

			</div>
		</section>
		
			
	<?php
	}
	
	?>
	
